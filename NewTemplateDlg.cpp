// SelectTemplate.cpp : implementation file
//

#include "stdafx.h"
#include "NewTemplateDlg.h"
#include "ResLangFileReader.h"


// CNewTemplateDlg dialog

IMPLEMENT_DYNAMIC(CNewTemplateDlg, CDialog)

BEGIN_MESSAGE_MAP(CNewTemplateDlg, CDialog)
	ON_LBN_SELCHANGE(IDC_LISTDLG9_1, &CNewTemplateDlg::OnLbnSelchangeListdlg91)
	ON_EN_CHANGE(IDC_EDITDLG9_1, &CNewTemplateDlg::OnEnChangeEditdlg91)
	ON_BN_CLICKED(IDOK, &CNewTemplateDlg::OnBnClickedOk)
END_MESSAGE_MAP()

CNewTemplateDlg::CNewTemplateDlg(CWnd* pParent /*=NULL*/,int open_as)
	: CDialog(CNewTemplateDlg::IDD, pParent),
		m_sLangFN(L""),
		m_bInitialized(FALSE),
		m_nSelectedIndex(-1),
		m_sTemplateName(L""),
		m_sPricelistName(L"")
{
	m_nOpenAs = open_as;
}

CNewTemplateDlg::~CNewTemplateDlg()
{
}

void CNewTemplateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNewTemplateDlg)
	DDX_Control(pDX, IDC_LBLDLG9_1, m_lbldlg9_1);
	DDX_Control(pDX, IDC_LBLDLG9_2, m_lbldlg9_2);

	DDX_Control(pDX, IDC_LISTDLG9_1, m_lbdlg9_1);

	DDX_Control(pDX, IDC_EDITDLG9_1, m_editdlg9_1);

	DDX_Control(pDX, IDOK, m_wndOKBtn);
	DDX_Control(pDX, IDCANCEL, m_wndCancelBtn);

	//}}AFX_DATA_MAP

}

// CNewTemplateDlg message handlers
BOOL CNewTemplateDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (!m_bInitialized)
	{

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				m_editdlg9_1.SetDisabledColor(BLACK,INFOBK);
				m_editdlg9_1.SetEnabledColor(BLACK,WHITE);
				if (m_nOpenAs == 0)
				{
					SetWindowText(xml.str(IDS_STRING4600));
					m_lbldlg9_1.SetWindowText(xml.str(IDS_STRING4601));
					m_lbldlg9_2.SetWindowText(xml.str(IDS_STRING4602));
					m_editdlg9_1.EnableWindow(TRUE);
					m_editdlg9_1.SetReadOnly(FALSE);
					m_wndOKBtn.EnableWindow(FALSE);
				}
				else if (m_nOpenAs == 1)
				{
					SetWindowText(xml.str(IDS_STRING46001));
					m_lbldlg9_1.SetWindowText(xml.str(IDS_STRING46011));
					m_lbldlg9_2.SetWindowText(xml.str(IDS_STRING46021));
					m_editdlg9_1.SetWindowTextW(m_sTemplateName);
					m_editdlg9_1.EnableWindow(FALSE);
					m_editdlg9_1.SetReadOnly(TRUE);
					m_wndOKBtn.EnableWindow(TRUE);
				}
				m_wndOKBtn.SetWindowText(xml.str(IDS_STRING100));
				m_wndCancelBtn.SetWindowText(xml.str(IDS_STRING101));
			}
			xml.clean();
		}

		if (m_vecVecPricelist.size() > 0)
		{
			for (UINT i = 0;i < m_vecVecPricelist.size();i++)
			{
				m_lbdlg9_1.AddString(m_vecVecPricelist[i].getName());
			}
			if (m_nOpenAs == 1)
			{			
				m_lbdlg9_1.SetCurSel(m_lbdlg9_1.FindString(0,m_sPricelistName));
			}
		}


		m_bInitialized = TRUE;
	}

	return TRUE;
}

// CNewTemplateDlg message handlers

void CNewTemplateDlg::setData(CVecPricelist& vec)	
{ 
	m_vecVecPricelist = vec; 
}

BOOL CNewTemplateDlg::isSelected()
{
	return (m_sPrl.GetLength() > 0 && m_sName.GetLength() > 0);
}

void CNewTemplateDlg::OnLbnSelchangeListdlg91()
{
	m_nSelectedIndex = m_lbdlg9_1.GetCurSel();
	m_wndOKBtn.EnableWindow((m_lbdlg9_1.GetCurSel() > -1) && m_editdlg9_1.GetWindowTextLengthW() > 0);
}

void CNewTemplateDlg::OnEnChangeEditdlg91()
{
	m_wndOKBtn.EnableWindow((m_lbdlg9_1.GetCurSel() > -1) && m_editdlg9_1.GetWindowTextLengthW() > 0);
}

void CNewTemplateDlg::OnBnClickedOk()
{
	m_nSelectedIndex = m_lbdlg9_1.GetCurSel();
	if (m_nSelectedIndex > -1 && m_nSelectedIndex < m_vecVecPricelist.size() )
	{
		m_sPrl =	m_vecVecPricelist[m_nSelectedIndex].getPricelist();
		m_sPrlName =	m_vecVecPricelist[m_nSelectedIndex].getName();
		m_nPrlID =	m_vecVecPricelist[m_nSelectedIndex].getPrlID();
	}

	m_sName = m_editdlg9_1.getText();


	OnOK();
}
