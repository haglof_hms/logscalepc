#pragma once

#include "Resource.h"

#include "DBHandling.h"
#include "DatePickerCombo.h"
#include "afxdtctl.h"

///////////////////////////////////////////////////////////////////////////////////////////
// CLogSalesDoc

class CLogSalesDoc : public CDocument
{
protected: // create from serialization only
	CLogSalesDoc();
	DECLARE_DYNCREATE(CLogSalesDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLogSalesDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CLogSalesDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CLogSalesDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};



#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
/////////////////////////////////////////////////////////////////////////////
// class CLogSalesFrame

class CLogSalesFrame : public CChildFrameBase
{
//private:

	DECLARE_DYNCREATE(CLogSalesFrame)
	CXTPStatusBar m_wndStatusBar;
	CXTPToolBar m_wndToolBar;
	CXTPDockingPaneManager m_paneManager;
	CString m_sLangFN;
	CString m_sShellDataFile;
	int m_nShellDataIndex;
	BOOL m_bFirstOpen;
	BOOL m_bInitReports;

	CDialogBar m_wndFieldSelectionDlg;		// Sample Field chooser window

	BOOL m_bSysCommand;

	vecSTDReports m_vecReports;
	CComboBox m_cboxPrintOut;

	BOOL m_bIsPrintOutTBtn;

	CFont *m_fnt1;

protected:
	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	HICON m_hIcon;
public:
	CLogSalesFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

	BOOL isFirstOpen()		{ return m_bFirstOpen; }

// Attributes

// Operations

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	virtual ~CLogSalesFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	
	//{{AFX_MSG(CLogSalesFrame)
	afx_msg int OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg void OnDestroy(void);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnSysCommand(UINT nID,LPARAM lParam);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	
	afx_msg void OnUpdatePrintOutTBtn(CCmdUI* pCmdUI);
	afx_msg void OnTBBtnPrintOut();
	afx_msg void OnPrintOutCBox();

	afx_msg void OnCommand(UINT nID);
	//}}AFX_MSG
	
	DECLARE_MESSAGE_MAP()
};

// CLogSalesFormView form view
class CLogSalesReportView;

class CLogSalesFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CLogSalesFormView)

	BOOL m_bInitialized;
	CString m_sLangFN;

	CString m_sMsgCap;
	CString m_sMsgDelete1;
	CString m_sMsgDelete2;

	CString m_sMsgCheck1;
	CString m_sMsgCheck2;
	CString m_sMsgCheck3;

	CString m_sMsgConfirmSave;

	CString m_sMsgOpenEXCEL;

	CString m_sTagNumber;
	CString m_sTicket;
	
	CString m_sMsgIllegalChars;

	CMyExtStatic m_lbl6_1;
	CMyExtStatic m_lbl6_20;
	CMyExtStatic m_lbl6_21;
	CMyExtStatic m_lbl6_22;
	CMyExtStatic m_lbl6_23;
	CMyExtStatic m_lbl6_2;
	CMyExtStatic m_lbl6_3;
	CMyExtStatic m_lbl6_24;
	CMyExtStatic m_lbl6_25;
	CMyExtStatic m_lbl6_26;
	CMyExtStatic m_lbl6_27;
	CMyExtStatic m_lbl6_28;
	CMyExtStatic m_lbl6_29;
	CMyExtStatic m_lbl6_30;
	CMyExtStatic m_lbl6_31;

	CMyExtEdit m_ed6_1;
	CMyExtEdit m_ed6_15;
	CMyExtEdit m_ed6_16;
	CMyExtEdit m_ed6_17;
	CMyExtEdit m_ed6_18;
	CMyExtEdit m_ed6_19;

	CComboBox m_cbx6_1;
	CComboBox m_cbx6_14;
	CComboBox m_cbx6_15;
	CComboBox m_cbx6_16;
	CComboBox m_cbx6_17;
	CComboBox m_cbx6_18;
	CComboBox m_cbx6_19;
	CComboBox m_cbx6_20;

	CStringArray m_sarrHeadLines;

	CVecRegister m_vecRegister;

	CInventory m_recInventory;
	vecInventory m_vecInventory;

	CVecLogs m_vecLogs;

	vecInventoryLogs m_vecInvLogs;

	CMyTabControl m_wndTabControl;
	CXTPTabManagerItem *m_tabManager;

	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon);

	CDBHandling *m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;

	QUIT_TYPES::Q_T_RETURN checkData(CHECK_SAVE_TYPES::CHECK_SAVE cs);
	BOOL Save();
	void DeleteLog();

	void clearView();
	void enableView(BOOL enable);

	void populateReport(bool set_row = false,int log_id = -1);

	CLogSalesReportView *getSalesView(void);

	void exportToEXCEL();

protected:
	CLogSalesFormView();           // protected constructor used by dynamic creation
	virtual ~CLogSalesFormView();

public:
	enum { IDD = IDD_FORMVIEW6 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

	void setName(LPCTSTR name)	{ m_ed6_19.SetWindowTextW(name); }
	void setDate(LPCTSTR date)	{ m_csDate = date; UpdateData(FALSE); }
	void setGPSCoord1(LPCTSTR gps)	{ m_ed6_16.SetWindowTextW(gps); }
	void setGPSCoord2(LPCTSTR gps)	{ m_ed6_17.SetWindowTextW(gps); }
	void setNote(LPCTSTR note)	{ m_ed6_18.SetWindowTextW(note); }
	void setBuyer(LPCTSTR buyer)	
	{ 
		m_cbx6_1.SetCurSel(m_cbx6_1.FindStringExact(0,buyer)); 
	}

	QUIT_TYPES::Q_T_RETURN doSave(CHECK_SAVE_TYPES::CHECK_SAVE cs);
	void doOpenTagFile();
	void doPrintOut();
	void doDelete();
	void doExportToEXCEL();

	CInventory& getInventoryRec()	{ return m_recInventory; }

protected:
	//{{AFX_VIRTUAL(CLogSalesFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CLogSalesFormView)

	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnClose();
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam, LPARAM lParam);

	afx_msg void OnDestroy(void);

//	afx_msg void OnCommand(UINT nID);

	//{{AFX_MSG(CLogSalesFormView)

	DECLARE_MESSAGE_MAP()
public:
	CDateTimeCtrl m_dtDate;
	CString m_csDate;
};

////////////////////////////////////////////////////////////////////////////////////////
// CLogSalesReportView

class CLogSalesReportView : public CXTPReportView
{
	DECLARE_DYNCREATE(CLogSalesReportView)
	BOOL m_bInitialized;
	CString m_sLangFN;
	CString m_sFieldSelection;

	CString m_sMsgCap;
	CString m_sMsgNoMatchForTags;
	CString m_sMsgAllLogsAlreadyInInventory;
	CString m_sOpenDlgTagFilesCaliper;
	CString m_sOpenDlgTagFilesExcel;
	CString m_sOpenDlgTagFilesAll;

	CXTPReportSubListControl m_wndSubList;

	CDBHandling *m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;

	BOOL m_bPrintOutEnabled;
	BOOL m_bExportToExcel;

	BOOL setupReport();

	CStringArray m_sarrHeadLines;

	void handleTagFile(LPCTSTR file,BOOL reset);

	void openTagFile();
	void Delete();
	void exportToEXCEL();
public:

	void doRunPrintPreview(void);

	void doOpenTagFile()	{ openTagFile(); }
	void doPrintOut();
	void doExportToEXCEL() { exportToEXCEL(); }
protected:
	CLogSalesReportView();
	virtual ~CLogSalesReportView();

	void LoadReportState(void);
	void SaveReportState(void);

public:

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_VIRTUAL(CLogSalesReportView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	//{{AFX_MSG(CMDIDBFormFrame)
	void OnDestroy();
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
  afx_msg void OnSetFocus(CWnd*);

	afx_msg void OnReportItemValueClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnPrintPreview();
	afx_msg void OnFilePrint();
	afx_msg void OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView);
	
	afx_msg void OnFieldSelection(void);

	//}}AFX_MSG


	DECLARE_MESSAGE_MAP()
};
