#ifndef _LOGSCALECONTRACTPARSER_H_
#define _LOGSCALECONTRACTPARSER_H_

#include "pad_transaction_classes.h"

class LogScaleContractParser
{
	MSXML2::IXMLDOMDocumentPtr pDomDoc;
protected:
	CString getAttrText(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
	double getAttrDouble(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
	int getAttrInt(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
	BOOL getAttrBool(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
public:
	LogScaleContractParser(void);

	virtual ~LogScaleContractParser();

	BOOL LoadFromFile(LPCTSTR);
	BOOL LoadFromBuffer(LPCTSTR);
	BOOL SaveToFile(LPCTSTR);

	BOOL isALogScaleContract();

	BOOL getHeaderName(LPTSTR);
	BOOL getHeaderDoneBy(LPTSTR);
	BOOL getHeaderDate(LPTSTR);
	BOOL getHeaderMode(int *);
	BOOL getHeaderTrimFt(double *);
	BOOL getHeaderTrimCM(double *);
	BOOL getHeaderNotes(LPTSTR);
	BOOL getHeaderPrlName(LPTSTR);
	BOOL getHeaderDefSpcCode(LPTSTR);
	BOOL getHeaderDefGrdCode(LPTSTR);

	BOOL getDataFile(LPTSTR);

	long getDataFileSize();

	BOOL getXML(CString &xml);
};


#endif