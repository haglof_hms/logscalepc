// RegisterFormView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "RegisterDlg.h"

#include "reportclasses.h"

#include "ResLangFileReader.h"
// CRegisterDlg

IMPLEMENT_DYNCREATE(CRegisterDlg, CXTResizeDialog)

BEGIN_MESSAGE_MAP(CRegisterDlg, CXTResizeDialog)
	ON_WM_DESTROY()
	ON_WM_COPYDATA()

	ON_CBN_SELCHANGE(IDC_COMBO4_1, OnCbnSelchangeCombo41)
	ON_CBN_DROPDOWN(IDC_COMBO4_1, OnCbnDropdownCombo41)

	ON_BN_CLICKED(IDC_BUTTON1, OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, OnBnClickedButton4)
END_MESSAGE_MAP()

CRegisterDlg::CRegisterDlg()
	: CXTResizeDialog(CRegisterDlg::IDD),
		m_bInitialized(FALSE),
		m_sLangFN(L""),
		m_pDB(NULL),
		m_nSelectedType(-1)

{
}

CRegisterDlg::~CRegisterDlg()
{
}

void CRegisterDlg::OnDestroy()
{
	SaveReportState(m_repRegister);

	CXTResizeDialog::OnDestroy();
}


BOOL CRegisterDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeDialog::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CRegisterDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_COMBO4_1, m_cboxRegisterTypes);
	DDX_Control(pDX, IDC_LBL4_1, m_lbl4_1);
	DDX_Control(pDX, IDC_BUTTON1, m_btnOK);
	DDX_Control(pDX, IDC_BUTTON2, m_btnCancel);
	DDX_Control(pDX, IDC_BUTTON3, m_btnAddItem);
	DDX_Control(pDX, IDC_BUTTON4, m_btnDelItem);

	//}}AFX_DATA_MAP

}

// CRegisterDlg diagnostics

#ifdef _DEBUG
void CRegisterDlg::AssertValid() const
{
	CXTResizeDialog::AssertValid();
}

#ifndef _WIN32_WCE
void CRegisterDlg::Dump(CDumpContext& dc) const
{
	CXTResizeDialog::Dump(dc);
}
#endif
#endif //_DEBUG

// CRegisterDlg message handlers
BOOL CRegisterDlg::OnInitDialog()
{
	CXTResizeDialog::OnInitDialog();
	if (!m_bInitialized)
	{

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		m_lbl4_1.SetLblFontEx(14,FW_NORMAL);
		m_cboxRegisterTypes.SetLblFont(16,FW_BOLD);
		m_cboxRegisterTypes.SetEnabledColor(BLUE,WHITE);

		m_nSelectedType = -1;

		setupReport();

		LoadReportState(m_repRegister);

		m_bInitialized = TRUE;
	}

	return TRUE;
}

BOOL CRegisterDlg::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{

	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		if (m_dbConnectionData.conn->isConnected())
		{
			m_pDB = new CDBHandling(m_dbConnectionData);
		}
	}

	return CXTResizeDialog::OnCopyData(pWnd, pData);
}

void CRegisterDlg::setupReport()
{
	CXTPReportColumn *pCol = NULL;

	if (m_repRegister.GetSafeHwnd() == 0)
	{

		// Create the sheet1 list box.
		if (!m_repRegister.Create(this, ID_REPORT_REGISTER, L"Register_report",TRUE,FALSE))
		{
			TRACE0( "Failed to create m_repRegister.\n" );
			return;
		}
	}

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{

			// Set dialog caption
			SetWindowText(xml.str(IDS_STRING6103));

			m_btnOK.SetWindowText(xml.str(IDS_STRING100));
			m_btnCancel.SetWindowText(xml.str(IDS_STRING101));
			m_btnAddItem.SetWindowText(xml.str(IDS_STRING103));
			m_btnDelItem.SetWindowText(xml.str(IDS_STRING104));
			m_btnAddItem.EnableWindow(FALSE);
			m_btnDelItem.EnableWindow(FALSE);

			m_lbl4_1.SetWindowText(xml.str(IDS_STRING1500));

			m_sMsgCap = xml.str(IDS_STRING99); 
			m_sMsgDelete1 = xml.str(IDS_STRING1553);
			m_sMsgDelete2 = xml.str(IDS_STRING1554);
			m_sMsgMandatoryDataMissing = xml.str(IDS_STRING1520) + L"\n\n" + xml.str(IDS_STRING1521);
			

			m_repRegister.ShowWindow( SW_NORMAL );

			// Name/Type
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_0, xml.str(IDS_STRING1501), 200));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;

			// Abbrevation
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_1, xml.str(IDS_STRING1502), 80));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;
			pCol->GetEditOptions()->m_nMaxLength = 5;	// Abbrevation max 5 characters

			// Other info
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_2, xml.str(IDS_STRING1503), 150));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;

			// Address
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_3, xml.str(IDS_STRING1504), 150));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;

			// Address2
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_4, xml.str(IDS_STRING1505), 150));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;

			// ZipCode
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_5, xml.str(IDS_STRING1506), 50));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;

			// City
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_6, xml.str(IDS_STRING1507), 80));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;

			// Phone
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_7, xml.str(IDS_STRING1508), 100));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;

			// Cellphone
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_8, xml.str(IDS_STRING1509), 120));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;

			// WWW
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_9, xml.str(IDS_STRING1510), 150));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;

			m_repRegister.GetReportHeader()->AllowColumnRemove(FALSE);
			m_repRegister.SetMultipleSelection( FALSE );
			m_repRegister.SetGridStyle( TRUE, xtpReportGridSolid );
			m_repRegister.SetGridStyle( FALSE, xtpReportGridSmallDots );
			m_repRegister.FocusSubItems(TRUE);
			m_repRegister.AllowEdit(TRUE);
			m_repRegister.GetPaintManager()->SetFixedRowHeight(FALSE);

			CString sListOfRegisterItems = xml.str(IDS_STRING200);
			CStringArray sarrTokens;
			tokenizeString(sListOfRegisterItems,';',sarrTokens);
			m_cboxRegisterTypes.Clear();
			for (int i = 0;i < sarrTokens.GetCount();i++)
			{
				
				m_cboxRegisterTypes.AddString(sarrTokens.GetAt(i));

			}

			if (m_cboxRegisterTypes.GetCount() > 0)
			{
				m_cboxRegisterTypes.SetCurSel(0);
				OnCbnSelchangeCombo41();
			}

			RECT rect;
			GetClientRect(&rect);
			setResize(&m_repRegister,4,50,rect.right-8,rect.bottom-90);

			xml.clean();
		}
	}
}

void CRegisterDlg::populateReport()
{
	m_repRegister.ResetContent();
	if (m_pDB == NULL) return;
	m_pDB->getRegister(m_vecRegister,m_nSelectedType);

	if (m_vecRegister.size() > 0)
	{

		for (UINT i = 0;i < m_vecRegister.size();i++)
		{
			m_repRegister.AddRecord(new CRegisterReportRec(m_vecRegister[i]));
		}
		m_repRegister.Populate();
		m_repRegister.UpdateWindow();
	}
}

void CRegisterDlg::OnCbnSelchangeCombo41()
{
	if (Save())
	{
		int nIdx = m_cboxRegisterTypes.GetCurSel();
		if (nIdx != CB_ERR && m_pDB != NULL)
		{
			m_nSelectedType = nIdx+1;
			m_btnAddItem.EnableWindow(TRUE);
			m_btnDelItem.EnableWindow(TRUE);

			populateReport();
		}
		else
		{
			m_nSelectedType = -1;
		}
	}
}

void CRegisterDlg::OnCbnDropdownCombo41()
{
	// Save data if combobox is dropped down
	Save();
}

void CRegisterDlg::Add()
{
	// Add a record to list
	m_repRegister.AddRecord(new CRegisterReportRec());
	m_repRegister.Populate();
	m_repRegister.UpdateWindow();
}

void CRegisterDlg::Delete()
{
	CString sMsg = L"";
	sMsg.Format(m_sMsgDelete1,m_cboxRegisterTypes.getText());
	// Ask user twice, if he realy want's to delete
	if (::MessageBox(GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
	{
		return;
	}
	else	if (::MessageBox(GetSafeHwnd(),m_sMsgDelete2,m_sMsgCap,MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
	{
		return;
	}

	// Save before deleting any data
	Save();

	CXTPReportRow *pRow = m_repRegister.GetFocusedRow();
	CRegisterReportRec *rec = NULL;
	// We'll also add this new item to DB
	if (m_pDB != NULL && pRow != NULL)
	{
		rec = (CRegisterReportRec*)pRow->GetRecord();
		if (m_pDB->delRegisterItem(rec->getRecord()))
		{
			populateReport();
		}
	}
}

BOOL CRegisterDlg::Save()
{
	// We'll save all items for this type
	BOOL bMandatoryDataMissing = FALSE;
	CXTPReportRecords *pRecs = m_repRegister.GetRecords();
	CRegisterReportRec *rec = NULL;
	if (m_pDB != NULL && pRecs != NULL)
	{
		m_repRegister.Populate();
		// We'll check that user entered mandatory data
		for (int i = 0;i < pRecs->GetCount();i++)
		{
			rec = (CRegisterReportRec*)pRecs->GetAt(i);
			if (rec->getColText(COLUMN_0).IsEmpty() && rec->getColText(COLUMN_1).IsEmpty())
			{
				return TRUE;
			}
			else if (rec->getColText(COLUMN_0).IsEmpty() ||
					rec->getColText(COLUMN_1).IsEmpty())
			{
				::MessageBox(GetSafeHwnd(),m_sMsgMandatoryDataMissing,m_sMsgCap,MB_ICONASTERISK | MB_OK);
				bMandatoryDataMissing = TRUE;
				break;
			}
		}
		if (!bMandatoryDataMissing)
		{

			for (int i = 0;i < pRecs->GetCount();i++)
			{
				rec = (CRegisterReportRec*)pRecs->GetAt(i);
				if (rec->getRecord().getID() == -1)
				{
					m_pDB->newRegisterItem(CRegister(-1,m_nSelectedType,
																				rec->getColText(COLUMN_0),
																				rec->getColText(COLUMN_1),
																				rec->getColText(COLUMN_2),
																				rec->getColText(COLUMN_3),
																				rec->getColText(COLUMN_4),
																				rec->getColText(COLUMN_5),
																				rec->getColText(COLUMN_6),
																				rec->getColText(COLUMN_7),
																				rec->getColText(COLUMN_8),
																				rec->getColText(COLUMN_9),
																				rec->getColText(COLUMN_10)));
				}
				else if (rec->getRecord().getID() > 0)
				{
					m_pDB->updRegisterItem(CRegister(rec->getRecord().getPKID(),
																		 m_nSelectedType,
																		 rec->getColText(COLUMN_0),
																		 rec->getColText(COLUMN_1),
  																	 rec->getColText(COLUMN_2),
 	 																	 rec->getColText(COLUMN_3),
																		 rec->getColText(COLUMN_4),
																		 rec->getColText(COLUMN_5),
																		 rec->getColText(COLUMN_6),
																		 rec->getColText(COLUMN_7),
																		 rec->getColText(COLUMN_8),
																		 rec->getColText(COLUMN_9),
																		 rec->getColText(COLUMN_10)));
				}
			}
			populateReport();
			return TRUE;
		}
		else
			return FALSE;
	}

	return FALSE;
}


void CRegisterDlg::OnBnClickedButton1()
{
	if (Save())
	{
		OnOK();
	}
}

void CRegisterDlg::OnBnClickedButton2()
{
	OnCancel();
}
// Add item
void CRegisterDlg::OnBnClickedButton3()
{
	Add();
}
// Delete item
void CRegisterDlg::OnBnClickedButton4()
{
	Delete();
}
