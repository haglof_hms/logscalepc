#pragma once

#include "Resource.h"

// CChangeStatusDlg dialog

class CChangeStatusDlg : public CXTResizeDialog
{
	DECLARE_DYNAMIC(CChangeStatusDlg)
	// Data members
	BOOL m_bInitialized;
	CString m_sLangFN;


	CMyExtStatic m_lbl15_1;
	CMyExtStatic m_lbl15_2;

	CComboBox m_cbox15_1;

	CButton m_btnOK;
	CButton m_btnCancel;

	CStringArray m_sarrStatus;

	LOGSTATUS::STATUS m_logStatus;
public:
	CChangeStatusDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CChangeStatusDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG15 };

	LOGSTATUS::STATUS getStatus();

protected:
	//{{AFX_VIRTUAL(CDefaultsDlg)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnCbnSelchangeCombo151();
};
