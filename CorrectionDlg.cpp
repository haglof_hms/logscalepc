// CorrectionDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CorrectionDlg.h"
#include "fileparser.h"
#include "SetupSpcQualDlg.h"

#include "ResLangFileReader.h"

// CCorrectionDlg dialog

IMPLEMENT_DYNAMIC(CCorrectionDlg, CXTResizeDialog)

BEGIN_MESSAGE_MAP(CCorrectionDlg, CXTResizeDialog)
	ON_BN_CLICKED(IDOK, &CCorrectionDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON6_1, &CCorrectionDlg::OnBnClickedButton61)
	ON_CBN_SELCHANGE(IDC_COMBO6_12, &CCorrectionDlg::OnCbnSelchangeCombo612)
	ON_BN_CLICKED(IDC_BUTTON6_2, &CCorrectionDlg::OnBnClickedButton62)
	ON_BN_CLICKED(IDC_BUTTON6_3, &CCorrectionDlg::OnBnClickedButton63)
	ON_EN_CHANGE(IDC_EDIT6_14, &CCorrectionDlg::OnEnChangeEdit614)
END_MESSAGE_MAP()


CCorrectionDlg::CCorrectionDlg(CWnd* pParent /*=NULL*/,short open_as)
	: CXTResizeDialog(CCorrectionDlg::IDD, pParent),
		m_bInitialized(FALSE),
		m_sLangFN(L""),
		m_nOpenAs(open_as)
{

}

CCorrectionDlg::~CCorrectionDlg()
{
	if (m_pFont1 != NULL)
		delete m_pFont1;
}

void CCorrectionDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCorrectionDlg)

	DDX_Control(pDX, IDC_GROUP6_1, m_wndGrp6_1);
	DDX_Control(pDX, IDC_GROUP6_2, m_wndGrp6_2);
	DDX_Control(pDX, IDC_GROUP6_3, m_wndGrp6_3);

	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_BUTTON6_1, m_btnSetup);
	DDX_Control(pDX, IDC_BUTTON6_2, m_btnUseTicketNoFromFile);
	DDX_Control(pDX, IDC_BUTTON6_3, m_btnGenerateTicketNo);

	DDX_Control(pDX, IDC_LBL6_1, m_lbl6_1);

	DDX_Control(pDX, IDC_LBL6_4, m_lbl6_4);
	DDX_Control(pDX, IDC_LBL6_5, m_lbl6_5);
	DDX_Control(pDX, IDC_LBL6_6, m_lbl6_6);
	DDX_Control(pDX, IDC_LBL6_7, m_lbl6_7);
	DDX_Control(pDX, IDC_LBL6_8, m_lbl6_8);
	DDX_Control(pDX, IDC_LBL6_9, m_lbl6_9);
	DDX_Control(pDX, IDC_LBL6_10, m_lbl6_10);
	DDX_Control(pDX, IDC_LBL6_11, m_lbl6_11);
	DDX_Control(pDX, IDC_LBL6_12, m_lbl6_12);
	DDX_Control(pDX, IDC_LBL6_13, m_lbl6_13);
	DDX_Control(pDX, IDC_LBL6_14, m_lbl6_14);
	DDX_Control(pDX, IDC_LBL6_15, m_lbl6_15);
	DDX_Control(pDX, IDC_LBL6_16, m_lbl6_16);
	DDX_Control(pDX, IDC_LBL6_17, m_lbl6_17);
	DDX_Control(pDX, IDC_LBL6_18, m_lbl6_18);
	DDX_Control(pDX, IDC_LBL6_19, m_lbl6_19);
	DDX_Control(pDX, IDC_LBL6_32, m_lbl6_32);
	DDX_Control(pDX, IDC_LBL6_33, m_lbl6_33);
	
	DDX_Control(pDX, IDC_COMBO6_2, m_cbx6_1);
	DDX_Control(pDX, IDC_COMBO6_3, m_cbx6_2);
	DDX_Control(pDX, IDC_COMBO6_4, m_cbx6_3);
	DDX_Control(pDX, IDC_COMBO6_5, m_cbx6_4);
	DDX_Control(pDX, IDC_COMBO6_6, m_cbx6_5);
	DDX_Control(pDX, IDC_COMBO6_7, m_cbx6_6);
	DDX_Control(pDX, IDC_COMBO6_8, m_cbx6_7);
	DDX_Control(pDX, IDC_COMBO6_9, m_cbx6_9);
	DDX_Control(pDX, IDC_COMBO6_10, m_cbx6_10);
	DDX_Control(pDX, IDC_COMBO6_11, m_cbx6_11);
	DDX_Control(pDX, IDC_COMBO6_12, m_cbx6_12);

	DDX_Control(pDX, IDC_EDIT6_1, m_ed6_1);
	DDX_Control(pDX, IDC_EDIT6_2, m_ed6_2);
	DDX_Control(pDX, IDC_EDIT6_3, m_ed6_3);
	DDX_Control(pDX, IDC_EDIT6_4, m_ed6_4);
	DDX_Control(pDX, IDC_EDIT6_5, m_ed6_5);
	DDX_Control(pDX, IDC_EDIT6_6, m_ed6_6);
	DDX_Control(pDX, IDC_EDIT6_7, m_ed6_7);
	DDX_Control(pDX, IDC_EDIT6_8, m_ed6_8);
	DDX_Control(pDX, IDC_EDIT6_9, m_ed6_9);
	DDX_Control(pDX, IDC_EDIT6_10, m_ed6_10);
	DDX_Control(pDX, IDC_EDIT6_11, m_ed6_11);
	DDX_Control(pDX, IDC_EDIT6_12, m_ed6_12);
	DDX_Control(pDX, IDC_EDIT6_13, m_ed6_13);
	DDX_Control(pDX, IDC_EDIT6_14, m_ed6_14);

	DDX_Control(pDX, IDC_CHECK6_1, m_cb6_1);
	DDX_Control(pDX, IDC_CHECK6_2, m_cb6_2);
	DDX_Control(pDX, IDC_CHECK6_3, m_cb6_3);
	DDX_Control(pDX, IDC_CHECK6_4, m_cb6_4);
	DDX_Control(pDX, IDC_CHECK6_5, m_cb6_5);
	DDX_Control(pDX, IDC_CHECK6_6, m_cb6_6);
	DDX_Control(pDX, IDC_CHECK6_7, m_cb6_7);
	DDX_Control(pDX, IDC_CHECK6_8, m_cb6_8);

	//}}AFX_DATA_MAP
}

BOOL CCorrectionDlg::OnInitDialog()
{
	CXTResizeDialog::OnInitDialog();
	if (!m_bInitialized)
	{
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);


		m_dwButtonFlat = BS_XT_FLAT;
		m_dwButtonNormal = BS_XT_SEMIFLAT;

		m_btnGenerateTicketNo.SetXButtonStyle(m_dwButtonNormal);
		m_btnUseTicketNoFromFile.SetXButtonStyle(m_dwButtonNormal);

		m_lbl6_1.SetBkColor(INFOBK);
		m_lbl6_1.SetTextColor(BLUE);
		m_lbl6_17.SetLblFontEx(16,FW_BOLD);

		m_pFont1 = new CFont();
		LOGFONT lf;
		memset(&lf,0,sizeof(LOGFONT));
		lf.lfHeight = 16;
		lf.lfWeight = FW_BOLD;
		m_pFont1->CreateFontIndirect(&lf);

		m_wndGrp6_2.SetFont(m_pFont1);
		m_wndGrp6_3.SetFont(m_pFont1);

		m_ed6_1.SetDisabledColor(BLACK,INFOBK);
		m_ed6_1.SetReadOnly();
		m_ed6_2.SetDisabledColor(BLACK,INFOBK);
		m_ed6_2.SetReadOnly();
		m_ed6_3.SetDisabledColor(BLACK,INFOBK);
		m_ed6_3.SetReadOnly();
		m_ed6_4.SetDisabledColor(BLACK,INFOBK);
		m_ed6_4.SetReadOnly();
		m_ed6_5.SetDisabledColor(BLACK,INFOBK);
		m_ed6_5.SetReadOnly();
		m_ed6_6.SetDisabledColor(BLACK,INFOBK);
		m_ed6_6.SetReadOnly();
		m_ed6_7.SetDisabledColor(BLACK,INFOBK);
		m_ed6_7.SetReadOnly();
		m_ed6_8.SetDisabledColor(BLACK,INFOBK);
		m_ed6_8.SetReadOnly();
		m_ed6_9.SetDisabledColor(BLACK,INFOBK);
		m_ed6_9.SetReadOnly();
		m_ed6_10.SetDisabledColor(BLACK,INFOBK);
		m_ed6_10.SetReadOnly();
		m_ed6_11.SetDisabledColor(BLACK,INFOBK);
		m_ed6_11.SetReadOnly();
		m_ed6_13.SetDisabledColor(BLACK,INFOBK);
		m_ed6_13.SetReadOnly();

		m_ed6_11.SetAsNumeric();
		m_ed6_11.ModifyStyle(0,ES_RIGHT);
		m_ed6_12.SetAsNumeric();
		m_ed6_12.ModifyStyle(0,ES_RIGHT);


		setupReport();

		addCBoxData();

		populateReport();

		OnCbnSelchangeCombo612();

		if (m_nOpenAs == 0)
		{
			if (m_ed6_13.getText().IsEmpty())
			{
				m_btnGenerateTicketNo.SetStateX(TRUE);
				m_btnUseTicketNoFromFile.SetStateX(FALSE);
				m_xtButtonPressed = 1;	// m_btnGenerateTicketNo

			}
			else if (!m_ed6_13.getText().IsEmpty())
			{
				m_btnUseTicketNoFromFile.SetStateX(TRUE);
				m_btnGenerateTicketNo.SetStateX(FALSE);
				m_xtButtonPressed = 0;	// m_btnUseTicketNoFromFile
			}
		}
		else
		{
			m_ed6_14.SetDisabledColor(BLACK,GRAY);
			m_ed6_14.SetReadOnly();
			m_ed6_14.EnableWindow(FALSE);		
			m_btnGenerateTicketNo.SetStateX(FALSE);
			m_btnGenerateTicketNo.EnableWindow(FALSE);
			m_btnUseTicketNoFromFile.SetStateX(TRUE);
			m_xtButtonPressed = 0;	// m_btnGenerateTicketNo
		}


		m_bInitialized = TRUE;
	}

	return TRUE;
}

// CCorrectionDlg message handlers
// Create and add Assortment settings reportwindow
void CCorrectionDlg::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;
	RECT rect;
	GetClientRect(&rect);
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{

			// Set dialog caption
			SetWindowText(xml.str(IDS_STRING6108));

			m_lbl6_1.SetWindowTextW(xml.str(IDS_STRING1800)+ L" " + xml.str(IDS_STRING1801) + L" " + xml.str(IDS_STRING1802) + L" " + xml.str(IDS_STRING1804));
			m_wndGrp6_2.SetWindowTextW(xml.str(IDS_STRING1820));
			m_wndGrp6_3.SetWindowTextW(xml.str(IDS_STRING1821));

			m_lbl6_4.SetWindowTextW(xml.str(IDS_STRING1830));
			m_lbl6_5.SetWindowTextW(xml.str(IDS_STRING1831));
			m_lbl6_6.SetWindowTextW(xml.str(IDS_STRING1832));
			m_lbl6_7.SetWindowTextW(xml.str(IDS_STRING1833));
			m_lbl6_8.SetWindowTextW(xml.str(IDS_STRING1834));
			m_lbl6_9.SetWindowTextW(xml.str(IDS_STRING1835));
			m_lbl6_10.SetWindowTextW(xml.str(IDS_STRING1836));
			m_lbl6_11.SetWindowTextW(xml.str(IDS_STRING1837));
			m_lbl6_12.SetWindowTextW(xml.str(IDS_STRING1838));
			m_lbl6_13.SetWindowTextW(xml.str(IDS_STRING1840));
			m_lbl6_14.SetWindowTextW(xml.str(IDS_STRING1829));
			m_lbl6_15.SetWindowTextW(xml.str(IDS_STRING1814));
			m_lbl6_16.SetWindowTextW(xml.str(IDS_STRING1815));
			m_lbl6_17.SetWindowTextW(L"");
			m_lbl6_18.SetWindowTextW(xml.str(IDS_STRING1839));
			m_lbl6_19.SetWindowTextW(xml.str(IDS_STRING1841));
			m_lbl6_32.SetWindowTextW(xml.str(IDS_STRING18430));
			m_btnUseTicketNoFromFile.SetWindowTextW(xml.str(IDS_STRING18431));
			m_lbl6_33.SetWindowTextW(xml.str(IDS_STRING18432));
			m_btnGenerateTicketNo.SetWindowTextW(xml.str(IDS_STRING18433));

			m_btnOK.SetWindowText(xml.str(IDS_STRING105));
			m_btnCancel.SetWindowText(xml.str(IDS_STRING101));
			m_btnSetup.SetWindowText(xml.str(IDS_STRING1842));

			m_sMsgCap = xml.str(IDS_STRING99);
			m_sMsgMeasModeError.Format(L"%s\n%s\n\n",xml.str(IDS_STRING18031),xml.str(IDS_STRING18032));
			m_sMsgSpeciesError.Format(L"%s\n%s\n\n",xml.str(IDS_STRING18033),xml.str(IDS_STRING18034));

			m_sMsgFixData.Format(L"%s\n\n* %s\n\n* %s\n\n%s\n\n",
				xml.str(IDS_STRING1800),
				xml.str(IDS_STRING1801),
				xml.str(IDS_STRING1802),
				xml.str(IDS_STRING18030));

			m_sMsgTicketNumExists = xml.str(IDS_STRING1118);

			m_sLogsOK = xml.str(IDS_STRING1816);
			m_sLogsCorrection = xml.str(IDS_STRING1817);

			m_sMsgDataEnterError.Format(L"%s\n%s\n%s\n\n",
				xml.str(IDS_STRING1805),
				xml.str(IDS_STRING1806),
				xml.str(IDS_STRING1807));

			m_sMsgTicketNumberMissing = xml.str(IDS_STRING18440);

			m_sMsgIllegalChars = xml.str(IDS_STRING18441);

			tokenizeString(xml.str(IDS_STRING201),';',m_sarrLoadType);
			tokenizeString(xml.str(IDS_STRING202),';',m_sarrDeductionType);
			// Strings

			xml.clean();
			
		}
	}
}

void CCorrectionDlg::addCBoxData()
{
	CRegister rec = CRegister();
	if (m_vecRegister.size() > 0)
	{
		m_cbx6_1.AddString(L"");
		m_cbx6_2.AddString(L"");
		m_cbx6_3.AddString(L"");
		m_cbx6_4.AddString(L"");
		m_cbx6_5.AddString(L"");
		m_cbx6_6.AddString(L"");
		m_cbx6_10.AddString(L"");
		m_cbx6_11.AddString(L"");

		for (UINT i = 0;i < m_vecRegister.size();i++)
		{
			rec = m_vecRegister[i];

			switch (rec.getTypeID())
			{
				case REGISTER_TYPES::SOURCEID : m_cbx6_1.AddString(rec.getName()); break;
				case REGISTER_TYPES::SCALER : m_cbx6_2.AddString(rec.getName()); break;
				case REGISTER_TYPES::HAULER : m_cbx6_3.AddString(rec.getName()); break;
				case REGISTER_TYPES::BUYER : m_cbx6_4.AddString(rec.getName()); break;
				case REGISTER_TYPES::LOCATION : m_cbx6_5.AddString(rec.getName()); break;
				case REGISTER_TYPES::TRACTID : m_cbx6_6.AddString(rec.getName()); break;
				case REGISTER_TYPES::SUPPLIER : m_cbx6_10.AddString(rec.getName()); break;
				case REGISTER_TYPES::VENDOR : m_cbx6_11.AddString(rec.getName()); break;
			};
		}
	}	// if (m_vecRegister.size() > 0)

	if (m_sarrLoadType.GetCount() > 0)
	{
		for (short i = 0;i < m_sarrLoadType.GetCount();i++)
		{
			m_cbx6_7.AddString(m_sarrLoadType.GetAt(i));
		}
	}

	if (m_sarrDeductionType.GetCount() > 0)
	{
		for (short i = 0;i < m_sarrDeductionType.GetCount();i++)
		{
			m_cbx6_9.AddString(m_sarrDeductionType.GetAt(i));
		}
	}

	if (m_vecLogScaleTemplates.size() > 0)
	{
		for (UINT i = 0;i < m_vecLogScaleTemplates.size();i++)
		{
			m_cbx6_12.AddString(m_vecLogScaleTemplates[i].getTmplName());
			m_cbx6_12.SetItemData(i,(DWORD)m_vecLogScaleTemplates[i].getTmplID());
		}
		//--------------------------------------------------------------------------------------------------------
		// Template
		int nIndex = m_cbx6_12.FindStringExact(0,m_recTicket.getTmplUsed()) ;
		m_cbx6_12.SetCurSel(nIndex);

	}
}

void CCorrectionDlg::populateReport()
{
	int nCounter = 0;
	CString sStr = L"";
	int nIndex = CB_ERR;
	CLogs rec = CLogs();
	
	//--------------------------------------------------------------------------------------------------------
	// Template
	//nIndex = m_cbx6_12.FindStringExact(0,m_recTicket.getTmplUsed()) ;
	//m_cbx6_12.SetCurSel(nIndex);

	//--------------------------------------------------------------------------------------------------------
	// Add ticket data

	// Ticketnumber
	if (!m_recTicket.getTicket().IsEmpty())
	{
		m_ed6_13.SetWindowTextW(m_recTicket.getTicket().Trim());
		m_ed6_13.SetDisabledColor(BLACK,INFOBK);
		m_btnUseTicketNoFromFile.EnableWindow(TRUE);
	}
	else
	{
		m_ed6_13.SetDisabledColor(BLACK,GRAY);
		m_btnUseTicketNoFromFile.EnableWindow(FALSE);
		m_ed6_14.SetWindowTextW(m_sGeneratedTicketNum);
	}

	//--------------------------------------------------------------------------------------------------------
	// SourceID
	m_ed6_1.SetWindowTextW(m_recTicket.getSourceID().Trim());
	m_cb6_1.EnableWindow(!m_recTicket.getSourceID().Trim().IsEmpty());
	if ((nIndex = m_cbx6_1.FindStringExact(0,m_ed6_1.getText())) == CB_ERR) m_ed6_1.SetDisabledColor(BLACK,RED);
	else 
	{
		m_cbx6_1.SetCurSel(nIndex);
		m_cb6_1.EnableWindow(FALSE);
	}
	//--------------------------------------------------------------------------------------------------------
	// Scaler
	m_ed6_2.SetWindowTextW(m_recTicket.getScaler().Trim());
	m_cb6_2.EnableWindow(!m_recTicket.getScaler().Trim().IsEmpty());
	if ((nIndex = m_cbx6_2.FindStringExact(0,m_ed6_2.getText())) == CB_ERR) m_ed6_2.SetDisabledColor(BLACK,RED);
	else 
	{
		m_cbx6_2.SetCurSel(nIndex);
		m_cb6_2.EnableWindow(FALSE);
	}
	//--------------------------------------------------------------------------------------------------------
	// Hauler
	m_ed6_3.SetWindowTextW(m_recTicket.getHauler().Trim());
	m_cb6_3.EnableWindow(!m_recTicket.getHauler().Trim().IsEmpty());
	if ((nIndex = m_cbx6_3.FindStringExact(0,m_ed6_3.getText())) == CB_ERR) m_ed6_3.SetDisabledColor(BLACK,RED);
	else 
	{
		m_cbx6_3.SetCurSel(nIndex);
		m_cb6_3.EnableWindow(FALSE);
	}
	//--------------------------------------------------------------------------------------------------------
	// Buyer
	m_ed6_4.SetWindowTextW(m_recTicket.getBuyer().Trim());
	m_cb6_4.EnableWindow(!m_recTicket.getBuyer().Trim().IsEmpty());
	if ((nIndex = m_cbx6_4.FindStringExact(0,m_ed6_4.getText())) == CB_ERR) m_ed6_4.SetDisabledColor(BLACK,RED);
	else 
	{
		m_cbx6_4.SetCurSel(nIndex);
		m_cb6_4.EnableWindow(FALSE);
	}
	//--------------------------------------------------------------------------------------------------------
	// Location
	m_ed6_5.SetWindowTextW(m_recTicket.getLocation().Trim());
	m_cb6_5.EnableWindow(!m_recTicket.getLocation().Trim().IsEmpty());
	if ((nIndex = m_cbx6_5.FindStringExact(0,m_ed6_5.getText())) == CB_ERR) m_ed6_5.SetDisabledColor(BLACK,RED);
	else 
	{
		m_cbx6_5.SetCurSel(nIndex);
		m_cb6_5.EnableWindow(FALSE);
	}
	//--------------------------------------------------------------------------------------------------------
	// TractID
	m_ed6_6.SetWindowTextW(m_recTicket.getTractID().Trim());
	m_cb6_6.EnableWindow(!m_recTicket.getTractID().Trim().IsEmpty());
	if ((nIndex = m_cbx6_6.FindStringExact(0,m_ed6_6.getText())) == CB_ERR) m_ed6_6.SetDisabledColor(BLACK,RED);
	else 
	{
		m_cbx6_6.SetCurSel(nIndex);
		m_cb6_6.EnableWindow(FALSE);
	}
	//--------------------------------------------------------------------------------------------------------
	// Supplier
	m_ed6_9.SetWindowTextW(m_recTicket.getSupplier());
	m_cb6_7.EnableWindow(!m_recTicket.getSupplier().Trim().IsEmpty());
	if ((nIndex = m_cbx6_10.FindStringExact(0,m_ed6_9.getText())) == CB_ERR) m_ed6_9.SetDisabledColor(BLACK,RED);
	else 
	{
		m_cbx6_10.SetCurSel(nIndex);
		m_cb6_7.EnableWindow(FALSE);
	}
	//--------------------------------------------------------------------------------------------------------
	// Vendor
	m_ed6_10.SetWindowTextW(m_recTicket.getVendor());
	m_cb6_8.EnableWindow(!m_recTicket.getVendor().Trim().IsEmpty());
	if ((nIndex = m_cbx6_11.FindStringExact(0,m_ed6_10.getText())) == CB_ERR) m_ed6_10.SetDisabledColor(BLACK,RED);
	else 
	{
		m_cbx6_11.SetCurSel(nIndex);
		m_cb6_8.EnableWindow(FALSE);
	}

	// LoadType
	m_ed6_7.SetWindowTextW(m_recTicket.getLoadType().Trim());
	if ((nIndex = m_cbx6_7.FindStringExact(0,m_ed6_7.getText())) == CB_ERR) m_ed6_7.SetDisabledColor(BLACK,RED);
	else m_cbx6_7.SetCurSel(nIndex);
	// Reduktion
	m_ed6_8.SetWindowTextW(m_recTicket.getDeductionType().Trim());
	if ((nIndex = m_cbx6_9.FindStringExact(0,m_ed6_8.getText())) == CB_ERR) m_ed6_8.SetDisabledColor(BLACK,RED);
	else m_cbx6_9.SetCurSel(nIndex);

	// IB taper (formtal)
	m_ed6_11.setFloat(m_recTicket.getIBTaper(),3);
	if (m_recTicket.getIBTaper() > 0.0)
		m_ed6_12.setFloat(m_recTicket.getIBTaper(),3);
	else
	{
		m_ed6_11.SetDisabledColor(BLACK,RED);
	}

	if (nCounter > 0)
	{
		m_btnSetup.EnableWindow(m_cbx6_12.GetCurSel() > CB_ERR);
	}
	else
	{
		m_btnSetup.EnableWindow(FALSE);
	}

}

// Just check that all descepencies have been dealt with
// I.e. no flag is 1,2 or 3
BOOL CCorrectionDlg::checkLogs()
{
	for (UINT i = 0;i < m_vecLogs.size();i++)
	{
		if (m_vecLogs[i].getFlag() > 0) 
			return FALSE;
	}

	return TRUE;
}

BOOL CCorrectionDlg::checkTicketNumAlreadyExists(LPCTSTR ticket_num)
{
	CTickets rec;
	CString sTicketA = L"";
	CString sTicketB = ticket_num;
	if (m_vecTickets.size() > 0)
	{
		for (UINT i = 0;i < m_vecTickets.size();i++)
		{
			rec = m_vecTickets[i];
			sTicketA = rec.getTicket().Trim();
			if (sTicketA.CompareNoCase(sTicketB.Trim()) == 0) // && m_recTicket.getTicket().Trim().CompareNoCase(ticket_num) != 0)
			{
				return TRUE;
			}
		}
	}
	return FALSE;
}

int CCorrectionDlg::getSpcIDFromCode(LPCTSTR code)
{
	if (m_vecSpecies.size() > 0)
	{
		for (UINT i = 0;i < m_vecSpecies.size();i++)
		{
			if (m_vecSpecies[i].getSpcCode().CompareNoCase(code) == 0)
				return m_vecSpecies[i].getSpcID();
		}
	}

	return -1;
}

int CCorrectionDlg::getGradesIDFromCode(LPCTSTR code)
{
	CString sCode(code);
	if (m_vecGrades.size() > 0)
	{
		for (UINT i = 0;i < m_vecGrades.size();i++)
		{
			if (m_vecGrades[i].getGradesCode().CompareNoCase(sCode.Trim()) == 0)
				return m_vecGrades[i].getGradesID();
		}
	}

	return -1;
}

// Check species in file compared to species in
// selected contract (Template)
BOOL CCorrectionDlg::checkSpeciesAndGrades()
{
	CString sTmp = L"";
	CLogs rec = CLogs();
	BOOL bFoundSpecies = FALSE;
	BOOL bFoundGrades = FALSE;
	int nIdx = m_cbx6_12.GetCurSel(),nTemplateID = -1;
	if (nIdx > CB_ERR)
		nTemplateID = (int)m_cbx6_12.GetItemData(nIdx);
	vecPricelistInTicket vec;
	m_recLogScaleTmpl = CLogScaleTemplates();
	// Find Contract (template)
	if (m_vecLogScaleTemplates.size() > 0)
	{
		for (UINT i = 0;i < m_vecLogScaleTemplates.size();i++)
		{
			if (nTemplateID == m_vecLogScaleTemplates[i].getTmplID())
			{
				m_recLogScaleTmpl = m_vecLogScaleTemplates[i];
//				getSpeciesFromTemplate(m_vecLogScaleTemplates[i].getPricelist(),vec);
				getPricelistFromTemplate(m_recLogScaleTmpl.getPricelist(),-1,vec);
				break;
			}
		}	// for (UINT i = 0;i < m_vecLogScaleTemplates.size();i++)
	}

	m_nCounter = 0;
	//-----------------------------------------------------------
	// Add logs
	// Compare Logs in pricelist to Logs in Ticket
	if (m_vecLogs.size() > 0 && vec.size() > 0)
	{
		for (UINT iLog = 0;iLog < m_vecLogs.size();iLog++)
		{
			rec = m_vecLogs[iLog];
			// Reset flag before any check, might have been set
			// on user selecting different contracts.
			m_vecLogs[iLog].setFlag(-1);	

			bFoundSpecies = FALSE;
			bFoundGrades = FALSE;
			for (UINT iPrl = 0;iPrl < vec.size();iPrl++)
			{
				if (vec[iPrl].getSpeciesID() == getSpcIDFromCode(m_vecLogs[iLog].getSpcCode()))
				{
					bFoundSpecies = TRUE;
					break;
				}

			}	// for (UINT iPrl = 0;iPrl < vec.size();iPrl++)

			for (UINT iPrl = 0;iPrl < vec.size();iPrl++)
			{
				if (vec[iPrl].getGradesID() == getGradesIDFromCode(m_vecLogs[iLog].getGrade()))
				{
					bFoundGrades = TRUE;
					break;
				}

			}	// for (UINT iPrl = 0;iPrl < vec.size();iPrl++)

			if (!bFoundSpecies || !bFoundGrades)
			{
				//-----------------------------------------------------------
				// Add and Count number of discrepancies; species and/or qualities
				if (!bFoundSpecies && bFoundGrades)
				{
					rec.setFlag(1);
					m_vecLogs[iLog].setFlag(1);
				}
				else if (bFoundSpecies && !bFoundGrades)
				{
					rec.setFlag(2);
					m_vecLogs[iLog].setFlag(2);
				}
				else if (!bFoundSpecies && !bFoundGrades)
				{
					rec.setFlag(3);
					m_vecLogs[iLog].setFlag(3);
				}
				m_nCounter++;

			}
		}	// for (UINT iLog = 0;iLog < m_vecLogs.size();iLog++)
	}
	
	if (m_nCounter == 0)
	{
		for (UINT i = 0;i < m_vecLogs.size();i++)
		{
			m_mapUniqueTagsInLogs[m_vecLogs[i].getTagNumber().Trim()] += 1;
		}

		sTmp.Format(m_sLogsOK,m_mapUniqueTagsInLogs.size()); // m_vecLogs.size());
		m_lbl6_17.SetWindowTextW(sTmp);
		m_lbl6_17.SetTextColor(BLACK);
		return TRUE;
	}
	else
	{
		sTmp.Format(m_sLogsCorrection,m_nCounter, m_vecLogs.size());
		m_lbl6_17.SetWindowTextW(sTmp);
		m_lbl6_17.SetTextColor(RED);
		return FALSE;
	}
	
	return TRUE;
}

BOOL CCorrectionDlg::checkMeasuringMode()
{
	CLogScaleTemplates recLogScaleTmpl = CLogScaleTemplates();
	int nIdx = m_cbx6_12.GetCurSel(),nTemplateID = -1;
	if (nIdx > CB_ERR)
		nTemplateID = (int)m_cbx6_12.GetItemData(nIdx);
	vecPricelistInTicket vec;
	// Find Contract (template)
	if (m_vecLogScaleTemplates.size() > 0)
	{
		for (UINT i = 0;i < m_vecLogScaleTemplates.size();i++)
		{
			if (nTemplateID == m_vecLogScaleTemplates[i].getTmplID())
			{
				getSpeciesFromTemplate(m_vecLogScaleTemplates[i].getPricelist(),vec);
				recLogScaleTmpl = m_vecLogScaleTemplates[i];
				break;
			}
		}	// for (UINT i = 0;i < m_vecLogScaleTemplates.size();i++)
	}

	// Check measuringmode; compare selected Contract to Ticket-file
	if (recLogScaleTmpl.getMeasureMode() != m_recTicket.getMeasuringMode())
	{
		return FALSE;
	}

	return TRUE;
}


void CCorrectionDlg::OnBnClickedOk()
{
	CLogs recLog,recLogCorr;
	CString sValue = L""; //,sInFileValue = L"";
	BOOL bTicketNumOK = TRUE;
	BOOL bLoadTypeAndDeduction = FALSE;

	if (m_nOpenAs == 0)
	{
		if (m_xtButtonPressed == 0)
		{
			sValue = m_ed6_13.getText().Trim();
		}
		else if (m_xtButtonPressed == 1)
		{
			sValue = m_ed6_14.getText().Trim();
		}
		else if (m_ed6_13.getText().IsEmpty() && !m_ed6_14.getText().IsEmpty())
		{
			sValue = m_ed6_14.getText().Trim();
		}
	}
	else
	{
		sValue = m_ed6_13.getText().Trim();
	}
/*
	if (!m_ed6_13.getText().Trim().IsEmpty() && m_ed6_14.getText().Trim().IsEmpty())
		sValue = m_ed6_13.getText().Trim();
	else if (!m_ed6_14.getText().Trim().IsEmpty())
		sValue = m_ed6_14.getText().Trim();
*/
/*
	// Collect data from ticket information
	m_ed6_13.GetWindowTextW(sInFileValue);
	m_ed6_14.GetWindowTextW(sValue);
	if (!sInFileValue.IsEmpty())
	{
		sValue = sInFileValue;
	}
*/
	if (sValue.IsEmpty())
	{
		::MessageBox(GetSafeHwnd(),m_sMsgTicketNumberMissing,m_sMsgCap,MB_ICONSTOP | MB_OK);
		return;
	}

	if (checkTicketNumAlreadyExists(sValue) && m_nOpenLogsViewAs != ID_OPEN_LOGS)
	{
		::MessageBox(GetSafeHwnd(),m_sMsgTicketNumExists,m_sMsgCap,MB_ICONSTOP | MB_OK);
		return;
	}

	//#3611, lagt in koll inga ogiltiga tecken i Ticket numret	\ / ? : * " > < |
	if(checkIllegalChars(sValue))
	{
		::MessageBox(GetSafeHwnd(),_T("\\ / ? : * \" > < |")+m_sMsgIllegalChars,m_sMsgCap,MB_ICONSTOP | MB_OK);
		return;
	}

	// Check that mandatory data is entered
	if ((m_cbx6_12.GetCurSel() == CB_ERR) ||
			(m_cbx6_7.GetCurSel() == CB_ERR) ||
			(m_cbx6_9.GetCurSel() == CB_ERR) ||
		 (m_ed6_13.getText().Trim().IsEmpty() && m_ed6_14.getText().Trim().IsEmpty()) ||
		 (m_ed6_11.getFloat() == 0.0 && m_ed6_12.getFloat() == 0.0) ||
		 (m_nCounter > 0) )
	{
		::MessageBox(GetSafeHwnd(),m_sMsgDataEnterError,m_sMsgCap,MB_ICONSTOP | MB_OK);
		return;
	}

	// Passed check of ticketnumber
	m_recTicket.setTicket(sValue);


	if (m_cbx6_12.GetCurSel() > CB_ERR) 
		m_cbx6_12.GetWindowTextW(sValue);
	else
		sValue.Empty();
	m_recTicket.setTmplUsed(sValue);
	// Get tmpl id
	m_recTicket.setTemplateID((int)m_cbx6_12.GetItemData(m_cbx6_12.GetCurSel()));
	// Findout pricelist from template
	if (m_vecLogScaleTemplates.size() > 0)
	{
		for (UINT i = 0;i < m_vecLogScaleTemplates.size();i++)
		{
			if (m_vecLogScaleTemplates[i].getTmplID() == m_recTicket.getTemplateID())
			{
				m_recTicket.setPrlID(m_vecLogScaleTemplates[i].getPrlID());
				m_recTicket.setPrlUsed(m_vecLogScaleTemplates[i].getPrlName());
				m_recTicket.setTrimFT(m_vecLogScaleTemplates[i].getTrimFT());
				m_recTicket.setTrimCM(m_vecLogScaleTemplates[i].getTrimCM());
			}
		}
	}

	if (m_cbx6_1.GetCurSel() > CB_ERR) 
		m_cbx6_1.GetWindowTextW(sValue);
	else 
		sValue = m_ed6_1.getText();
	m_recTicket.setSourceID(sValue);

	if (m_cbx6_2.GetCurSel() > CB_ERR) 
		m_cbx6_2.GetWindowTextW(sValue);
	else 
		sValue = m_ed6_2.getText();
	m_recTicket.setScaler(sValue);

	if (m_cbx6_3.GetCurSel() > CB_ERR) 
		m_cbx6_3.GetWindowTextW(sValue);
	else 
		sValue = m_ed6_3.getText();
	m_recTicket.setHauler(sValue);

	if (m_cbx6_4.GetCurSel() > CB_ERR) 
		m_cbx6_4.GetWindowTextW(sValue);
	else 
		sValue = m_ed6_4.getText();
	m_recTicket.setBuyer(sValue);

	if (m_cbx6_5.GetCurSel() > CB_ERR) 
		m_cbx6_5.GetWindowTextW(sValue);
	else 
		sValue = m_ed6_5.getText();
	m_recTicket.setLocation(sValue);

	if (m_cbx6_6.GetCurSel() > CB_ERR) 
		m_cbx6_6.GetWindowTextW(sValue);
	else 
		sValue = m_ed6_6.getText();
	m_recTicket.setTractID(sValue);

	if (m_cbx6_7.GetCurSel() > CB_ERR) 
		m_cbx6_7.GetWindowTextW(sValue);
	else 
		sValue = m_ed6_7.getText();
	m_recTicket.setLoadType(sValue);

	if (m_cbx6_9.GetCurSel() > CB_ERR) 
		m_cbx6_9.GetWindowTextW(sValue);
	else 
		sValue = m_ed6_9.getText();
	m_recTicket.setDeductionType(sValue);

	if (m_cbx6_10.GetCurSel() > CB_ERR) 
		m_cbx6_10.GetWindowTextW(sValue);
	else 
		sValue = m_ed6_9.getText();
	m_recTicket.setSupplier(sValue);

	if (m_cbx6_11.GetCurSel() > CB_ERR) 
		m_cbx6_11.GetWindowTextW(sValue);
	else 
		sValue = m_ed6_10.getText();
	m_recTicket.setVendor(sValue);

	m_recTicket.setBarkRation(0.0);
	m_recTicket.setIBTaper(m_ed6_12.getFloat());

	// We'll check if user choosen to add haederdata to Register
	if (m_cb6_1.GetCheck()) m_mapHeaderData[REGISTER_TYPES::SOURCEID] = m_ed6_1.getText();
	if (m_cb6_2.GetCheck()) m_mapHeaderData[REGISTER_TYPES::SCALER] = m_ed6_2.getText();
	if (m_cb6_3.GetCheck()) m_mapHeaderData[REGISTER_TYPES::HAULER] = m_ed6_3.getText();
	if (m_cb6_4.GetCheck()) m_mapHeaderData[REGISTER_TYPES::BUYER] = m_ed6_4.getText();
	if (m_cb6_5.GetCheck()) m_mapHeaderData[REGISTER_TYPES::LOCATION] = m_ed6_5.getText();
	if (m_cb6_6.GetCheck()) m_mapHeaderData[REGISTER_TYPES::TRACTID] = m_ed6_6.getText();
	if (m_cb6_7.GetCheck()) m_mapHeaderData[REGISTER_TYPES::SUPPLIER] = m_ed6_9.getText();
	if (m_cb6_8.GetCheck()) m_mapHeaderData[REGISTER_TYPES::VENDOR] = m_ed6_10.getText();

	// Check measuringmode in ticket compared to seletced contract
	if (!checkMeasuringMode())
	{
		::MessageBox(GetSafeHwnd(),m_sMsgMeasModeError,m_sMsgCap,MB_ICONSTOP | MB_OK);
		m_btnSetup.EnableWindow(FALSE);
		return;
	}

	bLoadTypeAndDeduction = (m_cbx6_7.GetCurSel() > CB_ERR && m_cbx6_9.GetCurSel() > CB_ERR);

	if (checkLogs() && bTicketNumOK && bLoadTypeAndDeduction)
		OnOK();
	else
		::MessageBox(GetSafeHwnd(),m_sMsgFixData,m_sMsgCap,MB_ICONSTOP | MB_OK);
}

void CCorrectionDlg::OnBnClickedButton61()
{
	vecPricelistInTicket vecPrl;
	vecPricelistInTicket vecSpc;
	getPricelistFromTemplate(m_recLogScaleTmpl.getPricelist(),-1,vecPrl);
	getSpeciesFromTemplate(m_recLogScaleTmpl.getPricelist(),vecSpc);

	CSetupSpcQualDlg *pDlg = new CSetupSpcQualDlg();
	if (pDlg != NULL)
	{
		pDlg->setLogs(m_vecLogs);
		pDlg->setSpecies(m_vecSpecies);
		pDlg->setGrades(m_vecGrades);
		pDlg->setPricelistData(vecPrl);
		pDlg->setSpeciesData(vecSpc);
		if (pDlg->DoModal() == IDOK)
		{
			m_vecLogs = pDlg->getLogs();
			if (!checkSpeciesAndGrades())
			{
				::MessageBox(GetSafeHwnd(),m_sMsgSpeciesError,m_sMsgCap,MB_ICONSTOP | MB_OK);
				return;
			}
		}
	}
}

void CCorrectionDlg::OnCbnSelchangeCombo612()
{
	if (m_cbx6_12.GetCurSel() > CB_ERR)
	{
		// Check measuringmode in ticket compared to seletced contract
		if (!checkMeasuringMode())
		{
			if (m_bInitialized)
			{
				::MessageBox(GetSafeHwnd(),m_sMsgMeasModeError,m_sMsgCap,MB_ICONSTOP | MB_OK);
			}
			m_btnSetup.EnableWindow(FALSE);
			return;
		}

		m_btnSetup.EnableWindow(m_cbx6_12.GetCurSel() > CB_ERR);
		if (!checkSpeciesAndGrades())
		{
			if (m_bInitialized)
			{
				::MessageBox(GetSafeHwnd(),m_sMsgSpeciesError,m_sMsgCap,MB_ICONSTOP | MB_OK);
			}
			return;
		}
		
		// All seams to bo oke doke; will clean the Report
	}
}
// Toggle button; Use ticketnumber form file
void CCorrectionDlg::OnBnClickedButton62()
{
	if (!m_ed6_13.getText().IsEmpty())
	{
		m_btnGenerateTicketNo.SetStateX(FALSE);
		m_btnUseTicketNoFromFile.SetStateX(TRUE);
		m_xtButtonPressed = 0;	// m_btnUseTicketNoFromFile
	}
}

// Toggle button; Use generated ticketnumber
void CCorrectionDlg::OnBnClickedButton63()
{
	if (!m_ed6_13.getText().IsEmpty())
	{
		m_btnGenerateTicketNo.SetStateX(TRUE);
		m_btnUseTicketNoFromFile.SetStateX(FALSE);
		m_xtButtonPressed = 1;	// m_btnGenerateTicketNo
	}
	m_ed6_14.SetWindowTextW(m_sGeneratedTicketNum);
}

void CCorrectionDlg::OnEnChangeEdit614()
{
	if (m_ed6_13.getText().IsEmpty())
	{
		m_btnGenerateTicketNo.SetStateX(!m_ed6_14.getText().IsEmpty());
	}
}
