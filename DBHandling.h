#if !defined(__DBHANDLING_H__)
#define __DBHANDLING_H__

#pragma once

#include "TransactionClasses.h"

class CDBHandling : public CDBBaseClass_SQLApi
{
//private:
public:
	CDBHandling(void);
	CDBHandling(DB_CONNECTION_DATA &db_connection);

	////////////////////////////////////////////////////////////////
	// TICKETS
	BOOL getTickets(CVecTickets& vec,CXTPReportControl &report); 
	BOOL getTickets(CVecTickets& vec); 
	BOOL getTicket(CTickets& rec,int id); 
	BOOL newTicket(CTickets& rec); 
	BOOL updTicket(CTickets& rec); 
	BOOL delTicket(CTickets& rec); 
	BOOL delTicket(int pk_id); 
	int getLastLoadID(void);
	int getLastLoadID_generate();
	BOOL updTicketNumOfLogs(int pk_id,int num_of);
	// If Tickets-table is empty, run this method
	// to reseed IDENTITY-filed to start from 1
	BOOL resetTicketsIdentityField(void);	
	BOOL setLockOnTicket(int ticket_id,int v);

	////////////////////////////////////////////////////////////////
	// TicketSpc
	BOOL getTicketSpc(CVecTicketSpc& vec,int ticket_id,int spc_id = -1); 
	BOOL newTicketSpc(CTicketSpc& rec); 
	BOOL updTicketSpc(CTicketSpc& rec); 
	BOOL updTicketSpcCalcBasis(CTicketSpc& rec); 
	BOOL delTicketSpc(int ticket_id); 

	////////////////////////////////////////////////////////////////
	// TicketPrl
	BOOL getTicketPrl(CVecTicketPrl& vec,int ticket_id,int spc_id = -1); 
	BOOL newTicketPrl(CTicketPrl& rec); 
	BOOL updTicketPrl(CTicketPrl& rec); 
	BOOL updTicketPrlCalcBasis(CTicketPrl& rec); 
	BOOL delTicketPrl(int ticket_id); 

	////////////////////////////////////////////////////////////////
	// LOGS
	BOOL getLogs(CTickets& rec,CVecLogs& vec); 
	BOOL getAllLogs(CVecLogs& vec,LOGSTATUS::STATUS status = LOGSTATUS::NO_STATUS);
	BOOL getLogsFromTagNumbers(LPCTSTR sql,CVecLogs& vec,LOGSTATUS::STATUS status = LOGSTATUS::NO_STATUS);
	BOOL newLog(CLogs& rec); 
	BOOL updLog(CLogs& rec); 
	BOOL delLog(CLogs& rec); 
	BOOL avgLogData(CLogs& rec);
	// If Logs-table is empty, run this method
	// to reseed IDENTITY-filed to start from 1
	BOOL resetLogsIdentityField(void);	
	// Check if thre's more than one occurence of a tagnumber in the inventory database
	BOOL checkLogTagNumbers(LPCTSTR sql,vecDuplicateLogs &vec_duplicates);
	// Change status on a log. Used e.g. on sales
	BOOL setLogStatus(CLogs& rec);
	BOOL setLogStatus(CString sql);
	// Clean logs table; if Tagnumber is empty delete the row
	BOOL cleanLogsTable(int ticket_id);
	// Check if TagNumber already in table
	BOOL isLogTagUnique(CLogs& rec);
	BOOL isLogTagUnique2(CLogs& rec);
	BOOL isLogTagUnique3(LPCTSTR tag_num);

	////////////////////////////////////////////////////////////////
	// SPECIES
	BOOL getSpecies(CVecSpecies& vec,CXTPReportControl &report); 
	BOOL getSpecies(CVecSpecies& vec); 
	BOOL newSpecies(CSpecies& rec); 
	BOOL updSpecies(CSpecies& rec); 
	BOOL delSpecies(CSpecies& rec); 
	// If Species-table is empty, run this method
	// to reseed IDENTITY-filed to start from 1
	BOOL resetSpeciesIdentityField(void);	
	// Check for duplicate species in table
	BOOL isSpeciesUnique(); 
	// Check for duplicate species in table
	BOOL isSpeciesCodeInDB(LPCTSTR spc_code); 
	int getLastSpeciesID(void);

	////////////////////////////////////////////////////////////////
	// Grades
	BOOL getGrades(CVecGrades& vec,CXTPReportControl &report); 
	BOOL getGrades(CVecGrades& vec); 
	BOOL newGrades(CGrades& rec); 
	BOOL updGrades(CGrades& rec); 
	BOOL delGrades(CGrades& rec); 
	// If Grades-table is empty, run this method
	// to reseed IDENTITY-filed to start from 1
	BOOL resetGradesIdentityField(void);	
	// Check for duplicate species in table
	BOOL isGradesUnique(); 
	// Check for duplicate species in table
	BOOL isGradesCodeInDB(LPCTSTR spc_code); 
	int getLastGradesID(void);

	////////////////////////////////////////////////////////////////
	// Pricelist
	BOOL getPricelist(CVecPricelist& vec,int prl_id = -1); 
	BOOL getPricelist(CPricelist& rec,int prl_id = -1); 
	BOOL newPricelist(CPricelist& rec); 
	BOOL updPricelist(CPricelist& rec); 
	BOOL delPricelist(int spc_id = -1); 
	// reseed IDENTITY-filed to start from 1
	BOOL resetPricelistIdentityField(void);	
	BOOL isPricelistNameAlreadyUsed(LPCTSTR name);

	////////////////////////////////////////////////////////////////
	// Register
	BOOL getRegister(CVecRegister& vec,int type = -1); 
	BOOL newRegisterItem(CRegister& rec); 
	BOOL updRegisterItem(CRegister& rec); 
	BOOL delRegisterItem(CRegister& rec); 
	BOOL resetRegisterIdentityField(void);	
	BOOL isNameInRegister(int type,LPCTSTR name);

	////////////////////////////////////////////////////////////////
	// CalcTypes
	// Not used in new LogScalePC; 120117 p�d
	// Collected in DLL volumefunctions and
	// Userdefined volume-tables
	//	BOOL getCalcTypes(CVecCalcTypes& vec); 

	////////////////////////////////////////////////////////////////
	// Defaults
	BOOL getDefaults(CVecDefaults& vec); 
	BOOL addDefaults(CDefaults& rec); 
	BOOL updDefaults(CDefaults& rec); 
	BOOL isDefValueAdded(int id);

	////////////////////////////////////////////////////////////////
	// User defined volume-tables
	BOOL getUserVolTables(CVecUserVolTables& vec);
	BOOL addUserVolTable(CUserVolTables& rec);
	BOOL updUserVolTable(CUserVolTables& rec);
	BOOL delUserVolTable(CUserVolTables& rec);
	BOOL resetUserVolTableIdentityField(void);

	////////////////////////////////////////////////////////////////
	// Handle selected extra volumefunctions/ticket

	BOOL getSelVolFuncs(vecSelectedVolFuncs& vec,int ticket_id = -1);
	BOOL addSelVolFuncs(CSelectedVolFuncs& rec);
	BOOL updSelVolFuncs(CSelectedVolFuncs& rec);
	BOOL delSelVolFuncs(int ticket_id);
	BOOL resetSelVolFuncsIdentityField(void);

	////////////////////////////////////////////////////////////////
	// LogScale templates
	BOOL getTmplTables(vecLogScaleTemplates& vec);
	BOOL addTmplTable(CLogScaleTemplates& rec);
	BOOL updTmplTable(CLogScaleTemplates& rec);
	BOOL delTmplTable(CLogScaleTemplates& rec);
	BOOL updTmplTablePricelist(int tmpl_id,int prl_id,LPCTSTR prl_name,LPCTSTR prl);
	BOOL resetTmplTableIdentityField(void);
	BOOL isTmplInUse(int tmpl_id);
	BOOL isTmplNameAlreadyUsed(LPCTSTR name);


	////////////////////////////////////////////////////////////////
	// Default uservolumes selected
	BOOL getDefaultUserVolFuncSel(vecDefUserVolFuncs& vec); 
	BOOL addDefaultUserVolFuncSel(CDefUserVolFuncs& rec); 
	BOOL delDefaultUserVolFuncSel(); 

	////////////////////////////////////////////////////////////////
	// Inventory tables; for i.g. Sales etc.
	BOOL getInventories(vecInventory& vec);
	BOOL getInventory(int pk_id,int type,CInventory& rec);
	BOOL newInventory(CInventory& rec);
	BOOL updInventory(CInventory& rec);
	BOOL delInventory(CInventory& rec);
	BOOL delInventory(int pk_id,int type = -1);
	void getLastInventoryAndTypeID(int *pk_id,int *type);
	int getLastInventoryID_generate();
	// reseed IDENTITY-filed to start from 1
	BOOL resetInventoryIdentityField(void);	

	BOOL getInventoryLogs(vecInventoryLogs& vec,int invent_id = -1);
	BOOL newInventoryLogs(CInventoryLogs& rec);
	BOOL newInventoryLogs(CString sql);
	BOOL delInventoryLogs(CInventoryLogs& rec);

	BOOL isTicketInInventLogs(int pk_id);

	BOOL getLogsSale(CVecLogs& vec,int invent_id = -1);
	BOOL avgSaleLogData(CLogs& rec,int invent_id = -1);
	BOOL isSalesIDUnique(LPCTSTR sales_id,CInventory& rec);

	// Summerize data
	BOOL updSumLogData(int load_id);

	// Storage search 
	BOOL runSearchQuestion(vecSearch& vec,LPCTSTR sql,int meas_mode,int lang_set);
	BOOL runSearchQuestionSales(vecSearchSales& vec,LPCTSTR sql);

};

#endif
