#if !defined(__CREATETABLES_H__)
#define __CREATETABLES_H__
#pragma once

//////////////////////////////////////////////////////////////////////////////////////////
//	Create database table; 110427
const LPCTSTR table_Tickets = _T("CREATE TABLE dbo.%s (")
	_T("pkLoadID int identity(1,1) NOT NULL,")
	_T("sSourceDate nvarchar(25) NULL,")		// Datum dd-mm-yyyy
	_T("sSourceID nvarchar(50) NULL,")
	_T("sScaler nvarchar(50) NULL,")
	_T("sLocation nvarchar(50) NULL,")
	_T("sBuyer nvarchar(50) NULL,")
	_T("sOtherInfo nvarchar(128) NULL,")
	_T("sLoadID nvarchar(50) NULL,")
	_T("sTicket nvarchar(50) NULL,")
	_T("sTractID nvarchar(50) NULL,")
	_T("sHauler nvarchar(50) NULL,")
	_T("sSupplier nvarchar(50) NULL,")
	_T("sVendor nvarchar(50) NULL,")
	_T("sLoadType nvarchar(50) NULL,")
	_T("sDate nvarchar(25) NULL,")
	_T("fWeight float NULL,")
	_T("nNumOfLogs int NULL,")
	_T("sNotes nvarchar(255) NULL,")
	_T("nSawlogVolumeUnit int NULL,")
	_T("nPulpwoodVolUnit int NULL,")
	_T("nRoundScaling int NULL,")
	_T("nUseSouthernDoyle int NULL,")
	_T("sDeductionType nvarchar(20) NULL,")
	_T("fIBTaper float NULL,")
	_T("fBarkRatio float NULL,")
	_T("nMeasuringMode int NULL,")
	_T("fSumVolFromWeight float NULL,")
	_T("fSumVolPricelist float NULL,")		// Volym utr�knad fr�n Prislista
	_T("fSumPrice float NULL,")						// Pris fr�n prislista
	//-----------------------------------------------------------
	_T("sFileVersion nvarchar(50) NULL,")
	_T("sTemplateUsed nvarchar(50) NULL,")
	_T("sPricelistUsed nvarchar(50) NULL,")
	_T("nPricelistID int NULL,")
	_T("sGISCoord1 nvarchar(50) NULL,")	// Can be Lat, X etc 120127
	_T("sGISCoord2 nvarchar(50) NULL,")	// Can be Long, Y etc	120127
	_T("nJAS smallint NULL,")										// Flag set in caliper
	_T("fTrimFT float NULL,")										// Trim for log-length
	_T("fTrimCM float NULL,")										// Trim for log-length
	_T("nTemplateID int NULL,")
	_T("nLocked smallint NULL,")
	_T("dCreated datetime NOT NULL default CURRENT_TIMESTAMP,")
	_T("dUpdated datetime NOT NULL default CURRENT_TIMESTAMP,")
	_T("primary key(pkLoadID));");

const LPCTSTR table_Logs = _T("CREATE TABLE dbo.%s (")
	_T("pkLogID int identity(1,1)  NOT NULL,")
	_T("fkLoadID int NOT NULL,")
	_T("sTagNumber nvarchar(50) NULL,")
	_T("sSpcCode nvarchar(15) NULL,")
	_T("sSpcname nvarchar(50) NULL,")
	_T("nFrequency int NULL,")
	_T("sGrade nvarchar(15) NULL,")
	_T("fDeduction float NULL,")
	_T("fDOBTop float NULL,")						// Toppdiamter �ver bark
	_T("fDIBTop float NULL,")						// Toppdiamter under bark
	_T("fDOBRoot float NULL,")					// Rotdiamter �ver bark
	_T("fDIBRoot float NULL,")					// Rotdiamter under bark
	_T("fLengthMeas float NULL,")				// Stockens verkliga l�ngd
	_T("fLengthCalc float NULL,")				// Stockens l�ngd trimmad och avrundad
	_T("fBark float NULL,")							// Barkavdrag
	_T("fVolPricelist float NULL,")			// Volym fr�n prislista
	_T("fLogPrice float NULL,")					// Pris per stock
	_T("nVolFuncID int NULL,")					// ID p� volymsfunktion
	_T("sVolFuncName nvarchar(30) NULL,")	// Namn p� volymsfunktion; ex. Doyle etc.
	//-----------------------------------------------------------
	// Kustomf�lt f�r volymsfunktioner
	_T("fCustVol1 float NULL,")
	_T("fCustVol2 float NULL,")
	_T("fCustVol3 float NULL,")
	_T("fCustVol4 float NULL,")
	_T("fCustVol5 float NULL,")
	_T("fCustVol6 float NULL,")
	_T("fCustVol7 float NULL,")
	_T("fCustVol8 float NULL,")
	_T("fCustVol9 float NULL,")
	_T("fCustVol10 float NULL,")
	_T("fCustVol11 float NULL,")
	_T("fCustVol12 float NULL,")
	_T("fCustVol13 float NULL,")
	_T("fCustVol14 float NULL,")
	_T("fCustVol15 float NULL,")
	_T("fCustVol16 float NULL,")
	_T("fCustVol17 float NULL,")
	_T("fCustVol18 float NULL,")
	_T("fCustVol19 float NULL,")
	_T("fCustVol20 float NULL,")
	//-----------------------------------------------------------
	// Egeninl�sta v�rden (styr �ver inm�tta JA)
	_T("fUserVolume float NULL,")					// Volym
	_T("fUserToppDiam float NULL,")				// Toppdiamter
	_T("fUserRootDiam float NULL,")				// Rotdiamter
	_T("fUserLength float NULL,")					// L�ngd
	_T("fUserPrice float NULL,")					// Pris
	_T("fDOBTop2 float NULL,")						// Toppdiamter �ver bark; added 2011-10-31 p�d
	_T("fDIBTop2 float NULL,")						// Toppdiamter under bark; added 2011-10-31 p�d
	_T("fDOBRoot2 float NULL,")						// Rotdiamter �ver bark; added 2011-10-31 p�d
	_T("fDIBRoot2 float NULL,")						// Rotdiamter under bark; added 2011-10-31 p�d
	_T("fUserToppDiam2 float NULL,")			// Toppdiamter; added 2011-10-31 p�d
	_T("fUserRootDiam2 float NULL,")			// Rotdiamter; added 2011-10-31 p�d
	//-----------------------------------------------------------
	_T("nStatusFlag smallint NULL,")			// Flagga f�r stockens status; ex. I lager,S�ld etc.
	_T("sReason nvarchar(15) NULL,")			// Reason (Anledning)
	_T("sNotes nvarchar(15) NULL,")				// Notering
	_T("sGISCoord1 nvarchar(50),")				// Can be Lat, X etc 120127
	_T("sGISCoord2 nvarchar(50),")				// Can be Long, Y etc 120127
	_T("nTicketOrigin smallint NULL,")		// Set 0 = From a ticketfile, 1 = Manually entered
	/*************************************************/
	_T("dCreated datetime NOT NULL default CURRENT_TIMESTAMP,")	// created,
	_T("dUpdated datetime NOT NULL default CURRENT_TIMESTAMP,")	// last updated
	/* Setup relations; primary and forigen */
	_T("primary key(pkLogID,fkLoadID),")
	_T("constraint fk_logscale_logs_n ")
	_T("foreign key (fkLoadID) ")
	_T("references logscaleTicketsNew(pkLoadID) ")
	_T("on delete cascade ")
	_T("on update cascade); ");


const LPCTSTR table_Register = _T("CREATE TABLE dbo.%s (")
	_T("pkID  int identity(1,1) NOT NULL,")		// Primary key
	_T("nTypeID smallint NOT NULL,")						// ID f�r typ (ex. Buyer,Vendor,Location etc. )
	_T("sName nvarchar(50) NULL,")					// Namn p� typ (ex Name of byer or vendor etc.)
	_T("sAbbrevation nvarchar(10) NULL,")		// F�rkortning, att anv�nda i klave
	_T("sID nvarchar(20) NULL,")						// Ev. ID-nummer
	_T("sOtherInfo nvarchar(50) NULL,")			// 15 i klave
	/*************************************************/
	_T("sAddress nvarchar(50) NULL,")				// Adress
	_T("sAddress2 nvarchar(50) NULL,")			// Adress2
	_T("sZipCode nvarchar(20) NULL,")				// ZipCode
	_T("sCity nvarchar(50) NULL,")					// Stad
	_T("sPhone nvarchar(30) NULL,")					// Telefonnummer
	_T("sCellPhone nvarchar(30) NULL,")			// Mobilnummer
	_T("sWWW nvarchar(50) NULL,")						// Hemsida
	/*************************************************/
	_T("dCreated datetime NOT NULL default CURRENT_TIMESTAMP,")	// created,
	_T("dUpdated datetime NOT NULL default CURRENT_TIMESTAMP,")	// last updated
	_T("primary key(pkID,nTypeID),")
	_T("constraint reg_1 unique nonclustered(nTypeID,sName)) ");

const LPCTSTR table_Species = _T("CREATE TABLE dbo.%s (")
	_T("pkSpeciesID int identity(1,1) NOT NULL,")		// Primary key
	_T("sSpeciesCode nvarchar(15) NULL,")					// Tr�dslagskod
	_T("sSpeciesName nvarchar(50) NULL,")	        // Namn p� tr�dslag
	_T("fBarkReductionInch float NULL,")					// Barkavdrag/Tr�dslag Inch/10
	_T("fBarkReductionMM float NULL,")						// Barkavdrag/Tr�dslag MM
	_T("dCreated datetime NOT NULL default CURRENT_TIMESTAMP,")	// created,
	_T("dUpdated datetime NOT NULL default CURRENT_TIMESTAMP,")	// last updated
	_T("primary key(pkSpeciesID))")//;
#ifdef _SET_BARKREDUCTION_ON_SPECIES
	_T("INSERT INTO logscaleSpeciesNew (sSpeciesCode,sSpeciesName,fBarkReductionInch,fBarkReductionMM) values('1','Pine',0.1,5.0);")
	_T("INSERT INTO logscaleSpeciesNew (sSpeciesCode,sSpeciesName,fBarkReductionInch,fBarkReductionMM) values('2','Spruce',0.1,5.0);")
	_T("INSERT INTO logscaleSpeciesNew (sSpeciesCode,sSpeciesName,fBarkReductionInch,fBarkReductionMM) values('3','Birch',0.1,5.0);")
	_T("INSERT INTO logscaleSpeciesNew (sSpeciesCode,sSpeciesName,fBarkReductionInch,fBarkReductionMM) values('4','',0.1,5.0);")
	_T("INSERT INTO logscaleSpeciesNew (sSpeciesCode,sSpeciesName,fBarkReductionInch,fBarkReductionMM) values('5','',0.1,5.0);")
	_T("INSERT INTO logscaleSpeciesNew (sSpeciesCode,sSpeciesName,fBarkReductionInch,fBarkReductionMM) values('6','',0.1,5.0);")
	_T("INSERT INTO logscaleSpeciesNew (sSpeciesCode,sSpeciesName,fBarkReductionInch,fBarkReductionMM) values('7','',0.1,5.0);")
	_T("INSERT INTO logscaleSpeciesNew (sSpeciesCode,sSpeciesName,fBarkReductionInch,fBarkReductionMM) values('8','',0.1,5.0);")
	_T("INSERT INTO logscaleSpeciesNew (sSpeciesCode,sSpeciesName,fBarkReductionInch,fBarkReductionMM) values('9','',0.1,5.0);")
	_T("INSERT INTO logscaleSpeciesNew (sSpeciesCode,sSpeciesName,fBarkReductionInch,fBarkReductionMM) values('10','',0.1,5.0);");
#else
	_T("INSERT INTO logscaleSpeciesNew (sSpeciesCode,sSpeciesName,fBarkReductionInch,fBarkReductionMM) values('1','Pine',0.0,0.0);")
	_T("INSERT INTO logscaleSpeciesNew (sSpeciesCode,sSpeciesName,fBarkReductionInch,fBarkReductionMM) values('2','Spruce',0.0,0.0);")
	_T("INSERT INTO logscaleSpeciesNew (sSpeciesCode,sSpeciesName,fBarkReductionInch,fBarkReductionMM) values('3','Birch',0.0,0.0);")
	_T("INSERT INTO logscaleSpeciesNew (sSpeciesCode,sSpeciesName,fBarkReductionInch,fBarkReductionMM) values('4','',0.0,0.0);")
	_T("INSERT INTO logscaleSpeciesNew (sSpeciesCode,sSpeciesName,fBarkReductionInch,fBarkReductionMM) values('5','',0.0,0.0);")
	_T("INSERT INTO logscaleSpeciesNew (sSpeciesCode,sSpeciesName,fBarkReductionInch,fBarkReductionMM) values('6','',0.0,0.0);")
	_T("INSERT INTO logscaleSpeciesNew (sSpeciesCode,sSpeciesName,fBarkReductionInch,fBarkReductionMM) values('7','',0.0,0.0);")
	_T("INSERT INTO logscaleSpeciesNew (sSpeciesCode,sSpeciesName,fBarkReductionInch,fBarkReductionMM) values('8','',0.0,0.0);")
	_T("INSERT INTO logscaleSpeciesNew (sSpeciesCode,sSpeciesName,fBarkReductionInch,fBarkReductionMM) values('9','',0.0,0.0);")
	_T("INSERT INTO logscaleSpeciesNew (sSpeciesCode,sSpeciesName,fBarkReductionInch,fBarkReductionMM) values('10','',0.0,0.0);");
#endif

const LPCTSTR table_Grades = _T("CREATE TABLE dbo.%s (")
	_T("pkGradeID int identity(1,1) NOT NULL,")		// Primary key
	_T("sGradeCode nvarchar(15) NULL,")						// Kvalitetskod
	_T("sGradeName nvarchar(50) NULL,")						// Namn p� kvalitet
	_T("nPulpwood smallint NULL,")								// 1 = Pulpwood
	_T("dCreated datetime NOT NULL default CURRENT_TIMESTAMP,")	// created,
	_T("dUpdated datetime NOT NULL default CURRENT_TIMESTAMP,")	// last updated
	_T("primary key(pkGradeID));")//;	// Remove semicolon and
//* uncomment to insert predefined grades (same as in LogScaleCE - PC by T. Matney)
	_T("INSERT INTO logscaleGradesNew (sGradeCode,sGradeName,nPulpwood) values('SW','Sawlog',0);")
	_T("INSERT INTO logscaleGradesNew (sGradeCode,sGradeName,nPulpwood) values('PW','Pulpwood',1);")
	_T("INSERT INTO logscaleGradesNew (sGradeCode,sGradeName,nPulpwood) values('CULL','Cull grade',1);")
	_T("INSERT INTO logscaleGradesNew (sGradeCode,sGradeName,nPulpwood) values('LG_1','User definable codes',0);")
	_T("INSERT INTO logscaleGradesNew (sGradeCode,sGradeName,nPulpwood) values('LG_2','User definable codes',0);")
	_T("INSERT INTO logscaleGradesNew (sGradeCode,sGradeName,nPulpwood) values('LG_3','User definable codes',0);")
	_T("INSERT INTO logscaleGradesNew (sGradeCode,sGradeName,nPulpwood) values('LG_4','User definable codes',0);")
	_T("INSERT INTO logscaleGradesNew (sGradeCode,sGradeName,nPulpwood) values('LG_5','User definable codes',0);");
	
const LPCTSTR table_Pricelist = _T("CREATE TABLE dbo.%s (")
	_T("pkPrlID int identity(1,1) NOT NULL,")		// Primary key
	_T("sName nvarchar(50) NULL,")										// Name of pricelist
	_T("sCreatedBy nvarchar(25) NULL,")								// Created by
	_T("sDate nvarchar(25) NULL,")										// Created date
	_T("sPricelist ntext NULL,")											// Pricelist
	_T("nLocked smallint NULL,")											// 1 = Locked, 0 = Unlocked
	_T("sNotes ntext NULL,")													// Noteringar
	_T("dCreated datetime NOT NULL default CURRENT_TIMESTAMP,")	// created,
	_T("dUpdated datetime NOT NULL default CURRENT_TIMESTAMP,")	// last updated
	_T("primary key(pkPrlID) );");

//-------------------------------------------------------------------------------
// Species in ticket
const LPCTSTR table_TicketSpc = _T("CREATE TABLE dbo.%s (")
	_T("fkTicketID int NOT NULL,")					// Primary key
	_T("fkSpeciesID int NOT NULL,")					// Primary key
	_T("sSpeciesCode nvarchar(15) NULL,")		// Tr�dslagskod
	_T("sSpeciesName nvarchar(50) NULL,")	  // Namn p� tr�dslag
	_T("fBarkReduction float NULL,")				// Barkavdrag/Tr�dslag
	_T("primary key(fkSpeciesID,fkTicketID), ")
	_T("constraint fk_logscale_ticket_n ")
	_T("foreign key (fkTicketID) ")
	_T("references logscaleTicketsNew(pkLoadID) ")
	_T("on delete cascade ")
	_T("on update cascade );");


// Pricelist for ticket
const LPCTSTR table_TicketPrl = _T("CREATE TABLE dbo.%s (")
	_T("fkTicketID int NOT NULL,")							// Primary key
	_T("fkSpeciesID int NOT NULL,")							// Primary key
	_T("fkGradesID int NOT NULL,")							// Primary key
	_T("sSpeciesCode nvarchar(15) NULL,")				// Specie code
	_T("sGradesCode nvarchar(15) NULL,")				// Grade code
	_T("fPrice float NULL,")										// Price for spieces and grade
	_T("nFuncID int NULL,")											// ID of volume-function 
	_T("sCalcType nvarchar(50) NULL,")					// Price calculated from type of volumeformula
	_T("nPulpwood smallint NULL,")							// 1 = Pulpwood same as in timGrades table
	_T("primary key(fkTicketID,fkSpeciesID,fkGradesID), ")
	_T("constraint fk_logscale_ticket_spc_n ")
	_T("foreign key (fkTicketID) ")
	_T("references logscaleTicketsNew(pkLoadID) ")
	_T("on delete cascade ")
	_T("on update cascade);");

// Table for holding information on "extra" volumefunctions
// NB!!! per Ticket
const LPCTSTR table_SelectedVolFunctions = _T("CREATE TABLE dbo.%s (")
	_T("pkID int identity(1,1) NOT NULL,")			// Primary key, identity
	_T("fkTicketID int NOT NULL,")							// Primary key, Traktid
	_T("nFuncID int NULL,")											// ID for selected function
	_T("sFullName nvarchar(50) NULL,")					// Full name of function
	_T("sAbbrevName nvarchar(25) NULL,")				// Abbrevation name of function
	_T("sBasisName nvarchar(25) NULL,")					// E.g. my name-mbf
	_T("primary key(pkID,fkTicketID), ")
	_T("constraint fk_logscale_ticket_extra_vfunc_n ")
	_T("foreign key (fkTicketID) ")
	_T("references logscaleTicketsNew(pkLoadID) ")
	_T("on delete cascade ")
	_T("on update cascade);");


//-------------------------------------------------------------------------------

// Default values for calculations
const LPCTSTR table_Defaults = _T("CREATE TABLE dbo.%s (")
	_T("nDefValueID smallint NOT NULL,")	// 1 = Sawlog volume unit,2 = Pulpwood volume unit etc, also primary key
	_T("sDefValue nvarchar(50) NULL,")	// String value, depending on nDefValueID set
	_T("sDefBasisValue nvarchar(25) NULL,")	// String value, depending on nDefValueID set
	_T("dCreated datetime NOT NULL default CURRENT_TIMESTAMP,")	// created,
	_T("dUpdated datetime NOT NULL default CURRENT_TIMESTAMP,")	// last updated
	_T("nFuncID int NULL,")	//#4258 lagt till FuncID i defaulttabellen
	_T("primary key(nDefValueID)) ")
	_T("INSERT INTO logscaleDefaultsNew (nDefValueID,sDefValue,sDefBasisValue,nFuncID) values(1,'Int-1/4','Int4-mbf',1);")
	_T("INSERT INTO logscaleDefaultsNew (nDefValueID,sDefValue,sDefBasisValue,nFuncID) values(2,'Cubic-feet','Cunit',11);")
	_T("INSERT INTO logscaleDefaultsNew (nDefValueID,sDefValue,sDefBasisValue,nFuncID) values(3,'PERCENT','PERCENT',-1);")
	_T("INSERT INTO logscaleDefaultsNew (nDefValueID,sDefValue,sDefBasisValue,nFuncID) values(4,'1','1',-1);")
	_T("INSERT INTO logscaleDefaultsNew (nDefValueID,sDefValue,sDefBasisValue,nFuncID) values(5,'SW','SW',-1);")
	_T("INSERT INTO logscaleDefaultsNew (nDefValueID,sDefValue,sDefBasisValue,nFuncID) values(6,'SHORT LOG','SHORT LOG',-1);")
	_T("INSERT INTO logscaleDefaultsNew (nDefValueID,sDefValue,sDefBasisValue,nFuncID) values(7,'1','1',-1);")
	_T("INSERT INTO logscaleDefaultsNew (nDefValueID,sDefValue,sDefBasisValue,nFuncID) values(80,'0.0','0.0',-1);")
	_T("INSERT INTO logscaleDefaultsNew (nDefValueID,sDefValue,sDefBasisValue,nFuncID) values(81,'0.0','0.0',-1);")
	_T("INSERT INTO logscaleDefaultsNew (nDefValueID,sDefValue,sDefBasisValue,nFuncID) values(9,'0','0',-1);")
	_T("INSERT INTO logscaleDefaultsNew (nDefValueID,sDefValue,sDefBasisValue,nFuncID) values(10,'0','0',-1);")
	_T("INSERT INTO logscaleDefaultsNew (nDefValueID,sDefValue,sDefBasisValue,nFuncID) values(11,'1.0','1.0',-1);")
	_T("INSERT INTO logscaleDefaultsNew (nDefValueID,sDefValue,sDefBasisValue,nFuncID) values(12,'0.1','0.1',-1);")
	_T("INSERT INTO logscaleDefaultsNew (nDefValueID,sDefValue,sDefBasisValue,nFuncID) values(13,'0.125','0.125',-1);");


// Create user volume-functions
const LPCTSTR table_UserVolTables = _T("CREATE TABLE dbo.%s (")
	_T("pkID int identity(1,1) NOT NULL,")	// Primary key, identity-field
	_T("nFuncID int NULL,")									// A unique id for this function
	_T("sFullName nvarchar(50) NULL,")					// Full name of function
	_T("sAbbrevName nvarchar(25) NULL,")					// Abbrevation name of function
	_T("sBasisName nvarchar(25) NULL,")					// E.g. my name-mbf
	_T("sCreatedBy nvarchar(25) NULL,")					// Created by
	_T("sDate nvarchar(25) NULL,")							// Created date
	_T("nTableType smallint NULL,")							// Type of volumetable (0=Simple, 1=..., etc.)
	_T("nMeasuringMode smallint NULL,")					// Measuring-mode; Inch or Metric
	_T("sNotes ntext NULL,")										// Notes for volume-table
	_T("sVolTable ntext NULL,")									// Actual table; semicolon-separated
	_T("nLocked smallint NULL,")								// Locked = 1, no changes can be made, UnLocked = 0
	_T("dCreated datetime NOT NULL default CURRENT_TIMESTAMP,")	// created,
	_T("dUpdated datetime NOT NULL default CURRENT_TIMESTAMP,")	// last updated
	_T("primary key(pkID));");


// Create default user-volume settings
const LPCTSTR table_DefaultUserVolTables = _T("CREATE TABLE dbo.%s (")
	_T("nFuncID int NOT NULL,")									// Primary key
	_T("sFullName nvarchar(50) NULL,")					// Full name of function
	_T("sAbbrevName nvarchar(25) NULL,")				// Abbrevation name of function
	_T("sBasisName nvarchar(25) NULL,")					// E.g. my name-mbf
	_T("primary key(nFuncID));");


// Table for holding information on templates
const LPCTSTR table_Templates = _T("CREATE TABLE dbo.%s (")
	_T("pkTmplID int identity(1,1) NOT NULL,")	// Primary key, identity
	_T("sTmplName nvarchar(50) NULL,")					// Full name of Template
	_T("sCreatedBy nvarchar(25) NULL,")					// Skapad av
	_T("sDate nvarchar(25) NULL,")							// Datum
	_T("nBuyer int NULL,")										// Leverant�r PKID
	_T("nHauler int NULL,")										// �kare PKID
	_T("nLocation int NULL,")									// Plats PKID
	_T("nTractID int NULL,")										// TraktID PKID
	_T("nScaler int NULL,")										// M�tare PKID
	_T("nSourceID int NULL,")									// K�ll-ID PKID
	_T("nVendor int NULL,")										// S�ljare PKID
	_T("nSupplier int NULL,")									// Leverant�r PKID
	_T("sNotes ntext NULL,")										// Notes for volume-table
	_T("sPricelist ntext NULL,")								// Semicolon-seperated data on pricelist
	_T("nPrlID int NULL,")											// ID of pricelist = primary key
	_T("sPrlName nvarchar(50) NULL,")						// Name of pricelist added to template
	_T("fTrimFT float NULL,")									// Trim for logs in Inch
	_T("fTrimCM float NULL,")										// Trim for logs in CM
	_T("nMeasureMode int NULL,")								// MeasureMode; 0 = Inch, 1 = Metric
	_T("nDefSpecies int NULL,")									// Id for default species
	_T("nDefGrade int NULL,")										// Id for default grade
	_T("dCreated datetime NOT NULL default CURRENT_TIMESTAMP,")	// created,
	_T("dUpdated datetime NOT NULL default CURRENT_TIMESTAMP,")	// last updated
	_T("primary key(pkTmplID));");

//////////////////////////////////////////////////////////////////////////////////////////
// Tables for Sales/In Mill etc.
const LPCTSTR table_Invent = _T("CREATE TABLE dbo.%s (")
	_T("pkInventID int identity(1,1) NOT NULL,")	// Primary key, identity
	_T("nType int NOT NULL,")								// 0 = In stocke, 1 = Sales, 2 = Sawmill
	_T("sName nvarchar(50) NULL,")					// Full name
	_T("sID nvarchar(25) NULL,")						// ID-number (Sales number etc.)
	_T("sCreatedBy nvarchar(25) NULL,")			// Skapad av
	_T("sDate nvarchar(25) NULL,")					// Datum
	_T("nNumOfLogs int NULL,")							// Antal stockar i Sale
	_T("fSumPrice float NULL,")							// Summa pris f�r denna sale
	_T("nBuyer int NULL,")									// Buyer PKID
	_T("nHauler int NULL,")									// �kare PKID
	_T("nLocation int NULL,")								// Plats PKID
	_T("nTractID int NULL,")								// TraktID PKID
	_T("nScaler int NULL,")									// M�tare PKID
	_T("nSourceID int NULL,")								// K�ll-ID PKID
	_T("nVendor int NULL,")									// S�ljare PKID
	_T("nSupplier int NULL,")								// Leverant�r PKID
	_T("sNotes ntext NULL,")								// Notes
	_T("sGPSCoord1 nvarchar(50) NULL,")			// Can be Lat, X etc 120127
	_T("sGPSCoord2 nvarchar(50) NULL,")			// Can be Long, Y etc	120127
	_T("dCreated datetime NOT NULL default CURRENT_TIMESTAMP,")	// created,
	_T("dUpdated datetime NOT NULL default CURRENT_TIMESTAMP,")	// last updated
	_T("primary key(pkInventID));");

const LPCTSTR table_Invent_Logs = _T("CREATE TABLE dbo.%s (")
	_T("fkInventID int NOT NULL,")				// Primary key
	_T("fkLoadID int NOT NULL,")					// Primary key = Ticket
	_T("fkLogID int NOT NULL,")						// Primary key
	_T("dCreated datetime NOT NULL default CURRENT_TIMESTAMP,")	// created,
	_T("dUpdated datetime NOT NULL default CURRENT_TIMESTAMP,")	// last updated
	_T("primary key(fkInventID,fkLoadID,fkLogID), ")
	_T("constraint fk_logscale_invent_info1_n ")
	_T("foreign key (fkInventID) ")
	_T("references logscaleSalesInventNew(pkInventID) ")
	_T("on delete cascade ")
	_T("on update cascade , ")
	_T("constraint fk_logscale_invent_info2_n ")
	_T("foreign key (fkLogID,fkLoadID) ")
	_T("references logscaleLogsNew(pkLogID,fkLoadID) ")
	_T("on delete cascade ")
	_T("on update cascade);");


// This script also does a check to see if column already exists; 2011-10-31

//**********************************************************************************************************************
// ALTER SPECIES TABLE

//**********************************************************************************************************************
// ALTER LOGS TABLE

//**********************************************************************************************************************
// ALTER TICKETS TABLE
const LPCTSTR alter_Tickets120618_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																				 where table_name = '%s' and column_name = 'nLocked')  \
																				 alter table %s add nLocked smallint");

// This script also does a check to see if column already exists; 090423 p�d
const LPCTSTR alter_Logs120305_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																				 where table_name = '%s' and column_name = 'nTicketOrigin')  \
																				 alter table %s add nTicketOrigin smallint");

const LPCTSTR alter_Logs120314_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																				 where table_name = '%s' and column_name = 'nTemplateID')  \
																				 alter table %s add nTemplateID int");
//#4258 alter defaults table
const LPCTSTR alter_Defaults150205_1 = _T("if not exists (select column_name from INFORMATION_SCHEMA.columns \
																				 where table_name = '%s' and column_name = 'nFuncID')  \
																				 alter table %s add nFuncID int");
#endif