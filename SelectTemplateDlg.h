#pragma once

#include "resource.h"

// CSelectTemplateDlg dialog

class CSelectTemplateDlg : public CDialog
{
	DECLARE_DYNAMIC(CSelectTemplateDlg)
	BOOL m_bInitialized;
	CString m_sLangFN;

	CListBox m_lbdlg2_1;
	CButton m_wndOKBtn;
	CButton m_wndCancelBtn;

	CLogScaleTemplates m_recSelectedTemplates;
	vecLogScaleTemplates m_vecLogScaleTemplates;

	void setupData();
public:
	CSelectTemplateDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSelectTemplateDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG2 };

	void setData(vecLogScaleTemplates &vec) { m_vecLogScaleTemplates = vec; }
	CLogScaleTemplates getSelectedTemplate()	{ return m_recSelectedTemplates; }
protected:
	//{{AFX_VIRTUAL(CSelectTemplateDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnLbnSelchangeList21();
};
