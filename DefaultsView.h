// CDefaultsView form view

#if !defined(__DEFAULTSVIEW_H__)
#define __DEFAULTSVIEW_H__

#pragma once

#include "stdafx.h"

#include "dbhandling.h"

#include "Resource.h"

class CMyPropertyGridItemDouble : public CXTPPropertyGridItemDouble
{
	CString sToolTip;
	int nMaxLength;
	bool bCheck; // true = <= 0.0 false = < 0.0
	BOOL isNumeric(LPCTSTR value);
public:
	// Default constructor
	CMyPropertyGridItemDouble();
	CMyPropertyGridItemDouble(LPCTSTR cap,double value,LPCTSTR format,int max_length,bool check);

	inline void setToolTip(LPCTSTR v)	{ sToolTip = v; }
protected:
	virtual void OnValueChanged(CString strValue);
};

class CMyPropertyGridItemNumber : public CXTPPropertyGridItemNumber
{
	CString sToolTip;
	int nMaxLength;

	BOOL isNumeric(LPCTSTR value);
public:
	// Default constructor
	CMyPropertyGridItemNumber();
	CMyPropertyGridItemNumber(LPCTSTR cap,long value,int max_length);

	inline void setToolTip(LPCTSTR v)	{ sToolTip = v; }
protected:
	virtual void OnValueChanged(CString strValue);
};


///////////////////////////////////////////////////////////////////////////////////////////
// CDefaultsDoc

class CDefaultsDoc : public CDocument
{
protected: // create from serialization only
	CDefaultsDoc();
	DECLARE_DYNCREATE(CDefaultsDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDefaultsDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CDefaultsDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CDefaultsDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
/////////////////////////////////////////////////////////////////////////////
// class CDefaultsFrame

class CDefaultsFrame : public CChildFrameBase
{
//private:

	DECLARE_DYNCREATE(CDefaultsFrame)
	CXTPToolBar m_wndToolBar;
	CXTPDockingPaneManager m_paneManager;
	CString m_sLangFN;

	BOOL m_bFirstOpen;
	BOOL m_bInitReports;

	BOOL m_bIsSysCommand;

	void setNavBarButtons()
	{
		// Send messages to HMSShell, disable buttons on toolbar; 120122 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

		// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 120122 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
	}

protected:
	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	HICON m_hIcon;
public:
	CDefaultsFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

// Attributes

// Operations

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	virtual ~CDefaultsFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	
	//{{AFX_MSG(CDefaultsFrame)
	afx_msg int OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl);
	afx_msg void OnSysCommand(UINT nID,LPARAM lParam);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnClose();
	afx_msg void OnDestroy(void);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);

	//}}AFX_MSG
	
	DECLARE_MESSAGE_MAP()
};

// CDefaultsView form view

class CDefaultsView : public CXTResizeFormView //CXTResizeFormView
{
	DECLARE_DYNCREATE(CDefaultsView)

	// Data members
	BOOL m_bInitialized;
	CString m_sLangFN;

	CString m_sMsgCap;
	CString m_sMsgMaxUserdefFunctions;
	CString m_sMsgToolTipNoGTZero;
	CString m_sMsgToolTipNoGTZero2;
	CString m_sMsgToolTipNoGTorEQZero;
	CString m_sMsgToolTipNoGTorEQZero2;

	CString m_sSettingsSWtext;	//#4258 ny variabel
	CString m_sSettingsPWtext;	//#4258 ny variabel

	CString m_sYes;
	CString m_sNo;

	CXTPPropertyGrid m_propDefaults;

	int m_nSawlogFuncID;	//#4258 ny variabel
	int m_nPulpwoodFuncID;	//#4258 ny variabel
	// Bind data members to propertygrid
	CString m_sSawlogVolumeUnit;
	CString m_sPulpwoodVolumeUnit;
	CString m_sDeductionType;
	CString m_sSpeciesCode;
	CString m_sLogGrade;
	CString m_sLoadType;
	long m_nFrequency;
	double m_fLogLengthInch;
	double m_fLogLengthMM;
	CString m_sUseSouthernDoyle;
	CString m_sRoundScaling;
	CString m_sPoundsPerCubicFoot;
	CString m_sBarkRatio;
	CString m_sTaperIB;
	double m_fTaperIB;

	CMyExtStatic m_lbl22_1;
	CMyExtStatic m_lbl22_2;
	CXTListCtrl m_list22_1;

	//CCalcTypes m_recCalcTypes;
	//CVecCalcTypes m_vecCalcTypes;
	vecFuncDesc m_vecFuncDesc;
	CVecUserVolTables m_vecUserVolTables;
	CFuncDesc m_recFuncDesc;

	CGrades m_recGrades;
	CVecGrades m_vecGrades;

	CSpecies m_recSpecies;
	CVecSpecies m_vecSpecies;

	CDefaults m_recDefaults;
	CVecDefaults m_vecDefaults;

	vecDefUserVolFuncs m_vecDefUserVolFuncs;

	CStringArray m_sarrDeductionTypes;
	CStringArray m_sarrLoadTypes;

	CDBHandling *m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;

	BOOL m_bEnableToolBarBtnSave;
	BOOL m_bEnableToolBarBtnDelete;

	// Methods
	void setupComponents();
	void setupPropGrid();
	void setupUserFuncList();

	CString getCalcTypeValue(LPCTSTR basis_value);
	CString getCalcTypeValue(int func_id);	//#4258

	BOOL checkNumberOfUserFunctionsSelected(BOOL show_msg);
	void setupUserVolFuncSelected();
	CDefUserVolFuncs getSelUserFunc(int func_id);

	BOOL Save();
public:
	CDefaultsView();           // protected constructor used by dynamic creation
	virtual ~CDefaultsView();

public:
	enum { IDD = IDD_FORMVIEW22 };

	BOOL doSave();

#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	//{{AFX_VIRTUAL(CDefaultsView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CDefaultsView)
	afx_msg void OnDestroy(void);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnCommand(UINT nID);
	afx_msg void OnUpdateToolbar(CCmdUI* pCmdUI);

	afx_msg void OnItemchangedList(NMHDR* pNMHDR, LRESULT* pResult);

	afx_msg LRESULT OnGridNotify(WPARAM, LPARAM);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

#endif
