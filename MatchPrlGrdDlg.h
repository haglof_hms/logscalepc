#pragma once

#include "resource.h"

#include "DBHandling.h"

// CMatchPrlGrdDlg dialog

class CMatchPrlGrdDlg : public CXTResizeDialog
{
	DECLARE_DYNAMIC(CMatchPrlGrdDlg)
	BOOL m_bInitialized;
	CString m_sLangFN;

	CString m_sConstraintAdd;
	CString m_sMissmatch;
	CString m_sAllOK;

	CMyExtStatic m_lbl20_1;

	CButton m_btnOK;
	CButton m_btnCancel;

	CVecSpecies m_vecSpecies;
	CVecGrades m_vecGrades;
	CVecSpecies m_vecImportSpecies;
	CVecGradesExt m_vecImportGrades;

	CMyReportControl m_repSpecies;
	CMyReportControl m_repGrades;

	CString m_sData;

	int addSpeciesConstraits(CSpecies rec);
	int addGradesConstraits(CGradesExt rec);

	CString getSpcCodeAndName(int spc_id);
	CString getGradesCodeAndName(int grade_id);

	void getMissmatchGrades(int grade_id,LPCTSTR spc_code,CGradesExt& rec);

	BOOL m_bSpcSelected;
	BOOL m_bGradesSelected;

	CDBHandling *m_pDB;

	void setupReport(void);
	void populateReport(void);

public:
	CMatchPrlGrdDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CMatchPrlGrdDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG20 };

	void setDB(CDBHandling *db)	{ m_pDB = db; }

	void setSpecies(CVecSpecies &vec)	{ m_vecSpecies = vec; }
	void setGrades(CVecGrades &vec)	{ m_vecGrades = vec; }

	void setImportSpecies(CVecSpecies &vec)	{ m_vecImportSpecies = vec; }
	void setImportGrades(CVecGradesExt &vec)	{ m_vecImportGrades = vec; }

	CString getData()	{ return m_sData; }

protected:
	//{{AFX_VIRTUAL(CCorrectionDlg)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CMatchPrlGrdDlg)
	afx_msg void OnReportSettingSpc(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportSettingGrd(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	//{{AFX_MSG(CMatchPrlGrdDlg)

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};
