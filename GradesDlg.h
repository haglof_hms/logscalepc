#pragma once

#include "dbhandling.h"

#include "Resource.h"

///////////////////////////////////////////////////////////////////////////////////////////
// CGradesDlgDoc

class CGradesDlgDoc : public CDocument
{
protected: // create from serialization only
	CGradesDlgDoc();
	DECLARE_DYNCREATE(CGradesDlgDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGradesDlgDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CGradesDlgDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CGradesDlgDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
/////////////////////////////////////////////////////////////////////////////
// class CGradesDlgFrame

class CGradesDlgFrame : public CChildFrameBase
{
//private:

	DECLARE_DYNCREATE(CGradesDlgFrame)
	CXTPToolBar m_wndToolBar;
	CXTPDockingPaneManager m_paneManager;
	CString m_sLangFN;

	BOOL m_bFirstOpen;
	BOOL m_bInitReports;
	BOOL m_bIsPrintOutTBtn;

	BOOL m_bSysCommand;

	void setNavBarButtons()
	{
		// Send messages to HMSShell, disable buttons on toolbar; 120122 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

		// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 120122 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
	}

protected:
	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	HICON m_hIcon;
public:
	CGradesDlgFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

// Attributes

// Operations

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	virtual ~CGradesDlgFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	
	//{{AFX_MSG(CGradesDlgFrame)
	afx_msg int OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl);
	afx_msg void OnSysCommand(UINT nID,LPARAM lParam);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnClose();
	afx_msg void OnDestroy(void);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);

	//}}AFX_MSG
	
	DECLARE_MESSAGE_MAP()
};


// CGradesDlgView dialog

class CGradesDlgView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CGradesDlgView)
	// Data members
	BOOL m_bInitialized;
	BOOL m_bOKToSave;
	CString m_sLangFN;
	CDBHandling *m_pDB;
	CVecGrades m_vecGrades;

	CString m_sMsgCap;
	CString m_sMsgDelete1;
	CString m_sMsgDelete2;
	CString m_sMsgGrades1;
	CString m_sMsgGrades2;

	CString m_sMsgNoDelete;

	BOOL m_bEnableAdd;
	BOOL m_bEnableDelete;
	BOOL m_bEnableSave;

	CVecPricelist m_vecVecPricelist;

	CMyReportControl m_repGrades;
	DB_CONNECTION_DATA m_dbConnectionData;
	
	// Methods

	void setupReport();

	void populateReport();

	void Add();
	void Delete();
	BOOL Save(BOOL populate,BOOL do_check);

	BOOL Check();

	BOOL isGradesCodeUnique(int row,LPCTSTR value);

	BOOL CheckDuplicate(LPCTSTR grade_code,int row);

public:
	CGradesDlgView(CWnd* pParent = NULL);   // standard constructor
	virtual ~CGradesDlgView();

// Dialog Data
	enum { IDD = IDD_FORMVIEW13 };

	BOOL doSave(BOOL populate)	{ return Save(populate,TRUE); }
protected:
	//{{AFX_VIRTUAL(CGradeDlgView)
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CGradeDlgView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy(void);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnReportValueChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/);

	afx_msg void OnCommand(UINT nID);
	afx_msg void OnUpdateToolbar(CCmdUI* pCmdUI);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};
