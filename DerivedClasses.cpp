#include "stdafx.h"

#include "DerivedClasses.h"


//////////////////////////////////////////////////////////////////////////
// CMyReportControl; derived from CXTPReportControl
IMPLEMENT_DYNCREATE(CMyReportControl, CXTPReportControl)

BEGIN_MESSAGE_MAP(CMyReportControl, CXTPReportControl)
	ON_WM_CHAR()
	ON_WM_KEYUP()
	ON_WM_LBUTTONUP()
END_MESSAGE_MAP()


CMyReportControl::CMyReportControl()
	: CXTPReportControl()
{
	bIsDirty = FALSE;
}

// Copy constructor
CMyReportControl::CMyReportControl(const CMyReportControl &c)
{
	*this = c;
}

BOOL CMyReportControl::Create(CWnd* pParentWnd,UINT nID,LPCTSTR name,BOOL add_hscroll,BOOL run_metrics)
{

	CXTPReportControl::Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_TABSTOP,
														CRect(0,0,0,0),
														pParentWnd,
														nID);

	if (add_hscroll)
	{
		GetReportHeader()->SetAutoColumnSizing( FALSE );
		EnableScrollBar(SB_HORZ, TRUE );
	}
	else
	{
		GetReportHeader()->SetAutoColumnSizing( TRUE );
		EnableScrollBar(SB_HORZ, FALSE );
	}

	sComponentName = name;

	EnableScrollBar(SB_VERT, TRUE );
	return TRUE;
}

void CMyReportControl::OnChar(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	bIsDirty = TRUE;
	CXTPReportControl::OnChar(nChar,nRepCnt,nFlags);

}

void CMyReportControl::OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	bIsDirty = TRUE;
	CXTPReportControl::OnKeyUp(nChar,nRepCnt,nFlags);
}

void CMyReportControl::OnLButtonUp(UINT flag,CPoint p)
{
	CXTPReportControl::OnLButtonUp(flag,p);
}


////////////////////////////////////////////////////////////////////////////////////////////////////////
//
IMPLEMENT_XTP_CONTROL( CMyControlPopup, CXTPControlPopup)

CMyControlPopup::CMyControlPopup(void)
{
	vecMenuItems.clear();
}

BOOL CMyControlPopup::OnSetPopup(BOOL bPopup)
{
	// Make sure there's any menuitems to add; 080425 p�d
	if (bPopup && vecMenuItems.size() > 0)
	{
		CMenu menu;
		VERIFY(menu.CreatePopupMenu());
		for (UINT i = 0;i < vecMenuItems.size();i++)
		{
			_menu_items item = vecMenuItems[i];
			// create menu items
			if (item.m_nID > -1)		
				menu.AppendMenuW(MF_STRING, item.m_nID,(vecMenuItems[i].m_sText));
			else if (item.m_nID == -1)		
				menu.AppendMenuW(MF_SEPARATOR, -1, _T(""));
		}
		SetCommandBar(&menu);
		menu.DestroyMenu();
	}
	return CXTPControlPopup::OnSetPopup(bPopup);
}

void CMyControlPopup::addMenuIDAndText(int id,LPCTSTR text)
{
	vecMenuItems.push_back(_menu_items(id,text));
}

