// CRegisterSourceIDsView form view

#if !defined(__REGISTERSOURCEIDSVIEW_H__)
#define __REGISTERSOURCEIDSVIEW_H__

#pragma once

#include "stdafx.h"

#include "dbhandling.h"

#include "Resource.h"

#include "ReportClasses.h"

///////////////////////////////////////////////////////////////////////////////////////////
// CRegisterSourceIDsDoc

class CRegisterSourceIDsDoc : public CDocument
{
protected: // create from serialization only
	CRegisterSourceIDsDoc();
	DECLARE_DYNCREATE(CRegisterSourceIDsDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRegisterSourceIDsDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CRegisterSourceIDsDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CRegisterSourceIDsDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
/////////////////////////////////////////////////////////////////////////////
// class CRegisterSourceIDsFrame

class CRegisterSourceIDsFrame : public CChildFrameBase
{
//private:

	DECLARE_DYNCREATE(CRegisterSourceIDsFrame)
	CXTPToolBar m_wndToolBar;
	CXTPDockingPaneManager m_paneManager;
	CString m_sLangFN;

	BOOL m_bFirstOpen;
	BOOL m_bInitReports;

	BOOL m_bIsSysCommand;

	void setNavBarButtons()
	{
		// Send messages to HMSShell, disable buttons on toolbar; 120122 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

		// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 120122 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
	}

protected:
	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	HICON m_hIcon;
public:
	CRegisterSourceIDsFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

// Attributes

// Operations

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	virtual ~CRegisterSourceIDsFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	
	//{{AFX_MSG(CRegisterSourceIDsFrame)
	afx_msg int OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl);
	afx_msg void OnSysCommand(UINT nID,LPARAM lParam);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnClose();
	afx_msg void OnDestroy(void);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);

	//}}AFX_MSG
	
	DECLARE_MESSAGE_MAP()
};


// CRegisterSourceIDsView form view

class CRegisterSourceIDsView : public CXTResizeFormView //CXTResizeFormView
{
	DECLARE_DYNCREATE(CRegisterSourceIDsView)

	// Data members
	BOOL m_bInitialized;
	CString m_sLangFN;

	CString m_sMsgCap;
	CString m_sMsgDelete1;
	CString m_sMsgDelete2;
	CString m_sMsgMandatoryDataMissing1;
	CString m_sMsgMandatoryDataMissing2;
	CString m_sMsgBuyerNameAlreadyUsed1;
	CString m_sMsgBuyerNameAlreadyUsed2;
	CString m_sMsgInTemplates1;
	CString m_sMsgInTemplates2;

	CMyReportControl m_repRegister;
	CMyExtStatic m_lbl19_1;

	CDBHandling *m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;

	vecLogScaleTemplates m_vecLogScaleTemplates;
	CVecRegister m_vecRegister;
	int m_nSelectedType;

	BOOL m_bEnableToolBarBtnSave;
	BOOL m_bEnableToolBarBtnDelete;

	BOOL m_bOnlySave;

	// Methods
	void setupReport();
	void populateReport();

	void New();
	void Delete();
	QUIT_TYPES::Q_T_RETURN Save(CHECK_SAVE_TYPES::CHECK_SAVE cs);

	QUIT_TYPES::Q_T_RETURN isNameOK(int row,LPCTSTR name);
	QUIT_TYPES::Q_T_RETURN checkDataBeforeSave(CRegisterReportRec *rec, CHECK_SAVE_TYPES::CHECK_SAVE cs);
public:
	CRegisterSourceIDsView();           // protected constructor used by dynamic creation
	virtual ~CRegisterSourceIDsView();

public:
	enum { IDD = IDD_FORMVIEW19 };

	QUIT_TYPES::Q_T_RETURN doSave(CHECK_SAVE_TYPES::CHECK_SAVE cs);

#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	//{{AFX_VIRTUAL(CRegisterSourceIDsView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CRegisterSourceIDsView)
	afx_msg void OnDestroy(void);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnCommand(UINT nID);
	afx_msg void OnUpdateToolbar(CCmdUI* pCmdUI);
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);
	afx_msg void OnReportValueChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};




#endif