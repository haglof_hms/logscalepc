// ChangeStatusDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ChangeStatusDlg.h"

#include "ResLangFileReader.h"

// CChangeStatusDlg dialog

IMPLEMENT_DYNAMIC(CChangeStatusDlg, CXTResizeDialog)



BEGIN_MESSAGE_MAP(CChangeStatusDlg, CXTResizeDialog)
	ON_CBN_SELCHANGE(IDC_COMBO15_1, &CChangeStatusDlg::OnCbnSelchangeCombo151)
END_MESSAGE_MAP()

CChangeStatusDlg::CChangeStatusDlg(CWnd* pParent /*=NULL*/)
	: CXTResizeDialog(CChangeStatusDlg::IDD, pParent),
		m_bInitialized(FALSE),
		m_sLangFN(L""),
		m_logStatus(LOGSTATUS::NO_STATUS)
{

}

CChangeStatusDlg::~CChangeStatusDlg()
{
}

BOOL CChangeStatusDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeDialog::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}


void CChangeStatusDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL15_1, m_lbl15_1);
	DDX_Control(pDX, IDC_LBL15_2, m_lbl15_2);

	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);

	DDX_Control(pDX, IDC_COMBO15_1, m_cbox15_1);

	//}}AFX_DATA_MAP
}

// CDefaultsDlg message handlers
BOOL CChangeStatusDlg::OnInitDialog()
{
	CXTResizeDialog::OnInitDialog();
	if (!m_bInitialized)
	{

		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);


		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				tokenizeString(xml.str(IDS_STRING203),';',m_sarrStatus);

				m_lbl15_2.SetBkColor(INFOBK);

				SetWindowText(xml.str(IDS_STRING5500));
				m_lbl15_1.SetWindowText(xml.str(IDS_STRING5501));
				m_lbl15_2.SetWindowText(xml.str(IDS_STRING5502));

				m_btnOK.SetWindowText(xml.str(IDS_STRING100));
				m_btnCancel.SetWindowText(xml.str(IDS_STRING101));			
				m_btnOK.EnableWindow(FALSE);
			
			}
		}

		// Add to combobox
		m_cbox15_1.ResetContent();
		m_cbox15_1.AddString(m_sarrStatus.GetAt(0));	// In stock
		m_cbox15_1.AddString(m_sarrStatus.GetAt(3));	// Scrap
		m_cbox15_1.AddString(m_sarrStatus.GetAt(4));	// Locked

		m_bInitialized = TRUE;
	}

	return TRUE;
}


// CChangeStatusDlg message handlers

LOGSTATUS::STATUS CChangeStatusDlg::getStatus()
{
	return m_logStatus;
}

void CChangeStatusDlg::OnCbnSelchangeCombo151()
{
	m_btnOK.EnableWindow(m_cbox15_1.GetCurSel() > CB_ERR);
	int nIdx = m_cbox15_1.GetCurSel();
	m_logStatus = LOGSTATUS::NO_STATUS;
	switch (nIdx)
	{
		case 0 : m_logStatus = LOGSTATUS::IN_STOCK; break;
		case 1 : m_logStatus = LOGSTATUS::SCRAP; break;
		case 2 : m_logStatus = LOGSTATUS::LOCKED; break;
	};

}
