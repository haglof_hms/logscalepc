// LogScaleView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "LogScaleView.h"

#include "fileparser.h"

#include "logscalevolume.h"

#include "ResLangFileReader.h"

#include "RegisterDlg.h"	// �ven doc och frame
#include "SpeciesDlg.h"
#include "GradesDlg.h"
#include "PricelistDlg.h"
#include "DefaultsDlg.h"	// �ven doc och frame

#include "ViewProgress.h"

//#include "monthcaldlg.h"

#include "CorrectionDlg.h"
#include "TicketDlg.h"

#include "SelectTemplateDlg.h"

#include "TicketLogsFormView.h"

#include "TemplateInfoDlg.h"

#include "TagNoMatchDlg.h"

#include "ExcelExportDlg.h"

#include "libxl.h"
#include "TicketExport.h"


using namespace libxl;

extern CString m_sShellDataFile;

class CLogScaleFrame;

/////////////////////////////////////////////////////////////////////////////
// CTicketReportFilterEditControl

IMPLEMENT_DYNCREATE(CTicketReportFilterEditControl, CXTPReportFilterEditControl)

BEGIN_MESSAGE_MAP(CTicketReportFilterEditControl, CXTPReportFilterEditControl)
	ON_WM_KEYUP()
END_MESSAGE_MAP()

void CTicketReportFilterEditControl::OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	CString S;
	CLogScaleFrame* pWnd = (CLogScaleFrame *)getFormViewParentByID(IDD_FORMVIEW);
	if (pWnd != NULL)
	{
		GetWindowText(S);
		pWnd->setEnableFilterOff(S != "");
	}

	CXTPReportFilterEditControl::OnKeyUp(nChar,nRepCnt,nFlags);
}

////////////////////////////////////////////////////////////////////////////
// CACBox; used in Toolbar for CMDIStandEntryFormFrame to hold
// e.g. Reports; 070323 p�d

BEGIN_MESSAGE_MAP(CACBox, CComboBox)
	//{{AFX_MSG_MAP(CMyExtCBox)
	ON_WM_DESTROY()
	ON_CONTROL_REFLECT_EX(CBN_SELCHANGE, OnCBoxChange)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CACBox::CACBox()
{
	m_fnt1 = new CFont();
}

void CACBox::OnDestroy()
{
	if (m_fnt1 != NULL)
		delete m_fnt1;
  
	CComboBox::OnDestroy();
}

void CACBox::setLanguageFN(LPCTSTR lng_fn)
{
	m_sLangFN = lng_fn;
}

void CACBox::setSTDReportsInCBox(LPCTSTR shell_data_file,LPCTSTR add_to,int index)
{
	CString sStr;
	if (getSTDReports(shell_data_file,add_to,index,m_vecReports))
	{
		ResetContent();
		for (UINT i = 0;i < m_vecReports.size();i++)
		{
			sStr = m_vecReports[i].getCaption();
			// Check if there's a Caption for this report
			// item. If not try to read the String in language-
			// file, for this Suite/Module; 070410 p�d
			if (sStr.IsEmpty())
			{
				sStr = getLangStr(m_vecReports[i].getStrID());
			}
			AddString(sStr);

		}	// for (UINT i = 0;i < m_vecReports.size();i++)
	}	// if (getSTDReports(m_vecReports))
}

BOOL CACBox::OnCBoxChange()
{
/*
	CMDIStandEntryFormFrame *pForm = (CMDIStandEntryFormFrame *)getFormViewParentByID(ID_VIEW_5000);
	if (pForm != NULL)
	{
		pForm->SendMessage(MSG_IN_SUITE,GetCurSel());
	}
*/
	return FALSE;
}

void CACBox::SetLblFont(int size,int weight,LPCTSTR font_name)
{
	LOGFONT lf;
	memset(&lf,0,sizeof(LOGFONT));
	lf.lfHeight = size;
	lf.lfWeight = weight;
	if (font_name != _T(""))
		_tcscpy(lf.lfFaceName,font_name);
	m_fnt1->CreateFontIndirect(&lf);

	SetFont(m_fnt1);
}

CString CACBox::getLangStr(int id)
{
	CString sStr;
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			sStr = xml->str(id);
		}
		delete xml;
	}
	return sStr;
}


///////////////////////////////////////////////////////////////////////////////////////////
// CLogScaleDoc


IMPLEMENT_DYNCREATE(CLogScaleDoc, CDocument)

BEGIN_MESSAGE_MAP(CLogScaleDoc, CDocument)
	//{{AFX_MSG_MAP(CLogScaleDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLogScaleDoc construction/destruction

CLogScaleDoc::CLogScaleDoc()
{
	// TODO: add one-time construction code here

}

CLogScaleDoc::~CLogScaleDoc()
{
}


BOOL CLogScaleDoc::OnNewDocument()
{
	// CHECK FOR LICENSE HERE!!!!! 2011-08-24 P�D
	if (!License())
	{
		return FALSE;
	}

	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CLogScaleDoc serialization

void CLogScaleDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CLogScaleDoc diagnostics

#ifdef _DEBUG
void CLogScaleDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CLogScaleDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


///////////////////////////////////////////////////////////////////////////////////////////
// CLogScaleFrame

IMPLEMENT_DYNCREATE(CLogScaleFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CLogScaleFrame, CMDIChildWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_SYSCOMMAND()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)

	ON_COMMAND(ID_TBBTN_PRINT, OnTBBtnPrintOut)
	ON_UPDATE_COMMAND_UI(ID_TBBTN_PRINT, OnUpdatePrintOutTBtn)

	ON_CONTROL(CBN_SELCHANGE,ID_TBBTN_REPORTCBOX, OnPrintOutCBox)

	ON_UPDATE_COMMAND_UI(ID_TBBTN_DEL_FILTER, OnUpdateFilterOff)
//	ON_COMMAND(ID_TOOLS_TAG_MATCH ,OnMatchTags)

	ON_XTP_CREATECONTROL()
END_MESSAGE_MAP()


// CLogScaleFrame construction/destruction

XTPDockingPanePaintTheme CLogScaleFrame::m_themeCurrent = xtpPaneThemeOffice2003;

CLogScaleFrame::CLogScaleFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
	m_bInitReports = FALSE;
	//m_sShellDataFile = L"";
	m_nShellDataIndex = -1;
	m_bIsPrintOutTBtn = FALSE;
	m_bIsFilterOffTBtn = FALSE;
}

CLogScaleFrame::~CLogScaleFrame()
{
}

void CLogScaleFrame::OnDestroy(void)
{
	if (m_fnt1)
		delete m_fnt1;
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6001_KEY);
	SavePlacement(this, csBuf);
}

void CLogScaleFrame::OnClose(void)
{
	CMDIChildWnd::OnClose();
}

void CLogScaleFrame::OnSysCommand(UINT nID,LPARAM lParam)
{
	if ((nID & 0xFFF0) == SC_CLOSE)
	{
		CLogScaleView *pView = (CLogScaleView*)getFormViewByID(IDD_FORMVIEW);
		if (pView != NULL)
		{
			// If user says NO, don't close LogScale
//			if (pView->Save(false))
//			{
				CMDIChildWnd::OnSysCommand(nID,lParam);
//			}
		}
	
	}
	else
	{
		CMDIChildWnd::OnSysCommand(nID,lParam);
	}
}

int CLogScaleFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	EnableDocking(CBRS_ALIGN_ANY);

	// Create and Load toolbar; 090107 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR1);
	m_wndToolBar.EnableDocking(xtpFlagAlignTop|xtpFlagStretched);

	// Initialize dialog bar m_wndFieldSelectionDlg
	if (!m_wndFieldSelectionDlg.Create(this, IDD_FIELD_SELECTION,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN_SELECTION_DLG))
	{
		return -1;      // fail to create
	}

	// Initialize dialog bar m_wndFilterEdit
	if (!m_wndFilterEdit.Create(this, IDD_FILTEREDIT20,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN_FILTER))
	{
		return -1;      // fail to create
	}

	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_wndFieldSelectionDlg.SetWindowText(xml.str(IDS_STRING1100));
			m_wndFilterEdit.SetWindowText(xml.str(IDS_STRING5950));

			//***************************************************************************************
			// Setup commandbars and manues; 051114 p�d
			CXTPToolBar* pToolBar = &m_wndToolBar;
			if (pToolBar->IsBuiltIn())
			{
				if (pToolBar->GetType() != xtpBarTypeMenuBar)
				{
					UINT nBarID = pToolBar->GetBarID();
					pToolBar->LoadToolBar(nBarID, FALSE);
					CXTPControls *p = pToolBar->GetControls();
					// Setup icons on toolbars, using resource dll; 051208 p�d
					if (nBarID == IDR_TOOLBAR1)
					{		
						setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_NEW,xml.str(IDS_STRING1103),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(1),RES_TB_SAVE,xml.str(IDS_STRING1104),TRUE);	//
						p->GetAt(1)->SetVisible(FALSE);
						setToolbarBtn(sTBResFN,p->GetAt(2),RES_TB_DEL,xml.str(IDS_STRING1105),TRUE);	//

						setToolbarBtn(sTBResFN,p->GetAt(3),RES_TB_IMPORT,xml.str(IDS_STRING1102),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(4),RES_TB_EXPORT,xml.str(IDS_STRING1107),TRUE);	//

						setToolbarBtn(sTBResFN,p->GetAt(5),RES_TB_PRINT,L"CBOX",TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(6),RES_TB_PRINT,xml.str(IDS_STRING1106),TRUE);	//

						setToolbarBtn(sTBResFN,p->GetAt(7),RES_TB_FILTER,xml.str(IDS_STRING1108),TRUE);	//
						//p->GetAt(7)->SetVisible(FALSE);
						setToolbarBtn(sTBResFN,p->GetAt(8),RES_TB_FILTER_OFF,xml.str(IDS_STRING1109),TRUE);	//
						//p->GetAt(8)->SetVisible(FALSE);

						setToolbarBtn(sTBResFN,p->GetAt(9),RES_TB_TOOLS,xml.str(IDS_STRING1110),TRUE);	//
						p->GetAt(9)->SetVisible(FALSE);
					}	// if (nBarID == IDR_TOOLBAR1)
				}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
			}	// if (pToolBar->IsBuiltIn())
			//***************************************************************************************

			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

	// docking for field chooser
	m_wndFieldSelectionDlg.EnableDocking(0);

	// Don't show fieldchooser at this stadge
	ShowControlBar(&m_wndFieldSelectionDlg, FALSE, FALSE);
	FloatControlBar(&m_wndFieldSelectionDlg, CPoint(100, GetSystemMetrics(SM_CYSCREEN) / 3));

	// docking for filter editing
	m_wndFilterEdit.EnableDocking(CBRS_ALIGN_TOP);

	ShowControlBar(&m_wndFilterEdit, FALSE, FALSE);
	FloatControlBar(&m_wndFilterEdit, CPoint(400, GetSystemMetrics(SM_CYSCREEN) / 3));

	m_bFirstOpen = TRUE;
	
	// We'll chack if the directory for columnsettings is created
	CString sDirectory;
	sDirectory.Format(L"%s\\%s",getMyDocumentsDir(),LOGS_COLUMNS_DIRECTORY);
	// Check if directory exists, on disk. If not create it; 080205 p�d
	if (!isDirectory(sDirectory))
	{
		createDirectory(sDirectory);
	}

	return 0; // creation ok
}

int CLogScaleFrame::OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl)
{

	if (lpCreateControl->nID == ID_TBBTN_SETTINGS)
	{
		CMyControlPopup *m_pToolsPopup = new CMyControlPopup();
		m_pToolsPopup->SetStyle(xtpButtonIcon);
		if (fileExists(m_sLangFN))
		{
			RLFReader xml; // = new RLFReader;
			if (xml.Load(m_sLangFN))
			{
				m_pToolsPopup->addMenuIDAndText(ID_TOOLS_PRICELIST,xml.str(IDS_STRING1113));
				m_pToolsPopup->addMenuIDAndText();	// Add separator
				m_pToolsPopup->addMenuIDAndText(ID_TOOLS_REGISTER,xml.str(IDS_STRING1114));
				m_pToolsPopup->addMenuIDAndText(ID_TOOLS_DEFAULTS,xml.str(IDS_STRING1115));
			}	// if (xml->Load(m_sLangFN))
			xml.clean();
		}

		lpCreateControl->pControl = m_pToolsPopup;
		return TRUE;
	}

	if (lpCreateControl->nID == ID_TBBTN_EXPORT)
	{
		CMyControlPopup *m_pToolsPopup = new CMyControlPopup();
		m_pToolsPopup->SetStyle(xtpButtonIcon);
		if (fileExists(m_sLangFN))
		{
			RLFReader xml; // = new RLFReader;
			if (xml.Load(m_sLangFN))
			{
				m_pToolsPopup->addMenuIDAndText(ID_EXPORT_EXCEL,xml.str(IDS_STRING11070));
				//m_pToolsPopup->addMenuIDAndText();	// Add separator
				//m_pToolsPopup->addMenuIDAndText(ID_EXPORT_TMPL,xml.str(IDS_STRING11071));
			}	// if (xml->Load(m_sLangFN))
			xml.clean();
		}

		lpCreateControl->pControl = m_pToolsPopup;
		return TRUE;
	}

	if (lpCreateControl->nID == ID_TBBTN_REPORTCBOX)
	{
		if (!m_cboxPrintOut.Create(WS_CHILD|WS_VISIBLE|CBS_DROPDOWNLIST|WS_CLIPCHILDREN,CRect(0,0,0,200), this, ID_TBBTN_REPORTCBOX) )
		{
			AfxMessageBox(_T("ERROR:\nOnCreateControl"));
		}
		else
		{
			m_fnt1 = new CFont();
			LOGFONT lf;
			memset(&lf,0,sizeof(LOGFONT));
			lf.lfHeight = 16;
			lf.lfWeight = FW_NORMAL;
			m_fnt1->CreateFontIndirect(&lf);


			m_cboxPrintOut.SetOwner(this);
			m_cboxPrintOut.MoveWindow(45, 3, 150, 20);
			m_cboxPrintOut.SetFont(m_fnt1);

	    CXTPControlCustom * pCB = CXTPControlCustom::CreateControlCustom(&m_cboxPrintOut);

			lpCreateControl->buttonStyle = xtpButtonIconAndCaption;
      lpCreateControl->pControl = pCB;
				
		}
		return TRUE;
	}

	return FALSE;
}


BOOL CLogScaleFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CLogScaleFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CLogScaleFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6001_KEY);
		LoadPlacement(this, csBuf);
  }

}

void CLogScaleFrame::OnSetFocus(CWnd* pWnd)
{
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,ID_DO_SOMETHING_IN_SHELL,ID_LPARAM_COMMAND2);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CMDIChildWnd::OnSetFocus(pWnd);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CLogScaleFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	CString sStr = L"";
	if (wParam == ID_DBNAVIG_LIST)
	{
//		showFormView(IDD_REPORTVIEW3,m_sLangFN);
	}
	else	if (wParam == (ID_DO_SOMETHING_IN_SHELL + ID_LPARAM_COMMAND2))
	{
		if (!m_bInitReports)
		{
			// The return message holds a _user_msg structure, collected in the
			// OnMessageFromShell( WPARAM wParam, LPARAM lParam ); 070410 p�d
			// In this case the szFileName item in _user_msg structure holds
			// the path and filename of the ShellData file used and the szArgStr
			// holds the Suite/UserModule name; 070410 p�d
			_user_msg *msg = (_user_msg*)lParam;
			if (msg != NULL)
			{
				m_sShellDataFile = msg->getFileName();
				//m_nShellDataIndex = msg->getIndex();
				m_nShellDataIndex = 6001; // explicit set Identifer, macth id in ShellData file; 090212 p�d
				//m_cboxPrintOut.setLanguageFN(m_sLangFN);
				getSTDReports(m_sShellDataFile,PROGRAM_NAME,m_nShellDataIndex,m_vecReports);
				m_cboxPrintOut.ResetContent();
				RLFReader xml; // = new RLFReader;
				if (xml.Load(m_sLangFN))
				{

					for (UINT i = 0;i < m_vecReports.size();i++)
					{
						if (m_vecReports[i].getCaption().IsEmpty())
							sStr = xml.str(m_vecReports[i].getStrID()); 
						else
							sStr = m_vecReports[i].getCaption();
						m_cboxPrintOut.AddString(sStr);
					}
				}
			}	// if (msg != NULL)

			m_bInitReports = TRUE;
		}
	}
	return 0L;
}


void CLogScaleFrame::OnUpdatePrintOutTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsPrintOutTBtn );
}

void CLogScaleFrame::OnUpdateFilterOff(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsFilterOffTBtn );
}

void CLogScaleFrame::OnTBBtnPrintOut()
{
	CString sReportPathAndFN = L"";
	CString sFileExtension = L"";
	CString sArgStr = L"";
	int nTractID = -1; // LoadID
	getSTDReports(m_sShellDataFile,PROGRAM_NAME,m_nShellDataIndex,m_vecReports);

	int nIdx = m_cboxPrintOut.GetCurSel();

	if (nIdx > -1 && m_vecReports.size() > 0)
	{

		sArgStr = L"";
		CLogScaleView *pView = (CLogScaleView*)getFormViewByID(IDD_FORMVIEW);
		if (pView != NULL)
		{
			nTractID = pView->getActiveLoadID();
			sArgStr.Format(_T("%d;"),nTractID);
		}

		sReportPathAndFN.Format(_T("%s%s\\%s"),
														getReportsDir(),
														getLangSet(),
														m_vecReports[nIdx].getFileName());
		if (fileExists(sReportPathAndFN))
		{
			AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,WM_USER+4,
				(LPARAM)&_user_msg(333,	// ID = 333 for CrystalReport, ID = 300 for FastReports
				_T("OpenSuiteEx"),			// Exported/Imported function
				_T("Reports2.dll"),			// Suite to call; Report2.dll = Reportgenerator for CrystalReports (A.G.), Report.dll = Reportgenerator for FastReports
				(sReportPathAndFN),			// Use this report
				(sReportPathAndFN),
				sArgStr));
		}
	}	// if (nIdx > -1 && nIdx < m_vecReports.size())
}

void CLogScaleFrame::OnPrintOutCBox()
{
	m_bIsPrintOutTBtn = m_cboxPrintOut.GetCurSel() > CB_ERR;
}

void CLogScaleFrame::OnMatchTags()
{
}

// CLogScaleFrame diagnostics

#ifdef _DEBUG
void CLogScaleFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CLogScaleFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CLogScaleFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}


// PRIVATE


///////////////////////////////////////////////////////////////////////////////////////////
// CLogScaleView

IMPLEMENT_DYNCREATE(CLogScaleView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CLogScaleView, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_COPYDATA()
	ON_WM_CREATE()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessge)
	ON_NOTIFY(XTP_NM_REPORT_HEADER_RCLICK, ID_REPORT_TICKETS, OnReportItemRClick)
	ON_NOTIFY(NM_CLICK, ID_REPORT_TICKETS, OnReportItemClick)
	ON_NOTIFY(NM_DBLCLK, ID_REPORT_TICKETS, OnReportItemDblClick)
	ON_COMMAND(ID_TBBTN_SELECTION_DLG, OnFieldSelection)

	ON_COMMAND(ID_TBBTN_NEW,OnNewTicket)
	ON_COMMAND(ID_TBBTN_SAVE,OnSaveTicket)
	ON_COMMAND(ID_TBBTN_DEL,OnDeleteTicket)

	ON_COMMAND(ID_TBBTN_IMPORT,OnImportTicket)

	ON_COMMAND(ID_EXPORT_EXCEL,OnExportToExcel)
	ON_COMMAND(ID_EXPORT_TMPL,OnExportTemplate)

	ON_COMMAND(ID_TBBTN_FILTER,OnFilter)
	ON_COMMAND(ID_TBBTN_DEL_FILTER,OnFilterOff)

	ON_COMMAND_RANGE(ID_TOOLS_SPECIES,ID_TOOLS_DEFAULTS,OnSettings)
END_MESSAGE_MAP()

CLogScaleView::CLogScaleView()
	: CXTResizeFormView(CLogScaleView::IDD)
{
	m_bInitialized = FALSE;
	m_nSelLoadID = -1;
	m_bIsTicketNumOK = TRUE;
	m_nSelectedColumn = -1;
}

CLogScaleView::~CLogScaleView()
{
}
// CLogScaleView diagnostics

#ifdef _DEBUG
void CLogScaleView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CLogScaleView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG


void CLogScaleView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}

void CLogScaleView::OnDestroy()
{

// Not used for now; 2011-11-09 p�d
//	setColumnsSettingInReg(REG_TICKETS_COLUMNS_KEY,m_repTickets);
	
	SaveReportState();

	CXTResizeFormView::OnDestroy();
}

void CLogScaleView::OnClose()
{
	CXTResizeFormView::OnClose();
}

int CLogScaleView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;
	return 0;
}

BOOL CLogScaleView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CLogScaleView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();
	//	SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{
		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		CLogScaleFrame* pWnd = (CLogScaleFrame *)getFormViewParentByID(IDD_FORMVIEW);
		if (m_wndSubList.GetSafeHwnd() == NULL)
		{
			m_wndSubList.SubclassDlgItem(IDC_COLUMNS, &pWnd->m_wndFieldSelectionDlg);
			m_repTickets.GetColumns()->GetReportHeader()->SetSubListCtrl(&m_wndSubList);
		}

		if (m_wndFilterEdit.GetSafeHwnd() == NULL)
		{
			m_wndFilterEdit.SubclassDlgItem(IDC_FILTEREDIT20_1, &pWnd->m_wndFilterEdit);
			m_repTickets.GetColumns()->GetReportHeader()->SetFilterEditCtrl(&m_wndFilterEdit);
		}

		if (m_lbl20_1.GetSafeHwnd() == NULL)
		{
			m_lbl20_1.SubclassDlgItem(IDC_FILTERLBL20_1, &pWnd->m_wndFilterEdit);
			m_lbl20_1.SetBkColor(INFOBK);
		}

		if (m_lbl20_2.GetSafeHwnd() == NULL)
		{
			m_lbl20_2.SubclassDlgItem(IDC_FILTERLBL20_2, &pWnd->m_wndFilterEdit);
			m_lbl20_2.SetBkColor(INFOBK);
			m_lbl20_2.SetLblFont(14,FW_BOLD);
		}

		setupReportControl();

		LoadReportState();

		populateReport(false);

		m_bInitialized = TRUE;
	}

}

BOOL CLogScaleView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{

	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		if (m_dbConnectionData.conn->isConnected())
		{
			m_pDB = new CDBHandling(m_dbConnectionData);
			if (m_pDB != NULL)
			{
				m_pDB->getTmplTables(m_vecLogScaleTemplates);		
			}
		}
	}

	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CLogScaleView::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	if (m_repTickets.GetSafeHwnd() != NULL)
	{
		setResize(&m_repTickets,1,1,cx-2,cy-2);
	}
}

LRESULT CLogScaleView::OnSuiteMessge( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}

void CLogScaleView::OnReportItemRClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	ASSERT(pItemNotify->pColumn);
	CPoint ptClick = pItemNotify->pt;

	CMenu menu;
	VERIFY(menu.CreatePopupMenu());

	// create main menu items
	menu.AppendMenu(MF_STRING, ID_SHOW_FIELD_SELECTION, m_sFieldSelection);

	// track menu
	int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN |TPM_RIGHTBUTTON, ptClick.x, ptClick.y, this, NULL);

	// other general items
	switch (nMenuResult)
	{
		case ID_SHOW_FIELD_SELECTION:
			OnFieldSelection();	
		break;
	}
	menu.DestroyMenu();
}

void CLogScaleView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	CRect rect;
	POINT pt;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;

	// prevents us from crashing when clicking inside the report, but not on an item
	if(pItemNotify == NULL) 
		return;

	if(pItemNotify->pColumn != NULL) 
	{
		m_nSelectedColumn = pItemNotify->pColumn->GetIndex();
		//setFilterWindow();	#4164
	}

	// prevents us from crashing when clicking inside the report, but not on an item
	if(pItemNotify->pItem == NULL)
		return;

	switch (pItemNotify->pColumn->GetIndex())
	{

		// �ppna Logs; spara ticketdata och f�ra �ver ticket till logs
		// Changed from COLUMN_5 to COLUMN_0; 110701 P�D
		case COLUMNS::TIC_TICKET_NUM :
			{
				// Do a hit-test; 080513 p�d
				rect = pItemNotify->pColumn->GetRect();		
				pt = pItemNotify->pt;
				// Check if the user clicked on the Icon or not; 080513 p�d
				if (hitTest_X(pt.x,rect.left,13))
				{
					CTickets rec = CTickets();
					if (SaveTicket(rec))
					{
						regSetInt(REG_ROOT,TICKET_MEASURINGMODE_KEY,L"MODE",rec.getMeasuringMode());
						showFormView(IDD_FORMVIEW5,m_sLangFN,(LPARAM)&rec,ID_OPEN_LOGS);
					}	// if (SaveTicket(rec))
				}
			}
			break;
	};

}

void CLogScaleView::OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;

	// prevents us from crashing when clicking inside the report, but not on an item
	if(pItemNotify == NULL) 
		return;

	// prevents us from crashing when clicking inside the report, but not on an item
	if(pItemNotify->pItem == NULL)
		return;

	CTicketsReportRec *pRec = (CTicketsReportRec*)m_repTickets.GetFocusedRow()->GetRecord();
	if (pRec == NULL) 
		return;

	CTickets rec = CTickets(); //pRec->getRecord();
	if (SaveTicket(rec))
	{
		regSetInt(REG_ROOT,TICKET_MEASURINGMODE_KEY,L"MODE",rec.getMeasuringMode());
		showFormView(IDD_FORMVIEW5,m_sLangFN,(LPARAM)&rec,ID_OPEN_LOGS);
	}	// if (SaveTicket(rec))
}


void CLogScaleView::setupReportControl(void)
{
	CXTPReportColumn *pCol = NULL;
	if (m_repTickets.GetSafeHwnd() == 0)
	{

		// Create the sheet1 list box.
		if (!m_repTickets.Create(this, ID_REPORT_TICKETS, L"Tickets_report", TRUE, FALSE))
		{
			TRACE0( "Failed to create m_repTickets.\n" );
			return;
		}
	}

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{

			// String-resource
			m_sFieldSelection = xml.str(IDS_STRING1100);

			m_sMsgCap = xml.str(IDS_STRING99);
			m_sMsgDelete1 = xml.str(IDS_STRING1116);
			m_sMsgDelete2 = xml.str(IDS_STRING1117);
			m_sMsgTicketNumUsed = xml.str(IDS_STRING1118);
			m_sMsgErrNewTicket1 = xml.str(IDS_STRING1119);
			m_sMsgErrUpdTicket1 = xml.str(IDS_STRING1120);
			m_sMsgErrNewTicket2.Format(L"%s\n\n%s\n%s\n\n%s\n\n",xml.str(IDS_STRING1119),xml.str(IDS_STRING1121),xml.str(IDS_STRING1122),xml.str(IDS_STRING1123));
			m_sMsgErrUpdTicket2.Format(L"%s\n\n%s\n%s\n\n%s\n\n",xml.str(IDS_STRING1120),xml.str(IDS_STRING1121),xml.str(IDS_STRING1122),xml.str(IDS_STRING1123));
			m_sMsgOpenEXCEL = xml.str(IDS_STRING1124);
			m_sSaveTemplateCap = xml.str(IDS_STRING1902);
			//m_sMissingTicketData = xml.str(IDS_STRING1125) + L"\n\n" + xml.str(IDS_STRING1126) + L"\n\n" + xml.str(IDS_STRING1127);
			m_sMissingTemplate1 = xml.str(IDS_STRING1137);
			m_sMissingTemplate2 = xml.str(IDS_STRING1138);

			m_sMsgDuplicateTagNumbers.Format(L"%s\n%s\n\n%s\n\n",
				xml.str(IDS_STRING1141),
				xml.str(IDS_STRING1142),
				xml.str(IDS_STRING1143));

			m_sMsgCanNotBeRemovedSale.Format(L"%s\n%s\n\n",
				xml.str(IDS_STRING1144),
				xml.str(IDS_STRING1145));
			
			m_sMsgCanNotBeRemovedSawmill.Format(L"%s\n%s\n\n",
				xml.str(IDS_STRING1146),
				xml.str(IDS_STRING1147));

			m_sLblInch = xml.str(IDS_STRING109);
			m_sLblMM = xml.str(IDS_STRING110);
			m_sLblFeet = xml.str(IDS_STRING111);
			m_sLblDM  = xml.str(IDS_STRING112);
			m_sLblBF  = xml.str(IDS_STRING113);
			m_sLblDM3  = xml.str(IDS_STRING114);
			m_sLblFT3  = xml.str(IDS_STRING115);

			m_sFilterOn	= xml.str(IDS_STRING5951);

			tokenizeString(xml.str(IDS_STRING203),';',m_sarrStatus);
			tokenizeString(xml.str(IDS_STRING201),';',m_sarrLoadType);
			tokenizeString(xml.str(IDS_STRING202),';',m_sarrDeductionType);
			tokenizeString(xml.str(IDS_STRING205),';',m_sarrMeasureTypes);
			
			if (m_repTickets.GetSafeHwnd() != NULL)
			{

				VERIFY(m_ilIcons.Create(16,13, ILC_COLOR24|ILC_MASK, 0, 1));
				CBitmap bmp;
				VERIFY(bmp.LoadBitmap(IDB_BITMAP1));
				m_ilIcons.Add(&bmp, RGB(255, 0, 255));

				m_repTickets.SetImageList(&m_ilIcons);

				m_repTickets.ShowWindow( SW_NORMAL );
				m_repTickets.ShowGroupBy( TRUE );

				// Ticketnumber
				pCol = m_repTickets.AddColumn(new CXTPReportColumn(COLUMNS::TIC_TICKET_NUM, xml.str(IDS_STRING1000), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = FALSE; //TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = FALSE; // TRUE;
				// LoadID
				pCol = m_repTickets.AddColumn(new CXTPReportColumn(COLUMNS::TIC_LOAD_ID, xml.str(IDS_STRING1001), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = FALSE; //TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = FALSE; //TRUE;
				// Date
				pCol = m_repTickets.AddColumn(new CXTPReportColumn(COLUMNS::TIC_SOURCE_DATE, xml.str(IDS_STRING1002), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Numof Logs
				pCol = m_repTickets.AddColumn(new CXTPReportColumn(COLUMNS::TIC_NUMOF_LOGS, xml.str(IDS_STRING1005), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->SetAlignment(DT_CENTER);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Sum volume pricelist
				pCol = m_repTickets.AddColumn(new CXTPReportColumn(COLUMNS::TIC_SUM_VOL_PRICELIST, xml.str(IDS_STRING1007), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Price
				pCol = m_repTickets.AddColumn(new CXTPReportColumn(COLUMNS::TIC_SUM_PRICE, xml.str(IDS_STRING1012), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Weight
				pCol = m_repTickets.AddColumn(new CXTPReportColumn(COLUMNS::TIC_WEIGHT, xml.str(IDS_STRING1006), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = FALSE; //TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = FALSE; //TRUE;
				// Hauler
				pCol = m_repTickets.AddColumn(new CXTPReportColumn(COLUMNS::TIC_HAULER, xml.str(IDS_STRING1003), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Supplier
				pCol = m_repTickets.AddColumn(new CXTPReportColumn(COLUMNS::TIC_SUPPLIER, xml.str(IDS_STRING1027), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Vendor
				pCol = m_repTickets.AddColumn(new CXTPReportColumn(COLUMNS::TIC_VENDOR, xml.str(IDS_STRING1030), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Scaler
				pCol = m_repTickets.AddColumn(new CXTPReportColumn(COLUMNS::TIC_SCALER, xml.str(IDS_STRING1004), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// SourceID
				pCol = m_repTickets.AddColumn(new CXTPReportColumn(COLUMNS::TIC_SOURCE_ID, xml.str(IDS_STRING1013), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Location
				pCol = m_repTickets.AddColumn(new CXTPReportColumn(COLUMNS::TIC_LOCATION, xml.str(IDS_STRING1014), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Load type
				pCol = m_repTickets.AddColumn(new CXTPReportColumn(COLUMNS::TIC_LOAD_TYPE, xml.str(IDS_STRING1018), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Deduction type
				pCol = m_repTickets.AddColumn(new CXTPReportColumn(COLUMNS::TIC_DEDUCTION, xml.str(IDS_STRING1019), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Buyer
				pCol = m_repTickets.AddColumn(new CXTPReportColumn(COLUMNS::TIC_BUYER, xml.str(IDS_STRING1015), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// TrackID
				pCol = m_repTickets.AddColumn(new CXTPReportColumn(COLUMNS::TIC_TRACT_ID, xml.str(IDS_STRING1017), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = FALSE; //TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = FALSE; //TRUE;
				// Taper IB
				pCol = m_repTickets.AddColumn(new CXTPReportColumn(COLUMNS::TIC_TAPER_IB, xml.str(IDS_STRING1021), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = FALSE; //TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = FALSE; //TRUE;
				// Other info
				pCol = m_repTickets.AddColumn(new CXTPReportColumn(COLUMNS::TIC_OTHER_INFO, xml.str(IDS_STRING1016), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = FALSE; //TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = FALSE; //TRUE;
				// GPS coord 1
				pCol = m_repTickets.AddColumn(new CXTPReportColumn(COLUMNS::TIC_GPS_COORD1, xml.str(IDS_STRING1028), 100));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = FALSE; //TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = FALSE; //TRUE;
				// GPS coord 2
				pCol = m_repTickets.AddColumn(new CXTPReportColumn(COLUMNS::TIC_GPS_COORD2, xml.str(IDS_STRING1029), 100));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = FALSE; //TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = FALSE; //TRUE;
				// Pricelist
				pCol = m_repTickets.AddColumn(new CXTPReportColumn(COLUMNS::TIC_PRICELIST, xml.str(IDS_STRING1031), 100));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = FALSE; //TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = FALSE; //TRUE;

				m_repTickets.GetReportHeader()->AllowColumnRemove(TRUE);
				m_repTickets.SetMultipleSelection( FALSE );
				m_repTickets.SetGridStyle( TRUE, xtpReportGridSolid );
				m_repTickets.SetGridStyle( FALSE, xtpReportGridSmallDots );
				m_repTickets.FocusSubItems(TRUE);
				m_repTickets.AllowEdit(TRUE);
				m_repTickets.GetPaintManager()->SetFixedRowHeight(FALSE);

				xml.clean();

				RECT rect;
				GetClientRect(&rect);
				setResize(&m_repTickets,1,1,rect.right-2,rect.bottom-2);

			}	// if (m_repTickets.GetSafeHwnd() != NULL)
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))
}

void CLogScaleView::populateReport(bool focus_row)
{
	CString sStr = L"";
	if (m_pDB != NULL)
	{
		getDLLVolumeFuncDesc(m_vecFuncDesc,m_vecUserVolTables);

		// Get data from database and also add to report
		m_pDB->getTickets(m_vecTickets);
		// Get register data
		m_pDB->getRegister(m_vecRegister);
		// Get species and grades
		m_pDB->getSpecies(m_vecSpecies);
		m_pDB->getGrades(m_vecGrades);
		// Get pricelist
		m_pDB->getPricelist(m_vecPricelist);
		// Get calculation types
//		m_pDB->getCalcTypes(m_vecCalcTypes);
		// get User volume tables
		m_pDB->getUserVolTables(m_vecUserVolTables);

		m_pDB->getDefaults(m_vecDefaults);
/*
		sStr = getDefaults(DEF_LOG_LENGTH_INCH,m_vecDefaults,m_vecFuncDesc);
		if (sStr.CompareNoCase(L"0") == 0)
		{
			sStr = L"0.0";
			CDefaults rec = CDefaults(DEF_LOG_LENGTH_INCH,localeToStr(sStr),localeToStr(sStr));
			m_pDB->updDefaults(rec);
		}

		sStr = getDefaults(DEF_LOG_LENGTH_MM,m_vecDefaults,m_vecFuncDesc);
		if (sStr.CompareNoCase(L"0") == 0)
		{
			sStr = L"0.00";
			CDefaults rec = CDefaults(DEF_LOG_LENGTH_MM,localeToStr(sStr),localeToStr(sStr));
			m_pDB->updDefaults(rec);
		}

		sStr = getDefaults(DEF_TAPER_IB,m_vecDefaults,m_vecFuncDesc);
		if (_tstof(sStr) == 0.0)
		{
			sStr = L"0.125";
			CDefaults rec = CDefaults(DEF_TAPER_IB,localeToStr(sStr),localeToStr(sStr));
			m_pDB->updDefaults(rec);
		}
*/
	}
	

	m_repTickets.ResetContent();
	if (m_vecTickets.size() == 0)
	{
//		m_pDB->resetTicketsIdentityField();
		m_pDB->resetLogsIdentityField();
		m_pDB->resetSelVolFuncsIdentityField();
	}
	else
	{
		for (UINT i = 0;i < m_vecTickets.size();i++)
		{
			m_repTickets.AddRecord(new CTicketsReportRec(m_vecTickets[i]));
		}
		m_repTickets.Populate();
		m_repTickets.UpdateWindow();

		if (focus_row)
			m_repTickets.SetFocusedRow(m_repTickets.GetRows()->GetAt(m_repTickets.GetRows()->GetCount()-1));

	}
/*
	// We'll try to update TicketLogs
	CTicketLogsFormView *pView = (CTicketLogsFormView *)getFormViewByID(IDD_FORMVIEW5);
	if (pView != NULL)
	{
		pView->populateData(TRUE /* Reload register data* /,TRUE /* Save and calculate logs* /);
	}
*/
}

int CLogScaleView::getActiveLoadID()
{
	CXTPReportRow *pRow = m_repTickets.GetFocusedRow();
	if (pRow != NULL)
	{
		CTicketsReportRec *pRec = (CTicketsReportRec*)pRow->GetRecord();
		if (pRec != NULL)
			return pRec->getRecord().getPKID();
	}
	return -1;
}

void CLogScaleView::OnFieldSelection(void)
{
	CLogScaleFrame* pWnd = (CLogScaleFrame *)getFormViewParentByID(IDD_FORMVIEW);
	if (pWnd != NULL)
	{
		BOOL bShow = !pWnd->m_wndFieldSelectionDlg.IsVisible();
		pWnd->ShowControlBar(&pWnd->m_wndFieldSelectionDlg, bShow, FALSE);
	}
}

// Saves a specific ticket (focused row)
BOOL CLogScaleView::SaveTicket(CTickets& rec)
{

	CXTPReportRow* pRow = NULL;
	CTicketsReportRec *pRec = NULL;
	BOOL bOkToSave = TRUE;
	BOOL bOkToClose = TRUE;
	m_repTickets.Populate();
	pRow = m_repTickets.GetFocusedRow();
	if (pRow != NULL && m_pDB != NULL)
	{
		pRec = (CTicketsReportRec *)pRow->GetRecord();
		if (pRec != NULL)
		{
			rec = pRec->getRecord();
			rec.setTicket(pRec->getIconColText(COLUMNS::TIC_TICKET_NUM));	
			if (pRec->getIconColText(COLUMNS::TIC_TICKET_NUM).IsEmpty())
				bOkToSave = FALSE;
			rec.setLoadID(pRec->getColText(COLUMNS::TIC_LOAD_ID));
			rec.setDate(pRec->getColText(COLUMNS::TIC_SOURCE_DATE));
			rec.setHauler(pRec->getColText(COLUMNS::TIC_HAULER));
			rec.setScaler(pRec->getColText(COLUMNS::TIC_SCALER));
			rec.setNumOfLogs(pRec->getColInt(COLUMNS::TIC_NUMOF_LOGS));
			rec.setWeight(pRec->getColFloat(COLUMNS::TIC_WEIGHT));
			rec.setSumVolPricelist(pRec->getColFloat(COLUMNS::TIC_SUM_VOL_PRICELIST));
			rec.setSumPrice(pRec->getColFloat(COLUMNS::TIC_SUM_PRICE));
			rec.setSourceID(pRec->getColText(COLUMNS::TIC_SOURCE_ID));
			rec.setLocation(pRec->getColText(COLUMNS::TIC_LOCATION));
			rec.setBuyer(pRec->getColText(COLUMNS::TIC_BUYER));
			rec.setOtherInfo(pRec->getColText(COLUMNS::TIC_OTHER_INFO));
			rec.setTractID(pRec->getColText(COLUMNS::TIC_TRACT_ID));
			rec.setLoadType(pRec->getColText(COLUMNS::TIC_LOAD_TYPE));
			rec.setDeductionType(pRec->getColText(COLUMNS::TIC_DEDUCTION));
			rec.setBarkRation(-1.0);	// Not used, barkratio is set per species
			rec.setIBTaper(pRec->getColFloat(COLUMNS::TIC_TAPER_IB));
			// Check if its a newly added or updated
			if (rec.getPKID() == -1)
			{
				if (bOkToSave)	
				{
					m_pDB->newTicket(rec);
					int nLoadID = m_pDB->getLastLoadID();
					rec.setPKID(nLoadID);
					populateReport(false);
				}
				else
					::MessageBox(GetSafeHwnd(),m_sMsgErrNewTicket2,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
			}
			else if (rec.getPKID() > 0)
			{
				if (bOkToSave)				
					m_pDB->updTicket(rec);
				else 
					::MessageBox(GetSafeHwnd(),m_sMsgErrUpdTicket2,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
			}
		}	// for (int i = 0;i < pRows->GetCount();i++)
	}	// if (pRows != NULL)

	return TRUE;

}
// Save ALL tickets
BOOL CLogScaleView::Save(bool populate)
{
	BOOL bOkToClose = TRUE;

	CXTPReportRows* pRows = m_repTickets.GetRows();
	CTicketsReportRec *pRec = NULL;
	CTickets rec;
	BOOL bOkToSave = TRUE;
	BOOL bTicketNumOK = TRUE;
	CTicketLogsFormView *pView = (CTicketLogsFormView *)getFormViewByID(IDD_FORMVIEW5);
	
	CViewProgress wndPrg;

	// Create the edit control and add it to the status bar
	if (!wndPrg.Create(CViewProgress::IDD))
	{
		TRACE0("Failed to create CViewProgress.\n");
	}
	
	
	if (pView != NULL)
	{
		bTicketNumOK = pView->isTicketNumberOK();
	}
	if (pRows != NULL && m_pDB != NULL)
	{
		if (wndPrg.GetSafeHwnd())
		{
			wndPrg.setRange(0,pRows->GetCount());
			wndPrg.setStep();
			wndPrg.ShowWindow(SW_NORMAL);
			wndPrg.UpdateWindow();
		}

		m_repTickets.Populate();
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CTicketsReportRec *)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL)
			{
				rec = pRec->getRecord();
				rec.setTicket(pRec->getIconColText(COLUMNS::TIC_TICKET_NUM));	
				if (pRec->getIconColText(COLUMNS::TIC_TICKET_NUM).IsEmpty())
					bOkToSave = FALSE;
				rec.setLoadID(pRec->getColText(COLUMNS::TIC_LOAD_ID));
				rec.setDate(pRec->getColText(COLUMNS::TIC_SOURCE_DATE));
				rec.setHauler(pRec->getColText(COLUMNS::TIC_HAULER));
				rec.setScaler(pRec->getColText(COLUMNS::TIC_SCALER));
				rec.setNumOfLogs(pRec->getColInt(COLUMNS::TIC_NUMOF_LOGS));
				rec.setWeight(pRec->getColFloat(COLUMNS::TIC_WEIGHT));

				rec.setSumVolPricelist(pRec->getColFloat(COLUMNS::TIC_SUM_VOL_PRICELIST));
				rec.setSumPrice(pRec->getColFloat(COLUMNS::TIC_SUM_PRICE));
				rec.setSourceID(pRec->getColText(COLUMNS::TIC_SOURCE_ID));
				rec.setLocation(pRec->getColText(COLUMNS::TIC_LOCATION));
				rec.setBuyer(pRec->getColText(COLUMNS::TIC_BUYER));
				rec.setOtherInfo(pRec->getColText(COLUMNS::TIC_OTHER_INFO));
				rec.setTractID(pRec->getColText(COLUMNS::TIC_TRACT_ID));
				rec.setLoadType(pRec->getColText(COLUMNS::TIC_LOAD_TYPE));
				rec.setDeductionType(pRec->getColText(COLUMNS::TIC_DEDUCTION));
				rec.setBarkRation(-1.0);	// Not used, barkratio is set per species
				rec.setIBTaper(pRec->getColFloat(COLUMNS::TIC_TAPER_IB));
				// Check if its a newly added or updated
				if (rec.getPKID() == -1)
				{
					if (bOkToSave)				
						m_pDB->newTicket(rec);
					else if (populate)
						::MessageBox(GetSafeHwnd(),m_sMsgErrNewTicket1,m_sMsgCap,MB_ICONEXCLAMATION |MB_OK);
					else
						if (::MessageBox(GetSafeHwnd(),m_sMsgErrNewTicket2,m_sMsgCap,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
							bOkToClose = FALSE;
				}
				else if (rec.getPKID() > 0)
				{
					if (bOkToSave)			
					{
						m_pDB->updTicket(rec);
					}
					else if (populate)
						::MessageBox(GetSafeHwnd(),m_sMsgErrUpdTicket1,m_sMsgCap,MB_ICONEXCLAMATION |MB_OK);
					else
						if (::MessageBox(GetSafeHwnd(),m_sMsgErrUpdTicket2,m_sMsgCap,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
							bOkToClose = FALSE;
				}

				if (wndPrg.GetSafeHwnd())
					wndPrg.setPos(i+1);
			}
		}	// for (int i = 0;i < pRows->GetCount();i++)
	}	// if (pRows != NULL)
	if (populate)
		populateReport(false);

	if (wndPrg.GetSafeHwnd())
	{
		wndPrg.ShowWindow(SW_HIDE);
		wndPrg.DestroyWindow();
	}

	return bOkToClose;
}

void CLogScaleView::Delete()
{

	CTickets recTicket = CTickets();
	CXTPReportRow *pRow = m_repTickets.GetFocusedRow();
	CTicketsReportRec* pRec = NULL;
	CString sMsg = L"",sRegKey = L"";
	if (pRow != NULL && m_pDB != NULL)
	{
		pRec = (CTicketsReportRec*)pRow->GetRecord();
		if (pRec != NULL)
		{
			recTicket = pRec->getRecord();
			// We'll check if this ticket is included in a sale (sawmill)
			// If so, it's not possible to remove. Remove from sale (sawmill) first.
			if (m_pDB->isTicketInInventLogs(recTicket.getPKID()))
			{
				::MessageBox(GetSafeHwnd(),m_sMsgCanNotBeRemovedSale,m_sMsgCap,MB_ICONQUESTION | MB_OK);
				return;
			}

			sMsg.Format(m_sMsgDelete1,pRec->getIconColText(COLUMNS::TIC_TICKET_NUM));
			if (::MessageBox(GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
			{
				return;
			}
#ifdef _ASK_TWICE_ON_DELETE_TICKET
			else if (::MessageBox(GetSafeHwnd(),m_sMsgDelete2,m_sMsgCap,MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
			{
				return;
			}
#endif
			if (m_pDB->delTicket(recTicket))
			{
				populateReport(false);
				if (m_vecTickets.size() == 0)
				{
					m_pDB->resetTicketsIdentityField();
				}
				sRegKey.Format(L"%s_%d",recTicket.getTicket(),recTicket.getPKID());
				delColumnsSettingFile(sRegKey,COLSET_TICKET);
			}
		}	// if (pRec != NULL)
	}	// if (pRow != NULL)
}

void CLogScaleView::OnNewTicket(void)
{
	// Make sure we have ALL templates
	if (m_pDB != NULL)
	{
		m_pDB->getTmplTables(m_vecLogScaleTemplates);		
	}

	if (m_vecLogScaleTemplates.size() == 0)
	{
		::MessageBox(GetSafeHwnd(),m_sMissingTemplate1,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
		return;
	}

	CSelectTemplateDlg *pDlg = new CSelectTemplateDlg();
	if (pDlg != NULL)
	{
		pDlg->setData(m_vecLogScaleTemplates);
		if (pDlg->DoModal() == IDOK)
		{
			regSetInt(REG_ROOT,TICKET_MEASURINGMODE_KEY,L"MODE",pDlg->getSelectedTemplate().getMeasureMode());
			showFormView(IDD_FORMVIEW5,m_sLangFN,(LPARAM)&pDlg->getSelectedTemplate(),ID_OPEN_LOGS_TMPL);
		}
	}
}

void CLogScaleView::OnSaveTicket(void)
{
	// Save ALL tickets
	Save(true);
}

void CLogScaleView::OnDeleteTicket(void)
{
	Delete();
}

BOOL CLogScaleView::isTicketNumOK(LPCTSTR ticket_num)
{
	BOOL bReturn = TRUE;
	CString sTicketNum1 = L"",sTicketNum2 = ticket_num;
	CXTPReportRows *pRows = m_repTickets.GetRows();
	CTicketsReportRec *pRec = NULL;
	if (pRows != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CTicketsReportRec *)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL)
			{
				sTicketNum1 = pRec->getIconColText(COLUMNS::TIC_TICKET_NUM).Trim();
				if (sTicketNum1.CompareNoCase(sTicketNum2.Trim()) == 0)
				{
					bReturn = FALSE;
					break;
				}
			}
		}
	}

	return bReturn;
}

BOOL CLogScaleView::checkLogData(CVecLogs& vec,CTickets& rec,bool ticket_num_ok)
{
	BOOL bReturn = FALSE;
	CSpecies recSpecies = CSpecies();
	CGrades recGrades = CGrades();
	CLogs recLog = CLogs();
	BOOL bFoundSpc = FALSE;
	BOOL bFoundGrade = FALSE;
	CString sLogSpecies = L"";
	CString sSpecies = L"";
	CString sLogGrade = L"";
	CString sGrade = L"";
	std::map<int,CString> mapHeaderData;
	CString sGenerateTicketNum = L"";

	if (vec.size() > 0)
	{
		for (UINT log = 0;log < vec.size();log++)
		{
			recLog = vec[log];
			sLogSpecies = recLog.getSpcCode().Trim();
			sLogGrade = recLog.getGrade().Trim();
			bFoundSpc = FALSE;
			// First check that speciecodes in logs from file
			// are included in logscaleSpecies-table
			if (m_vecSpecies.size() > 0)
			{
				for (UINT spc = 0;spc < m_vecSpecies.size();spc++)
				{
					recSpecies = m_vecSpecies[spc];
					sSpecies = recSpecies.getSpcCode().Trim();
					if (sLogSpecies.CompareNoCase(sSpecies) == 0)
					{
						bFoundSpc = TRUE;
						break;
					}
				}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
			}	// if (m_vecSpecies.size() > 0)

			bFoundGrade = FALSE;
			// First check that speciecodes in logs from file
			// are included in logscaleSpecies-table
			if (m_vecGrades.size() > 0)
			{
				for (UINT grd = 0;grd < m_vecGrades.size();grd++)
				{
					recGrades = m_vecGrades[grd];
					sGrade = recGrades.getGradesCode().Trim();
					if (sLogGrade.CompareNoCase(sGrade) == 0)
					{
						bFoundGrade = TRUE;
						break;
					}
				}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
			}	// if (m_vecSpecies.size() > 0)

			if (!bFoundSpc || !bFoundGrade)
			{
				if (!bFoundSpc && bFoundGrade)
					vec[log].setFlag(1);
				else if (bFoundSpc && !bFoundGrade)
					vec[log].setFlag(2);
				else if (!bFoundSpc && !bFoundGrade)
					vec[log].setFlag(3);
			}

		}	// for (UINT log = 0;log < vec.size();log++)
	}
	if (vec.size() > 0)
	{
		// Make sure we have ALL templates
		if (m_pDB != NULL)
		{
			m_pDB->getTmplTables(m_vecLogScaleTemplates);		
			sGenerateTicketNum = generateTicketNumber(m_pDB->getLastLoadID_generate());
		}

		CCorrectionDlg *pDlg = new CCorrectionDlg(NULL,0);
		if (pDlg != NULL)
		{
			pDlg->setErrorLogs(vec);
			pDlg->setSpecies(m_vecSpecies);
			pDlg->setGrades(m_vecGrades);
			pDlg->setRegister(m_vecRegister);
			pDlg->setTicket(rec,m_vecTickets);
			pDlg->setTemplates(m_vecLogScaleTemplates);
			//pDlg->setTicketNumOK(m_bIsTicketNumOK);
			pDlg->setGeneratedTicketNum(sGenerateTicketNum);
			pDlg->setOpenLogsViewAs(ID_OPEN_LOGS_FILE);
			if (pDlg->DoModal() == IDOK)
			{
				vec = pDlg->getLogs();
				rec = pDlg->getTicket();
				// Check if user's choosen to add to Register
				pDlg->getHeaderData(mapHeaderData);
				if (mapHeaderData.size() > 0)
				{
					for (int i = REGISTER_TYPES::BUYER ;i <= REGISTER_TYPES::SUPPLIER;i++)
					{
						if (!mapHeaderData[i].IsEmpty())
						m_pDB->newRegisterItem(CRegister(-1,i,mapHeaderData[i],L"",L"",L"",L"",L"",L"",L"",L"",L"",L""));
					}
				}

				bReturn = TRUE;
			}
			delete pDlg;
		}
	}
	return bReturn;
}

void CLogScaleView::OnImportTicket(void)
{
	bool bDoOpenLogs = true;
	int nLoadID = -1,nMode = -1;
	CTickets recTicket = CTickets();
	CVecLogs vec,vec_to_add_to_inventory;
	CLogs recLog = CLogs();
	CString  strFile = L""; // _T("*.lsa"),S;
	CString strFilter = _T("LSA(*.lsa)|*.lsa|");
	
	strFile = regGetStr(REG_ROOT,TICKET_IMPORT_DIRECTORY,DIRECTORY_KEY);
	strFile += L"*.lsa";

	CFileDialog dlg(TRUE,L"lsa",strFile,4|2,strFilter);

	if (m_vecLogScaleTemplates.size() == 0)
	{
		::MessageBox(GetSafeHwnd(),m_sMissingTemplate2,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
		return;
	}
	
	CViewProgress wndPrg;

	// Create the edit control and add it to the status bar
	if (!wndPrg.Create(CViewProgress::IDD))
	{
		TRACE0("Failed to create edit control.\n");
	}

	if(dlg.DoModal() == IDOK)
	{
		regSetStr(REG_ROOT,TICKET_IMPORT_DIRECTORY,DIRECTORY_KEY,getFilePath(dlg.GetPathName()));

		// get Logs in Inventory
		if (m_pDB != NULL)
		{
			m_pDB->getAllLogs(m_vecLogs);
		}
		
		CFileParser *pars = new CFileParser(dlg.GetPathName());
		if (pars->getTicket(recTicket))
		{
			m_bIsTicketNumOK = true;

			if (m_pDB != NULL)
			{
				if (m_pDB->newTicket(recTicket))
				{
					nLoadID = m_pDB->getLastLoadID();
					if (nLoadID > -1)
					{
						// Nothing to do, measuremode the same
						if (recTicket.getMeasuringMode() == 0) 
							nMode = 4;				
						// Already entered data have inch and added data has meter
						else if (recTicket.getMeasuringMode() == 1) 
							nMode = 5;	
						if (pars->getLogs(nMode,nLoadID,vec))
						{
							// We'll need to check that tag-numbers in this Ticket isn't already
							// used in the Inventory database
							if (!checkLogTagNumbers(recTicket.getTicket(),dlg.GetFileTitle(),vec,vec_to_add_to_inventory))
							{
								recTicket.setPKID(nLoadID);
								m_pDB->delTicket(recTicket);
								return;
							}
							/*
							// Make user aware, ticketnumber already used
							if (!isTicketNumOK(recTicket.getTicket()))
							{
							//	::MessageBox(GetSafeHwnd(),m_sMsgTicketNumUsed,m_sMsgCap,MB_ICONASTERISK | MB_OK);
								m_bIsTicketNumOK = false;
							}
							*/

							// We'll do a check of data; speciecode and grades, in logs.
							// 
							if (checkLogData(vec_to_add_to_inventory,recTicket,m_bIsTicketNumOK))
							{
								UpdateWindow();
								if (wndPrg.GetSafeHwnd())
								{
									wndPrg.setRange(0,vec_to_add_to_inventory.size());
									wndPrg.setStep();
									wndPrg.ShowWindow(SW_NORMAL);
									wndPrg.UpdateWindow();
								}

								bDoOpenLogs = true;
								// Set loadid (primary key), before update
								recTicket.setPKID(nLoadID);
								m_pDB->updTicket(recTicket);
								// Add logs to database table (timsLogs) for ticket (timsTickets)
								if (vec_to_add_to_inventory.size() > 0)
								{
									for (int i = 0;i < vec_to_add_to_inventory.size();i++)
									{
										recLog = vec_to_add_to_inventory[i];
										recLog.setSpcName(getSpcName(recLog.getSpcCode()));
										m_pDB->newLog(recLog);
										if (wndPrg.GetSafeHwnd())
											wndPrg.setPos(i+1);
									}	// for (int i = 0;i < vec.size();i++)
								}	// if (vec.size() > 0)
							}	// if (checkLogData(vec))
							else
							{
								bDoOpenLogs = false;
								m_pDB->delTicket(nLoadID);
							}
						}	// if (pars->getLogs(nLoadID,vec))
					}	// if (nLoadID > -1)
				}	// if (m_pDB->newTicket(rec))
			}	// if (m_pDB != NULL)
		}	// if (pars->getTicket(rec))
		delete pars;
		wndPrg.setRange(0,100);
		wndPrg.setPos(40);
		// Do an update; sum. log data, and save it to ticket
		m_pDB->updSumLogData(nLoadID);
		wndPrg.setPos(60);
		populateReport(true);
		if (wndPrg.GetSafeHwnd())
		{
			wndPrg.ShowWindow(SW_HIDE);
			wndPrg.DestroyWindow();
		}
		// We'll also open "M�tbesked"
		if (bDoOpenLogs)
		{
			regSetInt(REG_ROOT,TICKET_MEASURINGMODE_KEY,L"MODE",recTicket.getMeasuringMode());
			showFormView(IDD_FORMVIEW5,m_sLangFN,(LPARAM)&recTicket,ID_OPEN_LOGS_FILE);
		}
		
	}	// if(dlg.DoModal() == IDOK)

}

void CLogScaleView::doExportToExcel(LPCTSTR ticket_name,int id)	
{ 
	CXTPReportRows *pRows = m_repTickets.GetRows();
	CTicketsReportRec *pRec = NULL;

	if (pRows != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CTicketsReportRec *)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL)
			{
				if (pRec->getRecord().getTicket().CompareNoCase(ticket_name) == 0)
				{
					pRows->GetAt(i)->SetSelected(TRUE);
					m_repTickets.SetFocusedRow(pRows->GetAt(i));
					break;
				}
			}
		}
	}

	OnExportToExcel(); 
}


// Export ticket to EXCEL
void CLogScaleView::OnExportToExcel(void)
{
	CXTPReportRow *pRow = m_repTickets.GetFocusedRow();
	CTicketsReportRec *pRec = NULL;

	if (pRow == NULL) return;

	if (pRow != NULL)
	{
		pRec = (CTicketsReportRec *)pRow->GetRecord();
		if (pRec != NULL)
		{
			m_recTicket = pRec->getRecord();

			CTicketExport cExport;
			cExport.ExportToExcel(m_pDB, &m_recTicket);
		}
	}
}

void CLogScaleView::addTicket(CTicketsReportRec *pRec)
{
}

// Create a template-file to be sent to caliper
void CLogScaleView::OnExportTemplate(void)
{
}

CString CLogScaleView::getSpcName(LPCTSTR spc_code)
{
	CString spcCode = L"";
	if (m_vecSpecies.size() > 0)
	{
		for (UINT i = 0;i < m_vecSpecies.size();i++)
		{
			spcCode = m_vecSpecies[i].getSpcCode().Trim();
			if (spcCode.CompareNoCase(spc_code) == 0)
				return m_vecSpecies[i].getSpcName();
		}
	}

	return L"";
}

void CLogScaleView::OnFilter(void)
{
	CLogScaleFrame* pWnd = (CLogScaleFrame *)getFormViewParentByID(IDD_FORMVIEW);
	if (pWnd != NULL)
	{
		setFilterWindow(false);
		BOOL bShow = !pWnd->m_wndFilterEdit.IsVisible();
		pWnd->ShowControlBar(&pWnd->m_wndFilterEdit, bShow, FALSE);
		pWnd->setEnableFilterOff(!m_repTickets.GetFilterText().IsEmpty());
	}

}

void CLogScaleView::OnFilterOff(void)
{
	m_repTickets.SetFilterText(_T(""));
	m_repTickets.Populate();
	m_wndFilterEdit.SetWindowText(_T(""));
	CLogScaleFrame* pWnd = (CLogScaleFrame *)getFormViewParentByID(IDD_FORMVIEW);
	if (pWnd != NULL)
	{
		pWnd->setEnableFilterOff(FALSE);
	}	// if (pWnd != NULL)
}

void CLogScaleView::setFilterWindow(bool bOld)
{
	// H�mta ut vilken kolumn som �r vald. #4164
	if(!bOld)
	{
		CXTPReportColumn *pCol = m_repTickets.GetFocusedColumn();
		if (pCol != NULL)
			m_nFilteredColumn = pCol->GetIndex();
	}

	m_lbl20_1.SetWindowText(m_sFilterOn + _T(" :"));
	if (m_nFilteredColumn > -1 && m_nFilteredColumn < m_repTickets.GetColumns()->GetCount())
	{
		CXTPReportColumns *pCols = m_repTickets.GetColumns();
		CXTPReportColumn *pColumn = pCols->GetAt(m_nFilteredColumn);
		int nColumn = pColumn->GetIndex();
		if (pCols && nColumn < pCols->GetCount())
		{
			for (int i = 0;i < pCols->GetCount();i++)
			{
				pCols->GetAt(i)->SetFiltrable( i == nColumn );
			}
		}

		m_lbl20_2.SetWindowText(pColumn->GetCaption());
	}
	else
	{
		m_lbl20_2.SetWindowText(L"");
	}
}

BOOL CLogScaleView::checkLogTagNumbers(LPCTSTR ticket,LPCTSTR ticket_file,CVecLogs &vec_in_ticket,CVecLogs &vec_logs_to_add_to_inventory)
{
	BOOL bFound = FALSE,bQuit = TRUE;
	CString sTmp = L"";
	CString sSQL = L"";
	vecDuplicateLogs vec_duplicates; // Already in inventory
	CLogs recLog = CLogs();
	CVecLogs vec_match;	// Logs already in logs-inventory
	CStrIntMap mapTags;	// Actual number of valid tags in file, also counts duplicates
	if (vec_in_ticket.size() > 0)
	{
		for (UINT i1 = 0;i1 < vec_in_ticket.size();i1++)
		{
			sTmp.Format(L"%s.sTagNumber='%s' ",TBL_LOGS,vec_in_ticket[i1].getTagNumber());
			if (i1 < vec_in_ticket.size()-1)
				sSQL += sTmp + L" or ";
			else
				sSQL += sTmp;

			mapTags[vec_in_ticket[i1].getTagNumber()] += 1;
		}

		if (m_pDB != NULL)
		{
			m_pDB->checkLogTagNumbers(sSQL,vec_duplicates);
		}
	}

	// Setup Match vector
	if (vec_duplicates.size() > 0 && vec_in_ticket.size() > 0)
	{
		for (UINT i = 0;i < vec_duplicates.size();i++)
		{
			for (UINT i1 = 0;i1 < vec_in_ticket.size();i1++)
			{
				recLog = vec_in_ticket[i1];
				if (recLog.getTagNumber().CompareNoCase(vec_duplicates[i].sTagNumber) == 0)
				{
					recLog.setTicket(vec_duplicates[i].sTicketNumber);
					recLog.setStatusFlag(vec_duplicates[i].nStatus);
					vec_match.push_back(recLog);
					break;
				}	// if (vec_in_ticket[i1].getTagNumber().CompareNoCase(vec_duplicates[i].sTagNumber) == 0)
			}	// for (UINT i1 = 0;i1 < vec_in_ticket.size();i1++)
		}	// for (UINT i = 0;i < vec_duplicates.size();i++)

		for (UINT i1 = 0;i1 < vec_in_ticket.size();i1++)
		{
			recLog = vec_in_ticket[i1];
			bFound = FALSE;
			if (vec_duplicates.size() > 0)
			{
				for (UINT i = 0;i < vec_duplicates.size();i++)
				{
					if (recLog.getTagNumber().CompareNoCase(vec_duplicates[i].sTagNumber) == 0)
					{
						bFound = TRUE;
						break;
					}	// if (vec_in_ticket[i1].getTagNumber().CompareNoCase(vec_duplicates[i].sTagNumber) == 0)
				}	// for (UINT i1 = 0;i1 < vec_in_ticket.size();i1++)
			}
			if (!bFound)
			{
				vec_logs_to_add_to_inventory.push_back(recLog);
			}
		}	// for (UINT i = 0;i < vec_duplicates.size();i++)
	}
	else if (vec_in_ticket.size() > 0 && vec_duplicates.size() == 0)
	{
		bQuit = TRUE;
		for (UINT i1 = 0;i1 < vec_in_ticket.size();i1++)
		{
			recLog = vec_in_ticket[i1];
			vec_logs_to_add_to_inventory.push_back(recLog);
		}	// for (UINT i = 0;i < vec_duplicates.size();i++)
	}

//	if (vec_duplicates.size() > 0)
//	{
		CTagNoMatchDlg *pDlg = new CTagNoMatchDlg(NULL,0);
		if (pDlg != NULL)
		{	
			pDlg->setLogTagsNotOK(vec_match);
			pDlg->setLogTagsOK(vec_logs_to_add_to_inventory); //vec_in_ticket);
			pDlg->setTotalNumberOfLogs(vec_in_ticket.size());
			pDlg->setLogTags(mapTags);
			pDlg->setLogTagsDup(mapTags);
			pDlg->setTicket(ticket);
			pDlg->setTicketFile(ticket_file);
			if (pDlg->DoModal() == IDOK)
			{
				bQuit = TRUE;
				if (vec_logs_to_add_to_inventory.size() == 0)
				{
					::MessageBox(GetSafeHwnd(),m_sMsgDuplicateTagNumbers,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
				}
			}
			else
			{
				bQuit = FALSE;
			}
			delete pDlg;
		}
//	}

	return (vec_logs_to_add_to_inventory.size() > 0 && bQuit);	// No matches
}

BOOL CLogScaleView::isTicketAlreadyUsed(CString& ticket_num)
{
	CXTPReportRows* pRows = m_repTickets.GetRows();
	CTicketsReportRec *pRec = NULL;
	if (pRows != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CTicketsReportRec *)pRows->GetAt(i)->GetRecord();
			if (pRec->getIconColText(COLUMNS::TIC_TICKET_NUM).Trim().CompareNoCase(ticket_num.Trim()) == 0)
				return TRUE;
		}
	}
	return FALSE;
}


void CLogScaleView::OnSettings(UINT nID)
{
/*
	switch(nID)
	{

		case ID_TOOLS_SPECIES:
		break;
		case ID_TOOLS_GRADES:
		break;
		case ID_TOOLS_PRICELIST:
		break;
		case ID_TOOLS_REGISTER:
		break;
		case ID_TOOLS_DEFAULTS:
		break;
	};
*/
}

void CLogScaleView::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (!AfxGetApp()->GetProfileBinary(REG_TICKETS_REPORT_KEY, _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		m_repTickets.SerializeState(ar);
	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;

	// Get filtertext for this Report
	sFilterText = AfxGetApp()->GetProfileString(REG_TICKETS_REPORT_KEY, _T("FilterText"), _T(""));
	// Get selected column index into registry; 120418 p�d
	m_nFilteredColumn = AfxGetApp()->GetProfileInt(REG_TICKETS_REPORT_KEY, _T("SelColIndex"),0);

	m_repTickets.SetFilterText(sFilterText);
	m_wndFilterEdit.SetWindowText(sFilterText);
	setFilterWindow(true);
	m_repTickets.Populate();

	CLogScaleFrame* pWnd = (CLogScaleFrame *)getFormViewParentByID(IDD_FORMVIEW);
	if (pWnd != NULL)
	{
		pWnd->setEnableFilterOff(!sFilterText.IsEmpty());
	}

}

void CLogScaleView::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	m_repTickets.SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary(REG_TICKETS_REPORT_KEY, _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);

	sFilterText = m_repTickets.GetFilterText();
	AfxGetApp()->WriteProfileString(REG_TICKETS_REPORT_KEY, _T("FilterText"), sFilterText);

	// Set selected column index into registry; 120418 p�d
	AfxGetApp()->WriteProfileInt(REG_TICKETS_REPORT_KEY, _T("SelColIndex"), m_nFilteredColumn);

}

