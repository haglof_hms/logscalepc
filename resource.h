//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by LogScalePC.rc
//
#define IDI_FORMVIEW                    6000
#define IDD_FORMVIEW                    6001
#define IDD_FORMVIEW4                   6003
#define IDD_FORMVIEW1                   6004
#define IDC_COMBO1                      6004
#define IDC_COMBO6_10                   6004
#define IDC_COMBO8_10                   6004
#define IDC_COMBO6_1                    6004
#define IDC_COMBO15_1                   6004
#define IDC_COMBO17_1                   6004
#define IDC_COMBO6516_1                 6004
#define IDD_FORMVIEW3                   6005
#define IDC_LBL_SPECIES                 6005
#define IDD_DIALOG3                     6005
#define IDC_COMBO6_13                   6005
#define IDC_COMBO6516_2                 6005
#define IDD_FORMVIEW5                   6006
#define IDC_COMBO6_14                   6006
#define IDC_COMBO6516_3                 6006
#define IDD_FORMVIEW6                   6007
#define IDC_COMBO6_15                   6007
#define IDC_COMBO6516_4                 6007
#define IDR_TOOLBAR3                    6008
#define IDC_COMBO6_16                   6008
#define IDC_COMBO6516_5                 6008
#define IDC_REMOVE                      6009
#define IDC_COMBO6_17                   6009
#define IDD_FORMVIEW2                   6010
#define IDC_COMBO6_18                   6010
#define IDD_DIALOG1                     6011
#define IDC_COMBO6_19                   6011
#define IDR_TOOLBAR4                    6012
#define IDC_COMBO6_20                   6012
#define IDD_DIALOG4                     6016
#define IDD_DIALOG5                     6017
#define IDC_LBL1                        6020
#define IDR_TOOLBAR5                    6020
#define IDC_LBL2                        6021
#define IDC_LBL4_1                      6021
#define IDR_TOOLBAR2                    6022
#define IDD_DIALOG6                     6022
#define IDB_BITMAP1                     6023
#define IDB_PNG1                        6024
#define IDD_DIALOG7                     6028
#define IDD_DIALOG8                     6029
#define IDD_DIALOG9                     6030
#define IDD_DIALOG10                    6031
#define IDR_TOOLBAR6                    6032
#define IDC_COMBO4_1                    6033
#define IDC_BUTTON1                     6034
#define IDB_BMP_MINUS                   6034
#define IDC_BUTTON2                     6035
#define IDB_BMP_PLUS                    6035
#define IDC_BUTTON3                     6036
#define IDD_DIALOG11                    6036
#define IDC_BUTTON4                     6037
#define IDR_TOOLBAR7                    6037
#define IDC_MONTHCALENDAR1              6038
#define IDC_LBL5_1                      6039
#define IDR_TOOLBAR8                    6039
#define IDC_LBL5_2                      6040
#define IDC_LBL5_3                      6041
#define IDD_DIALOG2                     6041
#define IDC_LBL5_4                      6042
#define IDD_DIALOG12                    6042
#define IDC_LBL6_1                      6043
#define IDD_DIALOG13                    6043
#define IDC_EDIT1                       6044
#define IDC_EDIT5_1                     6044
#define IDC_EDIT9_1                     6044
#define IDC_EDITDLG9_1                  6044
#define IDD_DIALOG14                    6044
#define IDC_LBL6_20                     6044
#define IDC_EDIT18_1                    6044
#define IDC_FILTEREDIT20_1              6044
#define IDC_EDIT6516_4                  6044
#define IDC_COMBO2                      6045
#define IDC_COMBO5_2                    6045
#define IDC_COMBO7_2                    6045
#define IDC_COMBO6_11                   6045
#define IDB_BITMAP2                     6045
#define IDC_LBL6_21                     6045
#define IDC_COMBO8_11                   6045
#define IDC_COMBO17_2                   6045
#define IDC_COMBO6516_6                 6045
#define IDC_DATE6516_1                  6045
#define IDC_EDIT6_2                     6046
#define IDC_LBL6_22                     6046
#define IDC_COMBO8_13                   6046
#define IDD_DIALOG15                    6046
#define IDC_DATE6516_2                  6046
#define IDC_COMBO3                      6047
#define IDC_COMBO5_3                    6047
#define IDC_COMBO7_3                    6047
#define IDC_COMBO6_12                   6047
#define IDC_LBL6_23                     6047
#define IDD_DIALOG16                    6047
#define IDC_COMBO17_3                   6047
#define IDC_EDIT6516_5                  6047
#define IDC_LBL6_2                      6048
#define IDC_EDIT6516_6                  6048
#define IDC_LBL6_3                      6049
#define IDC_EDIT6516_7                  6049
#define IDC_EDIT6_3                     6050
#define IDC_LBL6_24                     6050
#define IDC_EDIT6516_8                  6050
#define IDC_COMBO6_4                    6051
#define IDC_LBL6_25                     6051
#define IDC_EDIT6516_9                  6051
#define IDC_EDIT6_4                     6052
#define IDC_LBL6_26                     6052
#define IDC_DATE6516_3                  6052
#define IDC_COMBO6_5                    6053
#define IDC_LBL6_27                     6053
#define IDB_BITMAP3                     6053
#define IDC_DATE6516_4                  6053
#define IDC_EDIT6_5                     6054
#define IDC_LBL6_28                     6054
#define IDD_DIALOG17                    6054
#define IDC_DATE6516_5                  6054
#define IDC_COMBO6_6                    6055
#define IDC_LBL6_29                     6055
#define IDC_DATE6516_6                  6055
#define IDC_EDIT6_6                     6056
#define IDC_LBL6_30                     6056
#define IDD_DIALOG18                    6056
#define IDC_COMBO6_7                    6057
#define IDC_LBL6_31                     6057
#define IDD_DIALOG19                    6057
#define IDC_EDIT6_7                     6058
#define IDD_FILTEREDIT20                6058
#define IDC_COMBO6_8                    6059
#define IDD_FIELD_SELECTION4            6059
#define IDC_EDIT6_8                     6060
#define IDD_DIALOG20                    6060
#define IDD_DIALOG21                    6061
#define IDC_COMBO6_9                    6061
#define IDR_TOOLBAR9                    6061
#define IDC_EDIT6_9                     6062
#define IDC_EDIT6_10                    6063
#define IDB_BITMAP4                     6063
#define IDC_EDIT6_11                    6064
#define IDC_EDIT6_12                    6065
#define IDC_LBL6_4                      6066
#define IDC_LBL6_5                      6067
#define IDC_LBL6_6                      6068
#define IDC_LBL6_7                      6069
#define IDC_LBL6_8                      6070
#define IDC_LBL6_9                      6071
#define IDC_LBL6_10                     6072
#define IDC_LBL6_11                     6073
#define IDC_LBL6_12                     6074
#define IDC_LBL6_13                     6075
#define IDC_EDIT6_1                     6076
#define IDC_COMBO6_2                    6077
#define IDC_EDIT6_15                    6077
#define IDC_COMBO6_3                    6078
#define IDC_EDIT6_16                    6078
#define IDC_LBL6_14                     6079
#define IDC_LBL7_1                      6079
#define IDC_EDIT6_17                    6079
#define IDC_EDIT6_13                    6080
#define IDC_EDIT7_1                     6080
#define IDC_EDIT6_18                    6080
#define IDC_EDIT6_14                    6081
#define IDC_LBL8_1                      6081
#define IDC_LBL8_2                      6082
#define IDC_LBL6_18                     6082
#define IDC_LBL8_3                      6083
#define IDC_LBL8_4                      6084
#define IDC_LBL8_5                      6085
#define IDC_LBL8_6                      6086
#define IDC_LBL8_7                      6087
#define IDC_LBL8_8                      6088
#define IDC_LBL8_9                      6089
#define IDC_LBL8_10                     6090
#define IDC_LBL8_11                     6091
#define IDC_LBL8_12                     6092
#define IDC_LBL8_13                     6093
#define IDC_LBL8_14                     6094
#define IDC_LBL8_15                     6095
#define IDC_EDIT8_2                     6096
#define IDC_EDIT8_3                     6097
#define IDC_COMBO8_4                    6098
#define IDC_COMBO8_5                    6099
#define IDC_COLUMNS                     6100
#define IDC_COMBO8_6                    6100
#define IDC_EDIT8_4                     6101
#define IDR_TOOLBAR1                    6102
#define IDC_COMBO8_7                    6102
#define IDC_COMBO8_8                    6103
#define IDD_FIELD_SELECTION             6104
#define IDC_COMBO8_9                    6104
#define IDC_EDIT8_5                     6105
#define IDD_FIELD_SELECTION3            6105
#define IDC_EDIT8_6                     6106
#define IDC_EDIT8_1                     6107
#define IDC_COMBO8_1                    6108
#define IDC_COMBO8_2                    6109
#define IDC_COMBO8_3                    6110
#define IDC_EDIT5_2                     6111
#define IDC_EDIT5_3                     6112
#define IDC_COMBO5_4                    6113
#define IDC_COMBO5_5                    6114
#define IDC_COMBO5_6                    6115
#define IDC_COMBO5_7                    6116
#define IDC_COMBO5_12                   6117
#define IDC_COMBO5_8                    6118
#define IDC_COMBO5_9                    6119
#define IDC_EDIT5_4                     6120
#define IDC_EDIT5_8                     6120
#define IDC_EDIT5_6                     6121
#define IDC_EDIT5_7                     6122
#define IDC_LBL5_5                      6123
#define IDC_LBL5_6                      6124
#define IDC_LBL5_7                      6125
#define IDC_LBL5_8                      6126
#define IDC_LBL5_9                      6127
#define IDC_LBL5_10                     6128
#define IDC_LBL5_11                     6129
#define IDC_LBL5_12                     6130
#define IDC_LBL5_13                     6131
#define IDC_LBL5_19                     6131
#define IDC_LBL5_14                     6132
#define IDC_LBL5_15                     6133
#define IDC_COMBO5_1                    6134
#define IDC_LBL6_15                     6135
#define IDC_LBL5_16                     6135
#define IDC_LBL6_16                     6136
#define IDC_LBL5_17                     6136
#define IDC_LBL6_17                     6137
#define IDC_LBL5_18                     6137
#define IDC_COLUMNS2                    6138
#define IDC_LBL5_20                     6138
#define IDC_LBL6_32                     6138
#define IDC_COLUMN3                     6139
#define IDC_COLUMNS3                    6139
#define IDC_LBL5_21                     6139
#define IDC_LBL6_33                     6139
#define IDC_RADIO1                      6140
#define IDC_LBL5_22                     6140
#define IDC_RADIO6516_1                 6140
#define IDC_LBL5_23                     6141
#define IDC_COMBO5_10                   6142
#define IDC_LIST10_1                    6143
#define IDC_LBL5_24                     6143
#define IDC_LBL10_1                     6144
#define IDC_LBL5_25                     6144
#define IDC_BUTTON10_3                  6145
#define IDC_BUTTON10_4                  6146
#define IDC_BUTTON10_1                  6147
#define IDC_BUTTON10_2                  6148
#define IDC_BUTTON10_5                  6149
#define IDC_EDIT7_2                     6150
#define IDC_EDIT7_3                     6151
#define IDC_EDIT7_4                     6152
#define IDC_EDIT7_6                     6154
#define IDC_LBL7_2                      6155
#define IDC_LBL7_3                      6156
#define IDC_LBL7_4                      6157
#define IDC_LBL7_5                      6158
#define IDC_LBL7_6                      6159
#define IDC_LBL7_7                      6160
#define IDC_COMBO7_1                    6161
#define IDC_BUTTON7_1                   6162
#define IDC_BUTTON7_2                   6163
#define IDC_BUTTON7_3                   6164
#define IDC_BUTTON7_4                   6165
#define IDC_LBL7_8                      6166
#define IDC_LBL7_9                      6167
#define IDC_LBL7_10                     6168
#define IDC_GROUP7_1                    6169
#define IDC_EDIT7_7                     6170
#define IDC_EDIT7_8                     6171
#define IDC_EDIT7_9                     6172
#define IDC_EDIT7_10                    6173
#define IDC_LBL7_11                     6174
#define IDC_LBL7_12                     6175
#define IDC_LBL7_13                     6176
#define IDC_LBL7_14                     6177
#define IDC_LBL7_15                     6178
#define IDC_LBL7_16                     6179
#define IDC_LBL7_17                     6180
#define IDC_LBL7_18                     6181
#define IDC_LIST2                       6183
#define IDC_LIST8_1                     6185
#define IDC_EDIT9_2                     6186
#define IDC_EDIT9_3                     6187
#define IDC_LBL9_1                      6188
#define IDC_LBL9_2                      6189
#define IDC_LBL9_3                      6190
#define IDC_LBL9_4                      6191
#define IDC_COMBO9_1                    6192
#define IDC_LISTDLG9_1                  6193
#define IDC_LBLDLG9_1                   6194
#define IDC_LBLDLG9_2                   6195
#define IDC_LBL14_1                     6196
#define IDC_LBL15_1                     6197
#define IDC_LBL16_1                     6198
#define IDC_LBL17_1                     6199
#define IDD_FIELD_SELECTION2            6200
#define IDC_LBL18_1                     6200
#define IDC_LBL19_1                     6201
#define IDC_LBL20_1                     6202
#define IDC_LBL21_1                     6203
#define IDC_LBL22_1                     6204
#define IDC_LBL22_2                     6205
#define IDC_LIST22_1                    6206
#define IDC_LIST23_1                    6210
#define IDC_COMBO5_11                   6211
#define IDC_EDIT5_9                     6213
#define IDC_LIST1                       6215
#define IDC_LIST2_1                     6215
#define IDC_LIST5_1                     6215
#define IDC_EDIT5_10                    6215
#define IDC_LIST16_1                    6215
#define IDC_LIST17_1                    6215
#define IDC_LBL3_1                      6216
#define IDC_LBL12_1                     6217
#define IDC_CHECK6_1                    6218
#define IDC_CHECK6_2                    6219
#define IDC_LBL6_19                     6220
#define IDC_CHECK6_3                    6221
#define IDC_CHECK6_4                    6222
#define IDC_CHECK6_5                    6223
#define IDC_CHECK6_6                    6224
#define IDC_CHECK6_7                    6225
#define IDC_CHECK6_8                    6226
#define IDC_BUTTON6_1                   6227
#define IDC_LBL8_16                     6229
#define IDC_GRP8_1                      6230
#define IDC_LBL8_17                     6231
#define IDC_LIST3                       6231
#define IDC_BUTTON5_1                   6232
#define IDC_BUTTON5_2                   6233
#define IDC_LBL8_19                     6235
#define IDC_LBL8_20                     6236
#define IDC_EDIT6_19                    6237
#define IDC_LBL15_2                     6239
#define IDC_BUTTON16_1                  6243
#define IDC_LBL17_2                     6244
#define IDC_LBL17_3                     6245
#define IDC_LBL17_4                     6246
#define IDC_CHECK17_1                   6248
#define IDC_LBL18_2                     6250
#define IDC_LBL18_3                     6251
#define IDC_BUTTON18_1                  6252
#define IDC_BUTTON18_2                  6253
#define IDC_LBL18_4                     6254
#define IDC_BUTTON6_2                   6254
#define IDC_LBL18_5                     6255
#define IDC_BUTTON6_3                   6255
#define IDC_PROGRESS1                   6256
#define IDC_PROGRESS19_1                6256
#define IDC_FILTERLBL20_1               6257
#define IDC_FILTERLBL20_2               6258
#define IDC_COLUMNS4                    6259
#define IDC_GROUP6_1                    6260
#define IDC_GROUP6516_1                 6260
#define IDC_GROUP6_2                    6261
#define IDC_BUTTON6516_1                6261
#define IDC_GROUP6_3                    6262
#define IDC_BUTTON6516_2                6262
#define IDC_GRP20_1                     6263
#define IDC_LBL6516_1                   6263
#define IDC_LBL6516_2                   6264
#define IDC_LBL6516_3                   6265
#define IDC_BUTTON17_1                  6265
#define IDC_LBL6516_4                   6266
#define IDC_BUTTON17_2                  6266
#define IDC_LBL6516_5                   6267
#define IDC_LBL6516_6                   6268
#define IDC_LBL6516_7                   6269
#define IDC_LBL6516_8                   6270
#define IDC_LBL6516_9                   6271
#define IDC_LBL6516_10                  6272
#define IDC_LBL6516_11                  6273
#define IDC_LBL6516_13                  6274
#define IDC_LBL6516_14                  6275
#define IDC_GROUP6516_2                 6276
#define IDC_EDIT6516_1                  6277
#define IDC_EDIT6516_2                  6278
#define IDC_EDIT6516_3                  6279
#define IDC_RADIO6516_2                 6280
#define IDC_GROUP6516_3                 6281
#define IDC_LBL6516_12                  6282
#define IDC_RADIO6516_3                 6282
#define IDC_EDIT6516_10                 6283
#define IDC_LBL6516_15                  6284
#define IDC_CHECK1                      6286
#define IDC_CHECK5_1                    6286
#define IDC_DATE                        6288
#define IDC_DATE1                       6292
#define IDC_DATE_6516_1                 6292
#define IDC_DATE2                       6293
#define IDC_DATE_6516_2                 6293
#define IDC_DATE3                       6294
#define IDC_DATE_6516_4                 6294
#define IDC_DATE4                       6295
#define IDC_DATETIMEPICKER1             6295
#define IDC_DATE_6516_3                 6296
#define IDD_FORMVIEW7                   6500
#define IDD_FORMVIEW8                   6501
#define IDD_FORMVIEW9                   6502
#define IDD_FORMVIEW12                  6503
#define IDD_FORMVIEW13                  6504
#define IDD_FORMVIEW14                  6505
#define IDD_FORMVIEW15                  6506
#define IDD_FORMVIEW16                  6507
#define IDD_FORMVIEW17                  6508
#define IDD_FORMVIEW18                  6509
#define IDD_FORMVIEW19                  6510
#define IDD_FORMVIEW20                  6511
#define IDD_FORMVIEW21                  6512
#define IDD_FORMVIEW22                  6513
#define IDD_FORMVIEW23                  6514
#define IDD_FORMVIEW11                  6515
#define IDD_REPORTVIEW6516              6516
#define ID_LIST_PRL                     6600
#define ID_LIST_CONTRACT                6601
#define ID_LIST_USER_VTABLE             6602
#define ID_BUTTON32807                  32807
#define ID_BUTTON32808                  32808
#define ID_BUTTON32809                  32809
#define ID_BUTTON32810                  32810
#define ID_BUTTON32811                  32811
#define ID_BUTTON32812                  32812
#define ID_BUTTON32813                  32813
#define ID_BUTTON32814                  32814
#define ID_BUTTON32815                  32815
#define ID_BUTTON32816                  32816
#define ID_BUTTON32817                  32817
#define ID_BUTTON32818                  32818
#define ID_BUTTON32819                  32819
#define ID_BUTTON32820                  32820
#define ID_BUTTON32821                  32821
#define ID_BUTTON32822                  32822
#define ID_BUTTON32823                  32823
#define ID_BUTTON32824                  32824
#define ID_TBBTN_NEW                    33771
#define ID_TBBTN_EDIT                   33772
#define ID_TBBTN_SAVE                   33772
#define ID_TBBTN_DEL                    33773
#define ID_BUTTON32774                  33774
#define ID_TBBTN_IMPORT                 33774
#define ID_BUTTON32775                  33775
#define ID_BUTTON32776                  33776
#define ID_BUTTON32777                  33777
#define ID_BUTTON32778                  33778
#define ID_BUTTON32779                  33779
#define ID_BUTTON32780                  33780
#define ID_BUTTON32781                  33781
#define ID_BUTTON32782                  33782
#define ID_BUTTON32783                  33783
#define ID_BUTTON32784                  33784
#define ID_TBBTN_EXPORT                 33785
#define ID_TBBTN_REPORTCBOX             33786
#define ID_TBBTN_PRINT                  33787
#define ID_TBBTN_FILTER                 33788
#define ID_TBBTN_DEL_FILTER             33789
#define ID_TBBTN_SETTINGS               33790
#define ID_BUTTON32791                  33791
#define ID_BUTTON32792                  33792
#define ID_BUTTON32793                  33793
#define ID_BUTTON32794                  33794
#define ID_BUTTON32795                  33795
#define ID_BUTTON32796                  33796
#define ID_BUTTON32797                  33797
#define ID_BUTTON32798                  33798
#define ID_BUTTON32799                  33799
#define ID_BUTTON32800                  33800
#define ID_BUTTON32801                  33801
#define ID_BUTTON32802                  33802
#define ID_BUTTON32803                  33803
#define ID_BUTTON32804                  33804
#define ID_BUTTON32805                  33805
#define ID_BUTTON32806                  33806

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        6064
#define _APS_NEXT_COMMAND_VALUE         32825
#define _APS_NEXT_CONTROL_VALUE         6296
#define _APS_NEXT_SYMED_VALUE           6000
#endif
#endif
