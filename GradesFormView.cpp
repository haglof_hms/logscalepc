#include "stdafx.h"
#include "gradesformview.h"

#include "ResLangFileReader.h"

#include "reportclasses.h"

IMPLEMENT_DYNCREATE(CGradesReportView,  CXTPReportView) //CXTResizeFormView)

BEGIN_MESSAGE_MAP(CGradesReportView,  CXTPReportView) //CXTResizeFormView)
	ON_WM_DESTROY()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
END_MESSAGE_MAP()

CGradesReportView::CGradesReportView()
	: CXTPReportView(),m_pDB(NULL),m_bInitialized(FALSE)
{
}

CGradesReportView::~CGradesReportView()
{
}

void CGradesReportView::OnDestroy()
{
	saveGrades(m_pDB);

	CXTPReportView::OnDestroy();
}

void CGradesReportView::OnInitialUpdate()
{
	CXTPReportView::OnInitialUpdate();
	if (!m_bInitialized)
	{
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupReport();

		m_bInitialized = TRUE;
	}

}

BOOL CGradesReportView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
//	if( !CXTResizeFormView::PreCreateWindow(cs) )
//		return FALSE;
	if( ! CXTPReportView::PreCreateWindow(cs) )
		return FALSE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CGradesReportView diagnostics

#ifdef _DEBUG
void CGradesReportView::AssertValid() const
{
	CXTPReportView::AssertValid();
}

void CGradesReportView::Dump(CDumpContext& dc) const
{
	CXTPReportView::Dump(dc);
//	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CGradesReportView message handlers

LRESULT CGradesReportView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch (wParam)
	{
		case ID_SAVE_ITEM :
		{
			break;
		}	// case ID_SAVE_ITEM :
	}	// switch (wParam)
	return 0L;
}

// Create and add Assortment settings reportwindow
void CGradesReportView::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_0, xml.str(IDS_STRING1211), 80,FALSE));
			pCol->AllowRemove(FALSE);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;
			pCol->GetEditOptions()->m_dwEditStyle = ES_UPPERCASE;

			pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_1, xml.str(IDS_STRING1212), 200,FALSE));
			pCol->AllowRemove(FALSE);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;

			pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_2, xml.str(IDS_STRING1213), 80,FALSE,XTP_REPORT_NOICON,FALSE));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK);
			pCol->GetEditOptions()->m_bAllowEdit = FALSE;

			GetReportCtrl().GetReportHeader()->AllowColumnRemove(FALSE);
			GetReportCtrl().SetMultipleSelection( FALSE );
			GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );
			GetReportCtrl().SetGridStyle( FALSE, xtpReportGridSmallDots );
			GetReportCtrl().FocusSubItems(TRUE);
			GetReportCtrl().AllowEdit(TRUE);

			// Strings
			m_sMsgCap = xml.str(IDS_STRING99);
			m_sMsgDelete1 = xml.str(IDS_STRING1255);
			m_sMsgDelete2 = xml.str(IDS_STRING1256);


			xml.clean();
		}
	}
}

// PUBLIC
void CGradesReportView::populateReport(CDBHandling *pDB)
{
	if (m_pDB == NULL)
		m_pDB = pDB;
	// We'll also add this new item to DB
	if (pDB != NULL)
	{
		pDB->getGrades(m_vecGrades,GetReportCtrl());
	}
}

void CGradesReportView::addGrades(CDBHandling *pDB)
{
	GetReportCtrl().AddRecord(new CGradesReportRec(CGrades()));
	GetReportCtrl().Populate();
	GetReportCtrl().UpdateWindow();
}

void CGradesReportView::deleteGrades(CDBHandling *pDB)
{
	// Ask user twice, if he realy want's to delete
	if (::MessageBox(GetSafeHwnd(),m_sMsgDelete1,m_sMsgCap,MB_ICONQUESTION | MB_YESNO) == IDNO)
	{
		return;
	}
	else	if (::MessageBox(GetSafeHwnd(),m_sMsgDelete2,m_sMsgCap,MB_ICONQUESTION | MB_YESNO) == IDNO)
	{
		return;
	}

	CXTPReportRow *pRow = GetReportCtrl().GetFocusedRow();
	CGradesReportRec *rec = NULL;
	// We'll also add this new item to DB
	if (pDB != NULL && pRow != NULL)
	{
		rec = (CGradesReportRec*)pRow->GetRecord();
		if (pDB->delGrades(rec->getRecord()))
		{
			pDB->getGrades(m_vecGrades,GetReportCtrl());
			// Check if there's no grades left. If so, reset IDENTITY-field
			if (m_vecGrades.size() == 0)
				pDB->resetGradesIdentityField();
		}
	}
}

void CGradesReportView::saveGrades(CDBHandling *pDB)
{
	CXTPReportRecords *pRecs = GetReportCtrl().GetRecords();
	CGradesReportRec *rec = NULL;
	// We'll also add this new item to DB
	if (pDB != NULL && pRecs != NULL)
	{
		GetReportCtrl().Populate();
		for (int i = 0;i < pRecs->GetCount();i++)
		{
			rec = (CGradesReportRec*)pRecs->GetAt(i);
			if (rec->getRecord().getGradesID() == -1)
			{
				pDB->newGrades(CGrades(-1,rec->getColText(COLUMN_0),rec->getColText(COLUMN_1),rec->getColChecked(COLUMN_2)));
			}
			else if (rec->getRecord().getGradesID() > 0)
			{
				pDB->updGrades(CGrades(rec->getRecord().getGradesID(),rec->getColText(COLUMN_0),rec->getColText(COLUMN_1),rec->getColChecked(COLUMN_2)));
			}
		}

		pDB->getGrades(m_vecGrades,GetReportCtrl());
	}
}

