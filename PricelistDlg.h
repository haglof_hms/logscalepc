#if !defined(__PRICLISTFORMVIEW_H__)
#define __PRICELISTFORMVIEW_H__

#pragma once

#include "DerivedClasses.h"

#include "ReportClasses.h"

#include "DBHandling.h"

#pragma once

// CPricelistDlg form view

class CPricelistDlg : public CXTResizeDialog
{
	DECLARE_DYNCREATE(CPricelistDlg)

	// Data members
	BOOL m_bInitialized;
	CString m_sLangFN;
	
	CString m_sMsgCap;
	CString m_sMsgDelete1;
	CString m_sMsgDelete2;

	CButton m_btnOK;
	CButton m_btnCancel;

	CMyExtStatic m_lbl3_1;
	//CMyReportControl m_repPricelist;
	CMyReportControl m_repContract;

	CVecPricelist m_vecPricelist;
	CPricelist m_recSelPricelist;
	CTickets m_recTicket;

	CLogScaleTemplates m_recSelectedTmpl;
	vecLogScaleTemplates m_vecLogScaleTemplates;

	CDBHandling *m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;

	CImageList m_ilIcons;

	// Methods
	void setupReport();

	void populateReport();

public:
	CPricelistDlg(CTickets& rec = CTickets());           // protected constructor used by dynamic creation
	virtual ~CPricelistDlg();


	CPricelist getSelPricelist()	{ return m_recSelPricelist; }

	CLogScaleTemplates getSelTmpl()	{ return m_recSelectedTmpl; }

public:
	enum { IDD = IDD_DIALOG3 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	//{{AFX_VIRTUAL(CGradesReportView)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CGradesperSpcView)
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnDestroy();
	afx_msg void OnShowWindow(BOOL bShow,UINT nStatus);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
};


#endif