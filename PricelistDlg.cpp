// PricelistFormView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "PricelistDlg.h"

#include "selectcalctypedlg.h"

#include "ResLangFileReader.h"

// CPricelistDlg

IMPLEMENT_DYNCREATE(CPricelistDlg, CXTResizeDialog)

BEGIN_MESSAGE_MAP(CPricelistDlg, CXTResizeDialog)
	ON_WM_COPYDATA()
	ON_WM_DESTROY()
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDOK, OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, OnBnClickedButton2)
END_MESSAGE_MAP()

CPricelistDlg::CPricelistDlg(CTickets& rec)
	: CXTResizeDialog(CPricelistDlg::IDD),
		m_bInitialized(FALSE),
		m_sLangFN(L""),
		m_pDB(NULL),
		m_recTicket(rec)
{

}

CPricelistDlg::~CPricelistDlg()
{
}

BOOL CPricelistDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeDialog::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CPricelistDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL3_1, m_lbl3_1);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDC_BUTTON2, m_btnCancel);
	//}}AFX_DATA_MAP

}

BOOL CPricelistDlg::OnInitDialog()
{
	CXTResizeDialog::OnInitDialog();
	if (!m_bInitialized)
	{

		m_lbl3_1.SetBkColor(INFOBK);
		m_lbl3_1.SetTextColor(BLUE);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupReport();

		populateReport();

		m_bInitialized = TRUE;
	}

	return TRUE;
}

void CPricelistDlg::OnDestroy()
{
	CXTResizeDialog::OnDestroy();
}

void CPricelistDlg::OnShowWindow(BOOL bShow,UINT nStatus)
{
	m_repContract.SetFocus();
	CXTResizeDialog::OnShowWindow(bShow, nStatus);
}

BOOL CPricelistDlg::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{

	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		if (m_dbConnectionData.conn->isConnected())
		{
			m_pDB = new CDBHandling(m_dbConnectionData);
			if (m_pDB != NULL)
			{
				//m_pDB->getPricelist(m_vecPricelist);
				m_pDB->getTmplTables(m_vecLogScaleTemplates);
			}
		}
	}

	return CXTResizeDialog::OnCopyData(pWnd, pData);
}


// CPricelistDlg diagnostics

#ifdef _DEBUG
void CPricelistDlg::AssertValid() const
{
	CXTResizeDialog::AssertValid();
}

#ifndef _WIN32_WCE
void CPricelistDlg::Dump(CDumpContext& dc) const
{
	CXTResizeDialog::Dump(dc);
}
#endif
#endif //_DEBUG


// CPricelistDlg message handlers
// Create and add Assortment settings reportwindow
void CPricelistDlg::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;

	if (m_repContract.GetSafeHwnd() == 0)
	{

		// Create the sheet1 list box.
		if (!m_repContract.Create(this, ID_REPORT_PRICELIST, L"Pricelist_report"))
		{
			TRACE0( "Failed to create m_repContract.\n" );
			return;
		}
	}

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{

			SetWindowText(xml.str(IDS_STRING6106));

			m_lbl3_1.SetWindowText(xml.str(IDS_STRING1350) + L"\n" + xml.str(IDS_STRING1351));

			m_btnOK.SetWindowText(xml.str(IDS_STRING100));
			m_btnCancel.SetWindowText(xml.str(IDS_STRING101));

			if (m_repContract.GetSafeHwnd() != NULL)
			{

				VERIFY(m_ilIcons.Create(16,13, ILC_COLOR24|ILC_MASK, 0, 1));
				CBitmap bmp;
				VERIFY(bmp.LoadBitmap(IDB_BITMAP1));
				m_ilIcons.Add(&bmp, RGB(255, 0, 255));

				m_repContract.SetImageList(&m_ilIcons);

				m_repContract.ShowWindow( SW_NORMAL );

				// Name of pricelist
				pCol = m_repContract.AddColumn(new CXTPReportColumn(COLUMN_0, xml.str(IDS_STRING1300), 180,FALSE,XTP_REPORT_NOICON,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Created by
				pCol = m_repContract.AddColumn(new CXTPReportColumn(COLUMN_1, xml.str(IDS_STRING1301), 100,FALSE,XTP_REPORT_NOICON,FALSE));
				pCol->AllowRemove(FALSE);
				// Date
				pCol = m_repContract.AddColumn(new CXTPReportColumn(COLUMN_2, xml.str(IDS_STRING1302), 100,FALSE,XTP_REPORT_NOICON,FALSE));
				pCol->AllowRemove(FALSE);

				m_repContract.GetReportHeader()->AllowColumnRemove(FALSE);
				m_repContract.SetMultipleSelection( FALSE );
				m_repContract.SetGridStyle( TRUE, xtpReportGridSolid );
				m_repContract.SetGridStyle( FALSE, xtpReportGridSmallDots );
				m_repContract.FocusSubItems(TRUE);
				m_repContract.AllowEdit(FALSE);
				m_repContract.GetPaintManager()->SetFixedRowHeight(FALSE);

			}	// if (m_repContract.GetSafeHwnd() != NULL)

			xml.clean();
			
			RECT rect;
			GetClientRect(&rect);
			setResize(&m_repContract,3,50,rect.right-3,rect.bottom-90);

			m_repContract.SetFocus();


		}
	}
}

void CPricelistDlg::populateReport()
{
	int nSelectedRow = -1;
	m_repContract.ResetContent();
	if (m_vecLogScaleTemplates.size() > 0)
	{
		for (UINT i1 = 0;i1 < m_vecLogScaleTemplates.size();i1++)
		{
			CLogScaleTemplates rec = m_vecLogScaleTemplates[i1];
			m_repContract.AddRecord(new CSelectContractReportRec(rec));
			if (rec.getTmplName().CompareNoCase(m_recTicket.getTmplUsed()) == 0)
				nSelectedRow = i1;
		}	// for (UINT i1 = 0;i1 < m_vecLogScaleTemplates.size();i1++)
	}	// if (m_vecLogScaleTemplates.size() > 0)
	m_repContract.Populate();
	m_repContract.UpdateWindow();

	if (nSelectedRow < m_repContract.GetRows()->GetCount())
	{
		m_repContract.SetFocusedRow(m_repContract.GetRows()->GetAt(nSelectedRow));
		m_repContract.SetFocusedColumn(m_repContract.GetColumns()->GetAt(COLUMN_0));
	}
}

void CPricelistDlg::OnBnClickedButton1()
{
	m_recSelPricelist = CPricelist();
	CSelectContractReportRec *pRec = NULL;
	CXTPReportRow *pRow = m_repContract.GetFocusedRow();
	if (pRow != NULL)
	{
		if (pRow->GetIndex() >= 0 && pRow->GetIndex() < m_vecLogScaleTemplates.size())
		{
			m_recSelectedTmpl = m_vecLogScaleTemplates[pRow->GetIndex()];
		}
	}


	OnOK();
}

void CPricelistDlg::OnBnClickedButton2()
{
	OnCancel();
}
