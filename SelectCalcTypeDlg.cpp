// SelectCalcTypeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SelectCalcTypeDlg.h"

#include "ResLangFileReader.h"

#include "reportclasses.h"
// CSelectCalcTypeDlg dialog

IMPLEMENT_DYNAMIC(CSelectCalcTypeDlg, CDialog)

BEGIN_MESSAGE_MAP(CSelectCalcTypeDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CSelectCalcTypeDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_REMOVE, &CSelectCalcTypeDlg::OnBnClickedRemove)
END_MESSAGE_MAP()


CSelectCalcTypeDlg::CSelectCalcTypeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectCalcTypeDlg::IDD, pParent),
		m_sLangFN(L""),
		m_bInitialized(FALSE),
		m_sCalcTypeSet(L""),
		m_sCalcBasisSelected(L"")
{
	m_bIsPulpwood = FALSE;
	m_nFuncID = -1;	//#4258
}

CSelectCalcTypeDlg::~CSelectCalcTypeDlg()
{
}

void CSelectCalcTypeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelectCalcTypeDlg)
	DDX_Control(pDX, IDOK, m_wndOKBtn);
	DDX_Control(pDX, IDCANCEL, m_wndCancelBtn);
	DDX_Control(pDX, IDC_REMOVE, m_wndRemoveBtn);
	//}}AFX_DATA_MAP

}

// CSelectCalcTypeDlg message handlers
BOOL CSelectCalcTypeDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CXTPReportColumn *pCol = NULL;

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (!m_bInitialized)
	{

		if (m_repCalcTypes.GetSafeHwnd() == 0)
		{

			// Create the sheet1 list box.
			if (!m_repCalcTypes.Create(this, ID_REPORT_CALCTYPES, L"CalcTypes_report"))
			{
				TRACE0( "Failed to create m_repCalcTypes.\n" );
				return FALSE;
			}
		}

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				SetWindowText(xml.str(IDS_STRING1400));

				if (m_repCalcTypes.GetSafeHwnd() != NULL)
				{

					m_repCalcTypes.ShowWindow( SW_NORMAL );

					// Calculation name
					pCol = m_repCalcTypes.AddColumn(new CXTPReportColumn(COLUMN_0, xml.str(IDS_STRING1401), 160,TRUE,XTP_REPORT_NOICON,FALSE));
					pCol->AllowRemove(FALSE);
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					// Cal
					pCol = m_repCalcTypes.AddColumn(new CXTPReportColumn(COLUMN_1, xml.str(IDS_STRING1402), 100,TRUE,XTP_REPORT_NOICON,FALSE));
					pCol->AllowRemove(FALSE);
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;
					// Calculation basis
					pCol = m_repCalcTypes.AddColumn(new CXTPReportColumn(COLUMN_2, xml.str(IDS_STRING1403), 100,TRUE,XTP_REPORT_NOICON,FALSE));
					pCol->AllowRemove(FALSE);
					pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
					pCol->GetEditOptions()->m_bAllowEdit = FALSE;


					m_repCalcTypes.GetReportHeader()->AllowColumnRemove(FALSE);
					m_repCalcTypes.GetReportHeader()->AllowColumnReorder(FALSE);
					m_repCalcTypes.GetReportHeader()->AllowColumnResize( FALSE );
					m_repCalcTypes.GetReportHeader()->AllowColumnSort( FALSE );
					m_repCalcTypes.GetReportHeader()->SetAutoColumnSizing( TRUE );
					m_repCalcTypes.SetMultipleSelection( FALSE );
					m_repCalcTypes.SetGridStyle( TRUE, xtpReportGridSolid );
					m_repCalcTypes.SetGridStyle( FALSE, xtpReportGridSmallDots );
					m_repCalcTypes.FocusSubItems(FALSE);
					m_repCalcTypes.AllowEdit(FALSE);
					m_repCalcTypes.GetPaintManager()->SetFixedRowHeight(FALSE);

					CRect rect;
					GetClientRect(&rect);
					setResize(&m_repCalcTypes,2,2,rect.right-4,rect.bottom-35);

					// Setup List
					int nIndex = -1,nSelItemIndex = -1;
					CCalcTypeReportRec* pRec = NULL;
					CCalcTypeReportRec* pRecSelection = NULL;
					if (m_vecFuncDesc.size() > 0)
					{
						for (UINT i = 0;i < m_vecFuncDesc.size();i++)
						{
							pRec = new CCalcTypeReportRec(m_vecFuncDesc[i].getFuncID(),m_vecFuncDesc[i].getFullName(),m_vecFuncDesc[i].getAbbrevName(),m_vecFuncDesc[i].getBasis());
							if (pRec != NULL)
							{
								if (!m_bIsPulpwood)
								{
									m_repCalcTypes.AddRecord(pRec);
								}		
								else if (m_vecFuncDesc[i].getOnlySW() == 0)
								{
									m_repCalcTypes.AddRecord(pRec);
								}			
								if(m_vecFuncDesc[i].getFuncID() == m_nFuncID)	//#4258	anv�nda FuncID ist�llet f�r att hitta aktuell funktion
								//if (m_sFuncDescSet.CompareNoCase(m_vecFuncDesc[i].getBasis()) == 0)	
								{
									pRecSelection = pRec;
								}
							}
						}

						m_repCalcTypes.Populate();
						m_repCalcTypes.UpdateWindow();
						if (pRecSelection != NULL)
						{
							CXTPReportRow *pRow = m_repCalcTypes.GetRows()->Find(pRecSelection);
							if (pRow != NULL)
							{
								m_repCalcTypes.SetFocusedRow(pRow);
								m_repCalcTypes.SetFocus();
								m_repCalcTypes.UpdateWindow();
							}	// if (pRow != NULL)
						}
					}
				}


				m_wndOKBtn.SetWindowText(xml.str(IDS_STRING100));
				m_wndCancelBtn.SetWindowText(xml.str(IDS_STRING101));
				m_wndRemoveBtn.SetWindowText(xml.str(IDS_STRING102));
				m_wndRemoveBtn.ShowWindow(SW_HIDE);

			}
			xml.clean();
		}

		m_bInitialized = TRUE;
	}

	return TRUE;
}


void CSelectCalcTypeDlg::setVecCalcTypes(CVecCalcTypes &vec,LPCTSTR calc_type_set)	
{ 
	m_vecCalcTypes = vec; 
	m_sCalcTypeSet = calc_type_set;
}

void CSelectCalcTypeDlg::setVecFuncDesc(vecFuncDesc &vec,LPCTSTR func_set, int func_id)	//#4258 lagt till func_id
{
	m_vecFuncDesc = vec; 
	m_sFuncDescSet = func_set;
	m_nFuncID = func_id;	
}


void CSelectCalcTypeDlg::OnBnClickedOk()
{
	// Find selected item
	CCalcTypeReportRec *pRec = (CCalcTypeReportRec*)m_repCalcTypes.GetFocusedRow()->GetRecord();
	if (pRec != NULL)
	{
		m_sCalcBasisSelected = pRec->getColText(COLUMN_2);
		m_nFuncID = pRec->getID();	//#4258 s�tt aktuellt FuncID
	}
	OnOK();
}

void CSelectCalcTypeDlg::OnBnClickedRemove()
{
	// Remove any Basis and quit
	m_sCalcBasisSelected.Empty();
	OnOK();
}
