#pragma once

#include "Resource.h"

// CGradesperSpcView form view

class CGradesperSpcView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CGradesperSpcView)

	// Data members
	BOOL m_bInitialized;
	CString m_sLangFN;

	CMyExtStatic m_lblSpecies;
	CMyExtStatic m_lblGrades;
	CMyReportControl m_repSpecies;
	CMyReportControl m_repGrades;

	CImageList m_ilIcons;

	// Methods
	void setupReports();

protected:
	CGradesperSpcView();           // protected constructor used by dynamic creation
	virtual ~CGradesperSpcView();

public:
	enum { IDD = IDD_FORMVIEW2 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

	CMyReportControl& repCtrlSpecies()	{ return m_repSpecies; }
	CMyReportControl& repCtrlGrades()	{ return m_repGrades; }

protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGradesperSpcView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CGradesperSpcView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnClose();
	afx_msg void OnDestroy(void);

	afx_msg void OnReportClick(NMHDR* pNotifyStruct,LRESULT * /*result*/);


	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


