// MatchPrlGrdDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MatchPrlGrdDlg.h"

#include "ResLangFileReader.h"

#include "ReportClasses.h"

#include <algorithm>

#include <sstream>

// Function to sort vecLanguages, makin' sure we get
// languages in ascending order; 081020 p�d
bool SortSpeciesData(CSpecies& l1, CSpecies& l2)
{
	return l1.getSpcID() < l2.getSpcID();
}

bool SortGradesData(CGradesExt& l1, CGradesExt& l2)
{
	return l1.getGradesID() < l2.getGradesID();
}

// CMatchPrlGrdDlg dialog

IMPLEMENT_DYNAMIC(CMatchPrlGrdDlg, CXTResizeDialog)

BEGIN_MESSAGE_MAP(CMatchPrlGrdDlg, CXTResizeDialog)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, ID_REPORT_SETUP_SPECIES2, OnReportSettingSpc)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, ID_REPORT_SETUP_GRADES2, OnReportSettingGrd)
	ON_BN_CLICKED(IDOK, &CMatchPrlGrdDlg::OnBnClickedOk)
END_MESSAGE_MAP()

CMatchPrlGrdDlg::CMatchPrlGrdDlg(CWnd* pParent /*=NULL*/)
	: CXTResizeDialog(CMatchPrlGrdDlg::IDD, pParent),
		m_bInitialized(FALSE),
		m_bSpcSelected(FALSE),
		m_bGradesSelected(FALSE),
		m_pDB(NULL)
{

}

CMatchPrlGrdDlg::~CMatchPrlGrdDlg()
{
}

void CMatchPrlGrdDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMatchPrlGrdDlg)
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_LBL20_1, m_lbl20_1);
	//}}AFX_DATA_MAP

}

BOOL CMatchPrlGrdDlg::OnInitDialog()
{
	CXTResizeDialog::OnInitDialog();
	if (!m_bInitialized)
	{
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		m_lbl20_1.SetBkColor(INFOBK);
		m_lbl20_1.SetTextColor(BLUE);
		m_lbl20_1.SetLblFontEx(14);

		setupReport();
		
		populateReport();
		
		m_bInitialized = TRUE;
	}

	return TRUE;
}

// CMatchPrlGrdDlg message handlers

void CMatchPrlGrdDlg::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;
	RECT rect;
	GetClientRect(&rect);

	if (m_repSpecies.GetSafeHwnd() == 0)
	{

		// Create the sheet1 list box.
		if (!m_repSpecies.Create(this, ID_REPORT_SETUP_SPECIES2, L"repSpecies_report"))
		{
			TRACE0( "Failed to create m_repSpecies.\n" );
			return;
		}
	}

	if (m_repGrades.GetSafeHwnd() == 0)
	{

		// Create the sheet1 list box.
		if (!m_repGrades.Create(this, ID_REPORT_SETUP_GRADES2, L"repGrades_report"))
		{
			TRACE0( "Failed to create m_repGrades.\n" );
			return;
		}
	}

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			// Set dialog caption
			SetWindowText(xml.str(IDS_STRING61300));

			m_btnOK.SetWindowText(xml.str(IDS_STRING100));
			m_btnCancel.SetWindowText(xml.str(IDS_STRING101));
			m_btnOK.EnableWindow(TRUE);

			m_sMissmatch = xml.str(IDS_STRING61330) + L" " + xml.str(IDS_STRING61331);
			m_sAllOK = xml.str(IDS_STRING61332) + L"\n" + xml.str(IDS_STRING61333);

			m_lbl20_1.SetWindowTextW(m_sAllOK);

			m_sConstraintAdd =  L"<" + xml.str(IDS_STRING61320) + L">";

			if (m_repSpecies.GetSafeHwnd() != NULL)
			{

				m_repSpecies.ShowWindow( SW_NORMAL );

				// Species in file
				pCol = m_repSpecies.AddColumn(new CXTPReportColumn(COLUMN_0, xml.str(IDS_STRING61301), 150,FALSE,XTP_REPORT_NOICON,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Species selection
				pCol = m_repSpecies.AddColumn(new CXTPReportColumn(COLUMN_1, xml.str(IDS_STRING61302), 150,FALSE,XTP_REPORT_NOICON,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->GetEditOptions()->AddComboButton();

				m_repSpecies.GetReportHeader()->AllowColumnRemove(FALSE);
				m_repSpecies.SetMultipleSelection( FALSE );
				m_repSpecies.SetGridStyle( TRUE, xtpReportGridSolid );
				m_repSpecies.SetGridStyle( FALSE, xtpReportGridSmallDots );
				m_repSpecies.FocusSubItems(TRUE);
				m_repSpecies.AllowEdit(TRUE);
				m_repSpecies.GetPaintManager()->SetFixedRowHeight(FALSE);

			}	// if (m_repSpecies.GetSafeHwnd() != NULL)

			// Strings
	
			setResize(&m_repSpecies,2,35,rect.right/2-25,rect.bottom-70);

			if (m_repGrades.GetSafeHwnd() != NULL)
			{

				m_repGrades.ShowWindow( SW_NORMAL );

				// Pulpwood
				pCol = m_repGrades.AddColumn(new CXTPReportColumn(COLUMN_0, xml.str(IDS_STRING61305), 60,FALSE,XTP_REPORT_NOICON,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				// Grades in file
				pCol = m_repGrades.AddColumn(new CXTPReportColumn(COLUMN_1, xml.str(IDS_STRING61303), 150,FALSE,XTP_REPORT_NOICON,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Grades selection
				pCol = m_repGrades.AddColumn(new CXTPReportColumn(COLUMN_2, xml.str(IDS_STRING61304), 145,FALSE,XTP_REPORT_NOICON,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->GetEditOptions()->AddComboButton();

				m_repGrades.GetReportHeader()->AllowColumnRemove(FALSE);
				m_repGrades.SetMultipleSelection( FALSE );
				m_repGrades.SetGridStyle( TRUE, xtpReportGridSolid );
				m_repGrades.SetGridStyle( FALSE, xtpReportGridSmallDots );
				m_repGrades.FocusSubItems(TRUE);
				m_repGrades.AllowEdit(TRUE);
				m_repGrades.GetPaintManager()->SetFixedRowHeight(FALSE);

			}	// if (m_repGrades.GetSafeHwnd() != NULL)

			// Strings

			xml.clean();
			
			setResize(&m_repGrades,rect.right/2-10,35,rect.right-(rect.right/2),rect.bottom-70);
		}

		if (m_pDB != NULL)
		{
			m_pDB->getSpecies(m_vecSpecies);
			m_pDB->getGrades(m_vecGrades);
		}

	}
}

void CMatchPrlGrdDlg::populateReport(void)
{
	CSetupSpeciesReportRec *pRecSpc = NULL;
	CSetupGradesReportRec2 *pRecGrd = NULL;
	CString sLastSpcCode = L"";
	CString sLastGrdCode = L"";
	CSpecies recSpc = CSpecies();
	CGradesExt recGrd = CGradesExt();
	int nIndex = -1;
	if (m_vecImportSpecies.size() > 0)
	{
		std::sort(m_vecImportSpecies.begin(),m_vecImportSpecies.end(),SortSpeciesData);
		for (UINT i = 0;i < m_vecImportSpecies.size();i++)
		{
			recSpc = m_vecImportSpecies[i];
			if (sLastSpcCode.CompareNoCase(recSpc.getSpcCode()) != 0)
			{
				nIndex = addSpeciesConstraits(recSpc);
				if (nIndex == -1)
				{
					m_lbl20_1.SetWindowTextW(m_sMissmatch);
					m_btnOK.EnableWindow(FALSE);
				}
				if ((pRecSpc = (CSetupSpeciesReportRec*)m_repSpecies.AddRecord(new CSetupSpeciesReportRec(i,recSpc))) != NULL)
				{
					if (nIndex > -1 && nIndex < m_vecSpecies.size())
					{
						pRecSpc->setColText(COLUMN_1,getSpcCodeAndName(m_vecSpecies[nIndex].getSpcID()));
					}
				}
			}
			sLastSpcCode = recSpc.getSpcCode();
		}
		m_repSpecies.Populate();
		m_repSpecies.UpdateWindow();
	}
	if (m_vecImportGrades.size() > 0)
	{
		std::sort(m_vecImportGrades.begin(),m_vecImportGrades.end(),SortGradesData);
		for (UINT i = 0;i < m_vecImportGrades.size();i++)
		{
			recGrd = m_vecImportGrades[i];
			if (sLastGrdCode.CompareNoCase(recGrd.getGradesCode()) != 0)
			{
				nIndex = addGradesConstraits(recGrd);
				if (nIndex == -1)
				{
					m_lbl20_1.SetWindowTextW(m_sMissmatch);
					m_btnOK.EnableWindow(FALSE);
				}
				if ((pRecGrd = (CSetupGradesReportRec2 *)m_repGrades.AddRecord(new CSetupGradesReportRec2(i,recGrd))) != NULL)
				{
					if (nIndex > -1 && nIndex < m_vecGrades.size())
					{
						pRecGrd->setColText(COLUMN_2,getGradesCodeAndName(m_vecGrades[nIndex].getGradesID()));
					}
				}
			}
			sLastGrdCode = recGrd.getGradesCode();
		}

		m_repGrades.Populate();
		m_repGrades.UpdateWindow();
	}

}


CString CMatchPrlGrdDlg::getSpcCodeAndName(int spc_id)
{
	CString sTmp = L"";
	if (m_vecSpecies.size() > 0)
	{
		for (UINT i = 0;i < m_vecSpecies.size();i++)
		{
			if (m_vecSpecies[i].getSpcID() == spc_id)
			{
					if (!m_vecSpecies[i].getSpcName().IsEmpty())
						sTmp.Format(L"%s - %s",m_vecSpecies[i].getSpcCode(),m_vecSpecies[i].getSpcName());
					else
						sTmp.Format(L"%s",m_vecSpecies[i].getSpcCode());
				return sTmp;
			}
		}
	}
	return L"";
}

CString CMatchPrlGrdDlg::getGradesCodeAndName(int grade_id)
{
	CString sTmp = L"";
	if (m_vecGrades.size() > 0)
	{
		for (UINT i = 0;i < m_vecGrades.size();i++)
		{
			if (m_vecGrades[i].getGradesID() == grade_id)
			{
				if (!m_vecGrades[i].getGradesName().IsEmpty())
					sTmp.Format(L"%s - %s",m_vecGrades[i].getGradesCode(),m_vecGrades[i].getGradesName());
				else
					sTmp.Format(L"%s",m_vecGrades[i].getGradesCode());
				return sTmp;
			}
		}
	}
	return L"";
}

void CMatchPrlGrdDlg::getMissmatchGrades(int grade_id,LPCTSTR spc_code,CGradesExt& rec)
{
	rec = CGradesExt();
	for (UINT i = 0;i < m_vecImportGrades.size();i++)
	{
		if (m_vecImportGrades[i].getGradesID() == grade_id && m_vecImportGrades[i].getSpcCode().CompareNoCase(spc_code) == 0)
			rec = m_vecImportGrades[i];
	}
}


int CMatchPrlGrdDlg::addSpeciesConstraits(CSpecies rec)
{
	CString sSpcCodeAndNameA = L"",sSpcCode = L"";
	CXTPReportRecordItemConstraints *pCons = NULL;
	CXTPReportRecordItemConstraint *pCon = NULL;
	CXTPReportColumns *pColumns = m_repSpecies.GetColumns();
	CXTPReportRows *pRows = m_repSpecies.GetRows();
	int nIndex = -1;
	if (pColumns != NULL)
	{
		CXTPReportColumn *pSpcCol = pColumns->Find(COLUMN_1);
		if (pSpcCol != NULL)
		{
			// Get constraints for Specie column and remove all items; 111129 p�d
			if ((pCons = pSpcCol->GetEditOptions()->GetConstraints()) != NULL)
			{
				pCons->RemoveAll();
			}	// if (pCons != NULL)
			// If there's species, add to Specie column in report; 111129 p�d
			if (m_vecSpecies.size() > 0)
			{
				pSpcCol->GetEditOptions()->AddConstraint(m_sConstraintAdd,(int)-1);
				for (UINT i = 0;i < m_vecSpecies.size();i++)
				{
					sSpcCodeAndNameA = getSpcCodeAndName(m_vecSpecies[i].getSpcID());
					pSpcCol->GetEditOptions()->AddConstraint(sSpcCodeAndNameA,(int)i);
				}	// for (UINT i = 0;i < m_vecSpecies.size();i++)

				for (UINT i = 0;i < m_vecSpecies.size();i++)
				{
					sSpcCode = m_vecSpecies[i].getSpcCode().Trim();
					if (sSpcCode.CompareNoCase(rec.getSpcCode().Trim()) == 0)
					{
						nIndex = i;
						break;
					}
				}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
			}	// if (m_vecSpecies.size() > 0)
		}	// if (pSpcCol != NULL)
	}	// if (pColumns != NULL)

	return nIndex;
}

int CMatchPrlGrdDlg::addGradesConstraits(CGradesExt rec)
{
	CString sGrdCode = L"";
	int nIndex = -1;
	CXTPReportRecordItemConstraints *pCons = NULL;
	CXTPReportColumns *pColumns = m_repGrades.GetColumns();
	CXTPReportRows *pRows = m_repGrades.GetRows();
	if (pColumns != NULL)
	{
		CXTPReportColumn *pGradeCol = pColumns->Find(COLUMN_2);
		if (pGradeCol != NULL)
		{
			// Get constraints for Specie column and remove all items; 111129 p�d
			pCons = pGradeCol->GetEditOptions()->GetConstraints();
			if (pCons != NULL)
			{
				pCons->RemoveAll();
			}	// if (pCons != NULL)

			// If there's grades, add to Grades column in report; 111129 p�d
			if (m_vecGrades.size() > 0)
			{
				pGradeCol->GetEditOptions()->AddConstraint(m_sConstraintAdd,(int)-1);
				for (UINT i = 0;i < m_vecGrades.size();i++)
				{
					pGradeCol->GetEditOptions()->AddConstraint(getGradesCodeAndName(m_vecGrades[i].getGradesID()),(int)i); //m_vecGrades[i].getGradesID());
				}	// for (UINT i = 0;i < m_vecSpecies.size();i++)

				for (UINT i = 0;i < m_vecGrades.size();i++)
				{
					sGrdCode = m_vecGrades[i].getGradesCode();
					if (sGrdCode.CompareNoCase(rec.getGradesCode().Trim()) == 0)
					{
						nIndex = i;
						break;
					}

				}	// for (UINT i = 0;i < m_vecSpecies.size();i++)

			}	// if (m_vecSpecies.size() > 0)
		}	// if (pSpcCol != NULL)

	}	// if (pColumns != NULL)

	return nIndex;

}

void CMatchPrlGrdDlg::OnReportSettingSpc(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	BOOL bSpcSelected = FALSE,bGradesSelected = FALSE;
	CXTPReportRows *pRows = NULL;
	CSetupSpeciesReportRec *pRecSpc = NULL;
	CSetupGradesReportRec2 *pRecGrd = NULL;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	// prevents us from crashing when clicking inside the report, but not on an item
	if(pItemNotify == NULL) 
		return;

	// prevents us from crashing when clicking inside the report, but not on an item
	if(pItemNotify->pItem == NULL)
		return;

	if ((pRows = m_repSpecies.GetRows()) != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			if ((pRecSpc = (CSetupSpeciesReportRec *)pRows->GetAt(i)->GetRecord()) != NULL)
			{
				if (pRecSpc->getColText(COLUMN_1).IsEmpty())
				{
					m_bSpcSelected = FALSE;
					break;
				}	// if (pRecSpc->getColText().IsEmpty())
				else
				{
					m_bSpcSelected = TRUE;
				}
			}	// if ((pRecSpc = (CSetupSpeciesReportRec *)pRows->GetAt(i)->GetRecord()) != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)
	}	// if ((pRows = m_repSpecies.GetRows()) != NULL)

	if ((pRows = m_repGrades.GetRows()) != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			if ((pRecGrd = (CSetupGradesReportRec2 *)pRows->GetAt(i)->GetRecord()) != NULL)
			{
				if (pRecGrd->getColText(COLUMN_2).IsEmpty())
				{
					m_bGradesSelected = FALSE;
					break;
				}	// if (pRecSpc->getColText().IsEmpty())
				else
				{
					m_bGradesSelected = TRUE;
				}
			}	// if ((pRecSpc = (CSetupSpeciesReportRec *)pRows->GetAt(i)->GetRecord()) != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)
	}	// if ((pRows = m_repSpecies.GetRows()) != NULL)

	m_btnOK.EnableWindow(m_bSpcSelected && m_bGradesSelected);
}

void CMatchPrlGrdDlg::OnReportSettingGrd(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	CXTPReportRows *pRows = NULL;
	CSetupSpeciesReportRec *pRecSpc = NULL;
	CSetupGradesReportRec2 *pRecGrd = NULL;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	// prevents us from crashing when clicking inside the report, but not on an item
	if(pItemNotify == NULL) 
		return;

	if ((pRows = m_repGrades.GetRows()) != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			if ((pRecGrd = (CSetupGradesReportRec2 *)pRows->GetAt(i)->GetRecord()) != NULL)
			{
				if (pRecGrd->getColText(COLUMN_2).IsEmpty())
				{
					m_bGradesSelected = FALSE;
					break;
				}	// if (pRecSpc->getColText().IsEmpty())
				else
				{
					m_bGradesSelected = TRUE;
				}
			}	// if ((pRecSpc = (CSetupSpeciesReportRec *)pRows->GetAt(i)->GetRecord()) != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)
	}	// if ((pRows = m_repSpecies.GetRows()) != NULL)

	if ((pRows = m_repSpecies.GetRows()) != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			if ((pRecSpc = (CSetupSpeciesReportRec *)pRows->GetAt(i)->GetRecord()) != NULL)
			{
				if (pRecSpc->getColText(COLUMN_1).IsEmpty())
				{
					m_bSpcSelected = FALSE;
					break;
				}	// if (pRecSpc->getColText().IsEmpty())
				else
				{
					m_bSpcSelected = TRUE;
				}
			}	// if ((pRecSpc = (CSetupSpeciesReportRec *)pRows->GetAt(i)->GetRecord()) != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)
	}	// if ((pRows = m_repSpecies.GetRows()) != NULL)


	m_btnOK.EnableWindow(m_bSpcSelected && m_bGradesSelected);

}

void CMatchPrlGrdDlg::OnBnClickedOk()
{
	CString sTmp = L"",sFuncID = L"",S;
	CSpecies recSpc = CSpecies();
	CGrades recGrd = CGrades();
	CGradesExt recGrdExt = CGradesExt();

	CSetupSpeciesReportRec *pRecSpc = NULL;
	CXTPReportRecordItemConstraint *pConsSpc = NULL;
	CXTPReportColumns *pColsSpc = NULL;
	CXTPReportColumn *pColSpc = NULL;
	CXTPReportRows *pRowsSpc = NULL;
	
	CSetupGradesReportRec2 *pRecGrd = NULL;
	CXTPReportRecordItemConstraint *pConsGrd = NULL;
	CXTPReportColumns *pColsGrd = NULL;
	CXTPReportColumn *pColGrd = NULL;
	CXTPReportRows *pRowsGrd = NULL;
	int nSpcIndex = -1,nNewSpcID = -1;
	int nGrdIndex = -1,nNewGrdID = -1;
	int nFuncID = -1;

	m_sData.Empty();

	if ((pColsSpc = m_repSpecies.GetColumns()) == NULL) return;
	if ((pColSpc = pColsSpc->Find(COLUMN_1)) == NULL) return;

	if ((pColsGrd = m_repGrades.GetColumns()) == NULL) return;
	if ((pColGrd = pColsGrd->Find(COLUMN_2)) == NULL) return;

	//----------------------------------------------------------------------------
	// Check out Species entered. If index equals 0 the items is set to <Add>
	if (((pRowsSpc = m_repSpecies.GetRows()) != NULL) && (pRowsGrd = m_repGrades.GetRows()) != NULL)
	{
		/////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Check if user's adding any species, in import-file but not included in database
		for (int i1 = 0;i1 < pRowsSpc->GetCount();i1++)
		{
			if ((pRecSpc = (CSetupSpeciesReportRec*)pRowsSpc->GetAt(i1)->GetRecord()) != NULL)
			{
				if ((pConsSpc = pColSpc->GetEditOptions()->FindConstraint(pRecSpc->getColText(COLUMN_1))) != NULL)
				{
					nSpcIndex = pConsSpc->GetIndex();
					// Check if the constraint index points to <Add>. 
					if (nSpcIndex == 0)
					{
						// This species we'll add to the database.
						// A species not in database.
						if (m_pDB != NULL)
						{
							if (m_pDB->newSpecies(CSpecies(-1,pRecSpc->getSpcRecord().getSpcCode(),pRecSpc->getSpcRecord().getSpcName(),
																							  pRecSpc->getSpcRecord().getBarkReductionInch(),pRecSpc->getSpcRecord().getBarkReductionMM())))
							{
								// Get last entered species id, i.e. primary key
								nNewSpcID = m_pDB->getLastSpeciesID();
								m_pDB->getSpecies(m_vecSpecies);
								addSpeciesConstraits(CSpecies());
								pRecSpc->setColText(COLUMN_1,getSpcCodeAndName(nNewSpcID));
								m_repSpecies.Populate();
								m_repSpecies.UpdateWindow();
							}	// if (m_pDB->newSpecies(CSpecies(-1,pRecSpc->getSpcRecord().getSpcCode(),pRecSpc->getSpcRecord().getSpcName(),0.0,0.0)))
						}	// if (m_pDB != NULL)					
					}	// if (nSpcIndex == 0)
				}
			}	// if ((pRecSpc = (CSetupSpeciesReportRec*)pRows->GetAt(i)->GetRecord()) != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)

		/////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Check if user's adding any grades, in import-file but not included in database
		for (int i2 = 0;i2 < pRowsGrd->GetCount();i2++)
		{	
			if ((pRecGrd = (CSetupGradesReportRec2*)pRowsGrd->GetAt(i2)->GetRecord()) != NULL)
			{
				if ((pConsGrd = pColGrd->GetEditOptions()->FindConstraint(pRecGrd->getColText(COLUMN_2))) != NULL)
				{
					nGrdIndex = pConsGrd->GetIndex();
					// Check if the constraint index points to <Add>. 
					// This grade is set in Imported file and includes
					// price and function for this grade.
					if (nGrdIndex == 0)
					{
						// This grades we'll add to the database.
						if (m_pDB != NULL)
						{									
							if (m_pDB->newGrades(CGrades(-1,pRecGrd->getGrdRecord().getGradesCode(),pRecGrd->getGrdRecord().getGradesName(),pRecGrd->getColChecked(COLUMN_0))))
							{
								// Get last entered species id, i.e. primary key
								nNewGrdID = m_pDB->getLastGradesID();
								m_pDB->getGrades(m_vecGrades);
								addGradesConstraits(pRecGrd->getGrdRecord());
								pRecGrd->setColText(COLUMN_2,getGradesCodeAndName(nNewGrdID));
								m_repGrades.Populate();
								m_repGrades.UpdateWindow();
							}	// if (m_pDB->newGrades(CGrades(-1,pRecGrd->getGrdRecord().getGradesCode(),pRecGrd->getGrdRecord().getGradesName(),pRecGrd->getColChecked(COLUMN_0))))									
						}	// if (m_pDB != NULL)					
					}	// if (nGrdIndex == 0)
				}	// if ((pConsGrd = pColGrd->GetEditOptions()->FindConstraint(pRecGrd->getColText(COLUMN_2))) != NULL)
			}	// if ((pRecGrd = (CSetupGradesReportRec2*)pRowsGrd->GetAt(i1)->GetRecord()) != NULL)
		}	// for (int i2 = 0;i2 < pRowsGrd->GetCount();i2++)

		/////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Start setting up the data
		for (int i1 = 0;i1 < pRowsSpc->GetCount();i1++)
		{
			if ((pRecSpc = (CSetupSpeciesReportRec*)pRowsSpc->GetAt(i1)->GetRecord()) != NULL)
			{
				if ((pConsSpc = pColSpc->GetEditOptions()->FindConstraint(pRecSpc->getColText(COLUMN_1))) != NULL)
				{
					nSpcIndex = pConsSpc->GetIndex();
					if (nSpcIndex > 0)
					{
						if (nSpcIndex > 0 && nSpcIndex < m_vecSpecies.size()+1)
						{
							recSpc = m_vecSpecies[nSpcIndex-1];
							sTmp.Format(L"%d;%d;%s;%s;\n",FUNC_INDEX::SPECIES,recSpc.getSpcID(),recSpc.getSpcCode(),recSpc.getSpcName());
							m_sData += sTmp;
						}					
					}	// if (nSpcIndex > 0)
				}

				for (int i2 = 0;i2 < pRowsGrd->GetCount();i2++)
				{	
					if ((pRecGrd = (CSetupGradesReportRec2*)pRowsGrd->GetAt(i2)->GetRecord()) != NULL)
					{
						if ((pConsGrd = pColGrd->GetEditOptions()->FindConstraint(pRecGrd->getColText(COLUMN_2))) != NULL)
						{
							nGrdIndex = pConsGrd->GetIndex();
							if (nGrdIndex > 0)
							{
								if (nGrdIndex > 0 && nGrdIndex < m_vecGrades.size()+1)
								{
									recGrd = m_vecGrades[nGrdIndex-1];				

									getMissmatchGrades(pRecGrd->getGrdRecord().getGradesID(),pRecSpc->getSpcRecord().getSpcCode().Trim(),recGrdExt);
									nFuncID = recGrdExt.getFuncID();
									if (recGrd.getGradesID() ==	pRecGrd->getGrdRecord().getGradesID())
										sTmp.Format(L"%d;%d;%d;%.3f;%d;\n",FUNC_INDEX::DATA,recSpc.getSpcID(),recGrdExt.getGradesID(),recGrdExt.getPrice(),nFuncID);
									else
										sTmp.Format(L"%d;%d;%d;%.3f;%d;\n",FUNC_INDEX::DATA,recSpc.getSpcID(),recGrd.getGradesID(),recGrdExt.getPrice(),nFuncID);
									m_sData += sTmp;
								}	// if (nGrdIndex > 0 && nGrdIndex < m_vecGrades.size()+1)
							}	// if (nGrdIndex > 0)						
						}	// if ((pConsGrd = pColGrd->GetEditOptions()->FindConstraint(pRecGrd->getColText(COLUMN_2))) != NULL)
					}	// if ((pRecGrd = (CSetupGradesReportRec2*)pRowsGrd->GetAt(i1)->GetRecord()) != NULL)
				}	// for (int i2 = 0;i2 < pRowsGrd->GetCount();i2)
			}	// if ((pRecSpc = (CSetupSpeciesReportRec*)pRows->GetAt(i)->GetRecord()) != NULL)
		}	// for (int i1 = 0;i1 < pRowsSpc->GetCount();i1++)
	}	// if (((pRowsSpc = m_repSpecies.GetRows()) != NULL) && (pRowsGrd = m_repGrades.GetRows()) != NULL)
	//AfxMessageBox(m_sData);
	OnOK();
}