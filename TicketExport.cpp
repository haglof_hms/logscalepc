#include "StdAfx.h"
#include "TicketExport.h"
#include "ResLangFileReader.h"
#include "ExcelExportDlg.h"
#include "DBHandling.h"
#include "libxl.h"

using namespace libxl;

CTicketExport::CTicketExport(void)
{
}

CTicketExport::~CTicketExport(void)
{
}

void CTicketExport::ExportToExcel(CDBHandling *pDB, CTickets *ticketExport)
{
	CString sValue = L"",sFileName = L"",sPath = L"";
	RLFReader xml;
	CString sRegKey = L"",S;
	int nExtraVolFuncFmt[MAX_NUMBER_OF_EXTRA_VFUNC];
	CLogs recLog = CLogs();
	int f[4],nColCnt = 0;
	vecColumnsSet vec;

	CString csLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
	if (!fileExists(csLangFN)) return;


	tokenizeString(xml.str(IDS_STRING205),';',m_sarrMeasureTypes);
	tokenizeString(xml.str(IDS_STRING203),';',m_sarrStatus);


	for (int i = 0;i < MAX_NUMBER_OF_EXTRA_VFUNC;i++)
	{
		nExtraVolFuncFmt[i] = 2;
	}

	if (xml.Load(csLangFN))
	{
		//#3611, lagt in koll inga ogiltiga tecken i Ticket numret	\ / ? : * " > < |
		if(checkIllegalChars(ticketExport->getTicket()))
		{
			::MessageBox(AfxGetMainWnd()->GetSafeHwnd(), _T("\\ / ? : * \" > < |")+xml.str(IDS_STRING18441), xml.str(IDS_STRING99), MB_ICONEXCLAMATION | MB_OK);
		}
		else
		{				
			CExcelExportDlg *pDlg = new CExcelExportDlg();
			if (pDlg != NULL)
			{
				sRegKey.Format(L"%s_%d", ticketExport->getTicket(), ticketExport->getPKID());
				pDlg->getColumnsSetting(sRegKey,COLSET_TICKET);	
				if (pDlg->DoModal() == IDOK)
				{
					pDlg->getColumnsSelected(vec);

					if (pDB != NULL)
					{
						sFileName = ticketExport->getTicket();	//pRec->getIconColText(COLUMN_0);
						sPath = regGetStr(REG_ROOT,TICKET_EXPORT_TO_EXCEL_DIR,DIRECTORY_KEY);
						sPath += sFileName;

						CFileDialog dlg(FALSE,L".xls",sPath,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,L"EXCEL *.xls|*.xls|");
						if (dlg.DoModal() != IDOK) return;
						regSetStr(REG_ROOT,TICKET_EXPORT_TO_EXCEL_DIR,DIRECTORY_KEY,getFilePath(dlg.GetPathName()));


						// Get logs for selected ticket
						pDB->getLogs(*ticketExport, m_vecLogs);
						pDB->getSelVolFuncs(m_vecSelectedVolFuncs, ticketExport->getPKID());

						if (m_vecSelectedVolFuncs.size() > 0)
						{
							for (UINT i = 0;i < m_vecSelectedVolFuncs.size();i++)
							{
								if (m_vecSelectedVolFuncs[i].getFuncID() == ID_SCRIB_W)
								{
									nExtraVolFuncFmt[i] = 3;
								}
							}
						}

						Book* book = xlCreateBook();
						if(book)
						{
							// Set registraton-key
							book->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0hfeflb");
							f[0] = book->addCustomNumFormat(L"0.0");
							f[1] = book->addCustomNumFormat(L"0.00");
							f[2] = book->addCustomNumFormat(L"0.000");
							f[3] = book->addCustomNumFormat(L"0");
							Format* fmt[5];
							fmt[0] = book->addFormat();
							fmt[0]->setNumFormat(f[0]);
							fmt[1] = book->addFormat();
							fmt[1]->setNumFormat(f[1]);
							fmt[2] = book->addFormat();
							fmt[2]->setNumFormat(f[2]);
							fmt[3] = book->addFormat();
							fmt[3]->setWrap();
							fmt[4] = book->addFormat();
							fmt[4]->setNumFormat(f[3]);
							Sheet* sheet = book->addSheet(L"Sheet1");
							if(sheet)
							{
								sheet->writeStr(1, 0,xml.str(IDS_STRING1000));
								sheet->writeStr(2, 0,xml.str(IDS_STRING1004));
								sheet->writeStr(3, 0,xml.str(IDS_STRING1003));
								sheet->writeStr(4, 0,xml.str(IDS_STRING1002));
								sheet->writeStr(1, 1, ticketExport->getTicket());
								sheet->writeStr(2, 1, ticketExport->getHauler());
								sheet->writeStr(3, 1, ticketExport->getScaler());
								sheet->writeStr(4, 1, ticketExport->getDate());

								sheet->writeStr(1, 3,xml.str(IDS_STRING2016));
								if (ticketExport->getMeasuringMode() == 0)
								{
									sheet->writeStr(1, 4,(m_sarrMeasureTypes.GetCount() == 2) ? m_sarrMeasureTypes[0] : L"");
									sheet->writeStr(2, 3,xml.str(IDS_STRING2022));
									sheet->writeNum(2, 4,ticketExport->getTrimFT());
								}
								else if (ticketExport->getMeasuringMode() == 1)
								{
									sheet->writeStr(1, 4,(m_sarrMeasureTypes.GetCount() == 2) ? m_sarrMeasureTypes[1] : L"");
									sheet->writeStr(2, 3,xml.str(IDS_STRING2023));
									sheet->writeNum(2, 4,ticketExport->getTrimCM());
								}

								for (int i = 0;i < vec.size();i++)
								{
									sheet->writeStr(6, i,vec[i].getColName(),fmt[3]);
								}

								for (int col = 0;col < vec.size();col++)
								{
									nColCnt = col;

									for (UINT row = 0;row < m_vecLogs.size();row++)
									{
										recLog = m_vecLogs[row];
										// Columns for row
										if (vec[nColCnt].getColIndex() == 0) sheet->writeStr(row+7, nColCnt,recLog.getTagNumber());
										if (vec[nColCnt].getColIndex() == 1) sheet->writeStr(row+7, nColCnt,recLog.getSpcCode());
										if (vec[nColCnt].getColIndex() == 2) sheet->writeStr(row+7, nColCnt,recLog.getSpcName());
										if (recLog.getStatusFlag() >= 0 && recLog.getStatusFlag() < m_sarrStatus.GetCount())
											if (vec[nColCnt].getColIndex() == 3) sheet->writeStr(row+7, nColCnt,m_sarrStatus.GetAt(recLog.getStatusFlag()));
											else
												if (vec[nColCnt].getColIndex() == 3) sheet->writeStr(row+7, nColCnt,L"N/A");

										sValue.Format(L"%d",recLog.getFrequency());
										if (vec[nColCnt].getColIndex() == 4) sheet->writeStr(row+7, nColCnt,sValue);
										if (vec[nColCnt].getColIndex() == 5) sheet->writeStr(row+7, nColCnt,recLog.getGrade());						
										if (vec[nColCnt].getColIndex() == 6) sheet->writeNum(row+7, nColCnt,recLog.getDeduction(),fmt[0]);
										if (vec[nColCnt].getColIndex() == 7) sheet->writeNum(row+7, nColCnt,recLog.getDOBTop(),fmt[2]);
										if (vec[nColCnt].getColIndex() == 8) sheet->writeNum(row+7, nColCnt,recLog.getDOBTop2(),fmt[2]);
										if (vec[nColCnt].getColIndex() == 9) sheet->writeNum(row+7, nColCnt,recLog.getDIBTop(),fmt[2]);
										if (vec[nColCnt].getColIndex() == 10) sheet->writeNum(row+7, nColCnt,recLog.getDIBTop2(),fmt[2]);
										if (vec[nColCnt].getColIndex() == 11) sheet->writeNum(row+7, nColCnt,recLog.getDOBRoot(),fmt[2]);
										if (vec[nColCnt].getColIndex() == 12) sheet->writeNum(row+7, nColCnt,recLog.getDOBRoot2(),fmt[2]);
										if (vec[nColCnt].getColIndex() == 13) sheet->writeNum(row+7, nColCnt,recLog.getDIBRoot(),fmt[2]);
										if (vec[nColCnt].getColIndex() == 14) sheet->writeNum(row+7, nColCnt,recLog.getDIBRoot2(),fmt[2]);
										if (vec[nColCnt].getColIndex() == 15) sheet->writeNum(row+7, nColCnt,recLog.getLengthMeas(),fmt[2]);
										if (vec[nColCnt].getColIndex() == 16) sheet->writeNum(row+7, nColCnt,recLog.getLengthCalc(),fmt[2]);
										if (vec[nColCnt].getColIndex() == 17) sheet->writeNum(row+7, nColCnt,recLog.getBark(),fmt[2]);
										if (vec[nColCnt].getColIndex() == 18) sheet->writeNum(row+7, nColCnt,recLog.getVolPricelist(),fmt[2]);
										if (vec[nColCnt].getColIndex() == 19) sheet->writeNum(row+7, nColCnt,recLog.getLogPrice(),fmt[2]);
										if (vec[nColCnt].getColIndex() == 20) sheet->writeStr(row+7, nColCnt,recLog.getVolFuncName());
										if (vec[nColCnt].getColIndex() == 21) sheet->writeNum(row+7, nColCnt,recLog.getVolCust1(),fmt[nExtraVolFuncFmt[0]]);
										if (vec[nColCnt].getColIndex() == 22) sheet->writeNum(row+7, nColCnt,recLog.getVolCust2(),fmt[nExtraVolFuncFmt[1]]);
										if (vec[nColCnt].getColIndex() == 23) sheet->writeNum(row+7, nColCnt,recLog.getVolCust3(),fmt[nExtraVolFuncFmt[2]]);
										if (vec[nColCnt].getColIndex() == 24) sheet->writeNum(row+7, nColCnt,recLog.getVolCust4(),fmt[nExtraVolFuncFmt[3]]);
										if (vec[nColCnt].getColIndex() == 25) sheet->writeNum(row+7, nColCnt,recLog.getVolCust5(),fmt[nExtraVolFuncFmt[4]]);
										if (vec[nColCnt].getColIndex() == 26) sheet->writeNum(row+7, nColCnt,recLog.getVolCust6(),fmt[nExtraVolFuncFmt[5]]);
										if (vec[nColCnt].getColIndex() == 27) sheet->writeNum(row+7, nColCnt,recLog.getVolCust7(),fmt[nExtraVolFuncFmt[6]]);
										if (vec[nColCnt].getColIndex() == 28) sheet->writeNum(row+7, nColCnt,recLog.getVolCust8(),fmt[nExtraVolFuncFmt[7]]);
										if (vec[nColCnt].getColIndex() == 29) sheet->writeNum(row+7, nColCnt,recLog.getVolCust9(),fmt[nExtraVolFuncFmt[8]]);
										if (vec[nColCnt].getColIndex() == 30) sheet->writeNum(row+7, nColCnt,recLog.getVolCust10(),fmt[nExtraVolFuncFmt[9]]);
										if (vec[nColCnt].getColIndex() == 31) sheet->writeNum(row+7, nColCnt,recLog.getVolCust11(),fmt[nExtraVolFuncFmt[10]]);
										if (vec[nColCnt].getColIndex() == 32) sheet->writeNum(row+7, nColCnt,recLog.getVolCust12(),fmt[nExtraVolFuncFmt[11]]);
										if (vec[nColCnt].getColIndex() == 33) sheet->writeNum(row+7, nColCnt,recLog.getVolCust13(),fmt[nExtraVolFuncFmt[12]]);
										if (vec[nColCnt].getColIndex() == 34) sheet->writeNum(row+7, nColCnt,recLog.getVolCust14(),fmt[nExtraVolFuncFmt[13]]);
										if (vec[nColCnt].getColIndex() == 35) sheet->writeNum(row+7, nColCnt,recLog.getVolCust15(),fmt[nExtraVolFuncFmt[14]]);
										if (vec[nColCnt].getColIndex() == 36) sheet->writeNum(row+7, nColCnt,recLog.getVolCust16(),fmt[nExtraVolFuncFmt[15]]);
										if (vec[nColCnt].getColIndex() == 37) sheet->writeNum(row+7, nColCnt,recLog.getVolCust17(),fmt[nExtraVolFuncFmt[16]]);
										if (vec[nColCnt].getColIndex() == 38) sheet->writeNum(row+7, nColCnt,recLog.getVolCust18(),fmt[nExtraVolFuncFmt[17]]);
										if (vec[nColCnt].getColIndex() == 39) sheet->writeNum(row+7, nColCnt,recLog.getVolCust19(),fmt[nExtraVolFuncFmt[18]]);
										if (vec[nColCnt].getColIndex() == 40) sheet->writeNum(row+7, nColCnt,recLog.getVolCust20(),fmt[nExtraVolFuncFmt[19]]);
										if (vec[nColCnt].getColIndex() == 41) sheet->writeNum(row+7, nColCnt,recLog.getURootDia(),fmt[2]);
										if (vec[nColCnt].getColIndex() == 42) sheet->writeNum(row+7, nColCnt,recLog.getURootDia2(),fmt[2]);
										if (vec[nColCnt].getColIndex() == 43) sheet->writeNum(row+7, nColCnt,recLog.getUVol(),fmt[2]);
										if (vec[nColCnt].getColIndex() == 44) sheet->writeNum(row+7, nColCnt,recLog.getUTopDia(),fmt[2]);
										if (vec[nColCnt].getColIndex() == 45) sheet->writeNum(row+7, nColCnt,recLog.getUTopDia2(),fmt[2]);
										if (vec[nColCnt].getColIndex() == 46) sheet->writeNum(row+7, nColCnt,recLog.getULength(),fmt[2]);
										if (vec[nColCnt].getColIndex() == 47) sheet->writeNum(row+7, nColCnt,recLog.getUPrice(),fmt[2]);
										if (vec[nColCnt].getColIndex() == 48) sheet->writeStr(row+7, nColCnt,recLog.getReason());
										if (vec[nColCnt].getColIndex() == 49) sheet->writeStr(row+7, nColCnt,recLog.getNotes());

									}
								}
							}

							if(book->save(dlg.GetPathName())) 
							{
#ifdef _OPEN_EXCEL_IN_TICKET
								CString csMsgOpenEXCEL = xml.str(IDS_STRING1124);
								CString csMsgCap = xml.str(IDS_STRING99);

								if (::MessageBox(AfxGetMainWnd()->GetSafeHwnd(), csMsgOpenEXCEL, csMsgCap, MB_ICONQUESTION | MB_YESNO) == IDYES)
									::ShellExecute(NULL, L"open", dlg.GetPathName(), NULL, NULL, SW_SHOW);        
#endif
							}
							else
							{
								CString S = L"";
								S.Format(L"%S",book->errorMessage());
								AfxMessageBox(S);
							}

							book->release();		
						} 
					} 
				}
			}
			delete pDlg;
		}
	}
	xml.clean();
}
