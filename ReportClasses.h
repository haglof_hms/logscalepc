#if !defined(__REPOPRTCLASSES_H__)
#define __REPOPRTCLASSES_H__
#pragma once

#include "TransactionClasses.h"

//////////////////////////////////////////////////////////////////////////////////////////
//	Number of decimals in CFloatItem
const TCHAR sz0dec[] = _T("%.0f");
const TCHAR sz1dec[] = _T("%.1f");
const TCHAR sz2dec[] = _T("%.2f");
const TCHAR sz3dec[] = _T("%.3f");


//////////////////////////////////////////////////////////////////////////////////////////
class CFloatItem : public CXTPReportRecordItemNumber
{
//private:
	double m_fValue;
public:
	CFloatItem(double fValue,LPCTSTR fmt_str = sz1dec) : 
			CXTPReportRecordItemNumber(fValue)
	{
		SetFormatString(fmt_str);
		m_fValue = fValue;
	}

	void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
	{
			m_fValue = (double)_tstof(szText);
			SetValue(m_fValue);
	}

	void setFloatItem(double value)	
	{ 
		m_fValue = value; 
		SetValue(value);
	}
	double getFloatItem(void)	{ return m_fValue; }
};

class CIntItem : public CXTPReportRecordItemNumber
{
//private:
	int m_nValue;
public:
	CIntItem(int nValue) : CXTPReportRecordItemNumber(nValue)	
	{
		m_nValue = nValue;
	}

	void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
	{
			m_nValue = _tstoi(szText);
			SetValue(m_nValue);
	}

	void setIntItem(int value)	
	{ 
		m_nValue = value; 
		SetValue(value);
	}
	int getIntItem(void)	{	return m_nValue; 	}
};

class CLongItem : public CXTPReportRecordItemNumber
{
//private:
	long m_nValue;
public:
	CLongItem(long nValue) : CXTPReportRecordItemNumber(nValue)	// Use one decimal in Percent value; 051219 p�d
	{
		m_nValue = nValue;
	}

	void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
	{
			m_nValue = _tstol(szText);
			SetValue(m_nValue);
	}

	long getLongItem(void)	{	return m_nValue; 	}
};


class CTextItem : public CXTPReportRecordItemText
{
//private:
	CString m_sText;
public:
	CTextItem(CString sValue) : CXTPReportRecordItemText(sValue)
	{
		m_sText = sValue;
	}
	void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
	{
			m_sText = szText;
			SetValue(m_sText);
	}

	CString getTextItem(void)	{ return m_sText; }
	void setTextItem(LPCTSTR text)	
	{ 
		m_sText = text; 
		SetValue(m_sText);
	}
};

//////////////////////////////////////////////////////////////////////////
// Customized record item, used for displaying checkboxes.
class CCheckItem : public CXTPReportRecordItem
{
public:
	// Constructs record item with the initial checkbox value.
	CCheckItem(BOOL bCheck)
	{
		HasCheckbox(TRUE);
		SetChecked(bCheck);
	}
	virtual BOOL getChecked(void)	{	return IsChecked()? TRUE: FALSE; }
	virtual void setChecked(BOOL bCheck) { SetChecked(bCheck); }
};

//////////////////////////////////////////////////////////////////////////
// Customized record item, used for displaying attachments icons.
class CExIconItem : public CXTPReportRecordItem
{
	CString m_sText;
	double m_fValue;
	int m_dec;
public:
	// Constructs record item with the initial value.
	CExIconItem(CString text,int icon_id);
	CExIconItem(int num_of,int icon_id);
	CExIconItem(double value,int dec,int icon_id);
	void OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText);

	void setTextColor(COLORREF rgb_value)		{ SetTextColor(rgb_value);	}
	void setTextBkColor(COLORREF rgb_value)	{ SetBackgroundColor(rgb_value);	}
	void setTextItem(CString text)
	{
		m_sText = text;
		SetCaption((m_sText));
	}

	CString getExIconText(void)		{ return m_sText; }
	void setExIconText(LPCTSTR v)	
	{ 
		m_sText = v; 
		SetCaption((m_sText));
	}

	int getExIconInt(void)		{ return (!m_sText.IsEmpty() ? _tstoi(m_sText):0); }
	void setExIconInt(int v)	
	{ 
		TCHAR tmp[50];
		_stprintf(tmp, _T("%d"),v);
		m_sText = tmp;
		SetCaption((m_sText));
	}

	double getExIconFloat(void)		{ return m_fValue; }
	void setExIconFloat(double v)	
	{ 
		TCHAR tmp[50];
		m_fValue = v; 
		_stprintf(tmp, _T("%.*f"), m_dec,m_fValue);
		SetCaption((tmp));
	}

};

//////////////////////////////////////////////////////////////////////////////////////////
// ReportRecord for Tickets
class CTicketsReportRec : public CXTPReportRecord
{
//private:
	CTickets record;
public:
	CTicketsReportRec(void)
	{
		record = CTickets();
		AddItem(new CExIconItem(L"",3));	
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CIntItem(0));
		AddItem(new CFloatItem(0.0,sz3dec));
		AddItem(new CFloatItem(0.0,sz3dec));
		AddItem(new CFloatItem(0.0,sz3dec));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
	}
	CTicketsReportRec(CTickets& rec)
	{
		record = rec;

		AddItem(new CExIconItem(rec.getTicket(),3));
		AddItem(new CTextItem(rec.getLoadID()));
		AddItem(new CTextItem(rec.getDate()));
		AddItem(new CIntItem(rec.getNumOfLogs()));
		AddItem(new CFloatItem(rec.getSumVolPricelist(),sz3dec));
		AddItem(new CFloatItem(rec.getSumPrice(),sz2dec));
		AddItem(new CFloatItem(rec.getWeight(),sz1dec));
		AddItem(new CTextItem(rec.getHauler()));
		AddItem(new CTextItem(rec.getSupplier()));
		AddItem(new CTextItem(rec.getVendor()));
		AddItem(new CTextItem(rec.getScaler()));
		AddItem(new CTextItem(rec.getSourceID()));
		AddItem(new CTextItem(rec.getLocation()));
		AddItem(new CTextItem(rec.getLoadType()));
		AddItem(new CTextItem(rec.getDeductionType()));
		AddItem(new CTextItem(rec.getBuyer()));
		AddItem(new CTextItem(rec.getTractID()));
		AddItem(new CFloatItem(rec.getIBTaper(),sz3dec));
		AddItem(new CTextItem(rec.getOtherInfo()));
		AddItem(new CTextItem(rec.getGISCoord1()));
		AddItem(new CTextItem(rec.getGISCoord2()));
		AddItem(new CTextItem(rec.getPrlUsed()));

		bkc(this->GetItem(COLUMNS::TIC_TICKET_NUM),INFOBK,FALSE);
		bkc(this->GetItem(COLUMNS::TIC_LOAD_ID),LTCYAN,FALSE);
		bkc(this->GetItem(COLUMNS::TIC_SOURCE_DATE),LTCYAN,FALSE);
		bkc(this->GetItem(COLUMNS::TIC_NUMOF_LOGS),LTCYAN,FALSE);
		bkc(this->GetItem(COLUMNS::TIC_SUM_VOL_PRICELIST),LTCYAN,FALSE);
		bkc(this->GetItem(COLUMNS::TIC_SUM_PRICE),LTCYAN,FALSE);

		bkc(this->GetItem(COLUMNS::TIC_WEIGHT),LTCYAN,FALSE);
		bkc(this->GetItem(COLUMNS::TIC_HAULER),LTCYAN,FALSE);
		bkc(this->GetItem(COLUMNS::TIC_SUPPLIER),LTCYAN,FALSE);
		bkc(this->GetItem(COLUMNS::TIC_VENDOR),LTCYAN,FALSE);
		bkc(this->GetItem(COLUMNS::TIC_SCALER),LTCYAN,FALSE);
		bkc(this->GetItem(COLUMNS::TIC_SOURCE_ID),LTCYAN,FALSE);
		bkc(this->GetItem(COLUMNS::TIC_LOCATION),LTCYAN,FALSE);
		bkc(this->GetItem(COLUMNS::TIC_LOAD_TYPE),LTCYAN,FALSE);
		bkc(this->GetItem(COLUMNS::TIC_DEDUCTION),LTCYAN,FALSE);
		bkc(this->GetItem(COLUMNS::TIC_BUYER),LTCYAN,FALSE);
		bkc(this->GetItem(COLUMNS::TIC_TRACT_ID),LTCYAN,FALSE);
		bkc(this->GetItem(COLUMNS::TIC_TAPER_IB),LTCYAN,FALSE);
		bkc(this->GetItem(COLUMNS::TIC_OTHER_INFO),LTCYAN,FALSE);
		bkc(this->GetItem(COLUMNS::TIC_GPS_COORD1),LTCYAN,FALSE);
		bkc(this->GetItem(COLUMNS::TIC_GPS_COORD2),LTCYAN,FALSE);
		bkc(this->GetItem(COLUMNS::TIC_PRICELIST),LTCYAN,FALSE);
}

	inline CTickets& getRecord() { return record; }

	inline int getColInt(int item) const	{ return ((CIntItem*)GetItem(item))->getIntItem();	}
	inline CString getColText(int item) const { return ((CTextItem*)GetItem(item))->getTextItem();	}
	inline double getColFloat(int item) const	{ return ((CFloatItem*)GetItem(item))->getFloatItem(); }

	inline void setColText(int item,LPCTSTR text) { ((CTextItem*)GetItem(item))->setTextItem(text); }
	inline void setColFloat(int item,double value)	{ ((CFloatItem*)GetItem(item))->setFloatItem(value);	}

	inline CString getIconColText(int item) const { return ((CExIconItem*)GetItem(item))->getExIconText();	}
	inline void setIconColText(int item,LPCTSTR text) { ((CExIconItem*)GetItem(item))->setExIconText(text); }
	inline int getIconColInt(int item) const { return ((CExIconItem*)GetItem(item))->getExIconInt();	}
	inline void setIconColInt(int item,int v) { ((CExIconItem*)GetItem(item))->setExIconInt(v); }

};


//////////////////////////////////////////////////////////////////////////////////////////
// ReportRecord for Logs
class CLogsReportRec : public CXTPReportRecord
{
//private:
	CLogs record;
	bool m_bIsFooter;

public:
	CLogsReportRec(int load_id = -1)
	{
		record = CLogs();
		// Add loadid (Primary key for ticket)
		record.setLoadID(load_id);
		m_bIsFooter = false;
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CIntItem(0));
		AddItem(new CTextItem(L""));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));		
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));

		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
	
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
	}
	CLogsReportRec(CLogs& rec,CStringArray &log_status,int measuring_mode,vecSelectedVolFuncs &vec_sel,CStringArray &headlines = CStringArray(),bool is_footer = false,short row = 0)
	{
		record = rec;
		m_bIsFooter = is_footer;
		CStringArray sarrDecimals;
		if (!is_footer)
		{
			AddItem(new CTextItem(rec.getTagNumber().Trim()));
			AddItem(new CTextItem(rec.getSpcCode()));
			AddItem(new CTextItem(rec.getSpcName()));
			if (rec.getStatusFlag() >= LOGSTATUS::IN_STOCK && rec.getStatusFlag() < log_status.GetCount())
				AddItem(new CTextItem(log_status.GetAt(rec.getStatusFlag())));
			else
				AddItem(new CTextItem(L""));

			AddItem(new CIntItem(rec.getFrequency()));
			AddItem(new CTextItem(rec.getGrade()));
			AddItem(new CFloatItem(rec.getDeduction(),sz1dec));

			AddItem(new CFloatItem(rec.getDOBTop(),sz1dec));
			AddItem(new CFloatItem(rec.getDOBTop2(),sz1dec));
			AddItem(new CFloatItem(rec.getDIBTop(),sz1dec));
			AddItem(new CFloatItem(rec.getDIBTop2(),sz1dec));
			AddItem(new CFloatItem(rec.getDOBRoot(),sz1dec));
			AddItem(new CFloatItem(rec.getDOBRoot2(),sz1dec));
			AddItem(new CFloatItem(rec.getDIBRoot(),sz1dec));
			AddItem(new CFloatItem(rec.getDIBRoot2(),sz1dec));
			AddItem(new CFloatItem(rec.getLengthMeas(),sz2dec));
			AddItem(new CFloatItem(rec.getLengthCalc(),sz2dec));

			AddItem(new CFloatItem(rec.getBark(),sz1dec));	// Present double barkreduction in inch/10

			if (rec.getVolFuncID() == ID_SCRIB_W)
			{
				AddItem(new CFloatItem(rec.getVolPricelist(),sz0dec));
			}
			else
			{
				AddItem(new CFloatItem(rec.getVolPricelist(),sz3dec));
			}
			AddItem(new CFloatItem(rec.getLogPrice(),sz2dec));
			AddItem(new CTextItem(rec.getVolFuncName()));

			for (int i = 0;i < MAX_NUMBER_OF_EXTRA_VFUNC;i++)
				sarrDecimals.Add(sz3dec);
			for (UINT i1 = 0;i1 < vec_sel.size();i1++)
			{
				if (vec_sel[i1].getFuncID() == ID_SCRIB_W)
					sarrDecimals[i1] = sz0dec;
			}

			AddItem(new CFloatItem(rec.getVolCust1(),sarrDecimals.GetAt(0)));
			AddItem(new CFloatItem(rec.getVolCust2(),sarrDecimals.GetAt(1)));
			AddItem(new CFloatItem(rec.getVolCust3(),sarrDecimals.GetAt(2)));
			AddItem(new CFloatItem(rec.getVolCust4(),sarrDecimals.GetAt(3)));
			AddItem(new CFloatItem(rec.getVolCust5(),sarrDecimals.GetAt(4)));
			AddItem(new CFloatItem(rec.getVolCust6(),sarrDecimals.GetAt(5)));
			AddItem(new CFloatItem(rec.getVolCust7(),sarrDecimals.GetAt(6)));
			AddItem(new CFloatItem(rec.getVolCust8(),sarrDecimals.GetAt(7)));
			AddItem(new CFloatItem(rec.getVolCust9(),sarrDecimals.GetAt(8)));
			AddItem(new CFloatItem(rec.getVolCust10(),sarrDecimals.GetAt(9)));
			AddItem(new CFloatItem(rec.getVolCust11(),sarrDecimals.GetAt(10)));
			AddItem(new CFloatItem(rec.getVolCust12(),sarrDecimals.GetAt(11)));
			AddItem(new CFloatItem(rec.getVolCust13(),sarrDecimals.GetAt(12)));
			AddItem(new CFloatItem(rec.getVolCust14(),sarrDecimals.GetAt(13)));
			AddItem(new CFloatItem(rec.getVolCust15(),sarrDecimals.GetAt(14)));
			AddItem(new CFloatItem(rec.getVolCust16(),sarrDecimals.GetAt(15)));
			AddItem(new CFloatItem(rec.getVolCust17(),sarrDecimals.GetAt(16)));
			AddItem(new CFloatItem(rec.getVolCust18(),sarrDecimals.GetAt(17)));
			AddItem(new CFloatItem(rec.getVolCust19(),sarrDecimals.GetAt(18)));
			AddItem(new CFloatItem(rec.getVolCust20(),sarrDecimals.GetAt(19)));

			AddItem(new CFloatItem(rec.getURootDia(),sz1dec));
			AddItem(new CFloatItem(rec.getURootDia2(),sz1dec));
			AddItem(new CFloatItem(rec.getUVol(),sz1dec));
			AddItem(new CFloatItem(rec.getUTopDia(),sz1dec));
			AddItem(new CFloatItem(rec.getUTopDia2(),sz1dec));
			AddItem(new CFloatItem(rec.getULength(),sz2dec));
			AddItem(new CFloatItem(rec.getUPrice(),sz2dec));
			AddItem(new CTextItem(rec.getReason()));
			AddItem(new CTextItem(rec.getNotes()));

			if (rec.getTicketOrigin() == LOGORIGIN::MANUALLY)
			{
				setReportRowFntData(this->GetItem(COLUMNS::LOG_TAG_NUMBER),BLACK,WHITE,FALSE,TRUE);
			}
			else if (rec.getTicketOrigin() == LOGORIGIN::MANUALLY_ON_FILE)
			{
				setReportRowFntData(this->GetItem(COLUMNS::LOG_TAG_NUMBER),BLUE,INFOBK,FALSE,FALSE);
			}
			else
			{
				setReportRowFntData(this->GetItem(COLUMNS::LOG_TAG_NUMBER),BLACK,INFOBK,FALSE,FALSE);
			}

			for (int i = COLUMNS::LOG_SPC_CODE;i <= COLUMNS::LOG_NOTES;i++)
			{
				
				if (rec.getStatusFlag() == LOGSTATUS::IN_STOCK)
				{
					if ((i >= COLUMNS::LOG_LENGTH_CALC && i <= COLUMNS::LOG_VOL_CUST_20) || i == COLUMNS::LOG_SPC_NAME || i == COLUMNS::LOG_DOB_TOP || i == COLUMNS::LOG_DOB_TOP2 ||
						i == COLUMNS::LOG_DOB_ROOT || i == COLUMNS::LOG_DOB_ROOT2)
					setReportRowFntData(this->GetItem(i),BLACK,LTCYAN);
				}
				else if (rec.getStatusFlag() == LOGSTATUS::SOLD)
				{
					if (i != COLUMNS::LOG_STATUS_FLAG)
						setReportRowFntData(this->GetItem(i),WHITE,RGB(255,48,48),FALSE,FALSE);
					if (i == COLUMNS::LOG_NOTES)
						setReportRowFntData(this->GetItem(i),BLACK,WHITE);
				}
				else if (rec.getStatusFlag() == LOGSTATUS::IN_MILL)
				{
					if (i != COLUMNS::LOG_STATUS_FLAG)
						setReportRowFntData(this->GetItem(i),BLACK,GREEN,FALSE,FALSE);
					if (i == COLUMNS::LOG_NOTES)
						setReportRowFntData(this->GetItem(i),BLACK,WHITE);
				}
				else if (rec.getStatusFlag() == LOGSTATUS::SCRAP)
				{
					if (i != COLUMNS::LOG_STATUS_FLAG)
						setReportRowFntData(this->GetItem(i),WHITE,RED,FALSE,FALSE);
					if (i == COLUMNS::LOG_NOTES)
						setReportRowFntData(this->GetItem(i),BLACK,WHITE);
				}
				else if (rec.getStatusFlag() == LOGSTATUS::LOCKED)
				{
					if (i != COLUMNS::LOG_STATUS_FLAG)
						setReportRowFntData(this->GetItem(i),BLACK,LTCYAN,FALSE,FALSE);
					if (i == COLUMNS::LOG_NOTES)
						setReportRowFntData(this->GetItem(i),BLACK,WHITE);
				}

			}

		}
		else if (row == 1)
		{

			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_DOB_TOP)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_DOB_TOP2)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_DIB_TOP)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_DIB_TOP2)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_DOB_ROOT)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_DOB_ROOT2)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_DIB_ROOT)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_DIB_ROOT2)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_LENGTH_MEAS)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_LENGTH_CALC)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_BARK)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_VOL_PRICELIST)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_PRICE)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_VOL_FUNC_NAME)));

			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_VOL_CUST_1)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_VOL_CUST_2)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_VOL_CUST_3)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_VOL_CUST_4)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_VOL_CUST_5)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_VOL_CUST_6)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_VOL_CUST_7)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_VOL_CUST_8)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_VOL_CUST_9)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_VOL_CUST_10)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_VOL_CUST_11)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_VOL_CUST_12)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_VOL_CUST_13)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_VOL_CUST_14)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_VOL_CUST_15)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_VOL_CUST_16)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_VOL_CUST_17)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_VOL_CUST_18)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_VOL_CUST_19)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_VOL_CUST_20)));


			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_U_LARGE_DIAM)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_U_LARGE_DIAM2)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_U_VOL)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_U_SMALL_DIAM)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_U_SMALL_DIAM2)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_U_LENGTH)));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_U_PRICE)));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			
			for (int i = COLUMNS::LOG_DEDUCTION;i < COLUMNS::LOG_REASON;i++)
				setReportRowFntData(this->GetItem(i),BLUE,LTCYAN,FALSE,FALSE);

		}
		else if (row == 2)
		{

			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(headlines.GetAt(COLUMNS::LOG_DEDUCTION)));
			AddItem(new CFloatItem(rec.getDOBTop(),sz1dec));
			AddItem(new CFloatItem(rec.getDOBTop2(),sz1dec));
			AddItem(new CFloatItem(rec.getDIBTop(),sz1dec));
			AddItem(new CFloatItem(rec.getDIBTop2(),sz1dec));
			AddItem(new CFloatItem(rec.getDOBRoot(),sz1dec));
			AddItem(new CFloatItem(rec.getDOBRoot2(),sz1dec));
			AddItem(new CFloatItem(rec.getDIBRoot(),sz1dec));
			AddItem(new CFloatItem(rec.getDIBRoot2(),sz1dec));
			AddItem(new CFloatItem(rec.getLengthMeas(),sz2dec));
			AddItem(new CFloatItem(rec.getLengthCalc(),sz2dec));
			if (measuring_mode == 0)	// Inch
			{
				AddItem(new CFloatItem((rec.getBark()*DOUBLE_BARK_FACTOR),sz1dec));	// Present double barkreduction in inch/10
			}
			else if (measuring_mode == 1)	// Metric
			{
				AddItem(new CFloatItem((rec.getBark()*DOUBLE_BARK_FACTOR),sz1dec));	// Present double barkreduction in mm
			}
//			AddItem(new CFloatItem(rec.getBark(),sz1dec));
			if (rec.getVolFuncID() == ID_SCRIB_W)
			{
				AddItem(new CFloatItem(rec.getVolPricelist(),sz0dec));
			}
			else
			{
				AddItem(new CFloatItem(rec.getVolPricelist(),sz3dec));
			}

			AddItem(new CFloatItem(rec.getLogPrice(),sz2dec));
			AddItem(new CTextItem(L""));

			for (int i = 0;i < MAX_NUMBER_OF_EXTRA_VFUNC;i++)
				sarrDecimals.Add(sz3dec);
			for (UINT i1 = 0;i1 < vec_sel.size();i1++)
			{
				if (vec_sel[i1].getFuncID() == ID_SCRIB_W)
					sarrDecimals[i1] = sz0dec;
			}

			AddItem(new CFloatItem(rec.getVolCust1(),sarrDecimals.GetAt(0)));
			AddItem(new CFloatItem(rec.getVolCust2(),sarrDecimals.GetAt(1)));
			AddItem(new CFloatItem(rec.getVolCust3(),sarrDecimals.GetAt(2)));
			AddItem(new CFloatItem(rec.getVolCust4(),sarrDecimals.GetAt(3)));
			AddItem(new CFloatItem(rec.getVolCust5(),sarrDecimals.GetAt(4)));
			AddItem(new CFloatItem(rec.getVolCust6(),sarrDecimals.GetAt(5)));
			AddItem(new CFloatItem(rec.getVolCust7(),sarrDecimals.GetAt(6)));
			AddItem(new CFloatItem(rec.getVolCust8(),sarrDecimals.GetAt(7)));
			AddItem(new CFloatItem(rec.getVolCust9(),sarrDecimals.GetAt(8)));
			AddItem(new CFloatItem(rec.getVolCust10(),sarrDecimals.GetAt(9)));
			AddItem(new CFloatItem(rec.getVolCust11(),sarrDecimals.GetAt(10)));
			AddItem(new CFloatItem(rec.getVolCust12(),sarrDecimals.GetAt(11)));
			AddItem(new CFloatItem(rec.getVolCust13(),sarrDecimals.GetAt(12)));
			AddItem(new CFloatItem(rec.getVolCust14(),sarrDecimals.GetAt(13)));
			AddItem(new CFloatItem(rec.getVolCust15(),sarrDecimals.GetAt(14)));
			AddItem(new CFloatItem(rec.getVolCust16(),sarrDecimals.GetAt(15)));
			AddItem(new CFloatItem(rec.getVolCust17(),sarrDecimals.GetAt(16)));
			AddItem(new CFloatItem(rec.getVolCust18(),sarrDecimals.GetAt(17)));
			AddItem(new CFloatItem(rec.getVolCust19(),sarrDecimals.GetAt(18)));
			AddItem(new CFloatItem(rec.getVolCust20(),sarrDecimals.GetAt(19)));
/*
			AddItem(new CFloatItem(rec.getVolCust1(),sz3dec));
			AddItem(new CFloatItem(rec.getVolCust2(),sz3dec));
			AddItem(new CFloatItem(rec.getVolCust3(),sz3dec));
			AddItem(new CFloatItem(rec.getVolCust4(),sz3dec));
			AddItem(new CFloatItem(rec.getVolCust5(),sz3dec));
			AddItem(new CFloatItem(rec.getVolCust6(),sz3dec));
			AddItem(new CFloatItem(rec.getVolCust7(),sz3dec));
			AddItem(new CFloatItem(rec.getVolCust8(),sz3dec));
			AddItem(new CFloatItem(rec.getVolCust9(),sz3dec));
			AddItem(new CFloatItem(rec.getVolCust10(),sz3dec));
			AddItem(new CFloatItem(rec.getVolCust11(),sz3dec));
			AddItem(new CFloatItem(rec.getVolCust12(),sz3dec));
			AddItem(new CFloatItem(rec.getVolCust13(),sz3dec));
			AddItem(new CFloatItem(rec.getVolCust14(),sz3dec));
			AddItem(new CFloatItem(rec.getVolCust15(),sz3dec));
			AddItem(new CFloatItem(rec.getVolCust16(),sz3dec));
			AddItem(new CFloatItem(rec.getVolCust17(),sz3dec));
			AddItem(new CFloatItem(rec.getVolCust18(),sz3dec));
			AddItem(new CFloatItem(rec.getVolCust19(),sz3dec));
			AddItem(new CFloatItem(rec.getVolCust20(),sz3dec));
*/
			AddItem(new CFloatItem(rec.getURootDia(),sz1dec));
			AddItem(new CFloatItem(rec.getURootDia2(),sz1dec));
			AddItem(new CFloatItem(rec.getUVol(),sz1dec));
			AddItem(new CFloatItem(rec.getUTopDia(),sz1dec));
			AddItem(new CFloatItem(rec.getUTopDia2(),sz1dec));
			AddItem(new CFloatItem(rec.getULength(),sz2dec));
			AddItem(new CFloatItem(rec.getUPrice(),sz2dec));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));

			for (int i = COLUMNS::LOG_DEDUCTION;i < COLUMNS::LOG_REASON;i++)
			setReportRowFntData(this->GetItem(i),BLUE,LTCYAN,TRUE,FALSE);
		}
	}

	inline bool getIsFooter()	{ return m_bIsFooter; }

	inline CLogs& getRecord() { return record; }

	inline int getColInt(int item) const	{ return ((CIntItem*)GetItem(item))->getIntItem();	}
	inline CString getColText(int item) const { return ((CTextItem*)GetItem(item))->getTextItem();	}
	inline double getColFloat(int item) const	{ return ((CFloatItem*)GetItem(item))->getFloatItem(); }

	inline void setColInt(int item,int value) { ((CIntItem*)GetItem(item))->setIntItem(value); }
	inline void setColText(int item,LPCTSTR text) { ((CTextItem*)GetItem(item))->setTextItem(text); }
	inline void setColFloat(int item,double value)	{ ((CFloatItem*)GetItem(item))->setFloatItem(value);	}

	inline CString getIconColText(int item) const { return ((CExIconItem*)GetItem(item))->getExIconText();	}
	inline void setIconColText(int item,LPCTSTR text) { ((CExIconItem*)GetItem(item))->setExIconText(text); }

};

//////////////////////////////////////////////////////////////////////////////////////////
// ReportRecord for Logs
class CInventoryReportRec : public CXTPReportRecord
{
//private:
	int m_nPKID;
	int m_nType;
	CInventory rec;
public:
	CInventoryReportRec()
	{
		m_nPKID = -1;
		m_nType = -1;
		CInventory rec = CInventory();
		// Add loadid (Primary key for ticket)
		AddItem(new CExIconItem(L"",3));	
		AddItem(new CTextItem(L""));
		AddItem(new CIntItem(0));
		AddItem(new CFloatItem(0.0,sz2dec));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
	}
	CInventoryReportRec(CInventory& recInv,int pk_id,int type,LPCTSTR id,LPCTSTR name,int num_of_logs,double price,
											LPCTSTR buyer,LPCTSTR created_by,LPCTSTR date,LPCTSTR hauler,LPCTSTR scaler,LPCTSTR supplier,LPCTSTR source_id,
											LPCTSTR location,LPCTSTR tract_id,LPCTSTR vendor,LPCTSTR gps_c1,LPCTSTR gps_c2)
	{
		m_nPKID = pk_id;
		m_nType = type;
		rec = recInv;
		AddItem(new CExIconItem(id,3));	
		AddItem(new CTextItem(name));
		AddItem(new CIntItem(num_of_logs));
		AddItem(new CFloatItem(price,sz2dec));
		AddItem(new CTextItem(buyer));
		AddItem(new CTextItem(created_by));
		AddItem(new CTextItem(date));
		AddItem(new CTextItem(hauler));
		AddItem(new CTextItem(scaler));
		AddItem(new CTextItem(supplier));
		AddItem(new CTextItem(source_id));
		AddItem(new CTextItem(location));
		AddItem(new CTextItem(tract_id));
		AddItem(new CTextItem(vendor));
		AddItem(new CTextItem(gps_c1));
		AddItem(new CTextItem(gps_c2));
	}

	inline CInventory getRecord()	{ return rec; }

	inline int getPKID()	{ return m_nPKID; }
	inline int getType()	{ return m_nType; }

	inline CString getColText(int item) const { return ((CTextItem*)GetItem(item))->getTextItem();	}

	inline void setColText(int item,LPCTSTR text) { ((CTextItem*)GetItem(item))->setTextItem(text); }

	inline CString getIconColText(int item) const { return ((CExIconItem*)GetItem(item))->getExIconText();	}
	inline void setIconColText(int item,LPCTSTR text) { ((CExIconItem*)GetItem(item))->setExIconText(text); }

};


//////////////////////////////////////////////////////////////////////////////////////////
// ReportRecord for Logs
class CLogSalesReportRec : public CXTPReportRecord
{
//private:
	CLogs record;
	bool m_bIsFooter;
public:
	CLogSalesReportRec(int load_id = -1)
	{
		record = CLogs();
		// Add loadid (Primary key for ticket)
		record.setLoadID(load_id);
		m_bIsFooter = false;
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CIntItem(0));
		AddItem(new CTextItem(L""));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CFloatItem(0.0,sz0dec));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
	}
	CLogSalesReportRec(CLogs& rec,CStringArray &headlines = CStringArray(),bool is_footer = false,short row = 0)
	{
		record = rec;
		m_bIsFooter = is_footer;
		if (!is_footer)
		{
			AddItem(new CTextItem(rec.getTicket().Trim()));
			AddItem(new CTextItem(rec.getTagNumber().Trim()));
			AddItem(new CTextItem(rec.getSpcCode()));
			AddItem(new CTextItem(rec.getSpcName()));
			AddItem(new CIntItem(rec.getFrequency()));
			AddItem(new CTextItem(rec.getGrade()));
			AddItem(new CFloatItem(rec.getDeduction(),sz1dec));
			AddItem(new CFloatItem(rec.getDOBTop(),sz3dec));
			AddItem(new CFloatItem(rec.getDOBTop2(),sz3dec));
			AddItem(new CFloatItem(rec.getDIBTop(),sz3dec));
			AddItem(new CFloatItem(rec.getDIBTop2(),sz3dec));
			AddItem(new CFloatItem(rec.getDOBRoot(),sz3dec));
			AddItem(new CFloatItem(rec.getDOBRoot2(),sz3dec));
			AddItem(new CFloatItem(rec.getDIBRoot(),sz3dec));
			AddItem(new CFloatItem(rec.getDIBRoot2(),sz3dec));
			AddItem(new CFloatItem(rec.getLengthMeas(),sz3dec));
			AddItem(new CFloatItem(rec.getLengthCalc(),sz3dec));
			AddItem(new CFloatItem(rec.getBark(),sz3dec));
			AddItem(new CFloatItem(rec.getVolPricelist(),sz3dec));
			AddItem(new CFloatItem(rec.getLogPrice(),sz2dec));
			AddItem(new CTextItem(rec.getVolFuncName()));
			AddItem(new CFloatItem(rec.getUVol(),sz3dec));
			AddItem(new CFloatItem(rec.getUTopDia(),sz3dec));
			AddItem(new CFloatItem(rec.getUTopDia2(),sz3dec));
			AddItem(new CFloatItem(rec.getURootDia(),sz3dec));
			AddItem(new CFloatItem(rec.getURootDia2(),sz3dec));
			AddItem(new CFloatItem(rec.getULength(),sz3dec));
			AddItem(new CFloatItem(rec.getUPrice(),sz2dec));
			AddItem(new CTextItem(rec.getReason()));
			AddItem(new CTextItem(rec.getNotes()));
		}
		else if (row == 1)
		{

			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(headlines.GetAt(COLUMN_6)));
			AddItem(new CTextItem(headlines.GetAt(COLUMN_7)));
			AddItem(new CTextItem(headlines.GetAt(COLUMN_8)));
			AddItem(new CTextItem(headlines.GetAt(COLUMN_9)));
			AddItem(new CTextItem(headlines.GetAt(COLUMN_10)));
			AddItem(new CTextItem(headlines.GetAt(COLUMN_11)));
			AddItem(new CTextItem(headlines.GetAt(COLUMN_12)));
			AddItem(new CTextItem(headlines.GetAt(COLUMN_13)));
			AddItem(new CTextItem(headlines.GetAt(COLUMN_14)));
			AddItem(new CTextItem(headlines.GetAt(COLUMN_15)));
			AddItem(new CTextItem(headlines.GetAt(COLUMN_16)));
			AddItem(new CTextItem(headlines.GetAt(COLUMN_17)));
			AddItem(new CTextItem(headlines.GetAt(COLUMN_18)));
			
			setReportRowFntData(this->GetItem(COLUMN_6),BLUE,LTCYAN);
			setReportRowFntData(this->GetItem(COLUMN_7),BLUE,LTCYAN);
			setReportRowFntData(this->GetItem(COLUMN_8),BLUE,LTCYAN);
			setReportRowFntData(this->GetItem(COLUMN_9),BLUE,LTCYAN);
			setReportRowFntData(this->GetItem(COLUMN_10),BLUE,LTCYAN);
			setReportRowFntData(this->GetItem(COLUMN_11),BLUE,LTCYAN);
			setReportRowFntData(this->GetItem(COLUMN_12),BLUE,LTCYAN);
			setReportRowFntData(this->GetItem(COLUMN_13),BLUE,LTCYAN);
			setReportRowFntData(this->GetItem(COLUMN_14),BLUE,LTCYAN);
			setReportRowFntData(this->GetItem(COLUMN_15),BLUE,LTCYAN);
			setReportRowFntData(this->GetItem(COLUMN_16),BLUE,LTCYAN);
			setReportRowFntData(this->GetItem(COLUMN_17),BLUE,LTCYAN);
			setReportRowFntData(this->GetItem(COLUMN_18),BLUE,LTCYAN);
			setReportRowFntData(this->GetItem(COLUMN_19),BLUE,LTCYAN);
		}
		else if (row == 2)
		{

			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(headlines.GetAt(COLUMN_5)));
			AddItem(new CFloatItem(rec.getDOBTop(),sz3dec));
			AddItem(new CFloatItem(rec.getDOBTop2(),sz3dec));
			AddItem(new CFloatItem(rec.getDIBTop(),sz3dec));
			AddItem(new CFloatItem(rec.getDIBTop2(),sz3dec));
			AddItem(new CFloatItem(rec.getDOBRoot(),sz3dec));
			AddItem(new CFloatItem(rec.getDOBRoot2(),sz3dec));
			AddItem(new CFloatItem(rec.getDIBRoot(),sz3dec));
			AddItem(new CFloatItem(rec.getDIBRoot2(),sz3dec));
			AddItem(new CFloatItem(rec.getLengthMeas(),sz3dec));
			AddItem(new CFloatItem(rec.getLengthCalc(),sz3dec));
			AddItem(new CFloatItem(rec.getBark(),sz3dec));
			AddItem(new CFloatItem(rec.getVolPricelist(),sz3dec));
			AddItem(new CFloatItem(rec.getLogPrice(),sz2dec));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));
			AddItem(new CTextItem(L""));

			setReportRowFntData(this->GetItem(COLUMN_6),BLUE,LTCYAN,TRUE);
			setReportRowFntData(this->GetItem(COLUMN_7),BLUE,LTCYAN,TRUE);
			setReportRowFntData(this->GetItem(COLUMN_8),BLUE,LTCYAN,TRUE);
			setReportRowFntData(this->GetItem(COLUMN_9),BLUE,LTCYAN,TRUE);
			setReportRowFntData(this->GetItem(COLUMN_10),BLUE,LTCYAN,TRUE);
			setReportRowFntData(this->GetItem(COLUMN_11),BLUE,LTCYAN,TRUE);
			setReportRowFntData(this->GetItem(COLUMN_12),BLUE,LTCYAN,TRUE);
			setReportRowFntData(this->GetItem(COLUMN_13),BLUE,LTCYAN,TRUE);
			setReportRowFntData(this->GetItem(COLUMN_14),BLUE,LTCYAN,TRUE);
			setReportRowFntData(this->GetItem(COLUMN_15),BLUE,LTCYAN,TRUE);
			setReportRowFntData(this->GetItem(COLUMN_16),BLUE,LTCYAN,TRUE);
			setReportRowFntData(this->GetItem(COLUMN_17),BLUE,LTCYAN,TRUE);	
			setReportRowFntData(this->GetItem(COLUMN_18),BLUE,LTCYAN,TRUE);
			setReportRowFntData(this->GetItem(COLUMN_19),BLUE,LTCYAN,TRUE);
/*
			setReportRowFntData(this->GetItem(COLUMN_20),BLUE,LTCYAN,TRUE);
			setReportRowFntData(this->GetItem(COLUMN_21),BLUE,LTCYAN,TRUE);
			setReportRowFntData(this->GetItem(COLUMN_22),BLUE,LTCYAN,TRUE);
			setReportRowFntData(this->GetItem(COLUMN_23),BLUE,LTCYAN,TRUE);
			setReportRowFntData(this->GetItem(COLUMN_24),BLUE,LTCYAN,TRUE);
			setReportRowFntData(this->GetItem(COLUMN_25),BLUE,LTCYAN,TRUE);
			setReportRowFntData(this->GetItem(COLUMN_26),BLUE,LTCYAN,TRUE);
			setReportRowFntData(this->GetItem(COLUMN_27),BLUE,LTCYAN,TRUE);
			setReportRowFntData(this->GetItem(COLUMN_28),BLUE,LTCYAN,TRUE);
			setReportRowFntData(this->GetItem(COLUMN_29),BLUE,LTCYAN,TRUE);
*/

		}
	}

	inline bool getIsFooter()	{ return m_bIsFooter; }

	inline CLogs& getRecord() { return record; }

	inline int getColInt(int item) const	{ return ((CIntItem*)GetItem(item))->getIntItem();	}
	inline CString getColText(int item) const { return ((CTextItem*)GetItem(item))->getTextItem();	}
	inline double getColFloat(int item) const	{ return ((CFloatItem*)GetItem(item))->getFloatItem(); }

	inline void setColText(int item,LPCTSTR text) { ((CTextItem*)GetItem(item))->setTextItem(text); }
	inline void setColFloat(int item,double value)	{ ((CFloatItem*)GetItem(item))->setFloatItem(value);	}

	inline CString getIconColText(int item) const { return ((CExIconItem*)GetItem(item))->getExIconText();	}
	inline void setIconColText(int item,LPCTSTR text) { ((CExIconItem*)GetItem(item))->setExIconText(text); }

};


//////////////////////////////////////////////////////////////////////////////////////////
// ReportRecord for Species
class CSpeciesReportRec : public CXTPReportRecord
{
//private:
	CSpecies record;
	bool bIsValid;
public:
	CSpeciesReportRec(void)
	{
		record = CSpecies();
		bIsValid = true;
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CFloatItem(0.0,sz1dec));
		AddItem(new CFloatItem(0.0,sz1dec));
	}
	CSpeciesReportRec(CSpecies& rec,bool is_valid)
	{
		record = rec;
		bIsValid = is_valid;
		AddItem(new CTextItem(rec.getSpcCode()));
		AddItem(new CTextItem(rec.getSpcName() ));
		AddItem(new CFloatItem(rec.getBarkReductionInch(),sz1dec));
		AddItem(new CFloatItem(rec.getBarkReductionMM(),sz1dec));
	}
	
	inline bool getIsValid()	{ return bIsValid; }
	inline void setIsValid(bool v)	{ bIsValid = v; }
		
	inline CSpecies& getRecord() { return record; }

	inline CString getColText(int item) const { return ((CTextItem*)GetItem(item))->getTextItem();	}
	inline void setColText(int item,LPCTSTR text) { ((CTextItem*)GetItem(item))->setTextItem(text); }

	inline double getColFloat(int item) const { return ((CFloatItem*)GetItem(item))->getFloatItem();	}
	inline void setColFloat(int item,double v) { ((CFloatItem*)GetItem(item))->setFloatItem(v); }

};

//////////////////////////////////////////////////////////////////////////////////////////
// ReportRecord for Species
class CTicketSpcReportRec : public CXTPReportRecord
{
//private:
	CTicketSpc record;
public:
	CTicketSpcReportRec(void)
	{
		record = CTicketSpc();
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CFloatItem(0.0,sz1dec));
		this->GetItem(COLUMN_0)->SetBackgroundColor(INFOBK);
		this->GetItem(COLUMN_1)->SetBackgroundColor(INFOBK);
	}
	CTicketSpcReportRec(CTicketSpc& rec)
	{
		record = rec;
		AddItem(new CTextItem(rec.getSpeciesCode()));
		AddItem(new CTextItem(rec.getSpeciesName() ));
		AddItem(new CFloatItem(rec.getBarkReduction(),sz1dec));
		this->GetItem(COLUMN_0)->SetBackgroundColor(INFOBK);
		this->GetItem(COLUMN_1)->SetBackgroundColor(INFOBK);
}

	inline CTicketSpc& getRecord() { return record; }

	inline CString getColText(int item) const { return ((CTextItem*)GetItem(item))->getTextItem();	}
	inline void setColText(int item,LPCTSTR text) { ((CTextItem*)GetItem(item))->setTextItem(text); }

	inline double getColFloat(int item) const { return ((CFloatItem*)GetItem(item))->getFloatItem();	}
	inline void setColFloat(int item,double v) { ((CFloatItem*)GetItem(item))->setFloatItem(v); }

};

//////////////////////////////////////////////////////////////////////////////////////////
// ReportRecord for Grades
class CGradesReportRec : public CXTPReportRecord
{
//private:
	CGrades record;
	bool bIsValid;
public:
	CGradesReportRec(void)
	{
		record = CGrades();
		bIsValid = true;
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CCheckItem(FALSE));
	}
	CGradesReportRec(CGrades& rec,bool is_valid)
	{
		record = rec;
		bIsValid = is_valid;
		AddItem(new CTextItem(rec.getGradesCode()));
		AddItem(new CTextItem(rec.getGradesName() ));
		AddItem(new CCheckItem((rec.getIsPulpwood() == 1 ? TRUE:FALSE)));
	}

	inline bool getIsValid()	{ return bIsValid; }
	inline void setIsValid(bool v)	{ bIsValid = v; }

	inline CGrades& getRecord() { return record; }

	inline CString getColText(int item) const { return ((CTextItem*)GetItem(item))->getTextItem();	}
	inline void setColText(int item,LPCTSTR text) { ((CTextItem*)GetItem(item))->setTextItem(text); }

	inline BOOL getColChecked(int item) const { return ((CCheckItem*)GetItem(item))->getChecked();	}
	inline void setColChecked(int item,BOOL check) { ((CCheckItem*)GetItem(item))->setChecked(check); }
};


//////////////////////////////////////////////////////////////////////////////////////////
// ReportRecord for Pricelist
class CPricelistTmplReportRec : public CXTPReportRecord
{
	//private:
	int nFuncID;	//#4258 lagt till 
public:
	CPricelistTmplReportRec(void)
	{
		nFuncID = -1;
		AddItem(new CTextItem(L""));
		AddItem(new CFloatItem(0.0,sz2dec));
#ifdef _LOCK_PRICELIST_IN_TEMPLATES
		AddItem(new CExIconItem(L"",-1));
#else		
		AddItem(new CExIconItem(L"",3));
#endif

		this->GetItem(COLUMN_0)->SetBackgroundColor(INFOBK);
#ifdef _LOCK_PRICELIST_IN_TEMPLATES
		this->GetItem(COLUMN_1)->SetBackgroundColor(INFOBK);
#endif
		this->GetItem(COLUMN_2)->SetBackgroundColor(INFOBK);
	}

	CPricelistTmplReportRec(LPCTSTR grade,double price,LPCTSTR basis, int func_id)
	{
		nFuncID = func_id;
		AddItem(new CTextItem(grade));
		AddItem(new CFloatItem(price,sz2dec));
#ifdef _LOCK_PRICELIST_IN_TEMPLATES
		AddItem(new CExIconItem(basis,-1));
#else
		AddItem(new CExIconItem(basis,3));
#endif

		this->GetItem(COLUMN_0)->SetBackgroundColor(INFOBK);
#ifdef _LOCK_PRICELIST_IN_TEMPLATES
		this->GetItem(COLUMN_1)->SetBackgroundColor(INFOBK);
#endif
		this->GetItem(COLUMN_2)->SetBackgroundColor(INFOBK);
	}

	inline int getFuncID() {return nFuncID;}
	inline void setFuncID(int funcid) {nFuncID = funcid;}

	inline CString getIconColText(int item) const { return ((CExIconItem*)GetItem(item))->getExIconText();	}
	inline void setIconColText(int item,LPCTSTR text) { ((CExIconItem*)GetItem(item))->setExIconText(text); }

	inline CString getColText(int item) const { return ((CTextItem*)GetItem(item))->getTextItem();	}
	inline void setColText(int item,LPCTSTR text) { ((CTextItem*)GetItem(item))->setTextItem(text); }

	inline double getColDbl(int item) const { return ((CFloatItem*)GetItem(item))->getFloatItem();	}
	inline void setColDbl(int item,double v) { ((CFloatItem*)GetItem(item))->setFloatItem(v); }

};


//////////////////////////////////////////////////////////////////////////////////////////
// ReportRecord for Pricelist
class CPricelistReportRec : public CXTPReportRecord
{
//private:
	int nGradeID;
	int nFuncID;	//#4258 lagt till
public:
	CPricelistReportRec(void)
	{
		nGradeID = -1;
		nFuncID = -1;
		AddItem(new CTextItem(L""));
		AddItem(new CFloatItem(0.0,sz2dec));
		AddItem(new CExIconItem(L"",3));

		this->GetItem(COLUMN_0)->SetBackgroundColor(INFOBK);
		this->GetItem(COLUMN_2)->SetBackgroundColor(INFOBK);
	}

	CPricelistReportRec(int id,LPCTSTR grade,double price,LPCTSTR basis, int funcid)
	{
		nGradeID = id;
		nFuncID = funcid;

		AddItem(new CTextItem(grade));
		AddItem(new CFloatItem(price,sz2dec));
		AddItem(new CExIconItem(basis,3));
		this->GetItem(COLUMN_0)->SetBackgroundColor(INFOBK);
		this->GetItem(COLUMN_2)->SetBackgroundColor(INFOBK);
	}

	inline int getGradesID()	{ return nGradeID; }
	inline int getFuncID() {return nFuncID;}
	inline void setFuncID(int funcid) {nFuncID = funcid;}

	inline CString getIconColText(int item) const { return ((CExIconItem*)GetItem(item))->getExIconText();	}
	inline void setIconColText(int item,LPCTSTR text) { ((CExIconItem*)GetItem(item))->setExIconText(text); }

	inline CString getColText(int item) const { return ((CTextItem*)GetItem(item))->getTextItem();	}
	inline void setColText(int item,LPCTSTR text) { ((CTextItem*)GetItem(item))->setTextItem(text); }

	inline double getColDbl(int item) const { return ((CFloatItem*)GetItem(item))->getFloatItem();	}
	inline void setColDbl(int item,double v) { ((CFloatItem*)GetItem(item))->setFloatItem(v); }

};

//////////////////////////////////////////////////////////////////////////////////////////
// ReportRecord for ListPrl
class CListPrlReportRec : public CXTPReportRecord
{
//private:
	CPricelist recPrl;
public:
	CListPrlReportRec(void)
	{
		recPrl = CPricelist();
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
	}

	CListPrlReportRec(CPricelist& rec)
	{
		recPrl = rec;
		AddItem(new CTextItem(recPrl.getName()));
		AddItem(new CTextItem(recPrl.getCreatedBy()));
		AddItem(new CTextItem(recPrl.getDate()));
		AddItem(new CTextItem(recPrl.getNotes()));
	}

	inline CPricelist& getRecord()	{ return recPrl; }

	inline CString getColText(int item) const { return ((CTextItem*)GetItem(item))->getTextItem();	}
	inline void setColText(int item,LPCTSTR text) { ((CTextItem*)GetItem(item))->setTextItem(text); }
};

//////////////////////////////////////////////////////////////////////////////////////////
// ReportRecord for ListContract
class CListContractReportRec : public CXTPReportRecord
{
//private:
	CListContract recContract;
public:
	CListContractReportRec(void)
	{
		recContract = CListContract();

		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CFloatItem(0.0,sz1dec));
		AddItem(new CFloatItem(0.0,sz0dec));
	}

	CListContractReportRec(CListContract& rec)
	{
		recContract = rec;
		AddItem(new CTextItem(recContract.getTmplName()));
		AddItem(new CTextItem(recContract.getCreatedBy()));
		AddItem(new CTextItem(recContract.getDate()));
		AddItem(new CTextItem(recContract.getBuyer()));
		AddItem(new CTextItem(recContract.getHauler()));
		AddItem(new CTextItem(recContract.getLocation()));
		AddItem(new CTextItem(recContract.getTractID()));
		AddItem(new CTextItem(recContract.getScaler()));
		AddItem(new CTextItem(recContract.getSourceID()));
		AddItem(new CTextItem(recContract.getVendor()));
		AddItem(new CTextItem(recContract.getSupplier()));
		AddItem(new CTextItem(recContract.getNotes()));
		AddItem(new CTextItem(recContract.getPrlName()));
		AddItem(new CTextItem(recContract.getMeasureMode()));
		AddItem(new CFloatItem(recContract.getTrimFT(),sz1dec));
		AddItem(new CFloatItem(recContract.getTrimCM(),sz0dec));
	}

	inline CListContract& getRecord()	{ return recContract; }

	inline CString getColText(int item) const { return ((CTextItem*)GetItem(item))->getTextItem();	}
	inline void setColText(int item,LPCTSTR text) { ((CTextItem*)GetItem(item))->setTextItem(text); }
};

//////////////////////////////////////////////////////////////////////////////////////////
// ReportRecord for ListUserVolumeTables
class CListUserVTReportRec : public CXTPReportRecord
{
//private:
	CUserVT recUserVT;
public:
	CListUserVTReportRec(void)
	{
		recUserVT = CUserVT();

		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
	}

	CListUserVTReportRec(CUserVT& rec)
	{
		recUserVT = rec;
		AddItem(new CTextItem(recUserVT.getFullName()));
		AddItem(new CTextItem(recUserVT.getAbbrevName()));
		AddItem(new CTextItem(recUserVT.getBasisName()));
		AddItem(new CTextItem(recUserVT.getCreatedBy()));
		AddItem(new CTextItem(recUserVT.getDate()));
		AddItem(new CTextItem(recUserVT.getMeasuringMode()));
		AddItem(new CTextItem(recUserVT.getNotes()));
	}

	inline CUserVT& getRecord()	{ return recUserVT; }

	inline CString getColText(int item) const { return ((CTextItem*)GetItem(item))->getTextItem();	}
	inline void setColText(int item,LPCTSTR text) { ((CTextItem*)GetItem(item))->setTextItem(text); }
};


//////////////////////////////////////////////////////////////////////////////////////////
// ReportRecord for Contract
class CSelectContractReportRec : public CXTPReportRecord
{
//private:
	CLogScaleTemplates recTmpl;
public:
	CSelectContractReportRec(void)
	{
		recTmpl = CLogScaleTemplates();
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));

		this->GetItem(COLUMN_0)->SetBackgroundColor(INFOBK);
		this->GetItem(COLUMN_1)->SetBackgroundColor(INFOBK);
		this->GetItem(COLUMN_2)->SetBackgroundColor(INFOBK);
	}

	CSelectContractReportRec(CLogScaleTemplates& rec)
	{
		recTmpl = rec;
		AddItem(new CTextItem(rec.getTmplName()));
		AddItem(new CTextItem(rec.getCreatedBy()));
		AddItem(new CTextItem(rec.getDate()));
		this->GetItem(COLUMN_0)->SetBackgroundColor(INFOBK);
		this->GetItem(COLUMN_1)->SetBackgroundColor(INFOBK);
		this->GetItem(COLUMN_2)->SetBackgroundColor(INFOBK);
	}

	inline CLogScaleTemplates& getRecord()	{ return recTmpl; }

	inline CString getColText(int item) const { return ((CTextItem*)GetItem(item))->getTextItem();	}
	inline void setColText(int item,LPCTSTR text) { ((CTextItem*)GetItem(item))->setTextItem(text); }
};

//////////////////////////////////////////////////////////////////////////////////////////
// ReportRecord for Selection of Calculation basis
class CCalcTypeReportRec : public CXTPReportRecord
{
//private:
	int nID; // Primary key
public:
	CCalcTypeReportRec(void)
	{
		nID = -1;
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
	}
	CCalcTypeReportRec(int id,LPCTSTR calc_name,LPCTSTR calc_abbrev,LPCTSTR calc_basis)
	{
		nID = id;
		AddItem(new CTextItem(calc_name));
		AddItem(new CTextItem(calc_abbrev));
		AddItem(new CTextItem(calc_basis));
	}

	inline int getID()	{ return nID; }

	inline CString getColText(int item) const { return ((CTextItem*)GetItem(item))->getTextItem();	}
	inline void setColText(int item,LPCTSTR text) { ((CTextItem*)GetItem(item))->setTextItem(text); }
};

//////////////////////////////////////////////////////////////////////////////////////////
// ReportRecord for Correctins Species/Grades from added file
class CCorrectionsReportRec : public CXTPReportRecord
{
//private:
	CLogs record;
	int nIndex;
public:
	CCorrectionsReportRec(void)
	{
		nIndex = -1;
		record = CLogs();
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
	}
	CCorrectionsReportRec(int idx,CLogs& rec)
	{
		record = rec;
		nIndex = idx;
		AddItem(new CTextItem(rec.getSpcCode() ));
		if (rec.getFlag() == 1 || rec.getFlag() == 3 )
		{
			AddItem(new CTextItem(L""));
			setReportRowFntData(this->GetItem(COLUMN_0),BLUE,RED);
		}
		else
		{
			AddItem(new CTextItem(rec.getSpcCode()));
			setReportRowFntData(this->GetItem(COLUMN_0),BLACK,INFOBK);
			setReportRowFntData(this->GetItem(COLUMN_1),BLACK,INFOBK);
		}

		AddItem(new CTextItem(rec.getGrade()));
		if (rec.getFlag() == 2 || rec.getFlag() == 3 )
		{
			AddItem(new CTextItem(L""));
			setReportRowFntData(this->GetItem(COLUMN_2),BLUE,RED);
		}
		else
		{
			AddItem(new CTextItem(rec.getGrade()));
			setReportRowFntData(this->GetItem(COLUMN_2),BLACK,INFOBK);
			setReportRowFntData(this->GetItem(COLUMN_3),BLACK,INFOBK);
		}
}

	inline CLogs& getRecord() { return record; }

	inline int getIndex()	{ return nIndex; }

	inline CString getColText(int item) const { return ((CTextItem*)GetItem(item))->getTextItem();	}
	inline void setColText(int item,LPCTSTR text) { ((CTextItem*)GetItem(item))->setTextItem(text); }

};

//////////////////////////////////////////////////////////////////////////////////////////
// ReportRecord for setting Species/Grades from added file
class CSetupSpeciesReportRec : public CXTPReportRecord
{
//private:
	CLogs record;
	CSpecies recSpc;
	int nIndex;
public:
	CSetupSpeciesReportRec(void)
	{
		nIndex = -1;
		record = CLogs();
		recSpc = CSpecies();
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
	}
	CSetupSpeciesReportRec(int idx,CLogs& rec)
	{
		record = rec;
		recSpc = CSpecies();
		nIndex = idx;
		AddItem(new CTextItem(rec.getSpcCode() ));
		AddItem(new CTextItem(L""));
	}

	CSetupSpeciesReportRec(int idx,CSpecies& rec)
	{
		record = CLogs();
		recSpc = rec;
		nIndex = idx;
		if (rec.getSpcName().IsEmpty())
			AddItem(new CTextItem(rec.getSpcCode()));
		else
			AddItem(new CTextItem(rec.getSpcCode() + L" - " + rec.getSpcName()));
		AddItem(new CTextItem(L""));
	}

	inline CLogs& getRecord() { return record; }
	inline CSpecies& getSpcRecord() { return recSpc; }

	inline int getIndex()	{ return nIndex; }

	inline CString getColText(int item) const { return ((CTextItem*)GetItem(item))->getTextItem();	}
	inline void setColText(int item,LPCTSTR text) { ((CTextItem*)GetItem(item))->setTextItem(text); }

};

//////////////////////////////////////////////////////////////////////////////////////////
// CSetupGradesReportRec 
class CSetupGradesReportRec : public CXTPReportRecord
{
//private:
	CLogs record;
	int nIndex;
public:
	CSetupGradesReportRec(void)
	{
		nIndex = -1;
		record = CLogs();
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
	}
	CSetupGradesReportRec(int idx,CLogs& rec)
	{
		record = rec;
		nIndex = idx;
		AddItem(new CTextItem(rec.getGrade() ));
		AddItem(new CTextItem(L""));
	}

	inline CLogs& getRecord() { return record; }

	inline int getIndex()	{ return nIndex; }

	inline CString getColText(int item) const { return ((CTextItem*)GetItem(item))->getTextItem();	}
	inline void setColText(int item,LPCTSTR text) { ((CTextItem*)GetItem(item))->setTextItem(text); }

};

//////////////////////////////////////////////////////////////////////////////////////////
// CSetupGradesReportRec2 
class CSetupGradesReportRec2 : public CXTPReportRecord
{
//private:
	CGradesExt recGrd;
	int nIndex;
public:
	CSetupGradesReportRec2(void)
	{
		nIndex = -1;
		recGrd = CGrades();
		AddItem(new CCheckItem(FALSE));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
	}
	CSetupGradesReportRec2(int idx,CGradesExt& rec)
	{
		recGrd = rec;
		nIndex = idx;
		AddItem(new CCheckItem(rec.getIsPulpwood()));
		if (rec.getGradesName().IsEmpty())
			AddItem(new CTextItem(rec.getGradesCode()));
		else
			AddItem(new CTextItem(rec.getGradesCode() + L" - " + rec.getGradesName()));
		AddItem(new CTextItem(L""));
	}

	inline CGradesExt& getGrdRecord() { return recGrd; }

	inline int getIndex()	{ return nIndex; }

	inline BOOL getColChecked(int item) const { return ((CCheckItem*)GetItem(item))->getChecked();	}
	inline void setColChecked(int item,BOOL check) { ((CCheckItem*)GetItem(item))->setChecked(check); }
	
	inline CString getColText(int item) const { return ((CTextItem*)GetItem(item))->getTextItem();	}
	inline void setColText(int item,LPCTSTR text) { ((CTextItem*)GetItem(item))->setTextItem(text); }

};

//////////////////////////////////////////////////////////////////////////////////////////
// ReportRecord for Register
class CRegisterReportRec : public CXTPReportRecord
{
//private:
	CRegister record;
public:
	CRegisterReportRec(void)
	{
		record = CRegister();
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
	}
	CRegisterReportRec(CRegister& rec)
	{
		record = rec;
		AddItem(new CTextItem(rec.getName() ));
		AddItem(new CTextItem(rec.getAbbrevation() ));
		AddItem(new CTextItem(rec.getID() ));
		AddItem(new CTextItem(rec.getOtherInfo() ));
		AddItem(new CTextItem(rec.getAddress() ));
		AddItem(new CTextItem(rec.getAddress2() ));
		AddItem(new CTextItem(rec.getZipCode() ));
		AddItem(new CTextItem(rec.getCity() ));
		AddItem(new CTextItem(rec.getPhone() ));
		AddItem(new CTextItem(rec.getCellPhone() ));
		AddItem(new CTextItem(rec.getWWW() ));
}

	inline CRegister& getRecord() { return record; }

	inline CString getColText(int item) const { return ((CTextItem*)GetItem(item))->getTextItem();	}
	inline void setColText(int item,LPCTSTR text) { ((CTextItem*)GetItem(item))->setTextItem(text); }

};


//////////////////////////////////////////////////////////////////////////////////////////
// ReportRecord for Ticket errors on Import
class CImportTicketReportRec : public CXTPReportRecord
{
//private:
public:
	CImportTicketReportRec(void)
	{
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
	}
	CImportTicketReportRec(int id,LPCTSTR cap,LPCTSTR value,LPCTSTR spc_value)
	{
		AddItem(new CTextItem(cap ));
		AddItem(new CTextItem(value ));
		AddItem(new CTextItem(spc_value ));
	}
	CImportTicketReportRec(int id,LPCTSTR cap,double value,double spc_value)
	{
		AddItem(new CTextItem(cap ));
		AddItem(new CFloatItem(value ));
		AddItem(new CFloatItem(spc_value ));
	}

	inline CString getColText(int item) const { return ((CTextItem*)GetItem(item))->getTextItem();	}
	inline void setColText(int item,LPCTSTR text) { ((CTextItem*)GetItem(item))->setTextItem(text); }

};

//////////////////////////////////////////////////////////////////////////////////////////
// ReportRecord for Usercreated value-tables
class CUserVolumeTableRec : public CXTPReportRecord
{
//private:
	double fDiam;
	double fStep;
	int nNumOfColumns;
public:
	CUserVolumeTableRec()	{}

	CUserVolumeTableRec(double diam,double step,int num_of_columns)
	{
		fDiam = diam;
		fStep = step;
		nNumOfColumns = num_of_columns;
		if (num_of_columns > 0)
		{
			for (int i = 0;i < num_of_columns;i++)
			{
				if (i == 0)
					AddItem(new CFloatItem(diam,sz0dec));
				else
					AddItem(new CFloatItem(0.0,sz3dec));
			}
		}
	}
	// Use this to add data for columns
	CUserVolumeTableRec(double diam,double step,std::vector<double>& vec)
	{
		fDiam = diam;
		fStep = step;
		nNumOfColumns = vec.size();
		if (vec.size() > 0)
		{
			for (UINT col = 0;col < vec.size();col++)
			{
				if (col == 0)
				{
					AddItem(new CFloatItem(fDiam,sz0dec));
				}
				else
				{
					AddItem(new CFloatItem(vec[col],sz3dec));
				}
			}
		}
	}

	// Use this to add data for columns
	void AddToColumn(int column,int num_of)
	{
		if (num_of > 0 && column > 0)
		{
			for (int i = column;i < (num_of + column);i++)
			{
				AddItem(new CFloatItem(0.0,sz3dec));
			}
		}
	}


	inline double getDiam()	{ return fDiam; }
	inline double getStep()	{ return fStep; }
	inline int getNumOfColumns()	{ return nNumOfColumns; }

	inline CString getDiamStep()	{ CString S; S.Format(L"%.1f$%.1f",fDiam,fStep); return S; }

	inline int getColInt(int item) const	{ return ((CIntItem*)GetItem(item))->getIntItem();	}
	inline void setColInt(int item,int v) const	{ ((CIntItem*)GetItem(item))->setIntItem(v);	}

	inline double getColFloat(int item) const	{ return ((CFloatItem*)GetItem(item))->getFloatItem();	}
	inline void setColFloat(int item,double v) const	{ ((CFloatItem*)GetItem(item))->setFloatItem(v);	}

};


//////////////////////////////////////////////////////////////////////////////////////////
// TemplateExport for Grades
class CTemplateExportReportRec : public CXTPReportRecord
{
//private:
	CLogScaleTemplates record;
public:
	CTemplateExportReportRec(void)
	{
		record = CLogScaleTemplates();
		AddItem(new CCheckItem(FALSE));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
	}
	CTemplateExportReportRec(CLogScaleTemplates& rec)
	{
		record = rec;
		AddItem(new CCheckItem(FALSE));
		AddItem(new CTextItem(rec.getTmplName()));
		AddItem(new CTextItem(rec.getCreatedBy() ));
		AddItem(new CTextItem(rec.getDate() ));
		AddItem(new CTextItem(rec.getPrlName() ));
	}

	inline CLogScaleTemplates& getRecord() { return record; }

	inline CString getColText(int item) const { return ((CTextItem*)GetItem(item))->getTextItem();	}
	inline void setColText(int item,LPCTSTR text) { ((CTextItem*)GetItem(item))->setTextItem(text); }

	inline BOOL getColChecked(int item) const { return ((CCheckItem*)GetItem(item))->getChecked();	}
	inline void setColChecked(int item,BOOL check) { ((CCheckItem*)GetItem(item))->setChecked(check); }
};


//////////////////////////////////////////////////////////////////////////////////////////
// Search
class CSearchReportRec : public CXTPReportRecord
{
//private:
	CString sTmp;
	CSearch record;
public:
	CSearchReportRec(void)
	{
		record = CSearch();
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
//		AddItem(new CFloatItem(0.0,sz1dec));
//		AddItem(new CFloatItem(0.0,sz1dec));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
//		AddItem(new CFloatItem(0.0,sz1dec));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
		AddItem(new CTextItem(L""));
	}
	CSearchReportRec(CSearch& rec,CStringArray& status)
	{
		record = rec;
//		AddItem(new CTextItem(rec.getTicket()));
		AddItem(new CExIconItem(rec.getTicket(),3));

		AddItem(new CTextItem(rec.getTag()));
		if (rec.getSalesName().IsEmpty())
			AddItem(new CExIconItem(L"",8));
		else
			AddItem(new CExIconItem(rec.getSalesName(),3));
		if (rec.getStatus() >= 0 && rec.getStatus() < status.GetCount())
			AddItem(new CTextItem(status.GetAt(rec.getStatus())));
		AddItem(new CTextItem(rec.getSpcCode().Trim()));
		AddItem(new CTextItem(rec.getSpcName()));
		AddItem(new CTextItem(rec.getGradeCode().Trim()));
		sTmp.Format(L"%.3f",rec.getVolume());
		AddItem(new CTextItem(sTmp));
		//AddItem(new CFloatItem(rec.getVolume(),sz3dec));
	
		sTmp.Format(L"%.1f",rec.getPrice());
		AddItem(new CTextItem(sTmp));
//		AddItem(new CFloatItem(rec.getPrice(),sz1dec));

		AddItem(new CTextItem(rec.getFuncName()));

//		sTmp.Format(L"%.1f",rec.getLengthMeas());
//		AddItem(new CTextItem(sTmp));
		AddItem(new CFloatItem(rec.getLengthMeas(),sz1dec));

		AddItem(new CTextItem(rec.getStoreDate()));
		AddItem(new CTextItem(rec.getUpdDate()));
	}

	inline CSearch& getRecord() { return record; }
	inline CString getColText(int item) const { return ((CTextItem*)GetItem(item))->getTextItem();	}
	inline double getColFloat(int item) const { return ((CFloatItem*)GetItem(item))->getFloatItem();	}

	inline CString getIconColText(int item) const { return ((CExIconItem*)GetItem(item))->getExIconText();	}

};

#endif
