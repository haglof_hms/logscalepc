#include "stdafx.h"
#include "dbhandling.h"

#include "reportclasses.h"


CDBHandling::CDBHandling(void)
	: CDBBaseClass_SQLApi(SA_Client_NotSpecified)
{
}

CDBHandling::CDBHandling(DB_CONNECTION_DATA &db_connection)
	: CDBBaseClass_SQLApi(db_connection,1)
{
}

////////////////////////////////////////////////////////////////
// TICKETS
// Add result from dataset directly into CXTPReportControl 
BOOL CDBHandling::getTickets(CVecTickets& vec,CXTPReportControl &report)
{
	CString sSQL = L"";
	CTickets rec = CTickets();
	BOOL bReturn = TRUE;
	try
	{
		vec.clear();
		if (report.GetSafeHwnd())
		{
			report.ResetContent();	// Clear

			sSQL.Format(_T("SELECT * FROM %s ORDER BY pkLoadID"),TBL_TICKETS);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();
			
			while(m_saCommand.FetchNext())
			{
				SADateTime saCreated = m_saCommand.Field("dCreated").asDateTime();
				SADateTime saUpdated = m_saCommand.Field("dUpdated").asDateTime();
				rec = CTickets(m_saCommand.Field("pkLoadID").asLong(),
											 (LPCTSTR)m_saCommand.Field("sSourceDate").asString(),
											 (LPCTSTR)m_saCommand.Field("sSourceID").asString(),
											 (LPCTSTR)m_saCommand.Field("sScaler").asString(),
											 (LPCTSTR)m_saCommand.Field("sLocation").asString(),
											 (LPCTSTR)m_saCommand.Field("sBuyer").asString(),
											 (LPCTSTR)m_saCommand.Field("sOtherInfo").asString(),
											 (LPCTSTR)m_saCommand.Field("sLoadID").asString(),
											 (LPCTSTR)m_saCommand.Field("sTicket").asString(),
											 (LPCTSTR)m_saCommand.Field("sTractID").asString(),
											 (LPCTSTR)m_saCommand.Field("sHauler").asString(),
											 (LPCTSTR)m_saCommand.Field("sSupplier").asString(),
											 (LPCTSTR)m_saCommand.Field("sVendor").asString(),
											 (LPCTSTR)m_saCommand.Field("sLoadType").asString(),
											 (LPCTSTR)m_saCommand.Field("sDate").asString(),
											 m_saCommand.Field("fWeight").asDouble(),
											 m_saCommand.Field("nNumOfLogs").asShort(),
											 (LPCTSTR)m_saCommand.Field("sNotes").asString(),
											 m_saCommand.Field("nSawlogVolumeUnit").asShort(),
											 m_saCommand.Field("nPulpwoodVolUnit").asShort(),
											 m_saCommand.Field("nRoundScaling").asShort(),
											 m_saCommand.Field("nUseSouthernDoyle").asShort(),
											 (LPCTSTR)m_saCommand.Field("sDeductionType").asString(),
											 m_saCommand.Field("fIBTaper").asDouble(),
											 m_saCommand.Field("fBarkRatio").asDouble(),
											 m_saCommand.Field("nMeasuringMode").asShort(),
											 m_saCommand.Field("fSumVolFromWeight").asDouble(),
											 m_saCommand.Field("fSumVolPricelist").asDouble(),
											 m_saCommand.Field("fSumPrice").asDouble(),
											 (LPCTSTR)m_saCommand.Field("sFileVersion").asString(),
											 (LPCTSTR)m_saCommand.Field("sTemplateUsed").asString(),
											 (LPCTSTR)m_saCommand.Field("sPricelistUsed").asString(),
											 m_saCommand.Field("nPricelistID").asShort(),
											 (LPCTSTR)m_saCommand.Field("sGISCoord1").asString(),
											 (LPCTSTR)m_saCommand.Field("sGISCoord2").asString(),
											 m_saCommand.Field("nJAS").asShort(),
											 m_saCommand.Field("fTrimFT").asDouble(),
											 m_saCommand.Field("fTrimCM").asDouble(),
											 m_saCommand.Field("nTemplateID").asShort(),
											 m_saCommand.Field("nLocked").asShort(),
											 (LPCTSTR)convSADateTime(saCreated),
											 (LPCTSTR)convSADateTime(saUpdated));
				vec.push_back(rec);

				report.AddRecord(new CTicketsReportRec(rec));
			}

			doCommit();
			report.Populate();
			report.UpdateWindow();
		}
		else
			bReturn = FALSE;

	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return bReturn;
}

BOOL CDBHandling::getTickets(CVecTickets& vec)
{
	CString sSQL = L"";
	try
	{
		vec.clear();
		
		sSQL.Format(_T("SELECT * FROM %s"),TBL_TICKETS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
				SADateTime saCreated = m_saCommand.Field("dCreated").asDateTime();
				SADateTime saUpdated = m_saCommand.Field("dUpdated").asDateTime();
				vec.push_back( CTickets(m_saCommand.Field("pkLoadID").asLong(),
											 (LPCTSTR)m_saCommand.Field("sSourceDate").asString(),
											 (LPCTSTR)m_saCommand.Field("sSourceID").asString(),
											 (LPCTSTR)m_saCommand.Field("sScaler").asString(),
											 (LPCTSTR)m_saCommand.Field("sLocation").asString(),
											 (LPCTSTR)m_saCommand.Field("sBuyer").asString(),
											 (LPCTSTR)m_saCommand.Field("sOtherInfo").asString(),
											 (LPCTSTR)m_saCommand.Field("sLoadID").asString(),
											 (LPCTSTR)m_saCommand.Field("sTicket").asString(),
											 (LPCTSTR)m_saCommand.Field("sTractID").asString(),
											 (LPCTSTR)m_saCommand.Field("sHauler").asString(),
											 (LPCTSTR)m_saCommand.Field("sSupplier").asString(),
											 (LPCTSTR)m_saCommand.Field("sVendor").asString(),
											 (LPCTSTR)m_saCommand.Field("sLoadType").asString(),
											 (LPCTSTR)m_saCommand.Field("sDate").asString(),
											 m_saCommand.Field("fWeight").asDouble(),
											 m_saCommand.Field("nNumOfLogs").asShort(),
											 (LPCTSTR)m_saCommand.Field("sNotes").asString(),
											 m_saCommand.Field("nSawlogVolumeUnit").asShort(),
											 m_saCommand.Field("nPulpwoodVolUnit").asShort(),
											 m_saCommand.Field("nRoundScaling").asShort(),
											 m_saCommand.Field("nUseSouthernDoyle").asShort(),
											 (LPCTSTR)m_saCommand.Field("sDeductionType").asString(),
											 m_saCommand.Field("fIBTaper").asDouble(),
											 m_saCommand.Field("fBarkRatio").asDouble(),
											 m_saCommand.Field("nMeasuringMode").asShort(),
											 m_saCommand.Field("fSumVolFromWeight").asDouble(),
											 m_saCommand.Field("fSumVolPricelist").asDouble(),
											 m_saCommand.Field("fSumPrice").asDouble(),
											 (LPCTSTR)m_saCommand.Field("sFileVersion").asString(),
											 (LPCTSTR)m_saCommand.Field("sTemplateUsed").asString(),
											 (LPCTSTR)m_saCommand.Field("sPricelistUsed").asString(),
											 m_saCommand.Field("nPricelistID").asShort(),
											 (LPCTSTR)m_saCommand.Field("sGISCoord1").asString(),
											 (LPCTSTR)m_saCommand.Field("sGISCoord2").asString(),
											 m_saCommand.Field("nJAS").asShort(),
											 m_saCommand.Field("fTrimFT").asDouble(),
											 m_saCommand.Field("fTrimCM").asDouble(),
											 m_saCommand.Field("nTemplateID").asShort(),
											 m_saCommand.Field("nLocked").asShort(),
											 (LPCTSTR)convSADateTime(saCreated),
											 (LPCTSTR)convSADateTime(saUpdated)));
		}

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::getTicket(CTickets& rec,int id)
{
	CString sSQL = L"";
	BOOL bReturn = TRUE;
	try
	{
			sSQL.Format(_T("SELECT * FROM %s WHERE pkLoadID=%d ORDER BY pkLoadID"),TBL_TICKETS,id);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();
			
			while(m_saCommand.FetchNext())
			{
				SADateTime saCreated = m_saCommand.Field("dCreated").asDateTime();
				SADateTime saUpdated = m_saCommand.Field("dUpdated").asDateTime();
				rec = CTickets(m_saCommand.Field("pkLoadID").asLong(),
											 (LPCTSTR)m_saCommand.Field("sSourceDate").asString(),
											 (LPCTSTR)m_saCommand.Field("sSourceID").asString(),
											 (LPCTSTR)m_saCommand.Field("sScaler").asString(),
											 (LPCTSTR)m_saCommand.Field("sLocation").asString(),
											 (LPCTSTR)m_saCommand.Field("sBuyer").asString(),
											 (LPCTSTR)m_saCommand.Field("sOtherInfo").asString(),
											 (LPCTSTR)m_saCommand.Field("sLoadID").asString(),
											 (LPCTSTR)m_saCommand.Field("sTicket").asString(),
											 (LPCTSTR)m_saCommand.Field("sTractID").asString(),
											 (LPCTSTR)m_saCommand.Field("sHauler").asString(),
											 (LPCTSTR)m_saCommand.Field("sSupplier").asString(),
											 (LPCTSTR)m_saCommand.Field("sVendor").asString(),
											 (LPCTSTR)m_saCommand.Field("sLoadType").asString(),
											 (LPCTSTR)m_saCommand.Field("sDate").asString(),
											 m_saCommand.Field("fWeight").asDouble(),
											 m_saCommand.Field("nNumOfLogs").asShort(),
											 (LPCTSTR)m_saCommand.Field("sNotes").asString(),
											 m_saCommand.Field("nSawlogVolumeUnit").asShort(),
											 m_saCommand.Field("nPulpwoodVolUnit").asShort(),
											 m_saCommand.Field("nRoundScaling").asShort(),
											 m_saCommand.Field("nUseSouthernDoyle").asShort(),
											 (LPCTSTR)m_saCommand.Field("sDeductionType").asString(),
											 m_saCommand.Field("fIBTaper").asDouble(),
											 m_saCommand.Field("fBarkRatio").asDouble(),
											 m_saCommand.Field("nMeasuringMode").asShort(),
											 m_saCommand.Field("fSumVolFromWeight").asDouble(),
											 m_saCommand.Field("fSumVolPricelist").asDouble(),
											 m_saCommand.Field("fSumPrice").asDouble(),
											 (LPCTSTR)m_saCommand.Field("sFileVersion").asString(),
											 (LPCTSTR)m_saCommand.Field("sTemplateUsed").asString(),
											 (LPCTSTR)m_saCommand.Field("sPricelistUsed").asString(),
											 m_saCommand.Field("nPricelistID").asShort(),
											 (LPCTSTR)m_saCommand.Field("sGISCoord1").asString(),
											 (LPCTSTR)m_saCommand.Field("sGISCoord2").asString(),
											 m_saCommand.Field("nJAS").asShort(),
											 m_saCommand.Field("fTrimFT").asDouble(),
											 m_saCommand.Field("fTrimCM").asDouble(),
											 m_saCommand.Field("nTemplateID").asShort(),
											 m_saCommand.Field("nLocked").asShort(),
											 (LPCTSTR)convSADateTime(saCreated),
											 (LPCTSTR)convSADateTime(saUpdated));

		}
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return bReturn;
}

BOOL CDBHandling::newTicket(CTickets& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"INSERT INTO %s (sSourceDate,sSourceID,sScaler,sLocation,sBuyer,sOtherInfo,sLoadID,sTicket,sTractID,sHauler,sSupplier,sVendor,sLoadType,"
								L"sDate,fWeight,nNumOfLogs,sNotes,nSawlogVolumeUnit,nPulpwoodVolUnit,nRoundScaling,nUseSouthernDoyle,sDeductionType,"
								L"fIBTaper,fBarkRatio,nMeasuringMode,fSumVolFromWeight,fSumVolPricelist,fSumPrice,sFileVersion,sTemplateUsed,sPricelistUsed,"
								L"nPricelistID,sGISCoord1,sGISCoord2,nJAS,fTrimFT,fTrimCM,nTemplateID,nLocked) "
								L"VALUES (:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19,:20,:21,:22,:23,:24,:25,:26,:27,:28,:29,:30,:31,:32,:33,:34,:35,:36,:37,:38,:39)",TBL_TICKETS);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsString()	= rec.getSourceDate();
		m_saCommand.Param(2).setAsString()	= rec.getSourceID();
		m_saCommand.Param(3).setAsString()	= rec.getScaler();
		m_saCommand.Param(4).setAsString()	= rec.getLocation();
		m_saCommand.Param(5).setAsString()	= rec.getBuyer();
		m_saCommand.Param(6).setAsString()	= rec.getOtherInfo();
		m_saCommand.Param(7).setAsString()	= rec.getLoadID();
		m_saCommand.Param(8).setAsString()	= rec.getTicket();
		m_saCommand.Param(9).setAsString()	= rec.getTractID();
		m_saCommand.Param(10).setAsString()	= rec.getHauler();
		m_saCommand.Param(11).setAsString()	= rec.getSupplier();
		m_saCommand.Param(12).setAsString()	= rec.getVendor();
		m_saCommand.Param(13).setAsString()	= rec.getLoadType();
		m_saCommand.Param(14).setAsString()	= rec.getDate();
		m_saCommand.Param(15).setAsDouble()	= rec.getWeight();
		m_saCommand.Param(16).setAsShort()	= rec.getNumOfLogs();
		m_saCommand.Param(17).setAsString()	= rec.getNotes();
		m_saCommand.Param(18).setAsShort()	= rec.getSawlogVolUnit();
		m_saCommand.Param(19).setAsShort()	= rec.getPulpVolUnit();
		m_saCommand.Param(20).setAsShort()	= rec.getRoundScaling();
		m_saCommand.Param(21).setAsShort()	= rec.getUseSouthernDoyle();
		m_saCommand.Param(22).setAsString()	= rec.getDeductionType();
		m_saCommand.Param(23).setAsDouble()	= rec.getIBTaper();
		m_saCommand.Param(24).setAsDouble()	= rec.getBarkRation();
		m_saCommand.Param(25).setAsShort()	= rec.getMeasuringMode();
		m_saCommand.Param(26).setAsDouble()	= rec.getVolWeight();
		m_saCommand.Param(27).setAsDouble()	= rec.getSumVolPricelist();
		m_saCommand.Param(28).setAsDouble()	= rec.getSumPrice();
		m_saCommand.Param(29).setAsString()	= rec.getFileVersion();
		m_saCommand.Param(30).setAsString()	= rec.getTmplUsed();
		m_saCommand.Param(31).setAsString()	= rec.getPrlUsed();
		m_saCommand.Param(32).setAsShort()	= rec.getPrlID();
		m_saCommand.Param(33).setAsString()	= rec.getGISCoord1();
		m_saCommand.Param(34).setAsString()	= rec.getGISCoord2();
		m_saCommand.Param(35).setAsShort()	= rec.getJAS();
		m_saCommand.Param(36).setAsDouble()	= rec.getTrimFT();
		m_saCommand.Param(37).setAsDouble()	= rec.getTrimCM();
		m_saCommand.Param(38).setAsShort()	= rec.getTemplateID();
		m_saCommand.Param(39).setAsShort()	= rec.getLocked();

		m_saCommand.Execute();

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
 		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	rec.setPKID(getLastLoadID());

	return TRUE;
}

BOOL CDBHandling::updTicket(CTickets& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"UPDATE %s SET sSourceDate=:1,sSourceID=:2,sScaler=:3,sLocation=:4,sBuyer=:5,sOtherInfo=:6,sLoadID=:7,sTicket=:8,sTractID=:9,sHauler=:10,sSupplier=:11,sVendor=:12,sLoadType=:13,"
								L"sDate=:14,fWeight=:15,nNumOfLogs=:16,sNotes=:17,nSawlogVolumeUnit=:18,nPulpwoodVolUnit=:19,nRoundScaling=:20,nUseSouthernDoyle=:21,sDeductionType=:22,"
								L"fIBTaper=:23,fBarkRatio=:24,nMeasuringMode=:25,fSumVolFromWeight=:26,fSumVolPricelist=:27,fSumPrice=:28,sFileVersion=:29,sTemplateUsed=:30,"
								L"sPricelistUsed=:31,nPricelistID=:32,sGISCoord1=:33,sGISCoord2=:34,nJAS=:35,fTrimFT=:36,fTrimCM=:37,nTemplateID=:38,nLocked=:39,dUpdated=GETDATE() WHERE pkLoadID=:40",TBL_TICKETS);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsString()	= rec.getSourceDate();
		m_saCommand.Param(2).setAsString()	= rec.getSourceID();
		m_saCommand.Param(3).setAsString()	= rec.getScaler();
		m_saCommand.Param(4).setAsString()	= rec.getLocation();
		m_saCommand.Param(5).setAsString()	= rec.getBuyer();
		m_saCommand.Param(6).setAsString()	= rec.getOtherInfo();
		m_saCommand.Param(7).setAsString()	= rec.getLoadID();
		m_saCommand.Param(8).setAsString()	= rec.getTicket();
		m_saCommand.Param(9).setAsString()	= rec.getTractID();
		m_saCommand.Param(10).setAsString()	= rec.getHauler();
		m_saCommand.Param(11).setAsString()	= rec.getSupplier();
		m_saCommand.Param(12).setAsString()	= rec.getVendor();
		m_saCommand.Param(13).setAsString()	= rec.getLoadType();
		m_saCommand.Param(14).setAsString()	= rec.getDate();
		m_saCommand.Param(15).setAsDouble()	= rec.getWeight();
		m_saCommand.Param(16).setAsShort()	= rec.getNumOfLogs();
		m_saCommand.Param(17).setAsString()	= rec.getNotes();
		m_saCommand.Param(18).setAsShort()	= rec.getSawlogVolUnit();
		m_saCommand.Param(19).setAsShort()	= rec.getPulpVolUnit();
		m_saCommand.Param(20).setAsShort()	= rec.getRoundScaling();
		m_saCommand.Param(21).setAsShort()	= rec.getUseSouthernDoyle();
		m_saCommand.Param(22).setAsString()	= rec.getDeductionType();
		m_saCommand.Param(23).setAsDouble()	= rec.getIBTaper();
		m_saCommand.Param(24).setAsDouble()	= rec.getBarkRation();
		m_saCommand.Param(25).setAsShort()	= rec.getMeasuringMode();
		m_saCommand.Param(26).setAsDouble()	= rec.getVolWeight();	
		m_saCommand.Param(27).setAsDouble()	= rec.getSumVolPricelist();
		m_saCommand.Param(28).setAsDouble()	= rec.getSumPrice();	
		m_saCommand.Param(29).setAsString()	= rec.getFileVersion();
		m_saCommand.Param(30).setAsString()	= rec.getTmplUsed();
		m_saCommand.Param(31).setAsString()	= rec.getPrlUsed();
		m_saCommand.Param(32).setAsShort()	= rec.getPrlID();
		m_saCommand.Param(33).setAsString()	= rec.getGISCoord1();
		m_saCommand.Param(34).setAsString()	= rec.getGISCoord2();
		m_saCommand.Param(35).setAsShort()	= rec.getJAS();
		m_saCommand.Param(36).setAsDouble()	= rec.getTrimFT();
		m_saCommand.Param(37).setAsDouble()	= rec.getTrimCM();
		m_saCommand.Param(38).setAsShort()	= rec.getTemplateID();
		m_saCommand.Param(39).setAsShort()	= rec.getLocked();

		m_saCommand.Param(40).setAsLong()	= rec.getPKID();

		m_saCommand.Execute();

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::delTicket(CTickets& rec)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("DELETE FROM %s WHERE pkLoadID=:1"),TBL_TICKETS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = rec.getPKID();

		m_saCommand.Execute();	
		
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::delTicket(int pk_id)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("DELETE FROM %s WHERE pkLoadID=:1"),TBL_TICKETS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Param(1).setAsLong() = pk_id;

		m_saCommand.Execute();	
		
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

int CDBHandling::getLastLoadID_generate(void)
{
	int nLastID = 0;
	CString sSQL;
	try
	{
		sSQL.Format(_T("SELECT MAX(pkLoadID) AS 'last_id' FROM %s"),TBL_TICKETS);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Execute();	

		while(m_saCommand.FetchNext())
		{
			nLastID = m_saCommand.Field("last_id").asLong();
		}
		
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return 0;
	}

	return nLastID;
}

int CDBHandling::getLastLoadID(void)
{
	return last_identity(TBL_TICKETS,_T("pkLoadID"));
}


BOOL CDBHandling::resetTicketsIdentityField(void)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("DBCC CHECKIDENT('%s', RESEED, 0)"),TBL_TICKETS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}
	return TRUE;
}


BOOL CDBHandling::setLockOnTicket(int ticket_id,int v)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"UPDATE %s SET nLocked=:1,dUpdated=GETDATE() WHERE pkLoadID=:2",TBL_TICKETS);
		
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsShort()	= v;

		m_saCommand.Param(2).setAsLong()	= ticket_id;

		m_saCommand.Execute();

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::updTicketNumOfLogs(int pk_id,int num_of)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"UPDATE %s SET nNumOfLogs=:1,dUpdated=GETDATE() WHERE pkLoadID=:2",TBL_TICKETS);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsShort()	= num_of;

		m_saCommand.Param(2).setAsLong()	= pk_id;

		m_saCommand.Execute();

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

////////////////////////////////////////////////////////////////
// TICKET SPECIES

BOOL CDBHandling::getTicketSpc(CVecTicketSpc& vec,int ticket_id,int spc_id)
{
	CString sSQL = L"";
	CTicketSpc rec = CTicketSpc();
	BOOL bReturn = TRUE;
	try
	{
		vec.clear();

		if (spc_id == -1)
			sSQL.Format(_T("SELECT * FROM %s WHERE fkTicketID=%d ORDER BY fkSpeciesID,fkTicketID"),TBL_TICKET_SPC,ticket_id);
		else if (spc_id > 0)
			sSQL.Format(_T("SELECT * FROM %s WHERE fkSpeciesID=%d AND fkTicketID=%d ORDER BY fkSpeciesID,fkTicketID"),TBL_TICKET_SPC,spc_id,ticket_id);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
	
		while(m_saCommand.FetchNext())
		{
			rec = CTicketSpc(m_saCommand.Field("fkTicketID").asLong(),
											 m_saCommand.Field("fkSpeciesID").asShort(),
											 m_saCommand.Field("sSpeciesCode").asString(),
											 m_saCommand.Field("sSpeciesName").asString(),
											 m_saCommand.Field("fBarkReduction").asDouble());
			vec.push_back(rec);

		}

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return bReturn;
}

BOOL CDBHandling::newTicketSpc(CTicketSpc& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"INSERT INTO %s (fkTicketID,fkSpeciesID,sSpeciesCode,sSpeciesName,fBarkReduction) VALUES (:1,:2,:3,:4,:5)",TBL_TICKET_SPC);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsLong()		= rec.getTicketID();
		m_saCommand.Param(2).setAsShort()		= rec.getSpeciesID();
		m_saCommand.Param(3).setAsString()	= rec.getSpeciesCode();
		m_saCommand.Param(4).setAsString()	= rec.getSpeciesName();
		m_saCommand.Param(5).setAsDouble()	= rec.getBarkReduction();

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::updTicketSpc(CTicketSpc& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"UPDATE %s SET sSpeciesCode=:1,sSpeciesName=:2,fBarkReduction=:3 WHERE fkSpeciesID=:4 AND fkTicketID=:5",TBL_TICKET_SPC);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsString()	= rec.getSpeciesCode();
		m_saCommand.Param(2).setAsString()	= rec.getSpeciesName();
		m_saCommand.Param(3).setAsDouble()	= rec.getBarkReduction();

		m_saCommand.Param(4).setAsShort()		= rec.getSpeciesID();
		m_saCommand.Param(5).setAsLong()		= rec.getTicketID();

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::delTicketSpc(int ticket_id)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"DELETE FROM %s WHERE fkTicketID=:1",TBL_TICKET_SPC);
		
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsLong()	= ticket_id;

		m_saCommand.Execute();	
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

////////////////////////////////////////////////////////////////
// TICKET PRICELIST

BOOL CDBHandling::getTicketPrl(CVecTicketPrl& vec,int ticket_id,int spc_id)
{
	CString sSQL = L"";
	CTicketPrl rec = CTicketPrl();
	BOOL bReturn = TRUE;
	try
	{
		vec.clear();

		if (spc_id == -1)
			sSQL.Format(_T("SELECT * FROM %s WHERE fkTicketID=%d ORDER BY fkTicketID,fkSpeciesID"),TBL_TICKET_PRL,ticket_id);
		else if (spc_id > 0)
			sSQL.Format(_T("SELECT * FROM %s WHERE fkTicketID=%d AND fkSpeciesID=%d ORDER BY fkTicketID,fkSpeciesID"),TBL_TICKET_PRL,ticket_id,spc_id);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
	
		while(m_saCommand.FetchNext())
		{
			rec = CTicketPrl(m_saCommand.Field("fkTicketID").asLong(),
											 m_saCommand.Field("fkSpeciesID").asShort(),
											 m_saCommand.Field("fkGradesID").asShort(),
											 m_saCommand.Field("sSpeciesCode").asString(),
											 m_saCommand.Field("sGradesCode").asString(),
											 m_saCommand.Field("fPrice").asDouble(),
											 m_saCommand.Field("nFuncID").asShort(),
											 m_saCommand.Field("sCalcType").asString(),
											 m_saCommand.Field("nPulpwood").asShort());
			vec.push_back(rec);

		}

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return bReturn;
}

BOOL CDBHandling::newTicketPrl(CTicketPrl& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"INSERT INTO %s (fkTicketID,fkSpeciesID,fkGradesID,sSpeciesCode,sGradesCode,fPrice,nFuncID,sCalcType,nPulpwood) VALUES (:1,:2,:3,:4,:5,:6,:7,:8,:9)",TBL_TICKET_PRL);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsLong()		= rec.getTicketID();
		m_saCommand.Param(2).setAsShort()		= rec.getSpeciesID();
		m_saCommand.Param(3).setAsShort()		= rec.getGradesID();
		m_saCommand.Param(4).setAsString()	= rec.getSpeciesCode();
		m_saCommand.Param(5).setAsString()	= rec.getGradesCode();
		m_saCommand.Param(6).setAsDouble()	= rec.getPrice();
		m_saCommand.Param(7).setAsShort()		= rec.getFuncID();
		m_saCommand.Param(8).setAsString()	= rec.getCalcType();
		m_saCommand.Param(9).setAsShort()		= rec.getIsPulpwood();

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::updTicketPrl(CTicketPrl& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"UPDATE %s SET sSpeciesCode=:1,sGradesCode=:2,fPrice=:3,nFuncID=:4,sCalcType=:5,nPulpwood=:6 WHERE fkTicketID=:7 AND fkSpeciesID=:8 AND fkGradesID=:9",TBL_TICKET_PRL);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsString()	= rec.getSpeciesCode();
		m_saCommand.Param(2).setAsString()	= rec.getGradesCode();
		m_saCommand.Param(3).setAsDouble()	= rec.getPrice();
		m_saCommand.Param(4).setAsShort()		= rec.getFuncID();
		m_saCommand.Param(5).setAsString()	= rec.getCalcType();
		m_saCommand.Param(6).setAsShort()		= rec.getIsPulpwood();

		m_saCommand.Param(7).setAsLong()		= rec.getTicketID();
		m_saCommand.Param(8).setAsShort()		= rec.getSpeciesID();
		m_saCommand.Param(9).setAsShort()		= rec.getGradesID();

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::updTicketPrlCalcBasis(CTicketPrl& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"UPDATE %s SET sCalcType=:1 WHERE fkTicketID=:2 AND fkSpeciesID=:3 AND fkGradesID=:4",TBL_TICKET_PRL);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsString()	= rec.getCalcType();
		m_saCommand.Param(2).setAsLong()		= rec.getTicketID();
		m_saCommand.Param(3).setAsShort()		= rec.getSpeciesID();
		m_saCommand.Param(4).setAsShort()		= rec.getGradesID();

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::delTicketPrl(int ticket_id)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"DELETE FROM %s WHERE fkTicketID=:1",TBL_TICKET_PRL);
		
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsLong()	= ticket_id;

		m_saCommand.Execute();	
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}


////////////////////////////////////////////////////////////////
// LOGS
BOOL CDBHandling::getLogs(CTickets& rec,CVecLogs& vec)
{
	CString sSQL = L"";
	try
	{
		vec.clear();
		
		sSQL.Format(_T("SELECT * FROM %s WHERE fkLoadID=%d ORDER BY pkLogID"),TBL_LOGS,rec.getPKID());
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saCreated = m_saCommand.Field("dCreated").asDateTime();
			SADateTime saUpdated = m_saCommand.Field("dUpdated").asDateTime();
			vec.push_back(CLogs(m_saCommand.Field("pkLogID").asLong(),
												  m_saCommand.Field("fkLoadID").asLong(),
													(LPCTSTR)m_saCommand.Field("sTagNumber").asString(),
													(LPCTSTR)m_saCommand.Field("sSpcCode").asString(),
													(LPCTSTR)m_saCommand.Field("sSpcName").asString(),
													m_saCommand.Field("nFrequency").asShort(),
													(LPCTSTR)m_saCommand.Field("sGrade").asString(),
													m_saCommand.Field("fDeduction").asDouble(),
													m_saCommand.Field("fDOBTop").asDouble(),
													m_saCommand.Field("fDIBTop").asDouble(),
													m_saCommand.Field("fDOBRoot").asDouble(),
													m_saCommand.Field("fDIBRoot").asDouble(),
													m_saCommand.Field("fLengthMeas").asDouble(),
													m_saCommand.Field("fLengthCalc").asDouble(),
													m_saCommand.Field("fBark").asDouble(),
													m_saCommand.Field("fVolPricelist").asDouble(),
													m_saCommand.Field("fLogPrice").asDouble(),
													m_saCommand.Field("nVolFuncID").asShort(),
													(LPCTSTR)m_saCommand.Field("sVolFuncName").asString(),
													m_saCommand.Field("fCustVol1").asDouble(),
													m_saCommand.Field("fCustVol2").asDouble(),
													m_saCommand.Field("fCustVol3").asDouble(),
													m_saCommand.Field("fCustVol4").asDouble(),
													m_saCommand.Field("fCustVol5").asDouble(),
													m_saCommand.Field("fCustVol6").asDouble(),
													m_saCommand.Field("fCustVol7").asDouble(),
													m_saCommand.Field("fCustVol8").asDouble(),
													m_saCommand.Field("fCustVol9").asDouble(),
													m_saCommand.Field("fCustVol10").asDouble(),
													m_saCommand.Field("fCustVol11").asDouble(),
													m_saCommand.Field("fCustVol12").asDouble(),
													m_saCommand.Field("fCustVol13").asDouble(),
													m_saCommand.Field("fCustVol14").asDouble(),
													m_saCommand.Field("fCustVol15").asDouble(),
													m_saCommand.Field("fCustVol16").asDouble(),
													m_saCommand.Field("fCustVol17").asDouble(),
													m_saCommand.Field("fCustVol18").asDouble(),
													m_saCommand.Field("fCustVol19").asDouble(),
													m_saCommand.Field("fCustVol20").asDouble(),
													m_saCommand.Field("fUserVolume").asDouble(),
													m_saCommand.Field("fUserToppDiam").asDouble(),
													m_saCommand.Field("fUserRootDiam").asDouble(),
													m_saCommand.Field("fUserLength").asDouble(),
													m_saCommand.Field("fUserPrice").asDouble(),
													m_saCommand.Field("fDOBTop2").asDouble(),
													m_saCommand.Field("fDIBTop2").asDouble(),
													m_saCommand.Field("fDOBRoot2").asDouble(),
													m_saCommand.Field("fDIBRoot2").asDouble(),
													m_saCommand.Field("fUserToppDiam2").asDouble(),
													m_saCommand.Field("fUserRootDiam2").asDouble(),
												  (LPCTSTR)m_saCommand.Field("sReason").asString(),
												  (LPCTSTR)m_saCommand.Field("sNotes").asString(),
												  (LPCTSTR)m_saCommand.Field("sGISCoord1").asString(),
												  (LPCTSTR)m_saCommand.Field("sGISCoord2").asString(),
													m_saCommand.Field("nStatusFlag").asShort(),
												  -1,	// Flag, not included as column in logscaleLogs-table
													m_saCommand.Field("nTicketOrigin").asShort(),
													-1,
												  (LPCTSTR)convSADateTime(saCreated),
												  (LPCTSTR)convSADateTime(saUpdated)));
		}


		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::getAllLogs(CVecLogs& vec,LOGSTATUS::STATUS status)
{
	CString sSQL = L"";
	try
	{
		vec.clear();
		if (status == LOGSTATUS::NO_STATUS)
		{
			sSQL.Format(_T("SELECT a.*,b.sTicket FROM %s a,%s b WHERE a.fkLoadID=b.pkLoadID ORDER BY b.sTicket"),TBL_LOGS,TBL_TICKETS);
		}
		else
		{
			sSQL.Format(_T("SELECT a.*,b.sTicket FROM %s a,%s b WHERE a.fkLoadID=b.pkLoadID AND a.nStatusFlag=%d ORDER BY b.sTicket"),TBL_LOGS,TBL_TICKETS,status);
		}
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saCreated = m_saCommand.Field("dCreated").asDateTime();
			SADateTime saUpdated = m_saCommand.Field("dUpdated").asDateTime();
			vec.push_back(CLogs(m_saCommand.Field("pkLogID").asLong(),
												  m_saCommand.Field("fkLoadID").asLong(),
													(LPCTSTR)m_saCommand.Field("sTagNumber").asString(),
													(LPCTSTR)m_saCommand.Field("sSpcCode").asString(),
													(LPCTSTR)m_saCommand.Field("sSpcName").asString(),
													m_saCommand.Field("nFrequency").asShort(),
													(LPCTSTR)m_saCommand.Field("sGrade").asString(),
													m_saCommand.Field("fDeduction").asDouble(),
													m_saCommand.Field("fDOBTop").asDouble(),
													m_saCommand.Field("fDIBTop").asDouble(),
													m_saCommand.Field("fDOBRoot").asDouble(),
													m_saCommand.Field("fDIBRoot").asDouble(),
													m_saCommand.Field("fLengthMeas").asDouble(),
													m_saCommand.Field("fLengthCalc").asDouble(),
													m_saCommand.Field("fBark").asDouble(),
													m_saCommand.Field("fVolPricelist").asDouble(),
													m_saCommand.Field("fLogPrice").asDouble(),
													m_saCommand.Field("nVolFuncID").asShort(),
													(LPCTSTR)m_saCommand.Field("sVolFuncName").asString(),
													m_saCommand.Field("fCustVol1").asDouble(),
													m_saCommand.Field("fCustVol2").asDouble(),
													m_saCommand.Field("fCustVol3").asDouble(),
													m_saCommand.Field("fCustVol4").asDouble(),
													m_saCommand.Field("fCustVol5").asDouble(),
													m_saCommand.Field("fCustVol6").asDouble(),
													m_saCommand.Field("fCustVol7").asDouble(),
													m_saCommand.Field("fCustVol8").asDouble(),
													m_saCommand.Field("fCustVol9").asDouble(),
													m_saCommand.Field("fCustVol10").asDouble(),
													m_saCommand.Field("fCustVol11").asDouble(),
													m_saCommand.Field("fCustVol12").asDouble(),
													m_saCommand.Field("fCustVol13").asDouble(),
													m_saCommand.Field("fCustVol14").asDouble(),
													m_saCommand.Field("fCustVol15").asDouble(),
													m_saCommand.Field("fCustVol16").asDouble(),
													m_saCommand.Field("fCustVol17").asDouble(),
													m_saCommand.Field("fCustVol18").asDouble(),
													m_saCommand.Field("fCustVol19").asDouble(),
													m_saCommand.Field("fCustVol20").asDouble(),
													m_saCommand.Field("fUserVolume").asDouble(),
													m_saCommand.Field("fUserToppDiam").asDouble(),
													m_saCommand.Field("fUserRootDiam").asDouble(),
													m_saCommand.Field("fUserLength").asDouble(),
													m_saCommand.Field("fUserPrice").asDouble(),
													m_saCommand.Field("fDOBTop2").asDouble(),
													m_saCommand.Field("fDIBTop2").asDouble(),
													m_saCommand.Field("fDOBRoot2").asDouble(),
													m_saCommand.Field("fDIBRoot2").asDouble(),
													m_saCommand.Field("fUserToppDiam2").asDouble(),
													m_saCommand.Field("fUserRootDiam2").asDouble(),
												  (LPCTSTR)m_saCommand.Field("sReason").asString(),
												  (LPCTSTR)m_saCommand.Field("sNotes").asString(),
												  (LPCTSTR)m_saCommand.Field("sGISCoord1").asString(),
												  (LPCTSTR)m_saCommand.Field("sGISCoord2").asString(),
													m_saCommand.Field("nStatusFlag").asShort(),
													-1,
													m_saCommand.Field("nTicketOrigin").asShort(),
												  -1,	// Flag, not included as column in logscaleLogs-table
												  (LPCTSTR)convSADateTime(saCreated),
												  (LPCTSTR)convSADateTime(saUpdated),
													(LPCTSTR)m_saCommand.Field("sTicket").asString()));
		}

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::getLogsFromTagNumbers(LPCTSTR sql,CVecLogs& vec,LOGSTATUS::STATUS status)
{
	CString sSQL = L"";
	try
	{
		vec.clear();
		if (status == LOGSTATUS::NO_STATUS)
		{
			sSQL.Format(_T("SELECT a.*,b.sTicket FROM %s a,%s b WHERE a.fkLoadID=b.pkLoadID %s ORDER BY b.sTicket"),TBL_LOGS,TBL_TICKETS,sql);
		}
		else
		{
			sSQL.Format(_T("SELECT a.*,b.sTicket FROM %s a,%s b WHERE a.fkLoadID=b.pkLoadID AND a.nStatusFlag=%d %s ORDER BY b.sTicket"),TBL_LOGS,TBL_TICKETS,status,sql);
		}
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saCreated = m_saCommand.Field("dCreated").asDateTime();
			SADateTime saUpdated = m_saCommand.Field("dUpdated").asDateTime();
			vec.push_back(CLogs(m_saCommand.Field("pkLogID").asLong(),
												  m_saCommand.Field("fkLoadID").asLong(),
													(LPCTSTR)m_saCommand.Field("sTagNumber").asString(),
													(LPCTSTR)m_saCommand.Field("sSpcCode").asString(),
													(LPCTSTR)m_saCommand.Field("sSpcName").asString(),
													m_saCommand.Field("nFrequency").asShort(),
													(LPCTSTR)m_saCommand.Field("sGrade").asString(),
													m_saCommand.Field("fDeduction").asDouble(),
													m_saCommand.Field("fDOBTop").asDouble(),
													m_saCommand.Field("fDIBTop").asDouble(),
													m_saCommand.Field("fDOBRoot").asDouble(),
													m_saCommand.Field("fDIBRoot").asDouble(),
													m_saCommand.Field("fLengthMeas").asDouble(),
													m_saCommand.Field("fLengthCalc").asDouble(),
													m_saCommand.Field("fBark").asDouble(),
													m_saCommand.Field("fVolPricelist").asDouble(),
													m_saCommand.Field("fLogPrice").asDouble(),
													m_saCommand.Field("nVolFuncID").asShort(),
													(LPCTSTR)m_saCommand.Field("sVolFuncName").asString(),
													m_saCommand.Field("fCustVol1").asDouble(),
													m_saCommand.Field("fCustVol2").asDouble(),
													m_saCommand.Field("fCustVol3").asDouble(),
													m_saCommand.Field("fCustVol4").asDouble(),
													m_saCommand.Field("fCustVol5").asDouble(),
													m_saCommand.Field("fCustVol6").asDouble(),
													m_saCommand.Field("fCustVol7").asDouble(),
													m_saCommand.Field("fCustVol8").asDouble(),
													m_saCommand.Field("fCustVol9").asDouble(),
													m_saCommand.Field("fCustVol10").asDouble(),
													m_saCommand.Field("fCustVol11").asDouble(),
													m_saCommand.Field("fCustVol12").asDouble(),
													m_saCommand.Field("fCustVol13").asDouble(),
													m_saCommand.Field("fCustVol14").asDouble(),
													m_saCommand.Field("fCustVol15").asDouble(),
													m_saCommand.Field("fCustVol16").asDouble(),
													m_saCommand.Field("fCustVol17").asDouble(),
													m_saCommand.Field("fCustVol18").asDouble(),
													m_saCommand.Field("fCustVol19").asDouble(),
													m_saCommand.Field("fCustVol20").asDouble(),
													m_saCommand.Field("fUserVolume").asDouble(),
													m_saCommand.Field("fUserToppDiam").asDouble(),
													m_saCommand.Field("fUserRootDiam").asDouble(),
													m_saCommand.Field("fUserLength").asDouble(),
													m_saCommand.Field("fUserPrice").asDouble(),
													m_saCommand.Field("fDOBTop2").asDouble(),
													m_saCommand.Field("fDIBTop2").asDouble(),
													m_saCommand.Field("fDOBRoot2").asDouble(),
													m_saCommand.Field("fDIBRoot2").asDouble(),
													m_saCommand.Field("fUserToppDiam2").asDouble(),
													m_saCommand.Field("fUserRootDiam2").asDouble(),
												  (LPCTSTR)m_saCommand.Field("sReason").asString(),
												  (LPCTSTR)m_saCommand.Field("sNotes").asString(),
												  (LPCTSTR)m_saCommand.Field("sGISCoord1").asString(),
												  (LPCTSTR)m_saCommand.Field("sGISCoord2").asString(),
													m_saCommand.Field("nStatusFlag").asShort(),
													-1,
													m_saCommand.Field("nTicketOrigin").asShort(),
												  -1,	// Flag, not included as column in logscaleLogs-table
												  (LPCTSTR)convSADateTime(saCreated),
												  (LPCTSTR)convSADateTime(saUpdated),
													(LPCTSTR)m_saCommand.Field("sTicket").asString()));
		}

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}


BOOL CDBHandling::newLog(CLogs& rec)
{
	CString sSQL = L"";
	try
	{
		// Check if lopgtag is unique, if not set tagnumber = ""
		if (!isLogTagUnique(rec))
			rec.setTagNumber(L"");

		sSQL.Format(L"INSERT INTO %s (fkLoadID,sTagNumber,sSpcCode,sSpcName,nFrequency,sGrade,fDeduction,fDOBTop,fDIBTop,fDOBRoot,fDIBRoot,fLengthMeas,fLengthCalc,fBark,fVolPricelist,fLogPrice,nVolFuncID,sVolFuncName,"								
								L"fCustVol1,fCustVol2,fCustVol3,fCustVol4,fCustVol5,fCustVol6,fCustVol7,fCustVol8,fCustVol9,fCustVol10,"
								L"fCustVol11,fCustVol12,fCustVol13,fCustVol14,fCustVol15,fCustVol16,fCustVol17,fCustVol18,fCustVol19,fCustVol20,"								
								L"fUserVolume,fUserToppDiam,fUserRootDiam,fUserLength,fUserPrice,fDOBTop2,fDIBTop2,fDOBRoot2,fDIBRoot2,fUserToppDiam2,fUserRootDiam2,"							
								L"nStatusFlag,sReason,sNotes,sGISCoord1,sGISCoord2,nTicketOrigin) "
								L"VALUES (:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19,:20,:21,:22,:23,:24,:25,:26,:27,:28,:29,:30,:31,:32,:33,:34,:35,:36,:37,:38,:39,:40,"
								L":41,:42,:43,:44,:45,:46,:47,:48,:49,:50,:51,:52,:53,:54,:55)",TBL_LOGS);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsLong()		= rec.getLoadID();
		m_saCommand.Param(2).setAsString()	= rec.getTagNumber();
		m_saCommand.Param(3).setAsString()	= rec.getSpcCode();
		m_saCommand.Param(4).setAsString()	= rec.getSpcName();
		m_saCommand.Param(5).setAsShort()		= rec.getFrequency();
		m_saCommand.Param(6).setAsString()	= rec.getGrade();
		m_saCommand.Param(7).setAsDouble()	= rec.getDeduction();
		m_saCommand.Param(8).setAsDouble()	= rec.getDOBTop();
		m_saCommand.Param(9).setAsDouble()	= rec.getDIBTop();
		m_saCommand.Param(10).setAsDouble()	= rec.getDOBRoot();
		m_saCommand.Param(11).setAsDouble()	= rec.getDIBRoot();
		m_saCommand.Param(12).setAsDouble()	= rec.getLengthMeas();
		m_saCommand.Param(13).setAsDouble()	= rec.getLengthCalc();
		m_saCommand.Param(14).setAsDouble()	= rec.getBark();
		m_saCommand.Param(15).setAsDouble()	= rec.getVolPricelist();
		m_saCommand.Param(16).setAsDouble()	= rec.getLogPrice();
		m_saCommand.Param(17).setAsShort()		= rec.getVolFuncID();
		m_saCommand.Param(18).setAsString()	= rec.getVolFuncName();

		m_saCommand.Param(19).setAsDouble()	= rec.getVolCust1();
		m_saCommand.Param(20).setAsDouble()	= rec.getVolCust2();
		m_saCommand.Param(21).setAsDouble()	= rec.getVolCust3();
		m_saCommand.Param(22).setAsDouble()	= rec.getVolCust4();
		m_saCommand.Param(23).setAsDouble()	= rec.getVolCust5();
		m_saCommand.Param(24).setAsDouble()	= rec.getVolCust6();
		m_saCommand.Param(25).setAsDouble()	= rec.getVolCust7();
		m_saCommand.Param(26).setAsDouble()	= rec.getVolCust8();
		m_saCommand.Param(27).setAsDouble()	= rec.getVolCust9();
		m_saCommand.Param(28).setAsDouble()	= rec.getVolCust10();
		m_saCommand.Param(29).setAsDouble()	= rec.getVolCust11();
		m_saCommand.Param(30).setAsDouble()	= rec.getVolCust12();
		m_saCommand.Param(31).setAsDouble()	= rec.getVolCust13();
		m_saCommand.Param(32).setAsDouble()	= rec.getVolCust14();
		m_saCommand.Param(33).setAsDouble()	= rec.getVolCust15();
		m_saCommand.Param(34).setAsDouble()	= rec.getVolCust16();
		m_saCommand.Param(35).setAsDouble()	= rec.getVolCust17();
		m_saCommand.Param(36).setAsDouble()	= rec.getVolCust18();
		m_saCommand.Param(37).setAsDouble()	= rec.getVolCust19();
		m_saCommand.Param(38).setAsDouble()	= rec.getVolCust20();


		m_saCommand.Param(39).setAsDouble()	= rec.getUVol();
		m_saCommand.Param(40).setAsDouble()	= rec.getUTopDia();
		m_saCommand.Param(41).setAsDouble()	= rec.getURootDia();
		m_saCommand.Param(42).setAsDouble()	= rec.getULength();
		m_saCommand.Param(43).setAsDouble()	= rec.getUPrice();

		m_saCommand.Param(44).setAsDouble()	= rec.getDOBTop2();
		m_saCommand.Param(45).setAsDouble()	= rec.getDIBTop2();
		m_saCommand.Param(46).setAsDouble()	= rec.getDOBRoot2();
		m_saCommand.Param(47).setAsDouble()	= rec.getDIBRoot2();
		m_saCommand.Param(48).setAsDouble()	= rec.getUTopDia2();
		m_saCommand.Param(49).setAsDouble()	= rec.getURootDia2();

		m_saCommand.Param(50).setAsShort()	= rec.getStatusFlag();
		
		m_saCommand.Param(51).setAsString()	= rec.getReason();
		m_saCommand.Param(52).setAsString()	= rec.getNotes();
		m_saCommand.Param(53).setAsString()	= rec.getGISCoord1();
		m_saCommand.Param(54).setAsString()	= rec.getGISCoord2();
		m_saCommand.Param(55).setAsShort()	= rec.getTicketOrigin();

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::updLog(CLogs& rec)
{
	CString sSQL = L"";
	try
	{

		sSQL.Format(L"UPDATE %s SET sTagNumber=:1,sSpcCode=:2,sSpcName=:3,nFrequency=:4,sGrade=:5,fDeduction=:6,fDOBTop=:7,fDIBTop=:8,fDOBRoot=:9,fDIBRoot=:10,fLengthMeas=:11,fLengthCalc=:12,"
			L"fBark=:13,fVolPricelist=:14,fLogPrice=:15,nVolFuncID=:16,sVolFuncName=:17,"
			L"fCustVol1=:18,fCustVol2=:19,fCustVol3=:20,fCustVol4=:21,fCustVol5=:22,fCustVol6=:23,fCustVol7=:24,fCustVol8=:25,fCustVol9=:26,fCustVol10=:27,"
			L"fCustVol11=:28,fCustVol12=:29,fCustVol13=:30,fCustVol14=:31,fCustVol15=:32,fCustVol16=:33,fCustVol17=:34,fCustVol18=:35,fCustVol19=:36,fCustVol20=:37,"
			L"fUserVolume=:38,fUserToppDiam=:39,fUserRootDiam=:40,fUserLength=:41,fUserPrice=:42,fDOBTop2=:43,fDIBTop2=:44,fDOBRoot2=:45,fDIBRoot2=:46,fUserToppDiam2=:47,fUserRootDiam2=:48,"
			L"nStatusFlag=:49,sReason=:50,sNotes=:51,sGISCoord1=:52,sGISCoord2=:53,nTicketOrigin=:54,dUpdated=GETDATE() WHERE pkLogID=:55 AND fkLoadID=:56 AND nStatusFlag=%d",
			TBL_LOGS,LOGSTATUS::IN_STOCK);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsString()	= rec.getTagNumber();
		m_saCommand.Param(2).setAsString()	= rec.getSpcCode();
		m_saCommand.Param(3).setAsString()	= rec.getSpcName();
		m_saCommand.Param(4).setAsShort()		= rec.getFrequency();
		m_saCommand.Param(5).setAsString()	= rec.getGrade();
		m_saCommand.Param(6).setAsDouble()	= rec.getDeduction();
		m_saCommand.Param(7).setAsDouble()	= rec.getDOBTop();
		m_saCommand.Param(8).setAsDouble()	= rec.getDIBTop();
		m_saCommand.Param(9).setAsDouble()	= rec.getDOBRoot();
		m_saCommand.Param(10).setAsDouble()	= rec.getDIBRoot();
		m_saCommand.Param(11).setAsDouble()	= rec.getLengthMeas();
		m_saCommand.Param(12).setAsDouble()	= rec.getLengthCalc();
		m_saCommand.Param(13).setAsDouble()	= rec.getBark();
		m_saCommand.Param(14).setAsDouble()	= rec.getVolPricelist();
		m_saCommand.Param(15).setAsDouble()	= rec.getLogPrice();
		m_saCommand.Param(16).setAsShort()		= rec.getVolFuncID();
		m_saCommand.Param(17).setAsString()	= rec.getVolFuncName();

		m_saCommand.Param(18).setAsDouble()	= rec.getVolCust1();
		m_saCommand.Param(19).setAsDouble()	= rec.getVolCust2();
		m_saCommand.Param(20).setAsDouble()	= rec.getVolCust3();
		m_saCommand.Param(21).setAsDouble()	= rec.getVolCust4();
		m_saCommand.Param(22).setAsDouble()	= rec.getVolCust5();
		m_saCommand.Param(23).setAsDouble()	= rec.getVolCust6();
		m_saCommand.Param(24).setAsDouble()	= rec.getVolCust7();
		m_saCommand.Param(25).setAsDouble()	= rec.getVolCust8();
		m_saCommand.Param(26).setAsDouble()	= rec.getVolCust9();
		m_saCommand.Param(27).setAsDouble()	= rec.getVolCust10();
		m_saCommand.Param(28).setAsDouble()	= rec.getVolCust11();
		m_saCommand.Param(29).setAsDouble()	= rec.getVolCust12();
		m_saCommand.Param(30).setAsDouble()	= rec.getVolCust13();
		m_saCommand.Param(31).setAsDouble()	= rec.getVolCust14();
		m_saCommand.Param(32).setAsDouble()	= rec.getVolCust15();
		m_saCommand.Param(33).setAsDouble()	= rec.getVolCust16();
		m_saCommand.Param(34).setAsDouble()	= rec.getVolCust17();
		m_saCommand.Param(35).setAsDouble()	= rec.getVolCust18();
		m_saCommand.Param(36).setAsDouble()	= rec.getVolCust19();
		m_saCommand.Param(37).setAsDouble()	= rec.getVolCust20();

		m_saCommand.Param(38).setAsDouble()	= rec.getUVol();
		m_saCommand.Param(39).setAsDouble()	= rec.getUTopDia();
		m_saCommand.Param(40).setAsDouble()	= rec.getURootDia();
		m_saCommand.Param(41).setAsDouble()	= rec.getULength();
		m_saCommand.Param(42).setAsDouble()	= rec.getUPrice();

		m_saCommand.Param(43).setAsDouble()	= rec.getDOBTop2();
		m_saCommand.Param(44).setAsDouble()	= rec.getDIBTop2();
		m_saCommand.Param(45).setAsDouble()	= rec.getDOBRoot2();
		m_saCommand.Param(46).setAsDouble()	= rec.getDIBRoot2();
		m_saCommand.Param(47).setAsDouble()	= rec.getUTopDia2();
		m_saCommand.Param(48).setAsDouble()	= rec.getURootDia2();

		m_saCommand.Param(49).setAsShort()	= rec.getStatusFlag();
		
		m_saCommand.Param(50).setAsString()	= rec.getReason();
		m_saCommand.Param(51).setAsString()	= rec.getNotes();

		m_saCommand.Param(52).setAsString()	= rec.getGISCoord1();
		m_saCommand.Param(53).setAsString()	= rec.getGISCoord2();

		m_saCommand.Param(54).setAsShort()	= rec.getTicketOrigin();

		m_saCommand.Param(55).setAsLong()	= rec.getLogID();
		m_saCommand.Param(56).setAsLong()	= rec.getLoadID();

		m_saCommand.Execute();	
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::delLog(CLogs& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"DELETE FROM %s WHERE pkLogID=:1 AND fkLoadID=:2",TBL_LOGS);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsLong()	= rec.getLogID();
		m_saCommand.Param(2).setAsLong()	= rec.getLoadID();

		m_saCommand.Execute();	
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}


BOOL CDBHandling::avgLogData(CLogs& rec)
{
	CString sSQL = L"",S;
	try
	{

		sSQL.Format(L"SELECT AVG(fDOBTop) AS 'DOBT',"
								L"AVG(fDIBTop) AS 'DIBT',"
								L"AVG(fDOBRoot) AS 'DOBR',"
								L"AVG(fDIBRoot) AS 'DIBR',"
								L"AVG(fBark) AS 'BARK',"
								L"AVG(fLengthMeas) AS 'LENGTHMEAS',"
								L"AVG(fLengthCalc) AS 'LENGTHCALC',"
								L"AVG(fDOBTop2) AS 'DOBT2',"
								L"AVG(fDIBTop2) AS 'DIBT2',"
								L"AVG(fDOBRoot2) AS 'DOBR2',"
								L"AVG(fDIBRoot2) AS 'DIBR2',"
								L"SUM(fVolPricelist) AS 'VOLPRICELIST',"
								L"SUM(fCustVol1) AS 'CUST1',"
								L"SUM(fCustVol2) AS 'CUST2',"
								L"SUM(fCustVol3) AS 'CUST3',"
								L"SUM(fCustVol4) AS 'CUST4',"
								L"SUM(fCustVol5) AS 'CUST5',"
								L"SUM(fCustVol6) AS 'CUST6',"
								L"SUM(fCustVol7) AS 'CUST7',"
								L"SUM(fCustVol8) AS 'CUST8',"
								L"SUM(fCustVol9) AS 'CUST9',"
								L"SUM(fCustVol10) AS 'CUST10',"
								L"SUM(fCustVol11) AS 'CUST11',"
								L"SUM(fCustVol12) AS 'CUST12',"
								L"SUM(fCustVol13) AS 'CUST13',"
								L"SUM(fCustVol14) AS 'CUST14',"
								L"SUM(fCustVol15) AS 'CUST15',"
								L"SUM(fCustVol16) AS 'CUST16',"
								L"SUM(fCustVol17) AS 'CUST17',"
								L"SUM(fCustVol18) AS 'CUST18',"
								L"SUM(fCustVol19) AS 'CUST19',"
								L"SUM(fCustVol20) AS 'CUST20',"
								L"SUM(fLogPrice) AS 'LOGPRICE', "
								L"AVG(fUserToppDiam) AS 'UDIAT',"
								L"AVG(fUserToppDiam2) AS 'UDIAT2',"
								L"AVG(fUserVolume) AS 'UVOL',"
								L"AVG(fUserRootDiam) AS 'UDIAR',"
								L"AVG(fUserRootDiam2) AS 'UDIAR2',"
								L"AVG(fUserLength) AS 'ULEN',"
								L"SUM(fUserPrice) AS 'UPRICE'"
								L"FROM %s WHERE fkLoadID=%d",TBL_LOGS,rec.getLoadID());
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			rec.setDOBTop(m_saCommand.Field("DOBT").asDouble());
			rec.setDIBTop(m_saCommand.Field("DIBT").asDouble());
			rec.setDOBRoot(m_saCommand.Field("DOBR").asDouble());
			rec.setDIBRoot(m_saCommand.Field("DIBR").asDouble());
			rec.setBark(m_saCommand.Field("BARK").asDouble());
			rec.setLengthMeas(m_saCommand.Field("LENGTHMEAS").asDouble());
			rec.setLengthCalc(m_saCommand.Field("LENGTHCALC").asDouble());
			
			rec.setDOBTop2(m_saCommand.Field("DOBT2").asDouble());
			rec.setDIBTop2(m_saCommand.Field("DIBT2").asDouble());
			rec.setDOBRoot2(m_saCommand.Field("DOBR2").asDouble());
			rec.setDIBRoot2(m_saCommand.Field("DIBR2").asDouble());
			
			rec.setVolPricelist(m_saCommand.Field("VOLPRICELIST").asDouble());
			rec.setLogPrice(m_saCommand.Field("LOGPRICE").asDouble());
			rec.setVolCust1(m_saCommand.Field("CUST1").asDouble());
			rec.setVolCust2(m_saCommand.Field("CUST2").asDouble());
			rec.setVolCust3(m_saCommand.Field("CUST3").asDouble());
			rec.setVolCust4(m_saCommand.Field("CUST4").asDouble());
			rec.setVolCust5(m_saCommand.Field("CUST5").asDouble());
			rec.setVolCust6(m_saCommand.Field("CUST6").asDouble());
			rec.setVolCust7(m_saCommand.Field("CUST7").asDouble());
			rec.setVolCust8(m_saCommand.Field("CUST8").asDouble());
			rec.setVolCust9(m_saCommand.Field("CUST9").asDouble());
			rec.setVolCust10(m_saCommand.Field("CUST10").asDouble());
			rec.setVolCust11(m_saCommand.Field("CUST11").asDouble());
			rec.setVolCust12(m_saCommand.Field("CUST12").asDouble());
			rec.setVolCust13(m_saCommand.Field("CUST13").asDouble());
			rec.setVolCust14(m_saCommand.Field("CUST14").asDouble());
			rec.setVolCust15(m_saCommand.Field("CUST15").asDouble());
			rec.setVolCust16(m_saCommand.Field("CUST16").asDouble());
			rec.setVolCust17(m_saCommand.Field("CUST17").asDouble());
			rec.setVolCust18(m_saCommand.Field("CUST18").asDouble());
			rec.setVolCust19(m_saCommand.Field("CUST19").asDouble());
			rec.setVolCust20(m_saCommand.Field("CUST20").asDouble());
			
			rec.setUTopDia(m_saCommand.Field("UDIAT").asDouble());
			rec.setUTopDia2(m_saCommand.Field("UDIAT2").asDouble());
			rec.setUVol(m_saCommand.Field("UVOL").asDouble());
			rec.setURootDia(m_saCommand.Field("UDIAR").asDouble());
			rec.setURootDia2(m_saCommand.Field("UDIAR2").asDouble());
			rec.setULength(m_saCommand.Field("ULEN").asDouble());
			rec.setUPrice(m_saCommand.Field("UPRICE").asDouble());
	}


		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::resetLogsIdentityField(void)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("DBCC CHECKIDENT('%s', RESEED, 0)"),TBL_LOGS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}
	return TRUE;
}

// Check if thre's more than one occurence of a tagnumber in the
// inventory database
BOOL CDBHandling::checkLogTagNumbers(LPCTSTR sql,vecDuplicateLogs &vec_duplicates)
{
	CString sSQL(sql);
	try
	{
		vec_duplicates.clear();
		
		sSQL.Format(L"select logscaleTicketsNew.sTicket,logscaleLogsNew.sTagNumber,count(logscaleLogsNew.sTagNumber) as 'antal',logscaleLogsNew.fkLoadID,logscaleLogsNew.pkLogID,logscaleLogsNew.nStatusFlag "
								L"from logscaleLogsNew "
								L"inner join logscaleTicketsNew on "
								L"logscaleLogsNew.fkLoadID=logscaleTicketsNew.pkLoadID "
								L"where %s "
								L"group by logscaleTicketsNew.sTicket,logscaleLogsNew.sTagNumber,logscaleLogsNew.fkLoadID,logscaleLogsNew.pkLogID,logscaleLogsNew.nStatusFlag",sql);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			if (m_saCommand.Field("antal").asShort() > 0)
			{
				vec_duplicates.push_back(CDuplicateLogs(TRUE,
									m_saCommand.Field("fkLoadID").asLong(),
									m_saCommand.Field("pkLogID").asLong(),
									(LPCTSTR)m_saCommand.Field("sTicket").asString(),
									(LPCTSTR)m_saCommand.Field("sTagNumber").asString(),
									m_saCommand.Field("nStatusFlag").asShort()));
			}
		}


		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::setLogStatus(CLogs& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"UPDATE %s SET nStatusFlag=:1,dUpdated=GETDATE() WHERE pkLogID=:2 AND fkLoadID=:3",TBL_LOGS);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsShort()	= rec.getStatusFlag();

		m_saCommand.Param(2).setAsLong()	= rec.getLogID();
		m_saCommand.Param(3).setAsLong()	= rec.getLoadID();

		m_saCommand.Execute();	
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::setLogStatus(CString sql)
{
	CString sSQL(sql);
	try
	{
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Execute();	
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::cleanLogsTable(int ticket_id)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"DELETE FROM %s WHERE fkLoadID=:1 AND sTagNumber=''",TBL_LOGS);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsLong()	= ticket_id;

		m_saCommand.Execute();	
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;

}

BOOL CDBHandling::isLogTagUnique(CLogs& rec)
{
	short nCnt = 0;
	CString sSQL  = L"";
	try
	{
		
		sSQL.Format(L"select 1 from %s where sTagNumber='%s'",TBL_LOGS,rec.getTagNumber());

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			nCnt++;
			break;
		}

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return (nCnt == 0);
}

BOOL CDBHandling::isLogTagUnique2(CLogs& rec)
{
	short nCnt = 0;
	CString sSQL  = L"";
	try
	{
		
		sSQL.Format(L"select 1 from %s where sTagNumber='%s' and pkLogID!=%d and fkLoadID!=%d",
			TBL_LOGS,rec.getTagNumber(),rec.getLogID(),rec.getLoadID());

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			nCnt++;
			break;
		}

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return (nCnt == 0);
}

BOOL CDBHandling::isLogTagUnique3(LPCTSTR tag_num)
{
	short nCnt = 0;
	CString sSQL  = L"";
	try
	{
		
		sSQL.Format(L"select 1 from %s where sTagNumber='%s'",
			TBL_LOGS,tag_num);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			nCnt++;
			break;
		}

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return (nCnt == 0);
}


///////////////////////////////////////////////////////////////////////////////////////////////
// Sum logdata for ticket and update ticket
BOOL CDBHandling::updSumLogData(int load_id)
{
	CSumLogData rec = CSumLogData();
	CString sSQL;
	try
	{
		sSQL.Format(L"select COUNT(pkLogID) as numof,"
								L"SUM(fVolPricelist) as volpricelist,"
								L"SUM(fLogPrice) as logprice,"
								L"SUM(fUserVolume) as uvol,"
								L"SUM(fUserToppDiam) as utopdia,"
								L"SUM(fUserRootDiam) as urootdia,"
								L"SUM(fUserLength) as ulength,"
								L"SUM(fUserPrice) as uprice "
								L"from %s where fkLoadID=%d",TBL_LOGS,load_id);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		while(m_saCommand.FetchNext())
		{
			rec = CSumLogData(load_id,
												m_saCommand.Field("numof").asShort(),
												m_saCommand.Field("volpricelist").asDouble(),
												m_saCommand.Field("logprice").asDouble(),
												m_saCommand.Field("uvol").asDouble(),
												m_saCommand.Field("utopdia").asDouble(),
												m_saCommand.Field("urootdia").asDouble(),
												m_saCommand.Field("ulength").asDouble(),
												m_saCommand.Field("uprice").asDouble() );
		}
		doCommit();
		m_saCommand.Close();

		sSQL.Format(L"UPDATE %s SET nNumOfLogs=:1,fSumVolPricelist=:2,fSumPrice=:3,dUpdated=GETDATE() WHERE pkLoadID=:4",TBL_TICKETS);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsShort()		= rec.getNumOfLogs();
		m_saCommand.Param(2).setAsDouble()	= rec.getVolPricelist();
		m_saCommand.Param(3).setAsDouble()	= rec.getLogPrice();
		
		m_saCommand.Param(4).setAsLong()	= rec.getPKID();

		m_saCommand.Execute();

	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}
	return TRUE;
}


////////////////////////////////////////////////////////////////
// SPECIES
BOOL CDBHandling::getSpecies(CVecSpecies& vec,CXTPReportControl &report)
{
	CString sSQL = L"";
	CSpecies rec = CSpecies();
	BOOL bReturn = TRUE;
	try
	{
		vec.clear();
		if (report.GetSafeHwnd())
		{
			report.ResetContent();	// Clear

			sSQL.Format(_T("SELECT * FROM %s ORDER BY pkSpeciesID"),TBL_SPECIES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();
			
			while(m_saCommand.FetchNext())
			{
				SADateTime saCreated = m_saCommand.Field("dCreated").asDateTime();
				SADateTime saUpdated = m_saCommand.Field("dUpdated").asDateTime();
				rec = CSpecies(m_saCommand.Field("pkSpeciesID").asShort(),
											 (LPCTSTR)m_saCommand.Field("sSpeciesCode").asString(),
											 (LPCTSTR)m_saCommand.Field("sSpeciesName").asString(),
											  m_saCommand.Field("fBarkReductionInch").asDouble(),
											  m_saCommand.Field("fBarkReductionMM").asDouble(),
											 (LPCTSTR)convSADateTime(saCreated),
											 (LPCTSTR)convSADateTime(saUpdated));
				vec.push_back(rec);

				CXTPReportRecord *pRec = report.AddRecord(new CSpeciesReportRec(rec,true));
			}

			doCommit();
			report.Populate();
			report.UpdateWindow();
		}
		else
			bReturn = FALSE;

	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return bReturn;
}

BOOL CDBHandling::getSpecies(CVecSpecies& vec)
{
	CString sSQL = L"",S;
	CSpecies rec = CSpecies();
	BOOL bReturn = TRUE;
	try
	{
		vec.clear();

		sSQL.Format(_T("SELECT * FROM %s ORDER BY pkSpeciesID"),TBL_SPECIES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saCreated = m_saCommand.Field("dCreated").asDateTime();
			SADateTime saUpdated = m_saCommand.Field("dUpdated").asDateTime();
			rec = CSpecies(m_saCommand.Field("pkSpeciesID").asShort(),
										 (LPCTSTR)m_saCommand.Field("sSpeciesCode").asString(),
										 (LPCTSTR)m_saCommand.Field("sSpeciesName").asString(),
										  m_saCommand.Field("fBarkReductionInch").asDouble(),
										  m_saCommand.Field("fBarkReductionMM").asDouble(),
										 (LPCTSTR)convSADateTime(saCreated),
										 (LPCTSTR)convSADateTime(saUpdated));
			vec.push_back(rec);
			
		}
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return bReturn;
}

BOOL CDBHandling::newSpecies(CSpecies& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"INSERT INTO %s (sSpeciesCode,sSpeciesName,fBarkReductionInch,fBarkReductionMM) VALUES (:1,:2,:3,:4)",TBL_SPECIES);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsString()	= rec.getSpcCode();
		m_saCommand.Param(2).setAsString()	= rec.getSpcName();
		m_saCommand.Param(3).setAsDouble()	= rec.getBarkReductionInch();
		m_saCommand.Param(4).setAsDouble()	= rec.getBarkReductionMM();

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::updSpecies(CSpecies& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"UPDATE %s SET sSpeciesCode=:1,sSpeciesName=:2,fBarkReductionInch=:3,fBarkReductionMM=:4,dUpdated=GETDATE() WHERE pkSpeciesID=:5 ",TBL_SPECIES);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsString()	= rec.getSpcCode();
		m_saCommand.Param(2).setAsString()	= rec.getSpcName();
		m_saCommand.Param(3).setAsDouble()	= rec.getBarkReductionInch();
		m_saCommand.Param(4).setAsDouble()	= rec.getBarkReductionMM();
		m_saCommand.Param(5).setAsShort()		= rec.getSpcID();

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::delSpecies(CSpecies& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"DELETE FROM %s WHERE pkSpeciesID=:1",TBL_SPECIES);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsShort()	= rec.getSpcID();

		m_saCommand.Execute();	
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::resetSpeciesIdentityField(void)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("DBCC CHECKIDENT('%s', RESEED, 0)"),TBL_SPECIES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}
	return TRUE;
}

BOOL CDBHandling::isSpeciesUnique()
{
	CString sSQL = L"",S;
	BOOL bReturn = TRUE;
	try
	{
			sSQL.Format(_T("select sSpeciesCode,count(sSpeciesCode) as 'antal' from %s group by sSpeciesCode"),TBL_SPECIES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();
			
			while(m_saCommand.FetchNext())
			{
				S.Format(L"ANTAL %d",m_saCommand.Field("antal").asShort());
				if (m_saCommand.Field("antal").asShort() > 1)
					bReturn = FALSE;
			}

			doCommit();

	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return bReturn;
}

BOOL CDBHandling::isSpeciesCodeInDB(LPCTSTR spc_code)
{
	CString sSQL = L"";
	BOOL bReturn = FALSE;
	try
	{
			sSQL.Format(_T("select 1 from %s where sSpeciesCode='%s'"),TBL_SPECIES,spc_code);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();
			
			while(m_saCommand.FetchNext())
			{
				bReturn = TRUE;
			}

			doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return bReturn;
}

int CDBHandling::getLastSpeciesID(void)
{
	return last_identity(TBL_SPECIES,_T("pkSpeciesID"));
}

////////////////////////////////////////////////////////////////
// GRADES
BOOL CDBHandling::getGrades(CVecGrades& vec,CXTPReportControl &report)
{
	CString sSQL = L"";
	CGrades rec = CGrades();
	BOOL bReturn = TRUE;
	try
	{
		vec.clear();
		if (report.GetSafeHwnd())
		{
			report.ResetContent();	// Clear

			sSQL.Format(_T("SELECT * FROM %s ORDER BY pkGradeID"),TBL_GRADES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();
			
			while(m_saCommand.FetchNext())
			{
				SADateTime saCreated = m_saCommand.Field("dCreated").asDateTime();
				SADateTime saUpdated = m_saCommand.Field("dUpdated").asDateTime();
				rec = CGrades(m_saCommand.Field("pkGradeID").asShort(),
											 (LPCTSTR)m_saCommand.Field("sGradeCode").asString(),
											 (LPCTSTR)m_saCommand.Field("sGradeName").asString(),
											 m_saCommand.Field("nPulpWood").asShort(),
											 (LPCTSTR)convSADateTime(saCreated),
											 (LPCTSTR)convSADateTime(saUpdated));
				vec.push_back(rec);

				report.AddRecord(new CGradesReportRec(rec,true));
			}

			doCommit();
			report.Populate();
			report.UpdateWindow();
		}
		else
			bReturn = FALSE;

	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return bReturn;
}

BOOL CDBHandling::getGrades(CVecGrades& vec)
{
	CString sSQL = L"";
	CGrades rec = CGrades();
	BOOL bReturn = TRUE;
	try
	{
		vec.clear();
		sSQL.Format(_T("SELECT * FROM %s ORDER BY pkGradeID"),TBL_GRADES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saCreated = m_saCommand.Field("dCreated").asDateTime();
			SADateTime saUpdated = m_saCommand.Field("dUpdated").asDateTime();
			rec = CGrades(m_saCommand.Field("pkGradeID").asShort(),
										 (LPCTSTR)m_saCommand.Field("sGradeCode").asString(),
										 (LPCTSTR)m_saCommand.Field("sGradeName").asString(),
										 m_saCommand.Field("nPulpWood").asShort(),
										 (LPCTSTR)convSADateTime(saCreated),
										 (LPCTSTR)convSADateTime(saUpdated));
			vec.push_back(rec);

			doCommit();
		}
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return bReturn;
}

BOOL CDBHandling::newGrades(CGrades& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"INSERT INTO %s (sGradeCode,sGradeName,nPulpWood) VALUES (:1,:2,:3)",TBL_GRADES);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsString()	= rec.getGradesCode();
		m_saCommand.Param(2).setAsString()	= rec.getGradesName();
		m_saCommand.Param(3).setAsShort()		= rec.getIsPulpwood();

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::updGrades(CGrades& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"UPDATE %s SET sGradeCode=:1,sGradeName=:2,nPulpWood=:3,dUpdated=GETDATE() WHERE pkGradeID=:4",TBL_GRADES);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsString()	= rec.getGradesCode();
		m_saCommand.Param(2).setAsString()	= rec.getGradesName();
		m_saCommand.Param(3).setAsShort()		= rec.getIsPulpwood();
		m_saCommand.Param(4).setAsShort()		= rec.getGradesID();

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::delGrades(CGrades& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"DELETE FROM %s WHERE pkGradeID=:1",TBL_GRADES);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsShort()	= rec.getGradesID();

		m_saCommand.Execute();	
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::resetGradesIdentityField(void)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("DBCC CHECKIDENT('%s', RESEED, 0)"),TBL_GRADES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}
	return TRUE;
}

BOOL CDBHandling::isGradesUnique()
{
	CString sSQL = L"",S;
	BOOL bReturn = TRUE;
	try
	{
			sSQL.Format(_T("select sGradeCode,count(sGradeCode) as 'antal' from %s group by sGradeCode"),TBL_GRADES);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();
			
			while(m_saCommand.FetchNext())
			{
				S.Format(L"ANTAL %d",m_saCommand.Field("antal").asShort());
				if (m_saCommand.Field("antal").asShort() > 1)
					bReturn = FALSE;
			}

			doCommit();

	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return bReturn;
}

BOOL CDBHandling::isGradesCodeInDB(LPCTSTR spc_code)
{
	CString sSQL = L"";
	BOOL bReturn = FALSE;
	try
	{
			sSQL.Format(_T("select 1 from %s where sGradeCode='%s'"),TBL_GRADES,spc_code);
			m_saCommand.setCommandText((SAString)sSQL);
			m_saCommand.Execute();
			
			while(m_saCommand.FetchNext())
			{
				bReturn = TRUE;
			}

			doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return bReturn;
}

int CDBHandling::getLastGradesID(void)
{
	return last_identity(TBL_GRADES,_T("pkGradeID"));
}


////////////////////////////////////////////////////////////////
// PRICELIST
BOOL CDBHandling::getPricelist(CVecPricelist& vec,int prl_id)
{
	CString sSQL = L"";
	CPricelist rec = CPricelist();
	BOOL bReturn = TRUE;
	try
	{
		vec.clear();

		if (prl_id == -1)
			sSQL.Format(_T("SELECT * FROM %s ORDER BY pkPrlID"),TBL_PRICELIST);
		else if (prl_id > 0)
			sSQL.Format(_T("SELECT * FROM %s WHERE pkPrlID=%d ORDER BY pkPrlID"),TBL_PRICELIST,prl_id);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
	
		while(m_saCommand.FetchNext())
		{
			SADateTime saCreated = m_saCommand.Field("dCreated").asDateTime();
			SADateTime saUpdated = m_saCommand.Field("dUpdated").asDateTime();
			rec = CPricelist(m_saCommand.Field("pkPrlID").asShort(),
											 m_saCommand.Field("sName").asString(),
											 m_saCommand.Field("sCreatedBy").asString(),
											 m_saCommand.Field("sDate").asString(),
											 m_saCommand.Field("sPricelist").asLongChar(),
											 m_saCommand.Field("nLocked").asShort(),
											 m_saCommand.Field("sNotes").asLongChar(),
											(LPCTSTR)convSADateTime(saCreated),
											(LPCTSTR)convSADateTime(saUpdated));
			vec.push_back(rec);

		}

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return bReturn;
}

BOOL CDBHandling::getPricelist(CPricelist& rec,int prl_id)
{
	CString sSQL = L"";
	BOOL bReturn = TRUE;
	try
	{
		if (prl_id == -1)
			sSQL.Format(_T("SELECT * FROM %s ORDER BY pkPrlID"),TBL_PRICELIST);
		else if (prl_id > 0)
			sSQL.Format(_T("SELECT * FROM %s WHERE pkPrlID=%d ORDER BY pkPrlID"),TBL_PRICELIST,prl_id);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
	
		while(m_saCommand.FetchNext())
		{
			SADateTime saCreated = m_saCommand.Field("dCreated").asDateTime();
			SADateTime saUpdated = m_saCommand.Field("dUpdated").asDateTime();
			rec = CPricelist(m_saCommand.Field("pkPrlID").asShort(),
											 m_saCommand.Field("sName").asString(),
											 m_saCommand.Field("sCreatedBy").asString(),
											 m_saCommand.Field("sDate").asString(),
											 m_saCommand.Field("sPricelist").asLongChar(),
											 m_saCommand.Field("nLocked").asShort(),
											 m_saCommand.Field("sNotes").asLongChar(),
											(LPCTSTR)convSADateTime(saCreated),
											(LPCTSTR)convSADateTime(saUpdated));

		}

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return bReturn;
}

BOOL CDBHandling::newPricelist(CPricelist& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"INSERT INTO %s (sName,sCreatedBy,sDate,sPricelist,nLocked,sNotes) VALUES (:1,:2,:3,:4,:5,:6)",TBL_PRICELIST);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsString()	= rec.getName();
		m_saCommand.Param(2).setAsString()	= rec.getCreatedBy();
		m_saCommand.Param(3).setAsString()	= rec.getDate();
		m_saCommand.Param(4).setAsLongChar()	= rec.getPricelist();
		m_saCommand.Param(5).setAsShort()		= rec.getLocked();
		m_saCommand.Param(6).setAsLongChar()	= rec.getNotes();

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::updPricelist(CPricelist& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"UPDATE %s SET sName=:1,sCreatedBy=:2,sDate=:3,sPricelist=:4,nLocked=:5,sNotes=:6,dUpdated=GETDATE() WHERE pkPrlID=:7",TBL_PRICELIST);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsString()	= rec.getName();
		m_saCommand.Param(2).setAsString()	= rec.getCreatedBy();
		m_saCommand.Param(3).setAsString()	= rec.getDate();
		m_saCommand.Param(4).setAsLongChar()	= rec.getPricelist();
		m_saCommand.Param(5).setAsShort()		= rec.getLocked();
		m_saCommand.Param(6).setAsLongChar()	= rec.getNotes();

		m_saCommand.Param(7).setAsShort()		= rec.getPrlID();

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}


BOOL CDBHandling::delPricelist(int spc_id)
{
	CString sSQL = L"";
	try
	{
			sSQL.Format(L"DELETE FROM %s WHERE pkPrlID=:1",TBL_PRICELIST);
		
		m_saCommand.setCommandText((SAString)sSQL);
		
		if (spc_id > 0)
			m_saCommand.Param(1).setAsShort()	= spc_id;

		m_saCommand.Execute();	
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::resetPricelistIdentityField(void)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("DBCC CHECKIDENT('%s', RESEED, 0)"),TBL_PRICELIST);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}
	return TRUE;
}

BOOL CDBHandling::isPricelistNameAlreadyUsed(LPCTSTR name)
{
	int nCnt = 0;
	CString sSQL = L"";
	try
	{
		sSQL.Format(_T("SELECT count(sName) as 'antal' FROM %s WHERE sName='%s'"),TBL_PRICELIST,name);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
	
		while(m_saCommand.FetchNext())
		{
			nCnt = m_saCommand.Field("antal").asShort();				
		}

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return (nCnt == 1);
}



////////////////////////////////////////////////////////////////
// Register
BOOL CDBHandling::getRegister(CVecRegister& vec,int type)
{
	CString sSQL = L"";
	CRegister rec = CRegister();
	BOOL bReturn = TRUE;
	try
	{
		vec.clear();

		if (type > -1)
			sSQL.Format(_T("SELECT * FROM %s WHERE nTypeID=%d"),TBL_REGISTER,type);
		else
			sSQL.Format(_T("SELECT * FROM %s"),TBL_REGISTER);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
	
		while(m_saCommand.FetchNext())
		{
			SADateTime saCreated = m_saCommand.Field("dCreated").asDateTime();
			SADateTime saUpdated = m_saCommand.Field("dUpdated").asDateTime();
			rec = CRegister(m_saCommand.Field("pkID").asShort(),
											 m_saCommand.Field("nTypeID").asShort(),
											 m_saCommand.Field("sName").asString(),
											 m_saCommand.Field("sAbbrevation").asString(),
											 m_saCommand.Field("sID").asString(),
											 m_saCommand.Field("sOtherInfo").asString(),
											 m_saCommand.Field("sAddress").asString(),
											 m_saCommand.Field("sAddress2").asString(),
											 m_saCommand.Field("sZipCode").asString(),
											 m_saCommand.Field("sCity").asString(),
											 m_saCommand.Field("sPhone").asString(),
											 m_saCommand.Field("sCellPhone").asString(),
											 m_saCommand.Field("sWWW").asString(),
											(LPCTSTR)convSADateTime(saCreated),
											(LPCTSTR)convSADateTime(saUpdated));
			vec.push_back(rec);

		}
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return bReturn;
}

BOOL CDBHandling::newRegisterItem(CRegister& rec)
{
	CString sSQL = L"",S;
	try
	{
		sSQL.Format(L"INSERT INTO %s (nTypeID,sName,sAbbrevation,sID,sOtherInfo,sAddress,sAddress2,sZipCode,sCity,sPhone,sCellPhone,sWWW)"
			L" VALUES (:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12)",TBL_REGISTER);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsShort()		= rec.getTypeID();
		m_saCommand.Param(2).setAsString()	= rec.getName();
		m_saCommand.Param(3).setAsString()	= rec.getAbbrevation();
		m_saCommand.Param(4).setAsString()	= rec.getID();
		m_saCommand.Param(5).setAsString()	= rec.getOtherInfo();
		m_saCommand.Param(6).setAsString()	= rec.getAddress();
		m_saCommand.Param(7).setAsString()	= rec.getAddress2();
		m_saCommand.Param(8).setAsString()	= rec.getZipCode();
		m_saCommand.Param(9).setAsString()	= rec.getCity();
		m_saCommand.Param(10).setAsString()	= rec.getPhone();
		m_saCommand.Param(11).setAsString()	= rec.getCellPhone();
		m_saCommand.Param(12).setAsString()	= rec.getWWW();

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		if (e.ErrNativeCode() != 2627)
		{
			AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		}
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::updRegisterItem(CRegister& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"UPDATE %s SET sName=:1,sAbbrevation=:2,sID=:3,sOtherInfo=:4,sAddress=:5,sAddress2=:6,sZipCode=:7,sCity=:8,sPhone=:9,sCellPhone=:10,sWWW=:11 WHERE pkID=:12 AND nTypeID=:13",TBL_REGISTER);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsString()	= rec.getName();
		m_saCommand.Param(2).setAsString()	= rec.getAbbrevation();
		m_saCommand.Param(3).setAsString()	= rec.getID();
		m_saCommand.Param(4).setAsString()	= rec.getOtherInfo();
		m_saCommand.Param(5).setAsString()	= rec.getAddress();
		m_saCommand.Param(6).setAsString()	= rec.getAddress2();
		m_saCommand.Param(7).setAsString()	= rec.getZipCode();
		m_saCommand.Param(8).setAsString()	= rec.getCity();
		m_saCommand.Param(9).setAsString()	= rec.getPhone();
		m_saCommand.Param(10).setAsString()	= rec.getCellPhone();
		m_saCommand.Param(11).setAsString()	= rec.getWWW();

		m_saCommand.Param(12).setAsShort()		= rec.getPKID();;
		m_saCommand.Param(13).setAsShort()		= rec.getTypeID();

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::delRegisterItem(CRegister& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"DELETE FROM %s WHERE pkID=:1 AND nTypeID=:2",TBL_REGISTER);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsShort()		= rec.getPKID();;
		m_saCommand.Param(2).setAsShort()		= rec.getTypeID();

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::resetRegisterIdentityField(void)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("DBCC CHECKIDENT('%s', RESEED, 0)"),TBL_REGISTER);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}
	return TRUE;
}

BOOL CDBHandling::isNameInRegister(int type,LPCTSTR name)
{
	int nCnt = 0;
	CString sSQL = L"";
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("SELECT 1 FROM %s WHERE nTypeID=%d AND sName=\'%s\'"),TBL_REGISTER,type,name);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
	
		while(m_saCommand.FetchNext())
		{
			nCnt++;
		}
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return (nCnt > 0);	// More than one occurence
}

/*
////////////////////////////////////////////////////////////////
// CalcTypes
// Not used in new LogScalePC; 120117 p�d
// Collected in DLL volumefunctions and
// Userdefined volume-tables

BOOL CDBHandling::getCalcTypes(CVecCalcTypes& vec)
{
	CString sSQL = L"";
	CCalcTypes rec = CCalcTypes();
	BOOL bReturn = TRUE;
	try
	{
		vec.clear();
		sSQL.Format(_T("SELECT * FROM %s ORDER BY pkCalcTypeID"),TBL_CALCTYPES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saCreated = m_saCommand.Field("dCreated").asDateTime();
			SADateTime saUpdated = m_saCommand.Field("dUpdated").asDateTime();
			rec = CCalcTypes(m_saCommand.Field("pkCalcTypeID").asShort(),
											 m_saCommand.Field("sCalcType").asString(),
											 m_saCommand.Field("sCalcBasis").asString(),
											 m_saCommand.Field("nOnlySW").asShort(),
											(LPCTSTR)convSADateTime(saCreated),
											(LPCTSTR)convSADateTime(saUpdated));
			vec.push_back(rec);
		}

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return bReturn;
}
*/
////////////////////////////////////////////////////////////////
// Defaults
BOOL CDBHandling::getDefaults(CVecDefaults& vec)
{
	CString sSQL = L"";
	CDefaults rec = CDefaults();
	BOOL bReturn = TRUE;
	try
	{
		vec.clear();
		sSQL.Format(_T("SELECT * FROM %s ORDER BY nDefValueID"),TBL_DEFAULTS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saCreated = m_saCommand.Field("dCreated").asDateTime();
			SADateTime saUpdated = m_saCommand.Field("dUpdated").asDateTime();
			rec = CDefaults(m_saCommand.Field("nDefValueID").asShort(),
											 m_saCommand.Field("sDefValue").asString(),
											 m_saCommand.Field("sDefBasisValue").asString(),
											(LPCTSTR)convSADateTime(saCreated),
											(LPCTSTR)convSADateTime(saUpdated),
											m_saCommand.Field("nFuncID").asShort());	//#4258 h�mtar ut FuncID
			vec.push_back(rec);
		}

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return bReturn;
}

BOOL CDBHandling::addDefaults(CDefaults& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"INSERT INTO %s (nDefValueID,sDefValue,sDefBasisValue,nFuncID) "
			L"VALUES(:1,:2,:3,:4)",TBL_DEFAULTS);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsShort()		= rec.getDefvalueID();
		m_saCommand.Param(2).setAsString()	= rec.getDefStr();
		m_saCommand.Param(3).setAsString()	= rec.getDefBasisStr();
		m_saCommand.Param(4).setAsShort() = rec.getDefFuncID();	//#4258 sparar FuncID i db

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::updDefaults(CDefaults& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"UPDATE %s SET sDefValue=:1,sDefBasisValue=:2,dUpdated=GETDATE(),nFuncID=:4 WHERE nDefValueID=:3",TBL_DEFAULTS);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsString()	= rec.getDefStr();
		m_saCommand.Param(2).setAsString()	= rec.getDefBasisStr();
		m_saCommand.Param(3).setAsShort()		= rec.getDefvalueID();
		m_saCommand.Param(4).setAsShort() = rec.getDefFuncID();	//#4258 uppdaterar FuncID i db

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::isDefValueAdded(int id)
{
	CString sSQL = L"";
	BOOL bReturn = FALSE;
	try
	{
		sSQL.Format(_T("SELECT 1 FROM %s WHERE nDefValueID=%d"),TBL_DEFAULTS,id);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			bReturn = TRUE;
		}

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return bReturn;
}


////////////////////////////////////////////////////////////////
// UserVolTables
BOOL CDBHandling::getUserVolTables(CVecUserVolTables& vec)
{
	CString sSQL = L"";
	CUserVolTables rec = CUserVolTables();
	BOOL bReturn = TRUE;
	try
	{
		vec.clear();
		sSQL.Format(_T("SELECT * FROM %s ORDER BY pkID"),TBL_USER_VOL_TABLES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saCreated = m_saCommand.Field("dCreated").asDateTime();
			SADateTime saUpdated = m_saCommand.Field("dUpdated").asDateTime();
			rec = CUserVolTables(m_saCommand.Field("pkID").asLong(),
													 m_saCommand.Field("nFuncID").asShort(),
													 m_saCommand.Field("sFullName").asString(),
													 m_saCommand.Field("sAbbrevName").asString(),
													 m_saCommand.Field("sBasisName").asString(),
													 m_saCommand.Field("sCreatedBy").asString(),
													 m_saCommand.Field("sDate").asString(),
													 m_saCommand.Field("nTableType").asShort(),
													 m_saCommand.Field("nMeasuringMode").asShort(),
													 m_saCommand.Field("sNotes").asLongChar(),
													 m_saCommand.Field("sVolTable").asLongChar(),
													 m_saCommand.Field("nLocked").asShort(),
													(LPCTSTR)convSADateTime(saCreated),
													(LPCTSTR)convSADateTime(saUpdated));
			vec.push_back(rec);
		}
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return bReturn;
}

BOOL CDBHandling::addUserVolTable(CUserVolTables& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"INSERT INTO %s (nFuncID,sFullname,sAbbrevName,sBasisName,sCreatedBy,sDate,nTableType,nMeasuringMode,sNotes,sVolTable,nLocked) "
			L"VALUES(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11)",TBL_USER_VOL_TABLES);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsShort()		= rec.getFuncID();
		m_saCommand.Param(2).setAsString()	= rec.getFullName();
		m_saCommand.Param(3).setAsString()	= rec.getAbbrevName();
		m_saCommand.Param(4).setAsString()	= rec.getBasisName();
		m_saCommand.Param(5).setAsString()	= rec.getCreatedBy();
		m_saCommand.Param(6).setAsString()	= rec.getDate();
		m_saCommand.Param(7).setAsShort()		= rec.getTableType();
		m_saCommand.Param(8).setAsShort()		= rec.getMeasuringMode();
		m_saCommand.Param(9).setAsLongChar()	= rec.getNotes();
		m_saCommand.Param(10).setAsLongChar()	= rec.getVolTable();
		m_saCommand.Param(11).setAsShort()	= rec.getLocked();

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::updUserVolTable(CUserVolTables& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"UPDATE %s SET nFuncID=:1,sFullname=:2,sAbbrevName=:3,sBasisName=:4,sCreatedBy=:5,sDate=:6,nTableType=:7,nMeasuringMode=:8,sNotes=:9,sVolTable=:10,nLocked=:11,dUpdated=GETDATE() WHERE pkID=:12",TBL_USER_VOL_TABLES);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsShort()		= rec.getFuncID();
		m_saCommand.Param(2).setAsString()	= rec.getFullName();
		m_saCommand.Param(3).setAsString()	= rec.getAbbrevName();
		m_saCommand.Param(4).setAsString()	= rec.getBasisName();
		m_saCommand.Param(5).setAsString()	= rec.getCreatedBy();
		m_saCommand.Param(6).setAsString()	= rec.getDate();
		m_saCommand.Param(7).setAsShort()		= rec.getTableType();
		m_saCommand.Param(8).setAsShort()		= rec.getMeasuringMode();
		m_saCommand.Param(9).setAsString()	= rec.getNotes();
		m_saCommand.Param(10).setAsLongChar()	= rec.getVolTable();
		m_saCommand.Param(11).setAsShort()	= rec.getLocked();

		m_saCommand.Param(12).setAsLong()		= rec.getID();

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::delUserVolTable(CUserVolTables& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"DELETE FROM %s WHERE pkID=:1",TBL_USER_VOL_TABLES);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsLong()		= rec.getID();

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::resetUserVolTableIdentityField(void)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("DBCC CHECKIDENT('%s', RESEED, 0)"),TBL_USER_VOL_TABLES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}
	return TRUE;
}


////////////////////////////////////////////////////////////////
// LogScale templates
BOOL CDBHandling::getTmplTables(vecLogScaleTemplates& vec)
{
	CString sSQL = L"";
	CLogScaleTemplates rec = CLogScaleTemplates();
	BOOL bReturn = TRUE;
	try
	{
		vec.clear();
		sSQL.Format(_T("SELECT * FROM %s ORDER BY pkTmplID"),TBL_TEMPLATES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saCreated = m_saCommand.Field("dCreated").asDateTime();
			SADateTime saUpdated = m_saCommand.Field("dUpdated").asDateTime();
			rec = CLogScaleTemplates(m_saCommand.Field("pkTmplID").asShort(),
														 m_saCommand.Field("sTmplName").asString(),
														 m_saCommand.Field("sCreatedBy").asString(),
														 m_saCommand.Field("sDate").asString(),
														 m_saCommand.Field("nBuyer").asShort(),
														 m_saCommand.Field("nHauler").asShort(),
														 m_saCommand.Field("nLocation").asShort(),
														 m_saCommand.Field("nTractID").asShort(),
														 m_saCommand.Field("nScaler").asShort(),
														 m_saCommand.Field("nSourceID").asShort(),
														 m_saCommand.Field("nVendor").asShort(),
														 m_saCommand.Field("nSupplier").asShort(),
														 m_saCommand.Field("sNotes").asLongChar(),
														 m_saCommand.Field("sPricelist").asLongChar(),
														 m_saCommand.Field("nPrlID").asShort(),
														 m_saCommand.Field("sPrlName").asString(),
 														 m_saCommand.Field("fTrimFT").asDouble(),
 														 m_saCommand.Field("fTrimCM").asDouble(),
														 m_saCommand.Field("nMeasureMode").asShort(),
														 m_saCommand.Field("nDefSpecies").asShort(),
														 m_saCommand.Field("nDefGrade").asShort(),
														(LPCTSTR)convSADateTime(saCreated),
													(LPCTSTR)convSADateTime(saUpdated));
			vec.push_back(rec);
		}

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return bReturn;
}

BOOL CDBHandling::addTmplTable(CLogScaleTemplates& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"INSERT INTO %s (sTmplName,sCreatedBy,sDate,nBuyer,nHauler,nLocation,nTractID,nScaler,nSourceID,nVendor,nSupplier,sNotes,"
								L"sPricelist,nPrlID,sPrlName,fTrimFT,fTrimCM,nMeasureMode,nDefSpecies,nDefGrade) "
								L"VALUES(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19,:20)",TBL_TEMPLATES);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsString()	= rec.getTmplName();
		m_saCommand.Param(2).setAsString()	= rec.getCreatedBy();
		m_saCommand.Param(3).setAsString()	= rec.getDate();
		m_saCommand.Param(4).setAsShort()		= rec.getBuyer();
		m_saCommand.Param(5).setAsShort()		= rec.getHauler();
		m_saCommand.Param(6).setAsShort()		= rec.getLocation();
		m_saCommand.Param(7).setAsShort()		= rec.getTractID();
		m_saCommand.Param(8).setAsShort()		= rec.getScaler();
		m_saCommand.Param(9).setAsShort()		= rec.getSourceID();
		m_saCommand.Param(10).setAsShort()	= rec.getVendor();
		m_saCommand.Param(11).setAsShort()	= rec.getSupplier();
		m_saCommand.Param(12).setAsLongChar()	= rec.getNotes();
		m_saCommand.Param(13).setAsLongChar()	= rec.getPricelist();
		m_saCommand.Param(14).setAsShort()	= rec.getPrlID();
		m_saCommand.Param(15).setAsString()	= rec.getPrlName();
		m_saCommand.Param(16).setAsDouble()	= rec.getTrimFT();
		m_saCommand.Param(17).setAsDouble()	= rec.getTrimCM();
		m_saCommand.Param(18).setAsShort()	= rec.getMeasureMode();
		m_saCommand.Param(19).setAsShort()	= rec.getDefSpecies();
		m_saCommand.Param(20).setAsShort()	= rec.getDefGrade();

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::updTmplTable(CLogScaleTemplates& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"UPDATE %s SET sTmplName=:1,sCreatedBy=:2,sDate=:3,nBuyer=:4,nHauler=:5,nLocation=:6,nTractID=:7,nScaler=:8,"
								L"nSourceID=:9,nVendor=:10,nSupplier=:11,sNotes=:12,sPricelist=:13,nPrlID=:14,sPrlName=:15,fTrimFT=:16,fTrimCM=:17,"
								L"nMeasureMode=:18,nDefSpecies=:19,nDefGrade=:20,dUpdated=GETDATE() WHERE pkTmplID=:21 "
			,TBL_TEMPLATES);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsString()	= rec.getTmplName();
		m_saCommand.Param(2).setAsString()	= rec.getCreatedBy();
		m_saCommand.Param(3).setAsString()	= rec.getDate();
		m_saCommand.Param(4).setAsShort()		= rec.getBuyer();
		m_saCommand.Param(5).setAsShort()		= rec.getHauler();
		m_saCommand.Param(6).setAsShort()		= rec.getLocation();
		m_saCommand.Param(7).setAsShort()		= rec.getTractID();
		m_saCommand.Param(8).setAsShort()		= rec.getScaler();
		m_saCommand.Param(9).setAsShort()		= rec.getSourceID();
		m_saCommand.Param(10).setAsShort()	= rec.getVendor();
		m_saCommand.Param(11).setAsShort()	= rec.getSupplier();
		m_saCommand.Param(12).setAsLongChar()	= rec.getNotes();
		m_saCommand.Param(13).setAsLongChar()	= rec.getPricelist();
		m_saCommand.Param(14).setAsShort()	= rec.getPrlID();
		m_saCommand.Param(15).setAsString()	= rec.getPrlName();
		m_saCommand.Param(16).setAsDouble()	= rec.getTrimFT();
		m_saCommand.Param(17).setAsDouble()	= rec.getTrimCM();
		m_saCommand.Param(18).setAsShort()	= rec.getMeasureMode();
		m_saCommand.Param(19).setAsShort()	= rec.getDefSpecies();
		m_saCommand.Param(20).setAsShort()	= rec.getDefGrade();

		m_saCommand.Param(21).setAsShort()	= rec.getTmplID();

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::delTmplTable(CLogScaleTemplates& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"DELETE FROM %s WHERE pkTmplID=:1",TBL_TEMPLATES);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsShort()		= rec.getTmplID();

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::updTmplTablePricelist(int tmpl_id,int prl_id,LPCTSTR prl_name,LPCTSTR prl)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"UPDATE %s SET sPricelist=:1,nPrlID=:2,sPrlName=:3,dUpdated=GETDATE() WHERE pkTmplID=:4 "
			,TBL_TEMPLATES);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsLongChar()	= prl;
		m_saCommand.Param(2).setAsShort()		= prl_id;
		m_saCommand.Param(3).setAsString()	= prl_name;
		
		m_saCommand.Param(4).setAsShort()		= tmpl_id;

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::resetTmplTableIdentityField(void)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("DBCC CHECKIDENT('%s', RESEED, 0)"),TBL_TEMPLATES);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}
	return TRUE;
}

// Check if a contract (template) is in use in Ticket-table
BOOL CDBHandling::isTmplInUse(int tmpl_id)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(_T("SELECT 1 FROM %s WHERE nTemplateID=%d"),TBL_TICKETS,tmpl_id);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			return TRUE;
		}

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return FALSE;
}

BOOL CDBHandling::isTmplNameAlreadyUsed(LPCTSTR name)
{
	int nCnt = 0;
	CString sSQL = L"";
	try
	{
		sSQL.Format(_T("SELECT count(sTmplName) as 'antal' FROM %s WHERE sTmplName='%s'"),TBL_TEMPLATES,name);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
	
		while(m_saCommand.FetchNext())
		{
			nCnt = m_saCommand.Field("antal").asShort();				
		}

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return (nCnt == 1);
}



////////////////////////////////////////////////////////////////
// Handle selected extra volumefunctions/ticket
BOOL CDBHandling::getSelVolFuncs(vecSelectedVolFuncs& vec,int ticket_id)
{
	CString sSQL = L"";
	CSelectedVolFuncs rec = CSelectedVolFuncs();
	BOOL bReturn = TRUE;
	try
	{
		vec.clear();
		if (ticket_id == -1)
			sSQL.Format(_T("SELECT * FROM %s ORDER BY nFuncID,fkTicketID"),TBL_SEL_VOL_FUNCS);
		else if (ticket_id > -1)
			sSQL.Format(_T("SELECT * FROM %s WHERE fkTicketID=%d ORDER BY nFuncID"),TBL_SEL_VOL_FUNCS,ticket_id);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			rec = CSelectedVolFuncs(m_saCommand.Field("pkID").asShort(),
														 m_saCommand.Field("fkTicketID").asShort(),
														 m_saCommand.Field("nFuncID").asShort(),
														 m_saCommand.Field("sFullName").asString(),
														 m_saCommand.Field("sAbbrevName").asString(),
														 m_saCommand.Field("sBasisName").asString());
			vec.push_back(rec);
		}

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::addSelVolFuncs(CSelectedVolFuncs& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"INSERT INTO %s (fkTicketID,nFuncID,sFullName,sAbbrevName,sBasisName) "
			L"VALUES(:1,:2,:3,:4,:5)",TBL_SEL_VOL_FUNCS);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsShort()		= rec.getTicketID();
		m_saCommand.Param(2).setAsShort()		= rec.getFuncID();
		m_saCommand.Param(3).setAsString()	= rec.getFullName();
		m_saCommand.Param(4).setAsString()	= rec.getAbbrevName();
		m_saCommand.Param(5).setAsString()	= rec.getBasisName();

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}


	return TRUE;
}

BOOL CDBHandling::updSelVolFuncs(CSelectedVolFuncs& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"UPDATE %s SET nFuncID=:1,sFullName=:2,sAbbrevName=:3,sBasisName=:4 WHERE pkID=:5 AND fkTicketID=:6 ",TBL_SEL_VOL_FUNCS);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsShort()		= rec.getFuncID();
		m_saCommand.Param(2).setAsString()	= rec.getFullName();
		m_saCommand.Param(3).setAsString()	= rec.getAbbrevName();
		m_saCommand.Param(4).setAsString()	= rec.getBasisName();

		m_saCommand.Param(5).setAsShort()		= rec.getID();
		m_saCommand.Param(6).setAsShort()		= rec.getTicketID();

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::delSelVolFuncs(int ticket_id)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"DELETE FROM %s WHERE fkTicketID=:1 ",TBL_SEL_VOL_FUNCS);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsShort()		= ticket_id;

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::resetSelVolFuncsIdentityField(void)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("DBCC CHECKIDENT('%s', RESEED, 0)"),TBL_SEL_VOL_FUNCS);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}
	return TRUE;
}

////////////////////////////////////////////////////////////////
// Handle selected default extra volumefunctions
BOOL CDBHandling::getDefaultUserVolFuncSel(vecDefUserVolFuncs& vec)
{
	CString sSQL = L"";
	CDefUserVolFuncs rec = CDefUserVolFuncs();
	BOOL bReturn = TRUE;
	try
	{
		vec.clear();
		sSQL.Format(_T("SELECT * FROM %s ORDER BY nFuncID"),TBL_DEF_USER_VOL_SEL);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			rec = CDefUserVolFuncs(m_saCommand.Field("nFuncID").asShort(),
														 m_saCommand.Field("sFullName").asString(),
														 m_saCommand.Field("sAbbrevName").asString(),
														 m_saCommand.Field("sBasisName").asString());
			vec.push_back(rec);
		}

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::addDefaultUserVolFuncSel(CDefUserVolFuncs& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"INSERT INTO %s (nFuncID,sFullName,sAbbrevName,sBasisName) VALUES(:1,:2,:3,:4)",TBL_DEF_USER_VOL_SEL);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsShort()		= rec.getFuncID();
		m_saCommand.Param(2).setAsString()	= rec.getFullName();
		m_saCommand.Param(3).setAsString()	= rec.getAbbrevName();
		m_saCommand.Param(4).setAsString()	= rec.getBasisName();

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}


	return TRUE;
}


BOOL CDBHandling::delDefaultUserVolFuncSel()
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"DELETE FROM %s",TBL_DEF_USER_VOL_SEL);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}


////////////////////////////////////////////////////////////////
// Inventory tables; for i.g. Sales etc.
BOOL CDBHandling::getInventories(vecInventory& vec)
{
	CString sSQL = L"";
	BOOL bReturn = TRUE;
	try
	{
		vec.clear();
		sSQL.Format(_T("SELECT * FROM %s ORDER BY pkInventID"),TBL_INVENTORY);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			vec.push_back(CInventory(m_saCommand.Field("pkInventID").asLong(),
											 m_saCommand.Field("nType").asShort(),
											 m_saCommand.Field("sName").asString(),
											 m_saCommand.Field("sID").asString(),
											 m_saCommand.Field("sCreatedBy").asString(),
											 m_saCommand.Field("sDate").asString(),
											 m_saCommand.Field("nNumOfLogs").asShort(),
											 m_saCommand.Field("fSumPrice").asDouble(),
											 m_saCommand.Field("nBuyer").asShort(),
											 m_saCommand.Field("nHauler").asShort(),
											 m_saCommand.Field("nLocation").asShort(),
											 m_saCommand.Field("nTractID").asShort(),
											 m_saCommand.Field("nScaler").asShort(),
											 m_saCommand.Field("nSourceID").asShort(),
											 m_saCommand.Field("nVendor").asShort(),
											 m_saCommand.Field("nSupplier").asShort(),
											 m_saCommand.Field("sNotes").asLongChar(),
											 m_saCommand.Field("sGPSCoord1").asString(),
											 m_saCommand.Field("sGPSCoord2").asString()));
		}

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::getInventory(int pk_id,int type,CInventory& rec)
{
	CString sSQL = L"";
	BOOL bReturn = TRUE;
	try
	{
		if (type > -1)
			sSQL.Format(_T("SELECT * FROM %s WHERE pkInventID=%d AND nType=%d ORDER BY pkInventID"),TBL_INVENTORY,pk_id,type);
		else
			sSQL.Format(_T("SELECT * FROM %s WHERE pkInventID=%d ORDER BY pkInventID"),TBL_INVENTORY,pk_id);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			rec = CInventory(m_saCommand.Field("pkInventID").asLong(),
											 m_saCommand.Field("nType").asShort(),
											 m_saCommand.Field("sName").asString(),
											 m_saCommand.Field("sID").asString(),
											 m_saCommand.Field("sCreatedBy").asString(),
											 m_saCommand.Field("sDate").asString(),
											 m_saCommand.Field("nNumOfLogs").asShort(),
											 m_saCommand.Field("fSumPrice").asDouble(),
											 m_saCommand.Field("nBuyer").asShort(),
											 m_saCommand.Field("nHauler").asShort(),
											 m_saCommand.Field("nLocation").asShort(),
											 m_saCommand.Field("nTractID").asShort(),
											 m_saCommand.Field("nScaler").asShort(),
											 m_saCommand.Field("nSourceID").asShort(),
											 m_saCommand.Field("nVendor").asShort(),
											 m_saCommand.Field("nSupplier").asShort(),
											 m_saCommand.Field("sNotes").asLongChar(),
											 m_saCommand.Field("sGPSCoord1").asString(),
											 m_saCommand.Field("sGPSCoord2").asString());
		}

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}


BOOL CDBHandling::newInventory(CInventory& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"INSERT INTO %s (nType,sName,sID,sCreatedBy,sDate,nNumOfLogs,fSumPrice,nBuyer,nHauler,nLocation,nTractID,nScaler,nSourceID,nVendor,nSupplier,sNotes,sGPSCoord1,sGPSCoord2) "
			L"VALUES(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18)",TBL_INVENTORY);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsShort()		= rec.getType();
		m_saCommand.Param(2).setAsString()	= rec.getName();
		m_saCommand.Param(3).setAsString()	= rec.getID();
		m_saCommand.Param(4).setAsString()	= rec.getCreatedBy();
		m_saCommand.Param(5).setAsString()	= rec.getDate();
		m_saCommand.Param(6).setAsShort()		= rec.getNumOfLogs();
		m_saCommand.Param(7).setAsDouble()	= rec.getSumPrice();
		m_saCommand.Param(8).setAsShort()		= rec.getBuyerID();
		m_saCommand.Param(9).setAsShort()		= rec.getHaulerID();
		m_saCommand.Param(10).setAsShort()		= rec.getLocationID();
		m_saCommand.Param(11).setAsShort()		= rec.getTractID();
		m_saCommand.Param(12).setAsShort()	= rec.getScalerID();
		m_saCommand.Param(13).setAsShort()	= rec.getSourceID();
		m_saCommand.Param(14).setAsShort()	= rec.getVendorID();
		m_saCommand.Param(15).setAsShort()	= rec.getSupplierID();
		m_saCommand.Param(16).setAsLongChar()	= rec.getNotes();
		m_saCommand.Param(17).setAsString()	= rec.getGPSCoord1();
		m_saCommand.Param(18).setAsString()	= rec.getGPSCoord2();

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}


	return TRUE;
}

BOOL CDBHandling::updInventory(CInventory& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"UPDATE %s SET sName=:1,sID=:2,sCreatedBy=:3,sDate=:4,nNumOfLogs=:5,fSumPrice=:6,nBuyer=:7,nHauler=:8,nLocation=:9,nTractID=:10,nScaler=:11,nSourceID=:12,nVendor=:13,"
								L"nSupplier=:14,sNotes=:15,sGPSCoord1=:16,sGPSCoord2=:17 WHERE pkInventID=:18 AND nType=:19",TBL_INVENTORY);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Param(1).setAsString()	= rec.getName();
		m_saCommand.Param(2).setAsString()	= rec.getID();
		m_saCommand.Param(3).setAsString()	= rec.getCreatedBy();
		m_saCommand.Param(4).setAsString()	= rec.getDate();
		m_saCommand.Param(5).setAsShort()		= rec.getNumOfLogs();
		m_saCommand.Param(6).setAsDouble()	= rec.getSumPrice();
		m_saCommand.Param(7).setAsShort()		= rec.getBuyerID();
		m_saCommand.Param(8).setAsShort()		= rec.getHaulerID();
		m_saCommand.Param(9).setAsShort()		= rec.getLocationID();
		m_saCommand.Param(10).setAsShort()		= rec.getTractID();
		m_saCommand.Param(11).setAsShort()	= rec.getScalerID();
		m_saCommand.Param(12).setAsShort()	= rec.getSourceID();
		m_saCommand.Param(13).setAsShort()	= rec.getVendorID();
		m_saCommand.Param(14).setAsShort()	= rec.getSupplierID();
		m_saCommand.Param(15).setAsLongChar()	= rec.getNotes();
		m_saCommand.Param(16).setAsString()	= rec.getGPSCoord1();
		m_saCommand.Param(17).setAsString()	= rec.getGPSCoord2();

		m_saCommand.Param(18).setAsLong()		= rec.getPKID();
		m_saCommand.Param(19).setAsShort()	= rec.getType();

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}


	return TRUE;
}

BOOL CDBHandling::delInventory(CInventory &rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"DELETE FROM %s WHERE pkInventID=%d",TBL_INVENTORY,rec.getPKID());
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::delInventory(int pk_id,int type)
{
	CString sSQL = L"";
	try
	{
		if (type == -1)
			sSQL.Format(L"DELETE FROM %s WHERE pkInventID=%d",TBL_INVENTORY,pk_id);
		else
			sSQL.Format(L"DELETE FROM %s WHERE pkInventID=%d AND nType=%d",TBL_INVENTORY,pk_id,type);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

void CDBHandling::getLastInventoryAndTypeID(int *pk_id,int *type)
{
	*pk_id = last_identity(TBL_INVENTORY,_T("pkInventID"));
	*type = last_identity(TBL_INVENTORY,_T("nType"));

}

int CDBHandling::getLastInventoryID_generate()
{
	int nLastID = 0;
	CString sSQL;
	try
	{
		sSQL.Format(_T("SELECT MAX(pkInventID) AS 'last_id' FROM %s"),TBL_INVENTORY);
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Execute();	

		while(m_saCommand.FetchNext())
		{
			nLastID = m_saCommand.Field("last_id").asShort();
		}
		
		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return 0;
	}

	return nLastID;
}

BOOL CDBHandling::resetInventoryIdentityField(void)
{
	CString sSQL;
	try
	{
		sSQL.Format(_T("DBCC CHECKIDENT('%s', RESEED, 0)"),TBL_INVENTORY);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		doCommit();
	}
	catch(SAException &e)
	{
		 // print error message
		AfxMessageBox((LPCWSTR)e.ErrText());
		doRollback();
		return FALSE;
	}
	return TRUE;
}

BOOL CDBHandling::getInventoryLogs(vecInventoryLogs& vec,int invent_id)
{
	CString sSQL = L"";
	BOOL bReturn = TRUE;
	try
	{
		vec.clear();
		if (invent_id == -1)
			sSQL.Format(_T("SELECT * FROM %s ORDER BY fkInventID"),TBL_INVENTORY_LOGS);
		else
			sSQL.Format(_T("SELECT * FROM %s WHERE fkInventID=%d ORDER BY fkInventID"),TBL_INVENTORY_LOGS,invent_id);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			vec.push_back(CInventoryLogs(m_saCommand.Field("fkInventID").asLong(),
																	 m_saCommand.Field("fkLoadID").asLong(),
																	 m_saCommand.Field("fkLogID").asLong()));
		}

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::newInventoryLogs(CInventoryLogs& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"INSERT INTO %s (fkInventID,fkLoadID,fkLogID) VALUES(:1,:2,:3)",TBL_INVENTORY_LOGS);
		m_saCommand.setCommandText((SAString)sSQL);

			m_saCommand.Param(1).setAsLong()		= rec.getFKInventID();
			m_saCommand.Param(2).setAsLong()		= rec.getFKLoadID();
			m_saCommand.Param(3).setAsLong()		= rec.getFKLogID();

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}


	return TRUE;
}

BOOL CDBHandling::newInventoryLogs(CString sql)
{
	CString sSQL(sql);
	try
	{
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}


	return TRUE;
}

// Delete a specific log from Invnetory sales etc.
// NB! The actual log in logscaleLogsNew table will NOT be deleted
BOOL CDBHandling::delInventoryLogs(CInventoryLogs& rec)
{
	CString sSQL = L"";
	try
	{
		sSQL.Format(L"DELETE FROM %s WHERE fkInventID=%d AND fkLoadID=%d AND fkLogID=%d ",
			TBL_INVENTORY_LOGS,
			rec.getFKInventID(),
			rec.getFKLoadID(),
			rec.getFKLogID());
		m_saCommand.setCommandText((SAString)sSQL);

		m_saCommand.Execute();	

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::getLogsSale(CVecLogs& vec,int invent_id)
{
	CString sSQL = L"";
	try
	{
		vec.clear();
		
		sSQL.Format(_T("SELECT logscaleTicketsNew.sTicket,logscaleLogsNew.* FROM logscaleLogsNew INNER JOIN logscaleSalesInventLogsNew ON ")
								_T("logscaleSalesInventLogsNew.fkLoadID=logscaleLogsNew.fkLoadID AND ")
								_T("logscaleSalesInventLogsNew.fkLogID=logscaleLogsNew.pkLogID ")
								_T("inner join logscaleTicketsNew on ")
								_T("logscaleTicketsNew.pkLoadID=logscaleLogsNew.fkLoadID ")
								_T("where logscaleSalesInventLogsNew.fkInventID=%d"),invent_id);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			SADateTime saCreated = m_saCommand.Field("dCreated").asDateTime();
			SADateTime saUpdated = m_saCommand.Field("dUpdated").asDateTime();
			vec.push_back(CLogs(m_saCommand.Field("pkLogID").asLong(),
												  m_saCommand.Field("fkLoadID").asLong(),
													(LPCTSTR)m_saCommand.Field("sTagNumber").asString(),
													(LPCTSTR)m_saCommand.Field("sSpcCode").asString(),
													(LPCTSTR)m_saCommand.Field("sSpcName").asString(),
													m_saCommand.Field("nFrequency").asShort(),
													(LPCTSTR)m_saCommand.Field("sGrade").asString(),
													m_saCommand.Field("fDeduction").asDouble(),
													m_saCommand.Field("fDOBTop").asDouble(),
													m_saCommand.Field("fDIBTop").asDouble(),
													m_saCommand.Field("fDOBRoot").asDouble(),
													m_saCommand.Field("fDIBRoot").asDouble(),
													m_saCommand.Field("fLengthMeas").asDouble(),
													m_saCommand.Field("fLengthCalc").asDouble(),
													m_saCommand.Field("fBark").asDouble(),
													m_saCommand.Field("fVolPricelist").asDouble(),
													m_saCommand.Field("fLogPrice").asDouble(),
													m_saCommand.Field("nVolFuncID").asShort(),
													(LPCTSTR)m_saCommand.Field("sVolFuncName").asString(),
													m_saCommand.Field("fCustVol1").asDouble(),
													m_saCommand.Field("fCustVol2").asDouble(),
													m_saCommand.Field("fCustVol3").asDouble(),
													m_saCommand.Field("fCustVol4").asDouble(),
													m_saCommand.Field("fCustVol5").asDouble(),
													m_saCommand.Field("fCustVol6").asDouble(),
													m_saCommand.Field("fCustVol7").asDouble(),
													m_saCommand.Field("fCustVol8").asDouble(),
													m_saCommand.Field("fCustVol9").asDouble(),
													m_saCommand.Field("fCustVol10").asDouble(),
													m_saCommand.Field("fCustVol11").asDouble(),
													m_saCommand.Field("fCustVol12").asDouble(),
													m_saCommand.Field("fCustVol13").asDouble(),
													m_saCommand.Field("fCustVol14").asDouble(),
													m_saCommand.Field("fCustVol15").asDouble(),
													m_saCommand.Field("fCustVol16").asDouble(),
													m_saCommand.Field("fCustVol17").asDouble(),
													m_saCommand.Field("fCustVol18").asDouble(),
													m_saCommand.Field("fCustVol19").asDouble(),
													m_saCommand.Field("fCustVol20").asDouble(),
													m_saCommand.Field("fUserVolume").asDouble(),
													m_saCommand.Field("fUserToppDiam").asDouble(),
													m_saCommand.Field("fUserRootDiam").asDouble(),
													m_saCommand.Field("fUserLength").asDouble(),
													m_saCommand.Field("fUserPrice").asDouble(),
													m_saCommand.Field("fDOBTop2").asDouble(),
													m_saCommand.Field("fDIBTop2").asDouble(),
													m_saCommand.Field("fDOBRoot2").asDouble(),
													m_saCommand.Field("fDIBRoot2").asDouble(),
													m_saCommand.Field("fUserToppDiam2").asDouble(),
													m_saCommand.Field("fUserRootDiam2").asDouble(),
												  (LPCTSTR)m_saCommand.Field("sReason").asString(),
												  (LPCTSTR)m_saCommand.Field("sNotes").asString(),
												  (LPCTSTR)m_saCommand.Field("sGISCoord1").asString(),
												  (LPCTSTR)m_saCommand.Field("sGISCoord2").asString(),
													m_saCommand.Field("nStatusFlag").asShort(),
												  -1,	// Flag, not included as column in logscaleLogs-table
													m_saCommand.Field("nTicketOrigin").asShort(),
													-1,
												  (LPCTSTR)convSADateTime(saCreated),
												  (LPCTSTR)convSADateTime(saUpdated),
													(LPCTSTR)m_saCommand.Field("sTicket").asString()));
		}


		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}


BOOL CDBHandling::avgSaleLogData(CLogs& rec,int invent_id)
{
	CString sSQL = L"",S;
	try
	{

		sSQL.Format(L"SELECT AVG(fDOBTop) AS 'DOBT',"
								L"AVG(fDIBTop) AS 'DIBT',"
								L"AVG(fDOBRoot) AS 'DOBR',"
								L"AVG(fDIBRoot) AS 'DIBR',"
								L"AVG(fBark) AS 'BARK',"
								L"AVG(fLengthMeas) AS 'LENGTHMEAS',"
								L"AVG(fLengthCalc) AS 'LENGTHCALC',"
								L"AVG(fDOBTop2) AS 'DOBT2',"
								L"AVG(fDIBTop2) AS 'DIBT2',"
								L"AVG(fDOBRoot2) AS 'DOBR2',"
								L"AVG(fDIBRoot2) AS 'DIBR2',"
								L"SUM(fVolPricelist) AS 'VOLPRICELIST',"
								L"SUM(fLogPrice) AS 'LOGPRICE',"
								L"SUM(fCustVol1) AS 'CUST1',"
								L"SUM(fCustVol2) AS 'CUST2',"
								L"SUM(fCustVol3) AS 'CUST3',"
								L"SUM(fCustVol4) AS 'CUST4',"
								L"SUM(fCustVol5) AS 'CUST5',"
								L"SUM(fCustVol6) AS 'CUST6',"
								L"SUM(fCustVol7) AS 'CUST7',"
								L"SUM(fCustVol8) AS 'CUST8',"
								L"SUM(fCustVol9) AS 'CUST9',"
								L"SUM(fCustVol10) AS 'CUST10',"
								L"SUM(fCustVol11) AS 'CUST11',"
								L"SUM(fCustVol12) AS 'CUST12',"
								L"SUM(fCustVol13) AS 'CUST13',"
								L"SUM(fCustVol14) AS 'CUST14',"
								L"SUM(fCustVol15) AS 'CUST15',"
								L"SUM(fCustVol16) AS 'CUST16',"
								L"SUM(fCustVol17) AS 'CUST17',"
								L"SUM(fCustVol18) AS 'CUST18',"
								L"SUM(fCustVol19) AS 'CUST19',"
								L"SUM(fCustVol20) AS 'CUST20',"
								L"AVG(fUserToppDiam) AS 'UDIAT',"
								L"AVG(fUserToppDiam2) AS 'UDIAT2',"
								L"AVG(fUserVolume) AS 'UVOL',"
								L"AVG(fUserRootDiam) AS 'UDIAR',"
								L"AVG(fUserRootDiam2) AS 'UDIAR2',"
								L"AVG(fUserLength) AS 'ULEN',"
								L"SUM(fUserPrice) AS 'UPRICE' "
								L"FROM logscaleLogsNew INNER JOIN logscaleSalesInventLogsNew ON "
								L"logscaleSalesInventLogsNew.fkLoadID=logscaleLogsNew.fkLoadID AND "
								L"logscaleSalesInventLogsNew.fkLogID=logscaleLogsNew.pkLogID "
								L"WHERE logscaleSalesInventLogsNew.fkInventID=%d",invent_id);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			rec.setDOBTop(m_saCommand.Field("DOBT").asDouble());
			rec.setDIBTop(m_saCommand.Field("DIBT").asDouble());
			rec.setDOBRoot(m_saCommand.Field("DOBR").asDouble());
			rec.setDIBRoot(m_saCommand.Field("DIBR").asDouble());
			rec.setBark(m_saCommand.Field("BARK").asDouble());
			rec.setLengthMeas(m_saCommand.Field("LENGTHMEAS").asDouble());
			rec.setLengthCalc(m_saCommand.Field("LENGTHCALC").asDouble());
			
			rec.setDOBTop2(m_saCommand.Field("DOBT2").asDouble());
			rec.setDIBTop2(m_saCommand.Field("DIBT2").asDouble());
			rec.setDOBRoot2(m_saCommand.Field("DOBR2").asDouble());
			rec.setDIBRoot2(m_saCommand.Field("DIBR2").asDouble());
			
			rec.setVolPricelist(m_saCommand.Field("VOLPRICELIST").asDouble());
			rec.setLogPrice(m_saCommand.Field("LOGPRICE").asDouble());
			rec.setVolCust1(m_saCommand.Field("CUST1").asDouble());
			rec.setVolCust2(m_saCommand.Field("CUST2").asDouble());
			rec.setVolCust3(m_saCommand.Field("CUST3").asDouble());
			rec.setVolCust4(m_saCommand.Field("CUST4").asDouble());
			rec.setVolCust5(m_saCommand.Field("CUST5").asDouble());
			rec.setVolCust6(m_saCommand.Field("CUST6").asDouble());
			rec.setVolCust7(m_saCommand.Field("CUST7").asDouble());
			rec.setVolCust8(m_saCommand.Field("CUST8").asDouble());
			rec.setVolCust9(m_saCommand.Field("CUST9").asDouble());
			rec.setVolCust10(m_saCommand.Field("CUST10").asDouble());
			rec.setVolCust11(m_saCommand.Field("CUST11").asDouble());
			rec.setVolCust12(m_saCommand.Field("CUST12").asDouble());
			rec.setVolCust13(m_saCommand.Field("CUST13").asDouble());
			rec.setVolCust14(m_saCommand.Field("CUST14").asDouble());
			rec.setVolCust15(m_saCommand.Field("CUST15").asDouble());
			rec.setVolCust16(m_saCommand.Field("CUST16").asDouble());
			rec.setVolCust17(m_saCommand.Field("CUST17").asDouble());
			rec.setVolCust18(m_saCommand.Field("CUST18").asDouble());
			rec.setVolCust19(m_saCommand.Field("CUST19").asDouble());
			rec.setVolCust20(m_saCommand.Field("CUST20").asDouble());
			
			rec.setUTopDia(m_saCommand.Field("UDIAT").asDouble());
			rec.setUTopDia2(m_saCommand.Field("UDIAT2").asDouble());
			rec.setUVol(m_saCommand.Field("UVOL").asDouble());
			rec.setURootDia(m_saCommand.Field("UDIAR").asDouble());
			rec.setURootDia2(m_saCommand.Field("UDIAR2").asDouble());
			rec.setULength(m_saCommand.Field("ULEN").asDouble());
			rec.setUPrice(m_saCommand.Field("UPRICE").asDouble());
	}


		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}

BOOL CDBHandling::isTicketInInventLogs(int pk_id)
{
	short nCnt = 0;
	CString sSQL = L"";
	BOOL bReturn = TRUE;
	try
	{
		sSQL.Format(_T("SELECT 1 FROM %s WHERE fkLoadID=%d"),TBL_INVENTORY_LOGS,pk_id);
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			nCnt++;
		}

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return (nCnt > 0);
}

BOOL CDBHandling::isSalesIDUnique(LPCTSTR sales_id,CInventory& rec)
{
	short nCnt = 0;
	CString sSQL = L"";
	BOOL bReturn = TRUE;
	try
	{
		sSQL.Format(_T("SELECT 1 FROM %s WHERE sID='%s' AND pkInventID<>%d ORDER BY pkInventID"),TBL_INVENTORY,sales_id,rec.getPKID());
		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			nCnt++;
		}

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return (nCnt == 0);
}

BOOL CDBHandling::runSearchQuestion(vecSearch& vec,LPCTSTR sql,int meas_mode,int lang_set)
{
	CString sSQL = L"";
	BOOL bReturn = TRUE;
	CString sDateStored = L"",sTimeStored = L"",sDateUpd = L"",sTimeUpd = L"";
	CString sWhere(sql),sWhere2 = L"";
	if (!sWhere.IsEmpty())
		sWhere2 += L" WHERE " + sWhere;
	try
	{
		vec.clear();
		sSQL.Format(_T("select logscaleTicketsNew.pkLoadID, logscaleLogsNew.pkLogID,logscaleTicketsNew.sTicket,logscaleLogsNew.sTagNumber,logscaleLogsNew.nStatusFlag,logscaleLogsNew.sSpcCode,logscaleLogsNew.sSpcName,")
								_T("logscaleLogsNew.sGrade,logscaleLogsNew.fVolPricelist,logscaleLogsNew.fLogPrice,logscaleLogsNew.sVolFuncName,logscaleLogsNew.fLengthMeas,")
								_T("CONVERT(nchar(10),logscaleLogsNew.dCreated,111) as 'C_YYYYMMDD',")
							  _T("CONVERT(nchar(8),logscaleLogsNew.dCreated,108) as 'C_DATE',")
								_T("CONVERT(nchar(10),logscaleLogsNew.dUpdated,111) as 'U_YYYYMMDD',")
							  _T("CONVERT(nchar(8),logscaleLogsNew.dUpdated,108) as 'U_DATE'")
								_T("from dbo.logscaleLogsNew ")
								_T("inner join dbo.logscaleTicketsNew on ")
								_T("logscaleTicketsNew.pkLoadID=logscaleLogsNew.fkLoadID ")
								_T(" %s "),sWhere2);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{
			sDateStored = (LPCTSTR)m_saCommand.Field("C_YYYYMMDD").asString();
			sDateUpd = (LPCTSTR)m_saCommand.Field("U_YYYYMMDD").asString();
			sDateStored.Replace(L"/",L"-");
			sDateUpd.Replace(L"/",L"-");
			sTimeStored = (LPCTSTR)m_saCommand.Field("C_DATE").asString();
			sTimeUpd = (LPCTSTR)m_saCommand.Field("U_DATE").asString();

			vec.push_back(CSearch(m_saCommand.Field("pkLoadID").asLong(),
														m_saCommand.Field("pkLogID").asLong(),
														(LPCTSTR)m_saCommand.Field("sTicket").asString(),
														(LPCTSTR)m_saCommand.Field("sTagNumber").asString(),
														m_saCommand.Field("nStatusFlag").asShort(),
														(LPCTSTR)m_saCommand.Field("sSpcCode").asString(),
														(LPCTSTR)m_saCommand.Field("sSpcName").asString(),
														(LPCTSTR)m_saCommand.Field("sGrade").asString(),
														m_saCommand.Field("fVolPricelist").asDouble(),
														m_saCommand.Field("fLogPrice").asDouble(),
														(LPCTSTR)m_saCommand.Field("sVolFuncName").asString(),
														m_saCommand.Field("fLengthMeas").asDouble(),
														sDateStored + L" " + sTimeStored,
														sDateUpd + L" " + sTimeUpd));

		}

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}


BOOL CDBHandling::runSearchQuestionSales(vecSearchSales& vec,LPCTSTR sql)
{
	CString sSQL = L"";
	BOOL bReturn = TRUE;
	CString sWhere(sql),sWhere2 = L"";
	if (!sWhere.IsEmpty())
		sWhere2 += L" WHERE " + sWhere;
	try
	{
		vec.clear();
		sSQL.Format(_T("select logscaleTicketsNew.pkLoadID, logscaleLogsNew.pkLogID,logscaleSalesInventNew.pkInventID,logscaleTicketsNew.sTicket,logscaleLogsNew.sTagNumber,logscaleSalesInventNew.sID ")
								_T("from dbo.logscaleLogsNew ")								
								_T("inner join dbo.logscaleTicketsNew on ")
								_T("logscaleTicketsNew.pkLoadID=logscaleLogsNew.fkLoadID ")							
								_T("inner join dbo.logscaleSalesInventLogsNew on  ")
								_T("logscaleSalesInventLogsNew.fkLoadID=logscaleLogsNew.fkLoadID AND ")
								_T("logscaleSalesInventLogsNew.fkLogID=logscaleLogsNew.pkLogID ")								
								_T("inner join dbo.logscaleSalesInventNew on  ")
								_T("logscaleSalesInventNew.pkInventID=logscaleSalesInventLogsNew.fkInventID ")
								_T(" %s "),sWhere2);

		m_saCommand.setCommandText((SAString)sSQL);
		m_saCommand.Execute();
		
		while(m_saCommand.FetchNext())
		{

			vec.push_back(CSearchSales(m_saCommand.Field("pkLoadID").asLong(),
																m_saCommand.Field("pkLogID").asLong(),
																m_saCommand.Field("pkInventID").asLong(),
																(LPCTSTR)m_saCommand.Field("sTicket").asString(),
																(LPCTSTR)m_saCommand.Field("sTagNumber").asString(),
																(LPCTSTR)m_saCommand.Field("sID").asString()));
		}

		doCommit();
	}
	catch(SAException &e)
	{
		doRollback();
		AfxMessageBox((LPCWSTR)e.ErrText().GetWideChars());
		return FALSE;
	}

	return TRUE;
}
