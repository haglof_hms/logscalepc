// SetupSpcQualDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SetupSpcQualDlg.h"

#include "ResLangFileReader.h"

#include "ReportClasses.h"

#include <algorithm>

// CSetupSpcQualDlg dialog

// Function to sort vecLanguages, makin' sure we get
// languages in ascending order; 081020 p�d
bool SortSpeciesData(CLogs& l1, CLogs& l2)
{
	return l1.getSpcCode() < l2.getSpcCode();
}

bool SortGradesData(CLogs& l1, CLogs& l2)
{
	return l1.getGrade() < l2.getGrade();
}


IMPLEMENT_DYNAMIC(CSetupSpcQualDlg, CXTResizeDialog)

BEGIN_MESSAGE_MAP(CSetupSpcQualDlg, CXTResizeDialog)
	ON_BN_CLICKED(IDOK, &CSetupSpcQualDlg::OnBnClickedOk)
END_MESSAGE_MAP()

CSetupSpcQualDlg::CSetupSpcQualDlg(CWnd* pParent /*=NULL*/)
	: CXTResizeDialog(CSetupSpcQualDlg::IDD, pParent),
		m_bInitialized(FALSE)
{

}

CSetupSpcQualDlg::~CSetupSpcQualDlg()
{
}

void CSetupSpcQualDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCorrectionDlg)
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	//}}AFX_DATA_MAP
}

BOOL CSetupSpcQualDlg::OnInitDialog()
{
	CXTResizeDialog::OnInitDialog();
	if (!m_bInitialized)
	{
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);


		setupReports();

		populateReports();
		m_bInitialized = TRUE;

	}

	return TRUE;
}

// CSetupSpcQualDlg message handlers

void CSetupSpcQualDlg::setupReports()
{
	CXTPReportColumn *pCol = NULL;
	RECT rect;
	GetClientRect(&rect);

	if (m_repSpecies.GetSafeHwnd() == 0)
	{

		// Create the sheet1 list box.
		if (!m_repSpecies.Create(this, ID_REPORT_SETUP_SPECIES, L"repSpecies_report"))
		{
			TRACE0( "Failed to create m_repSpecies.\n" );
			return;
		}
	}

	if (m_repGrades.GetSafeHwnd() == 0)
	{

		// Create the sheet1 list box.
		if (!m_repGrades.Create(this, ID_REPORT_SETUP_GRADES, L"repGrades_report"))
		{
			TRACE0( "Failed to create m_repGrades.\n" );
			return;
		}
	}

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			// Set dialog caption
			SetWindowText(xml.str(IDS_STRING4100));

			m_btnOK.SetWindowText(xml.str(IDS_STRING100));
			m_btnCancel.SetWindowText(xml.str(IDS_STRING101));

			if (m_repSpecies.GetSafeHwnd() != NULL)
			{

				m_repSpecies.ShowWindow( SW_NORMAL );

				// Species in file
				pCol = m_repSpecies.AddColumn(new CXTPReportColumn(COLUMN_0, xml.str(IDS_STRING4101), 100,FALSE,XTP_REPORT_NOICON,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Species selection
				pCol = m_repSpecies.AddColumn(new CXTPReportColumn(COLUMN_1, xml.str(IDS_STRING4102), 130,FALSE,XTP_REPORT_NOICON,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->GetEditOptions()->AddComboButton();

				m_repSpecies.GetReportHeader()->AllowColumnRemove(FALSE);
				m_repSpecies.SetMultipleSelection( FALSE );
				m_repSpecies.SetGridStyle( TRUE, xtpReportGridSolid );
				m_repSpecies.SetGridStyle( FALSE, xtpReportGridSmallDots );
				m_repSpecies.FocusSubItems(TRUE);
				m_repSpecies.AllowEdit(TRUE);
				m_repSpecies.GetPaintManager()->SetFixedRowHeight(FALSE);

			}	// if (m_repSpecies.GetSafeHwnd() != NULL)

			// Strings
	
			setResize(&m_repSpecies,2,5,rect.right/2-4,rect.bottom-40);

			if (m_repGrades.GetSafeHwnd() != NULL)
			{

				m_repGrades.ShowWindow( SW_NORMAL );

				// Grades in file
				pCol = m_repGrades.AddColumn(new CXTPReportColumn(COLUMN_0, xml.str(IDS_STRING4103), 100,FALSE,XTP_REPORT_NOICON,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Grades selection
				pCol = m_repGrades.AddColumn(new CXTPReportColumn(COLUMN_1, xml.str(IDS_STRING4104), 130,FALSE,XTP_REPORT_NOICON,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->GetEditOptions()->AddComboButton();

				m_repGrades.GetReportHeader()->AllowColumnRemove(FALSE);
				m_repGrades.SetMultipleSelection( FALSE );
				m_repGrades.SetGridStyle( TRUE, xtpReportGridSolid );
				m_repGrades.SetGridStyle( FALSE, xtpReportGridSmallDots );
				m_repGrades.FocusSubItems(TRUE);
				m_repGrades.AllowEdit(TRUE);
				m_repGrades.GetPaintManager()->SetFixedRowHeight(FALSE);

			}	// if (m_repGrades.GetSafeHwnd() != NULL)

			// Strings

			xml.clean();
			
			setResize(&m_repGrades,rect.right/2+4,5,rect.right-(rect.right/2-8),rect.bottom-40);
		}

	}
}


void CSetupSpcQualDlg::populateReports()
{
	CString sLastSpcCode = L"";
	CString sLastGrdCode = L"";
	CLogs rec = CLogs();
	if (m_vecTmpLogs.size() > 0)
	{
		std::sort(m_vecTmpLogs.begin(),m_vecTmpLogs.end(),SortSpeciesData);
		for (UINT i = 0;i < m_vecTmpLogs.size();i++)
		{
			rec = m_vecTmpLogs[i];
			if (rec.getFlag() == 1 || rec.getFlag() == 3)
			{
				if (sLastSpcCode.CompareNoCase(rec.getSpcCode()) != 0)
				{
					addSpeciesConstraits();
					m_repSpecies.AddRecord(new CSetupSpeciesReportRec(i,rec));
				}
				sLastSpcCode = rec.getSpcCode();
			}
		}
		m_repSpecies.Populate();
		m_repSpecies.UpdateWindow();

		std::sort(m_vecTmpLogs.begin(),m_vecTmpLogs.end(),SortGradesData);
		for (UINT i = 0;i < m_vecTmpLogs.size();i++)
		{
			rec = m_vecTmpLogs[i];
			if (rec.getFlag() == 2 || rec.getFlag() == 3)
			{
				if (sLastGrdCode.CompareNoCase(rec.getGrade()) != 0)
				{
					addGradesConstraits();
					m_repGrades.AddRecord(new CSetupGradesReportRec(i,rec));
				}
				sLastGrdCode = rec.getGrade();
			}
		}

		m_repGrades.Populate();
		m_repGrades.UpdateWindow();
	}
}

CString CSetupSpcQualDlg::getSpcCode(int spc_id)
{
	if (m_vecSpecies.size() > 0)
	{
		for (UINT i = 0;i < m_vecSpecies.size();i++)
		{
			if (m_vecSpecies[i].getSpcID() == spc_id)
			{
				return m_vecSpecies[i].getSpcCode();
			}
		}
	}
	return L"";
}

CString CSetupSpcQualDlg::getSpcName(int spc_id)
{
	if (m_vecSpecies.size() > 0)
	{
		for (UINT i = 0;i < m_vecSpecies.size();i++)
		{
			if (m_vecSpecies[i].getSpcID() == spc_id)
			{
				return m_vecSpecies[i].getSpcName();
			}
		}
	}
	return L"";
}

CString CSetupSpcQualDlg::getGradesCode(int grade_id)
{
	if (m_vecGrades.size() > 0)
	{
		for (UINT i = 0;i < m_vecGrades.size();i++)
		{
			if (m_vecGrades[i].getGradesID() == grade_id)
			{
				return m_vecGrades[i].getGradesCode();
			}
		}
	}
	return L"";
}

CString CSetupSpcQualDlg::getGradesName(int grade_id)
{
	if (m_vecGrades.size() > 0)
	{
		for (UINT i = 0;i < m_vecGrades.size();i++)
		{
			if (m_vecGrades[i].getGradesID() == grade_id)
			{
				return m_vecGrades[i].getGradesName();
			}
		}
	}
	return L"";
}

CString CSetupSpcQualDlg::getSpcCodeAndName(int spc_id)
{
	CString sTmp = L"";
	if (m_vecSpecies.size() > 0)
	{
		for (UINT i = 0;i < m_vecSpecies.size();i++)
		{
			if (m_vecSpecies[i].getSpcID() == spc_id)
			{
					if (!m_vecSpecies[i].getSpcName().IsEmpty())
						sTmp.Format(L"%s - %s",m_vecSpecies[i].getSpcCode(),m_vecSpecies[i].getSpcName());
					else
						sTmp.Format(L"%s",m_vecSpecies[i].getSpcCode());
				return sTmp;
			}
		}
	}
	return L"";
}

CString CSetupSpcQualDlg::getGradesCodeAndName(int grade_id)
{
	CString sTmp = L"";
	if (m_vecGrades.size() > 0)
	{
		for (UINT i = 0;i < m_vecGrades.size();i++)
		{
			if (m_vecGrades[i].getGradesID() == grade_id)
			{
				if (!m_vecGrades[i].getGradesName().IsEmpty())
					sTmp.Format(L"%s - %s",m_vecGrades[i].getGradesCode(),m_vecGrades[i].getGradesName());
				else
					sTmp.Format(L"%s",m_vecGrades[i].getGradesCode());
				return sTmp;
			}
		}
	}
	return L"";
}

void CSetupSpcQualDlg::addSpeciesConstraits()
{

	CString sTmp = L"";
	CXTPReportRecordItemConstraints *pCons = NULL;
	CXTPReportColumns *pColumns = m_repSpecies.GetColumns();
	CXTPReportRows *pRows = m_repSpecies.GetRows();
	if (pColumns != NULL)
	{
		CXTPReportColumn *pSpcCol = pColumns->Find(COLUMN_1);
		if (pSpcCol != NULL)
		{
			// Get constraints for Specie column and remove all items; 111129 p�d
			pCons = pSpcCol->GetEditOptions()->GetConstraints();
			if (pCons != NULL)
			{
				pCons->RemoveAll();
			}	// if (pCons != NULL)

			// If there's species, add to Specie column in report; 111129 p�d
			if (m_vecSpeciesInTicket.size() > 0)
			{
				for (UINT i = 0;i < m_vecSpeciesInTicket.size();i++)
				{
					pSpcCol->GetEditOptions()->AddConstraint(getSpcCodeAndName(m_vecSpeciesInTicket[i].getSpeciesID()),(int)i); //m_vecSpecies[i].getSpcID());
				}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
			}	// if (m_vecSpecies.size() > 0)

/*
			// If there's species, add to Specie column in report; 111129 p�d
			if (m_vecSpecies.size() > 0)
			{
				for (UINT i = 0;i < m_vecSpecies.size();i++)
				{
					if (!m_vecSpecies[i].getSpcName().IsEmpty())
						sTmp.Format(L"%s - %s",m_vecSpecies[i].getSpcCode(),m_vecSpecies[i].getSpcName());
					else
						sTmp.Format(L"%s",m_vecSpecies[i].getSpcCode());
					pSpcCol->GetEditOptions()->AddConstraint(sTmp,(int)i); //m_vecSpecies[i].getSpcID());
				}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
			}	// if (m_vecSpecies.size() > 0)
*/

		}	// if (pSpcCol != NULL)

	}	// if (pColumns != NULL)

}

void CSetupSpcQualDlg::addGradesConstraits()
{
	CString sTmp = L"";
	int nSpcID = -1;
	CXTPReportRecordItemConstraints *pCons = NULL;
	CXTPReportColumns *pColumns = m_repGrades.GetColumns();
	CXTPReportRows *pRows = m_repGrades.GetRows();
	if (pColumns != NULL)
	{
		CXTPReportColumn *pGradeCol = pColumns->Find(COLUMN_1);
		if (pGradeCol != NULL)
		{
			// Get constraints for Specie column and remove all items; 111129 p�d
			pCons = pGradeCol->GetEditOptions()->GetConstraints();
			if (pCons != NULL)
			{
				pCons->RemoveAll();
			}	// if (pCons != NULL)

			// If there's grades, add to Grades column in report; 111129 p�d
			if (m_vecPricelistInTicket.size() > 0)
			{
				nSpcID = m_vecPricelistInTicket[0].getSpeciesID();
				for (UINT i = 0;i < m_vecPricelistInTicket.size();i++)
				{
					if (nSpcID == m_vecPricelistInTicket[i].getSpeciesID())
					{
						pGradeCol->GetEditOptions()->AddConstraint(getGradesCodeAndName(m_vecPricelistInTicket[i].getGradesID()),(int)i); //m_vecGrades[i].getGradesID());
					}
				}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
			}	// if (m_vecSpecies.size() > 0)
/*
			// If there's grades, add to Grades column in report; 111129 p�d
			if (m_vecGrades.size() > 0)
			{
				for (UINT i = 0;i < m_vecGrades.size();i++)
				{
					if (!m_vecGrades[i].getGradesName().IsEmpty())
						sTmp.Format(L"%s - %s",m_vecGrades[i].getGradesCode(),m_vecGrades[i].getGradesName());
					else
						sTmp.Format(L"%s",m_vecGrades[i].getGradesCode());

					pGradeCol->GetEditOptions()->AddConstraint(sTmp,i); //m_vecGrades[i].getGradesID());
				}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
			}	// if (m_vecSpecies.size() > 0)
*/

		}	// if (pSpcCol != NULL)

	}	// if (pColumns != NULL)

}

void CSetupSpcQualDlg::setSpeciesInLogs(LPCTSTR spc_code_change_from,LPCTSTR spc_code_change_to,LPCTSTR spc_name_change_to)
{
	if (m_vecLogs.size() > 0)
	{
	
		for (UINT i = 0;i < m_vecLogs.size();i++)
		{
			if (m_vecLogs[i].getSpcCode().CompareNoCase(spc_code_change_from) == 0)
			{	
			
				m_vecLogs[i].setSpcCode(spc_code_change_to);
				m_vecLogs[i].setSpcName(spc_name_change_to);	
				m_vecLogs[i].setFlag(-1);
						
			}	// if (m_vecLogs[i].getSpcCode().CompareNoCase(spc_code_change_from) == 0)
				
		}	// for (UINT i = 0;i < m_vecLogs.size();i++)
		
	}	// if (m_vecLogs.size() > 0)
}

void CSetupSpcQualDlg::setGradesInLogs(LPCTSTR grade_code_change_from,LPCTSTR grade_code_change_to)
{
	if (m_vecLogs.size() > 0)
	{
	
		for (UINT i = 0;i < m_vecLogs.size();i++)
		{
			if (m_vecLogs[i].getGrade().CompareNoCase(grade_code_change_from) == 0)
			{	
			
				m_vecLogs[i].setGrade(grade_code_change_to);
				m_vecLogs[i].setFlag(-1);
						
			}	// if (m_vecLogs[i].getSpcCode().CompareNoCase(spc_code_change_from) == 0)
				
		}	// for (UINT i = 0;i < m_vecLogs.size();i++)
		
	}	// if (m_vecLogs.size() > 0)
}


void CSetupSpcQualDlg::OnBnClickedOk()
{
	int nIndex = -1;
	CString sCol0Str = L"";
	CSetupSpeciesReportRec *pSpcRec = NULL;
	CSetupGradesReportRec *pGradeRec = NULL;
	CXTPReportRows *pRows = NULL;
	CXTPReportColumn *pCol = NULL;


	pRows = m_repSpecies.GetRows();
	pCol = m_repSpecies.GetColumns()->Find(COLUMN_1);
	if (pRows != NULL && pCol != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pSpcRec = (CSetupSpeciesReportRec *)pRows->GetAt(i)->GetRecord();
			if (pSpcRec != NULL)
			{
				// Get constraints for Specie column and remove all items; 070509 p�d
				if (!pSpcRec->getColText(COLUMN_1).IsEmpty())
				{
					sCol0Str = pSpcRec->getColText(COLUMN_0);
					CXTPReportRecordItemConstraint *pCons = pCol->GetEditOptions()->FindConstraint(pSpcRec->getColText(COLUMN_1));
					if (pCons != NULL)
					{
						nIndex = (int)pCons->m_dwData;
						if (nIndex > -1 && nIndex < m_vecSpeciesInTicket.size())
						{
							//CString S;
							//S.Format(L"%s   %s",sCol0Str,m_vecSpecies[nIndex].getSpcName());
							//AfxMessageBox(S);
							setSpeciesInLogs(sCol0Str,getSpcCode(m_vecSpeciesInTicket[nIndex].getSpeciesID()),getSpcName(m_vecSpeciesInTicket[nIndex].getSpeciesID()));
						}
					}	// if (pCons != NULL)
				}	// if (!pSpcRec->getColText(COLUMN_1).IsEmpty())
			}	// if (pSpcRec != NULL)
		}	// for (int i = 0;i < pSpcRows->GetCount();i++)
	}	// if (pSpcRows != NULL && pSpcCol != NULL)


	pRows = m_repGrades.GetRows();
	pCol = m_repGrades.GetColumns()->Find(COLUMN_1);
	if (pRows != NULL && pCol != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pGradeRec = (CSetupGradesReportRec *)pRows->GetAt(i)->GetRecord();
			if (pGradeRec != NULL)
			{
				// Get constraints for Specie column and remove all items; 070509 p�d
				if (!pGradeRec->getColText(COLUMN_1).IsEmpty())
				{
					sCol0Str = pGradeRec->getColText(COLUMN_0);
					CXTPReportRecordItemConstraint *pCons = pCol->GetEditOptions()->FindConstraint(pGradeRec->getColText(COLUMN_1));
					if (pCons != NULL)
					{
						nIndex = (int)pCons->m_dwData;
						if (nIndex > -1 && nIndex < m_vecPricelistInTicket.size())
						{
							//CString S;
							//S.Format(L"%s   %s",sCol0Str,m_vecSpecies[nIndex].getSpcName());
							//AfxMessageBox(S);
							setGradesInLogs(sCol0Str,getGradesCode(m_vecPricelistInTicket[nIndex].getGradesID()));
						}
					}	// if (pCons != NULL)
				}	// if (!pSpcRec->getColText(COLUMN_1).IsEmpty())
			}	// if (pSpcRec != NULL)
		}	// for (int i = 0;i < pSpcRows->GetCount();i++)
	}	// if (pRows != NULL && pCol != NULL)

	OnOK();
}
