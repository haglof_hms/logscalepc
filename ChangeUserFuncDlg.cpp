// MonthcalDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ChangeUserFuncDlg.h"

#include "ResLangFileReader.h"

// CChangeUserFuncDlg dialog

IMPLEMENT_DYNAMIC(CChangeUserFuncDlg, CXTResizeDialog)


BEGIN_MESSAGE_MAP(CChangeUserFuncDlg, CXTResizeDialog)
	ON_BN_CLICKED(IDOK, &CChangeUserFuncDlg::OnBnClickedOk)
END_MESSAGE_MAP()

CChangeUserFuncDlg::CChangeUserFuncDlg(CWnd* pParent /*=NULL*/)
	: CXTResizeDialog(CChangeUserFuncDlg::IDD, pParent),
	m_bInitialized(FALSE),
	m_sLangFN(L""),
	m_nTicketID(-1)
{

}

CChangeUserFuncDlg::~CChangeUserFuncDlg()
{
}

void CChangeUserFuncDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL5_1, m_lbl5_1);
	DDX_Control(pDX, IDC_LIST5_1, m_list5_1);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	//}}AFX_DATA_MAP
}

BOOL CChangeUserFuncDlg::OnInitDialog()
{
	CXTResizeDialog::OnInitDialog();
	if (!m_bInitialized)
	{

		m_lbl5_1.SetBkColor(INFOBK);
		m_lbl5_1.SetTextColor(BLUE);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				SetWindowText(xml.str(IDS_STRING4900));
				m_lbl5_1.SetWindowTextW(xml.str(IDS_STRING4920) + L"\n" + xml.str(IDS_STRING4921));
				m_btnOK.SetWindowText(xml.str(IDS_STRING100));
				m_btnCancel.SetWindowText(xml.str(IDS_STRING101));

				m_list5_1.SetExtendedStyle(m_list5_1.GetStyle()|LVS_EX_CHECKBOXES|LVS_EX_GRIDLINES|LVS_EX_FULLROWSELECT);
				m_list5_1.InsertColumn(0,xml.str(IDS_STRING4901),LVCFMT_LEFT,40);
				m_list5_1.InsertColumn(1,xml.str(IDS_STRING4902),LVCFMT_LEFT,200);
				m_list5_1.InsertColumn(2,xml.str(IDS_STRING4903),LVCFMT_LEFT,120);
				m_list5_1.InsertColumn(3,xml.str(IDS_STRING4904),LVCFMT_LEFT,120);
				m_list5_1.InsertColumn(4,xml.str(IDS_STRING4905),LVCFMT_LEFT,120);
				// Add volume-functions to listctrl
				if (m_vecFuncDesc.size() > 0)
				{
					for (UINT i = 0;i < m_vecFuncDesc.size();i++)
					{
						if (m_vecFuncDesc[i].getFuncID() < 1000)
						{
							InsertRow(m_list5_1,i,false,5,L"",
								m_vecFuncDesc[i].getFullName(),
								m_vecFuncDesc[i].getAbbrevName(),
								m_vecFuncDesc[i].getBasis(),
								xml.str(IDS_STRING49050));
						}
						else
						{
							InsertRow(m_list5_1,i,false,5,L"",
								m_vecFuncDesc[i].getFullName(),
								m_vecFuncDesc[i].getAbbrevName(),
								m_vecFuncDesc[i].getBasis(),
								xml.str(IDS_STRING49051));
						}
						m_list5_1.SetItemData(i,(DWORD)m_vecFuncDesc[i].getFuncID());
						m_list5_1.SetCheck(i,isActive(m_vecFuncDesc[i].getFuncID()));
					}	
				}

				xml.clean();
			}
		}


		m_bInitialized = TRUE;
	}

	return TRUE;
}

// CChangeUserFuncDlg message handlers

BOOL CChangeUserFuncDlg::isActive(int func_id)
{
	if (m_vecSelectedVolFuncs.size() == 0)
		return FALSE;

	for (UINT i = 0;i < m_vecSelectedVolFuncs.size();i++)
	{
		if (m_vecSelectedVolFuncs[i].getFuncID() == func_id)
			return TRUE;
	}
	return FALSE;
}


void CChangeUserFuncDlg::getSelectedFunctionData(int func_id)
{
	for (UINT i = 0;i < m_vecFuncDesc.size();i++)
	{
		if (m_vecFuncDesc[i].getFuncID() == func_id)
		{
			m_vecReturnSelectedVolFuncs.push_back(CSelectedVolFuncs(-1,m_nTicketID,func_id,
																						m_vecFuncDesc[i].getFullName(),
																						m_vecFuncDesc[i].getAbbrevName(),
																						m_vecFuncDesc[i].getBasis()));
		}
	}
}

void CChangeUserFuncDlg::OnBnClickedOk()
{
	m_vecReturnSelectedVolFuncs.clear();
	for (int i1 = 0;i1 < m_list5_1.GetItemCount();i1++)
	{
		if (m_list5_1.GetCheck(i1))
			getSelectedFunctionData((int)m_list5_1.GetItemData(i1));
	}

	OnOK();
}
