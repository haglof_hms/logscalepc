// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently

#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // Exclude rarely-used stuff from Windows headers
#endif

#include "targetver.h"

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // some CString constructors will be explicit

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxole.h>         // MFC OLE classes
#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC Automation classes
#endif // _AFX_NO_OLE_SUPPORT

#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>                      // MFC ODBC database classes
#endif // _AFX_NO_DB_SUPPORT

#ifndef _AFX_NO_DAO_SUPPORT
#include <afxdao.h>                     // MFC DAO database classes
#endif // _AFX_NO_DAO_SUPPORT

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>                     // MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

/////////////////////////////////////////////////////////////////////////////////////////
// Define for locking/unlocking pricelist in Templates
#define _LOCK_PRICELIST_IN_TEMPLATES

/////////////////////////////////////////////////////////////////////////////////////////
// Define checking/not checking barkreductions set on Ticket
// when user selects another pricelist.
#define _CHECK_SPECIES_ON_TICKET

/////////////////////////////////////////////////////////////////////////////////////////
// Define check number of species in a contract based on number of species
// already entered in the ticket
#define _CHECK_CONTRACT_SPECIES

// Define check number of grades in a contract based on number of species
// already entered in the ticket
#define _CHECK_CONTRACT_GRADES

// Define autogenerate Ticketnumber 
#define _AUTOGENERATE_TICKET_NUM

// Define autogenerate Salesnumber 
#define _AUTOGENERATE_SALE_NUM

// Define ask to open EXCEL 
#define _OPEN_EXCEL_IN_TICKET
#define _OPEN_EXCEL_IN_SALE
#define _OPEN_EXCEL_IN_MATCH_TAG
#define _OPEN_EXCEL_IN_VOLUMETABLE

/////////////////////////////////////////////////////////////////////////////////////////
// Define set default barkreductions > 0.0 on creating speciestable
#define _SET_BARKREDUCTION_ON_SPECIES

/////////////////////////////////////////////////////////////////////////////////////////
// Define for ask twice on delete
//#define _ASK_TWICE_ON_DELETE_REGISTER
//#define _ASK_TWICE_ON_DELETE_SPC_GRADE
//#define _ASK_TWICE_ON_DELETE_SALE
//#define _ASK_TWICE_ON_DELETE_LOG_ON_SALE
#define _ASK_TWICE_ON_DELETE_TICKET
#define _ASK_TWICE_ON_DELETE_LOG_ON_TICKET

/////////////////////////////////////////////////////////////////////////////////////////
// Define force error correction on Quit
#define _FORCE_ERROR_CORRECTION_PRICELIST
#define _FORCE_ERROR_CORRECTION_CONTRACT
#define _FORCE_ERROR_CORRECTION_TICKET
#define _FORCE_ERROR_CORRECTION_SALE	// Able to create an Empty Sale

/////////////////////////////////////////////////////////////////////////////////////////
// Define do check of imported pricelist in contracty
#define _DO_PRICELIST_CHECK_ON_IMPORT_IN_CONTRACT

#define _DO_PRICELIST_CHECK_ON_IMPORT

#include <SQLAPI.h> // main SQLAPI++ header

#include <vector>
#include <map>

#define _USE_MATH_DEFINES
#include <math.h>

// XML handling
#import <msxml3.dll> //named_guids
#include <msxml2.h>

#if (_MSC_VER > 1310) // VS2005
#pragma comment(linker, "\"/manifestdependency:type='Win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='X86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

#include <XTToolkitPro.h> // Xtreme Toolkit MFC extensions

// ... in PAD_HMSFuncLib
#include "pad_hms_miscfunc.h"
#include "pad_xmlshelldatahandler.h"
// ... in PAD_DBTransactionLib
#include "DBBaseClass_SQLApi.h"	
#include "DBBaseClass_ADODirect.h"

#include "DerivedClasses.h"
#include "TransactionClasses.h"

#include "LogScaleVolTransactionClasses.h"	// 

// Used for Languagefile, registry entries etc.; 110426 P�D
const LPCTSTR PROGRAM_NAME						= _T("LogScalePC");				

#define MSG_IN_SUITE				 					(WM_USER + 10)		// This identifer's used to send messages internally
#define MSG_OPEN_SUITE_ARG						(WM_USER + 11)	// Used for messages sent from a Suite/usermodule

#define MSG_LOGSCALE_ARGUMENT					0x92500

#define ID_SHOWVIEW_MSG								0x8116

#define ID_OPEN_LOGS_TMPL							0x8117	// New ticket selected template
#define ID_OPEN_LOGS									0x8118	// Open logs
#define ID_OPEN_LOGS_FILE							0x8119	// Open logs from ticket-file
#define ID_OPEN_LOGS_SEARCH						0x8120	// Open ticket logs from search

#define ID_OPEN_NEW_SALE							0x8200	// New ticket selected template
#define ID_OPEN_SALE									0x8201	// New ticket selected template
#define ID_OPEN_SALE_TAG_FILE					0x8202	// New ticket selected template
#define ID_OPEN_SALE_SEARCH						0x8203	// Open sales logs from search

#define ID_SHOW_FIELD_SELECTION				0x8600

/////////////////////////////////////////////////////////////////////
#define USE_MODULE_COMMUNICATION

/////////////////////////////////////////////////////////////////////
// Control ID (ReportControls etc.)
#define ID_REPORT_TICKETS						0x9000
#define ID_REPORT_PRICELIST					0x9001
#define ID_REPORT_GRADES2						0x9002
#define ID_REPORT_CALCTYPES					0x9003
#define ID_REPORT_REGISTER					0x9004
#define ID_REPORT_LOGS							0x9005
#define ID_REPORT_CORRECTION_TICKET 0x9006
#define ID_REPORT_CORRECTION_TICKET2 0x9007
#define ID_REPORT_CORRECTION_LOGS		0x9008
#define ID_REPORT_TEMPLATES					0x9009
#define ID_REPORT_SPECIES						0x9010
#define ID_REPORT_GRADES						0x9011
#define ID_REPORT_BUYERS						0x9012
#define ID_REPORT_HAULERS						0x9013
#define ID_REPORT_LOCATIONS					0x9014
#define ID_REPORT_TRACTIDS					0x9015
#define ID_REPORT_SCALERS						0x9016
#define ID_REPORT_SOURCEIDS					0x9017
#define ID_REPORT_VENDORS						0x9018
#define ID_REPORT_SUPPLIERS					0x9019
#define ID_REPORT_DEFAULTS					0x9020
#define ID_REPORT_TMPL_EXPORT				0x9021
#define ID_REPORT_SPECIES2					0x9022
#define ID_REPORT_SETUP_SPECIES			0x9023
#define ID_REPORT_SETUP_GRADES			0x9024
#define ID_REPORT_SETUP_SPECIES2		0x9025
#define ID_REPORT_SETUP_GRADES2			0x9026
#define ID_REPORT_SETUP_SPECIES3		0x9027



#define ID_TABCONTROL								0x9050
#define ID_TABCONTROL1							0x9051
#define ID_TABCONTROL2							0x9052

// IDD_FILED_SELECTION
#define ID_TBBTN_SELECTION_DLG			0x9001
//#define ID_SHOW_FIELD_SELECTION			0x9002
#define ID_TBBTN_SELECTION_DLG2			0x9003
#define ID_SHOW_FIELD_SELECTION2		0x9004
#define ID_TBBTN_SELECTION_DLG3			0x9005
#define ID_SHOW_FIELD_SELECTION3		0x9006


// Tools menu
#define ID_TOOLS_SPECIES						0x9010
#define ID_TOOLS_GRADES							0x9011
#define ID_TOOLS_PRICELIST					0x9012
#define ID_TOOLS_REGISTER						0x9013
#define ID_TOOLS_DEFAULTS						0x9014

#define ID_TOOLS_IMPORT_EXCEL				0x9015
#define ID_TOOLS_EXPORT_EXCEL				0x9016

#define ID_EXPORT_EXCEL							0x9100
#define ID_EXPORT_TMPL							0x9101

#define ID_TOOLS_EXPORT_EXCEL2			0x9102	// Logs view
#define ID_TOOLS_PRICELIST2					0x9103	// Logs view
#define ID_TOOLS_TAG_MATCH					0x9104	// Logs view
#define ID_EXPORT_TMPL2							0x9105
#define ID_TOOLS_EXTRA_VFUNC2				0x9106	// Logs view
#define ID_TOOLS_BARK_REDUCTION2		0x9107	// Logs view
#define ID_TOOLS_CONTRACT						0x9108	// Logs view
#define ID_TOOLS_CHANGE_STATUS			0x9109	// Logs view

// Tools in Template view
#define ID_TMPL_TOOLS_UPD_PRL				0x9150	// Update/Change pricelist
#define ID_TMPL_TOOLS_EXPORT_TMPL		0x9151	// Update/Change pricelist
#define ID_TMPL_TOOLS_IMPORT_TMPL		0x9152	// Update/Change pricelist

#define IDC_TABCONTROL_UCREATEVTBLE	0x9200	// Create User volume tables

#define ID_GROUP_BYTHIS							0x9300
#define ID_SHOW_GROUPBOX						0x9301
#define ID_SHOW_FIELDCHOOSER				0x9302
#define ID_SHOW_FIELDCHOOSER1				0x9303
#define ID_SHOW_FIELDCHOOSER2				0x9304
#define ID_PREVIEW1									0x9305
#define ID_PREVIEW2									0x9306

/////////////////////////////////////////////////////////////////////
// String id
#define IDS_STRING99								99
#define IDS_STRING236								236
#define IDS_STRING240								240
#define IDS_STRING241								241
#define IDS_STRING242								242
#define IDS_STRING243								243
#define IDS_STRING244								244
#define IDS_STRING245								245
#define IDS_STRING246								246
#define IDS_STRING100								100
#define IDS_STRING101								101
#define IDS_STRING102								102
#define IDS_STRING103								103
#define IDS_STRING104								104
#define IDS_STRING105								105
#define IDS_STRING106								106
#define IDS_STRING107								107
#define IDS_STRING108								108
#define IDS_STRING109								109
#define IDS_STRING110								110
#define IDS_STRING111								111
#define IDS_STRING112								112
#define IDS_STRING113								113
#define IDS_STRING114								114
#define IDS_STRING115								115
#define IDS_STRING116								116
#define IDS_STRING117								117
#define IDS_STRING118								118
#define IDS_STRING119								119
#define IDS_STRING120								120
#define IDS_STRING121								121
#define IDS_STRING122								122
#define IDS_STRING123								123
#define IDS_STRING124								124
#define IDS_STRING125								125
#define IDS_STRING126								126
#define IDS_STRING127								127
#define IDS_STRING128								128

#define IDS_STRING200								200	// List of register items (Scaler,Hauler etc.)
#define IDS_STRING201								201	// List of load types (short logs,whole tree,mixed)
#define IDS_STRING202								202	// List of deduction types (percent,absolute)
#define IDS_STRING203								203	// List of log status (In Stock,Sold,Sawmill etc.)
#define IDS_STRING204								204	// List of type of volume-tables, user created
#define IDS_STRING205								205	// List of measuring modes
#define IDS_STRING206								206	// List of column-headline (first column), user-created volumetables
#define IDS_STRING207								207	// List of sarch as for e.g. lrngth

#define IDS_STRING10								10
#define IDS_STRING1000							1000
#define IDS_STRING1001							1001
#define IDS_STRING1002							1002
#define IDS_STRING1003							1003
#define IDS_STRING1004							1004
#define IDS_STRING1005							1005
#define IDS_STRING1006							1006
#define IDS_STRING1007							1007
#define IDS_STRING1008							1008
#define IDS_STRING1009							1009
#define IDS_STRING1010							1010
#define IDS_STRING1011							1011
#define IDS_STRING1012							1012
#define IDS_STRING1013							1013
#define IDS_STRING1014							1014
#define IDS_STRING1015							1015
#define IDS_STRING1016							1016
#define IDS_STRING1017							1017
#define IDS_STRING1018							1018
#define IDS_STRING1019							1019
#define IDS_STRING1020							1020
#define IDS_STRING1021							1021
#define IDS_STRING1022							1022
#define IDS_STRING1023							1023
#define IDS_STRING1024							1024
#define IDS_STRING1025							1025
#define IDS_STRING1026							1026
#define IDS_STRING1027							1027
#define IDS_STRING1028							1028
#define IDS_STRING1029							1029
#define IDS_STRING1030							1030
#define IDS_STRING1031							1031

#define IDS_STRING1100							1100
#define IDS_STRING1101							1101
#define IDS_STRING1102							1102
#define IDS_STRING1103							1103
#define IDS_STRING1104							1104
#define IDS_STRING1105							1105
#define IDS_STRING1106							1106
#define IDS_STRING1107							1107
#define IDS_STRING11070							11070
#define IDS_STRING11071							11071
#define IDS_STRING1108							1108
#define IDS_STRING1109							1109
#define IDS_STRING1110							1110
#define IDS_STRING1111							1111
#define IDS_STRING1112							1112
#define IDS_STRING1113							1113
#define IDS_STRING1114							1114
#define IDS_STRING1115							1115

#define IDS_STRING1116							1116
#define IDS_STRING1117							1117
#define IDS_STRING1118							1118

#define IDS_STRING1119							1119
#define IDS_STRING1120							1120
#define IDS_STRING1121							1121
#define IDS_STRING1122							1122
#define IDS_STRING1123							1123
#define IDS_STRING1124							1124
#define IDS_STRING1125							1125
#define IDS_STRING1126							1126
#define IDS_STRING1127							1127
#define IDS_STRING1128							1128
#define IDS_STRING1129							1129
#define IDS_STRING1130							1130
#define IDS_STRING1131							1131
#define IDS_STRING1132							1132
#define IDS_STRING1133							1133
#define IDS_STRING1134							1134
#define IDS_STRING1135							1135
#define IDS_STRING1136							1136
#define IDS_STRING1137							1137
#define IDS_STRING1138							1138
#define IDS_STRING1139							1139
#define IDS_STRING1140							1140
#define IDS_STRING1141							1141
#define IDS_STRING1142							1142
#define IDS_STRING1143							1143
#define IDS_STRING1144							1144
#define IDS_STRING1145							1145
#define IDS_STRING1146							1146
#define IDS_STRING1147							1147

// Species
#define IDS_STRING6104							6104	// Caption
#define IDS_STRING1200							1200
#define IDS_STRING1201							1201
#define IDS_STRING1202							1202
#define IDS_STRING1203							1203
#define IDS_STRING12030							12030
#define IDS_STRING1204							1204
#define IDS_STRING1205							1205
#define IDS_STRING1206							1206
#define IDS_STRING12060							12060
#define IDS_STRING1207							1207
#define IDS_STRING1208							1208
#define IDS_STRING1220							1220
#define IDS_STRING1221							1221
#define IDS_STRING1222							1222
#define IDS_STRING1230							1230
#define IDS_STRING1231							1231
#define IDS_STRING1232							1232

// Grades
#define IDS_STRING6105							6105	// Caption
#define IDS_STRING1250							1250
#define IDS_STRING1251							1251
#define IDS_STRING1252							1252
#define IDS_STRING1253							1253
#define IDS_STRING1254							1254
#define IDS_STRING12540							12540
#define IDS_STRING1255							1255
#define IDS_STRING1260							1260
#define IDS_STRING1270							1270
#define IDS_STRING1271							1271
#define IDS_STRING1272							1272

#define IDS_STRING1250							1250
#define IDS_STRING1251							1251
#define IDS_STRING1252							1252

#define IDS_STRING1253							1253
#define IDS_STRING1254							1254
#define IDS_STRING1255							1255
#define IDS_STRING1256							1256

// Pricelist dialog; change/update in Logs
#define IDS_STRING6106							6106	// Caption
#define IDS_STRING1300							1300
#define IDS_STRING1301							1301
#define IDS_STRING1302							1302
#define IDS_STRING1350							1350
#define IDS_STRING1351							1351

// SelectCalcType dialog
#define IDS_STRING1400							1400
#define IDS_STRING1401							1401
#define IDS_STRING1402							1402
#define IDS_STRING1403							1403

// Register
#define IDS_STRING6103							6103	// Caption
#define IDS_STRING1500							1500
#define IDS_STRING1501							1501
#define IDS_STRING1502							1502
#define IDS_STRING1503							1503
#define IDS_STRING1504							1504
#define IDS_STRING1505							1505
#define IDS_STRING1506							1506
#define IDS_STRING1507							1507
#define IDS_STRING1508							1508
#define IDS_STRING1509							1509
#define IDS_STRING1510							1510
#define IDS_STRING1511							1511
#define IDS_STRING1520							1520
#define IDS_STRING1521							1521
#define IDS_STRING1522							1522
#define IDS_STRING15300							15300
#define IDS_STRING15301							15301
#define IDS_STRING15302							15302
#define IDS_STRING15303							15303
#define IDS_STRING15304							15304
#define IDS_STRING15305							15305
#define IDS_STRING15306							15306
#define IDS_STRING15307							15307
#define IDS_STRING1531							1531
#define IDS_STRING1550							1550
#define IDS_STRING1551							1551
#define IDS_STRING1552							1552
#define IDS_STRING1553							1553
#define IDS_STRING1554							1554
#define IDS_STRING1555							1555
#define IDS_STRING1556							1556
#define IDS_STRING1557							1557
#define IDS_STRING1558							1558
#define IDS_STRING1559							1559

// Defaults
#define IDS_STRING6107							6107	// Caption
#define IDS_STRING1600							1600
#define IDS_STRING1601							1601
#define IDS_STRING1602							1602
#define IDS_STRING1603							1603
#define IDS_STRING1604							1604
#define IDS_STRING1605							1605
#define IDS_STRING1606							1606
#define IDS_STRING1607							1607
#define IDS_STRING16080							16080
#define IDS_STRING16081							16081
#define IDS_STRING1609							1609
#define IDS_STRING1610							1610
#define IDS_STRING1611							1611
#define IDS_STRING1612							1612
#define IDS_STRING1613							1613
#define IDS_STRING1650							1650
#define IDS_STRING1651							1651
#define IDS_STRING1652							1652
#define IDS_STRING1653							1653
#define IDS_STRING1654							1654
#define IDS_STRING1655							1655

// TicketLogs
#define IDS_STRING1700							1700
#define IDS_STRING1701							1701
#define IDS_STRING1702							1702
#define IDS_STRING1703							1703
#define IDS_STRING1704							1704
#define IDS_STRING1705							1705
#define IDS_STRING17050							17050
#define IDS_STRING1706							1706
#define IDS_STRING17060							17060
#define IDS_STRING1707							1707
#define IDS_STRING17070							17070
#define IDS_STRING1708							1708
#define IDS_STRING17080							17080
#define IDS_STRING17090							17090
#define IDS_STRING17091							17091
#define IDS_STRING1710							1710
#define IDS_STRING1711							1711
#define IDS_STRING1712							1712
#define IDS_STRING17120							17120
#define IDS_STRING1713							1713
#define IDS_STRING17130							17130
#define IDS_STRING17131							17131
#define IDS_STRING1714							1714
#define IDS_STRING1715							1715
#define IDS_STRING1716							1716
#define IDS_STRING17160							17160
#define IDS_STRING1717							1717
#define IDS_STRING1718							1718
#define IDS_STRING17180							17180
#define IDS_STRING1719							1719
#define IDS_STRING17190							17190
#define IDS_STRING1720							1720
#define IDS_STRING1721							1721
#define IDS_STRING1722							1722
#define IDS_STRING1723							1723
#define IDS_STRING1724							1724
#define IDS_STRING1725							1725
#define IDS_STRING1726							1726
#define IDS_STRING1727							1727
#define IDS_STRING17270							17270
#define IDS_STRING1728							1728
#define IDS_STRING17280							17280
#define IDS_STRING1729							1729
#define IDS_STRING17290							17290
#define IDS_STRING1730							1730
#define IDS_STRING17300							17300
#define IDS_STRING17310							17310
#define IDS_STRING17311							17311
#define IDS_STRING1732							1732
#define IDS_STRING1733							1733
#define IDS_STRING1734							1734
#define IDS_STRING1735							1735
#define IDS_STRING17350							17350
#define IDS_STRING17351							17351
#define IDS_STRING17352							17352
#define IDS_STRING1736							1736
#define IDS_STRING1737							1737
#define IDS_STRING1738							1738
#define IDS_STRING1739							1739
#define IDS_STRING17390							17390
#define IDS_STRING1740							1740
#define IDS_STRING1741							1741
#define IDS_STRING17410							17410
#define IDS_STRING1742							1742
#define IDS_STRING1743							1743
#define IDS_STRING1744							1744
#define IDS_STRING1745							1745
#define IDS_STRING1746							1746
#define IDS_STRING1747							1747
#define IDS_STRING1748							1748
#define IDS_STRING1720							1720


#define IDS_STRING1750							1750
#define IDS_STRING1751							1751
#define IDS_STRING1752							1752
#define IDS_STRING1753							1753
#define IDS_STRING1754							1754
#define IDS_STRING1755							1755
#define IDS_STRING1756							1756
#define IDS_STRING17560							17560
#define IDS_STRING17561							17561
#define IDS_STRING17562							17562
#define IDS_STRING17563							17563
#define IDS_STRING17564							17564

#define IDS_STRING1760							1760
#define IDS_STRING1761							1761
#define IDS_STRING1762							1762

#define IDS_STRING1763							1763
#define IDS_STRING17630							17630
#define IDS_STRING1764							1764
#define IDS_STRING17640							17640
#define IDS_STRING1765							1765
#define IDS_STRING17650							17650
#define IDS_STRING1766							1766
#define IDS_STRING1767							1767
#define IDS_STRING1768							1768
#define IDS_STRING1769							1769
#define IDS_STRING1770							1770
#define IDS_STRING1771							1771
#define IDS_STRING1772							1772
#define IDS_STRING1773							1773
#define IDS_STRING1774							1774
#define IDS_STRING1775							1775
#define IDS_STRING1776							1776
#define IDS_STRING1777							1777
#define IDS_STRING1778							1778
#define IDS_STRING1779							1779
#define IDS_STRING1780							1780
#define IDS_STRING1781							1781
#define IDS_STRING1782							1782
#define IDS_STRING1783							1783
#define IDS_STRING1784							1784
#define IDS_STRING1785							1785

// Corrction dialog
#define IDS_STRING6108							6108	// Caption
#define IDS_STRING1800							1800
#define IDS_STRING1801							1801
#define IDS_STRING1802							1802
#define IDS_STRING18030							18030
#define IDS_STRING18031							18031
#define IDS_STRING18032							18032
#define IDS_STRING18033							18033
#define IDS_STRING18034							18034
#define IDS_STRING1804							1804
#define IDS_STRING1805							1805
#define IDS_STRING1806							1806
#define IDS_STRING1807							1807
#define IDS_STRING1810							1810
#define IDS_STRING1811							1811
#define IDS_STRING1812							1812
#define IDS_STRING1813							1813
#define IDS_STRING1814							1814
#define IDS_STRING1815							1815
#define IDS_STRING1816							1816
#define IDS_STRING1817							1817

#define IDS_STRING1820							1820
#define IDS_STRING1821							1821

#define IDS_STRING1829							1829

#define IDS_STRING1830							1830
#define IDS_STRING1831							1831
#define IDS_STRING1832							1832
#define IDS_STRING1833							1833
#define IDS_STRING1834							1834
#define IDS_STRING1835							1835
#define IDS_STRING1836							1836
#define IDS_STRING1837							1837
#define IDS_STRING1838							1838
#define IDS_STRING1839							1839
#define IDS_STRING1840							1840
#define IDS_STRING1841							1841
#define IDS_STRING1842							1842
#define IDS_STRING18430							18430
#define IDS_STRING18431							18431
#define IDS_STRING18432							18432
#define IDS_STRING18433							18433
#define IDS_STRING18440							18440
#define IDS_STRING18441							18441
#define IDS_STRING18442							18442

#define IDS_STRING1900							1900
#define IDS_STRING1901							1901
#define IDS_STRING1902							1902

// Ticket dialog
#define IDS_STRING2000							2000
#define IDS_STRING2001							2001
#define IDS_STRING20010							20010
#define IDS_STRING2002							2002
#define IDS_STRING2003							2003
#define IDS_STRING2004							2004
#define IDS_STRING2005							2005
#define IDS_STRING2006							2006
#define IDS_STRING2007							2007
#define IDS_STRING2008							2008
#define IDS_STRING2009							2009
#define IDS_STRING2010							2010
#define IDS_STRING2011							2011
#define IDS_STRING2012							2012
#define IDS_STRING2013							2013
#define IDS_STRING2014							2014
#define IDS_STRING2015							2015
#define IDS_STRING2016							2016
#define IDS_STRING2017							2017
#define IDS_STRING2018							2018
#define IDS_STRING2019							2019
#define IDS_STRING2020							2020
#define IDS_STRING2021							2021
#define IDS_STRING2022							2022
#define IDS_STRING2023							2023
#define IDS_STRING2024							2024

#define IDS_STRING3000							3000
#define IDS_STRING3001							3001
#define IDS_STRING3002							3002
#define IDS_STRING3003							3003
#define IDS_STRING3004							3004
#define IDS_STRING3005							3005
#define IDS_STRING3020							3020
#define IDS_STRING30200							30200
#define IDS_STRING3021							3021
#define IDS_STRING3022							3022
#define IDS_STRING3023							3023
#define IDS_STRING3024							3024
#define IDS_STRING3025							3025
#define IDS_STRING3026							3026
#define IDS_STRING3027							3027
#define IDS_STRING3028							3028
#define IDS_STRING3029							3029
#define IDS_STRING3030							3030
#define IDS_STRING3031							3031
#define IDS_STRING3032							3032
#define IDS_STRING3033							3033
#define IDS_STRING3050							3050
#define IDS_STRING3051							3051

#define IDS_STRING3500							3500
#define IDS_STRING3501							3501
#define IDS_STRING3502							3502
#define IDS_STRING35020							35020
#define IDS_STRING35021							35021
#define IDS_STRING3503							3503
#define IDS_STRING3504							3504
#define IDS_STRING3505							3505
#define IDS_STRING3506							3506
#define IDS_STRING3507							3507
#define IDS_STRING3508							3508
#define IDS_STRING3509							3509
#define IDS_STRING3510							3510
#define IDS_STRING3511							3511
#define IDS_STRING3512							3512
#define IDS_STRING3513							3513
#define IDS_STRING3514							3514
#define IDS_STRING3550							3550
#define IDS_STRING3551							3551
#define IDS_STRING3552							3552
#define IDS_STRING3553							3553
#define IDS_STRING3554							3554
#define IDS_STRING3560							3560
#define IDS_STRING3570							3570
#define IDS_STRING3571							3571
#define IDS_STRING3580							3580
#define IDS_STRING3581							3581
#define IDS_STRING3590							3590
#define IDS_STRING3591							3591
#define IDS_STRING3592							3592
#define IDS_STRING3593							3593
#define IDS_STRING3594							3594
#define IDS_STRING3595							3595
#define IDS_STRING3596							3596
#define IDS_STRING3597							3597
#define IDS_STRING3598							3598
#define IDS_STRING3599							3599

#define IDS_STRING4000							4000
#define IDS_STRING4001							4001
#define IDS_STRING4002							4002
#define IDS_STRING4003							4003
#define IDS_STRING4004							4004
#define IDS_STRING4005							4005
#define IDS_STRING4006							4006
#define IDS_STRING4007							4007

#define IDS_STRING4100							4100
#define IDS_STRING4101							4101
#define IDS_STRING4102							4102
#define IDS_STRING4103							4103
#define IDS_STRING4104							4104

#define IDS_STRING4200							4200
#define IDS_STRING4201							4201
#define IDS_STRING4202							4202
#define IDS_STRING4203							4203
#define IDS_STRING4204							4204
#define IDS_STRING4205							4205
#define IDS_STRING4206							4206
#define IDS_STRING4207							4207
#define IDS_STRING4208							4208
#define IDS_STRING4209							4209
#define IDS_STRING4210							4210
#define IDS_STRING4211							4211
#define IDS_STRING4212							4212
#define IDS_STRING4213							4213

#define IDS_STRING4250							4250
#define IDS_STRING4251							4251
#define IDS_STRING4252							4252
#define IDS_STRING4253							4253
#define IDS_STRING4254							4254
#define IDS_STRING4255							4255
#define IDS_STRING4256							4256
#define IDS_STRING4257							4257
#define IDS_STRING4258							4258
#define IDS_STRING4259							4259
#define IDS_STRING4260							4260
#define IDS_STRING4261							4261
#define IDS_STRING4262							4262
#define IDS_STRING4263							4263
#define IDS_STRING4264							4264
#define IDS_STRING4265							4265
#define IDS_STRING4266							4266
#define IDS_STRING4267							4267
#define IDS_STRING4268							4268
#define IDS_STRING4269							4269
#define IDS_STRING4270							4270
#define IDS_STRING4271							4271
#define IDS_STRING4272							4272

#define IDS_STRING4300							4300
#define IDS_STRING4301							4301
#define IDS_STRING4302							4302
#define IDS_STRING4350							4350
#define IDS_STRING4351							4351
#define IDS_STRING4352							4352
#define IDS_STRING4353							4353
#define IDS_STRING4354							4354
#define IDS_STRING4360							4360

#define IDS_STRING44000							44000
#define IDS_STRING44001							44001
#define IDS_STRING44002							44002
#define IDS_STRING44003							44003
#define IDS_STRING44004							44004
#define IDS_STRING44005							44005
#define IDS_STRING44006							44006
#define IDS_STRING44007							44007
#define IDS_STRING44008							44008
#define IDS_STRING44009							44009
#define IDS_STRING44010							44010
#define IDS_STRING44011							44011
#define IDS_STRING44012							44012
#define IDS_STRING44013							44013
#define IDS_STRING44014							44014
#define IDS_STRING44015							44015
#define IDS_STRING4410							4410
#define IDS_STRING4411							4411
#define IDS_STRING4412							4412
#define IDS_STRING4413							4413
#define IDS_STRING4430							4430
#define IDS_STRING4431							4431
#define IDS_STRING4432							4432
#define IDS_STRING4450							4450
#define IDS_STRING4451							4451
#define IDS_STRING4452							4452
#define IDS_STRING4453							4453
#define IDS_STRING4454							4454
#define IDS_STRING44540							44540
#define IDS_STRING44541							44541
#define IDS_STRING44550							44550
#define IDS_STRING44551							44551
#define IDS_STRING44552							44552
#define IDS_STRING44553							44553
#define IDS_STRING44554							44554
#define IDS_STRING44555							44555
#define IDS_STRING44556							44556
#define IDS_STRING44557							44557
#define IDS_STRING4460							4460
#define IDS_STRING4461							4461
#define IDS_STRING4462							4462
#define IDS_STRING4463							4463
#define IDS_STRING4464							4464
#define IDS_STRING4465							4465
#define IDS_STRING4466							4466
#define IDS_STRING4467							4467
#define IDS_STRING4468							4468
#define IDS_STRING4469							4469
#define IDS_STRING4470							4470
#define IDS_STRING4471							4471
#define IDS_STRING4472							4472
#define IDS_STRING4473							4473
#define IDS_STRING4474							4474
#define IDS_STRING4475							4475

#define IDS_STRING4500							4500
#define IDS_STRING4501							4501
#define IDS_STRING4502							4502
#define IDS_STRING4503							4503
#define IDS_STRING4530							4530
#define IDS_STRING4531							4531
#define IDS_STRING4532							4532
#define IDS_STRING45550							45550
#define IDS_STRING45551							45551
#define IDS_STRING45552							45552
#define IDS_STRING45553							45553
#define IDS_STRING45554							45554
#define IDS_STRING45555							45555
#define IDS_STRING45556							45556
#define IDS_STRING45557							45557
#define IDS_STRING45558							45558
#define IDS_STRING45559							45559
#define IDS_STRING45560							45560
#define IDS_STRING45561							45561
#define IDS_STRING4560							4560
#define IDS_STRING4561							4561
#define IDS_STRING4562							4562
#define IDS_STRING4563							4563
#define IDS_STRING4564							4564
#define IDS_STRING4565							4565
#define IDS_STRING4566							4566
#define IDS_STRING4567							4567
#define IDS_STRING4568							4568
#define IDS_STRING4569							4569
#define IDS_STRING4570							4570
#define IDS_STRING4571							4571
#define IDS_STRING4572							4572
#define IDS_STRING4573							4573
#define IDS_STRING4574							4574
#define IDS_STRING4575							4575
#define IDS_STRING4576							4576
#define IDS_STRING4577							4577
#define IDS_STRING4578							4578
#define IDS_STRING4579							4579
#define IDS_STRING4580							4580

#define IDS_STRING4600							4600
#define IDS_STRING46001							46001
#define IDS_STRING4601							4601
#define IDS_STRING46011							46011
#define IDS_STRING4602							4602
#define IDS_STRING46021							46021

#define IDS_STRING4700							4700
#define IDS_STRING4701							4701
#define IDS_STRING4702							4702
#define IDS_STRING4703							4703
#define IDS_STRING4704							4704
#define IDS_STRING4720							4720
#define IDS_STRING4721							4721
#define IDS_STRING4722							4722
#define IDS_STRING4723							4723
#define IDS_STRING4724							4724
#define IDS_STRING4725							4725
#define IDS_STRING4726							4726
#define IDS_STRING4727							4727
#define IDS_STRING4728							4728

#define IDS_STRING4800							4800

#define IDS_STRING4900							4900
#define IDS_STRING4901							4901
#define IDS_STRING4902							4902
#define IDS_STRING4903							4903
#define IDS_STRING4904							4904
#define IDS_STRING4905							4905
#define IDS_STRING49050							49050
#define IDS_STRING49051							49051
#define IDS_STRING4920							4920
#define IDS_STRING4921							4921

#define IDS_STRING5000							5000
#define IDS_STRING5001							5001
#define IDS_STRING5002							5002
#define IDS_STRING5003							5003
#define IDS_STRING5020							5020

#define IDS_STRING5500							5500
#define IDS_STRING5501							5501
#define IDS_STRING5502							5502
#define IDS_STRING5503							5503

#define IDS_STRING5600							5600
#define IDS_STRING5601							5601
#define IDS_STRING5602							5602
#define IDS_STRING5603							5603
#define IDS_STRING5604							5604
#define IDS_STRING5605							5605
#define IDS_STRING5606							5606
#define IDS_STRING5610							5610
#define IDS_STRING5611							5611
#define IDS_STRING5612							5612
#define IDS_STRING5613							5613
#define IDS_STRING5620							5620
#define IDS_STRING5621							5621
#define IDS_STRING5630							5630
#define IDS_STRING5631							5631
#define IDS_STRING5632							5632
#define IDS_STRING5640							5640
#define IDS_STRING5641							5641
#define IDS_STRING5642							5642
#define IDS_STRING5643							5643
#define IDS_STRING5644							5644
#define IDS_STRING5645							5645
#define IDS_STRING5646							5646
#define IDS_STRING5647							5647

#define IDS_STRING5700							5700
#define IDS_STRING57010							57010
#define IDS_STRING57011							57011
#define IDS_STRING57012							57012
#define IDS_STRING57013							57013
#define IDS_STRING5702							5702
#define IDS_STRING5703							5703
#define IDS_STRING5704							5704
#define IDS_STRING5705							5705
#define IDS_STRING5706							5706
#define IDS_STRING5707							5707
#define IDS_STRING5708							5708
#define IDS_STRING5709							5709

#define IDS_STRING5800							5800
#define IDS_STRING5801							5801
#define IDS_STRING5810							5810
#define IDS_STRING5811							5811
#define IDS_STRING5812							5812
#define IDS_STRING5820							5820
#define IDS_STRING5821							5821
#define IDS_STRING5830							5830

#define IDS_STRING5900							5900

#define IDS_STRING5950							5950
#define IDS_STRING5951							5951
#define IDS_STRING5952							5952
#define IDS_STRING5953							5953
#define IDS_STRING5954							5954
#define IDS_STRING5955							5955
#define IDS_STRING5956							5956
#define IDS_STRING5957							5957
#define IDS_STRING5958							5958

#define IDS_STRING60000							60000
#define IDS_STRING60001							60001
#define IDS_STRING60002							60002
#define IDS_STRING60003							60003
#define IDS_STRING60004							60004
#define IDS_STRING60005							60005
#define IDS_STRING60006							60006
#define IDS_STRING60007							60007
#define IDS_STRING60008							60008
#define IDS_STRING60009							60009
#define IDS_STRING60010							60010
#define IDS_STRING60011							60011
#define IDS_STRING60012							60012
#define IDS_STRING60013							60013
#define IDS_STRING60014							60014
#define IDS_STRING60015							60015

#define IDS_STRING60100							60100
#define IDS_STRING60101							60101
#define IDS_STRING60102							60102
#define IDS_STRING60103							60103
#define IDS_STRING60104							60104
#define IDS_STRING60105							60105
#define IDS_STRING60106							60106
#define IDS_STRING60107							60107
#define IDS_STRING60108							60108
#define IDS_STRING60109							60109
#define IDS_STRING60110							60110
#define IDS_STRING60111							60111
#define IDS_STRING60112							60112
#define IDS_STRING60113							60113
#define IDS_STRING60114							60114
#define IDS_STRING60115							60115
#define IDS_STRING60116							60116
#define IDS_STRING60117							60117
#define IDS_STRING60118							60118
#define IDS_STRING60119							60119
#define IDS_STRING60120							60120
#define IDS_STRING60121							60121
#define IDS_STRING60122							60122
#define IDS_STRING60123							60123
#define IDS_STRING60124							60124
#define IDS_STRING60125							60125
#define IDS_STRING60126							60126
#define IDS_STRING60127							60127
#define IDS_STRING60128							60128
#define IDS_STRING60129							60129
#define IDS_STRING60130							60130
#define IDS_STRING60200							60200
#define IDS_STRING60201							60201
#define IDS_STRING60202							60202
#define IDS_STRING60203							60203
#define IDS_STRING60220							60220
#define IDS_STRING60221							60221
#define IDS_STRING60222							60222
#define IDS_STRING60250							60250
#define IDS_STRING60251							60251
#define IDS_STRING60252							60252
#define IDS_STRING60253							60253

#define IDS_STRING61300							61300
#define IDS_STRING61301							61301
#define IDS_STRING61302							61302
#define IDS_STRING61303							61303
#define IDS_STRING61304							61304
#define IDS_STRING61305							61305
#define IDS_STRING61320							61320
#define IDS_STRING61330							61330
#define IDS_STRING61331							61331
#define IDS_STRING61332							61332
#define IDS_STRING61333							61333

#define IDS_STRING61400							61400
#define IDS_STRING61401							61401
#define IDS_STRING61402							61402
#define IDS_STRING61420							61420
#define IDS_STRING61430							61430
#define IDS_STRING61431							61431
#define IDS_STRING61432							61432


/////////////////////////////////////////////////////////////////////
const LPCTSTR TOOLBAR_RES_DLL			= _T("HMSIcons.icl");		// Resource dll, holds icons for e.g. toolbars; 091014 p�d

const int RES_TB_FILTER						= 39;
const int RES_TB_FILTER_OFF				= 8;
const int RES_TB_TOOLS						= 47;
const int RES_TB_PRINT						= 36;
const int RES_TB_REFRESH					= 43;
const int RES_TB_EXECUTE					= 46;
const int RES_TB_ADD							= 0;
const int RES_TB_INFO							= 20;
const int RES_TB_FOLDER						= 12;
const int RES_TB_PREVIEW					= 31;
const int RES_TB_UPDATE						= 43;
const int RES_TB_IMPORT						= 19;
const int RES_TB_EXPORT						= 9;
const int RES_TB_GIS							= 74;
const int RES_TB_CALIPER					= 4;
const int RES_TB_RED_X						= 2;
const int RES_TB_CHECKMARK				= 29;
const int RES_TB_SEARCH						= 72;

const int RES_TB_NEW							= 3;
const int RES_TB_UPD							= 31;
const int RES_TB_DEL							= 22;
const int RES_TB_DEL2							= 28;
const int RES_TB_SAVE							= 35;

const LPCTSTR RSTR_TB_FILTER							= _T("Tabellfilter");
const LPCTSTR RSTR_TB_FILTER_OFF					= _T("Endfilter");
const LPCTSTR RSTR_TB_TOOLS								= _T("Verktyg");
const LPCTSTR RSTR_TB_PRINT								= _T("Skrivut");
const LPCTSTR RSTR_TB_REFRESH							= _T("Utrop");
const LPCTSTR RSTR_TB_ADD									= _T("Add");			
const LPCTSTR RSTR_TB_DEL									= _T("Minus");		
const LPCTSTR RSTR_TB_INFO								= _T("Info");		
const LPCTSTR RSTR_TB_FOLDER							= _T("Folder");		
const LPCTSTR RSTR_TB_PREVIEW							= _T("Preview");		
const LPCTSTR RSTR_TB_UPDATE							= _T("Uppdatera");		
const LPCTSTR RSTR_TB_IMPORT							= _T("Import");		

/************************************************************
	Pars caliper-files 
************************************************************/

// Tokens in Header for "old" ticket-files
const int PARS_SOURCE_DATE = 1;
const int PARS_SOURCE_ID = 2;
const int PARS_SCALER = 3;
const int PARS_LOCATION = 4;
const int PARS_BUYER = 5;
const int PARS_OTHER_INFO = 6;
const int PARS_LOAD_ID = 7;
const int PARS_TICKET = 8;
const int PARS_TRACT_ID = 9;
const int PARS_HAULER = 10;
const int PARS_LOAD_TYPE = 11;
const int PARS_DATE = 12;
const int PARS_WEIGHT = 13;
const int PARS_NUMBER_OF_LOGS = 14;
const int PARS_LOAD_NOTE = 15;
const int PARS_SAWLOG_VOLUME_UNIT = 16;
const int PARS_PULPWOOD_VOLUME_UNIT = 17;
const int PARS_ROUND_SCALING = 18;
const int PARS_USE_SOUTHERN_DOYLE = 19;
const int PARS_DEDUCTION_TYPE = 20;
const int PARS_IB_TAPER = 21;
const int PARS_BARK_RATIO = 22;
const int PARS_MEASURING_MODE = 23;
const int PARS_JAS = 24;
const int PARS_LATITUDE = 25;
const int PARS_LONGITUDE = 26;
const int PARS_TEMPLATE_NAME = 27;
const int PARS_SUPPLIER = 28;
const int PARS_VENDOR = 29;
const int PARS_LOG_DATA = 50;

//////////////////////////////////////////////////////////////////////////////////////////
// Defines for minimum size of the Window
const int MIN_X_SIZE										= 670;
const int MIN_Y_SIZE										= 370;

const int MIN_X_SIZE2										= 50;
const int MIN_Y_SIZE2										= 50;

//////////////////////////////////////////////////////////////////////////////////////////

const LPCTSTR LICENSE_NAME						= _T("License");				// Used for Languagefile for License; 080901 p�d
const LPCTSTR LICENSE_FILE_NAME				= _T("License.dll");		// Used on setting up filename and path; 080901 p�d
const LPCTSTR LICENSE_GET_LIC					= _T("GetLicense");


//////////////////////////////////////////////////////////////////////////////////////////
// Registry settings; 060303 p�d
//const LPCTSTR REG_ROOT											= _T("SOFTWARE\\HaglofManagmentSystem");

const LPCTSTR REG_LOGSCALE_6001_KEY					= _T("LogScale\\FRAME6001\\Placement");
const LPCTSTR REG_LOGSCALE_6003_KEY					= _T("LogScale\\FRAME6003\\Placement");
const LPCTSTR REG_LOGSCALE_6004_KEY					= _T("LogScale\\FRAME6004\\Placement");
const LPCTSTR REG_LOGSCALE_6006_KEY					= _T("LogScale\\FRAME6006\\Placement");
const LPCTSTR REG_LOGSCALE_6500_KEY					= _T("LogScale\\FRAME6500\\Placement");
const LPCTSTR REG_LOGSCALE_6501_KEY					= _T("LogScale\\FRAME6501\\Placement");
const LPCTSTR REG_LOGSCALE_6502_KEY					= _T("LogScale\\FRAME6502\\Placement");
const LPCTSTR REG_LOGSCALE_6503_KEY					= _T("LogScale\\FRAME6503\\Placement");
const LPCTSTR REG_LOGSCALE_6504_KEY					= _T("LogScale\\FRAME6504\\Placement");
const LPCTSTR REG_LOGSCALE_6505_KEY					= _T("LogScale\\FRAME6505\\Placement");
const LPCTSTR REG_LOGSCALE_6506_KEY					= _T("LogScale\\FRAME6506\\Placement");
const LPCTSTR REG_LOGSCALE_6507_KEY					= _T("LogScale\\FRAME6507\\Placement");
const LPCTSTR REG_LOGSCALE_6508_KEY					= _T("LogScale\\FRAME6508\\Placement");
const LPCTSTR REG_LOGSCALE_6509_KEY					= _T("LogScale\\FRAME6509\\Placement");
const LPCTSTR REG_LOGSCALE_6510_KEY					= _T("LogScale\\FRAME6510\\Placement");
const LPCTSTR REG_LOGSCALE_6511_KEY					= _T("LogScale\\FRAME6511\\Placement");
const LPCTSTR REG_LOGSCALE_6512_KEY					= _T("LogScale\\FRAME6512\\Placement");
const LPCTSTR REG_LOGSCALE_6513_KEY					= _T("LogScale\\FRAME6513\\Placement");
const LPCTSTR REG_LOGSCALE_6514_KEY					= _T("LogScale\\FRAME6514\\Placement");
const LPCTSTR REG_LOGSCALE_6515_KEY					= _T("LogScale\\FRAME6515\\Placement");
const LPCTSTR REG_LOGSCALE_6523_KEY					= _T("LogScale\\FRAME6523\\Placement");

// REPORT STATE
const LPCTSTR REG_TICKETS_REPORT_KEY				= _T("LogScale\\Tickets\\Report");	// 111005 p�d
const LPCTSTR REG_LOG_SALE_REPORT_KEY				= _T("LogScale\\LogSales\\Report");	// 111005 p�d



// REPORT COLUMN SETTINGS
const LPCTSTR REG_TICKETS_COLUMNS_KEY				= _T("HMS\\LogScale\\Tickets\\ColumnSettings");	// 111108 p�d
const LPCTSTR REG_LOGS_COLUMNS_KEY					= _T("HMS\\LogScale\\ColumnSettings");			// 111108 p�d
const LPCTSTR REG_SALES_COLUMNS_KEY					= _T("HMS\\LogScale\\Sales\\ColumnSettings");		// 111108 p�d

// Placement for List...ReportViews
const LPCTSTR REG_WP_LIST_PRL_KEY						= _T("LogScale\\FRAMELIST_PRL\\Placement");
const LPCTSTR REG_WP_LIST_CONTRACT_KEY			= _T("LogScale\\FRAMELIST_CONTRACT\\Placement");
const LPCTSTR REG_WP_LIST_UVOL_TBL_KEY			= _T("LogScale\\FRAMELIST_UVOLTBL\\Placement");
const LPCTSTR REG_WP_STORAGE_SEARCH_KEY			= _T("LogScale\\FRAMELIST_STORAGE_SEARCH\\Placement");

// State for List...ReportViews
const LPCTSTR REG_WP_LIST_PRL_STATE_KEY			= _T("LogScale\\FRAMELIST_PRL\\Report");
const LPCTSTR REG_WP_LIST_CONTRACT_STATE_KEY = _T("LogScale\\FRAMELIST_CONTRACT\\Report");
const LPCTSTR REG_WP_LIST_USERVT_STATE_KEY = _T("LogScale\\FRAMELIST_USERVT\\Report");
const LPCTSTR REG_WP_STORAGE_SEARCH_STATE_KEY = _T("LogScale\\FRAMELIST_STORAGE_SEARCH\\Report");

// Directory save columnsettings for logs
const LPCTSTR LOGS_COLUMNS_DIRECTORY				= L"HMS\\LogScale\\Logs";
const LPCTSTR SALES_COLUMNS_DIRECTORY				= L"HMS\\LogScale\\Sales";

// Directory save search setting(s)
const LPCTSTR SEARCH_SETTINGS_DIRECTORY			= L"HMS\\LogScale\\Search";

// Temporary setting in registry for MeasuringMode
const LPCTSTR TICKET_MEASURINGMODE_KEY			= L"LogScale\\MeasMode\\Value";

//-------------------------------------------------------------------------------------
// Path for last directory used in CFileDialog
const LPCTSTR DIRECTORY_KEY									= L"Directory";

const LPCTSTR TICKET_IMPORT_DIRECTORY				= L"LogScale\\Directories\\TicketImport";
const LPCTSTR TICKET_EXPORT_TO_EXCEL_DIR		= L"LogScale\\Directories\\TicketExportExcel";

const LPCTSTR SALE_IMPORT_TAG_DIR						= L"LogScale\\Directories\\SaleImportTag";
const LPCTSTR SALE_EXPORT_TO_EXCEL_DIR		= L"LogScale\\Directories\\SaleExportExcel";
const LPCTSTR VTABLE_EXPORT_TO_EXCEL_DIR		= L"LogScale\\Directories\\VTableExportExcel";

// Save excel-file that holds Ticket-tags already in register
const LPCTSTR TICKET_MATCHING_DIR						= L"LogScale\\Directories\\TicketMatchTag";

// Select directory on save contract to diak
const LPCTSTR CONTRACT_SAVETO_DIR						= L"LogScale\\Directories\\ContractSaveTo";


//////////////////////////////////////////////////////////////////////////////////////////
// Tables in database
/*
const LPCTSTR TBL_TICKETS					= L"logscaleTickets";
const LPCTSTR TBL_TICKET_PRL			= L"logscaleTicketPrl";
const LPCTSTR TBL_LOGS						= L"logscaleLogs";
const LPCTSTR TBL_REGISTER				= L"logscaleRegister";
const LPCTSTR TBL_SPECIES					= L"logscaleSpecies";
const LPCTSTR TBL_GRADES					= L"logscaleGrades";
const LPCTSTR TBL_PRICELIST				= L"logscalePricelist";
const LPCTSTR TBL_CALCTYPES				= L"logscaleCalcTypes";
const LPCTSTR TBL_DEFAULTS				= L"logscaleDefaults";
*/
// �ndrat namn p� tabeller till logscale, s� att GAMLA tabeller
// finns kvar.
const LPCTSTR TBL_TICKETS					= L"logscaleTicketsNew";
const LPCTSTR TBL_TICKET_PRL			= L"logscaleTicketPrlNew";
const LPCTSTR TBL_TICKET_SPC			= L"logscaleTicketSpcNew";
const LPCTSTR TBL_LOGS						= L"logscaleLogsNew";
const LPCTSTR TBL_REGISTER				= L"logscaleRegisterNew";
const LPCTSTR TBL_SPECIES					= L"logscaleSpeciesNew";
const LPCTSTR TBL_GRADES					= L"logscaleGradesNew";
const LPCTSTR TBL_PRICELIST				= L"logscalePricelistNew";
const LPCTSTR TBL_CALCTYPES				= L"logscaleCalcTypesNew";	// Kommer ist�llet fr�n VolymDLL och Userdefined volumetables
const LPCTSTR TBL_DEFAULTS				= L"logscaleDefaultsNew";
const LPCTSTR TBL_USER_VOL_TABLES	= L"logscaleUserVolTablesNew";
const LPCTSTR TBL_SEL_VOL_FUNCS		= L"logscaleSelVolFuncsNew";	// Selected volume-functions per ticket, besides function in Pricelist
const LPCTSTR TBL_TEMPLATES				= L"logscaleTemplatesNew";		// Templates
const LPCTSTR TBL_DEF_USER_VOL_SEL	= L"logscaleDefUserVolSelNew";		// Default volumefunctions selected

const LPCTSTR TBL_INVENTORY				= L"logscaleSalesInventNew";		// Main table for Sales etc
const LPCTSTR TBL_INVENTORY_LOGS	= L"logscaleSalesInventLogsNew";		// Holds info on wich logs are ex. sold. Kepp track of ticket and logs

const LPCTSTR GUID_CHECK  = L"{ABA59C2A-E959-4bd7-909F-7F22E83AA29A}";
const LPCTSTR HEADERSTRING_CHECK  = L"LogScaleDPtoLogScaleDTStandardExportFile";

const LPCTSTR GUID_CHECK2  = L"{92E62E11-2DA2-4bb2-B261-27E3C1D0CB3A}";
const LPCTSTR HEADERSTRING_CHECK2  = L"LogScaleDPtoHMSStandardExportFile";

const LPCTSTR FILEVERSION1		= L"LogScale v1";
const LPCTSTR FILEVERSION2		= L"LogScale v2";

const LPCTSTR TEMPLATEHEADER = L"LogScaleDTtoLogScaleDPTemplateFile";
const LPCTSTR TEMPLATEGUID  = L"{BA952462-B1FF-4527-8713-D7331CA61B08}";

#define SETUPFILE "LOGSCALESET.SET"
#define FILEEXT "*.LGSC"
#define PRINTEXT ".txt"
#define PRINTLSDTEXT ".lsa"


const LPCTSTR TEMPLATE_TMP_SUBDIR	= L"LogScaleTmpl";

const LPCTSTR TEMPLATETEXT  = L".lst";
const LPCTSTR TEMPLATETEXT_FILTER  = L"*.lst";	// All template-files

#define MAXY 63
#define MAXX 127

#define ALPHASYMB "_ABCDEFGHIJKLMNOPQRSTU�VWXYZ����/:- 0123456789"
#define DIGISYMB "0123456789"
#define DIGISYMBSP "0123456789 "

#define DEF_FILENAME "LOAD 0001      "
#define DEF_DATE  "mm/dd/yy"
#define DEF_EMPTY "               "
#define DEF_SPECIE "~~~   "
#define DEF_GRADE  "~~~~"


#define DOUBLE_BARK_FACTOR	2.0	// R�kna dubbelt barkavdrag

//////////////////////////////////////////////////////////////////////////////////////////
// Convert Inch to mm and mm to Inch
#define Inch2MM	25.4
#define MM2Inch 1.0/Inch2MM
// Convert feet to cm and cm to feet
#define Feet2CM	30.48
#define CM2Feet	1.0/Feet2CM

//////////////////////////////////////////////////////////////////////////////////////////
// Volume indexes used in GetLogVolume(...)
const int VOL_INT14				=	0;
const int VOL_INT18				=	1;
const int VOL_DOYLE				=	2;	// Doyle; skapare av formel
//const int VOL_KNAUF				=	3;	// Knauf; skapare av formel
const int VOL_BRUCE_SHUM	=	3;	// Bruce Shumacher; skapare av formel
const int VOL_SCRIB_W			=	4;	// Scribner West-side; skapare av formel
//const int VOL_SCRIB_E			=	6;	// Scribner East-side; skapare av formel
const int VOL_JAS					=	5;	// JAS; skapare av formel
//const int VOL_SC16F_SEGM	=	8;	// Scribner 16 foot segmentation log roule
//const int VOL_DY16F_SEGM	=	9;	// Doyle 16 foot segmentation log roule
const int VOL_CFIB				=	6;	// Cubic feet, inside bark
const int VOL_CFIB_100		=	7;	// Cubic feet, inside bark divided by 100.0
const int VOL_CFOB				=	8;	// Cubic feet, on bark

//////////////////////////////////////////////////////////////////////////////////////////
// Defaults identifers
const int DEF_SAWLOG				= 1;
const int DEF_PULPWOOD			= 2;
const int DEF_DEDUCTION			= 3;
//const int DEF_SPC_CODE			= 4;
//const int DEF_LOG_GRADE			= 5;
const int DEF_LOAD_TYPE			= 6;
const int DEF_FREQUENCY			= 7;
const int DEF_LOG_LENGTH_INCH		= 80;
const int DEF_LOG_LENGTH_MM		= 81;
const int DEF_USE_S_DOYLE		= 9;	// Use Southern doyle
const int DEF_ROUND_SC_DIAM	= 10;	// Round sclaing diameter
const int DEF_POUNDS_CUBIC_FOOT	= 11;	// Pounds/Cubic foot
const int DEF_BARK_RATIO		= 12;	// Bark ratio
const int DEF_TAPER_IB			= 13;	// Round sclaing diameter

//////////////////////////////////////////////////////////////////////////////////////////
// Colors
#define CYAN					RGB(		0,  255,  255)
#define LTCYAN				RGB(  224,  255,  255)
#define BLACK					RGB(  0,  0,  0)
#define WHITE					RGB(255,255,255)
#define GRAY					RGB(192,192,192)
#define BLUE					RGB(0,0,255)
#define GREEN					RGB(0,255,0)
#define RED						RGB(255,0,0)
#define INFOBK				::GetSysColor(COLOR_INFOBK)
#define COL3DFACE			::GetSysColor(COLOR_3DFACE)

// Name of module handling Calculations for Height,Volume, Bark etc; 070416 p�d
const LPCTSTR MODULES_SUBDIR			= _T("Modules");					// All modules are in subdirs of the Suites directory; 051117 p�d

//////////////////////////////////////////////////////////////////////////////////////////
// Set maximuns
#define MAX_NUMBER_OF_GRADES			200
#define MAX_NUMBER_OF_SPECIES			200
#define MAX_NUMBER_OF_EXTRA_VFUNC	20

//////////////////////////////////////////////////////////////////////////////////////////
// Create database table from string; 110427 p�d
//////////////////////////////////////////////////////////////////////////////////////////
// Added 2011-10-31 p�d
class Scripts
{
	CString _TableName;
	CString _Script;
	CString _DBName;
public:
	enum actionTypes { TBL_CREATE,TBL_ALTER };

	Scripts(void)
	{
		_TableName = _T("");
		_Script = _T("");
		_DBName = _T("");
	}

	Scripts(LPCTSTR table_name,LPCTSTR script,LPCTSTR db_name)
	{
		_TableName = table_name;
		_Script = script;
		_DBName = db_name;
	}

	CString getTableName()	{ return _TableName; }
	CString getScript()			{ return _Script; }
	CString getDBName()			{ return _DBName; }
};

typedef std::vector<Scripts> vecScriptFiles;


//////////////////////////////////////////////////////////////////////////////////////////
// Class for column-settings set in registry. Manly used in ExcelExportDlg
class ColumnsSet
{
//private
	int m_nIndex;			// Sequenze
	int m_nColIndex;	// COLUMN_n
	CString m_sColName;
	int m_nSet;	

public:
	ColumnsSet(void)
	{
		m_nIndex = 0;
		m_nColIndex = 0;
		m_sColName = L"";
		m_nSet = 1;	
	}
	ColumnsSet(int idx,int col_idx,LPCTSTR col_name,int set)
	{
		m_nIndex = idx;
		m_nColIndex = col_idx;
		m_sColName = col_name;
		m_nSet = set;	
	}

	// GET
	int getIndex()	{ return m_nIndex; }
	int getColIndex()	{ return m_nColIndex; }
	CString getColName()	{ return m_sColName; }
	int getSet()	{ return m_nSet; }
};

typedef std::vector<ColumnsSet> vecColumnsSet;

BOOL runSQLScriptFileEx(LPCTSTR table_name,CString script,CString db_name);
// Added 2011-10-31 p�d
BOOL runSQLScriptFileEx2(vecScriptFiles &vec,Scripts::actionTypes action);

//////////////////////////////////////////////////////////////////////////////////////////
// XML Export
const LPCTSTR PRICELIST_IDENTIFICATION	= _T("LogScalePricelist");

const LPCTSTR TAG_FIRST_ROW						= _T("<?xml version=\"1.0\" encoding=\"utf-8\"?>");

const LPCTSTR TAG_START_PRICELIST			= _T("<LogScalePricelist>");
const LPCTSTR TAG_END_PRICELIST				= _T("</LogScalePricelist>");

const LPCTSTR TAG_START_HEADER				= _T("<Header>");
const LPCTSTR TAG_HEADER_NAME					= _T("<Name>%s</Name>");
const LPCTSTR TAG_HEADER_DONE_BY			= _T("<Done_by>%s</Done_by>");
const LPCTSTR TAG_HEADER_DATE					= _T("<Date>%s</Date>");
const LPCTSTR TAG_HEADER_NOTES				= _T("<Notes>%s</Notes>");
const LPCTSTR TAG_END_HEADER					= _T("</Header>");

const LPCTSTR TAG_SPECIES							= _T("<Species>%s</Species>");
const LPCTSTR TAG_DATA_FILE						= _T("<DataFile>%s</DataFile>");

//////////////////////////////////////////////////////////////////////////////////////////
// XML Import
const LPCTSTR TAG_GET_HEADER_NAME			= _T("//LogScalePricelist/Header/Name");
const LPCTSTR TAG_GET_HEADER_DONE_BY	= _T("//LogScalePricelist/Header/Done_by");
const LPCTSTR TAG_GET_HEADER_DATE			= _T("//LogScalePricelist/Header/Date");
const LPCTSTR TAG_GET_HEADER_NOTES		= _T("//LogScalePricelist/Header/Notes");
const LPCTSTR TAG_GET_DATA_FILE				= _T("//LogScalePricelist/DataFile");


//////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////
// XML Export
const LPCTSTR CONTRACT_IDENTIFICATION	= _T("LogScaleContract");

const LPCTSTR TAG_START_CONTRACT			= _T("<LogScaleContract>");
const LPCTSTR TAG_END_CONTRACT				= _T("</LogScaleContract>");

const LPCTSTR TAG_START_HEADER2				= _T("<Header>");
const LPCTSTR TAG_HEADER_NAME2				= _T("<Name>%s</Name>");
const LPCTSTR TAG_HEADER_DONE_BY2			= _T("<Done_by>%s</Done_by>");
const LPCTSTR TAG_HEADER_DATE2				= _T("<Date>%s</Date>");
const LPCTSTR TAG_HEADER_MODE2				= _T("<Mode>%d</Mode>");		// Measuringmode
const LPCTSTR TAG_HEADER_TRIM_FT2			= _T("<TrimFt>%.1f</TrimFt>");
const LPCTSTR TAG_HEADER_TRIM_CM2			= _T("<TrimCM>%.0f</TrimCM>");
const LPCTSTR TAG_HEADER_NOTES2				= _T("<Notes>%s</Notes>");
const LPCTSTR TAG_HEADER_PRL_NAME2		= _T("<PrlName>%s</PrlName>");
const LPCTSTR TAG_HEADER_DEF_SPCCODE2	= _T("<DefSpcCode>%s</DefSpcCode>");	// I relation till prislista i kontraktet
const LPCTSTR TAG_HEADER_DEF_GRDCODE2	= _T("<DefGrdCode>%s</DefGrdCode>");	// I relation till prislista i kontraktet
const LPCTSTR TAG_END_HEADER2					= _T("</Header>");


//////////////////////////////////////////////////////////////////////////////////////////
// XML Import
const LPCTSTR TAG_GET_HEADER_NAME2			= _T("//LogScaleContract/Header/Name");
const LPCTSTR TAG_GET_HEADER_DONE_BY2		= _T("//LogScaleContract/Header/Done_by");
const LPCTSTR TAG_GET_HEADER_DATE2			= _T("//LogScaleContract/Header/Date");
const LPCTSTR TAG_GET_HEADER_MODE2			= _T("//LogScaleContract/Header/Mode");
const LPCTSTR TAG_GET_HEADER_TRIM_FT2		= _T("//LogScaleContract/Header/TrimFt");
const LPCTSTR TAG_GET_HEADER_TRIM_CM2		= _T("//LogScaleContract/Header/TrimCM");
const LPCTSTR TAG_GET_HEADER_NOTES2			= _T("//LogScaleContract/Header/Notes");
const LPCTSTR TAG_GET_HEADER_PRL_NAME2	= _T("//LogScaleContract/Header/PrlName");
const LPCTSTR TAG_GET_HEADER_DEF_SPC2		= _T("//LogScaleContract/Header/DefSpcCode");
const LPCTSTR TAG_GET_HEADER_DEF_GRD2		= _T("//LogScaleContract/Header/DefGrdCode");
const LPCTSTR TAG_GET_DATA_FILE2				= _T("//LogScaleContract/DataFile");

//////////////////////////////////////////////////////////////////////////////////////////
// ENUMERATIONS
enum 
{
	COLUMN_0,
	COLUMN_1,
	COLUMN_2,
	COLUMN_3,
	COLUMN_4,
	COLUMN_5,
	COLUMN_6,
	COLUMN_7,
	COLUMN_8,
	COLUMN_9,
	COLUMN_10,
	COLUMN_11,
	COLUMN_12,
	COLUMN_13,
	COLUMN_14,
	COLUMN_15,
	COLUMN_16,
	COLUMN_17,
	COLUMN_18,
	COLUMN_19,
	COLUMN_20,
	COLUMN_21,
	COLUMN_22,
	COLUMN_23,
	COLUMN_24,
	COLUMN_25,
	COLUMN_26,
	COLUMN_27,
	COLUMN_28,
	COLUMN_29,
	COLUMN_30,
	COLUMN_31,
	COLUMN_32,
	COLUMN_33,
	COLUMN_34
};

// Log status
class LOGSTATUS
{
public:
	enum STATUS
	{
		NO_STATUS = -1,
		IN_STOCK = 0,	// Stocken ligger i lager
		SOLD = 1,			// Stocken �r s�ld
		IN_MILL = 2,	// Stocken han hanterats i s�gverket
		SCRAP = 3,			// Stocken �r kasserad
		LOCKED = 4			// Stocken �r l�st
	};
};

// Log origin
class LOGORIGIN
{
public:
	enum ORIGIN
	{
		NO_ORIGIN = -1,
		FROM_FILE = 0,				// Stocken inl�st fr�n en ticket-fil
		MANUALLY = 1,					// Stocket manuellt inl�st
		MANUALLY_ON_FILE = 2	// Added
	};
};

class COLUMNS
{
public:
	enum SALES
	{
		SALE_ID = 0,
		SALE_NAME = 1,
		SALE_NUMOF_LOGS = 2,
		SALE_SUM_PRICE = 3,
		SALE_BUYER = 4,
		SALE_CREATED_BY = 5,
		SALE_DATE = 6,
		SALE_HAULER = 7,
		SALE_SCALER = 8,
		SALE_SUPPLIER = 9,
		SALE_SOURCEID = 10,
		SALE_LOCATION = 11,
		SALE_TRCTID = 12,
		SALE_VENDOR = 13,
		SALE_GPS_COORD1 = 14,
		SALE_GPS_COORD2 = 15
	};
	// Enumerated columns for Logs reportcontrol
	enum TICKETS
	{
		TIC_TICKET_NUM	= 0,
		TIC_LOAD_ID = 1,
		TIC_SOURCE_DATE = 2,
		TIC_NUMOF_LOGS = 3,
		TIC_SUM_VOL_PRICELIST = 4,
		TIC_SUM_PRICE = 5,
		TIC_WEIGHT = 6,
		TIC_HAULER = 7,
		TIC_SUPPLIER = 8,
		TIC_VENDOR = 9,
		TIC_SCALER = 10,
		TIC_SOURCE_ID = 11,
		TIC_LOCATION = 12,
		TIC_LOAD_TYPE = 13,
		TIC_DEDUCTION = 14,
		TIC_BUYER = 15,
		TIC_TRACT_ID = 16,
		TIC_TAPER_IB = 17,
		TIC_OTHER_INFO = 18,
		TIC_GPS_COORD1 = 19,
		TIC_GPS_COORD2 = 20,
		TIC_PRICELIST = 21
	};
	// Enumerated columns for Logs reportcontrol
	enum LOGS
	{
		LOG_TAG_NUMBER = 0,
		LOG_SPC_CODE = 1,
		LOG_SPC_NAME = 2,
		LOG_STATUS_FLAG = 3,
		LOG_FREQUENCY = 4,
		LOG_GRADE = 5,
		LOG_DEDUCTION = 6,
		LOG_DOB_TOP	= 7,
		LOG_DOB_TOP2	= 8,
		LOG_DIB_TOP = 9,
		LOG_DIB_TOP2 = 10,
		LOG_DOB_ROOT = 11,
		LOG_DOB_ROOT2 = 12,
		LOG_DIB_ROOT = 13,
		LOG_DIB_ROOT2 = 14,
		LOG_LENGTH_MEAS = 15,
		LOG_LENGTH_CALC = 16,
		LOG_BARK = 17,
		LOG_VOL_PRICELIST = 18,
		LOG_PRICE	= 19,
		LOG_VOL_FUNC_NAME = 20,
		LOG_VOL_CUST_1	= 21,
		LOG_VOL_CUST_2	= 22,
		LOG_VOL_CUST_3	= 23,
		LOG_VOL_CUST_4	= 24,
		LOG_VOL_CUST_5	= 25,
		LOG_VOL_CUST_6	= 26,
		LOG_VOL_CUST_7	= 27,
		LOG_VOL_CUST_8	= 28,
		LOG_VOL_CUST_9	= 29,
		LOG_VOL_CUST_10	= 30,
		LOG_VOL_CUST_11	= 31,
		LOG_VOL_CUST_12	= 32,
		LOG_VOL_CUST_13	= 33,
		LOG_VOL_CUST_14	= 34,
		LOG_VOL_CUST_15	= 35,
		LOG_VOL_CUST_16	= 36,
		LOG_VOL_CUST_17	= 37,
		LOG_VOL_CUST_18	= 38,
		LOG_VOL_CUST_19	= 39,
		LOG_VOL_CUST_20	= 40,
		LOG_U_LARGE_DIAM	= 41,
		LOG_U_LARGE_DIAM2	= 42,
		LOG_U_VOL	= 43,
		LOG_U_SMALL_DIAM	= 44,
		LOG_U_SMALL_DIAM2	= 45,
		LOG_U_LENGTH = 46,
		LOG_U_PRICE = 47,
		LOG_REASON = 48,
		LOG_NOTES = 49
	};
};

class QUIT_TYPES
{
public:
	enum Q_T_RETURN { QUIT_ALL_OK, QUIT_ANYWAY, DO_NOT_QUIT, NO_SAVE, DO_SAVE };
};

class CHECK_SAVE_TYPES
{
public:
	enum CHECK_SAVE { CHECK_SAVE_ON_QUIT, CHECK_SAVE_NO_QUIT, CHECK_JUST_SAVE };
};

class REGISTER_TYPES
{
public:
	enum REGS
	{ 
		BUYER = 100,			// Set in logscaleRegister table
		HAULER = 101,		// Set in logscaleRegister table
		LOCATION = 102,	// Set in logscaleRegister table
		TRACTID = 103,		// Set in logscaleRegister table
		SCALER = 104,		// Set in logscaleRegister table
		SOURCEID = 105,	// Set in logscaleRegister table
		VENDOR = 106,		// Set in logscaleRegister table
		SUPPLIER = 107,		// Set in logscaleRegister table
		DEDUCTION = 120,	// Default list in LanguageFile
		LOAD_TYPE = 121  // Default list in LanguageFile
	};
};

//////////////////////////////////////////////////////////////////////////////////////////
// 
enum  ENUM_COL_SETTINGS_TYPES { COLSET_TICKET, COLSET_SALES };


//////////////////////////////////////////////////////////////////////////////////////////
// 
enum  ENUM_PRL_IMPORT_TYPES { PRL_IMP_ALL_OK, PRL_IMP_EXCHANGE, PRL_IMP_ADD };

//////////////////////////////////////////////////////////////////////////////////////////
// Define for species in General tab for Usercreated volumetables
#define USER_CREATED_VOL_TABLE_SPC -99

//////////////////////////////////////////////////////////////////////////////////////////
// Class to hold data on Columnsettings
class ColSet
{
public:
	int col1;
	int col2;
	TCHAR szCap[255];
	short visible;
};

//////////////////////////////////////////////////////////////////////////////////////////
// Derived class CMyExtEdit
class CMacroEdit : public CXTEdit
{
	CString m_sItemData;
	int nItems;
	char *valid_c;
public:
	CMacroEdit();
	virtual ~CMacroEdit();

	inline bool isCharValid(char c)
	{
		for (short i = 0;i < nItems;i++)
		{
			if (c == valid_c[i])
				return true;
		}
		return false;
	}

	inline void setValidChar(char **c,int num_of)
	{
		valid_c = (char*)calloc(num_of,sizeof(char));
		nItems = num_of;
		for (short i = 0;i < nItems;i++)
			valid_c[i] = *c[i];
	}

	inline CString getText()
	{
		GetWindowText(m_sItemData);
		return m_sItemData;
	}

protected:
	//{{AFX_MSG(CMyExtEdit2)
	afx_msg void OnUpdate();
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()


};

//////////////////////////////////////////////////////////////////////////////////////////
// map
typedef std::map<CString,int> CStrIntMap;

//////////////////////////////////////////////////////////////////////////////////////////
// Misc. functions
double StrToDbl(LPCTSTR str);
SAString convSADateTime(SADateTime &);
void LoadReportState(CMyReportControl &rep);
void SaveReportState(CMyReportControl &rep);
void LoadReportState(CXTPReportControl &rep,int id);
void SaveReportState(CXTPReportControl &rep,int id);
CView *showFormView(int idd,LPCTSTR lang_fn,LPARAM lp,int open_msg_id);
CView *getFormViewByID(int idd);
CWnd *getFormViewParentByID(int idd);
BOOL msgToModuleWindowOpen(LPCTSTR module,WPARAM wp,LPARAM lp = 0);
void setToolbarBtn(LPCTSTR resource_dll_fn,CXTPControl *pCtrl,int icon_id,LPCTSTR tool_tip,BOOL show);
CString getToolBarResourceFN(void);
void setupForDBConnection(HWND hWndReciv,HWND hWndSend);
BOOL hitTest_X(int hit_x,int limit_x,int w_x);
CString getDefaults(int def_value,CVecDefaults& vec,CVecCalcTypes& vec2);
int getDefaultFuncID(int def_value,CVecDefaults& vec);	//#4258 ny funktion
CString getDefaults(int def_value,CVecDefaults& vec,vecFuncDesc& vec1);
CString getRegisterValue(int reg_type,int reg_id,CVecRegister& vec);
int getRegisterID(int reg_type,LPCTSTR reg_value,CVecRegister& vec);
void tokenizeString(LPCTSTR str,TCHAR c,CStringArray& items);
void setReportRowFntData(CXTPReportRecordItem* item,COLORREF text_color = BLACK,COLORREF bgnd_color = WHITE,BOOL bold = FALSE,BOOL editable = TRUE);
BOOL getSTDReports(LPCTSTR fn,LPCTSTR add_to,int index,vecSTDReports &vec);
void tmplString(CStdioFile& f,int id,int index,LPCTSTR value,bool comma = true);
void tmplString(CStdioFile& f,int id,int index,int value,bool comma = true);
void tmplString(CStdioFile& f,LPCTSTR value,bool comma = true);
void tmplString(CStdioFile& f,double value,bool comma = true,int dec = 1);
CString tmplDblString(double value,bool comma = true,int dec = 0);
int numOfInRegister(REGISTER_TYPES::REGS item,CVecRegister& vec);

// Static variable, check if user's opened a window (doesn't matter which).
// Only show "Demo - number of days"-message once.
static BOOL bShowLicenseDialog = TRUE;
BOOL License(void);

CString localeToStr(LPCTSTR value);
CString localeToStr(double value);

void saveColumnsSettingToFile(LPCTSTR reg_key,CXTPReportControl &rep,ENUM_COL_SETTINGS_TYPES save_as);
void readColumnsSettingFromFile(LPCTSTR reg_key,vecColumnsSet &vec,ENUM_COL_SETTINGS_TYPES save_as);
BOOL delColumnsSettingFile(LPCTSTR reg_key,ENUM_COL_SETTINGS_TYPES save_as);

int InsertRow(CListCtrl &ctrl,int nPos,bool checked,int nNoOfCols, LPCTSTR pText, ...);
int GetSelectedItem(CListCtrl &ctrl);
int GetSelectedItemByCB(CListCtrl &ctrl);

void bkc(CXTPReportRecordItem *pRecItem,COLORREF bkcol,BOOL editable);

int createUniqueNumber(CVecUserVolTables &vec);

BOOL getIsUserVolFunctionUsed(LPCTSTR tmpl,int func_id);

///////////////////////////////////////////////////////////////////////////////////////////
// Check FUNC_INDEX::SPECIES (1001) if prl data
BOOL getIsSpeciesInTable(LPCTSTR prl,int spc_id);
BOOL getIsGradeInTable(LPCTSTR prl,int grade_id);
int getNumOfSpcInTable(LPCTSTR prl);
// Get prices for each grade and species
BOOL getPricesForSpeciesInTemplate(LPCTSTR prl,int spc_id,vecDouble& vec);
// get ALL data for pricelist set in template
BOOL getPricelistFromTemplate(LPCTSTR prl,int spc_id,vecPricelistInTicket& vec);
// get species set in template
BOOL getSpeciesFromTemplate(LPCTSTR prl,vecPricelistInTicket& vec);

///////////////////////////////////////////////////////////////////////////////////////////
// Check if item's included in Template
BOOL isIncludedInTemplate(int reg_type,int pk_id,vecLogScaleTemplates& vec);

///////////////////////////////////////////////////////////////////////////////////////////
// Setup combobox for e.g. Buyer,Hauler etc
void cbRegisterData(int reg_id,CComboBox* cb,CVecRegister& vec,int sel_id = -1,bool add_empty_line = true);
int cbGetID(CComboBox* cb);
// Get value from registry, based on index in ComboBox
int cbGetRegisterID(int reg_id,CComboBox* cb,CVecRegister& vec);
///////////////////////////////////////////////////////////////////////////////////////////
// Communicating with exported functions from LogScaleVolumeFunctions DLL; 121113 p�d
BOOL getDLLVolumeFuncDesc(vecFuncDesc &func_list,CVecUserVolTables user_vol_tables);
BOOL calculateDLLVolumeLogs(CLogs &rec,vecExtraVolFuncID &vec,CVecUserVolTables &vec1,double taper_ib,int use_southern_doyle,int measure_mode,int deduction,double trim);

// Use this on manually creted Tickets
CString generateTicketNumber(int num);
// Use this on manually creted Sales
CString generateSalesNumber(int num);

CString getFilePath(LPCTSTR path);

int getStatusFromTagFile(LPCTSTR path,int def_status);

CString getDateExt(short type);


//#3611
bool checkIllegalChars(LPCTSTR str);
