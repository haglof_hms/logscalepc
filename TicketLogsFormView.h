#pragma once

#include "Resource.h"

#include "DBHandling.h"
#include "DatePickerCombo.h"

///////////////////////////////////////////////////////////////////////////////////////////
// CTicketLogsDoc

class CTicketLogsDoc : public CDocument
{
protected: // create from serialization only
	CTicketLogsDoc();
	DECLARE_DYNCREATE(CTicketLogsDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTicketLogsDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTicketLogsDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CTicketLogsDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};



#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
/////////////////////////////////////////////////////////////////////////////
// class CTicketLogsFrame
class CTicketLogsFrame : public CChildFrameBase
{
//private:

	DECLARE_DYNCREATE(CTicketLogsFrame)
	CXTPStatusBar m_wndStatusBar;
	CXTPToolBar m_wndToolBar;
	CXTPDockingPaneManager m_paneManager;
	CString m_sLangFN;
	BOOL m_bInitReports;

	BOOL m_bFirstOpen;
	BOOL m_bIsPrintOutTBtn;
	BOOL m_bIsContractTBtn;
	BOOL m_bIsBarkReductionTBtn;
	BOOL m_bIsExtraVFuncTBtn;
	BOOL m_bIsExportToExcelTBtn;
	BOOL m_bIsChangeLogStatusTBtn;
//	CString m_sShellDataFile;
	int m_nShellDataIndex;
	short m_bReturn;

	CDialogBar m_wndFieldSelectionDlg;		// Sample Field chooser window

	vecSTDReports m_vecReports;
	CComboBox m_cboxPrintOut;

	BOOL m_bSysCommand;

	CFont *m_fnt1;

protected:
	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	HICON m_hIcon;
public:
	CTicketLogsFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

	void enableToolsMenu(BOOL enabled);

	BOOL isFirstOpen()		{ return m_bFirstOpen; }

// Attributes

// Operations

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	virtual ~CTicketLogsFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	
	//{{AFX_MSG(CTicketLogsFrame)
	afx_msg int OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg void OnDestroy(void);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnSysCommand(UINT nID,LPARAM lParam);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );

	afx_msg void OnAddLogs();
	afx_msg void OnDelLogs();
	afx_msg void OnUpdLogs();

	afx_msg void OnUpdatePrintOutTBtn(CCmdUI* pCmdUI);
	afx_msg void OnTBBtnPrintOut();
	afx_msg void OnPrintOutCBox();

	afx_msg void OnImportTicket(void);
	afx_msg void OnExportTicket(void);

	afx_msg void OnExportToExcel();
	afx_msg void OnUpdateExportToExcelTBtn(CCmdUI* pCmdUI);

	afx_msg void OnContract();
	afx_msg void OnUpdateContractTBtn(CCmdUI* pCmdUI);
	afx_msg void OnPricelist();
	afx_msg void OnUpdatePricelistTBtn(CCmdUI* pCmdUI);

	afx_msg void OnExtraVFunc();
	afx_msg void OnUpdateExtraVFuncTBtn(CCmdUI* pCmdUI);

	afx_msg void OnBarkReduction();
	afx_msg void OnUpdateBarkReductionTBtn(CCmdUI* pCmdUI);

	afx_msg void OnChangeLogStatus();
	afx_msg void OnUpdateChangeLogStatusTBtn(CCmdUI* pCmdUI);

	//}}AFX_MSG
	
	DECLARE_MESSAGE_MAP()
};

// CTicketLogsFormView form view

class CLogsReportView;

class CTicketLogsFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CTicketLogsFormView)

	// Data members
	BOOL m_bInitialized;
	CString m_sLangFN;

	CString m_sLblInch;
	CString m_sLblMM;
	CString m_sLblFeet;
	CString m_sLblDM;
	CString m_sLblBF;
	CString m_sLblDM3;
	CString m_sLblFT3;
	CString m_sTrimFT;
	CString m_sTrimCM;

	CStringArray m_sarrMeasuringMode;

	CString m_sMsgDelete1;
	CString m_sMsgDelete2;
	CString m_sMsgDataMissing;
	CString m_sMsgCloseAnyway;
	CString m_sMsgTickenumberErr;
	CString m_sMsgChangeUpdateContract1;
	CString m_sMsgChangeUpdateContract2;
	CString m_sMsgBarkReductionMissing;
	CString m_sMsgUpdatePricelist1;
	CString m_sMsgUpdatePricelist2;
	CString m_sMsgDupletTagNumber;
	CString m_sMsgDupletTagNumber2;
	CString m_sMsgMissingSpcInPricelist;
	CString m_sMsgMissingGradesInPricelist;
	CString m_sMsgDeleteLogFromTicket;
	CString m_sMsgDuplicateTagNumbers;
	CString m_sMsgStatusSoldNotChange;
	CString m_sMsgStatusSawmillNotChange;
	CString m_sMsgErrorMeasuringMode;
	CString m_sMsgErrorMeasuringMode2;

	CMyExtStatic m_lbl5_1;
	CMyExtStatic m_lbl5_2;
	CMyExtStatic m_lbl5_3;
	CMyExtStatic m_lbl5_4;
	CMyExtStatic m_lbl5_5;
	CMyExtStatic m_lbl5_6;
	CMyExtStatic m_lbl5_7;
	CMyExtStatic m_lbl5_8;
	CMyExtStatic m_lbl5_9;
	CMyExtStatic m_lbl5_10;
	CMyExtStatic m_lbl5_11;
	CMyExtStatic m_lbl5_12;
	//CMyExtStatic m_lbl5_13; 	Barkavdrag anv�nds inte
	CMyExtStatic m_lbl5_14;
	CMyExtStatic m_lbl5_15;
	CMyExtStatic m_lbl5_16;
	CMyExtStatic m_lbl5_17;
	CMyExtStatic m_lbl5_18;
	CMyExtStatic m_lbl5_19;
	CMyExtStatic m_lbl5_20;
	CMyExtStatic m_lbl5_21;
	CMyExtStatic m_lbl5_22;
	CMyExtStatic m_lbl5_23;
	CMyExtStatic m_lbl5_24;
	CMyExtStatic m_lbl5_25;
	
	CMyExtEdit m_edit5_1;
	CMyExtEdit m_edit5_2;
	CMyExtEdit m_edit5_3;
//	CMyExtEdit m_edit5_4; Barkavdrag anv�nds inte
	CMyExtEdit m_edit5_5;
	CMyExtEdit m_edit5_6;
	CMyExtEdit m_edit5_7;
	CMyExtEdit m_edit5_8;
	CMyExtEdit m_edit5_10;
	
	//CMyDatePickerCombo m_dt5_1;
	CDateTimeCtrl m_dtDate;
	CString m_csDate;
	CComboBox m_cbox5_1;
	CComboBox m_cbox5_2;
	CComboBox m_cbox5_3;
	CComboBox m_cbox5_4;
	CComboBox m_cbox5_5;
	CComboBox m_cbox5_6;
	CComboBox m_cbox5_7;
	CComboBox m_cbox5_8;
	CComboBox m_cbox5_9;
	CComboBox m_cbox5_10;
	CComboBox m_cbox5_12;

	CXTButton m_btnContract;
	CXTButton m_btnPricelist;

	CXTButton m_btnLock5_1;

	CButton m_check5_1;

	CVecPricelist m_vecPricelist;
	
	CVecTicketSpc m_vecTicketSpc;
	CVecTicketPrl m_vecTicketPrl;
	CVecSpecies m_vecSpecies;
	CVecGrades m_vecGrades;
	CVecCalcTypes m_vecCalcTypes;
	CLogScaleTemplates m_selectedTemplate;
	vecLogScaleTemplates m_vecLogScaleTemplates;

	vecFuncDesc m_vecFuncDesc;
	CVecUserVolTables m_vecUserVolTables;

	vecDefUserVolFuncs m_vecDefUserVolFuncs;

	vecSelectedVolFuncs m_vecSelectedVolFuncs;

	CVecRegister m_vecRegister;
	CVecDefaults m_vecDefaults;	// Maybe'll set per ticket

	CVecTickets m_vecTickets;
	CVecLogs m_vecLogs;
	CTickets m_recTicket;
	CLogs m_recLogsTotal;
	CVecLogs m_vecAllLogs;
	CStringArray m_sarrHeadLines;

	CDBHandling *m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;

	CMyTabControl m_wndTabControl;
	CXTPTabManagerItem *m_tabManager;

	CStringArray m_sarrDeductionType;
	CStringArray m_sarrLoadType;

	BOOL m_bIsTicketNumberOK;

	CXTPReportSubListControl m_wndSubList;

	CStringArray m_sarrLogStatus;

	int m_nOpenLogsViewAs;

	// Methods

	void setEnable(BOOL enable);

	double getLogValue(BYTE nVolumeIndex,int spc_id,LPCTSTR grade_code, double dVolume);

	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon);

	void addConstraintSpecies();
	void addConstraintGrades();
	CString getLogStatus(int status_id);

	BOOL checkLogTagNumbers(LPCTSTR ticket,LPCTSTR ticket_file,CVecLogs &vec_in_ticket,CVecLogs &vec_logs_to_add_to_inventory);

//	BOOL checkLogTagNumbers(CVecLogs &vec_in_ticket);

	BOOL checkLogData(CVecLogs& vec,CTickets& rec,bool ticket_num_ok,LPCTSTR file_name);

	BOOL checkTicketExists(LPCTSTR ticket_num);

	BOOL checkBarkReduction();

	// Set Species,pricelist and extra volumwe-functions
	void setTicketDataPricelist();
	void setTicketDataExtraVFunc();
	void matchBarkReductionForSpeciesInTicket();

	CGrades getGradesData(int grades_id);
	CSpecies getSpeciesData(int spc_id);
	CFuncDesc getFuncData(int func_id);
	void setSpeciesIDOnLogs(void);

	double getTicketSpcBarkReduction(LPCTSTR spc_code);
	int getTicketPrlFunctionID(LPCTSTR spc_code,LPCTSTR grade_code);
	CString getTicketPrlFunction(LPCTSTR spc_code,LPCTSTR grade_code);
	//CLogScaleTemplates getTemplate(LPCTSTR tmpl_name);
	CLogScaleTemplates getTemplate(int tmpl_id);
	double getLengthTrim(double length,int func_id);


	void enableContactAndPricelistBtn(BOOL enable)
	{
		if (!enable)
		{
			m_btnContract.SetXButtonStyle(BS_XT_SEMIFLAT);
			m_btnPricelist.SetXButtonStyle(BS_XT_SEMIFLAT);
		}
		else
		{
			m_btnContract.SetXButtonStyle(BS_XT_WINXP_COMPAT);
			m_btnPricelist.SetXButtonStyle(BS_XT_WINXP_COMPAT);
		}
		m_btnContract.EnableWindow(enable);
		m_btnPricelist.EnableWindow(enable);
	}

	CLogsReportView *getLogsView(void);

protected:
	CTicketLogsFormView();           // protected constructor used by dynamic creation
	virtual ~CTicketLogsFormView();
	
public:
	enum { IDD = IDD_FORMVIEW5 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

	// Public methods
	CString m_sMsgIllegalChars;
	CString m_sMsgCap;
	CString speciesName(LPCTSTR spc_code);
	int speciesID(LPCTSTR spc_code);

	CTickets& getTicket()	{ return m_recTicket; }

	short newTicket(bool on_close = false);	// 1 = OK, 0=Not OK,-1 = Close anyway
	short updTicket(bool on_close = false);

	void populateLogsReport(bool set_row = false,int log_id = -1);

	void populateData(BOOL reload_data,BOOL save_logs);

	void addLogs();
	void delLogs();
	void updLogs();
	void ImportTicket(void);
	void ExportToEXCEL(void);
	void ExportTicket(void);

	void changeContract();
	void changePricelist();
	void changeExtraVolFunc();
	void changeBarkReduction();
	void changeLogStatus();

	BOOL isTicketNumberOK()		{ return m_bIsTicketNumberOK; }

	BOOL checkLogTagNumber(int row,LPCTSTR tag_num);
	BOOL checkLogTagNumber(CLogs& rec);

protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTicketLogsFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CTicketLogsFormView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnClose();
	afx_msg void OnDestroy(void);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam, LPARAM lParam);
	afx_msg void OnExportTicket(void);
	//{{AFX_MSG(CTicketLogsFormView)

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnChangeEdit51();
	afx_msg void OnBnClickedButton51();
	afx_msg void OnBnClickedButton52();
	afx_msg void OnBnClickedCheck51();
};

/////////////////////////////////////////////////////////////////////////////////////////////////
// CLogsReportView

class CLogsReportView : public  CXTPReportView
{
	DECLARE_DYNCREATE(CLogsReportView)

	// Data members
	BOOL m_bInitialized;
	CString m_sLangFN;
	CString m_sFieldSelection;
	CString m_sMsgCap;
	CString m_sTagNummer;
	CString m_sMsgRemoveLogs1;
	CString m_sMsgRemoveLogs2;
	CString m_sMsgLogsContinue1;
	CString m_sMsgLogsContinue2;

	CString m_sMsgErrorFrequens;
	CString m_sMsgErrorReduction;

	CVecPricelist m_vecPricelist;
	CVecTicketPrl m_vecTicketSpc;
	CVecTicketPrl m_vecTicketPrl;
	CVecCalcTypes m_vecCalcTypes;
	CVecDefaults m_vecDefaults;	// Maybe'll set per ticket
	CStringArray m_sarrDeductionType;
	CStringArray m_sarrLogStatus;

	vecSelectedVolFuncs m_vecSelectedVolFuncs;
	vecExtraVolFuncID m_vecExtraVolFuncID;
	CVecUserVolTables m_vecUserVolTables;
	CVecLogs m_vecNoDataLogs;

	CDBHandling *m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;

	// Methods
	BOOL setupReport(void);

	BOOL checkLog(CLogs& rec);

	int m_nColumnChanged;


protected:
	CLogsReportView();           // protected constructor used by dynamic creation
	virtual ~CLogsReportView();


public:

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;

#endif

	void SaveLog(BOOL recalc,BOOL check = TRUE);
	short SaveAllLogs(BOOL on_quit);

	void doSetupReport()	{ setupReport(); }
protected:
	//{{AFX_VIRTUAL(CLogsReportView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL
	
	//{{AFX_MSG(CLogsReportView)
	afx_msg void OnDestroy();
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg void OnReportValueChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);

	afx_msg void OnFieldSelection(void);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
