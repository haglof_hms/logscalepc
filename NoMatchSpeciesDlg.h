#pragma once

#include "Resource.h"

#include "DBHandling.h"


class CMatchedSpecies
{
	CString sNoMatchSpcCode;
	CString sNoMatchSpcName;
	int nChangeToSpcID;
	CString sChangeToSpcCode;
	CString sChangeToSpcName;
public:
	CMatchedSpecies()	
	{
		sNoMatchSpcCode = L"";
		sNoMatchSpcName = L"";
		nChangeToSpcID = -1;
		sChangeToSpcCode = L"";
		sChangeToSpcName = L"";
	}
	CMatchedSpecies(LPCTSTR nomatch_spccode,LPCTSTR nomatch_spcname,int changeto_spcid,LPCTSTR changeto_spccode,LPCTSTR changeto_spcname)	
	{
		sNoMatchSpcCode = nomatch_spccode;
		sNoMatchSpcName = nomatch_spcname;
		nChangeToSpcID = changeto_spcid;
		sChangeToSpcCode = changeto_spccode;
		sChangeToSpcName = changeto_spcname;
	}
	CMatchedSpecies(const CMatchedSpecies &c)	
	{
		*this = c;
	}
	// GET
	CString getNoMatchSpcCode()	const	{ return sNoMatchSpcCode; }
	CString getNoMatchSpcName()	const	{ return sNoMatchSpcName; }
	int getChangeToSpcID() const { return nChangeToSpcID; }
	CString getChangeToSpcCode()	const	{ return sChangeToSpcCode; }
	CString getChangeToSpcName()	const	{ return sChangeToSpcName; }
	// SET
	void setChangeToSpcID(int v) { nChangeToSpcID = v; }
	void setChangeToSpcCode(LPCTSTR v)	{ sChangeToSpcCode = v; }
	void setChangeToSpcName(LPCTSTR v)	{ sChangeToSpcName = v; }
};

typedef std::vector<CMatchedSpecies> CVecMatchedSpecies;

// CNoMatchSpeciesDlg dialog

class CNoMatchSpeciesDlg : public CXTResizeDialog
{
	DECLARE_DYNAMIC(CNoMatchSpeciesDlg)

	BOOL m_bInitialized;
	CString m_sLangFN;

	CString m_sConstraintAdd;
	CString m_sMissmatch;
	CString m_sAllOK;

	CString m_sData;

	CMyExtStatic m_lbl21_1;

	CButton m_btnOK;
	CButton m_btnCancel;

	CVecSpecies m_vecSpecies;
	CVecSpecies m_vecSpeciesFromTable;
	CVecMatchedSpecies m_vecChangeToSpecies;

	CMyReportControl m_repSpecies;

	BOOL m_bSpcSelected;

	CDBHandling *m_pDB;

	CString getSpcCodeAndName(int spc_id);

	int addSpeciesConstraits(CSpecies rec);

	void setupReport(void);
	void populateReport(void);
public:
	CNoMatchSpeciesDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CNoMatchSpeciesDlg();

	void setSpecies(CVecSpecies& vec)		{ m_vecSpecies = vec; }
	void setSpeciesFromtable(CVecSpecies& vec);
	
	void setDB(CDBHandling *db)	{ m_pDB = db; }

	void getChangeToSpecies(CVecMatchedSpecies &vec) { vec = m_vecChangeToSpecies; }

// Dialog Data
	enum { IDD = IDD_DIALOG21 };

protected:
	//{{AFX_VIRTUAL(CCorrectionDlg)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CNoMatchSpeciesDlg)
	afx_msg void OnReportSettingSpc(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	//{{AFX_MSG(CNoMatchSpeciesDlg)

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};
