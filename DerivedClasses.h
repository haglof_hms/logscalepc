#if !defined(__DERIVEDCLASSES_H__)
#define __DERIVEDCLASSES_H__

//////////////////////////////////////////////////////////////////////////
// CMyReportControl; derived from CXTPReportControl
class CMyReportControl : public CXTPReportControl
{
	DECLARE_DYNCREATE(CMyReportControl)
//private:	
	BOOL bIsDirty;
	CString sComponentName;
public:
	CMyReportControl();
	CMyReportControl(const CMyReportControl &);

	BOOL Create(CWnd* pParentWnd,UINT nID,LPCTSTR name,BOOL add_hscroll = FALSE,BOOL run_metrics = TRUE);

	CMyReportControl operator=(const CMyReportControl &c)
	{
		return c;
	}

	BOOL isDirty(void);

	void setIsDirty(BOOL val);

	inline CScrollBar *getVertScrollBar(void)	{	return GetScrollBarCtrl(SB_VERT);	}

	inline CString getComponentName()	{ return sComponentName; }


protected:
	//{{AFX_MSG(CMDIDBFormFrame)
	afx_msg void OnChar(UINT,UINT,UINT);
	afx_msg void OnKeyUp(UINT,UINT,UINT);
	afx_msg void OnLButtonUp(UINT,CPoint);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

//////////////////////////////////////////////////////////////////////////
// CMyControlPopup
class CMyControlPopup : public CXTPControlPopup
{
	DECLARE_XTP_CONTROL(CMyControlPopup)

	struct _menu_items
	{
		int m_nID;
		CString m_sText;

		_menu_items(int id,LPCTSTR text)
		{
			m_nID = id;
			m_sText = text;
		}
	};

	std::vector<_menu_items > vecMenuItems;
protected:
	virtual BOOL OnSetPopup(BOOL bPopup);
public:
	CMyControlPopup(void);
	void addMenuIDAndText(int id = -1,LPCTSTR text = _T(""));
};



#endif