#pragma once


#include "Resource.h"

// CSetupSpcQualDlg dialog

class CSetupSpcQualDlg : public CXTResizeDialog
{
	DECLARE_DYNAMIC(CSetupSpcQualDlg)

	BOOL m_bInitialized;
	CString m_sLangFN;

	CButton m_btnOK;
	CButton m_btnCancel;

	CMyReportControl m_repSpecies;
	CMyReportControl m_repGrades;

	CVecLogs m_vecLogs;
	CVecLogs m_vecTmpLogs;
	CVecSpecies m_vecSpecies;
	CVecGrades m_vecGrades;
	vecPricelistInTicket m_vecPricelistInTicket;
	vecPricelistInTicket m_vecSpeciesInTicket;

	void setupReports();

	void populateReports();

	void addSpeciesConstraits();
	void addGradesConstraits();

	void setSpeciesInLogs(LPCTSTR spc_code_change_from,LPCTSTR spc_code_change_to,LPCTSTR spc_name_change_to);
	void setGradesInLogs(LPCTSTR grade_code_change_from,LPCTSTR grade_code_change_to);

	CString getSpcCode(int spc_id);
	CString getSpcName(int spc_id);
	CString getGradesCode(int grade_id);
	CString getGradesName(int grade_id);

	CString getSpcCodeAndName(int spc_id);
	CString getGradesCodeAndName(int grade_id);
public:
	CSetupSpcQualDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSetupSpcQualDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG13 };

	void setLogs(CVecLogs &vec)	{ m_vecTmpLogs = vec; m_vecLogs = vec; }
	void setSpecies(CVecSpecies &vec)	{ m_vecSpecies = vec; }
	void setGrades(CVecGrades &vec)	{ m_vecGrades = vec; }
	CVecLogs& getLogs()	{ return m_vecLogs; }
	void setPricelistData(vecPricelistInTicket& vec) { m_vecPricelistInTicket = vec; }
	void setSpeciesData(vecPricelistInTicket& vec) { m_vecSpeciesInTicket = vec; }


protected:
	//{{AFX_VIRTUAL(CCorrectionDlg)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	//}}AFX_VIRTUAL

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};
