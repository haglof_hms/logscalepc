// TicketDlg.cpp : implementation file
//

#include "stdafx.h"
#include "TicketDlg.h"

#include "ResLangFileReader.h"


// CTicketDlg dialog

IMPLEMENT_DYNAMIC(CTicketDlg, CXTResizeDialog)

BEGIN_MESSAGE_MAP(CTicketDlg, CXTResizeDialog)
	ON_BN_CLICKED(IDOK, &CTicketDlg::OnBnClickedOk)
END_MESSAGE_MAP()

CTicketDlg::CTicketDlg(CWnd* pParent /*=NULL*/)
	: CXTResizeDialog(CTicketDlg::IDD, pParent),
		m_bInitialized(FALSE),
		m_sLangFN(L"")

		, m_csDate(_T(""))
{

}

CTicketDlg::~CTicketDlg()
{
}

void CTicketDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTicketDlg)
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_LBL8_1, m_lbl8_1);
	DDX_Control(pDX, IDC_LBL8_2, m_lbl8_2);
	DDX_Control(pDX, IDC_LBL8_3, m_lbl8_3);
	DDX_Control(pDX, IDC_LBL8_4, m_lbl8_4);
	DDX_Control(pDX, IDC_LBL8_5, m_lbl8_5);
	DDX_Control(pDX, IDC_LBL8_6, m_lbl8_6);
	DDX_Control(pDX, IDC_LBL8_7, m_lbl8_7);
	DDX_Control(pDX, IDC_LBL8_8, m_lbl8_8);
	DDX_Control(pDX, IDC_LBL8_9, m_lbl8_9);
	DDX_Control(pDX, IDC_LBL8_10, m_lbl8_10);
	DDX_Control(pDX, IDC_LBL8_11, m_lbl8_11);
	DDX_Control(pDX, IDC_LBL8_12, m_lbl8_12);
	DDX_Control(pDX, IDC_LBL8_13, m_lbl8_13);
	DDX_Control(pDX, IDC_LBL8_14, m_lbl8_14);
	DDX_Control(pDX, IDC_LBL8_15, m_lbl8_15);

	//DDX_Control(pDX, IDC_COMBO8_1, m_cbx8_1);
	DDX_Control(pDX, IDC_COMBO8_2, m_cbx8_2);
	DDX_Control(pDX, IDC_COMBO8_3, m_cbx8_3);
	DDX_Control(pDX, IDC_COMBO8_4, m_cbx8_4);
	DDX_Control(pDX, IDC_COMBO8_5, m_cbx8_5);
	DDX_Control(pDX, IDC_COMBO8_6, m_cbx8_6);
	DDX_Control(pDX, IDC_COMBO8_7, m_cbx8_7);
	DDX_Control(pDX, IDC_COMBO8_8, m_cbx8_8);
	DDX_Control(pDX, IDC_COMBO8_9, m_cbx8_9);

	DDX_Control(pDX, IDC_EDIT8_1, m_ed8_1);
	DDX_Control(pDX, IDC_EDIT8_2, m_ed8_2);
	DDX_Control(pDX, IDC_EDIT8_3, m_ed8_3);
	DDX_Control(pDX, IDC_EDIT8_4, m_ed8_4);
	DDX_Control(pDX, IDC_EDIT8_5, m_ed8_5);
	DDX_Control(pDX, IDC_EDIT8_6, m_ed8_6);
	//}}AFX_DATA_MAP

	DDX_Control(pDX, IDC_DATE, m_dtDate);
	DDX_DateTimeCtrl(pDX, IDC_DATE, m_csDate);
}

BOOL CTicketDlg::OnInitDialog()
{
	CXTResizeDialog::OnInitDialog();
	if (!m_bInitialized)
	{

		m_ed8_3.SetAsNumeric();
		m_ed8_3.ModifyStyle(0,ES_RIGHT);

		m_ed8_5.SetAsNumeric();
		m_ed8_5.ModifyStyle(0,ES_RIGHT);

		m_ed8_6.SetAsNumeric();
		m_ed8_6.ModifyStyle(0,ES_RIGHT);

		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				m_sMsgCap = xml.str(IDS_STRING99);
				m_sMsgDataMissing = xml.str(IDS_STRING1125) + L"\n\n" + xml.str(IDS_STRING1126) + L"\n\n" + xml.str(IDS_STRING1127);

				SetWindowText(xml.str(IDS_STRING2000));

				m_lbl8_1.SetWindowTextW(xml.str(IDS_STRING2001));
				m_lbl8_2.SetWindowTextW(xml.str(IDS_STRING2002));
				m_lbl8_3.SetWindowTextW(xml.str(IDS_STRING2003));
				m_lbl8_4.SetWindowTextW(xml.str(IDS_STRING2004));
				m_lbl8_5.SetWindowTextW(xml.str(IDS_STRING2005));
				m_lbl8_6.SetWindowTextW(xml.str(IDS_STRING2006));
				m_lbl8_7.SetWindowTextW(xml.str(IDS_STRING2007));
				m_lbl8_8.SetWindowTextW(xml.str(IDS_STRING2008));
				m_lbl8_9.SetWindowTextW(xml.str(IDS_STRING2009));
				m_lbl8_10.SetWindowTextW(xml.str(IDS_STRING2010));
				m_lbl8_11.SetWindowTextW(xml.str(IDS_STRING2011));
				m_lbl8_12.SetWindowTextW(xml.str(IDS_STRING2012));
				m_lbl8_13.SetWindowTextW(xml.str(IDS_STRING2013));
				m_lbl8_14.SetWindowTextW(xml.str(IDS_STRING2014));
				m_lbl8_15.SetWindowTextW(xml.str(IDS_STRING2015));

				m_btnOK.SetWindowText(xml.str(IDS_STRING100));
				m_btnCancel.SetWindowText(xml.str(IDS_STRING101));


				tokenizeString(xml.str(IDS_STRING201),';',m_sarrLoadType);
				tokenizeString(xml.str(IDS_STRING202),';',m_sarrDeductionType);

			
				xml.clean();
			}
		}

		setCBoxdata();
		setData();

		m_bInitialized = TRUE;
	}

	return TRUE;
}

// CTicketDlg message handlers

void CTicketDlg::setCBoxdata()
{
	CRegister rec = CRegister();
	if (m_vecRegister.size() > 0)
	{
		for (UINT i = 0;i < m_vecRegister.size();i++)
		{
			rec = m_vecRegister[i];

				if (rec.getTypeID() == REGISTER_TYPES::HAULER) m_cbx8_2.AddString(rec.getName());
				else if (rec.getTypeID() == REGISTER_TYPES::SCALER) m_cbx8_3.AddString(rec.getName());
				else if (rec.getTypeID() == REGISTER_TYPES::SOURCEID) m_cbx8_4.AddString(rec.getName());
				else if (rec.getTypeID() == REGISTER_TYPES::LOCATION) m_cbx8_5.AddString(rec.getName());
				else if (rec.getTypeID() == REGISTER_TYPES::BUYER) m_cbx8_6.AddString(rec.getName());
				else if (rec.getTypeID() == REGISTER_TYPES::TRACTID) m_cbx8_7.AddString(rec.getName());
		}	// for (UINT i = 0;i < m_vecRegister.size();i++)
	}	// if (m_vecRegister.size() > 0)

	for (int i = 0;i < m_sarrLoadType.GetCount();i++)
	{
		m_cbx8_8.AddString(m_sarrLoadType.GetAt(i));
	}

	for (int i = 0;i < m_sarrDeductionType.GetCount();i++)
	{
		m_cbx8_9.AddString(m_sarrDeductionType.GetAt(i));
	}

}

void CTicketDlg::setData()
{
	if (m_recTicket.getPKID() > -1)
	{
		m_ed8_1.SetWindowTextW(m_recTicket.getTicket().Trim());
		m_ed8_2.SetWindowTextW(m_recTicket.getLoadID().Trim());
		m_ed8_3.setFloat(m_recTicket.getWeight(),0);
		m_ed8_4.SetWindowTextW(m_recTicket.getOtherInfo().Trim());
		m_ed8_5.setFloat(m_recTicket.getBarkRation(),1);
		m_ed8_6.setFloat(m_recTicket.getIBTaper(),3);

		m_csDate = m_recTicket.getDate().Trim();
		m_cbx8_2.SetCurSel(m_cbx8_2.FindString(0,m_recTicket.getHauler()));
		m_cbx8_3.SetCurSel(m_cbx8_3.FindString(0,m_recTicket.getScaler()));
		m_cbx8_4.SetCurSel(m_cbx8_4.FindString(0,m_recTicket.getSourceID()));
		m_cbx8_5.SetCurSel(m_cbx8_5.FindString(0,m_recTicket.getLocation()));
		m_cbx8_6.SetCurSel(m_cbx8_6.FindString(0,m_recTicket.getBuyer()));
		m_cbx8_7.SetCurSel(m_cbx8_7.FindString(0,m_recTicket.getTractID()));
		m_cbx8_8.SetCurSel(m_cbx8_8.FindString(0,m_recTicket.getLoadType()));
		m_cbx8_9.SetCurSel(m_cbx8_9.FindString(0,m_recTicket.getDeductionType()));

		UpdateData(FALSE);
	}
}

void CTicketDlg::OnBnClickedOk()
{
	UpdateData(TRUE);

	// Set data from entered
	m_recTicket.setTicket(m_ed8_1.getText());
	m_recTicket.setLoadID(m_ed8_2.getText());
	m_recTicket.setWeight(m_ed8_3.getFloat());
	m_recTicket.setOtherInfo(m_ed8_4.getText());
	m_recTicket.setBarkRation(m_ed8_5.getFloat());
	m_recTicket.setIBTaper(m_ed8_6.getFloat());

	CString sCBStr = L"";
	//m_cbx8_1.GetWindowTextW(sCBStr);
	m_recTicket.setDate(m_csDate);

	m_cbx8_2.GetWindowTextW(sCBStr);
	m_recTicket.setHauler(sCBStr);

	m_cbx8_3.GetWindowTextW(sCBStr);
	m_recTicket.setScaler(sCBStr);

	m_cbx8_4.GetWindowTextW(sCBStr);
	m_recTicket.setSourceID(sCBStr);

	m_cbx8_5.GetWindowTextW(sCBStr);
	m_recTicket.setLocation(sCBStr);

	m_cbx8_6.GetWindowTextW(sCBStr);
	m_recTicket.setBuyer(sCBStr);

	m_cbx8_7.GetWindowTextW(sCBStr);
	m_recTicket.setTractID(sCBStr);

	m_cbx8_8.GetWindowTextW(sCBStr);
	m_recTicket.setLoadType(sCBStr);

	m_cbx8_9.GetWindowTextW(sCBStr);
	m_recTicket.setDeductionType(sCBStr);

	// Check that data is added correctly
	
		if (m_recTicket.getTicket().IsEmpty() ||
			  m_recTicket.getLoadType().IsEmpty() ||
			  m_recTicket.getDeductionType().IsEmpty() ||
				m_recTicket.getBarkRation() == 0.0 ||
				m_recTicket.getIBTaper() == 0.0)
		{
			::MessageBox(GetSafeHwnd(),m_sMsgDataMissing,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
		}
		else
			OnOK();

}
