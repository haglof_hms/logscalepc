// GradesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "GradesDlg.h"

#include "ResLangFileReader.h"

#include "reportclasses.h"


///////////////////////////////////////////////////////////////////////////////////////////
// CGradesDlgDoc


IMPLEMENT_DYNCREATE(CGradesDlgDoc, CDocument)

BEGIN_MESSAGE_MAP(CGradesDlgDoc, CDocument)
	//{{AFX_MSG_MAP(CGradesDlgDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGradesDlgDoc construction/destruction

CGradesDlgDoc::CGradesDlgDoc()
{
	// TODO: add one-time construction code here

}

CGradesDlgDoc::~CGradesDlgDoc()
{
}


BOOL CGradesDlgDoc::OnNewDocument()
{

	// CHECK FOR LICENSE HERE!!!!! 2011-12-22 P�D
	if (!License())
	{
		return FALSE;
	}

	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CGradesDlgDoc serialization

void CGradesDlgDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CGradesDlgDoc diagnostics

#ifdef _DEBUG
void CGradesDlgDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CGradesDlgDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

///////////////////////////////////////////////////////////////////////////////////////////
// CGradesDlgFrame

IMPLEMENT_DYNCREATE(CGradesDlgFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CGradesDlgFrame, CMDIChildWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_SYSCOMMAND()
	//ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)

	ON_XTP_CREATECONTROL()
END_MESSAGE_MAP()


// CGradesDlgFrame construction/destruction

XTPDockingPanePaintTheme CGradesDlgFrame::m_themeCurrent = xtpPaneThemeOffice2003;

CGradesDlgFrame::CGradesDlgFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
	m_bInitReports = FALSE;
	m_bIsPrintOutTBtn = FALSE;
	m_bSysCommand = FALSE;

}

CGradesDlgFrame::~CGradesDlgFrame()
{
}

void CGradesDlgFrame::OnDestroy(void)
{
}

void CGradesDlgFrame::OnClose(void)
{
	// Send messages to HMSShell, disable buttons on toolbar; 120122 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 120122 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6504_KEY);
	SavePlacement(this, csBuf);

	if (!m_bSysCommand)
	{
		CGradesDlgView *pView = (CGradesDlgView*)getFormViewByID(IDD_FORMVIEW13);
		if (pView != NULL)
		{
			if (pView->doSave(FALSE))
			{
				setNavBarButtons();
				CMDIChildWnd::OnClose();
			}
		}
	}
	else
	{
		setNavBarButtons();
		CMDIChildWnd::OnClose();
	}

}

void CGradesDlgFrame::OnSysCommand(UINT nID,LPARAM lParam)
{
	if ((nID & 0xFFF0) == SC_CLOSE)
	{
		m_bSysCommand = TRUE;

		CGradesDlgView *pView = (CGradesDlgView*)getFormViewByID(IDD_FORMVIEW13);
		if (pView != NULL)
		{
			if (pView->doSave(FALSE))
			{
				setNavBarButtons();
				CMDIChildWnd::OnSysCommand(nID,lParam);
			}
		}
	}
	else if ((nID & 0xFFF0) != SC_CLOSE)
	{
		setNavBarButtons();
		CMDIChildWnd::OnSysCommand(nID,lParam);
	}
}

int CGradesDlgFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	EnableDocking(CBRS_ALIGN_ANY);

	// Create and Load toolbar; 090107 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR6);
	m_wndToolBar.EnableDocking(xtpFlagAlignTop|xtpFlagStretched);

	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			//***************************************************************************************
			// Setup commandbars and manues; 051114 p�d
			CXTPToolBar* pToolBar = &m_wndToolBar;
			if (pToolBar->IsBuiltIn())
			{
				if (pToolBar->GetType() != xtpBarTypeMenuBar)
				{
					UINT nBarID = pToolBar->GetBarID();
					pToolBar->LoadToolBar(nBarID, FALSE);
					CXTPControls *p = pToolBar->GetControls();
					// Setup icons on toolbars, using resource dll; 051208 p�d
					if (nBarID == IDR_TOOLBAR6)
					{		
						setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_NEW,xml.str(IDS_STRING1220),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(1),RES_TB_SAVE,xml.str(IDS_STRING1222),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(2),RES_TB_DEL,xml.str(IDS_STRING1221),TRUE);	//

						p->GetAt(3)->SetVisible(FALSE);
						p->GetAt(4)->SetVisible(FALSE);
						p->GetAt(5)->SetVisible(FALSE);
						p->GetAt(6)->SetVisible(FALSE);
						p->GetAt(7)->SetVisible(FALSE);
						p->GetAt(8)->SetVisible(FALSE);
					}	// if (nBarID == IDR_TOOLBAR5)
				}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
			}	// if (pToolBar->IsBuiltIn())
			//***************************************************************************************
			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

int CGradesDlgFrame::OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl)
{
	return TRUE;
}


LRESULT CGradesDlgFrame::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	return 0L;
}

BOOL CGradesDlgFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CGradesDlgFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CGradesDlgFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6504_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CGradesDlgFrame::OnSetFocus(CWnd* pWnd)
{
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,ID_DO_SOMETHING_IN_SHELL,ID_LPARAM_COMMAND2);

	setNavBarButtons();

	CMDIChildWnd::OnSetFocus(pWnd);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CGradesDlgFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}


// CGradesDlgFrame diagnostics

#ifdef _DEBUG
void CGradesDlgFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CGradesDlgFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CGradesDlgFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = 450;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CGradesDlgFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType,cx,cy);
}


// CGradesDlgView dialog

IMPLEMENT_DYNCREATE(CGradesDlgView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CGradesDlgView, CXTResizeFormView)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_WM_SIZE()
	ON_COMMAND_RANGE(ID_BUTTON32799,ID_BUTTON32806, OnCommand)
	ON_UPDATE_COMMAND_UI_RANGE(ID_BUTTON32799,ID_BUTTON32806, OnUpdateToolbar)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, ID_REPORT_GRADES, OnReportValueChanged)

END_MESSAGE_MAP()

CGradesDlgView::CGradesDlgView(CWnd* pParent /*=NULL*/)
	: CXTResizeFormView(CGradesDlgView::IDD),
		m_bInitialized(FALSE),
		m_sLangFN(L""),
		m_pDB(NULL),
		m_bEnableAdd(TRUE),
		m_bEnableDelete(TRUE),
		m_bEnableSave(TRUE)
{

}

CGradesDlgView::~CGradesDlgView()
{
}

void CGradesDlgView::OnDestroy()
{
	CXTResizeFormView::OnDestroy();
}

BOOL CGradesDlgView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}


void CGradesDlgView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	//}}AFX_DATA_MAP
}

int CGradesDlgView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}

void CGradesDlgView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();
	if (!m_bInitialized)
	{

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupReport();

		populateReport();

		m_bInitialized = TRUE;
	}
}

BOOL CGradesDlgView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{

	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		if (m_dbConnectionData.conn->isConnected())
		{
			m_pDB = new CDBHandling(m_dbConnectionData);

			if (m_pDB != NULL)
			{
				m_pDB->getPricelist(m_vecVecPricelist);
			}
		}
	}

	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CGradesDlgView::OnReportValueChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/)
{
	int nIdx = -1;
	CGradesReportRec *pRec = NULL;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{	
		nIdx = pItemNotify->pRow->GetIndex();
		m_repGrades.Populate();
		if ((pRec = (CGradesReportRec*)pItemNotify->pRow->GetRecord()) != NULL)
		{
//			if (m_pDB->isGradesCodeInDB(pRec->getColText(COLUMN_0)))
			if (CheckDuplicate(pRec->getColText(COLUMN_0),nIdx))
			{
				pItemNotify->pItem->SetTextColor(RED);
				::MessageBox(GetSafeHwnd(),m_sMsgGrades2,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
				//pRec->setColText(COLUMN_0,L"");
				pRec->setIsValid(false);

			}
			else
			{
				pItemNotify->pItem->SetTextColor(BLACK);
				pRec->setIsValid(true);
			}
		}
		m_repGrades.Populate();
		m_repGrades.UpdateWindow();
	}
}

// CGradesDlgView message handlers
void CGradesDlgView::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;
	if (m_repGrades.GetSafeHwnd() == 0)
	{

		// Create the sheet1 list box.
		if (!m_repGrades.Create(this, ID_REPORT_GRADES, L"Grades_report",TRUE,FALSE))
		{
			TRACE0( "Failed to create m_repGrades.\n" );
			return;
		}
	}

	if (m_repGrades.GetSafeHwnd() != 0)
	{

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				m_sMsgGrades2 = xml.str(IDS_STRING1270) + L"\n\n" + xml.str(IDS_STRING1272);
				m_sMsgGrades1 = xml.str(IDS_STRING1270) + L"\n\n" + xml.str(IDS_STRING1271);

				pCol = m_repGrades.AddColumn(new CXTPReportColumn(COLUMN_0, xml.str(IDS_STRING1251), 80,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_dwEditStyle = ES_UPPERCASE;
				pCol->GetEditOptions()->m_nMaxLength = 4;

				pCol = m_repGrades.AddColumn(new CXTPReportColumn(COLUMN_1, xml.str(IDS_STRING1252), 200,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_nMaxLength = 50;

				pCol = m_repGrades.AddColumn(new CXTPReportColumn(COLUMN_2, xml.str(IDS_STRING1253), 80,FALSE,XTP_REPORT_NOICON,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				m_repGrades.GetReportHeader()->AllowColumnRemove(FALSE);
				m_repGrades.SetMultipleSelection( FALSE );
				m_repGrades.SetGridStyle( TRUE, xtpReportGridSolid );
				m_repGrades.SetGridStyle( FALSE, xtpReportGridSmallDots );
				m_repGrades.FocusSubItems(TRUE);
				m_repGrades.AllowEdit(TRUE);

				CRect rect;
				GetClientRect(&rect);
				setResize(&m_repGrades,4,4,420,rect.bottom-8);

				// Strings
				m_sMsgCap = xml.str(IDS_STRING99);
				m_sMsgDelete1 = xml.str(IDS_STRING1254) + L"\n" + xml.str(IDS_STRING12540);
				m_sMsgDelete2 = xml.str(IDS_STRING1255);

				m_sMsgNoDelete = xml.str(IDS_STRING1260);

				xml.clean();

			}
		}
	}
}

void CGradesDlgView::OnUpdateToolbar(CCmdUI* pCmdUI)
{
	if (pCmdUI->m_nID == ID_BUTTON32799)
		pCmdUI->Enable(m_bEnableAdd);
	if (pCmdUI->m_nID == ID_BUTTON32800)
		pCmdUI->Enable(m_bEnableDelete);
	if (pCmdUI->m_nID == ID_BUTTON32801)
		pCmdUI->Enable(m_bEnableSave);
}

void CGradesDlgView::OnCommand(UINT nID)
{
	switch(nID)
	{
		case ID_BUTTON32799 : Add(); break;
		case ID_BUTTON32800 : Save(TRUE,TRUE); break;
		case ID_BUTTON32801 : Delete(); break;
	};
}

void CGradesDlgView::OnSize(UINT nType,int cx,int cy)
{
	if (m_repGrades.GetSafeHwnd())
	{
		setResize(&m_repGrades,4,4,420,cy-8);
	}

	CXTResizeFormView::OnSize(nType,cx,cy);
}

BOOL CGradesDlgView::isGradesCodeUnique(int row,LPCTSTR value)
{
	if (m_vecGrades.size() > 0)
	{
		for (UINT i = 0;i < m_vecGrades.size();i++)
		{
			if (m_vecGrades[i].getGradesCode().CompareNoCase(value) == 0 && i != row)
				return FALSE;
		}
	}
	return TRUE;
}

BOOL CGradesDlgView::CheckDuplicate(LPCTSTR grade_code,int row)
{
	CGradesReportRec *pRec = NULL;
	CXTPReportRows *pRows = m_repGrades.GetRows();
	if (pRows != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CGradesReportRec *)pRows->GetAt(i)->GetRecord();
			if (i != row && pRec->getColText(COLUMN_0).CompareNoCase(grade_code) == 0)
			{
				return TRUE;
			}
		}
	}
	return FALSE;
}


void CGradesDlgView::populateReport()
{
	// We'll also add this new item to DB
	if (m_pDB != NULL)
	{
		m_pDB->getGrades(m_vecGrades,m_repGrades);
		m_repGrades.SetFocus();
		m_repGrades.UpdateWindow();
	}

	m_bEnableAdd = (m_repGrades.GetRows()->GetCount() < MAX_NUMBER_OF_GRADES);
}

void CGradesDlgView::Add()
{
	m_repGrades.AddRecord(new CGradesReportRec(CGrades(),true));
	m_repGrades.Populate();
	m_repGrades.UpdateWindow();

	// Goto last record
	m_repGrades.SetFocusedRow(m_repGrades.GetRows()->GetAt(m_repGrades.GetRows()->GetCount()-1));
	m_repGrades.SetFocusedColumn(m_repGrades.GetColumns()->GetAt(COLUMN_0));

	m_bEnableAdd = (m_repGrades.GetRows()->GetCount() < MAX_NUMBER_OF_GRADES);
}

void CGradesDlgView::Delete()
{
	BOOL bIsUsed = FALSE;
	CString sMsg = L"";
	CXTPReportRow *pRow = m_repGrades.GetFocusedRow();
	CGradesReportRec *rec = NULL;
	if (m_pDB != NULL && pRow != NULL)
	{
		rec = (CGradesReportRec*)pRow->GetRecord();
		if (rec != NULL)
		{

			// Check if selected species is used in any pricelist or userfunction
			// Tell user that species cant be deleted
			// Check if selected species is used in any pricelist or userfunction
			// Tell user that species cant be deleted
			if (m_vecVecPricelist.size() > 0)
			{
				for (UINT i = 0;i < m_vecVecPricelist.size();i++)
				{
					if (getIsGradeInTable(m_vecVecPricelist[i].getPricelist(),rec->getRecord().getGradesID()))
					{
						bIsUsed = TRUE;
						break;
					}
				}
			}
			
			if (bIsUsed)
			{
				::MessageBox(GetSafeHwnd(),m_sMsgNoDelete,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
				return;
			}


			// Ask user twice, if he realy want's to delete
			if (rec->getColText(COLUMN_1).IsEmpty())
			{
				sMsg.Format(m_sMsgDelete1,rec->getColText(COLUMN_0),rec->getColText(COLUMN_0));
			}
			else
			{
				sMsg.Format(m_sMsgDelete1,rec->getColText(COLUMN_0) + L"  " + rec->getColText(COLUMN_1),rec->getColText(COLUMN_0) + L"  " + rec->getColText(COLUMN_1));
			}
			// Ask user twice, if he realy want's to delete
			if (::MessageBox(GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
			{
				return;
			}
#ifdef _ASK_TWICE_ON_DELETE_SPC_GRADE
			else	if (::MessageBox(GetSafeHwnd(),m_sMsgDelete2,m_sMsgCap,MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
			{
				return;
			}
#endif
			// Save before deleting any data
			Save(TRUE,FALSE);

			rec = (CGradesReportRec*)pRow->GetRecord();
			if (rec != NULL)
			{
				if (m_pDB->delGrades(rec->getRecord()))
				{
					m_pDB->getGrades(m_vecGrades,m_repGrades);
					// Check if there's no grades left. If so, reset IDENTITY-field
					if (m_vecGrades.size() == 0)
						m_pDB->resetGradesIdentityField();

					m_bEnableAdd = (m_repGrades.GetRows()->GetCount() < MAX_NUMBER_OF_GRADES);
				}	// if (m_pDB->delGrades(rec->getRecord()))
			}
		}
	}
}

BOOL CGradesDlgView::Save(BOOL populate,BOOL do_check)
{
	if (do_check)
	{
		if (!Check())
			return FALSE;
	}

	CXTPReportRecords *pRecs = m_repGrades.GetRecords();
	CGradesReportRec *rec = NULL;
	// We'll also add this new item to DB
	if (m_pDB != NULL && pRecs != NULL)
	{
		m_repGrades.Populate();
		for (int i = 0;i < pRecs->GetCount();i++) 
		{
			rec = (CGradesReportRec*)pRecs->GetAt(i);
			if (rec->getRecord().getGradesID() == -1)
			{
				if (!rec->getColText(COLUMN_0).IsEmpty())
				{
					m_pDB->newGrades(CGrades(-1,rec->getColText(COLUMN_0),rec->getColText(COLUMN_1),rec->getColChecked(COLUMN_2)));
				}
			}
			else if (rec->getRecord().getGradesID() > 0)
			{
				if (!rec->getColText(COLUMN_0).IsEmpty())
				{
					m_pDB->updGrades(CGrades(rec->getRecord().getGradesID(),rec->getColText(COLUMN_0),rec->getColText(COLUMN_1),rec->getColChecked(COLUMN_2)));
				}
			}
		}
		//if (populate)
			populateReport();
	}

	return TRUE;
}

BOOL CGradesDlgView::Check()
{
	CXTPReportRecords *pRecs = NULL;
	CGradesReportRec *rec = NULL;
	m_repGrades.Populate();
/*
	if (m_pDB != NULL)
	{

		if (!m_pDB->isGradesUnique()) 
		{
			::MessageBox(GetSafeHwnd(),m_sMsgGrades2,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
			return FALSE;		
		}
	}
*/
	pRecs = m_repGrades.GetRecords();
	if (pRecs != NULL)
	{
		for (int i = 0;i < pRecs->GetCount();i++)
		{
			rec = (CGradesReportRec*)pRecs->GetAt(i);
			if (!rec->getIsValid())
			{
				::MessageBox(GetSafeHwnd(),m_sMsgGrades1,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
				return FALSE;		
			}	// if (!rec->getIsValid())
		}	// for (int i = 0;i < pRecs->GetCount();i++)
	}	// if (pRecs != NULL)
	return TRUE;		
}
