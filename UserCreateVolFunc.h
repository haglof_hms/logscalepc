#pragma once

#include "DatePickerCombo.h"
#include "DBHandling.h"

#include "afxwin.h"
#include "afxcmn.h"


#include <map>
#include "Resource.h"

#include "libxl.h"
#include "afxdtctl.h"

using namespace libxl;


class CExcelImportLibXl
{
public:
	CExcelImportLibXl();	// constructor
	CExcelImportLibXl(CString File, CString SheetOrSeparator, bool Backup = true,bool Convert_Numbers = true);
	~CExcelImportLibXl(); // Perform some cleanup functions
	bool ReadRow(CStringArray &RowValues, long row = 0); // Read a row from spreadsheet. Default is read the next row
	void GetFieldNames (CStringArray &FieldNames); // Get the header row from spreadsheet
	long GetTotalRows(); // Get total number of rows in  spreadsheet

	int GetSheetCount();	// get amount of sheets in workbook
	CString GetSheetName(int nIndex);	// get the name of a sheet
	int SetCurrentSheet(int nIndex);	// set which sheet to use
	int GetColCount();	// get amount of columns in current sheet

	bool GetCellValue(int nRow, int nCol,CString &value);
	bool GetCellValue(int nRow, int nCol,int *value);
	bool GetCellValue(int nRow, int nCol,double *value);
	bool GetComment(int nRow,int nCol,CString &value);
	int OpenExcel(CString csFile, CString csSheetOrSeparator = L"", bool bBackup = false, bool bConvert_Numbers = false);
private:
	int Open(CString csFile);	// open Excel file
	Book* m_Book;
	int m_nSheetIndex;
	int m_nFiletype;	// type of file EXCEL or CSV

	long m_dCurrentRow; // Index of current row, starting from 1
	long m_dTotalRows; // Total number of rows in spreadsheet
	
	CString m_sSeparator;

	CStringArray m_atempArray; // Temporary array for use by functions
	CStringArray m_aFieldNames; // Header row in spreadsheet
	CStringArray m_aRows; // Content of all the rows in spreadsheet
};


///////////////////////////////////////////////////////////////////////////////////////////
// CUserCreateVolFuncDoc

class CUserCreateVolFuncDoc : public CDocument
{
protected: // create from serialization only
	CUserCreateVolFuncDoc();
	DECLARE_DYNCREATE(CUserCreateVolFuncDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUserCreateVolFuncDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CUserCreateVolFuncDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CUserCreateVolFuncDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
/////////////////////////////////////////////////////////////////////////////
// class CUserCreateVolFuncFrame

class CUserCreateVolFuncFrame : public CChildFrameBase
{
//private:

	DECLARE_DYNCREATE(CUserCreateVolFuncFrame)
	CXTPToolBar m_wndToolBar;
	CXTPDockingPaneManager m_paneManager;
	CString m_sLangFN;

	BOOL m_bFirstOpen;
	BOOL m_bInitReports;
	BOOL m_bIsPrintOutTBtn;

	BOOL m_bIsSysCommand;	// TRUE if syscommad have ben activeted

	void setNavBarButtons()
	{
		// Send messages to HMSShell, disable buttons on toolbar; 120122 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

		// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 120122 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
	}

protected:
	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	HICON m_hIcon;
public:
	CUserCreateVolFuncFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

// Attributes

// Operations

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	virtual ~CUserCreateVolFuncFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	
	//{{AFX_MSG(CUserCreateVolFuncFrame)
	afx_msg int OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl);
	afx_msg void OnSysCommand(UINT nID,LPARAM lParam);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnClose();
	afx_msg void OnDestroy(void);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);

	//}}AFX_MSG
	
	DECLARE_MESSAGE_MAP()
};


// CUserCreateVolFunc form view

class CReportSpcView;

class CUserCreateVolFunc : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CUserCreateVolFunc)
	
	enum DEL_COLUMNS {DEL_ALL_COLUMNS,DEL_LAST_COLUMN,DEL_SELECTED_COLUMN};
	enum DEL_ROWS {DEL_ALL_ROWS,DEL_LAST_ROW,DEL_SELECTED_ROW};
	enum DEL_SPECIES { DEL_ALL_SPECIES, DEL_SELECTED_SPECIES };

	CString m_sLangFN;

	CXTResizeGroupBox m_wndGrp7_1;

	CMyExtStatic m_lbl7_1;
	CMyExtStatic m_lbl7_2;
	CMyExtStatic m_lbl7_3;
	CMyExtStatic m_lbl7_4;
	CMyExtStatic m_lbl7_5;
	CMyExtStatic m_lbl7_6;
	CMyExtStatic m_lbl7_7;
	CMyExtStatic m_lbl7_8;
	CMyExtStatic m_lbl7_9;
	CMyExtStatic m_lbl7_10;
	CMyExtStatic m_lbl7_11;
	CMyExtStatic m_lbl7_12;
	CMyExtStatic m_lbl7_13;
	CMyExtStatic m_lbl7_14;
	CMyExtStatic m_lbl7_15;
	CMyExtStatic m_lbl7_16;
	CMyExtStatic m_lbl7_17;
	CMyExtStatic m_lbl7_18;

	CMyExtEdit m_edit7_1;
	CMyExtEdit m_edit7_2;
	CMyExtEdit m_edit7_3;
	CMyExtEdit m_edit7_4;
	CMyExtEdit m_edit7_6;
	CMyExtEdit m_edit7_7;
	CMyExtEdit m_edit7_8;
	CMyExtEdit m_edit7_9;
	CMyExtEdit m_edit7_10;
	
	//CMyDatePickerCombo m_dt7_2;
	CComboBox m_cbox7_1;
	CComboBox m_cbox7_2;

	CXTButton m_btn7_1;
	CXTButton m_btn7_2;
	CXTButton m_btn7_3;
	CXTButton m_btn7_4;

	CString m_sFeet;
	CString m_sInch;
	CString m_sMM;
	CString m_sDM;
	CString m_sM;

	CString m_sOpenDlgEXCEL;

	CString m_sColumnCaps;

	CString m_sGeneralCap;

	CString m_sMsgOpenEXCEL;

	CString m_sMsgCap;
	CString m_sMsgRemoveSpecies1;
	CString m_sMsgRemoveSpecies2;
	CString m_sMsgRemoveTable1;
	CString m_sMsgRemoveTable2;
	CString m_sMsgMissingData1;
	CString m_sMsgMissingData2;
	CString m_sMsgFunctionNameAlreadyUsed1;
	CString m_sMsgFunctionNameAlreadyUsed2;

	CString m_sMsgChangeMeasuringMode;

	CString m_sMsgTableUsedInTemplates;

	CStringArray m_sarrTableTypes;
	CStringArray m_sarrMeasuringMode;

	BOOL m_bInitialized;
	BOOL m_bMeasuringModeSelected;

	int m_nSelectedMeasureMode;

	CMyTabControl m_wndTabControl;

	int m_nSelectedIndex;
	CUserVolTables m_recSelectedVolTable;
	CVecUserVolTables m_vecUserVolTables;
	CVecSpecies m_vecSpecies;
	CVecSpecies m_vecSpeciesSelected;

	CVecSpecies m_vecNonMatchingSpecies;

	vecLogScaleTemplates m_vecLogScaleTemplates;
	CPricelist m_recPricelist;
	CVecPricelist m_vecVecPricelist;

	BOOL m_bEnableToolBarBtnSave;
	BOOL m_bEnableToolBarBtnDelete;
	BOOL m_bEnableToolBarBtnAddSpc;
	BOOL m_bEnableToolBarBtnDelSpc;
	BOOL m_bEnableToolBarBtnImport;
	BOOL m_bEnableToolBarBtnExport;
	BOOL m_bEnableToolBarBtnPreview;

	BOOL m_bOnlySave;

	CDBHandling *m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;

	void enableView(BOOL enable);

	BOOL isAlreadyOnTab(LPCTSTR spc);

	QUIT_TYPES::Q_T_RETURN isNameOfFunctionOK(LPCTSTR name);

	std::map<int,int> m_mapSpcID;
	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon,int spc_id);

	CReportSpcView *getReportSpcView(void);
	CReportSpcView *getReportSpcView(int idx);


	void ImportSimple(LPCTSTR file);
	void ImportFromExported(LPCTSTR file);

	void removeColumns(DEL_COLUMNS del_cols);
	void removeRows(DEL_ROWS del_rows);
	void removeSpecies(DEL_SPECIES del_species);
	void addFirstColumnCaption(void);

	BOOL checkSpecie(LPCTSTR spc_code);

	void populateData();

	void createData();	// Skapa data-filen som sparas i databasen

	BOOL getSpecies(int id,CSpecies& rec);
	void addSpeciesToTabs(bool is_general_tab = false);
	void setStartAndStepData();
	void clearData();	// For new item

	void setNavigationButtons(BOOL start_prev,BOOL end_next);

	void New();
	BOOL Save();
	void Delete();
	void AddSpecies();
	void DelSpecies();
	void Import();
	void Export();
	void PreView();
	
	BOOL checkIsFunctionUsed(int func_id);

	void addColumnToTab();
	void addColumnToTab(double col_value,double step_value);

	void addRowsToTab();
	void addRowsToTab(double row_value,double step_value,std::vector<double>& vec);

	// Hj�lpfunktioner f�r att l�sa funktions-data
	// fr�n "text-fil" i blob
	int numberOfSpecies(LPCTSTR data);
	void setupSpeciesTabs(LPCTSTR data);
	void setupSpeciesTableColumnData(LPCTSTR data);
	void setupSpeciesTableRowData(LPCTSTR data);

protected:
	CUserCreateVolFunc();           // protected constructor used by dynamic creation
	virtual ~CUserCreateVolFunc();

public:
	enum { IDD = IDD_FORMVIEW7 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

	void doSave();
	void doSetNavigationButtons();

	QUIT_TYPES::Q_T_RETURN checkDataBeforeSave(CHECK_SAVE_TYPES::CHECK_SAVE cs);

	void doPopulate(CUserVT& rec);

protected:
	//{{AFX_VIRTUAL(CUserCreateVolFunc)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CUserCreateVolFunc)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);

	afx_msg void OnCommand(UINT nID);
	afx_msg void OnToolsCommand(UINT nID);

	afx_msg void OnUpdateToolbar(CCmdUI* pCmdUI);
	afx_msg void OnUpdateToolsToolbar(CCmdUI* pCmdUI);
	
	afx_msg void OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult);
	//{{AFX_MSG(CUserCreateVolFunc)


	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnCbnSelchangeCombo73();
	afx_msg void OnCbnSelchangeCombo71();
	afx_msg void OnBnClickedButton71();
	afx_msg void OnBnClickedButton72();
	afx_msg void OnBnClickedButton73();
	afx_msg void OnBnClickedButton74();
	CDateTimeCtrl m_dtDate;
	CString m_csDate;
};


// ReportView used for each specie set in list
class CReportSpcView : public CXTPReportView
{
	DECLARE_DYNCREATE(CReportSpcView)
public:
	CReportSpcView();
	virtual ~CReportSpcView();

	//{{AFX_MSG(CReportSpcView)
	afx_msg void OnSave(UINT nID);
	afx_msg void OnTools(UINT nID);
	//{{AFX_MSG(CReportSpcView)

	DECLARE_MESSAGE_MAP()
};
