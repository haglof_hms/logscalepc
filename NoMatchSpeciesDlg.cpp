// NoMatchSpeciesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NoMatchSpeciesDlg.h"

#include "ResLangFileReader.h"
#include "ReportClasses.h"

#include <algorithm>

// Function to sort vecLanguages, makin' sure we get
// languages in ascending order; 081020 p�d

bool SortSpeciesData2(CSpecies& l1, CSpecies& l2)
{
	return l1.getSpcID() < l2.getSpcID();
}

// CNoMatchSpeciesDlg dialog

IMPLEMENT_DYNAMIC(CNoMatchSpeciesDlg, CXTResizeDialog)

BEGIN_MESSAGE_MAP(CNoMatchSpeciesDlg, CXTResizeDialog)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, ID_REPORT_SETUP_SPECIES3, OnReportSettingSpc)
	ON_BN_CLICKED(IDOK, &CNoMatchSpeciesDlg::OnBnClickedOk)
END_MESSAGE_MAP()

CNoMatchSpeciesDlg::CNoMatchSpeciesDlg(CWnd* pParent /*=NULL*/)
	: CXTResizeDialog(CNoMatchSpeciesDlg::IDD, pParent),
		m_bInitialized(FALSE),
		m_bSpcSelected(FALSE)
{

}

CNoMatchSpeciesDlg::~CNoMatchSpeciesDlg()
{
}

void CNoMatchSpeciesDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNoMatchSpeciesDlg)
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_LBL21_1, m_lbl21_1);
	//}}AFX_DATA_MAP
}
BOOL CNoMatchSpeciesDlg::OnInitDialog()
{
	CXTResizeDialog::OnInitDialog();
	if (!m_bInitialized)
	{
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		m_lbl21_1.SetBkColor(INFOBK);
		m_lbl21_1.SetTextColor(BLUE);
		m_lbl21_1.SetLblFontEx(14);

		setupReport();
		
		populateReport();
		
		m_bInitialized = TRUE;
	}

	return TRUE;
}

// CMatchPrlGrdDlg message handlers

void CNoMatchSpeciesDlg::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;
	RECT rect;
	GetClientRect(&rect);

	if (m_repSpecies.GetSafeHwnd() == 0)
	{

		// Create the sheet1 list box.
		if (!m_repSpecies.Create(this, ID_REPORT_SETUP_SPECIES3, L"repSpecies_report"))
		{
			TRACE0( "Failed to create m_repSpecies.\n" );
			return;
		}
	}

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			// Set dialog caption
			SetWindowText(xml.str(IDS_STRING61400));

			m_btnOK.SetWindowText(xml.str(IDS_STRING100));
			m_btnCancel.SetWindowText(xml.str(IDS_STRING101));
			m_btnOK.EnableWindow(TRUE);

			m_sMissmatch = xml.str(IDS_STRING61430) + L" " + xml.str(IDS_STRING61431);
			m_sAllOK = xml.str(IDS_STRING61432);

			m_lbl21_1.SetWindowTextW(m_sAllOK);

			m_sConstraintAdd =  L"<" + xml.str(IDS_STRING61420) + L">";

			if (m_repSpecies.GetSafeHwnd() != NULL)
			{

				m_repSpecies.ShowWindow( SW_NORMAL );

				// Species in file
				pCol = m_repSpecies.AddColumn(new CXTPReportColumn(COLUMN_0, xml.str(IDS_STRING61401), 150,FALSE,XTP_REPORT_NOICON,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Species selection
				pCol = m_repSpecies.AddColumn(new CXTPReportColumn(COLUMN_1, xml.str(IDS_STRING61402), 150,FALSE,XTP_REPORT_NOICON,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->GetEditOptions()->AddComboButton();

				m_repSpecies.GetReportHeader()->AllowColumnRemove(FALSE);
				m_repSpecies.SetMultipleSelection( FALSE );
				m_repSpecies.SetGridStyle( TRUE, xtpReportGridSolid );
				m_repSpecies.SetGridStyle( FALSE, xtpReportGridSmallDots );
				m_repSpecies.FocusSubItems(TRUE);
				m_repSpecies.AllowEdit(TRUE);
				m_repSpecies.GetPaintManager()->SetFixedRowHeight(FALSE);

			}	// if (m_repSpecies.GetSafeHwnd() != NULL)

			// Strings
	
			setResize(&m_repSpecies,2,38,rect.right-4,rect.bottom-70);
			
		}

	}
}

void CNoMatchSpeciesDlg::populateReport(void)
{
	CSetupSpeciesReportRec *pRecSpc = NULL;
	CString sLastSpcCode = L"";
	CSpecies recSpc = CSpecies();
	int nIndex = -1;

	m_repSpecies.ResetContent();
	if (m_vecSpeciesFromTable.size() > 0)
	{
		std::sort(m_vecSpeciesFromTable.begin(),m_vecSpeciesFromTable.end(),SortSpeciesData2);
		for (UINT i = 0;i < m_vecSpeciesFromTable.size();i++)
		{
			recSpc = m_vecSpeciesFromTable[i];
			if (sLastSpcCode.CompareNoCase(recSpc.getSpcCode()) != 0)
			{
				nIndex = addSpeciesConstraits(recSpc);
				if (nIndex == -1)
				{
					m_lbl21_1.SetWindowTextW(m_sMissmatch);
					m_btnOK.EnableWindow(FALSE);
				}
				if ((pRecSpc = (CSetupSpeciesReportRec*)m_repSpecies.AddRecord(new CSetupSpeciesReportRec(i,recSpc))) != NULL)
				{
					if (nIndex > -1 && nIndex < m_vecSpecies.size())
					{
						pRecSpc->setColText(COLUMN_1,getSpcCodeAndName(m_vecSpecies[nIndex].getSpcID()));
					}
				}
			}
			sLastSpcCode = recSpc.getSpcCode();
		}
		m_repSpecies.Populate();
		m_repSpecies.UpdateWindow();
	}

}


void CNoMatchSpeciesDlg::OnReportSettingSpc(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	BOOL bSpcSelected = FALSE,bGradesSelected = FALSE;
	CXTPReportRows *pRows = NULL;
	CSetupSpeciesReportRec *pRecSpc = NULL;
	CSetupGradesReportRec2 *pRecGrd = NULL;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	// prevents us from crashing when clicking inside the report, but not on an item
	if(pItemNotify == NULL) 
		return;

	// prevents us from crashing when clicking inside the report, but not on an item
	if(pItemNotify->pItem == NULL)
		return;

	if ((pRows = m_repSpecies.GetRows()) != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			if ((pRecSpc = (CSetupSpeciesReportRec *)pRows->GetAt(i)->GetRecord()) != NULL)
			{
				if (pRecSpc->getColText(COLUMN_1).IsEmpty())
				{
					m_bSpcSelected = FALSE;
					break;
				}	// if (pRecSpc->getColText().IsEmpty())
				else
				{
					m_bSpcSelected = TRUE;
				}
			}	// if ((pRecSpc = (CSetupSpeciesReportRec *)pRows->GetAt(i)->GetRecord()) != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)
	}	// if ((pRows = m_repSpecies.GetRows()) != NULL)

	m_btnOK.EnableWindow(m_bSpcSelected);
}

CString CNoMatchSpeciesDlg::getSpcCodeAndName(int spc_id)
{
	CString sTmp = L"";
	if (m_vecSpecies.size() > 0)
	{
		for (UINT i = 0;i < m_vecSpecies.size();i++)
		{
			if (m_vecSpecies[i].getSpcID() == spc_id)
			{
					if (!m_vecSpecies[i].getSpcName().IsEmpty())
						sTmp.Format(L"%s - %s",m_vecSpecies[i].getSpcCode(),m_vecSpecies[i].getSpcName());
					else
						sTmp.Format(L"%s",m_vecSpecies[i].getSpcCode());
				return sTmp;
			}
		}
	}
	return L"";
}

int CNoMatchSpeciesDlg::addSpeciesConstraits(CSpecies rec)
{
	CString sSpcCodeAndNameA = L"",sSpcCode = L"";
	CXTPReportRecordItemConstraints *pCons = NULL;
	CXTPReportRecordItemConstraint *pCon = NULL;
	CXTPReportColumns *pColumns = m_repSpecies.GetColumns();
	CXTPReportRows *pRows = m_repSpecies.GetRows();
	int nIndex = -1;
	if (pColumns != NULL)
	{
		CXTPReportColumn *pSpcCol = pColumns->Find(COLUMN_1);
		if (pSpcCol != NULL)
		{
			// Get constraints for Specie column and remove all items; 111129 p�d
			if ((pCons = pSpcCol->GetEditOptions()->GetConstraints()) != NULL)
			{
				pCons->RemoveAll();
			}	// if (pCons != NULL)
			// If there's species, add to Specie column in report; 111129 p�d
			if (m_vecSpecies.size() > 0)
			{
				pSpcCol->GetEditOptions()->AddConstraint(m_sConstraintAdd,(int)-1);
				for (UINT i = 0;i < m_vecSpecies.size();i++)
				{
					sSpcCodeAndNameA = getSpcCodeAndName(m_vecSpecies[i].getSpcID());
					pSpcCol->GetEditOptions()->AddConstraint(sSpcCodeAndNameA,(int)i);
				}	// for (UINT i = 0;i < m_vecSpecies.size();i++)

				for (UINT i = 0;i < m_vecSpecies.size();i++)
				{
					sSpcCode = m_vecSpecies[i].getSpcCode().Trim();
					if (sSpcCode.CompareNoCase(rec.getSpcCode().Trim()) == 0)
					{
						nIndex = i;
						break;
					}
				}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
			}	// if (m_vecSpecies.size() > 0)
		}	// if (pSpcCol != NULL)
	}	// if (pColumns != NULL)

	return nIndex;
}

void CNoMatchSpeciesDlg::setSpeciesFromtable(CVecSpecies& vec)		
{ 
	m_vecSpeciesFromTable = vec; 
	for (UINT i = 0;i < m_vecSpeciesFromTable.size();i++)
	{
		m_vecChangeToSpecies.push_back(CMatchedSpecies(m_vecSpeciesFromTable[i].getSpcCode(),
																									 m_vecSpeciesFromTable[i].getSpcName(),
																									 -1,
																									 L"",
																									 L""));
	}
}


void CNoMatchSpeciesDlg::OnBnClickedOk()
{

	CString sTmp = L"",sFuncID = L"",S;
	CSpecies recSpc = CSpecies();

	CSetupSpeciesReportRec *pRecSpc = NULL;
	CXTPReportRecordItemConstraint *pConsSpc = NULL;
	CXTPReportColumns *pColsSpc = NULL;
	CXTPReportColumn *pColSpc = NULL;
	CXTPReportRows *pRowsSpc = NULL;
	
	int nSpcIndex = -1,nNewSpcID = -1;
	int nFuncID = -1;

	m_sData.Empty();

	if ((pColsSpc = m_repSpecies.GetColumns()) == NULL) return;
	if ((pColSpc = pColsSpc->Find(COLUMN_1)) == NULL) return;

	//----------------------------------------------------------------------------
	// Check out Species entered. If index equals 0 the items is set to <Add>
	if (((pRowsSpc = m_repSpecies.GetRows()) != NULL))
	{
		/////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Check if user's adding any species, in import-file but not included in database
		for (int i1 = 0;i1 < pRowsSpc->GetCount();i1++)
		{
			if ((pRecSpc = (CSetupSpeciesReportRec*)pRowsSpc->GetAt(i1)->GetRecord()) != NULL)
			{
				if ((pConsSpc = pColSpc->GetEditOptions()->FindConstraint(pRecSpc->getColText(COLUMN_1))) != NULL)
				{
					nSpcIndex = pConsSpc->GetIndex();
					// Check if the constraint index points to <Add>. 
					if (nSpcIndex == 0)
					{
						// This species we'll add to the database.
						// A species not in database.
						if (m_pDB != NULL)
						{
							if (m_pDB->newSpecies(CSpecies(-1,pRecSpc->getSpcRecord().getSpcCode(),pRecSpc->getSpcRecord().getSpcName(),
																							  pRecSpc->getSpcRecord().getBarkReductionInch(),pRecSpc->getSpcRecord().getBarkReductionMM())))
							{
								// Get last entered species id, i.e. primary key
								nNewSpcID = m_pDB->getLastSpeciesID();
								m_pDB->getSpecies(m_vecSpecies);
								addSpeciesConstraits(CSpecies());
								pRecSpc->setColText(COLUMN_1,getSpcCodeAndName(nNewSpcID));
								m_repSpecies.Populate();
								m_repSpecies.UpdateWindow();

								// Find species not matched and add to m_vecChangeToSpecies
								// Set to be added to database
								for (UINT i2 = 0;i2 < m_vecChangeToSpecies.size();i2++)
								{
									if (pRecSpc->getSpcRecord().getSpcCode().CompareNoCase(m_vecChangeToSpecies[i2].getNoMatchSpcCode()) == 0)
									{
										m_vecChangeToSpecies[i2].setChangeToSpcID(nNewSpcID);
										m_vecChangeToSpecies[i2].setChangeToSpcCode(m_vecChangeToSpecies[i2].getNoMatchSpcCode());
										m_vecChangeToSpecies[i2].setChangeToSpcName(m_vecChangeToSpecies[i2].getNoMatchSpcName());
									}
								}

							}	// if (m_pDB->newSpecies(CSpecies(-1,pRecSpc->getSpcRecord().getSpcCode(),pRecSpc->getSpcRecord().getSpcName(),0.0,0.0)))
						}	// if (m_pDB != NULL)					
					}	// if (nSpcIndex == 0)
					else
					{
						// Find species not matched and add to m_vecChangeToSpecies
						// Set to be substituded.
						if (nSpcIndex > 0 && nSpcIndex < m_vecSpecies.size())
						{
								for (UINT i2 = 0;i2 < m_vecChangeToSpecies.size();i2++)
								{
									if (pRecSpc->getSpcRecord().getSpcCode().CompareNoCase(m_vecChangeToSpecies[i2].getNoMatchSpcCode()) == 0)
									{
										m_vecChangeToSpecies[i2].setChangeToSpcID(m_vecSpecies[nSpcIndex-1].getSpcID());
										m_vecChangeToSpecies[i2].setChangeToSpcCode(m_vecSpecies[nSpcIndex-1].getSpcCode());
										m_vecChangeToSpecies[i2].setChangeToSpcName(m_vecSpecies[nSpcIndex-1].getSpcName());
									}
								}
						}
					}
				}	// if ((pConsSpc = pColSpc->GetEditOptions()->FindConstraint(pRecSpc->getColText(COLUMN_1))) != NULL)
			}	// if ((pRecSpc = (CSetupSpeciesReportRec*)pRows->GetAt(i)->GetRecord()) != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)

	}	// if (((pRowsSpc = m_repSpecies.GetRows()) != NULL) && (pRowsGrd = m_repGrades.GetRows()) != NULL)

	OnOK();
}
