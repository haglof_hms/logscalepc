#if !defined(__STORAGESEARCHREPORTVIEW_H__)
#define __STORAGESEARCHREPORTVIEW_H__

#pragma once

#include "StdAfx.h"

#include "Resource.h"

#include "DBHandling.h"

#include "DatePickerCombo.h"
#include "afxdtctl.h"

///////////////////////////////////////////////////////////////////////////////////////////
// CStorageSearchDoc

class CStorageSearchDoc : public CDocument
{
protected: // create from serialization only
	CStorageSearchDoc();
	DECLARE_DYNCREATE(CStorageSearchDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStorageSearchDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CStorageSearchDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CStorageSearchDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>

class CStorageSearchFrame : public CChildFrameBase //CMDIChildWnd
{
	DECLARE_DYNCREATE(CStorageSearchFrame)

//private:
	CXTPDockingPaneManager m_paneManager;
	CString m_sLangFN;
protected:

	void setLanguage(void);
	void setupToolBarIcons(void);

	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	BOOL m_bFirstOpen;
	BOOL m_bEnableTBBTNFilterOff;
	CXTPToolBar m_wndToolBar;
	CFont m_fontIcon;

	WINDOWPLACEMENT m_wpPlacement;
	HICON m_hIcon;
public:

	CStorageSearchFrame();           // protected constructor used by dynamic creation
	virtual ~CStorageSearchFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	CDialogBar m_wndFieldChooserDlg;   // Sample Field chooser window
	CDialogBar m_wndFilterEdit;     // Sample Filter editing window

	void setEnableTBBTNFilterOff(BOOL v)
	{
		m_bEnableTBBTNFilterOff = v;
	}

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
	//{{AFX_MSG(CMDIStandEntryFormFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnPaint();
	afx_msg void OnUpdateTBBTNFilterOff(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};



//////////////////////////////////////////////////////////////////////////////
// Derived class from CXTPReportFilterEditControl to handle
// OnKeyUp() event, setting value for toolbar button

class CStorageSearchReportFilterEditControl : public CXTPReportFilterEditControl
{
	DECLARE_DYNCREATE(CStorageSearchReportFilterEditControl)
public:
	CStorageSearchReportFilterEditControl(void)
		: CXTPReportFilterEditControl()
	{}

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	afx_msg void OnKeyUp(UINT,UINT,UINT);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


//////////////////////////////////////////////////////////////////////////////
// CStorageSearchView form view

class CStorageSearchReportView;

class CStorageSearchView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CStorageSearchView)

	// Data members
	BOOL m_bInitialized;
	CString m_sLangFN;

	CString m_sTabCaption;
	CString m_sNumOfHits;

	CString m_sMsgCap;
	CString m_sMsgLengthSearch;
	CString m_sMsgDayMonthCheck;
	CString m_sMsgMeasuringmodeCheck;

	CXTResizeGroupBox m_wndGrp6516_1;
	CXTResizeGroupBox m_wndGrp6516_2;
	CXTResizeGroupBox m_wndGrp6516_3;

	CMyExtStatic m_lbl6516_1;
	CMyExtStatic m_lbl6516_2;
	CMyExtStatic m_lbl6516_3;
	CMyExtStatic m_lbl6516_4;
	CMyExtStatic m_lbl6516_5;
	CMyExtStatic m_lbl6516_6;
	CMyExtStatic m_lbl6516_7;
	CMyExtStatic m_lbl6516_8;
	CMyExtStatic m_lbl6516_9;
	CMyExtStatic m_lbl6516_10;
	CMyExtStatic m_lbl6516_11;
	CMyExtStatic m_lbl6516_13;
	CMyExtStatic m_lbl6516_15;

	CMacroEdit m_ed6516_1;
	CMyExtEdit m_ed6516_2;
	CMyExtEdit m_ed6516_3;
	
	CXTMaskEdit m_ed6516_4;
	CXTMaskEdit m_ed6516_5;
	CXTMaskEdit m_ed6516_6;
	CXTMaskEdit m_ed6516_7;
	
	CMyExtEdit m_ed6516_10;

	CButton m_btn6516_1;
	CButton m_btn6516_2;

	CButton m_rb6516_1;
	CButton m_rb6516_2;
	CButton m_rb6516_3;

	CComboBox m_cb6516_1;
	CComboBox m_cb6516_2;
	CComboBox m_cb6516_3;
	CComboBox m_cb6516_4;
	CComboBox m_cb6516_5;

	CMyTabControl m_wndTabControl;
	CXTPTabManagerItem *m_tabManager;

	CStorageSearchReportView *getStorageSearchReportView(void);

	CDBHandling *m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;

	CVecSpecies m_vecSpecies;
	CVecSpecies::iterator itSpc;
	CVecGrades m_vecGrades;
	CVecGrades::iterator itGrades;
	CVecRegister m_vecRegister;
	CStringArray m_sarrStatus;
	CStringArray m_sarrSearchAs;

	CString m_sSQLWhereClause;
	CString m_sSQLWhereClauseSales;

	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon);

	vecSearch m_vecSearchPopulate;
	vecSearch m_vecSearch;
	vecSearchSales m_vecSearchSales;

	BOOL setupSQLWhereClause();
	BOOL setupSQLWhereClauseSales();

	CString getSpcCode(int idx);
	CString getGradesCode(int idx);
	CString getCBText(CComboBox& cb);

	short m_LanguageSet;
	BOOL validateEnteredDate(int year,int month,int day);

	void setupLastSeach();

	void populateReport(void);

	CString interpretMacro(CString column,CString macro);
protected:
	CStorageSearchView();           // protected constructor used by dynamic creation
	virtual ~CStorageSearchView();
	
public:
	enum { IDD = IDD_REPORTVIEW6516 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

	short getLanguageSet()	{ return m_LanguageSet; }
protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStorageSearchView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CStorageSearchView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnClose();
	afx_msg void OnDestroy(void);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam, LPARAM lParam);
	afx_msg void OnExportToExcel();
	afx_msg void OnShowFieldFilter();
	afx_msg void OnShowFieldFilterOff();
	afx_msg void OnPrintPreview();
	//afx_msg void OnRefresh();
	//afx_msg void OnExportToExcel();
	
	//{{AFX_MSG(CStorageSearchView)

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton65161();
	afx_msg void OnBnClickedButton65162();
	CDateTimeCtrl m_dtDate1;
	CString m_csDate1;
	CDateTimeCtrl m_dtDate2;
	CString m_csDate2;
	CDateTimeCtrl m_dtDate3;
	CString m_csDate3;
	CDateTimeCtrl m_dtDate4;
	CString m_csDate4;
};

//////////////////////////////////////////////////////////////////////////////
// CStorageSearchReportView form view

class CStorageSearchReportView : public  CXTPReportView
{
	DECLARE_DYNCREATE(CStorageSearchReportView)

protected:
	CStorageSearchReportView();           // protected constructor used by dynamic creation
	virtual ~CStorageSearchReportView();

	CString m_sLangFN;

	CString m_sGroupByThisField;
	CString m_sGroupByBox;
	CString m_sFieldChooser;

	CString m_sFilterOn;

	CString m_sMsgCap;
	CString m_sMsgOpenEXCEL;
	CString	m_sTabCaption;

	int m_nSelectedColumn;
	
	BOOL setupReport(void);

	CImageList m_ilIcons;


	CXTPReportSubListControl m_wndSubList;
	CStorageSearchReportFilterEditControl m_wndFilterEdit;
	CMyExtStatic m_lbl20_1;
	CMyExtStatic m_lbl20_2;

	void LoadReportState(void);
	void SaveReportState(void);

	CDBHandling *m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;

	void setFilterWindow(void);
public:
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;

#endif


	inline void exportToExcel()	{ OnExportToExcel(); }
	inline void printPreview()	{ OnPrintPreview(); }
	inline void showFieldFilter()	{ OnShowFieldFilter(); }
	inline void showFieldFilterOff()	{ OnShowFieldFilterOff(); }

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CMDIDBFormFrame)
	void OnDestroy();
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * result);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnShowFieldChooser();
	afx_msg void OnShowFieldFilter();
	afx_msg void OnShowFieldFilterOff();
	afx_msg void OnPrintPreview();
	afx_msg void OnRefresh();
	afx_msg void OnExportToExcel();
	afx_msg void OnFilePrint();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};






#endif