#include "stdafx.h"

#include "reportclasses.h"

//#include "monthcaldlg.h"

//////////////////////////////////////////////////////////////////////////
// CExIconItem
CExIconItem::CExIconItem(CString text,int icon_id)
{
	m_sText = text;
	SetIconIndex(icon_id);
	SetCaption((text));
	m_dec = -1;
}
	
CExIconItem::CExIconItem(int num_of,int icon_id)
{
	if (num_of > 0)
	{
		m_sText.Format(_T("%d"),num_of);
		SetCaption(m_sText);
	}
	m_dec = -1;
	SetIconIndex(icon_id);
}

CExIconItem::CExIconItem(double value,int dec,int icon_id)
{
	TCHAR tmp[50];
	m_fValue = value;
	m_dec = dec; 
	_stprintf(tmp,_T("%.*f"),m_dec,value);
	SetCaption((tmp));

	SetIconIndex(icon_id);
}

void CExIconItem::OnEditChanged(XTP_REPORTRECORDITEM_ARGS* /*pItemArgs*/, LPCTSTR szText)
{
	TCHAR tmp[50];
	m_sText = szText;
	if (m_dec > -1)
	{
		m_fValue = _tstof(m_sText);
		_stprintf(tmp,_T("%.*f"),m_dec,_tstof(m_sText));
		SetCaption(tmp);
	}
	else
		SetCaption(m_sText);

}

