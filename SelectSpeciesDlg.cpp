// SelectSpeciesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SelectSpeciesDlg.h"

#include "ResLangFileReader.h"

// CSelectSpeciesDlg dialog

IMPLEMENT_DYNAMIC(CSelectSpeciesDlg, CDialog)

BEGIN_MESSAGE_MAP(CSelectSpeciesDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CSelectSpeciesDlg::OnBnClickedOk)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST2, &CSelectSpeciesDlg::OnLvnItemchangedList2)
END_MESSAGE_MAP()

CSelectSpeciesDlg::CSelectSpeciesDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectSpeciesDlg::IDD, pParent),
	m_bInitialized(FALSE)
{

}

CSelectSpeciesDlg::~CSelectSpeciesDlg()
{
}

void CSelectSpeciesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LIST2, m_wndListCtrl);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	//}}AFX_DATA_MAP
}

BOOL CSelectSpeciesDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CDialog::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

BOOL CSelectSpeciesDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (!m_bInitialized)
	{

		setup();

		m_bInitialized = TRUE;
	}

	return TRUE;
}

// CSelectSpeciesDlg message handlers

void CSelectSpeciesDlg::setup(void)
{
	int nCnt = 0;
	CString sSpcID;
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			SetWindowText(xml.str(IDS_STRING4300));

			m_btnOK.SetWindowTextW(xml.str(IDS_STRING100));
			m_btnCancel.SetWindowTextW(xml.str(IDS_STRING101));

			m_btnOK.EnableWindow(FALSE);

			m_wndListCtrl.InsertColumn(0, L"", LVCFMT_CENTER, 30);
			m_wndListCtrl.InsertColumn(1, xml.str(IDS_STRING4301), LVCFMT_LEFT, 50);
			m_wndListCtrl.InsertColumn(2, xml.str(IDS_STRING4302), LVCFMT_LEFT, 100);
		}
		xml.clean();
	}

	// Get the windows handle to the header control for the
	// list control then subclass the control.
	HWND hWndHeader = m_wndListCtrl.GetDlgItem(0)->GetSafeHwnd();
	m_header.SubclassWindow(hWndHeader);

	m_header.SetTheme(new CXTHeaderCtrlThemeOfficeXP());

	m_wndListCtrl.ModifyExtendedStyle(0,LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT);

	if (m_vecSpecies.size() > 0)
	{
		for (UINT i = 0;i < m_vecSpecies.size();i++)
		{
//			if (!isSpecieUsed(m_vecSpecies[i].getSpcID()))
//			{
				m_wndListCtrl.InsertItem(nCnt, _T(""), 0);
				m_wndListCtrl.SetItem(nCnt,1, LVIF_TEXT, m_vecSpecies[i].getSpcCode(), 0, NULL, NULL, NULL);
				m_wndListCtrl.SetItem(nCnt,2, LVIF_TEXT, (m_vecSpecies[i].getSpcName()), 0, NULL, NULL, NULL);
				m_wndListCtrl.SetItemData(nCnt,m_vecSpecies[i].getSpcID());
				nCnt++;
//			}
		}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
	}	// if (m_vecSpecies.size() > 0)
}


void CSelectSpeciesDlg::OnBnClickedOk()
{
	// Get selected species and add to m_vecSpeciesSelected; 120103 p�d
	int nSpcID = -1;
	m_vecSelectedSpecies.clear();

	if (m_wndListCtrl.GetItemCount() > 0 && m_vecSpecies.size() > 0)
	{
		for (int i = 0;i < m_wndListCtrl.GetItemCount();i++)
		{
			if (m_wndListCtrl.GetCheck(i))
			{
				nSpcID = m_wndListCtrl.GetItemData(i);
				for (UINT i = 0;i < m_vecSpecies.size();i++)
				{
					if (m_vecSpecies[i].getSpcID() == nSpcID)
						m_vecSelectedSpecies.push_back(m_vecSpecies[i]);
				}
			}	// if (m_wndListCtrl.GetCheck(i))
		}	// for (int i = 0;i < m_wndListCtrl.GetItemCount();i++)
	}	// if (m_wndListCtrl.GetItemCount() > 0 && m_vecSpecies.size() > 0)
	

	OnOK();
}

void CSelectSpeciesDlg::OnLvnItemchangedList2(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;
	BOOL bEnabled = FALSE;
	if (m_wndListCtrl.GetItemCount() > 0)
	{
		for (int i = 0;i < m_wndListCtrl.GetItemCount();i++)
		{
			if (m_wndListCtrl.GetCheck(i))
			{
				bEnabled = TRUE;
				break;
			}	// if (m_wndListCtrl.GetCheck(i))
		}	// for (int i = 0;i < m_wndListCtrl.GetItemCount();i++)
	}	// if (m_wndListCtrl.GetItemCount() > 0 && m_vecSpecies.size() > 0)
	m_btnOK.EnableWindow(bEnabled);

}
