#pragma once

#include "resource.h"

#include "dbhandling.h"

// CSelectCalcTypeDlg dialog

class CSelectCalcTypeDlg : public CDialog
{
	DECLARE_DYNAMIC(CSelectCalcTypeDlg)

	// Data members
	BOOL m_bInitialized;
	CString m_sLangFN;
	CVecCalcTypes m_vecCalcTypes;
	BOOL m_bIsPulpwood;
	CString m_sCalcTypeSet;
	CString m_sCalcBasisSelected;
	int m_nFuncID;	//#4258 ny variabel

	vecFuncDesc m_vecFuncDesc;
	CString m_sFuncDescSet;

	CButton m_wndOKBtn;
	CButton m_wndCancelBtn;
	CButton m_wndRemoveBtn;
	CMyReportControl m_repCalcTypes;

public:
	CSelectCalcTypeDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSelectCalcTypeDlg();

	void setVecCalcTypes(CVecCalcTypes &vec,LPCTSTR calc_type_set);
	void setVecFuncDesc(vecFuncDesc &vec,LPCTSTR func_set, int func_id);	//#4258 lagt till func_id

	CString& getSelectedCalcBasis()		{ return m_sCalcBasisSelected; }
	int getFuncID() {return m_nFuncID;}	//#4258 ny funktion

	void setIfPulpwood(BOOL is_pw)	{ m_bIsPulpwood = is_pw; }

// Dialog Data
	enum { IDD = IDD_DIALOG1 };

protected:	
	//{{AFX_VIRTUAL(CSelectCalcTypeDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG


	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedRemove();
};
