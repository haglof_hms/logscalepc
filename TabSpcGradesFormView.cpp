// SpeciesFormView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "TabSpcGradesFormView.h"

#include "fileparser.h"
#include "transactionclasses.h"

#include "ResLangFileReader.h"

///////////////////////////////////////////////////////////////////////////////////////////
// CTabSpcGradesDoc


IMPLEMENT_DYNCREATE(CTabSpcGradesDoc, CDocument)

BEGIN_MESSAGE_MAP(CTabSpcGradesDoc, CDocument)
	//{{AFX_MSG_MAP(CTabSpcGradesDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTabSpcGradesDoc construction/destruction

CTabSpcGradesDoc::CTabSpcGradesDoc()
{
	// TODO: add one-time construction code here

}

CTabSpcGradesDoc::~CTabSpcGradesDoc()
{
}


BOOL CTabSpcGradesDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CTabSpcGradesDoc serialization

void CTabSpcGradesDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CTabSpcGradesDoc diagnostics

#ifdef _DEBUG
void CTabSpcGradesDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CTabSpcGradesDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


///////////////////////////////////////////////////////////////////////////////////////////
// CTabSpcGradesFrame

IMPLEMENT_DYNCREATE(CTabSpcGradesFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CTabSpcGradesFrame, CMDIChildWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()

	ON_COMMAND(ID_BUTTON32775, OnAdd)
	ON_COMMAND(ID_BUTTON32776, OnDelete)
	ON_COMMAND(ID_BUTTON32777, OnSave)

	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_UPDATE_COMMAND_UI(ID_BUTTON32775, OnUpdateToolbarBtn)		// New button
	ON_UPDATE_COMMAND_UI(ID_BUTTON32776, OnUpdateToolbarBtn)		// Delete button
//	ON_UPDATE_COMMAND_UI(ID_BUTTON32777, OnUpdateToolbarBtn)	// Savebutton

END_MESSAGE_MAP()


// CTabSpcGradesFrame construction/destruction

XTPDockingPanePaintTheme CTabSpcGradesFrame::m_themeCurrent = xtpPaneThemeOffice2003;

CTabSpcGradesFrame::CTabSpcGradesFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
	m_bEnableToolBar = TRUE;
}

CTabSpcGradesFrame::~CTabSpcGradesFrame()
{
}

void CTabSpcGradesFrame::OnDestroy(void)
{

	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6004_KEY);
	SavePlacement(this, csBuf);

}

void CTabSpcGradesFrame::OnClose(void)
{
	CMDIChildWnd::OnClose();
}


int CTabSpcGradesFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	EnableDocking(CBRS_ALIGN_ANY);

	// Create and Load toolbar; 090107 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR2);
	m_wndToolBar.EnableDocking(xtpFlagAlignTop|xtpFlagStretched);

	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	CString sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
	if (fileExists(sLangFN))
	{
		RLFReader xml;
		if (xml.Load(sLangFN))
		{
			SetWindowText(xml.str(IDS_STRING1200));

			//***************************************************************************************
			// Setup commandbars and manues; 051114 p�d
			CXTPToolBar* pToolBar = &m_wndToolBar;
			if (pToolBar->IsBuiltIn())
			{
				if (pToolBar->GetType() != xtpBarTypeMenuBar)
				{
					UINT nBarID = pToolBar->GetBarID();
					pToolBar->LoadToolBar(nBarID, FALSE);
					CXTPControls *p = pToolBar->GetControls();
					// Setup icons on toolbars, using resource dll; 051208 p�d
					if (nBarID == IDR_TOOLBAR2)
					{		
						setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_ADD,xml.str(IDS_STRING1250),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(1),RES_TB_DEL2,xml.str(IDS_STRING1251),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(2),RES_TB_SAVE,xml.str(IDS_STRING1252),TRUE);	//
					}	// if (nBarID == IDR_TOOLBAR2)
				}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
			}	// if (pToolBar->IsBuiltIn())
			//***************************************************************************************
			xml.clean();
		}	// if (xml.Load(sLangFN))
	}	// if (fileExists(sLangFN))


	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

void CTabSpcGradesFrame::OnUpdateToolbarBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableToolBar);
}


BOOL CTabSpcGradesFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CTabSpcGradesFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CTabSpcGradesFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6001_KEY);
		LoadPlacement(this, csBuf);
  }

}

void CTabSpcGradesFrame::OnSetFocus(CWnd* pWnd)
{
	CMDIChildWnd::OnSetFocus(pWnd);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CTabSpcGradesFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}


// CTabSpcGradesFrame diagnostics

#ifdef _DEBUG
void CTabSpcGradesFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CTabSpcGradesFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CTabSpcGradesFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE2;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE2;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CTabSpcGradesFrame::toolbarEnableDisable(BOOL enable)
{
	m_bEnableToolBar = enable;
}

void CTabSpcGradesFrame::OnAdd()
{
	int nTabIdx = -1;
	CTabSpcGradesView *pView = (CTabSpcGradesView*)getFormViewByID(IDD_FORMVIEW1);
	if (pView != NULL)
	{
		nTabIdx = pView->getSelectedTabPageIndex();
		switch (nTabIdx)
		{
			case 0 : pView->addSpecies(); break;
			case 1 : pView->addGrades(); break;
		};
	}
}

void CTabSpcGradesFrame::OnDelete()
{
	int nTabIdx = -1;
	CTabSpcGradesView *pView = (CTabSpcGradesView*)getFormViewByID(IDD_FORMVIEW1);
	if (pView != NULL)
	{
		nTabIdx = pView->getSelectedTabPageIndex();
		switch (nTabIdx)
		{
			case 0 : pView->deleteSpecies(); break;
			case 1 : pView->deleteGrades(); break;
		};
	}
}


void CTabSpcGradesFrame::OnSave()
{
	int nTabIdx = -1;
	CTabSpcGradesView *pView = (CTabSpcGradesView*)getFormViewByID(IDD_FORMVIEW1);
	if (pView != NULL)
	{
		nTabIdx = pView->getSelectedTabPageIndex();
		switch (nTabIdx)
		{
			case 0 : pView->saveSpecies(); break;
			case 1 : pView->saveGrades(); break;
			case 2 : pView->saveGradesPerSpecies(); break;
		};
	}
}

// CTabSpcGradesView

IMPLEMENT_DYNCREATE(CTabSpcGradesView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CTabSpcGradesView, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_COPYDATA()
	ON_WM_CREATE()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessge)

	ON_NOTIFY(TCN_SELCHANGE, ID_TABCONTROL, OnSelectedChanged)

//	ON_COMMAND(ID_BUTTON32775, OnNew)
//	ON_COMMAND(ID_BUTTON32776, OnDel)

END_MESSAGE_MAP()

CTabSpcGradesView::CTabSpcGradesView()
	: CXTResizeFormView(CTabSpcGradesView::IDD),
	  m_bInitialized(FALSE)
{

}

CTabSpcGradesView::~CTabSpcGradesView()
{
}

int CTabSpcGradesView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	LOGFONT lf;
	VERIFY(m_fntTab.CreateFont(
   18,                        // nHeight
   0,                         // nWidth
   0,                         // nEscapement
   0,                         // nOrientation
   FW_NORMAL,                 // nWeight
   FALSE,                     // bItalic
   FALSE,                     // bUnderline
   0,                         // cStrikeOut
   ANSI_CHARSET,              // nCharSet
   OUT_DEFAULT_PRECIS,        // nOutPrecision
   CLIP_DEFAULT_PRECIS,       // nClipPrecision
   DEFAULT_QUALITY,           // nQuality
   DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
   _T("Times New Roman")));                 // lpszFacename

	m_fntTab.GetLogFont( &lf );

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS, CRect(0, 0, 0, 0), this, ID_TABCONTROL);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearancePropertyPage2003);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = TRUE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = TRUE;
	m_wndTabControl.GetPaintManager()->m_bBoldSelected = TRUE;
	m_wndTabControl.GetPaintManager()->SetFontIndirect( &lf );
	m_wndTabControl.GetPaintManager()->DisableLunaColors( FALSE );

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{

			AddView(RUNTIME_CLASS(CSpeciesReportView), xml.str(IDS_STRING1200), -1);
			AddView(RUNTIME_CLASS(CGradesReportView), xml.str(IDS_STRING1210), -1);
//			AddView(RUNTIME_CLASS(CGradesperSpcView), xml.str(IDS_STRING1220), -1);
			xml.clean();
		}
	}

	return 0;
}

void CTabSpcGradesView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}

BOOL CTabSpcGradesView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CTabSpcGradesView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();
	//	SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{
		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		CSpeciesReportView* page = getSpeciesPage();
		if (page != NULL)
		{
			page->populateReport(m_pDB);
		}
		
		m_bInitialized = TRUE;
	}

}

BOOL CTabSpcGradesView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{

	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		if (m_dbConnectionData.conn->isConnected())
		{
			m_pDB = new CDBHandling(m_dbConnectionData);
		}
	}

	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CTabSpcGradesView::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	if (m_wndTabControl.GetSafeHwnd())
	{
		m_wndTabControl.MoveWindow(0, 0, cx, cy);
	}

}

LRESULT CTabSpcGradesView::OnSuiteMessge( WPARAM wParam, LPARAM lParam )
{
	switch (wParam)
	{
		case ID_NEW_ITEM :
		{
			break;
		}	// case ID_NEW_ITEM :

		case ID_SAVE_ITEM :
		{
			break;
		}	// case ID_SAVE_ITEM :
		
		case ID_DELETE_ITEM :
		{
			break;
		}	// case ID_DELETE_ITEM :
	};
	return 0L;
}


// CTabSpcGradesView diagnostics

#ifdef _DEBUG
void CTabSpcGradesView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CTabSpcGradesView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG

// Add FormView to TabControl

int CTabSpcGradesView::getSelectedTabPageIndex(void)
{
	CXTPTabManagerItem *pItem = m_wndTabControl.getSelectedTabPage();
	if (pItem != NULL)
	{
		return pItem->GetIndex();
	}
	return -1;
}


CSpeciesReportView* CTabSpcGradesView::getSpeciesPage(void)
{
	m_tabManager = m_wndTabControl.getTabPage(0);
	if (m_tabManager)
	{
		CSpeciesReportView* pView = DYNAMIC_DOWNCAST(CSpeciesReportView, CWnd::FromHandle(m_tabManager->GetHandle()));
		ASSERT_KINDOF(CSpeciesReportView, pView);
		return pView;
	}
	return NULL;
}

CGradesReportView* CTabSpcGradesView::getGradesPage(void)
{
	m_tabManager = m_wndTabControl.getTabPage(1);
	if (m_tabManager)
	{
		CGradesReportView* pView = DYNAMIC_DOWNCAST(CGradesReportView, CWnd::FromHandle(m_tabManager->GetHandle()));
		ASSERT_KINDOF(CGradesReportView, pView);
		return pView;
	}
	return NULL;
}
/*
CGradesperSpcView* CTabSpcGradesView::getGradesPerSpeciesPage(void)
{

	m_tabManager = m_wndTabControl.getTabPage(2);
	if (m_tabManager)
	{
		CGradesperSpcView* pView = DYNAMIC_DOWNCAST(CGradesperSpcView, CWnd::FromHandle(m_tabManager->GetHandle()));
		ASSERT_KINDOF(CGradesperSpcView, pView);
		return pView;
	}

	return NULL;
}
*/
void CTabSpcGradesView::OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult)
{
	UNUSED_ALWAYS(pNMHDR);
	*pResult = 0;
	int nTabIdx = getSelectedTabPageIndex();
	CTabSpcGradesFrame *pForm = NULL;
	switch (nTabIdx)
	{
		case 2: 
			{
/*
				CGradesperSpcView* page = getGradesPerSpeciesPage();
				if (page != NULL)
				{
					page->populateReport(m_pDB);
				}
				if ((pForm = (CTabSpcGradesFrame*)getFormViewParentByID(IDD_FORMVIEW1)) != NULL)
					pForm->toolbarEnableDisable(FALSE); 
*/
			}
		break;
		case 1:
			{
				CGradesReportView* page = getGradesPage();
				if (page != NULL)
				{
					page->populateReport(m_pDB);
				}
				if ((pForm = (CTabSpcGradesFrame*)getFormViewParentByID(IDD_FORMVIEW1)) != NULL)
					pForm->toolbarEnableDisable(TRUE); 
			}
		break;
		case 0: 
			if ((pForm = (CTabSpcGradesFrame*)getFormViewParentByID(IDD_FORMVIEW1)) != NULL)
				pForm->toolbarEnableDisable(TRUE); 
		break;
	}
}

void CTabSpcGradesView::addSpecies()
{
	CSpeciesReportView* page = getSpeciesPage();
	if (page != NULL)
	{
		page->addSpecies(m_pDB);
	}
}

void CTabSpcGradesView::deleteSpecies()
{
	CSpeciesReportView* page = getSpeciesPage();
	if (page != NULL)
	{
		page->deleteSpecies(m_pDB);
	}
}

void CTabSpcGradesView::saveSpecies()
{
	CSpeciesReportView* page = getSpeciesPage();
	if (page != NULL)
	{
		page->saveSpecies(m_pDB);
	}
}

void CTabSpcGradesView::addGrades()
{
	CGradesReportView* page = getGradesPage();
	if (page != NULL)
	{
		page->addGrades(m_pDB);
	}
}

void CTabSpcGradesView::deleteGrades()
{
	CGradesReportView* page = getGradesPage();
	if (page != NULL)
	{
		page->deleteGrades(m_pDB);
	}
}

void CTabSpcGradesView::saveGrades()
{
	CGradesReportView* page = getGradesPage();
	if (page != NULL)
	{
		page->saveGrades(m_pDB);
	}
}

void CTabSpcGradesView::saveGradesPerSpecies()
{
/*
	CGradesperSpcView* page = getGradesPerSpeciesPage();
	if (page != NULL)
	{
		page->saveGradesPerSpecies(m_pDB);
	}
*/
}


BOOL CTabSpcGradesView::AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon)
{
	CCreateContext contextT;
	contextT.m_pCurrentDoc     = GetDocument();
	contextT.m_pNewViewClass   = pViewClass;
	contextT.m_pNewDocTemplate = GetDocument()->GetDocTemplate();

	CWnd* pWnd;
	TRY
	{
		pWnd = (CWnd*)pViewClass->CreateObject();
		if (pWnd == NULL)
		{
			AfxThrowMemoryException();
		}
	}
	CATCH_ALL(e)
	{
		TRACE0( "Out of memory creating a view.\n" );
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;

	int nTab = m_wndTabControl.GetItemCount();

	// Create with the right size (wrong position)
	CRect rect(0,0,0,0);
	if (!pWnd->Create(NULL, NULL, dwStyle,
		rect, &m_wndTabControl, (AFX_IDW_PANE_FIRST + nTab), &contextT))
	{
		TRACE0( "Warning: couldn't create client tab for view.\n" );
		// pWnd will be cleaned up by PostNcDestroy
		return NULL;
	}
	m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);

	//pWnd->SendMessage(WM_INITIALUPDATE);
	pWnd->SetOwner(this);

	return TRUE;
}
