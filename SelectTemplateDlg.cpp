// SelectTemplateDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SelectTemplateDlg.h"
#include "ResLangFileReader.h"


// CSelectTemplateDlg dialog

IMPLEMENT_DYNAMIC(CSelectTemplateDlg, CDialog)

BEGIN_MESSAGE_MAP(CSelectTemplateDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CSelectTemplateDlg::OnBnClickedOk)
	ON_LBN_SELCHANGE(IDC_LIST2_1, &CSelectTemplateDlg::OnLbnSelchangeList21)
END_MESSAGE_MAP()

CSelectTemplateDlg::CSelectTemplateDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectTemplateDlg::IDD, pParent),
		m_sLangFN(L""),
		m_bInitialized(FALSE)

{

}

CSelectTemplateDlg::~CSelectTemplateDlg()
{
}

void CSelectTemplateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNewTemplateDlg)
	DDX_Control(pDX, IDC_LIST2_1, m_lbdlg2_1);
	DDX_Control(pDX, IDOK, m_wndOKBtn);
	DDX_Control(pDX, IDCANCEL, m_wndCancelBtn);

	//}}AFX_DATA_MAP

}

// CSelectTemplateDlg message handlers
BOOL CSelectTemplateDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (!m_bInitialized)
	{
		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				SetWindowText(xml.str(IDS_STRING4800));
				m_wndOKBtn.SetWindowText(xml.str(IDS_STRING100));
				m_wndCancelBtn.SetWindowText(xml.str(IDS_STRING101));

				m_wndOKBtn.EnableWindow(FALSE);
			}
			xml.clean();
		}

		setupData();

		m_bInitialized = TRUE;
	}

	return TRUE;
}


// CSelectTemplateDlg message handlers
void CSelectTemplateDlg::setupData()
{
	if (m_vecLogScaleTemplates.size() > 0)
	{
		for (UINT i = 0;i < m_vecLogScaleTemplates.size();i++)
		{
			m_lbdlg2_1.AddString(m_vecLogScaleTemplates[i].getTmplName());
		}
	}
}

void CSelectTemplateDlg::OnBnClickedOk()
{
	int nSelectedIndex = m_lbdlg2_1.GetCurSel();
	m_recSelectedTemplates = CLogScaleTemplates();
	if (nSelectedIndex > -1 && nSelectedIndex < m_vecLogScaleTemplates.size() )
	{
		m_recSelectedTemplates =	m_vecLogScaleTemplates[nSelectedIndex];
	}

	OnOK();
}

void CSelectTemplateDlg::OnLbnSelchangeList21()
{
 m_wndOKBtn.EnableWindow(m_lbdlg2_1.GetCurSel() > LB_ERR);
}
