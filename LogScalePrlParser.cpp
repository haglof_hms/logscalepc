#include "StdAfx.h"
#include "LogScalePrlParser.h"

inline void CHECK( HRESULT _hr ) 
{ 
  if FAILED(_hr) throw(_hr); 
}


LogScalePrlParser::LogScalePrlParser(void)
{
	CHECK(CoInitialize(NULL));

	pDomDoc = NULL;

	// Create MSXML2 DOM Document
	pDomDoc.CreateInstance("Msxml2.DOMDocument.3.0");
	
	// Set parser in NON async mode
	pDomDoc->async	= VARIANT_FALSE;
	
	// Validate during parsing
	pDomDoc->validateOnParse = VARIANT_TRUE;
}

LogScalePrlParser::~LogScalePrlParser()
{
	pDomDoc->loadXML(_bstr_t(""));
	pDomDoc = NULL;
  CoUninitialize();
}

// Methods for Loagins and Saving xml file(s); 060407 p�d

BOOL LogScalePrlParser::LoadFromFile(LPCTSTR file)
{
	return pDomDoc->load(file);
}

BOOL LogScalePrlParser::LoadFromBuffer(LPCTSTR buffer)
{
	return pDomDoc->loadXML(buffer);
}

BOOL LogScalePrlParser::SaveToFile(LPCTSTR file)
{
	return pDomDoc->save(file);
}

// Protected
CString LogScalePrlParser::getAttrText(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[128];

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
	}	// if (pAttr)

	return szData;
}

double LogScalePrlParser::getAttrDouble(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[128];
	double fValue;

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
		fValue = _tstof(szData);
	}	// if (pAttr)

	return fValue;
}

int LogScalePrlParser::getAttrInt(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[128];
	int nValue;

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
		nValue = _tstoi(szData);
	}	// if (pAttr)

	return nValue;
}

BOOL LogScalePrlParser::getAttrBool(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[128];
	BOOL bValue;

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
		bValue = (_tcscmp(szData,_T("0")) == 0 ? FALSE : TRUE);
	}	// if (pAttr)

	return bValue;
}


// Public

BOOL  LogScalePrlParser::isALogScalePricelist()
{
	CComBSTR bstrData;
	TCHAR szData[128];

	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	if (pRoot)
	{	
		_tcscpy(szData,_bstr_t(pRoot->baseName));
		if (_tcscmp(szData,PRICELIST_IDENTIFICATION) == 0)
			return TRUE;
		else
			return FALSE;
	}

	return FALSE;
}

// Methods for reading Header information
BOOL LogScalePrlParser::getHeaderName(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(TAG_GET_HEADER_NAME);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));
		return TRUE;
	}

	return FALSE;
}

BOOL LogScalePrlParser::getHeaderDoneBy(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(TAG_GET_HEADER_DONE_BY);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));

		return TRUE;
	}

	return FALSE;
}

BOOL LogScalePrlParser::getHeaderNotes(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(TAG_GET_HEADER_NOTES);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));
		return TRUE;
	}

	return FALSE;
}

BOOL LogScalePrlParser::getHeaderDate(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(TAG_GET_HEADER_DATE);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));
		return TRUE;
	}

	return FALSE;
}

BOOL LogScalePrlParser::getDataFile(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(TAG_GET_DATA_FILE);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));
		return TRUE;
	}

	return FALSE;
}

long LogScalePrlParser::getDataFileSize()
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(TAG_GET_DATA_FILE);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		return bstrData.Length();
	}

	return 0;
}

BOOL LogScalePrlParser::getXML(CString &xml)
{
	CComBSTR bstrBuffer;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	if (pRoot)
	{
		pRoot->get_xml( &bstrBuffer );
		CW2CT szBuffer( bstrBuffer );
		xml = szBuffer;
		return TRUE;
	}

	return FALSE;
}
