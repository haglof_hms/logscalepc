#pragma once

#include "Resource.h"

#include "ReportClasses.h"

// CCorrectionDlg dialog

class CCorrectionDlg : public CXTResizeDialog
{
	DECLARE_DYNAMIC(CCorrectionDlg)
	// Data members
	BOOL m_bInitialized;
	CString m_sLangFN;
	CString m_sMsgCap;
	CString m_sMsgFixData;
	CString m_sMsgMeasModeError;
	CString m_sMsgSpeciesError;
	CString m_sMsgTicketNumExists;
	CString m_sLogsOK;
	CString m_sLogsCorrection;
	CString m_sMsgDataEnterError;
	CString m_sMsgTicketNumberMissing;
	CString m_sMsgIllegalChars;

	CVecLogs m_vecLogs;
	CVecLogs m_vecLogsCorr;
	CVecSpecies m_vecSpecies;
	CVecGrades m_vecGrades;
	CVecRegister m_vecRegister;
	CTickets m_recTicket;
	CVecTickets m_vecTickets;
	vecLogScaleTemplates m_vecLogScaleTemplates;

	CStringArray m_sarrLoadType;
	CStringArray m_sarrDeductionType;

	CXTResizeGroupBox m_wndGrp6_1;
	CXTResizeGroupBox m_wndGrp6_2;
	CXTResizeGroupBox m_wndGrp6_3;

	CMyExtStatic m_lbl6_1;
	CMyExtStatic m_lbl6_2;
	CMyExtStatic m_lbl6_3;
	CMyExtStatic m_lbl6_4;
	CMyExtStatic m_lbl6_5;
	CMyExtStatic m_lbl6_6;
	CMyExtStatic m_lbl6_7;
	CMyExtStatic m_lbl6_8;
	CMyExtStatic m_lbl6_9;
	CMyExtStatic m_lbl6_10;
	CMyExtStatic m_lbl6_11;
	CMyExtStatic m_lbl6_12;
	CMyExtStatic m_lbl6_13;
	CMyExtStatic m_lbl6_14;
	CMyExtStatic m_lbl6_15;
	CMyExtStatic m_lbl6_16;
	CMyExtStatic m_lbl6_17;
	CMyExtStatic m_lbl6_18;
	CMyExtStatic m_lbl6_19;
	CMyExtStatic m_lbl6_32;
	CMyExtStatic m_lbl6_33;

	CComboBox m_cbx6_1;
	CComboBox m_cbx6_2;
	CComboBox m_cbx6_3;
	CComboBox m_cbx6_4;
	CComboBox m_cbx6_5;
	CComboBox m_cbx6_6;
	CComboBox m_cbx6_7;
	CComboBox m_cbx6_9;
	CComboBox m_cbx6_10;
	CComboBox m_cbx6_11;
	CComboBox m_cbx6_12;

	CMyExtEdit m_ed6_1;
	CMyExtEdit m_ed6_2;
	CMyExtEdit m_ed6_3;
	CMyExtEdit m_ed6_4;
	CMyExtEdit m_ed6_5;
	CMyExtEdit m_ed6_6;
	CMyExtEdit m_ed6_7;
	CMyExtEdit m_ed6_8;
	CMyExtEdit m_ed6_9;
	CMyExtEdit m_ed6_10;
	CMyExtEdit m_ed6_11;
	CMyExtEdit m_ed6_12;
	CMyExtEdit m_ed6_13;
	CMyExtEdit m_ed6_14;

	CButton m_cb6_1;
	CButton m_cb6_2;
	CButton m_cb6_3;
	CButton m_cb6_4;
	CButton m_cb6_5;
	CButton m_cb6_6;
	CButton m_cb6_7;
	CButton m_cb6_8;

//	CMyReportControl m_repCorrectionLogs;
	CButton m_btnOK;
	CButton m_btnCancel;
	CButton m_btnSetup;
	CXTButton m_btnUseTicketNoFromFile;
	CXTButton m_btnGenerateTicketNo;


	CFont *m_pFont1;

	short m_xtButtonPressed;

	short m_nOpenAs;

	int m_nCounter;

	CLogScaleTemplates m_recLogScaleTmpl;

	std::map<int,CString> m_mapHeaderData;

	DWORD m_dwButtonFlat;
	DWORD m_dwButtonNormal;

	CString m_sGeneratedTicketNum;

	int m_nOpenLogsViewAs;

	CStrIntMap m_mapUniqueTagsInLogs;

	// Methods
	void setupReport(void);
	void populateReport(void);
	void addCBoxData();

	BOOL checkTicketNumAlreadyExists(LPCTSTR ticket_num);

	int getSpcIDFromCode(LPCTSTR code);
	int getGradesIDFromCode(LPCTSTR code);

	BOOL checkSpeciesAndGrades();
	BOOL checkMeasuringMode();
	
public:
	CCorrectionDlg(CWnd* pParent = NULL,short open_as = 0);   // standard constructor, open_as = 0 => From Ticket, 1 = From TicketLogs
	virtual ~CCorrectionDlg();

	void setErrorLogs(CVecLogs& vec)	{ m_vecLogs = vec; }
	void setSpecies(CVecSpecies& vec)	{ m_vecSpecies = vec; }
	void setGrades(CVecGrades& vec)	{ m_vecGrades = vec; }
	void setRegister(CVecRegister& vec)	{ m_vecRegister = vec; }
	void setTicket(CTickets& rec,CVecTickets& vec)	{ m_recTicket = rec; m_vecTickets = vec; }
	void setTemplates(vecLogScaleTemplates& vec)	{ m_vecLogScaleTemplates = vec; }
//	void setTicketNumOK(bool v)	{ m_bTicketNumOK = v; }
	CVecLogs& getLogs()	{ return m_vecLogs; }
	CTickets& getTicket()	{ return m_recTicket; }

	void setGeneratedTicketNum(LPCTSTR num)	{ m_sGeneratedTicketNum = num; }

	void getHeaderData(std::map<int,CString>& head)	{	head = m_mapHeaderData;	}

	void setOpenLogsViewAs(int v)	{	m_nOpenLogsViewAs = v;	}

	BOOL checkLogs();
// Dialog Data
	enum { IDD = IDD_DIALOG6 };

protected:
	//{{AFX_VIRTUAL(CCorrectionDlg)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CCorrectionDlg)
	//}}AFX_MSG


	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButton61();
	afx_msg void OnCbnSelchangeCombo612();
	afx_msg void OnBnClickedButton62();
	afx_msg void OnBnClickedButton63();
	afx_msg void OnEnChangeEdit614();
};
