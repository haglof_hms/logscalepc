#if !defined(__SPECIESFORMVIEW_H__)
#define __SPECIESFORMVIEW_H__

#include "DBHandling.h"

class CSpeciesReportView : public CXTPReportView
{
//private:

	DECLARE_DYNCREATE(CSpeciesReportView)

	// Data members
	BOOL m_bInitialized;
	CString m_sLangFN;
	CDBHandling *m_pDB;
	CVecSpecies m_vecSpecies;

	CString m_sMsgCap;
	CString m_sMsgDelete1;
	CString m_sMsgDelete2;

	// Methods
	void setupReport();
public:
	CSpeciesReportView();
	virtual ~CSpeciesReportView();

	void populateReport(CDBHandling *pDB);

	void addSpecies(CDBHandling *pDB);
	void deleteSpecies(CDBHandling *pDB);
	void saveSpecies(CDBHandling *pDB);


	//{{AFX_VIRTUAL(CSpeciesReportView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CSpeciesReportView)
	afx_msg void OnDestroy();
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

};



#endif
