// stdafx.cpp : source file that includes just the standard includes
// LogScalePC.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"
#include "resource.h"

#include "ResLangFileReader.h"

#include <ctime> 
#include <cstdlib>

#include <algorithm>

#include <sstream>

#include <fstream>

#include <strsafe.h>

#include "libxl.h"

using namespace libxl;

using namespace std;

extern HINSTANCE hInst;

//////////////////////////////////////////////////////////////////////////////////////////
// Create database table from string; 110427 p�d
BOOL runSQLScriptFileEx(LPCTSTR table_name,CString script,CString db_name)
{
	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	int nAuthentication;
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;

	CString sScript;

	try
	{

		if (!script.IsEmpty())
		{
			GetAuthentication(&nAuthentication);

			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (!db_name.IsEmpty())
					_tcscpy_s(sDBName,127,db_name);

				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
									 _tcscmp(sDBName,_T("")) != 0 &&
									 _tcscmp(sUserName,_T("")) != 0 &&
									 _tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;

				if (bIsOK)
				{
					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{
							// Set database as set in sDBName; 061109 p�d
							// Check if database exists, I NOT CREATE THE DATABASE; 061109 p�d
							if (pDB->existsDatabase(sDBName))
							{
								if (!pDB->existsTableInDB(sDBName,table_name))
								{
									pDB->setDefaultDatabase(sDBName);
									sScript.Format(script,table_name);
									pDB->commandExecute(sScript);
									bReturn = TRUE;
								}
							}	// if (pDB->existsDatabase(sDBName))
						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)
				}	// if (_tcscmp(sLocation,"") != 0 &&
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		}	// if (fileExists(fn))
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP | MB_OK);
		bReturn = FALSE;
	}
		
	return bReturn;
}

BOOL runSQLScriptFileEx2(vecScriptFiles &vec,Scripts::actionTypes action)
{
	TCHAR sDB_PATH[127];
	TCHAR sUserName[127];
	TCHAR sPSW[127];
	TCHAR sDSN[127];
	TCHAR sLocation[127];
	TCHAR sDBName[127];
	int nAuthentication;
	SAClient_t m_saClient;
	BOOL bReturn = FALSE;
	BOOL bIsOK = FALSE;

	Scripts scripts;

	CString sScript;

	try
	{

		if (vec.size() > 0)
		{

			GetAuthentication(&nAuthentication);

			if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
			{
				if (nAuthentication == 0)	// Windows login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0);
				}
				else if (nAuthentication == 1)	// Server login
				{
					bIsOK = (_tcscmp(sLocation,_T("")) != 0 &&
									 _tcscmp(sDBName,_T("")) != 0 &&
									 _tcscmp(sUserName,_T("")) != 0 &&
									 _tcscmp(sPSW,_T("")) != 0);
				}
				else
					bIsOK = FALSE;

				if (bIsOK)
				{
					CSQLServer_ADODirect *pDB = new CSQLServer_ADODirect();
					if (pDB)
					{
						if (pDB->connectToDatabase(nAuthentication,sUserName,sPSW,sLocation))
						{

							for (UINT i = 0;i < vec.size();i++)
							{
								scripts = vec[i];

								if (!scripts.getDBName().IsEmpty())
											_tcscpy_s(sDBName,127,scripts.getDBName());

								// Set database as set in sDBName; 061109 p�d
								// Check if database exists, I NOT CREATE THE DATABASE; 061109 p�d

								if (pDB->existsDatabase(sDBName))
								{
									if (action == Scripts::TBL_CREATE)
									{
										if (!pDB->existsTableInDB(sDBName,scripts.getTableName()))
										{
											pDB->setDefaultDatabase(sDBName);
											sScript.Format(scripts.getScript(),scripts.getTableName());
											pDB->commandExecute(sScript);
											bReturn = TRUE;
										}	// if (!pDB->existsTableInDB(sDBName,scripts._TableName))
									}	// if (scripts._CreateTable)
									else if (action == Scripts::TBL_ALTER)
									{
										if (pDB->existsTableInDB(sDBName,scripts.getTableName()))
										{
											pDB->setDefaultDatabase(sDBName);
											sScript.Format(scripts.getScript(),scripts.getTableName(),scripts.getTableName());
											pDB->commandExecute(sScript);
											bReturn = TRUE;
										}	// if (pDB->existsTableInDB(sDBName,scripts._TableName))
									}	// else if (!scripts._CreateTable)
								}	// if (pDB->existsDatabase(sDBName))
							} // for (UINT i = 0;i < vec.size();i++)
						}	// if (pDB->connectToDatabase(sUserName,sPSW,sLocation))
						delete pDB;
					}	// if (pDB)
				}	// if (strcmp(sLocation,"") != 0 &&
			}	// if (getDBUserInfo(sDB_PATH,sUserName,sPSW,sDSN,sLocation,sDBName,&m_saClient))
		}	// if (!script.IsEmpty())
	}
	catch(_com_error &e)
	{
		::MessageBox(0,e.Description(),_T("ERROR"),MB_ICONSTOP | MB_OK);
		bReturn = FALSE;
	}
		
	return bReturn;
}


//////////////////////////////////////////////////////////////////////////////////////////
// Misc. functions

double StrToDbl(LPCTSTR str)
{
	return localeStrToDbl(str);
}

// Converts to format dd-mm-yyyy hh:mm:ss
SAString convSADateTime(SADateTime &dt)
{
	TCHAR szTmp[21];

	_stprintf(szTmp,_T("%02d-%02d-%04d %02d:%02d:%02d"),
		dt.GetDay(),
		dt.GetMonth(),
		dt.GetYear(),
		dt.GetHour(),
		dt.GetMinute(),
		dt.GetSecond());

	return szTmp;
}


void LoadReportState(CMyReportControl &rep)
{
	CString sFilterText = L"",sReg = L"";
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (rep.GetSafeHwnd() == NULL) return;

	sReg.Format(L"%s\\%s",L"LogScale",rep.getComponentName());

	if (!AfxGetApp()->GetProfileBinary(sReg, _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);


	try
	{
		rep.SerializeState(ar);
	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;
/*	
	// Get selected column index into registry; 070219 p�d
	m_nSelectedColumn = AfxGetApp()->GetProfileInt(_T(REG_WP_CONTACTS_SELLEIST_REPORT_KEY), _T("SelColIndex"),0);
*/
}

void SaveReportState(CMyReportControl &rep)
{
	CString sFilterText = L"",sReg = L"";
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	if (rep.GetSafeHwnd() == NULL) return;

	sReg.Format(L"%s\\%s",L"LogScale",rep.getComponentName());

	rep.SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary(sReg, _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);
/*
	// Set selected column index into registry; 070219 p�d
	AfxGetApp()->WriteProfileInt(_T(REG_WP_CONTACTS_SELLEIST_REPORT_KEY), _T("SelColIndex"), m_nSelectedColumn);
*/
}


void LoadReportState(CXTPReportControl &rep,int id)
{
	CString sFilterText = L"",sReg = L"";
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (rep.GetSafeHwnd() == NULL) return;

	sReg.Format(L"%s\\%d",L"LogScale",id);

	if (!AfxGetApp()->GetProfileBinary(sReg, _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);


	try
	{
		rep.SerializeState(ar);
	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;
/*	
	// Get selected column index into registry; 070219 p�d
	m_nSelectedColumn = AfxGetApp()->GetProfileInt(_T(REG_WP_CONTACTS_SELLEIST_REPORT_KEY), _T("SelColIndex"),0);
*/
}

void SaveReportState(CXTPReportControl &rep,int id)
{
	CString sFilterText = L"",sReg = L"";
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	if (rep.GetSafeHwnd() == NULL) return;

	sReg.Format(L"%s\\%d",L"LogScale",id);

	rep.SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary(sReg, _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);
/*
	// Set selected column index into registry; 070219 p�d
	AfxGetApp()->WriteProfileInt(_T(REG_WP_CONTACTS_SELLEIST_REPORT_KEY), _T("SelColIndex"), m_nSelectedColumn);
*/
}

CView *showFormView(int idd,LPCTSTR lang_fn,LPARAM lp,int open_msg_id)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();
	CView *pActiveView;

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	RLFReader *xml = new RLFReader();
	if (xml->Load(lang_fn))
	{
		sCaption = xml->str(idd);
	}
	delete xml;

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{
			
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					pView->GetParent()->BringWindowToTop();
					pView->GetParent()->SetFocus();
					pView->SendMessage(MSG_IN_SUITE,open_msg_id,lp);
					pActiveView = pView;
					posDOC = (POSITION)1;
					break;
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)

			if (posDOC == NULL)
			{

				pTemplate->OpenDocumentFile(NULL);

				// Find the CDocument for this tamplate, and set title.
				// Title is set in Languagefile; OBS! The nTableIndex
				// matches the string id in the languagefile; 051129 p�d
				POSITION posDOC = pTemplate->GetFirstDocPosition();
				while (posDOC != NULL)
				{
					CDocument* pDocument = pTemplate->GetNextDoc(posDOC);
					// Set the caption of the document. Can be a resource string,
					// a string set in the language xml-file etc.
					CString sDocTitle;
					sDocTitle.Format(_T("%s"),sCaption);
					pDocument->SetTitle(sDocTitle);
					POSITION posView = pDocument->GetFirstViewPosition();
					if(posView != NULL)
					{
						CView* pView = pDocument->GetNextView(posView);
						pView->SendMessage(MSG_IN_SUITE,open_msg_id,lp);
						break;
					}	// if(posView != NULL)
				}

				break;
			}
		}
	}

	return pActiveView;
}


CView *getFormViewByID(int idd)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{	
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					if (pView)
					{
						return pView;
					}
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)
		}
	}
	return NULL;
}

CWnd *getFormViewParentByID(int idd)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		sDocName = '\n' + sDocName;
		if (pTemplate && sDocName.Compare(sResStr) == 0)
		{
			
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					if (pView)
					{
						return pView->GetParent();
					}
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)
		}
	}
	return NULL;
}

BOOL msgToModuleWindowOpen(LPCTSTR module,WPARAM wp,LPARAM lp)
{
#ifdef USE_MODULE_COMMUNICATION
	BOOL bReturn = FALSE;
  CString sDocName = L"";
  CDocTemplate *pTemplate = NULL;
  CWinApp *pApp = AfxGetApp();
	CView* pView = NULL;
	// Loop through doc templates
	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		pTemplate->GetDocString(sDocName, CDocTemplate::docName);
		if (pTemplate && sDocName.Compare(module) == 0)
		{
			
			POSITION posDOC = pTemplate->GetFirstDocPosition();
			// Open only one instance of this window; 061002 p�d
			while(posDOC != NULL)
			{
				CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
				POSITION posView = pDocument->GetFirstViewPosition();
				if(posView != NULL)
				{
					CView* pView = pDocument->GetNextView(posView);
					if (pView)
					{
						pView->SendMessage(MSG_IN_SUITE,wp,lp);
						bReturn = TRUE;
					}
				}	// if(posView != NULL)
			}	// while(posDOC != NULL)
		}
	}
	return bReturn;
#else
	return TRUE;
#endif
}


void setToolbarBtn(LPCTSTR resource_dll_fn,CXTPControl *pCtrl,int icon_id,LPCTSTR tool_tip,BOOL show)
{
	if (pCtrl)
	{
		if (show)
		{
			pCtrl->SetTooltip(tool_tip);
			HICON hIcon = ExtractIcon(AfxGetInstanceHandle(), resource_dll_fn, icon_id);
			if (hIcon) pCtrl->SetCustomIcon(hIcon);
		}
		else
			pCtrl->SetVisible(FALSE);
	}
}

CString getToolBarResourceFN(void)
{
	CString sPath;
	CString sToolBarResPath;
	::GetModuleFileName(AfxGetApp()->m_hInstance, sPath.GetBufferSetLength(_MAX_PATH), _MAX_PATH);
	sPath.ReleaseBuffer();

	int nIndex  = sPath.ReverseFind(_T('\\'));
	if (nIndex > 0) sPath = sPath.Left(nIndex + 1); 
	else sPath.Empty();

	sToolBarResPath.Format(_T("%s%s"),sPath,TOOLBAR_RES_DLL);

	return sToolBarResPath;
}

void setupForDBConnection(HWND hWndReciv,HWND hWndSend)
{
	if (hWndReciv != NULL)
	{
		DB_CONNECTION_DATA data;
    COPYDATASTRUCT HSData;
    memset(&HSData, 0, sizeof(COPYDATASTRUCT));
		HSData.dwData = 1;
		HSData.cbData = sizeof(DB_CONNECTION_DATA);
		HSData.lpData = &data;

		data.conn = NULL;

		::SendMessage(hWndReciv,WM_COPYDATA,(WPARAM)hWndSend, (LPARAM)&HSData);
	}
}


BOOL hitTest_X(int hit_x,int limit_x,int w_x)
{
	BOOL bHitX_OK = FALSE;
	// Check if hit, in x, is within lim�ts; 080513 p�d
	// if w_x = 0, don't check this value; 080513 p�d
	if (w_x == 0) bHitX_OK = TRUE;
	else if (w_x > 0)
	{
		bHitX_OK = (hit_x >= limit_x && hit_x <= limit_x+w_x);
	}

	return bHitX_OK;

}

CString getDefaults(int def_value,CVecDefaults& vec,CVecCalcTypes& vec1)
{
	if (vec.size() > 0)
	{
		for (UINT i = 0;i < vec.size();i++)
		{
			if (def_value == DEF_SAWLOG || def_value == DEF_PULPWOOD)
			{
				if (vec[i].getDefvalueID() == def_value)
				{
					if (vec1.size() > 0)
					{
						for (UINT j = 0;j < vec1.size();j++)
						{
							if (vec[i].getDefStr().CompareNoCase(vec1[j].getCalcType()) == 0)
								return vec1[j].getCalcType();
						}
					}
					else
						return L"";
				}
					
			}
			else
			{
				if (vec[i].getDefvalueID() == def_value)
					return vec[i].getDefStr();
			}
		}
	}

	return L"";
}

//#4258 h�mta FuncID fr�n defaultv�rdena, g�ller SW och PW
int getDefaultFuncID(int def_value,CVecDefaults& vec)
{
	CString S;
	if (vec.size() > 0)
	{
		for (UINT i = 0;i < vec.size();i++)
		{
				if (vec[i].getDefvalueID() == def_value)
				{
					if(vec[i].getDefFuncID() > 0)
						return vec[i].getDefFuncID();
					else
						return -1;
				}
		}
	}

	return -1;
}


CString getDefaults(int def_value,CVecDefaults& vec,vecFuncDesc& vec1)
{
	CString S;
	if (vec.size() > 0)
	{
		for (UINT i = 0;i < vec.size();i++)
		{
			if (def_value == DEF_SAWLOG || def_value == DEF_PULPWOOD)
			{
				if (vec[i].getDefvalueID() == def_value)
				{
					if (vec1.size() > 0)
					{
						for (UINT j = 0;j < vec1.size();j++)
						{
							if(vec[i].getDefFuncID() > 0)	//#4258 �ndrat, tittar p� FuncID ist�llet
							{
								if(vec[i].getDefFuncID() == vec1[j].getFuncID())
									return vec1[j].getBasis();
							}
							else
							{
								if (vec[i].getDefStr().CompareNoCase(vec1[j].getAbbrevName()) == 0)
									return vec1[j].getBasis();
							}
						}
					}
					else
						return L"";
				}	
			}
			else
			{
				if (vec[i].getDefvalueID() == def_value)
					return vec[i].getDefStr();
			}
		}
	}

	return L"";
}


CString getRegisterValue(int reg_type,int reg_id,CVecRegister& vec)
{
	if (reg_id == -1)
		return L"";
	if (vec.size() > 0)
	{
		for (UINT i = 0;i < vec.size();i++)
		{
			if (vec[i].getTypeID() == reg_type && vec[i].getPKID() == reg_id)
				return vec[i].getName();
		}
	}

	return L"";
}

int getRegisterID(int reg_type,LPCTSTR reg_value,CVecRegister& vec)
{
	if (vec.size() > 0)
	{
		for (UINT i = 0;i < vec.size();i++)
		{
			if (vec[i].getTypeID() == reg_type && vec[i].getName().CompareNoCase(reg_value) == 0)
				return vec[i].getPKID();
		}
	}

	return -1;
}

void tokenizeString(LPCTSTR str,TCHAR c,CStringArray& items)
{
	TCHAR szLine[255];
	CString sToken = L"";
	int nTokenCnt = 0;
	
	items.RemoveAll();
	memset(szLine,0,255);
	_tcscpy(szLine,str);
	// Count tokens in string
	for (TCHAR *p = szLine;p < _tcslen(szLine)+szLine;p++)
	{
		if (*p == c)
			nTokenCnt++;
	}
	for (int i = 0;i < nTokenCnt;i++)
	{
		AfxExtractSubString(sToken,szLine,i,c);
		items.Add(sToken);
	}

}


void setReportRowFntData(CXTPReportRecordItem* item,COLORREF text_color,COLORREF bgnd_color,BOOL bold,BOOL editable)
{
	if (item != NULL)
	{
		item->SetTextColor(text_color);
		item->SetBackgroundColor(bgnd_color);
		item->SetBold(bold);
		item->SetEditable(editable);
	}
}


BOOL getSTDReports(LPCTSTR fn,LPCTSTR add_to,int index,vecSTDReports &vec)
{
	vec.clear();
	if (fileExists(fn))
	{
		XMLShellData *pParser = new XMLShellData();
		if (pParser != NULL)
		{
			if (pParser->load(fn))
			{
				pParser->getReports(add_to,index,vec,L"");
			}
			delete pParser;
		}
	}
	return (vec.size() > 0);
}

void tmplString(CStdioFile& f,int id,int index,LPCTSTR value,bool comma)
{
	CString Str;
	if (comma)
		Str.Format(L"%d|%d|%s;\n",id,index,value);
	else
		Str.Format(L"%d|%d|%s\n",id,index,value);

	f.WriteString(Str);
}

void tmplString(CStdioFile& f,int id,int index,int value,bool comma)
{
	CString Str;
	if (comma)
		Str.Format(L"%d|%d|%d;\n",id,index,value);
	else
		Str.Format(L"%d|%d|%d\n",id,index,value);
	
	f.WriteString(Str);
}

void tmplString(CStdioFile& f,LPCTSTR value,bool comma)
{
	CString Str;
	if (comma)
		Str.Format(L"%s;\n",value);
	else
		Str.Format(L"%s\n",value);
	
	f.WriteString(Str);
}

void tmplString(CStdioFile& f,double value,bool comma,int dec)
{
	CString Str;
	if (comma)
		Str.Format(L"%.*f;\n",dec,value);
	else
		Str.Format(L"%.*f\n",dec,value);
	
	Str.Replace(L",",L".");

	f.WriteString(Str);
}

CString tmplDblString(double value,bool comma,int dec)
{
	CString Str;
	if (comma)
		Str.Format(L"%.*f;\n",dec,value);
	else
		Str.Format(L"%.*f\n",dec,value);
	
	Str.Replace(L",",L".");

	return Str;
}

int numOfInRegister(REGISTER_TYPES::REGS item,CVecRegister& vec)
{
	int nCnt = 0;
	if (vec.size() > 0)
	{
		for (UINT i = 0;i < vec.size();i++)
		{
			if (vec[i].getTypeID() == item)
				nCnt++;
		}
	}
	return nCnt;
}

BOOL License(void)
{
	CString sLangFN;
	CString sHeadline;
	CString sNoLicenseMsg;

	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
	if (fileExists(sLangFN))
	{
		RLFReader xml;
		if (xml.Load(sLangFN))
		{
			sHeadline = xml.str(IDS_STRING10);
		}
		xml.clean();
	}

	TCHAR szBuf[MAX_PATH], szBuf2[MAX_PATH];
	GetModuleFileName(hInst, szBuf, sizeof(szBuf) / sizeof(TCHAR));
	_tsplitpath(szBuf, NULL, NULL, szBuf2, NULL);

	// check if we have a valid license.
	_user_msg msg(820, _T("CheckLicense"), _T("License.dll"), _T("H2301"), sHeadline, (bShowLicenseDialog ? _T("1") : _T("0")),&szBuf2);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4, (LPARAM)&msg);

	// Check if there's a license-dll present; 090119 p�d
	if (_tcscmp(msg.getFunc(),_T("CheckLicense")) == 0)
	{
		// Commented out 2009-04-02 P�D, NOT USED
		//AfxMessageBox(sNoLicenseMsg);
		return FALSE;
	}	// if (_tcscmp(msg.getFunc(),_T("CheckLicense")) == 0)

	if (bShowLicenseDialog)
		bShowLicenseDialog = FALSE;

	return (_tcscmp(msg.getFunc(), _T("1")) == 0);	// License ok; 081210 p�d

}

CString localeToStr(LPCTSTR value)
{
	CString sDecPnt(getLocaleDecimalPoint());
	CString sValue(value);
	if (sValue.Find(L",") && sDecPnt == L".")	sValue.Replace(L",",sDecPnt);
	else if (sValue.Find(L".") && sDecPnt == L",")	sValue.Replace(L".",sDecPnt);

	return sValue;
}

CString localeToStr(double value)
{
	CString sValue = L"";
	CString sDecPnt(getLocaleDecimalPoint());
	sValue.Format(L"%f",value);
	if (sValue.Find(L",") && sDecPnt == L".")	sValue.Replace(L",",sDecPnt);
	else if (sValue.Find(L".") && sDecPnt == L",")	sValue.Replace(L".",sDecPnt);

	return sValue;
}

void saveColumnsSettingToFile(LPCTSTR reg_key,CXTPReportControl &rep,ENUM_COL_SETTINGS_TYPES save_as)
{
  CStdioFile fOut;
	CString sPath = L"";
	if (save_as == COLSET_TICKET)
		sPath.Format(L"%s\\%s\\%s.txt",getMyDocumentsDir(),LOGS_COLUMNS_DIRECTORY,reg_key);
	else if (save_as == COLSET_SALES)
		sPath.Format(L"%s\\%s\\%s.txt",getMyDocumentsDir(),SALES_COLUMNS_DIRECTORY,reg_key);
	fOut.Open(sPath, CFile::modeCreate | CFile::modeWrite);	
	// Save Tickets columnsettings to register
	CXTPReportColumns *pCols = rep.GetColumns();
	if (pCols != NULL)
	{
		CString sCap = L"",sRegStrCap = L"";
		for (int i = 0;i < pCols->GetCount();i++)
		{

			sCap = pCols->GetAt(i)->GetCaption();
			if (sCap.Find('\n',0) > -1)
				sCap.Delete(sCap.Find('\n',0),sCap.GetLength()-sCap.Find('\n',0));

			sRegStrCap.Format(L"%d;%d;%s;%d;\n",pCols->GetAt(i)->GetIndex(),pCols->GetAt(i)->GetItemIndex(),sCap,pCols->GetAt(i)->IsVisible());
			fOut.WriteString(sRegStrCap);
		}
	}
	fOut.Close();
}

void readColumnsSettingFromFile(LPCTSTR reg_key,vecColumnsSet &vec,ENUM_COL_SETTINGS_TYPES save_as)
{
  CStdioFile fIn;
	CString sPath = L"",S;
	TCHAR szLine[128];
	int nTokenCnt = 0;
	CString sValue;
	CString sIndex;
	CString sColIndex;
	CString sCaption;
	CString sVisible;

	if (save_as == COLSET_TICKET)
		sPath.Format(L"%s\\%s\\%s.txt",getMyDocumentsDir(),LOGS_COLUMNS_DIRECTORY,reg_key);
	else if (save_as == COLSET_SALES)
		sPath.Format(L"%s\\%s\\%s.txt",getMyDocumentsDir(),SALES_COLUMNS_DIRECTORY,reg_key);
	if (fileExists(sPath))
	{
		fIn.Open(sPath, CFile::modeRead | CFile::shareDenyWrite);
		while (fIn.ReadString(sValue))
		{
			wcscpy(szLine,sValue);

			nTokenCnt = 0;
			for (TCHAR *p=szLine;p < _tcslen(szLine) + szLine;p++)
			{
				if (*p == ';')
					nTokenCnt++;
			}
			
			// Make sure we have correct number of items
			if (nTokenCnt == 4)
			{
				AfxExtractSubString(sIndex,szLine,0,';');
				AfxExtractSubString(sColIndex,szLine,1,';');
				AfxExtractSubString(sCaption,szLine,2,';');
				AfxExtractSubString(sVisible,szLine,3,';');

				vec.push_back(ColumnsSet(_tstoi(sIndex),_tstoi(sColIndex),sCaption,_tstoi(sVisible)));

			}
		}
	fIn.Close();
	}
}

BOOL delColumnsSettingFile(LPCTSTR reg_key,ENUM_COL_SETTINGS_TYPES save_as)
{
	CString sPath = L"";
	if (save_as == COLSET_TICKET)
		sPath.Format(L"%s\\%s\\%s.txt",getMyDocumentsDir(),LOGS_COLUMNS_DIRECTORY,reg_key);
	else if (save_as == COLSET_SALES)
		sPath.Format(L"%s\\%s\\%s.txt",getMyDocumentsDir(),SALES_COLUMNS_DIRECTORY,reg_key);
	if (fileExists(sPath))
	{
		removeFile(sPath);
	}

	return TRUE;
}

int InsertRow(CListCtrl &ctrl,int nPos,bool checked,int nNoOfCols, LPCTSTR pText, ...)
{
    va_list argList;
    va_start(argList, pText);

    ASSERT(nNoOfCols >= 1); // use the 'normal' Insert function with 1 argument
    int nIndex = ctrl.InsertItem(LVIF_TEXT|LVIF_STATE, nPos, pText,0,LVIS_SELECTED,0,0);
    ASSERT(nIndex != -1);
    if (nIndex < 0) return(nIndex);
    for (int i = 1; i < nNoOfCols; i++) 
		{
        LPCTSTR p = va_arg(argList,LPCTSTR);
        if (p != NULL) 
				{
					ctrl.SetItemText(nIndex, i, p);   
					ctrl.SetCheck(nPos,checked);
        }
    }
    va_end(argList);
    return(nIndex);
}

int GetSelectedItem(CListCtrl &ctrl)
{
	int nItem = -1;
  POSITION nPos = ctrl.GetFirstSelectedItemPosition();
  if (nPos)
  {
		nItem = ctrl.GetNextSelectedItem(nPos);
  }
  return nItem;
}

int GetSelectedItemByCB(CListCtrl &ctrl)
{
	CPoint pt;
	GetCursorPos(&pt);
	ScreenToClient(ctrl.GetSafeHwnd(),&pt);
	UINT uFlags = 0;
	int iItem = ctrl.HitTest(pt, &uFlags);
	UINT uFlag1 = LVHT_ONITEMSTATEICON & uFlags;
	if((LVHT_ONITEMSTATEICON & uFlags) == LVHT_ONITEMSTATEICON)
	{
	  return iItem;
	}

	return -1;	// No found
}



void bkc(CXTPReportRecordItem *pRecItem,COLORREF bkcol,BOOL editable)
{
	if (pRecItem != NULL)
	{
		pRecItem->SetEditable(editable);
		pRecItem->SetBackgroundColor(bkcol);
	}
}

int createUniqueNumber(CVecUserVolTables &vec)
{
	bool bOK = true;
	srand((unsigned)time(0)); 
	int lowest=1000, highest=32000; 
	int range=(highest-lowest)+1; 
	int random_int = lowest+int(range*rand()/(RAND_MAX + 1.0)); 
	if (vec.size() == 0)
	{
		return random_int;
	}
	// Make sure this is an unique number in table
	else
	{
		do
		{
			bOK = true;
			for (UINT i = 0;i < vec.size();i++)
			{
				if (vec[i].getFuncID() == random_int)
				{
					bOK = false;
					break;
				}
			}
			// Generate a new number and check to
			// see if it's unique in table
			if (!bOK)
			{
				random_int = lowest+int(range*rand()/(RAND_MAX + 1.0)); 
			}
		} while (!bOK);
		return random_int;
	}
}

BOOL getIsUserVolFunctionUsed(LPCTSTR tmpl,int func_id)
{
	BOOL bFound = FALSE;
	CString sData(tmpl);
	CString sToken = L"";
	int nCnt = 0;
	while (AfxExtractSubString(sToken,sData,nCnt,';'))
	{
		if (_tstoi(sToken) == func_id)
			bFound = TRUE;
		nCnt++;
	}

	return bFound;
}

////////////////////////////////////////////////////////////////////////////////////
bool srtDataAsc(CFuncDesc& l1, CFuncDesc& l2)
{
	return l1.getFuncID() < l2.getFuncID();
}


////////////////////////////////////////////////////////////////////////////////////
// Check FUNC_INDEX::SPECIES (1001) if prl data
BOOL getIsSpeciesInTable(LPCTSTR prl,int spc_id)
{
	CString sData(prl);
	std::wstringstream ss;
	CString sToken = L"";
	std::wstring line;
	ss << sData.GetBuffer();
	while (std::getline(ss,line))
	{
		AfxExtractSubString(sToken,line.c_str(),0,';');
		if (_tstoi(sToken) == FUNC_INDEX::SPECIES)
		{
			AfxExtractSubString(sToken,line.c_str(),1,';');
			if (_tstoi(sToken) == spc_id)
				return TRUE;
		}
	}
	return FALSE;
}

BOOL getIsGradeInTable(LPCTSTR prl,int grade_id)
{
	CString sData(prl);
	std::wstringstream ss;
	CString sToken = L"";
	std::wstring line;
	ss << sData.GetBuffer();
	while (std::getline(ss,line))
	{
		AfxExtractSubString(sToken,line.c_str(),0,';');
		if (_tstoi(sToken) == FUNC_INDEX::DATA)
		{
			AfxExtractSubString(sToken,line.c_str(),2,';');
			if (_tstoi(sToken) == grade_id)
				return TRUE;
		}
	}
	return FALSE;
}

int getNumOfSpcInTable(LPCTSTR prl)
{
	CString sData(prl);
	int nCounter = 0;
	std::wstringstream ss;
	CString sToken = L"";
	std::wstring line;
	ss << sData.GetBuffer();
	while (std::getline(ss,line))
	{
		AfxExtractSubString(sToken,line.c_str(),0,';');
		if (_tstoi(sToken) == FUNC_INDEX::SPECIES)
		{
			nCounter++;
		}
	}
	return nCounter;
}

BOOL getPricesForSpeciesInTemplate(LPCTSTR prl,int spc_id,vecDouble& vec)
{
	BOOL bFoundSpecies = FALSE;
	CString sData(prl);
	int nCounter = 0;
	std::wstringstream ss;
	CString sToken = L"",sSpc = L"",sPrice = L"";
	std::wstring line;
	ss << sData.GetBuffer();
	vec.clear();
	while (std::getline(ss,line))
	{
		AfxExtractSubString(sToken,line.c_str(),0,';');
		if (_tstoi(sToken) == FUNC_INDEX::SPECIES)
		{
			AfxExtractSubString(sToken,line.c_str(),1,';');
			if (_tstoi(sToken) == spc_id)
			{
				bFoundSpecies = TRUE;
			}
		}
		if (bFoundSpecies)
		{
			AfxExtractSubString(sToken,line.c_str(),0,';');
			if (_tstoi(sToken) == FUNC_INDEX::DATA)
			{
				AfxExtractSubString(sSpc,line.c_str(),1,';');
				if (_tstoi(sSpc) == spc_id)
				{
					AfxExtractSubString(sPrice,line.c_str(),3,';');
					vec.push_back(_tstof(sPrice));
				}
			}
		}
	}
	return (vec.size() > 0);
}

BOOL getPricelistFromTemplate(LPCTSTR prl,int spc_id,vecPricelistInTicket& vec)
{
	BOOL bFoundSpecies = FALSE;
	CString sData(prl);
	int nCounter = 0;
	std::wstringstream ss;
	CString sToken = L"",sSpc = L"",sGrade = L"",sPrice = L"",sFuncID = L"";
	std::wstring line;
	ss << sData.GetBuffer();
	vec.clear();
	while (std::getline(ss,line))
	{
		AfxExtractSubString(sToken,line.c_str(),0,';');
		if (_tstoi(sToken) == FUNC_INDEX::SPECIES)
		{
			AfxExtractSubString(sToken,line.c_str(),1,';');
			if (_tstoi(sToken) == spc_id || spc_id == -1)
			{
				bFoundSpecies = TRUE;
			}
		}
		if (bFoundSpecies)
		{
			AfxExtractSubString(sToken,line.c_str(),0,';');
			if (_tstoi(sToken) == FUNC_INDEX::DATA)
			{
				AfxExtractSubString(sSpc,line.c_str(),1,';');
				if (_tstoi(sSpc) == spc_id || spc_id == -1)
				{
					AfxExtractSubString(sGrade,line.c_str(),2,';');
					AfxExtractSubString(sPrice,line.c_str(),3,';');
					AfxExtractSubString(sFuncID,line.c_str(),4,';');
					vec.push_back(CPricelistInTicket(_tstoi(sSpc),_tstoi(sGrade),_tstof(sPrice),_tstoi(sFuncID) ) );
				}
			}
		}
	}
	return (vec.size() > 0);
}


BOOL getSpeciesFromTemplate(LPCTSTR prl,vecPricelistInTicket& vec)
{
	BOOL bFoundSpecies = FALSE;
	CString sData(prl);
	int nCounter = 0;
	std::wstringstream ss;
	CString sToken = L"";
	std::wstring line;
	ss << sData.GetBuffer();
	vec.clear();
	while (std::getline(ss,line))
	{
		AfxExtractSubString(sToken,line.c_str(),0,';');
		if (_tstoi(sToken) == FUNC_INDEX::SPECIES)
		{
			AfxExtractSubString(sToken,line.c_str(),1,';');
			vec.push_back(CPricelistInTicket(_tstoi(sToken),-1,0.0,-1 ) );
		}
	}
	return (vec.size() > 0);
}

BOOL isIncludedInTemplate(int reg_type,int pk_id,vecLogScaleTemplates& vec)
{
	BOOL bReturn = FALSE;
	if (vec.size() == 0 || pk_id == -1)
		return FALSE;

	for (UINT i = 0;i < vec.size();i++)
	{
		switch(reg_type)
		{
			case REGISTER_TYPES::BUYER:
				if (vec[i].getBuyer() == pk_id)
				{
					bReturn = TRUE;
				}
				break;
			case REGISTER_TYPES::HAULER:
				if (vec[i].getHauler() == pk_id)
				{
					bReturn = TRUE;
				}
				break;
			case REGISTER_TYPES::LOCATION:
				if (vec[i].getLocation() == pk_id)
				{
					bReturn = TRUE;
				}
				break;
			case REGISTER_TYPES::TRACTID:
				if (vec[i].getTractID() == pk_id)
				{
					bReturn = TRUE;
				}
				break;
			case REGISTER_TYPES::SCALER:
				if (vec[i].getScaler() == pk_id)
				{
					bReturn = TRUE;
				}
				break;
			case REGISTER_TYPES::SOURCEID:
				if (vec[i].getSourceID() == pk_id)
				{
					bReturn = TRUE;
				}
				break;
			case REGISTER_TYPES::VENDOR:
				if (vec[i].getVendor() == pk_id)
				{
					bReturn = TRUE;
				}
				break;
			case REGISTER_TYPES::SUPPLIER:
				if (vec[i].getSupplier() == pk_id)
				{
					bReturn = TRUE;
				}
				break;
		};
		if (bReturn)
			break;
	}

	return bReturn;
	
}

void cbRegisterData(int reg_id,CComboBox* cb,CVecRegister& vec,int sel_id,bool add_empty_line)
{
	int nCnt = 0;
	CString sValue = L"";
	if (cb != NULL)
	{
		cb->ResetContent();	// Clear
		if (vec.size() > 0)
		{
			if (add_empty_line)
			{
				cb->AddString(L"");
				cb->SetItemData(nCnt,(DWORD)-1);	// Primary key
				nCnt++;
			}
			for (UINT i = 0;i < vec.size();i++)
			{
				if (vec[i].getTypeID() == reg_id)
				{
					cb->AddString(vec[i].getName());
					cb->SetItemData(nCnt,(DWORD)vec[i].getPKID());	// Primary key
					if (vec[i].getPKID() == sel_id)
						sValue = vec[i].getName();

					nCnt++;
				}
			}
		}

		if (cb->GetCount() > 0 && sel_id > -1)
		{
			cb->SetCurSel(cb->FindString(0,sValue));
		}
		else
		{
			cb->SetCurSel(-1);
		}
	}
}

int cbGetID(CComboBox* cb)
{
	int nValue = -1;
	if (cb != NULL)
	{
		if (cb->GetCount() > 0)
		{
			nValue = (int)cb->GetItemData(cb->GetCurSel());
			return nValue;
		}
	}
	return -1;
}

// Get value from registry, based on index in ComboBox
int cbGetRegisterID(int reg_id,CComboBox* cb,CVecRegister& vec)
{
	int idx = cb->GetCurSel(),id = -1;
	CString sValue = L"";
	if (idx > CB_ERR)
	{
		cb->GetWindowTextW(sValue);

		id = getRegisterID(reg_id,sValue,vec);

		return id;
	}

	return -1;
}

////////////////////////////////////////////////////////////////////////////////////
// Function in LogScaleVolFunc.dll
//BOOL getDLLVolumeFuncDesc(vecFuncDesc &func_list)
BOOL getDLLVolumeFuncDesc(vecFuncDesc &vec,CVecUserVolTables user_vol_tables)
{
	BOOL bReturn = FALSE;

	typedef CRuntimeClass *(*Func1)(vecFuncDesc&);
  Func1 proc1;

	CString sModule;
	
	sModule.Format(_T("%s%s\\%s"),getProgDir(),SUBDIR_MODULES,LOGSCALE_VOLUME_MODULE);

	//func_list.clear();
	HINSTANCE hModule = AfxLoadLibrary(sModule);
	if (hModule != NULL)
	{
		proc1 = (Func1)GetProcAddress((HMODULE)hModule, LOGSCALE_FUNCDESC_METHOD);
		if (proc1 != NULL)
		{
			proc1(vec);
			bReturn = TRUE;
		}	// if (proc != NULL)

		AfxFreeLibrary(hModule);
	}	// if (hModule != NULL)

	//sort(vec.begin(),vec.end(),srtDataAsc);

	if (user_vol_tables.size() > 0)
	{
		for (UINT i = 0;i < user_vol_tables.size();i++)
		{
			vec.push_back(CFuncDesc(user_vol_tables[i].getFuncID(),user_vol_tables[i].getFullName(),user_vol_tables[i].getAbbrevName(),user_vol_tables[i].getBasisName(),1));
		}	// for (UINT i = 0;i < user_vol_tables.size();i++)
	}	// if (user_vol_tables.size() > 0)


	return bReturn;
}

////////////////////////////////////////////////////////////////////////////////////
// Function in LogScaleVolFunc.dll

BOOL calculateDLLVolumeLogs(CLogs &rec,vecExtraVolFuncID &vec,CVecUserVolTables &vec1,double taper_ib,int use_southern_doyle,int measure_mode,int deduction,double trim)
{
	BOOL bReturn = FALSE;

	typedef CRuntimeClass *(*Func1)(CLogs &,vecExtraVolFuncID &,CVecUserVolTables &,double,int,int,int,double);
  Func1 proc1;

	CString sModule;
	
	sModule.Format(_T("%s%s\\%s"),getProgDir(),SUBDIR_MODULES,LOGSCALE_VOLUME_MODULE);

	//func_list.clear();
	HINSTANCE hModule = AfxLoadLibrary(sModule);
	if (hModule != NULL)
	{
		proc1 = (Func1)GetProcAddress((HMODULE)hModule, LOGSCALE_CALCULATE_METHOD2);
		if (proc1 != NULL)
		{
			proc1(rec,vec,vec1,taper_ib,use_southern_doyle,measure_mode,deduction,trim);
			bReturn = TRUE;
		}	// if (proc != NULL)

		AfxFreeLibrary(hModule);
	}	// if (hModule != NULL)

	return bReturn;
}


// Use this on manually creted Tickets
CString generateTicketNumber(int num)
{
	CString sTmp = L"";
	if (num > 1)
		sTmp.Format(L"T-%s-%04d",getYearMonth().Right(4),num);
	else
		sTmp.Format(L"T-%s-%04d",getYearMonth().Right(4),1);

	return sTmp;
}

// Use this on manually creted Sales
CString generateSalesNumber(int num)
{
	CString sTmp = L"";
	if (num > 1)
		sTmp.Format(L"SL-%s-%04d",getYearMonth().Right(4),num);
	else
		sTmp.Format(L"SL-%s-%04d",getYearMonth().Right(4),1);

	return sTmp;
}


CString getFilePath(LPCTSTR path)
{
  TCHAR drive[_MAX_DRIVE];
  TCHAR dir[_MAX_DIR];
  TCHAR fname[_MAX_FNAME];
  TCHAR ext[_MAX_EXT];

	_wsplitpath(path,drive,dir,fname,ext);

	CString sPath(dir),sDrive(drive);

	return sDrive+sPath;
}
// Check only Caliper tag-files
int getStatusFromTagFile(LPCTSTR path,int def_status)
{
	int nStatus = -1;
	CString sPath(path),sFileName = L"",sBuyer = L"";

	if (sPath.Right(4) == _T(".xls") || sPath.Right(5) == _T(".xlsx") )
	{
		return def_status;
	}
	if (sPath.Right(4) == _T(".lsb"))
	{
		// Setup sarrTags from text-file; 2011-12-06 p�d
		CStdioFile f;
		CFileException ex;

		CString sLine = L"";
		CString sToken = L"";
		BOOL bStart = FALSE;
		if (!f.Open(sPath,CFile::modeRead,&ex))
		{
			TCHAR err[1024];
			ex.GetErrorMessage(err,1024);
			AfxMessageBox(err);
			return -1;
		}
		while (f.ReadString(sLine))
		{
			sLine.Replace(':',';');
			sLine.Remove(' ');

			AfxExtractSubString(sToken,sLine,0,';');
			if (sToken.CompareNoCase(L"TypeID") == 0 || sToken.CompareNoCase(L"Type ID") == 0 )
			{
				AfxExtractSubString(sToken,sLine,1,';');
				nStatus = _tstoi(sToken);
			}
		}
	}

	return nStatus;
}


CString getDateExt(short type)
{
	CString sDate;
	CTime t = CTime::GetCurrentTime();
	if (type == 0)	// ENU
	{
		sDate.Format(_T("%02d/%02d/%04d"),t.GetDay(),t.GetMonth(),t.GetYear());
	}
	else
	{
		sDate.Format(_T("%04d-%02d-%02d"),t.GetYear(),t.GetMonth(),t.GetDay());
	}

	return sDate;
}

//////////////////////////////////////////////////////////////////////////////////////////
// Derived class CMacroEdit
BEGIN_MESSAGE_MAP(CMacroEdit, CXTEdit)
	//{{AFX_MSG_MAP(CMyExtEdit)
	ON_CONTROL_REFLECT(EN_UPDATE, OnUpdate)
	ON_WM_CHAR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CMacroEdit::CMacroEdit() 
	: CXTEdit(),
	valid_c(NULL)
{
}

CMacroEdit::~CMacroEdit() 
{ 
	if (valid_c != NULL)
	{
		free(valid_c); 
	}
}

void CMacroEdit::OnUpdate() 
{
	this->RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN | RDW_UPDATENOW);
}

void CMacroEdit::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if (isCharValid(nChar) || nChar == 8 /* backspace*/ || nChar == 32 /*space*/)
		CEdit::OnChar(nChar,nRepCnt,nFlags);
}


//#3611 Koll om ogiltiga tecken finns i str�ngen
bool checkIllegalChars(LPCTSTR str)
{
	bool bRet = true;

	CString cStr(str);
	
	if(cStr.FindOneOf(_T("\\/?:*\"><|")) == -1)
		bRet = false;

	return bRet;
}