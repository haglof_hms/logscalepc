// SelectSpeciesAndGradesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SelectSpeciesAndGradesDlg.h"

/*
// CSelectSpeciesAndGradesDlg dialog

IMPLEMENT_DYNAMIC(CSelectSpeciesAndGradesDlg, CDialog)

CSelectSpeciesAndGradesDlg::CSelectSpeciesAndGradesDlg(CWnd* pParent /*=NULL* /)
	: CDialog(CSelectSpeciesAndGradesDlg::IDD, pParent)
{

}

CSelectSpeciesAndGradesDlg::~CSelectSpeciesAndGradesDlg()
{
}

void CSelectSpeciesAndGradesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CSelectSpeciesAndGradesDlg, CDialog)
END_MESSAGE_MAP()


// CSelectSpeciesAndGradesDlg message handlers
*/

#include "ResLangFileReader.h"

// CSelectSpeciesAndGradesDlg dialog

IMPLEMENT_DYNAMIC(CSelectSpeciesAndGradesDlg, CDialog)

BEGIN_MESSAGE_MAP(CSelectSpeciesAndGradesDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CSelectSpeciesAndGradesDlg::OnBnClickedOk)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST2, &CSelectSpeciesAndGradesDlg::OnLvnItemchangedList2)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST3, &CSelectSpeciesAndGradesDlg::OnLvnItemchangedList2)
	ON_BN_CLICKED(IDC_BUTTON1, &CSelectSpeciesAndGradesDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CSelectSpeciesAndGradesDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CSelectSpeciesAndGradesDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CSelectSpeciesAndGradesDlg::OnBnClickedButton4)
END_MESSAGE_MAP()

CSelectSpeciesAndGradesDlg::CSelectSpeciesAndGradesDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectSpeciesAndGradesDlg::IDD, pParent),
	m_bInitialized(FALSE),
	m_bEnableSpecies(FALSE),
	m_bEnableGrades(FALSE)
{

}

CSelectSpeciesAndGradesDlg::~CSelectSpeciesAndGradesDlg()
{
}

void CSelectSpeciesAndGradesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LIST2, m_wndSpcListCtrl);
	DDX_Control(pDX, IDC_LIST3, m_wndGradeListCtrl);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_LBL14_1, m_lbl14_1);
	DDX_Control(pDX, IDC_BUTTON1, m_btn1);
	DDX_Control(pDX, IDC_BUTTON2, m_btn2);
	DDX_Control(pDX, IDC_BUTTON3, m_btn3);
	DDX_Control(pDX, IDC_BUTTON4, m_btn4);
	//}}AFX_DATA_MAP
}

BOOL CSelectSpeciesAndGradesDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CDialog::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

BOOL CSelectSpeciesAndGradesDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (!m_bInitialized)
	{

		m_lbl14_1.SetBkColor(INFOBK);
		m_lbl14_1.SetTextColor(BLUE);
		
		setup();

		m_bInitialized = TRUE;
	}

	return TRUE;
}

// CSelectSpeciesAndGradesDlg message handlers

void CSelectSpeciesAndGradesDlg::setup(void)
{
	int nCnt = 0;
	CString sSpcID;
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			SetWindowText(xml.str(IDS_STRING4350));

			m_btn1.SetWindowTextW(xml.str(IDS_STRING4353));
			m_btn2.SetWindowTextW(xml.str(IDS_STRING4354));
			m_btn3.SetWindowTextW(xml.str(IDS_STRING4353));
			m_btn4.SetWindowTextW(xml.str(IDS_STRING4354));

			m_lbl14_1.SetWindowTextW(xml.str(IDS_STRING4360));

			m_btnOK.SetWindowTextW(xml.str(IDS_STRING100));
			m_btnCancel.SetWindowTextW(xml.str(IDS_STRING101));

			m_btnOK.EnableWindow(m_bEnableSpecies && m_bEnableGrades);

			m_wndSpcListCtrl.InsertColumn(0, L"", LVCFMT_CENTER, 30);
			m_wndSpcListCtrl.InsertColumn(1, xml.str(IDS_STRING4351), LVCFMT_LEFT, 50);
			m_wndSpcListCtrl.InsertColumn(2, xml.str(IDS_STRING4352), LVCFMT_LEFT, 100);

			m_wndGradeListCtrl.InsertColumn(0, L"", LVCFMT_CENTER, 30);
			m_wndGradeListCtrl.InsertColumn(1, xml.str(IDS_STRING4351), LVCFMT_LEFT, 50);
			m_wndGradeListCtrl.InsertColumn(2, xml.str(IDS_STRING4352), LVCFMT_LEFT, 120);

		}
		xml.clean();
	}

	// Get the windows handle to the header control for the
	// list control then subclass the control.
	HWND hWndSpcHeader = m_wndSpcListCtrl.GetDlgItem(0)->GetSafeHwnd();
	m_spcHeader.SubclassWindow(hWndSpcHeader);

	m_spcHeader.SetTheme(new CXTHeaderCtrlThemeOfficeXP());

	m_wndSpcListCtrl.ModifyExtendedStyle(0,LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT);

	if (m_vecSpecies.size() > 0)
	{
		for (UINT i = 0;i < m_vecSpecies.size();i++)
		{
//			if (!isSpecieUsed(m_vecSpecies[i].getSpcID()))
//			{
				m_wndSpcListCtrl.InsertItem(nCnt, _T(""), 0);
				m_wndSpcListCtrl.SetItem(nCnt,1, LVIF_TEXT, m_vecSpecies[i].getSpcCode(), 0, NULL, NULL, NULL);
				m_wndSpcListCtrl.SetItem(nCnt,2, LVIF_TEXT, (m_vecSpecies[i].getSpcName()), 0, NULL, NULL, NULL);
				m_wndSpcListCtrl.SetItemData(nCnt,m_vecSpecies[i].getSpcID());
				nCnt++;
//			}
		}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
	}	// if (m_vecSpecies.size() > 0)

	nCnt = 0;
	HWND hWndGradeHeader = m_wndGradeListCtrl.GetDlgItem(0)->GetSafeHwnd();
	m_gradeHeader.SubclassWindow(hWndGradeHeader);

	m_gradeHeader.SetTheme(new CXTHeaderCtrlThemeOfficeXP());

	m_wndGradeListCtrl.ModifyExtendedStyle(0,LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT);

	if (m_vecGrades.size() > 0)
	{
		for (UINT i = 0;i < m_vecGrades.size();i++)
		{
			m_wndGradeListCtrl.InsertItem(nCnt, _T(""), 0);
			m_wndGradeListCtrl.SetItem(nCnt,1, LVIF_TEXT, m_vecGrades[i].getGradesCode(), 0, NULL, NULL, NULL);
			m_wndGradeListCtrl.SetItem(nCnt,2, LVIF_TEXT, (m_vecGrades[i].getGradesName()), 0, NULL, NULL, NULL);
			m_wndGradeListCtrl.SetItemData(nCnt,m_vecGrades[i].getGradesID());
			nCnt++;
		}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
	}	// if (m_vecSpecies.size() > 0)

	// Set species selected
	if (m_vecSelectedSpecies.size() > 0)
	{
		for (int i1 = 0;i1 < m_wndSpcListCtrl.GetItemCount();i1++)
		{
			for (UINT i2 = 0;i2 < m_vecSelectedSpecies.size();i2++)
			{
				if (m_vecSelectedSpecies[i2].getSpcID() == (int)m_wndSpcListCtrl.GetItemData(i1))
					m_wndSpcListCtrl.SetCheck(i1,TRUE);
			}
		}
	}
	// Set grades selected
	if (m_vecSelectedGradesIn.size() > 0)
	{
		for (int i1 = 0;i1 < m_wndGradeListCtrl.GetItemCount();i1++)
		{
			for (UINT i2 = 0;i2 < m_vecSelectedGradesIn.size();i2++)
			{
				if (m_vecSelectedGradesIn[i2].getGradesID() == (int)m_wndGradeListCtrl.GetItemData(i1))
					m_wndGradeListCtrl.SetCheck(i1,TRUE);
			}
		}
	}
}

BOOL CSelectSpeciesAndGradesDlg::isSpeciesSelected()
{
	// Get selected species and add to m_vecSelectedSpecies; 120103 p�d
	if (m_wndSpcListCtrl.GetItemCount() > 0 && m_vecSpecies.size() > 0)
	{
		for (int i = 0;i < m_wndSpcListCtrl.GetItemCount();i++)
		{
			if (m_wndSpcListCtrl.GetCheck(i))
			{
				return TRUE;
			}	// if (m_wndSpcListCtrl.GetCheck(i))
		}	// for (int i = 0;i < m_wndSpcListCtrl.GetItemCount();i++)
	}	// if (m_wndSpcListCtrl.GetItemCount() > 0 && m_vecSpecies.size() > 0)
	return FALSE;
}

BOOL CSelectSpeciesAndGradesDlg::isGradesSelected()
{
	if (m_wndGradeListCtrl.GetItemCount() > 0 && m_vecGrades.size() > 0)
	{
		for (int i = 0;i < m_wndGradeListCtrl.GetItemCount();i++)
		{
			if (m_wndGradeListCtrl.GetCheck(i))
			{
				return TRUE;
			}	// if (m_wndSpcListCtrl.GetCheck(i))
		}	// for (int i = 0;i < m_wndSpcListCtrl.GetItemCount();i++)
	}	// if (m_wndSpcListCtrl.GetItemCount() > 0 && m_vecSpecies.size() > 0)
	return FALSE;
}


void CSelectSpeciesAndGradesDlg::OnBnClickedOk()
{
	// Get selected species and add to m_vecSelectedSpecies; 120103 p�d
	int nSpcID = -1;
	m_vecSelectedSpecies.clear();

	if (m_wndSpcListCtrl.GetItemCount() > 0 && m_vecSpecies.size() > 0)
	{
		for (int i = 0;i < m_wndSpcListCtrl.GetItemCount();i++)
		{
			if (m_wndSpcListCtrl.GetCheck(i))
			{
				nSpcID = (int)m_wndSpcListCtrl.GetItemData(i);
				for (UINT i = 0;i < m_vecSpecies.size();i++)
				{
					if (m_vecSpecies[i].getSpcID() == nSpcID)
						m_vecSelectedSpecies.push_back(m_vecSpecies[i]);
				}
			}	// if (m_wndSpcListCtrl.GetCheck(i))
		}	// for (int i = 0;i < m_wndSpcListCtrl.GetItemCount();i++)
	}	// if (m_wndSpcListCtrl.GetItemCount() > 0 && m_vecSpecies.size() > 0)

	// Get selected grades and add to m_vecSelectedGradesOut; 120220 p�d
	int nGradeID = -1;
	m_vecSelectedGradesOut.clear();

	if (m_wndGradeListCtrl.GetItemCount() > 0 && m_vecGrades.size() > 0)
	{
		for (int i = 0;i < m_wndGradeListCtrl.GetItemCount();i++)
		{
			if (m_wndGradeListCtrl.GetCheck(i))
			{
				nGradeID = (int)m_wndGradeListCtrl.GetItemData(i);
				for (UINT i = 0;i < m_vecGrades.size();i++)
				{
					if (m_vecGrades[i].getGradesID() == nGradeID)
					{
						m_vecSelectedGradesOut.push_back(m_vecGrades[i]);
					}
				}
			}	// if (m_wndSpcListCtrl.GetCheck(i))
		}	// for (int i = 0;i < m_wndSpcListCtrl.GetItemCount();i++)
	}	// if (m_wndSpcListCtrl.GetItemCount() > 0 && m_vecSpecies.size() > 0)


	OnOK();
}

void CSelectSpeciesAndGradesDlg::OnLvnItemchangedList2(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;
	m_bEnableSpecies = isSpeciesSelected();
	m_bEnableGrades = isGradesSelected();
	m_btnOK.EnableWindow(m_bEnableSpecies && m_bEnableGrades);

}

void CSelectSpeciesAndGradesDlg::OnLvnItemchangedList3(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;
	m_bEnableSpecies = isSpeciesSelected();
	m_bEnableGrades = isGradesSelected();
	m_btnOK.EnableWindow(m_bEnableSpecies && m_bEnableGrades);

}

// Select all species
void CSelectSpeciesAndGradesDlg::OnBnClickedButton1()
{
	for (int i = 0;i < m_wndSpcListCtrl.GetItemCount();i++)
	{
		m_wndSpcListCtrl.SetCheck(i,TRUE);
	}
	m_bEnableSpecies = isSpeciesSelected();
	m_bEnableGrades = isGradesSelected();
	m_btnOK.EnableWindow(m_bEnableSpecies && m_bEnableGrades);
}

// Deselect all species
void CSelectSpeciesAndGradesDlg::OnBnClickedButton2()
{
	// TODO: Add your control notification handler code here
	for (int i = 0;i < m_wndSpcListCtrl.GetItemCount();i++)
	{
		m_wndSpcListCtrl.SetCheck(i,FALSE);
	}
	m_bEnableSpecies = isSpeciesSelected();
	m_bEnableGrades = isGradesSelected();
	m_btnOK.EnableWindow(m_bEnableSpecies && m_bEnableGrades);
}

// Select all grades
void CSelectSpeciesAndGradesDlg::OnBnClickedButton3()
{
	// TODO: Add your control notification handler code here
	for (int i = 0;i < m_wndGradeListCtrl.GetItemCount();i++)
	{
		m_wndGradeListCtrl.SetCheck(i,TRUE);
	}
	m_bEnableSpecies = isSpeciesSelected();
	m_bEnableGrades = isGradesSelected();
	m_btnOK.EnableWindow(m_bEnableSpecies && m_bEnableGrades);
}

// Deselect all grades
void CSelectSpeciesAndGradesDlg::OnBnClickedButton4()
{
	// TODO: Add your control notification handler code here
	for (int i = 0;i < m_wndGradeListCtrl.GetItemCount();i++)
	{
		m_wndGradeListCtrl.SetCheck(i,FALSE);
	}
	m_bEnableSpecies = isSpeciesSelected();
	m_bEnableGrades = isGradesSelected();
	m_btnOK.EnableWindow(m_bEnableSpecies && m_bEnableGrades);
}
