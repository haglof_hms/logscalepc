// RegisterSuppliersView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "TemplateExportView.h"

#include "LogScaleTemplatesFormView.h"

#include "ReportClasses.h"

#include "OwerwriteTmplDlg.h"

#include "ResLangFileReader.h"

#include "FolderDlg.h"
///////////////////////////////////////////////////////////////////////////////////////////
// CTemplateExportDoc

IMPLEMENT_DYNCREATE(CTemplateExportDoc, CDocument)

BEGIN_MESSAGE_MAP(CTemplateExportDoc, CDocument)
	//{{AFX_MSG_MAP(CTemplateExportDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTemplateExportDoc construction/destruction

CTemplateExportDoc::CTemplateExportDoc()
{
	// TODO: add one-time construction code here

}

CTemplateExportDoc::~CTemplateExportDoc()
{
}


BOOL CTemplateExportDoc::OnNewDocument()
{

	// CHECK FOR LICENSE HERE!!!!! 2011-12-22 P�D
	if (!License())
	{
		return FALSE;
	}

	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CTemplateExportDoc serialization

void CTemplateExportDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CTemplateExportDoc diagnostics

#ifdef _DEBUG
void CTemplateExportDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CTemplateExportDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

///////////////////////////////////////////////////////////////////////////////////////////
// CTemplateExportFrame

IMPLEMENT_DYNCREATE(CTemplateExportFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CTemplateExportFrame, CMDIChildWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_SYSCOMMAND()
	//ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)

	ON_XTP_CREATECONTROL()
END_MESSAGE_MAP()


// CTemplateExportFrame construction/destruction

XTPDockingPanePaintTheme CTemplateExportFrame::m_themeCurrent = xtpPaneThemeOffice2003;

CTemplateExportFrame::CTemplateExportFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
	m_bInitReports = FALSE;
	m_bIsSysCommand = FALSE;
}

CTemplateExportFrame::~CTemplateExportFrame()
{
}

void CTemplateExportFrame::OnDestroy(void)
{
}

void CTemplateExportFrame::OnClose(void)
{
	// Send messages to HMSShell, disable buttons on toolbar; 120122 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 120122 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6523_KEY);
	SavePlacement(this, csBuf);
	if (!m_bIsSysCommand)
	{
		cleanUpLogScaleTmplDir();
	}

	CMDIChildWnd::OnClose();
}

void CTemplateExportFrame::OnSysCommand(UINT nID,LPARAM lParam)
{
	if ((nID & 0xFFF0) == SC_CLOSE)
	{
		m_bIsSysCommand = TRUE;
		cleanUpLogScaleTmplDir();
		CMDIChildWnd::OnSysCommand(nID,lParam);
	}
	else if ((nID & 0xFFF0) != SC_CLOSE)
	{
		CMDIChildWnd::OnSysCommand(nID,lParam);
	}
}

void CTemplateExportFrame::cleanUpLogScaleTmplDir()
{
	CStringArray sarrFiles;
	CString sDirectory = L"";
	TCHAR szTmpPath[MAX_PATH];
	GetTempPath(MAX_PATH,szTmpPath);
	sDirectory.Format(L"%s%s\\",szTmpPath,TEMPLATE_TMP_SUBDIR);
	getListOfFilesInDirectory(TEMPLATETEXT_FILTER,sDirectory,sarrFiles,EF_FULLY_QUALIFIED);
	if (sarrFiles.GetCount() > 0)
	{
		for (int i = 0;i < sarrFiles.GetCount();i++)
		{
			if (fileExists(sarrFiles.GetAt(i)))
			{
				removeFile(sarrFiles.GetAt(i));
			}
		}
	}
}

int CTemplateExportFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	EnableDocking(CBRS_ALIGN_ANY);

	// Create and Load toolbar; 090107 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR8);
	m_wndToolBar.EnableDocking(xtpFlagAlignTop|xtpFlagStretched);

	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			//***************************************************************************************
			// Setup commandbars and manues; 051114 p�d
			CXTPToolBar* pToolBar = &m_wndToolBar;
			if (pToolBar->IsBuiltIn())
			{
				if (pToolBar->GetType() != xtpBarTypeMenuBar)
				{
					UINT nBarID = pToolBar->GetBarID();
					pToolBar->LoadToolBar(nBarID, FALSE);
					CXTPControls *p = pToolBar->GetControls();
					// Setup icons on toolbars, using resource dll; 051208 p�d
					if (nBarID == IDR_TOOLBAR8)
					{		
						setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_SAVE,xml.str(IDS_STRING4720),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(1),RES_TB_CALIPER,xml.str(IDS_STRING4721),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(2),RES_TB_CHECKMARK,xml.str(IDS_STRING4722),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(3),RES_TB_RED_X,xml.str(IDS_STRING4723),TRUE);	//
					}	// if (nBarID == IDR_TOOLBAR8)
				}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
			}	// if (pToolBar->IsBuiltIn())
			//***************************************************************************************
			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

int CTemplateExportFrame::OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl)
{
	return TRUE;
}


LRESULT CTemplateExportFrame::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	return 0L;
}

BOOL CTemplateExportFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CTemplateExportFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CTemplateExportFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6523_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CTemplateExportFrame::OnSetFocus(CWnd* pWnd)
{
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,ID_DO_SOMETHING_IN_SHELL,ID_LPARAM_COMMAND2);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 120122 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CMDIChildWnd::OnSetFocus(pWnd);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CTemplateExportFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}


// CTemplateExportFrame diagnostics

#ifdef _DEBUG
void CTemplateExportFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CTemplateExportFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CTemplateExportFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = 450;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CTemplateExportFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType,cx,cy);
}



// CTemplateExportView

IMPLEMENT_DYNCREATE(CTemplateExportView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CTemplateExportView, CXTResizeFormView)
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_WM_SIZE()

	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)

	ON_COMMAND_RANGE(ID_BUTTON32815,ID_BUTTON32818, OnCommand)
	ON_UPDATE_COMMAND_UI_RANGE(ID_BUTTON32815,ID_BUTTON32818, OnUpdateToolbar)

	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST23_1, OnItemchangedList)
END_MESSAGE_MAP()

CTemplateExportView::CTemplateExportView()
	: CXTResizeFormView(CTemplateExportView::IDD),
		m_bInitialized(FALSE),
		m_sLangFN(L""),
		m_pDB(NULL),
		m_bEnableToolBarBtn(FALSE),
		m_bChangeIndex(FALSE)
{
}

CTemplateExportView::~CTemplateExportView()
{
}

void CTemplateExportView::OnDestroy()
{
	CXTResizeFormView::OnDestroy();
}


BOOL CTemplateExportView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CTemplateExportView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LIST23_1, m_list23_1);
	//}}AFX_DATA_MAP
}

// CTemplateExportView diagnostics

#ifdef _DEBUG
void CTemplateExportView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CTemplateExportView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG

// CTemplateExportView message handlers
void CTemplateExportView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitDialog();
	if (!m_bInitialized)
	{

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		getDLLVolumeFuncDesc(m_vecFuncDesc,m_vecUserVolTables);

		TCHAR szTmpPath[MAX_PATH];
		CString sDirectory = L"";
		GetTempPath(MAX_PATH,szTmpPath);
		m_sDirectory.Format(L"%s%s",szTmpPath,TEMPLATE_TMP_SUBDIR);
		// Check if directory exists, on disk. If not create it; 120309 p�d
		if (!isDirectory(m_sDirectory))
		{
			createDirectory(m_sDirectory);
		}
		if (regGetStr(REG_ROOT,CONTRACT_SAVETO_DIR,DIRECTORY_KEY).IsEmpty())
		{
			regSetStr(REG_ROOT,CONTRACT_SAVETO_DIR,DIRECTORY_KEY,m_sDirectory);
		}

		setupReport();

		populateReport();

		m_bInitialized = TRUE;
	}
}

BOOL CTemplateExportView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{

	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		if (m_dbConnectionData.conn->isConnected())
		{
			m_pDB = new CDBHandling(m_dbConnectionData);
			if (m_pDB != NULL)
			{
				m_pDB->getTmplTables(m_vecLogScaleTemplates);
				m_pDB->getUserVolTables(m_vecUserVolTables);
				m_pDB->getDefaults(m_vecDefaults);
				m_pDB->getGrades(m_vecGrades);
				m_pDB->getSpecies(m_vecSpecies);
				m_pDB->getRegister(m_vecRegister);
			}
		}
	}

	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CTemplateExportView::OnSize(UINT nType,int cx,int cy)
{

	if (m_list23_1.GetSafeHwnd())
	{
		setResize(&m_list23_1,2,4,cx-4,cy-8);
	}

	CXTResizeFormView::OnSize(nType,cx,cy);
}

void CTemplateExportView::OnCommand(UINT nID)
{
	switch(nID)
	{
		case ID_BUTTON32815: saveTemplateToDisk(); break;
		case ID_BUTTON32816: sendTemplatesToCaliper(); break;
		case ID_BUTTON32817: selectAll(); break;
		case ID_BUTTON32818: deSelectAll(); break;
	};
}

void CTemplateExportView::OnUpdateToolbar(CCmdUI* pCmdUI)
{
	switch (pCmdUI->m_nID)
	{
		case ID_BUTTON32815:
		case ID_BUTTON32816:
			pCmdUI->Enable(m_bEnableToolBarBtn);
			break;
	};
}

LRESULT CTemplateExportView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	m_bChangeIndex = (lParam == IDD_FORMVIEW8);
	populateReport();
	return 0L;
}

void CTemplateExportView::OnItemchangedList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	int nSelRow = -1;
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	pResult = 0;
	if (pNMLV != NULL)
	{
		nSelRow =  pNMLV->iItem;
		if (m_bChangeIndex)
		{
			if (nSelRow >= 0 && nSelRow < m_vecLogScaleTemplates.size())
			{
				CLogScaleTemplatesFormView *pView = (CLogScaleTemplatesFormView*)getFormViewByID(IDD_FORMVIEW8);
				if (pView != NULL)
				{
					pView->doPopulate(m_vecLogScaleTemplates[nSelRow].getTmplID());
				}
			}
		}
	}
	m_bEnableToolBarBtn = FALSE;
	for (int i = 0;i < m_list23_1.GetItemCount();i++)
	{
		if (m_list23_1.GetCheck(i))
		{
			m_bEnableToolBarBtn = TRUE;
			break;
		}
	}

}

void CTemplateExportView::setupReport()
{
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_sMsgCap = xml.str(IDS_STRING99);
			m_sMsgCaliperAlert.Format(L"%s\n\n%s\n\n",xml.str(IDS_STRING4724),xml.str(IDS_STRING4725));
			m_sMsgUnableToCreateTmplFile = xml.str(IDS_STRING4726);

			m_sMsgDoneSavingToDisk = xml.str(IDS_STRING4727);
			m_sMsgDoneUploadToCaliper = xml.str(IDS_STRING4728);


			tokenizeString(xml.str(IDS_STRING201),';',m_sarrLoadType);
			tokenizeString(xml.str(IDS_STRING202),';',m_sarrDeductionType);

			m_list23_1.SetExtendedStyle(m_list23_1.GetStyle()|LVS_EX_CHECKBOXES|LVS_EX_GRIDLINES|LVS_EX_FULLROWSELECT);
			m_list23_1.InsertColumn(0,xml.str(IDS_STRING4700),LVCFMT_LEFT,40);
			m_list23_1.InsertColumn(1,xml.str(IDS_STRING4701),LVCFMT_LEFT,200);
			m_list23_1.InsertColumn(2,xml.str(IDS_STRING4702),LVCFMT_LEFT,80);
			m_list23_1.InsertColumn(3,xml.str(IDS_STRING4703),LVCFMT_LEFT,100);
			m_list23_1.InsertColumn(4,xml.str(IDS_STRING4704),LVCFMT_LEFT,150);

			xml.clean();
		}
	}
}

void CTemplateExportView::populateReport()
{
	m_list23_1.DeleteAllItems();
	if (m_vecLogScaleTemplates.size() > 0)
	{
		for (UINT i = 0;i < m_vecLogScaleTemplates.size();i++)
		{
			InsertRow(m_list23_1,i,false,5,L"",
				m_vecLogScaleTemplates[i].getTmplName(),
				m_vecLogScaleTemplates[i].getCreatedBy(),
				m_vecLogScaleTemplates[i].getDate(),
				m_vecLogScaleTemplates[i].getPrlName());
			m_list23_1.SetItemData(i,(DWORD)m_vecLogScaleTemplates[i].getTmplID());
		}	
	}
}

BOOL CTemplateExportView::createTemplate(LPCTSTR file_name)
{
	CSpecies recSpc = CSpecies();
	CGrades recGrades = CGrades();
	vecPricelistInTicket vecALLTmpl;
	vecPricelistInTicket vecSpcTmpl;
  CStdioFile fOut;
	CString sAsciifile = file_name,sTmp = L"";
	int nFuncID;
	try
	{
		recSpc = getSpeciesData(m_recSelectedTemplate.getDefSpecies());
		recGrades = getGradesData(m_recSelectedTemplate.getDefGrade());
		getSpeciesFromTemplate(m_recSelectedTemplate.getPricelist(),vecSpcTmpl);
		getPricelistFromTemplate(m_recSelectedTemplate.getPricelist(),-1,vecALLTmpl);


		if (!fOut.Open(sAsciifile,CFile::modeWrite|CFile::modeCreate))
		{
			::MessageBox(GetSafeHwnd(),m_sMsgUnableToCreateTmplFile,m_sMsgCap,MB_ICONSTOP | MB_OK);
			return FALSE;
		}
		

		//1 1 and 1 2 Start by adding header
		tmplString(fOut, 1, 1,TEMPLATEHEADER);
		tmplString(fOut, 1, 2,TEMPLATEGUID);
		// 2 1 Source Date
		tmplString(fOut, 2, 1,m_recSelectedTemplate.getDate()); // pRec->getColText(COLUMNS::TIC_SOURCE_DATE));
		// 2 2 Source ID
		tmplString(fOut, 2, 2,getRegisterValue(REGISTER_TYPES::SOURCEID,m_recSelectedTemplate.getSourceID(),m_vecRegister));
		// 2 3 Scaler
		tmplString(fOut, 2, 3,getRegisterValue(REGISTER_TYPES::SCALER,m_recSelectedTemplate.getScaler(),m_vecRegister)); // pRec->getColText(COLUMNS::TIC_SCALER));
		// 2 4 Location
		tmplString(fOut, 2, 4,getRegisterValue(REGISTER_TYPES::LOCATION,m_recSelectedTemplate.getLocation(),m_vecRegister)); // pRec->getColText(COLUMNS::TIC_LOCATION));
		// 2 5 Buyer
		tmplString(fOut, 2, 5,getRegisterValue(REGISTER_TYPES::BUYER,m_recSelectedTemplate.getBuyer(),m_vecRegister)); // pRec->getColText(COLUMNS::TIC_BUYER));
		// 2 6 Other info
		tmplString(fOut, 2, 6,L""); // pRec->getColText(COLUMNS::TIC_OTHER_INFO));
		// 2 7 Name of template
		tmplString(fOut, 2, 7,m_recSelectedTemplate.getTmplName()); 
		// 2 8 Supplier
		tmplString(fOut, 2, 8,getRegisterValue(REGISTER_TYPES::SUPPLIER,m_recSelectedTemplate.getSupplier(),m_vecRegister)); 
		// 2 9 Vendor
		tmplString(fOut, 2, 9,getRegisterValue(REGISTER_TYPES::VENDOR,m_recSelectedTemplate.getVendor(),m_vecRegister));

		// 3 1 Ticketnumber
		tmplString(fOut, 3, 1,L"");
		// 3 2 Tract ID
		tmplString(fOut, 3, 2,getRegisterValue(REGISTER_TYPES::TRACTID,m_recSelectedTemplate.getTractID(),m_vecRegister)); // pRec->getColText(COLUMNS::TIC_TRACT_ID));
		// 3 3 Hauler
		tmplString(fOut, 3, 3,getRegisterValue(REGISTER_TYPES::HAULER,m_recSelectedTemplate.getHauler(),m_vecRegister)); // pRec->getColText(COLUMNS::TIC_HAULER));
		// 3 4 Trim inch
		sTmp.Format(L"%.0f;",m_recSelectedTemplate.getTrimFT()*10.0);
		tmplString(fOut, 3, 4,sTmp,false);
		// 3 5 Trim CM
		sTmp.Format(L"%.0f;",m_recSelectedTemplate.getTrimCM());
		tmplString(fOut, 3, 5,sTmp,false);
		// 3 6 Measuringmode
		sTmp.Format(L"%d;",m_recSelectedTemplate.getMeasureMode());
		tmplString(fOut, 3, 6,sTmp,false);

		
		/////////////////////////////////////////////////////////////////////////////////////////////
		// DEFAULTS DATA
		//4 1 Sawlog Volume Unit
		/*for (UINT i1 = 0;i1 < m_vecFuncDesc.size();i1++)
		{
			if (getDefaults(DEF_SAWLOG,m_vecDefaults,m_vecFuncDesc).CompareNoCase(m_vecFuncDesc[i1].getBasis()) == 0)
			{
				sTmp.Format(L"%d",m_vecFuncDesc[i1].getFuncID());
				tmplString(fOut,4,1,sTmp);
				break;
			}
		}*/
		if ((nFuncID=getDefaultFuncID(DEF_SAWLOG,m_vecDefaults)) > 0)	//#4258 gjort om	
			{
				sTmp.Format(L"%d",nFuncID);
				tmplString(fOut,4,1,sTmp);
			}
		//4 2 Pulpwood Volume Unit
		/*for (UINT i1 = 0;i1 < m_vecFuncDesc.size();i1++)
		{
			//if (m_vecFuncDesc[i1].getOnlySW() == 0)
			//{
				if (getDefaults(DEF_PULPWOOD,m_vecDefaults,m_vecFuncDesc).CompareNoCase(m_vecFuncDesc[i1].getBasis()) == 0)
				{
					sTmp.Format(L"%d",m_vecFuncDesc[i1].getFuncID());
					tmplString(fOut,4,2,sTmp);
					break;
				//}
			}
		}*/
		if ((nFuncID=getDefaultFuncID(DEF_PULPWOOD,m_vecDefaults)) > 0)	//#4258 gjort om
			{
				sTmp.Format(L"%d",nFuncID);
				tmplString(fOut,4,2,sTmp);
			}

		//4 3 Default Deduction Type
		for (int i = 0;i < m_sarrDeductionType.GetCount();i++)
		{
			if (m_sarrDeductionType.GetAt(i).CompareNoCase(getDefaults(DEF_DEDUCTION,m_vecDefaults,m_vecFuncDesc)) == 0)
			{
				tmplString(fOut,4,3,i);
				break;
			}
		}

		//4 4 Default Specie Code
		if (vecSpcTmpl.size() > 0)
		{
			for (UINT i = 0;i < vecSpcTmpl.size();i++)
			{
				if (vecSpcTmpl[i].getSpeciesID() == m_recSelectedTemplate.getDefSpecies())
				{
					tmplString(fOut,4,4,i);
					break;
				}
			}
		}
		else
		{
			tmplString(fOut,4,4,0);
		}

		//4 5 Default Log Grade
		if (vecALLTmpl.size() > 0)
		{
			for (UINT i = 0;i < vecALLTmpl.size();i++)
			{
				if (vecALLTmpl[i].getGradesID() == m_recSelectedTemplate.getDefGrade())
				{
					tmplString(fOut,4,5,i);
					break;
				}
			}
		}
		else
		{
			tmplString(fOut,4,5,0);
		}

/*
		for (UINT i1 = 0;i1 < m_vecGrades.size();i1++)
		{
			if (m_vecGrades[i1].getGradesCode().Compare(getDefaults(DEF_LOG_GRADE,m_vecDefaults,m_vecFuncDesc)) == 0)
				tmplString(fOut,4,5,i1);
		}
*/
		//4 6 Default Load Type
		for (int i = 0;i < m_sarrLoadType.GetCount();i++)
		{
			if (m_sarrLoadType.GetAt(i).CompareNoCase(getDefaults(DEF_LOAD_TYPE,m_vecDefaults,m_vecFuncDesc)) == 0)
			{
				tmplString(fOut,4,6,i);
				break;
			}
		}

		//4 7 Default Frequency
		tmplString(fOut,4,7,getDefaults(DEF_FREQUENCY,m_vecDefaults,m_vecFuncDesc));

		//4 8 Default Log Length (inch)
		sTmp = getDefaults(DEF_LOG_LENGTH_INCH,m_vecDefaults,m_vecFuncDesc);
		double fValue = _tstof(sTmp)*10.0;
		sTmp.Format(L"%.0f",fValue);
		sTmp.Replace(',','.');
		tmplString(fOut,4,8,sTmp);

		//4 9 Use Southern Doyle Rule
		tmplString(fOut,4,9,getDefaults(DEF_USE_S_DOYLE,m_vecDefaults,m_vecFuncDesc));

		//4 10 Round Scaling Diameter
		tmplString(fOut,4,10,getDefaults(DEF_ROUND_SC_DIAM,m_vecDefaults,m_vecFuncDesc));

		//4 11 Default Log Length (mm)
		sTmp = getDefaults(DEF_LOG_LENGTH_MM,m_vecDefaults,m_vecFuncDesc);
		fValue = _tstof(sTmp)*100.0;
		sTmp.Format(L"%.0f",fValue);
		sTmp.Replace(',','.');
		tmplString(fOut,4,11,sTmp);

		//4 12 IB Taper
		sTmp = getDefaults(DEF_TAPER_IB,m_vecDefaults,m_vecFuncDesc);
		fValue = _tstof(sTmp)*1000.0;
		sTmp.Format(L"%.0f",fValue);
		sTmp.Replace(',','.');
		tmplString(fOut,4,12,sTmp);

		/////////////////////////////////////////////////////////////////////////////////////////////
		// SPECIES (Species code)
		tmplString(fOut,5,1,vecSpcTmpl.size());
		for (UINT i1 = 0;i1 < vecSpcTmpl.size();i1++)
		{
			for (UINT i = 0;i < m_vecSpecies.size();i++)
			{
				if (m_vecSpecies[i].getSpcID() == vecSpcTmpl[i1].getSpeciesID())
					tmplString(fOut,m_vecSpecies[i].getSpcCode(),false);
			}
		}
		/*
		tmplString(fOut,5,1,m_vecSpecies.size());
		for (UINT i = 0;i < m_vecSpecies.size();i++)
		{
			tmplString(fOut,m_vecSpecies[i].getSpcCode(),false);
		}
		*/

/*
		tmplString(fOut,27,1,m_vecSpecies.size());
		for (UINT i = 0;i < m_vecSpecies.size();i++)
		{
			tmplString(fOut,m_vecSpecies[i].getBarkReductionInch()*10.0,false,0);
		}
		tmplString(fOut,27,2,m_vecSpecies.size());
		for (UINT i = 0;i < m_vecSpecies.size();i++)
		{
			tmplString(fOut,m_vecSpecies[i].getBarkReductionMM(),false,0);
		}
*/
		/////////////////////////////////////////////////////////////////////////////////////////////
		// GRADES PER SPECIES (Species code)
		int nNumOfGrades = 0;
		if (vecSpcTmpl.size() > 0)
		{
			getPricelistFromTemplate(m_recSelectedTemplate.getPricelist(),vecSpcTmpl[0].getSpeciesID(),vecALLTmpl);
			nNumOfGrades = vecALLTmpl.size();
		}

		getPricelistFromTemplate(m_recSelectedTemplate.getPricelist(),-1,vecALLTmpl);
		tmplString(fOut,6,0,nNumOfGrades);
		for (UINT i = 0;i < vecALLTmpl.size();i++)
		{
			recGrades = getGradesData(vecALLTmpl[i].getGradesID());
			if (recGrades.getGradesID() == vecALLTmpl[i].getGradesID() && 
					vecALLTmpl[i].getSpeciesID() == vecSpcTmpl[0].getSpeciesID())
			{
				tmplString(fOut,recGrades.getGradesCode(),false);
			}
		}

		for (UINT i1 = 0;i1 < vecSpcTmpl.size();i1++)
		{
			tmplString(fOut,6,i1+1,nNumOfGrades);
			for (UINT i = 0;i < vecALLTmpl.size();i++)
			{
				recGrades = getGradesData(vecALLTmpl[i].getGradesID());
				if (recGrades.getGradesID() == vecALLTmpl[i].getGradesID() && 
						vecALLTmpl[i].getSpeciesID() == vecSpcTmpl[i1].getSpeciesID())
				{
					tmplString(fOut,recGrades.getGradesCode(),false);
				}
			}
		}

		/////////////////////////////////////////////////////////////////////////////////////////////
		// PRICES PER SPECIES (Species code)
		// get ALL information from template pricelist

		tmplString(fOut,7,0,nNumOfGrades);
		for (UINT i2 = 0;i2 < nNumOfGrades;i2++)
		{
			tmplString(fOut,0.0,false);
		}
		for (UINT i = 0;i < vecSpcTmpl.size();i++)
		{
			tmplString(fOut,7,i+1,nNumOfGrades); // Number of grades in pricelist
			//if (getPricesForSpeciesInTemplate(m_recSelectedTemplate.getPricelist(),m_vecSpecies[i].getSpcID(),vec))
			if (getPricelistFromTemplate(m_recSelectedTemplate.getPricelist(),vecSpcTmpl[i].getSpeciesID(),vecALLTmpl))
			{
				for (UINT i2 = 0;i2 < nNumOfGrades;i2++)
				{
					tmplString(fOut,vecALLTmpl[i2].getPrice(),false,2);
				}
			}
		}

		/////////////////////////////////////////////////////////////////////////////////////////////
		// SPECIES (Species bark reduction)
		tmplString(fOut,8,1,vecSpcTmpl.size());
		for (UINT i1 = 0;i1 < vecSpcTmpl.size();i1++)
		{
			for (UINT i = 0;i < m_vecSpecies.size();i++)
			{
				if (m_vecSpecies[i].getSpcID() == vecSpcTmpl[i1].getSpeciesID())
					tmplString(fOut,m_vecSpecies[i].getBarkReductionInch()*10.0,false,0);
			}
		}
		tmplString(fOut,8,2,vecSpcTmpl.size());
		for (UINT i1 = 0;i1 < vecSpcTmpl.size();i1++)
		{
			for (UINT i = 0;i < m_vecSpecies.size();i++)
			{
				if (m_vecSpecies[i].getSpcID() == vecSpcTmpl[i1].getSpeciesID())
					tmplString(fOut,m_vecSpecies[i].getBarkReductionMM(),false,0);
			}
		}

		/////////////////////////////////////////////////////////////////////////////////////////////
		// Set Grades Sawlog or PulpWood
		getPricelistFromTemplate(m_recSelectedTemplate.getPricelist(),-1,vecALLTmpl);
		tmplString(fOut,9,0,nNumOfGrades);
		for (UINT i = 0;i < vecALLTmpl.size();i++)
		{
			recGrades = getGradesData(vecALLTmpl[i].getGradesID());
			if (recGrades.getGradesID() == vecALLTmpl[i].getGradesID() && 
					vecALLTmpl[i].getSpeciesID() == vecSpcTmpl[0].getSpeciesID())
			{
				sTmp.Format(L"%d",recGrades.getIsPulpwood());
				tmplString(fOut,sTmp);
			}
		}

		for (UINT i1 = 0;i1 < vecSpcTmpl.size();i1++)
		{
			tmplString(fOut,9,i1+1,nNumOfGrades);
			for (UINT i = 0;i < vecALLTmpl.size();i++)
			{
				recGrades = getGradesData(vecALLTmpl[i].getGradesID());
				if (recGrades.getGradesID() == vecALLTmpl[i].getGradesID() && 
						vecALLTmpl[i].getSpeciesID() == vecSpcTmpl[i1].getSpeciesID())
				{

					sTmp.Format(L"%d",recGrades.getIsPulpwood());
					tmplString(fOut,sTmp);
				}
			}
		}



		/////////////////////////////////////////////////////////////////////////////////////////////
		// 99 1 Checksum = 1
		tmplString(fOut,L"99|1|1");	


		fOut.Close();

		return TRUE;
	}
	catch (CFileException &e)
	{
		TCHAR szErrMsg[255];
		e.GetErrorMessage(szErrMsg, 255);
		AfxMessageBox(szErrMsg);
		fOut.Close();
		return FALSE;
	}

}

CGrades CTemplateExportView::getGradesData(int grades_id)
{
	if (m_vecGrades.size() == 0) return CGrades();
	for (UINT i = 0;i < m_vecGrades.size();i++)
	{
		if (m_vecGrades[i].getGradesID() == grades_id)
			return m_vecGrades[i];
	}
	return CGrades();
}

CSpecies CTemplateExportView::getSpeciesData(int spc_id)
{
	if (m_vecSpecies.size() == 0) return CSpecies();
	for (UINT i = 0;i < m_vecSpecies.size();i++)
	{
		if (m_vecSpecies[i].getSpcID() == spc_id)
			return m_vecSpecies[i];
	}
	return CSpecies();
}

CLogScaleTemplates CTemplateExportView::getLogScaleTemplate(int id)
{
	if (m_vecLogScaleTemplates.size() > 0)
	{
		for (UINT i = 0;i < m_vecLogScaleTemplates.size();i++)
		{
			if (m_vecLogScaleTemplates[i].getTmplID() == id)
				return m_vecLogScaleTemplates[i];
		}
	}

	return CLogScaleTemplates();
}

void CTemplateExportView::saveTemplateToDisk()
{
	TCHAR szTmpPath[MAX_PATH];
	CString pathSelected = L"",pathAndFileName = L"",sRegPath = L"";

	sRegPath = regGetStr(REG_ROOT,CONTRACT_SAVETO_DIR,DIRECTORY_KEY);
	CFolderDialog *dlg = new CFolderDialog(0,sRegPath);
	if (dlg != NULL)
	{
		if (dlg->DoModal() == IDOK)
		{
			pathSelected = dlg->GetFolderPath();
			for (int i = 0;i < m_list23_1.GetItemCount();i++)
			{
				if (m_list23_1.GetCheck(i))
				{
					m_recSelectedTemplate = getLogScaleTemplate((int)m_list23_1.GetItemData(i));
					GetTempPath(MAX_PATH,szTmpPath);
					pathAndFileName.Format(L"%s\\%s%s",pathSelected,m_recSelectedTemplate.getTmplName(),TEMPLATETEXT);

					if (fileExists(pathAndFileName))
					{
						COwerwriteTmplDlg *pOwerWriteDlg = new COwerwriteTmplDlg();
						if (pOwerWriteDlg != NULL)
						{
							pOwerWriteDlg->setPath(pathAndFileName);
							if (pOwerWriteDlg->DoModal() == IDOK)
							{
								if (pOwerWriteDlg->getReturnValue() == 1)	// OwerWrite
								{
									createTemplate(pathAndFileName);
								}
								else if (pOwerWriteDlg->getReturnValue() == 2)	// Save as
								{
									pathAndFileName = pOwerWriteDlg->getNewPathAndFilename();
									createTemplate(pathAndFileName);
								}
							}
							delete pOwerWriteDlg;
						}
					}
					else
					{
						createTemplate(pathAndFileName);
					}
				}
			}	// for (int i = 0;i < m_list23_1.GetItemCount();i++)
			
			regSetStr(REG_ROOT,CONTRACT_SAVETO_DIR,DIRECTORY_KEY,pathSelected);

		}

		::MessageBox(GetSafeHwnd(),m_sMsgDoneSavingToDisk,m_sMsgCap,MB_ICONINFORMATION | MB_OK);
	
	}

}

void CTemplateExportView::sendTemplatesToCaliper()
{
	TCHAR szTmpPath[MAX_PATH];
	CString pathAndFileName = L"",sBuffer = L"";
	for (int i = 0;i < m_list23_1.GetItemCount();i++)
	{
		if (m_list23_1.GetCheck(i))
		{
			m_recSelectedTemplate = getLogScaleTemplate((int)m_list23_1.GetItemData(i));
			GetTempPath(MAX_PATH,szTmpPath);
			pathAndFileName.Format(L"%s%s\\%s%s",szTmpPath,TEMPLATE_TMP_SUBDIR,m_recSelectedTemplate.getTmplName(),TEMPLATETEXT);
			if (createTemplate(pathAndFileName))
			{
				sBuffer += pathAndFileName + L"%";
			}
		}
	}	// for (int i = 0;i < m_list23_1.GetItemCount();i++)
	sBuffer.Delete(sBuffer.GetLength()-1);
	
//	if (::MessageBox(GetSafeHwnd(),m_sMsgCaliperAlert,m_sMsgCap,MB_ICONSTOP | MB_YESNO | MB_DEFBUTTON2) == IDYES)
//	{

		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4,(LPARAM)&_user_msg(401,
																_T("OpenSuiteEx"), _T("Communication.dll"), _T(""), _T(""), _T(""), 
																(LPTSTR)sBuffer.GetBuffer()));

//	}
}

void CTemplateExportView::selectAll()
{
	for (int i = 0;i < m_list23_1.GetItemCount();i++)
	{
		m_list23_1.SetCheck(i,TRUE);
	}
}

void CTemplateExportView::deSelectAll()
{
	for (int i = 0;i < m_list23_1.GetItemCount();i++)
	{
		m_list23_1.SetCheck(i,FALSE);

	}
}

