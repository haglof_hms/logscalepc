#pragma once

#include "resource.h"
// CViewProgress dialog

class CViewProgress : public CXTResizeDialog
{
	DECLARE_DYNAMIC(CViewProgress)

	BOOL m_bInitialized;

	CProgressCtrl m_pBar;
	CMyExtStatic m_lbl19_1;
public:
	CViewProgress(CWnd* pParent = NULL);   // standard constructor
	virtual ~CViewProgress();

// Dialog Data
	enum { IDD = IDD_DIALOG19 };

	void setRange(int from,int to) { m_pBar.SetRange(from,to); }
	void setStep(int step = 1)	{ m_pBar.SetStep(step); }
	void setPos(int pos) { m_pBar.SetPos(pos); }

protected:
		//{{AFX_VIRTUAL(CCorrectionDlg)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	//}}AFX_VIRTUAL

	DECLARE_MESSAGE_MAP()
};
