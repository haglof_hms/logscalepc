// DefaultsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DefaultsDlg.h"

#include "ResLangFileReader.h"

#include <algorithm>


bool SortDataAsc(CFuncDesc& l1, CFuncDesc& l2)
{
	return l1.getFuncID() < l2.getFuncID();
}

bool SortDataDec(CFuncDesc& l1, CFuncDesc& l2)
{
	return l1.getFuncID() > l2.getFuncID();
}

// CDefaultsDlg dialog

IMPLEMENT_DYNAMIC(CDefaultsDlg, CXTResizeDialog)

BEGIN_MESSAGE_MAP(CDefaultsDlg, CXTResizeDialog)
	ON_WM_COPYDATA()
	ON_BN_CLICKED(IDOK, &CDefaultsDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON1, &CDefaultsDlg::OnBnClickedButton1)
END_MESSAGE_MAP()

CDefaultsDlg::CDefaultsDlg(CWnd* pParent /*=NULL*/)
	: CXTResizeDialog(CDefaultsDlg::IDD, pParent),
		m_bInitialized(FALSE),
		m_sLangFN(L""),
		m_pDB(NULL)
{

}

CDefaultsDlg::~CDefaultsDlg()
{
}

BOOL CDefaultsDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeDialog::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}


void CDefaultsDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_BUTTON1, m_btnSetStd);
	//}}AFX_DATA_MAP
}

// CDefaultsDlg message handlers
BOOL CDefaultsDlg::OnInitDialog()
{
	CXTResizeDialog::OnInitDialog();
	if (!m_bInitialized)
	{

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		getDLLVolumeFuncDesc(m_vecFuncDesc,m_vecUserVolTables);

		setupPropGrid();

		m_bInitialized = TRUE;
	}

	return TRUE;
}

BOOL CDefaultsDlg::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{

	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		if (m_dbConnectionData.conn->isConnected())
		{
			m_pDB = new CDBHandling(m_dbConnectionData);}
	}

	return CXTResizeDialog::OnCopyData(pWnd, pData);
}

void CDefaultsDlg::setupPropGrid()
{
	// Get data from DB
	if (m_pDB != NULL)
	{
		m_pDB->getGrades(m_vecGrades);
//		m_pDB->getCalcTypes(m_vecCalcTypes);
		m_pDB->getSpecies(m_vecSpecies);
		m_pDB->getDefaults(m_vecDefaults);
		m_pDB->getUserVolTables(m_vecUserVolTables);
	}

	if (m_propDefaults.GetSafeHwnd() == 0)
	{
		RECT rect;
		GetClientRect(&rect);
		m_propDefaults.Create(CRect(2, 2, rect.right - 4, rect.bottom - 40), this, 1000);
		m_propDefaults.SetOwner(this);
		m_propDefaults.ShowHelp( FALSE );
		m_propDefaults.SetViewDivider(0.55);
		m_propDefaults.SetTheme(xtpGridThemeOffice2003);
	}
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{

			tokenizeString(xml.str(IDS_STRING201),';',m_sarrLoadTypes);
			tokenizeString(xml.str(IDS_STRING202),';',m_sarrDeductionTypes);

			SetWindowText(xml.str(IDS_STRING6107));
			m_btnOK.SetWindowText(xml.str(IDS_STRING100));
			m_btnCancel.SetWindowText(xml.str(IDS_STRING101));
			m_btnSetStd.SetWindowText(xml.str(IDS_STRING108));
			m_sYes = xml.str(IDS_STRING106);
			m_sNo = xml.str(IDS_STRING107);

			m_sSettingsSWtext = xml.str(IDS_STRING1601);	//#4258
			m_sSettingsPWtext = xml.str(IDS_STRING1602);

			if (m_propDefaults.GetSafeHwnd() != 0)
			{
				CXTPPropertyGridItem* pSettings  = NULL;
				CXTPPropertyGridItem *pItem = NULL;
				CXTPPropertyGridItemNumber *pItemNumber = NULL;
				pSettings  = m_propDefaults.AddCategory(xml.str(IDS_STRING1600));
				if (pSettings != NULL)
				{
					// Sawlog volume unit
					if ((pItem = pSettings->AddChildItem(new CXTPPropertyGridItem(xml.str(IDS_STRING1601)))) != NULL)
					{
						pItem->BindToString(&m_sSawlogVolumeUnit);
						if (m_vecFuncDesc.size() > 0)
						{
							for (UINT i1 = 0;i1 < m_vecFuncDesc.size();i1++)
							{
								m_recFuncDesc = m_vecFuncDesc[i1];
								pItem->GetConstraints()->AddConstraint(m_recFuncDesc.getBasis(),m_recFuncDesc.getFuncID());
							}
						}
						pItem->SetFlags(xtpGridItemHasComboButton);
						pItem->SetConstraintEdit( FALSE );
						m_nSawlogFuncID = getDefaultFuncID(DEF_SAWLOG,m_vecDefaults);	//#4258 h�mtar ut FuncID
						m_sSawlogVolumeUnit = getDefaults(DEF_SAWLOG,m_vecDefaults,m_vecFuncDesc);
					}
					// Pulpwood volume unit
					if ((pItem = pSettings->AddChildItem(new CXTPPropertyGridItem(xml.str(IDS_STRING1602)))) != NULL)
					{
						pItem->BindToString(&m_sPulpwoodVolumeUnit);
						if (m_vecFuncDesc.size() > 0)
						{
							for (UINT i1 = 0;i1 < m_vecFuncDesc.size();i1++)
							{
								m_recFuncDesc = m_vecFuncDesc[i1];
								if (m_recFuncDesc.getOnlySW() == 0)
								{
									pItem->GetConstraints()->AddConstraint(m_recFuncDesc.getBasis(),m_recFuncDesc.getFuncID());
								}
							}
						}
						pItem->SetFlags(xtpGridItemHasComboButton);
						pItem->SetConstraintEdit( FALSE );
						m_nPulpwoodFuncID = getDefaultFuncID(DEF_PULPWOOD,m_vecDefaults);	//#4258 h�mtar ut FuncID
						m_sPulpwoodVolumeUnit = getDefaults(DEF_PULPWOOD,m_vecDefaults,m_vecFuncDesc);
					}
					// Deduction type
					if ((pItem = pSettings->AddChildItem(new CXTPPropertyGridItem(xml.str(IDS_STRING1603)))) != NULL)
					{
						pItem->BindToString(&m_sDeductionType);
						if (m_sarrDeductionTypes.GetCount() > 0)
						{
							for (int i1 = 0;i1 < m_sarrDeductionTypes.GetCount();i1++)
							{
								pItem->GetConstraints()->AddConstraint(m_sarrDeductionTypes.GetAt(i1),i1);
							}
						}

						pItem->SetFlags(xtpGridItemHasComboButton);
						pItem->SetConstraintEdit( FALSE );
						m_sDeductionType = getDefaults(DEF_DEDUCTION,m_vecDefaults,m_vecFuncDesc);
					}
					// Species code
					if ((pItem = pSettings->AddChildItem(new CXTPPropertyGridItem(xml.str(IDS_STRING1604)))) != NULL)
					{
						pItem->BindToString(&m_sSpeciesCode);
						if (m_vecSpecies.size() > 0)
						{
							for (UINT i1 = 0;i1 < m_vecSpecies.size();i1++)
							{
								m_recSpecies = m_vecSpecies[i1];
								pItem->GetConstraints()->AddConstraint(m_recSpecies.getSpcCode(),m_recSpecies.getSpcID());
							}
						}
						pItem->SetFlags(xtpGridItemHasComboButton);
						pItem->SetConstraintEdit( FALSE );
//						m_sSpeciesCode = getDefaults(DEF_SPC_CODE,m_vecDefaults,m_vecFuncDesc);
					}
					// Log grade
					if ((pItem = pSettings->AddChildItem(new CXTPPropertyGridItem(xml.str(IDS_STRING1605)))) != NULL)
					{
						pItem->BindToString(&m_sLogGrade);
						if (m_vecGrades.size() > 0)
						{
							for (UINT i1 = 0;i1 < m_vecGrades.size();i1++)
							{
								m_recGrades = m_vecGrades[i1];
								pItem->GetConstraints()->AddConstraint(m_recGrades.getGradesCode(),m_recGrades.getGradesID());
							}
						}
						pItem->SetFlags(xtpGridItemHasComboButton);
						pItem->SetConstraintEdit( FALSE );
//						m_sLogGrade = getDefaults(DEF_LOG_GRADE,m_vecDefaults,m_vecFuncDesc);
					}
					// Load type
					if ((pItem = pSettings->AddChildItem(new CXTPPropertyGridItem(xml.str(IDS_STRING1606)))) != NULL)
					{
						pItem->BindToString(&m_sLoadType);
						if (m_sarrLoadTypes.GetCount() > 0)
						{
							for (int i1 = 0;i1 < m_sarrLoadTypes.GetCount();i1++)
							{
								pItem->GetConstraints()->AddConstraint(m_sarrLoadTypes.GetAt(i1),i1);
							}
						}
						pItem->SetFlags(xtpGridItemHasComboButton);
						pItem->SetConstraintEdit( FALSE );
						m_sLoadType = getDefaults(DEF_LOAD_TYPE,m_vecDefaults,m_vecFuncDesc);
					}
					// Frequency
					if ((pItemNumber = (CXTPPropertyGridItemNumber*)pSettings->AddChildItem(new CXTPPropertyGridItemNumber(xml.str(IDS_STRING1607)))) != NULL)
					{
						pItemNumber->BindToNumber(&m_nFrequency);
						m_nFrequency = _tstoi(getDefaults(DEF_FREQUENCY,m_vecDefaults,m_vecFuncDesc));

					}
					/*
					// Log length (inch)
					if ((pItemNumber = (CXTPPropertyGridItemNumber*)pSettings->AddChildItem(new CXTPPropertyGridItemNumber(xml.str(IDS_STRING16080)))) != NULL)
					{
						pItemNumber->BindToNumber(&m_nLogLengthInch);
						m_nLogLengthInch = _tstoi(getDefaults(DEF_LOG_LENGTH_INCH,m_vecDefaults,m_vecFuncDesc));

					}
					// Log length (mm)
					if ((pItemNumber = (CXTPPropertyGridItemNumber*)pSettings->AddChildItem(new CXTPPropertyGridItemNumber(xml.str(IDS_STRING16081)))) != NULL)
					{
						pItemNumber->BindToNumber(&m_nLogLengthMM);
						m_nLogLengthMM = _tstoi(getDefaults(DEF_LOG_LENGTH_MM,m_vecDefaults,m_vecFuncDesc));

					}
					*/
					// Use southern doyle
					if ((pItem = (CXTPPropertyGridItem*)pSettings->AddChildItem(new CXTPPropertyGridItem(xml.str(IDS_STRING1609)))) != NULL)
					{
						pItem->SetFlags(xtpGridItemHasComboButton);
						pItem->BindToString(&m_sUseSouthernDoyle);
						pItem->SetConstraintEdit(FALSE);
						pItem->GetConstraints()->AddConstraint(m_sYes);
						pItem->GetConstraints()->AddConstraint(m_sNo);
						if (_tstoi(getDefaults(DEF_USE_S_DOYLE,m_vecDefaults,m_vecFuncDesc)) == 0)
							m_sUseSouthernDoyle = m_sNo;
						else if (_tstoi(getDefaults(DEF_USE_S_DOYLE,m_vecDefaults,m_vecFuncDesc)) == 1)
							m_sUseSouthernDoyle = m_sYes;

					}
					// Round scaling
					if ((pItem = (CXTPPropertyGridItem*)pSettings->AddChildItem(new CXTPPropertyGridItem(xml.str(IDS_STRING1610)))) != NULL)
					{
						pItem->SetFlags(xtpGridItemHasComboButton);
						pItem->BindToString(&m_sRoundScaling);
						pItem->SetConstraintEdit(FALSE);
						pItem->GetConstraints()->AddConstraint(m_sYes);
						pItem->GetConstraints()->AddConstraint(m_sNo);
						if (_tstoi(getDefaults(DEF_ROUND_SC_DIAM,m_vecDefaults,m_vecFuncDesc)) == 0)
							m_sRoundScaling = m_sNo;
						else if (_tstoi(getDefaults(DEF_ROUND_SC_DIAM,m_vecDefaults,m_vecFuncDesc)) == 1)
							m_sRoundScaling = m_sYes;
					}
					// Bark-ratio
					if ((pItem = (CXTPPropertyGridItem*)pSettings->AddChildItem(new CXTPPropertyGridItem(xml.str(IDS_STRING1612)))) != NULL)
					{
						pItem->BindToString(&m_sBarkRatio);
						m_sBarkRatio = getDefaults(DEF_BARK_RATIO,m_vecDefaults,m_vecFuncDesc);

					}
					// Pounds/Cubic foot
					if ((pItem = (CXTPPropertyGridItem*)pSettings->AddChildItem(new CXTPPropertyGridItem(xml.str(IDS_STRING1613)))) != NULL)
					{
						pItem->BindToString(&m_sTaperIB);
						m_sTaperIB = getDefaults(DEF_TAPER_IB,m_vecDefaults,m_vecFuncDesc);
					}
					// Pounds/Cubic foot
/*
					if ((pItem = (CXTPPropertyGridItem*)pSettings->AddChildItem(new CXTPPropertyGridItem(xml.str(IDS_STRING1611)))) != NULL)
					{
						pItem->BindToString(&m_sPoundsPerCubicFoot);
						m_sPoundsPerCubicFoot = getDefaults(DEF_POUNDS_CUBIC_FOOT,m_vecDefaults);
					}
*/			
					pSettings->Expand();

				}
			}
			xml.clean();
		}
	}
}

CString CDefaultsDlg::getCalcTypeBasisValue(LPCTSTR calc_type)
{
	if (m_vecFuncDesc.size() > 0)
	{
		for (UINT i = 0;i < m_vecFuncDesc.size();i++)
		{
			if (m_vecFuncDesc[i].getBasis() == calc_type)
				return m_vecFuncDesc[i].getBasis();
		}
	}
	return L"";
}

//#4258 h�mta mha FuncID ist�llet
CString CDefaultsDlg::getCalcTypeValue(int func_id)
{
	CString S;
	if (m_vecFuncDesc.size() > 0)
	{
		for (UINT i = 0;i < m_vecFuncDesc.size();i++)
		{
			if (m_vecFuncDesc[i].getFuncID() == func_id)
			{
				return m_vecFuncDesc[i].getBasis();
			}
		}
	}
	return L"";
}

void CDefaultsDlg::OnBnClickedOk()
{
	CString sFrequency = L"";
	CString sLogLength = L"";
	// Save and exit
	if (m_pDB != NULL)
	{
		CXTPPropertyGridItems* pItems = NULL;
		CXTPPropertyGridItem *pItem = NULL;
		CXTPPropertyGridItemConstraint *pConstraint = NULL;
		int nCurrent;
		pItems = m_propDefaults.GetCategories();
		if(pItems != NULL)
		{
			pItem = pItems->FindItem(m_sSettingsSWtext);
			if (pItem != NULL)
			{
				if((nCurrent = pItem->GetConstraints()->GetCurrent()) >= 0)
				{
					pConstraint = pItem->GetConstraints()->GetConstraintAt(nCurrent);
					if(pConstraint != NULL)
					{
						m_nSawlogFuncID = pConstraint->m_dwData;
					}
				}
			}

			pItem = pItems->FindItem(m_sSettingsPWtext);
			if (pItem != NULL)
			{
				if((nCurrent = pItem->GetConstraints()->GetCurrent()) >= 0)
				{
					pConstraint = pItem->GetConstraints()->GetConstraintAt(nCurrent);
					if(pConstraint != NULL)
					{
						m_nPulpwoodFuncID = pConstraint->m_dwData;
					}
				}
			}
		}
		
		m_pDB->updDefaults(CDefaults(DEF_SAWLOG,m_sSawlogVolumeUnit,m_nSawlogFuncID>0? getCalcTypeValue(m_nSawlogFuncID) : getCalcTypeBasisValue(m_sSawlogVolumeUnit),_T(""),_T(""),m_nSawlogFuncID));	//#4258 spara FuncID i db ox�
		m_pDB->updDefaults(CDefaults(DEF_PULPWOOD,m_sPulpwoodVolumeUnit,m_nPulpwoodFuncID>0? getCalcTypeValue(m_nPulpwoodFuncID):getCalcTypeBasisValue(m_sPulpwoodVolumeUnit),_T(""),_T(""),m_nPulpwoodFuncID));	//#4258 spara FuncID i db ox�
		//m_pDB->updDefaults(CDefaults(DEF_SAWLOG,m_sSawlogVolumeUnit,getCalcTypeBasisValue(m_sSawlogVolumeUnit)));	
		//m_pDB->updDefaults(CDefaults(DEF_PULPWOOD,m_sPulpwoodVolumeUnit,getCalcTypeBasisValue(m_sPulpwoodVolumeUnit)));	
		m_pDB->updDefaults(CDefaults(DEF_DEDUCTION,m_sDeductionType,getCalcTypeBasisValue(m_sDeductionType)));
//		m_pDB->updDefaults(-1);
//		m_pDB->updDefaults(-1);
		m_pDB->updDefaults(CDefaults(DEF_LOAD_TYPE,m_sLoadType,getCalcTypeBasisValue(m_sLoadType)));
		sFrequency.Format(L"%d",m_nFrequency);
		m_pDB->updDefaults(CDefaults(DEF_FREQUENCY,sFrequency,getCalcTypeBasisValue(sFrequency)));
/*		
		sLogLength.Format(L"%d",m_nLogLengthInch);
		if (m_pDB->isDefValueAdded(DEF_LOG_LENGTH_INCH))
			m_pDB->updDefaults(CDefaults(DEF_LOG_LENGTH_INCH,sLogLength,sLogLength));
		else
			m_pDB->addDefaults(CDefaults(DEF_LOG_LENGTH_INCH,sLogLength,sLogLength));

		sLogLength.Format(L"%d",m_nLogLengthMM);
		if (m_pDB->isDefValueAdded(DEF_LOG_LENGTH_MM))
			m_pDB->updDefaults(CDefaults(DEF_LOG_LENGTH_MM,sLogLength,sLogLength));
		else
			m_pDB->addDefaults(CDefaults(DEF_LOG_LENGTH_MM,sLogLength,sLogLength));
*/		
		if (m_sUseSouthernDoyle.CompareNoCase(m_sNo) == 0)
		{
			if (m_pDB->isDefValueAdded(DEF_USE_S_DOYLE))
				m_pDB->updDefaults(CDefaults(DEF_USE_S_DOYLE,L"0",L"0"));
			else
				m_pDB->addDefaults(CDefaults(DEF_USE_S_DOYLE,L"0",L"0"));
		}
		else if (m_sUseSouthernDoyle.CompareNoCase(m_sYes) == 0)
		{
			if (m_pDB->isDefValueAdded(DEF_USE_S_DOYLE))
				m_pDB->updDefaults(CDefaults(DEF_USE_S_DOYLE,L"1",L"1"));
			else
				m_pDB->addDefaults(CDefaults(DEF_USE_S_DOYLE,L"1",L"1"));
		}

		if (m_sRoundScaling.CompareNoCase(m_sNo) == 0)
		{
			if (m_pDB->isDefValueAdded(DEF_ROUND_SC_DIAM))
				m_pDB->updDefaults(CDefaults(DEF_ROUND_SC_DIAM,L"0",L"0"));
			else
				m_pDB->addDefaults(CDefaults(DEF_ROUND_SC_DIAM,L"0",L"0"));
		}
		else if (m_sRoundScaling.CompareNoCase(m_sYes) == 0)
		{
			if (m_pDB->isDefValueAdded(DEF_ROUND_SC_DIAM))
				m_pDB->updDefaults(CDefaults(DEF_ROUND_SC_DIAM,L"1",L"1"));
			else
				m_pDB->addDefaults(CDefaults(DEF_ROUND_SC_DIAM,L"1",L"1"));
		}

		if (m_pDB->isDefValueAdded(DEF_POUNDS_CUBIC_FOOT))
			m_pDB->updDefaults(CDefaults(DEF_POUNDS_CUBIC_FOOT,m_sPoundsPerCubicFoot,m_sPoundsPerCubicFoot));
		else
			m_pDB->addDefaults(CDefaults(DEF_POUNDS_CUBIC_FOOT,m_sPoundsPerCubicFoot,m_sPoundsPerCubicFoot));

		if (m_pDB->isDefValueAdded(DEF_BARK_RATIO))
			m_pDB->updDefaults(CDefaults(DEF_BARK_RATIO,localeToStr(m_sBarkRatio),localeToStr(m_sBarkRatio)));
		else
			m_pDB->addDefaults(CDefaults(DEF_BARK_RATIO,localeToStr(m_sBarkRatio),localeToStr(m_sBarkRatio)));

		if (m_pDB->isDefValueAdded(DEF_TAPER_IB))
			m_pDB->updDefaults(CDefaults(DEF_TAPER_IB,localeToStr(m_sTaperIB),localeToStr(m_sTaperIB)));
		else
			m_pDB->addDefaults(CDefaults(DEF_TAPER_IB,localeToStr(m_sTaperIB),localeToStr(m_sTaperIB)));

	}

	OnOK();
}

void CDefaultsDlg::OnBnClickedButton1()
{
	CString sFrequency = L"";
	CString sLogLength = L"";
/*	NOT SET
	m_sSawlogVolumeUnit = "Int-1/4";
	m_sPulpwoodVolumeUnit = L"Cubic feet";
	m_sDeductionType = L"PERCENT";
	m_sSpeciesCode = L"1";
	m_sLogGrade = L"SW";
	m_sLoadType = L"SHORT LOG";
*/
	m_nFrequency = 1;
	m_fLogLengthInch = 0.0;
	m_fLogLengthMM = 0.0;
	m_sUseSouthernDoyle = m_sNo;
	m_sRoundScaling = m_sNo;
	m_sPoundsPerCubicFoot.Format(L"%.1f",localeStrToDbl(L"1,0"));
	m_sBarkRatio.Format(L"%.1f",localeStrToDbl(L"0,1"));
	m_sTaperIB.Format(L"%.3f",localeStrToDbl(L"0,125"));

/*	NOT SET
	m_pDB->updDefaults(CDefaults(DEF_SAWLOG,m_sSawlogVolumeUnit,getCalcTypeBasisValue(m_sSawlogVolumeUnit)));
	m_pDB->updDefaults(CDefaults(DEF_PULPWOOD,m_sPulpwoodVolumeUnit,getCalcTypeBasisValue(m_sPulpwoodVolumeUnit)));
	m_pDB->updDefaults(CDefaults(DEF_DEDUCTION,m_sDeductionType,getCalcTypeBasisValue(m_sDeductionType)));
	m_pDB->updDefaults(CDefaults(DEF_SPC_CODE,m_sSpeciesCode,getCalcTypeBasisValue(m_sSpeciesCode)));
	m_pDB->updDefaults(CDefaults(DEF_LOG_GRADE,m_sLogGrade,getCalcTypeBasisValue(m_sLogGrade)));
	m_pDB->updDefaults(CDefaults(DEF_LOAD_TYPE,m_sLoadType,getCalcTypeBasisValue(m_sLoadType)));
*/	
	sFrequency.Format(L"%d",m_nFrequency);
	m_pDB->updDefaults(CDefaults(DEF_FREQUENCY,sFrequency,getCalcTypeBasisValue(sFrequency)));
	
	sLogLength.Format(L"%.1f",m_fLogLengthInch);
	m_pDB->updDefaults(CDefaults(DEF_LOG_LENGTH_INCH,sLogLength,sLogLength));
	sLogLength.Format(L"%.2f",m_fLogLengthMM);
	m_pDB->updDefaults(CDefaults(DEF_LOG_LENGTH_MM,sLogLength,sLogLength));
	m_pDB->updDefaults(CDefaults(DEF_USE_S_DOYLE,m_sUseSouthernDoyle,m_sUseSouthernDoyle));
	m_pDB->updDefaults(CDefaults(DEF_ROUND_SC_DIAM,m_sRoundScaling,m_sRoundScaling));

	m_pDB->updDefaults(CDefaults(DEF_POUNDS_CUBIC_FOOT,m_sPoundsPerCubicFoot,m_sPoundsPerCubicFoot));
	m_pDB->updDefaults(CDefaults(DEF_BARK_RATIO,localeToStr(m_sBarkRatio),localeToStr(m_sBarkRatio)));
	m_pDB->updDefaults(CDefaults(DEF_TAPER_IB,localeToStr(m_sTaperIB),localeToStr(m_sTaperIB)));
	
	m_propDefaults.Refresh();
	m_propDefaults.UpdateData();
	m_propDefaults.UpdateWindow();
}
