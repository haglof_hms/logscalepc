#include "stdafx.h"
#include "speciesformview.h"

#include "ResLangFileReader.h"

#include "reportclasses.h"

IMPLEMENT_DYNCREATE(CSpeciesReportView,  CXTPReportView) //CXTResizeFormView)

BEGIN_MESSAGE_MAP(CSpeciesReportView,  CXTPReportView) //CXTResizeFormView)
	ON_WM_DESTROY()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
END_MESSAGE_MAP()

CSpeciesReportView::CSpeciesReportView()
	: CXTPReportView(),m_pDB(NULL),m_bInitialized(FALSE)
{
}

CSpeciesReportView::~CSpeciesReportView()
{
}

void CSpeciesReportView::OnDestroy()
{
	saveSpecies(m_pDB);

	CXTPReportView::OnDestroy();
}

void CSpeciesReportView::OnInitialUpdate()
{
	CXTPReportView::OnInitialUpdate();

	if (!m_bInitialized)
	{
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupReport();

		m_bInitialized = TRUE;
	}

}

BOOL CSpeciesReportView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
//	if( !CXTResizeFormView::PreCreateWindow(cs) )
//		return FALSE;
	if( ! CXTPReportView::PreCreateWindow(cs) )
		return FALSE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CSpeciesReportView diagnostics

#ifdef _DEBUG
void CSpeciesReportView::AssertValid() const
{
	CXTPReportView::AssertValid();
}

void CSpeciesReportView::Dump(CDumpContext& dc) const
{
	CXTPReportView::Dump(dc);
//	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CSpeciesReportView message handlers

LRESULT CSpeciesReportView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch (wParam)
	{
		case ID_SAVE_ITEM :
		{
			break;
		}	// case ID_SAVE_ITEM :
	}	// switch (wParam)
	return 0L;
}

// Create and add Assortment settings reportwindow
void CSpeciesReportView::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_0, xml.str(IDS_STRING1201), 80,FALSE));
			pCol->AllowRemove(FALSE);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;

			pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_1, xml.str(IDS_STRING1202), 200,FALSE));
			pCol->AllowRemove(FALSE);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;

			GetReportCtrl().GetReportHeader()->AllowColumnRemove(FALSE);
			GetReportCtrl().SetMultipleSelection( FALSE );
			GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );
			GetReportCtrl().SetGridStyle( FALSE, xtpReportGridSmallDots );
			GetReportCtrl().FocusSubItems(TRUE);
			GetReportCtrl().AllowEdit(TRUE);

			// Strings
			m_sMsgCap = xml.str(IDS_STRING99);
			m_sMsgDelete1 = xml.str(IDS_STRING1253);
			m_sMsgDelete2 = xml.str(IDS_STRING1254);

			xml.clean();

		}
	}
}

// PUBLIC
void CSpeciesReportView::populateReport(CDBHandling *pDB)
{
	if (m_pDB == NULL)
		m_pDB = pDB;
	// We'll also add this new item to DB
	if (pDB != NULL)
	{
		pDB->getSpecies(m_vecSpecies,GetReportCtrl());
	}
}

void CSpeciesReportView::addSpecies(CDBHandling *pDB)
{
	GetReportCtrl().AddRecord(new CSpeciesReportRec(CSpecies()));
	GetReportCtrl().Populate();
	GetReportCtrl().UpdateWindow();
}

void CSpeciesReportView::deleteSpecies(CDBHandling *pDB)
{
	// Ask user twice, if he realy want's to delete
	if (::MessageBox(GetSafeHwnd(),m_sMsgDelete1,m_sMsgCap,MB_ICONQUESTION | MB_YESNO) == IDNO)
	{
		return;
	}
	else	if (::MessageBox(GetSafeHwnd(),m_sMsgDelete2,m_sMsgCap,MB_ICONQUESTION | MB_YESNO) == IDNO)
	{
		return;
	}

	CXTPReportRow *pRow = GetReportCtrl().GetFocusedRow();
	CSpeciesReportRec *rec = NULL;
	// We'll also add this new item to DB
	if (pDB != NULL && pRow != NULL)
	{
		rec = (CSpeciesReportRec*)pRow->GetRecord();
		if (pDB->delSpecies(rec->getRecord()))
		{
			pDB->getSpecies(m_vecSpecies,GetReportCtrl());
			// Check if there's no species left. If so, reset IDENTITY-field
			if (m_vecSpecies.size() == 0)
				pDB->resetSpeciesIdentityField();
		}
	}
}

void CSpeciesReportView::saveSpecies(CDBHandling *pDB)
{
	CXTPReportRecords *pRecs = GetReportCtrl().GetRecords();
	CSpeciesReportRec *rec = NULL;
	// We'll also add this new item to DB
	if (pDB != NULL && pRecs != NULL)
	{
		GetReportCtrl().Populate();
		for (int i = 0;i < pRecs->GetCount();i++)
		{
			rec = (CSpeciesReportRec*)pRecs->GetAt(i);
			if (rec->getRecord().getSpcID() == -1)
			{
				pDB->newSpecies(CSpecies(-1,rec->getColText(COLUMN_0),rec->getColText(COLUMN_1)));
			}
			else if (rec->getRecord().getSpcID() > 0)
			{
				pDB->updSpecies(CSpecies(rec->getRecord().getSpcID(),rec->getColText(COLUMN_0),rec->getColText(COLUMN_1)));
			}
		}

		pDB->getSpecies(m_vecSpecies,GetReportCtrl());
	}
}
