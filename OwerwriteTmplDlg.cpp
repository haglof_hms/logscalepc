// OwerwriteTmplDlg.cpp : implementation file
//

#include "stdafx.h"
#include "OwerwriteTmplDlg.h"

#include "ResLangFileReader.h"

// COwerwriteTmplDlg dialog

IMPLEMENT_DYNAMIC(COwerwriteTmplDlg, CXTResizeDialog)

BEGIN_MESSAGE_MAP(COwerwriteTmplDlg, CXTResizeDialog)
	ON_BN_CLICKED(IDC_BUTTON18_2, &COwerwriteTmplDlg::OnBnClickedButton182)
	ON_EN_CHANGE(IDC_EDIT18_1, &COwerwriteTmplDlg::OnEnChangeEdit181)
	ON_BN_CLICKED(IDC_BUTTON18_1, &COwerwriteTmplDlg::OnBnClickedButton181)
END_MESSAGE_MAP()

COwerwriteTmplDlg::COwerwriteTmplDlg(CWnd* pParent /*=NULL*/)
	: CXTResizeDialog(COwerwriteTmplDlg::IDD, pParent),
	m_bInitialized(FALSE)
{

}

COwerwriteTmplDlg::~COwerwriteTmplDlg()
{
}

BOOL COwerwriteTmplDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeDialog::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}


void COwerwriteTmplDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL18_1, m_lbl18_1);
	DDX_Control(pDX, IDC_LBL18_2, m_lbl18_2);
	DDX_Control(pDX, IDC_LBL18_3, m_lbl18_3);
	DDX_Control(pDX, IDC_LBL18_4, m_lbl18_4);
	DDX_Control(pDX, IDC_LBL18_5, m_lbl18_5);

	DDX_Control(pDX, IDC_EDIT18_1, m_edit18_1);

	DDX_Control(pDX, IDC_BUTTON18_1, m_btnOwerWrite);
	DDX_Control(pDX, IDC_BUTTON18_2, m_btnSaveAs);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	//}}AFX_DATA_MAP
}

// CDefaultsDlg message handlers
BOOL COwerwriteTmplDlg::OnInitDialog()
{
	CXTResizeDialog::OnInitDialog();
	if (!m_bInitialized)
	{

		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
		m_btnSaveAs.EnableWindow(FALSE);

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				SetWindowText(xml.str(IDS_STRING5800));
				m_lbl18_1.SetBkColor(INFOBK);
				m_lbl18_5.SetBkColor(INFOBK);
				m_lbl18_1.SetWindowTextW(xml.str(IDS_STRING5801));
				m_lbl18_2.SetWindowTextW(xml.str(IDS_STRING5810));
				m_lbl18_3.SetWindowTextW(xml.str(IDS_STRING5811));
				m_lbl18_4.SetWindowTextW(xml.str(IDS_STRING5812));

				m_btnOwerWrite.SetWindowTextW(xml.str(IDS_STRING5820));
				m_btnSaveAs.SetWindowTextW(xml.str(IDS_STRING5821));
				m_btnCancel.SetWindowTextW(xml.str(IDS_STRING101));

				m_sMsgCap = xml.str(IDS_STRING99);
				m_sMsgFileNameExists = xml.str(IDS_STRING5830);


				m_lbl18_5.SetWindowTextW(m_sFileName);
	
			}
		}

		m_bInitialized = TRUE;
	}

	return TRUE;
}


// COwerwriteTmplDlg message handlers

void COwerwriteTmplDlg::setPath(LPCTSTR v)	
{ 
	m_sPath = extractFilePath(v); 
	m_sFileName = extractFileName(v);
}

// Button OwerWrite clicked
void COwerwriteTmplDlg::OnBnClickedButton181()
{
	m_nReturnValue = 1;
	OnOK();
}

// Button Save As clicked
void COwerwriteTmplDlg::OnBnClickedButton182()
{
	CString sFileName(m_edit18_1.getText());
	m_nReturnValue = 2;
	// get new filename and check that this filename isn't already used.
	// If so ask user to add another filename and check ...
	m_sNewPathAndFilenName = m_sPath + sFileName + L".lst";
	if (fileExists(m_sNewPathAndFilenName))
	{
		::MessageBox(GetSafeHwnd(),m_sMsgFileNameExists,m_sMsgCap,MB_ICONSTOP | MB_OK);
	}
	else
		OnOK();
}

void COwerwriteTmplDlg::OnEnChangeEdit181()
{
	m_btnSaveAs.EnableWindow(!m_edit18_1.getText().IsEmpty());
}

