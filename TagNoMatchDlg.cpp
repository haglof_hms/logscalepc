// TagNoMatchDlg.cpp : implementation file
//

#include "stdafx.h"
#include "TagNoMatchDlg.h"

#include "libxl.h"

#include "ResLangFileReader.h"

using namespace libxl;

// CTagNoMatchDlg dialog

IMPLEMENT_DYNAMIC(CTagNoMatchDlg, CXTResizeDialog)



BEGIN_MESSAGE_MAP(CTagNoMatchDlg, CXTResizeDialog)
	ON_BN_CLICKED(IDC_BUTTON16_1, &CTagNoMatchDlg::OnBnClickedButton161)
END_MESSAGE_MAP()

CTagNoMatchDlg::CTagNoMatchDlg(CWnd* pParent /*=NULL*/,short open_as)
	: CXTResizeDialog(CTagNoMatchDlg::IDD, pParent),
	m_bInitialized(FALSE),
	m_nOpenAs(open_as),
	m_nTotalNumberOfLogs(0)
{

}

CTagNoMatchDlg::~CTagNoMatchDlg()
{
}

BOOL CTagNoMatchDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeDialog::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}


void CTagNoMatchDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL16_1, m_lbl16_1);
	DDX_Control(pDX, IDC_LIST16_1, m_list16_1);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_BUTTON16_1, m_btn16_1);
	//}}AFX_DATA_MAP
}

// CDefaultsDlg message handlers
BOOL CTagNoMatchDlg::OnInitDialog()
{
	CString sTmp = L"";
	CXTResizeDialog::OnInitDialog();
	if (!m_bInitialized)
	{

		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				SetWindowText(xml.str(IDS_STRING5600));

				
				m_sMsgCap = xml.str(IDS_STRING99);
				m_sMsgOpenEXCEL = xml.str(IDS_STRING1124);

				m_sMissing = xml.str(IDS_STRING5630);
				m_sNotInStock = xml.str(IDS_STRING5631);
				m_sDuplicateTags = xml.str(IDS_STRING5632);

				m_sTotalNumOfLogs = xml.str(IDS_STRING5640);
				m_sAlreadyRegOnOtherSale = xml.str(IDS_STRING5641);
				m_sAlreadyRegOnOtherTicket = xml.str(IDS_STRING5644);
				m_sNotRegistered = xml.str(IDS_STRING5642);
				m_sDuplicateTagNumbers = xml.str(IDS_STRING5645);

				m_sAllLogsOK = xml.str(IDS_STRING5643);

				m_sMsgNoInfoOnSheet = xml.str(IDS_STRING5646);
				
				m_sOKTags = xml.str(IDS_STRING5647);

				m_lbl16_1.SetBkColor(INFOBK);
				// OpenAs = Ticket
				if (m_nOpenAs == 0)
				{
					m_sMsgCapEXCEL = xml.str(IDS_STRING5603);
				}
				// OpenAs = Sales
				else if (m_nOpenAs == 1)
				{
					m_sMsgCapEXCEL = xml.str(IDS_STRING5601);
				}

				m_btnOK.SetWindowText(xml.str(IDS_STRING121));
				m_btnCancel.SetWindowText(xml.str(IDS_STRING101));

				m_btn16_1.SetWindowText(xml.str(IDS_STRING5620));

				tokenizeString(xml.str(IDS_STRING203),';',m_sarrStatus);

				m_list16_1.SetExtendedStyle(m_list16_1.GetStyle() | LVS_EX_GRIDLINES);
				m_list16_1.InsertColumn(0,xml.str(IDS_STRING5610),LVCFMT_LEFT,150);
				m_list16_1.InsertColumn(1,xml.str(IDS_STRING5611),LVCFMT_LEFT,120);
				m_list16_1.InsertColumn(2,xml.str(IDS_STRING5612),LVCFMT_LEFT,80);
				m_list16_1.InsertColumn(3,xml.str(IDS_STRING5613),LVCFMT_LEFT,250);

			}
		}

		setupListCtrl();

		m_bInitialized = TRUE;
	}

	return TRUE;
}


// CTagNoMatchDlg message handlers

void CTagNoMatchDlg::setupListCtrl()
{
	m_nMissing = 0;
	CString sStatus = L"",sTicket = L"";
	int nRow = m_vecLogTagsNotOK.size()+1;
	m_nDuplicateTagNumbers = 0;

	if (m_vecLogTagsNotOK.size() > 0)
	{
		// I think we should remove from m_mapLogTags if already in m_vecLogTagsNotOK
		if (m_mapLogTags.size() > 0)
		{
			for (CVecLogs::iterator itnl = m_vecLogTagsNotOK.begin();itnl != m_vecLogTagsNotOK.end();++itnl)
			{
				if (m_mapLogTags[itnl->getTagNumber()] >= 0)
					m_mapLogTags[itnl->getTagNumber()] = -1;
			}
		}
		for (UINT i = 0;i < m_vecLogTagsNotOK.size();i++)
		{
			sStatus.Empty();
			if (m_mapLogTagsDup[m_vecLogTagsNotOK[i].getTagNumber()] > 1)
			{
				sStatus = m_sDuplicateTags;
			}

			InsertRow(m_list16_1,i,false,4,m_vecLogTagsNotOK[i].getTicket(),m_vecLogTagsNotOK[i].getTagNumber(),m_sarrStatus[m_vecLogTagsNotOK[i].getStatusFlag()],sStatus);
		}
	}
	if (m_mapLogTags.size() > 0 && m_sarrStatus.GetCount() > 0)
	{
		for (CStrIntMap::iterator it = m_mapLogTags.begin(); it != m_mapLogTags.end();++it)
		{
			if (it->second == 0 || it->second > 1 || m_mapLogTagsDup[it->first] > 0)
			{
				// Setup in categories

				// Not in stock and no duplicates in tag-file
				if (it->second == 0 && m_mapLogTagsDup[it->first] == 1)
				{
					InsertRow(m_list16_1,nRow,false,4,L"",it->first,m_sMissing,m_sNotInStock);
					nRow++;
					m_nMissing++;
			}
				// Not in stock and duplicates in tag-file
				else if (it->second == 0 && m_mapLogTagsDup[it->first] > 1)
				{
					InsertRow(m_list16_1,nRow,false,4,L"",it->first,m_sMissing,m_sNotInStock + L" , " + m_sDuplicateTags);
					nRow++;
					m_nMissing++;
				}
				// In stock and duplicates in tag-file
				else if (it->second > 1 && m_mapLogTagsDup[it->first] > 1)
				{
					// We'll try to find ticket for this duplicate tag in tag-file
					for (CVecLogs::iterator itl = m_vecLogTagsOK.begin(); itl != m_vecLogTagsOK.end();++itl)
					{
						if (itl->getTagNumber().CompareNoCase(it->first) == 0)
						{
							sTicket = itl->getTicket();
						}
					}

					InsertRow(m_list16_1,nRow,false,4,sTicket,it->first,m_sarrStatus[0],m_sDuplicateTags);
					nRow++;
				}

			}
		}
	}


	// Setup information on label
  if (m_nOpenAs == 0)
	{
		if (m_vecLogTagsNotOK.size() > 0 || m_nTotalNumberOfLogs != m_mapLogTags.size())
		{
			for (CStrIntMap::iterator it = m_mapLogTags.begin();it != m_mapLogTags.end();++it)
			{
				if (it->second > 1)
				{
					m_nDuplicateTagNumbers += (it->second - 1);
				}
			}

			m_sMsgTotalNumOfLogs.Format(L"%s : %d",m_sTotalNumOfLogs,m_nTotalNumberOfLogs);
			m_sMsgAlreadyRegOnOtherTicket.Format(L"%s : %d",m_sAlreadyRegOnOtherTicket,m_vecLogTagsNotOK.size());
			m_sMsgDuplicateTagNumbers.Format(L"%s : %d",m_sDuplicateTagNumbers,m_nDuplicateTagNumbers);
			m_sMsgOKTags.Format(m_sOKTags,m_nTotalNumberOfLogs - (m_nDuplicateTagNumbers + m_vecLogTagsNotOK.size()),m_nTotalNumberOfLogs);
			m_lbl16_1.SetWindowTextW(m_sMsgTotalNumOfLogs + L"\n" + m_sMsgAlreadyRegOnOtherTicket + L"\n" + m_sMsgDuplicateTagNumbers + L"\n" + m_sMsgOKTags);
		}
		else
		{
			m_sMsgAllLogsOK.Format(m_sAllLogsOK,m_nTotalNumberOfLogs);
			m_lbl16_1.SetWindowTextW(L"\n" + m_sMsgAllLogsOK);
		}
	}
	else if (m_nOpenAs == 1)
	{

		for (CStrIntMap::iterator it = m_mapLogTagsDup.begin();it != m_mapLogTagsDup.end();++it)
		{
			if (it->second > 1)
			{
				m_nDuplicateTagNumbers += (it->second - 1);
			}
		}

		if (m_vecLogTagsNotOK.size() > 0 || m_nMissing > 0 || m_nDuplicateTagNumbers > 0)
		{
			m_sMsgTotalNumOfLogs.Format(L"%s : %d",m_sTotalNumOfLogs,m_nTotalNumberOfLogs);
			m_sMsgAlreadyRegOnOtherSale.Format(L"%s : %d",m_sAlreadyRegOnOtherSale,m_vecLogTagsNotOK.size());
			m_sMsgNotRegistered.Format(L"%s : %d",m_sNotRegistered,m_nMissing);
			m_sMsgDuplicateTagNumbers.Format(L"%s : %d",m_sDuplicateTagNumbers,m_nDuplicateTagNumbers);
			m_lbl16_1.SetWindowTextW(m_sMsgTotalNumOfLogs + L"\n" + m_sMsgAlreadyRegOnOtherSale + L"\n" + m_sMsgNotRegistered + L"\n" + m_sMsgDuplicateTagNumbers);
		}
		else
		{
			m_sMsgAllLogsOK.Format(m_sAllLogsOK,m_nTotalNumberOfLogs);
			m_lbl16_1.SetWindowTextW(L"\n" + m_sMsgAllLogsOK);
		}
	}
}

CString CTagNoMatchDlg::getSpcName(LPCTSTR spc_code)
{
	if (m_vecSpecies.size() == 0) return L"";
	
	for (UINT i = 0;i < m_vecSpecies.size();i++)
	{
		if (m_vecSpecies[i].getSpcCode().CompareNoCase(spc_code) == 0)
			return m_vecSpecies[i].getSpcName();
	}
	return L"";
}

// Create an Excel file with unvalid Tags
void CTagNoMatchDlg::OnBnClickedButton161()
{
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	int f[4],nRow = 0;
	CLogs recLog;
	CString sValue = L"",sTmp = L"";
	RLFReader xml;

	if (!fileExists(m_sLangFN)) return;

	Book* book = xlCreateBook();
	if(book)
	{
		// Set registraton-key
		book->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0hfeflb");
		f[0] = book->addCustomNumFormat(L"0.0");
		f[1] = book->addCustomNumFormat(L"0.00");
		f[2] = book->addCustomNumFormat(L"0.000");
		f[3] = book->addCustomNumFormat(L"0");
		libxl::Format* fmt[6];
		libxl::Font *fnt = book->addFont();
		fnt->setSize(8);
		fnt->setBold(true);
		libxl::Font *fnt2 = book->addFont();
		fnt->setSize(9);
		fnt->setBold(true);
		libxl::Font *fnt3 = book->addFont();
		fnt->setSize(9);
		fnt->setBold(true);
		fmt[0] = book->addFormat();
		fmt[0]->setNumFormat(f[0]);
		fmt[0]->setFont(fnt);
		fmt[1] = book->addFormat();
		fmt[1]->setNumFormat(f[1]);
		fmt[1]->setFont(fnt);
		fmt[2] = book->addFormat();
		fmt[2]->setNumFormat(f[2]);
		fmt[2]->setFont(fnt);
		fmt[3] = book->addFormat();
		fmt[3]->setWrap();
		fmt[3]->setFont(fnt);
		fmt[4] = book->addFormat();
		fmt[4]->setNumFormat(f[3]);
		fmt[4]->setWrap();
		fmt[4]->setFont(fnt2);
		fmt[5] = book->addFormat();
		fmt[5]->setFont(fnt3);
		Sheet* sheet = book->addSheet(L"Sheet1");
		if (xml.Load(m_sLangFN))
		{
			if(sheet)
			{
				sheet->writeStr(0, 0,m_sMsgCapEXCEL,fmt[5]);

				if (m_vecLogTagsNotOK.size() > 0)
				{
					sheet->setCol(0,24,7);
					if (m_nOpenAs == 0)
					{
						sheet->writeStr(2, 0,xml.str(IDS_STRING2001));
						sheet->writeStr(2, 1,m_sTicket);
			
						sheet->writeStr(5, 0,xml.str(IDS_STRING1700),fmt[3]);
						sheet->writeStr(5, 1,xml.str(IDS_STRING1701),fmt[3]);
						sheet->writeStr(5, 2,xml.str(IDS_STRING1762),fmt[3]);
						sheet->writeStr(5, 3,xml.str(IDS_STRING1702),fmt[3]);
						sheet->writeStr(5, 4,xml.str(IDS_STRING1703),fmt[3]);
						sheet->writeStr(5, 5,xml.str(IDS_STRING1704),fmt[3]);
						sheet->writeStr(5, 6,xml.str(IDS_STRING1705),fmt[3]);
						sheet->writeStr(5, 7,xml.str(IDS_STRING17050),fmt[3]);
						sheet->writeStr(5, 8,xml.str(IDS_STRING1706),fmt[3]);
						sheet->writeStr(5, 9,xml.str(IDS_STRING17060),fmt[3]);
						sheet->writeStr(5, 10,xml.str(IDS_STRING1707),fmt[3]);
						sheet->writeStr(5, 11,xml.str(IDS_STRING17070),fmt[3]);
						sheet->writeStr(5, 12,xml.str(IDS_STRING1708),fmt[3]);
						sheet->writeStr(5, 13,xml.str(IDS_STRING17080),fmt[3]);
						sheet->writeStr(5, 14,xml.str(IDS_STRING17090),fmt[3]);
						sheet->writeStr(5, 15,xml.str(IDS_STRING1710),fmt[3]);
						sheet->writeStr(5, 16,xml.str(IDS_STRING1719),fmt[3]);
						sheet->writeStr(5, 17,xml.str(IDS_STRING17190),fmt[3]);
						sheet->writeStr(5, 18,xml.str(IDS_STRING1718),fmt[3]);
						sheet->writeStr(5, 19,xml.str(IDS_STRING17180),fmt[3]);
						sheet->writeStr(5, 20,xml.str(IDS_STRING1720),fmt[3]);
					}
					else if (m_nOpenAs == 1)
					{
						sheet->writeStr(2, 0,xml.str(IDS_STRING5621));
						sheet->writeStr(2, 1,m_sTicketFile);

						sheet->writeStr(5, 0,xml.str(IDS_STRING2001),fmt[3]);
						sheet->writeStr(5, 1,xml.str(IDS_STRING1700),fmt[3]);
						sheet->writeStr(5, 2,xml.str(IDS_STRING1701),fmt[3]);
						sheet->writeStr(5, 3,xml.str(IDS_STRING1762),fmt[3]);
						sheet->writeStr(5, 4,xml.str(IDS_STRING1702),fmt[3]);
						sheet->writeStr(5, 5,xml.str(IDS_STRING1703),fmt[3]);
						sheet->writeStr(5, 6,xml.str(IDS_STRING1704),fmt[3]);
						sheet->writeStr(5, 7,xml.str(IDS_STRING1705),fmt[3]);
						sheet->writeStr(5, 8,xml.str(IDS_STRING17050),fmt[3]);
						sheet->writeStr(5, 9,xml.str(IDS_STRING1706),fmt[3]);
						sheet->writeStr(5, 10,xml.str(IDS_STRING17060),fmt[3]);
						sheet->writeStr(5, 11,xml.str(IDS_STRING1707),fmt[3]);
						sheet->writeStr(5, 12,xml.str(IDS_STRING17070),fmt[3]);
						sheet->writeStr(5, 13,xml.str(IDS_STRING1708),fmt[3]);
						sheet->writeStr(5, 14,xml.str(IDS_STRING17080),fmt[3]);
						sheet->writeStr(5, 15,xml.str(IDS_STRING17090),fmt[3]);
						sheet->writeStr(5, 16,xml.str(IDS_STRING1710),fmt[3]);
						sheet->writeStr(5, 17,xml.str(IDS_STRING1719),fmt[3]);
						sheet->writeStr(5, 18,xml.str(IDS_STRING17190),fmt[3]);
						sheet->writeStr(5, 19,xml.str(IDS_STRING1718),fmt[3]);
						sheet->writeStr(5, 20,xml.str(IDS_STRING17180),fmt[3]);
						sheet->writeStr(5, 21,xml.str(IDS_STRING1720),fmt[3]);
					}


					for (UINT row = 0;row < m_vecLogTagsNotOK.size();row++)
					{
						recLog = m_vecLogTagsNotOK[row];
						// OpenAs = Ticket
						if (m_nOpenAs == 0)
						{
							// Columns for row
							sheet->writeStr(row+6, 0,recLog.getTagNumber(),fmt[0]);
							sheet->writeStr(row+6, 1,recLog.getSpcCode(),fmt[0]);
							if (recLog.getStatusFlag() >= 0 && recLog.getStatusFlag() < m_sarrStatus.GetCount())
								sheet->writeStr(row+6, 2,m_sarrStatus.GetAt(recLog.getStatusFlag()),fmt[0]);
							else
								sheet->writeStr(row+6, 2,L"N/A",fmt[0]);

							sValue.Format(L"%d",recLog.getFrequency());
							sheet->writeStr(row+6, 3,sValue,fmt[0]);
							sheet->writeStr(row+6, 4,recLog.getGrade(),fmt[0]);						
							sheet->writeNum(row+6, 5,recLog.getDeduction(),fmt[0]);
							sheet->writeNum(row+6, 6,recLog.getDOBTop(),fmt[2]);
							sheet->writeNum(row+6, 7,recLog.getDOBTop2(),fmt[2]);
							sheet->writeNum(row+6, 8,recLog.getDIBTop(),fmt[2]);
							sheet->writeNum(row+6, 9,recLog.getDIBTop2(),fmt[2]);
							sheet->writeNum(row+6, 10,recLog.getDOBRoot(),fmt[2]);
							sheet->writeNum(row+6, 11,recLog.getDOBRoot2(),fmt[2]);
							sheet->writeNum(row+6, 12,recLog.getDIBRoot(),fmt[2]);
							sheet->writeNum(row+6, 13,recLog.getDIBRoot2(),fmt[2]);
							sheet->writeNum(row+6, 14,recLog.getLengthMeas(),fmt[2]);
							sheet->writeNum(row+6, 15,recLog.getBark(),fmt[2]);
							sheet->writeNum(row+6, 16,recLog.getURootDia(),fmt[2]);
							sheet->writeNum(row+6, 17,recLog.getURootDia2(),fmt[2]);
							sheet->writeNum(row+6, 18,recLog.getUTopDia(),fmt[2]);
							sheet->writeNum(row+6, 19,recLog.getUTopDia2(),fmt[2]);
							sheet->writeNum(row+6, 20,recLog.getULength(),fmt[2]);
						}
						// OpenAs = Sales
						else if (m_nOpenAs == 1)
						{
							// Columns for row
							sheet->writeStr(row+6, 0,recLog.getTicket(),fmt[0]);
							sheet->writeStr(row+6, 1,recLog.getTagNumber(),fmt[0]);
							sheet->writeStr(row+6, 2,recLog.getSpcCode(),fmt[0]);
							if (recLog.getStatusFlag() >= 0 && recLog.getStatusFlag() < m_sarrStatus.GetCount())
								sheet->writeStr(row+6, 3,m_sarrStatus.GetAt(recLog.getStatusFlag()),fmt[0]);
							else
								sheet->writeStr(row+6, 3,L"N/A",fmt[0]);

							sValue.Format(L"%d",recLog.getFrequency());
							sheet->writeStr(row+6, 4,sValue,fmt[0]);
							sheet->writeStr(row+6, 5,recLog.getGrade(),fmt[0]);						
							sheet->writeNum(row+6, 6,recLog.getDeduction(),fmt[0]);
							sheet->writeNum(row+6, 7,recLog.getDOBTop(),fmt[2]);
							sheet->writeNum(row+6, 8,recLog.getDOBTop2(),fmt[2]);
							sheet->writeNum(row+6, 9,recLog.getDIBTop(),fmt[2]);
							sheet->writeNum(row+6, 10,recLog.getDIBTop2(),fmt[2]);
							sheet->writeNum(row+6, 11,recLog.getDOBRoot(),fmt[2]);
							sheet->writeNum(row+6, 12,recLog.getDOBRoot2(),fmt[2]);
							sheet->writeNum(row+6, 13,recLog.getDIBRoot(),fmt[2]);
							sheet->writeNum(row+6, 14,recLog.getDIBRoot2(),fmt[2]);
							sheet->writeNum(row+6, 15,recLog.getLengthMeas(),fmt[2]);
							sheet->writeNum(row+6, 16,recLog.getBark(),fmt[2]);
							sheet->writeNum(row+6, 17,recLog.getURootDia(),fmt[2]);
							sheet->writeNum(row+6, 18,recLog.getURootDia2(),fmt[2]);
							sheet->writeNum(row+6, 19,recLog.getUTopDia(),fmt[2]);
							sheet->writeNum(row+6, 20,recLog.getUTopDia2(),fmt[2]);
							sheet->writeNum(row+6, 21,recLog.getULength(),fmt[2]);
						}	// if (m_nOpenAs == 1)
					}	// for (UINT row = 0;row < m_vecLogTagsNotOK.size();row++)
				} // if (m_vecLogTagsNotOK.size() > 0)
			}	// if(sheet)

			// We'll also add extra information on Missing and/or duplicate tagnumbers
			// in tag-file;

			if (m_nMissing > 0 || m_nDuplicateTagNumbers > 0 || m_vecLogTagsNotOK.size() > 0)
			{
//				Sheet* sheet = book->addSheet(L"Sheet2");
				if(sheet)
				{
					sheet->setCol(0,0,15);
					sheet->setCol(1,2,10);
					sheet->setCol(3,3,25);

					if (m_nOpenAs == 0)
					{
						sheet->writeStr(2, 0, m_sTotalNumOfLogs, fmt[3]);
						sheet->writeStr(3, 0, m_sAlreadyRegOnOtherTicket, fmt[3]);
						sheet->writeStr(4, 0, m_sDuplicateTagNumbers, fmt[3]);
						sheet->writeNum(2, 1, m_nTotalNumberOfLogs, fmt[4]);
						sheet->writeNum(3, 1, m_vecLogTagsNotOK.size(), fmt[4]);
						sheet->writeNum(4, 1, m_nDuplicateTagNumbers, fmt[4]);
						nRow =  sheet->lastRow() + 1;
					}
					else if (m_nOpenAs == 1)
					{
						sheet->writeStr(2, 0, m_sTotalNumOfLogs, fmt[3]);
						sheet->writeStr(3, 0, m_sAlreadyRegOnOtherSale, fmt[3]);
						sheet->writeStr(4, 0, m_sNotRegistered, fmt[3]);
						sheet->writeStr(5, 0, m_sDuplicateTagNumbers, fmt[3]);
						sheet->writeNum(2, 1, m_nTotalNumberOfLogs, fmt[4]);
						sheet->writeNum(3, 1, m_vecLogTagsNotOK.size(), fmt[4]);
						sheet->writeNum(4, 1, m_nMissing, fmt[4]);
						sheet->writeNum(5, 1, m_nDuplicateTagNumbers, fmt[4]);
						nRow =  sheet->lastRow() + 2;
					}

					if (m_list16_1.GetItemCount() > 0)
					{
						sheet->writeStr(nRow, 0, xml.str(IDS_STRING5610), fmt[3]);
						sheet->writeStr(nRow, 1, xml.str(IDS_STRING5611), fmt[3]);
						sheet->writeStr(nRow, 2, xml.str(IDS_STRING5612), fmt[3]);
						sheet->writeStr(nRow, 3, xml.str(IDS_STRING5613), fmt[3]);
						for (int i = nRow;i < m_list16_1.GetItemCount()+nRow;i++)
						{
							sheet->writeStr(i+1, 0, m_list16_1.GetItemText(i-nRow,0), fmt[3]);
							sheet->writeStr(i+1, 1, m_list16_1.GetItemText(i-nRow,1), fmt[3]);
							sheet->writeStr(i+1, 2, m_list16_1.GetItemText(i-nRow,2), fmt[3]);
							sheet->writeStr(i+1, 3, m_list16_1.GetItemText(i-nRow,3), fmt[3]);
						}
					}	// if (m_list16_1.GetItemCount() > 0)	
				}	// if(sheet2)
			}
			else
			{
				Sheet* sheet = book->addSheet(L"Sheet2");
				if(sheet)
				{
					sheet->writeStr(0, 0, m_sMsgNoInfoOnSheet);					
				}
			}

			xml.clean();
		}	// if (xml.Load(m_sLangFN))

		sTmp.Format(L"%s_%s",m_sTicket,m_sTicketFile);
		CString sPath = L"";
		sPath = regGetStr(REG_ROOT,TICKET_MATCHING_DIR,DIRECTORY_KEY);
		sPath += m_sTicketFile;
		
		CFileDialog dlg(FALSE,L".xls",sPath,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,L"EXCEL *.xls|*.xls|");
		if (dlg.DoModal() == IDOK)
		{
			if(book->save(dlg.GetPathName())) 
			{
#ifdef _OPEN_EXCEL_IN_MATCH_TAG
				//*	Uncomment to open excel after saving file
				if (::MessageBox(GetSafeHwnd(),m_sMsgOpenEXCEL,m_sMsgCap,MB_ICONQUESTION | MB_YESNO) == IDYES)
						::ShellExecute(NULL, L"open", dlg.GetPathName(), NULL, NULL, SW_SHOW);        
				//*/
#endif
			}
			else
			{
				CString S = L"";
				S.Format(L"%S",book->errorMessage());
				AfxMessageBox(S);
			}
			regSetStr(REG_ROOT,TICKET_MATCHING_DIR,DIRECTORY_KEY,getFilePath(dlg.GetPathName()));

		}

		book->release();		
	} 

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
