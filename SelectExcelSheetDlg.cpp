// SelectExcelSheetDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SelectExcelSheetDlg.h"

#include "ResLangFileReader.h"

// CSelectExcelSheetDlg dialog

IMPLEMENT_DYNAMIC(CSelectExcelSheetDlg, CDialog)

BEGIN_MESSAGE_MAP(CSelectExcelSheetDlg, CDialog)
	ON_CBN_SELCHANGE(IDC_COMBO17_1, &CSelectExcelSheetDlg::OnCbnSelchangeCombo171)
	ON_CBN_SELCHANGE(IDC_COMBO17_2, &CSelectExcelSheetDlg::OnCbnSelchangeCombo172)
	ON_CBN_SELCHANGE(IDC_COMBO17_3, &CSelectExcelSheetDlg::OnCbnSelchangeCombo173)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST17_1, &CSelectExcelSheetDlg::OnLvnItemchangedList171)
	ON_BN_CLICKED(IDOK, &CSelectExcelSheetDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON17_1, &CSelectExcelSheetDlg::OnBnClickedButton171)
	ON_BN_CLICKED(IDC_BUTTON17_2, &CSelectExcelSheetDlg::OnBnClickedButton172)
	ON_NOTIFY(LVN_ITEMCHANGING, IDC_LIST17_1, &CSelectExcelSheetDlg::OnLvnItemchangingList171)
END_MESSAGE_MAP()

CSelectExcelSheetDlg::CSelectExcelSheetDlg(CWnd* pParent /*=NULL*/,short open_as)
	: CDialog(CSelectExcelSheetDlg::IDD, pParent),
	m_bInitialized(FALSE),
	m_sLangFN(L""),
	m_bSelectedIndexSheets(false),
	m_nSelectedIndexTableType(CB_ERR),
	m_nSelectedIndexMeasMode(CB_ERR),
	m_nOpenAs(open_as),
	m_nLastCheckedItem(-1)
{
	m_vecSheetsSelected.clear();
}

CSelectExcelSheetDlg::~CSelectExcelSheetDlg()
{
}

void CSelectExcelSheetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_BUTTON17_1, m_btnSelectAll);
	DDX_Control(pDX, IDC_BUTTON17_2, m_btnDeSelectAll);

	DDX_Control(pDX, IDC_COMBO17_2, m_cbx17_2);
	DDX_Control(pDX, IDC_COMBO17_3, m_cbx17_3);

	DDX_Control(pDX, IDC_CHECK17_1, m_check17_1);

	DDX_Control(pDX, IDC_LBL17_1, m_lbl17_1);
	DDX_Control(pDX, IDC_LBL17_2, m_lbl17_2);
	DDX_Control(pDX, IDC_LBL17_3, m_lbl17_3);
	DDX_Control(pDX, IDC_LBL17_4, m_lbl17_4);

	DDX_Control(pDX, IDC_LIST17_1, m_list17_1);
	//}}AFX_DATA_MAP

}

BOOL CSelectExcelSheetDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CDialog::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

BOOL CSelectExcelSheetDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (!m_bInitialized)
	{
		m_lbl17_1.SetBkColor(INFOBK);
		m_lbl17_1.SetTextColor(BLUE);

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{

				SetWindowText(xml.str(IDS_STRING5700));
	
				m_btnOK.SetWindowTextW(xml.str(IDS_STRING100));
				m_btnCancel.SetWindowTextW(xml.str(IDS_STRING101));
				m_btnSelectAll.SetWindowTextW(xml.str(IDS_STRING5708));
				m_btnDeSelectAll.SetWindowTextW(xml.str(IDS_STRING5709));

				m_btnSelectAll.ShowWindow((m_nOpenAs == 0 ? SW_NORMAL : SW_HIDE));
				m_btnDeSelectAll.ShowWindow((m_nOpenAs == 0 ? SW_NORMAL : SW_HIDE));

				if (m_nOpenAs == 0)
					m_lbl17_1.SetWindowTextW(xml.str(IDS_STRING57010) + xml.str(IDS_STRING57011));
				else if (m_nOpenAs == 1)
					m_lbl17_1.SetWindowTextW(xml.str(IDS_STRING57012) + xml.str(IDS_STRING57013));

				m_lbl17_2.SetWindowTextW(xml.str(IDS_STRING5702));
				m_lbl17_3.SetWindowTextW(xml.str(IDS_STRING5703));
				m_lbl17_4.SetWindowTextW(xml.str(IDS_STRING5704));
				m_check17_1.SetWindowTextW(xml.str(IDS_STRING5705));
				m_check17_1.SetCheck(TRUE);

				tokenizeString(xml.str(IDS_STRING204),';',m_sarrTableTypes);
				tokenizeString(xml.str(IDS_STRING205),';',m_sarrMeasuringMode);

				m_btnOK.EnableWindow(false);

				m_list17_1.SetExtendedStyle(m_list17_1.GetStyle()|LVS_EX_CHECKBOXES|LVS_EX_GRIDLINES);
				m_list17_1.InsertColumn(0,xml.str(IDS_STRING5706),LVCFMT_LEFT,40);
				m_list17_1.InsertColumn(1,xml.str(IDS_STRING5707),LVCFMT_LEFT,220);

			}

			if (m_list17_1.GetItemCount() > 0)
				m_list17_1.DeleteAllItems();
			if (m_sarrSheets.GetCount() > 0)
			{
				for (short i = 0;i < m_sarrSheets.GetCount();i++)
				{
					InsertRow(m_list17_1,i,true,2,L"",m_sarrSheets.GetAt(i));
					m_list17_1.SetCheck(i,false);
				
				}
			}


			// We'll add TableTypes to Combobox
			for (int i = 0;i < m_sarrTableTypes.GetCount();i++)
			{
				m_cbx17_2.AddString(m_sarrTableTypes.GetAt(i));
			}

			// We'll add MeasuringMode to Combobox
			for (int i = 0;i < m_sarrMeasuringMode.GetCount();i++)
			{
				m_cbx17_3.AddString(m_sarrMeasuringMode.GetAt(i));
			}
		}
		m_bInitialized = TRUE;
	}

	return TRUE;
}

// CSelectExcelSheetDlg message handlers

void CSelectExcelSheetDlg::OnCbnSelchangeCombo171()
{
	//m_nSelectedIndexSheets = m_cbx17_1.GetCurSel();
	//m_btnOK.EnableWindow((m_nSelectedIndexSheets > -1) && (m_nSelectedIndexTableType > -1) && (m_nSelectedIndexMeasMode > -1));
}

void CSelectExcelSheetDlg::OnCbnSelchangeCombo172()
{
	m_nSelectedIndexTableType = m_cbx17_2.GetCurSel();
	m_btnOK.EnableWindow((m_bSelectedIndexSheets) && (m_nSelectedIndexTableType > -1) && (m_nSelectedIndexMeasMode > -1));
}

void CSelectExcelSheetDlg::OnCbnSelchangeCombo173()
{
	m_nSelectedIndexMeasMode = m_cbx17_3.GetCurSel();
	m_btnOK.EnableWindow((m_bSelectedIndexSheets) && (m_nSelectedIndexTableType > -1) && (m_nSelectedIndexMeasMode > -1));
}

void CSelectExcelSheetDlg::OnLvnItemchangedList171(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;

	if (m_nOpenAs == 1)
	{
		if (m_nLastCheckedItem > -1)
		{
			m_list17_1.SetCheck(m_nLastCheckedItem,FALSE);
			m_nLastCheckedItem = -1;
		}
				
	}
	if (m_list17_1.GetItemCount() > 0)
	{
		for (int i = 0;i < m_list17_1.GetItemCount();i++)
		{
			if (m_list17_1.GetCheck(i))
			{
				m_bSelectedIndexSheets = true;
				m_nLastCheckedItem = i;
				break;
			}	// if (m_list17_1.GetCheck(i))
		}	// for (int i = 0;i < m_list10_1.GetItemCount();i++)
	}	// if (m_list17_1.GetItemCount() > 0)
	m_btnOK.EnableWindow((m_bSelectedIndexSheets) && (m_nSelectedIndexTableType > -1) && (m_nSelectedIndexMeasMode > -1));

}

void CSelectExcelSheetDlg::OnBnClickedOk()
{
	m_vecSheetsSelected.clear();
	if (m_list17_1.GetItemCount() > 0)
	{
		for (int i = 0;i < m_list17_1.GetItemCount();i++)
		{
			if (m_list17_1.GetCheck(i))
			{
				m_vecSheetsSelected.push_back(i);
			}	// if (m_list17_1.GetCheck(i))
		}	// for (int i = 0;i < m_list10_1.GetItemCount();i++)
	}	// if (m_list17_1.GetItemCount() > 0)

	OnOK();
}

void CSelectExcelSheetDlg::OnBnClickedButton171()
{
	if (m_list17_1.GetItemCount() > 0)
	{
		for (int i = 0;i < m_list17_1.GetItemCount();i++)
		{
			m_list17_1.SetCheck(i,TRUE);
		}	// for (int i = 0;i < m_list10_1.GetItemCount();i++)
	}	// if (m_list17_1.GetItemCount() > 0)
}

void CSelectExcelSheetDlg::OnBnClickedButton172()
{
	if (m_list17_1.GetItemCount() > 0)
	{
		for (int i = 0;i < m_list17_1.GetItemCount();i++)
		{
			m_list17_1.SetCheck(i,FALSE);
		}	// for (int i = 0;i < m_list10_1.GetItemCount();i++)
	}	// if (m_list17_1.GetItemCount() > 0)
}

void CSelectExcelSheetDlg::OnLvnItemchangingList171(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: Add your control notification handler code here
	*pResult = 0;
}
