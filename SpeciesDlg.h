#pragma once

#include "dbhandling.h"

#include "Resource.h"

///////////////////////////////////////////////////////////////////////////////////////////
// CSpeciesDlgDoc

class CSpeciesDlgDoc : public CDocument
{
protected: // create from serialization only
	CSpeciesDlgDoc();
	DECLARE_DYNCREATE(CSpeciesDlgDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSpeciesDlgDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSpeciesDlgDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CSpeciesDlgDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
/////////////////////////////////////////////////////////////////////////////
// class CSpeciesDlgFrame

class CSpeciesDlgFrame : public CChildFrameBase
{
//private:

	DECLARE_DYNCREATE(CSpeciesDlgFrame)
	CXTPToolBar m_wndToolBar;
	CXTPDockingPaneManager m_paneManager;
	CString m_sLangFN;

	BOOL m_bFirstOpen;
	BOOL m_bInitReports;
	BOOL m_bIsPrintOutTBtn;

	BOOL m_bSysCommand;

	void setNavBarButtons()
	{
		// Send messages to HMSShell, disable buttons on toolbar; 120122 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

		// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 120122 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
	}
protected:
	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	HICON m_hIcon;
public:
	CSpeciesDlgFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

// Attributes

// Operations

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	virtual ~CSpeciesDlgFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	
	//{{AFX_MSG(CSpeciesDlgFrame)
	afx_msg int OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl);
	afx_msg void OnSysCommand(UINT nID,LPARAM lParam);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnClose();
	afx_msg void OnDestroy(void);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);

	//}}AFX_MSG
	
	DECLARE_MESSAGE_MAP()
};

// CSpeciesDlgView dialog

class CSpeciesDlgView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CSpeciesDlgView)

	// Data members
	BOOL m_bInitialized;
	BOOL m_bOkToSave;
	CString m_sLangFN;
	CDBHandling *m_pDB;
	CVecSpecies m_vecSpecies;

	CString m_sMsgCap;
	CString m_sMsgDelete1;
	CString m_sMsgDelete2;
	CString m_sMsgSpecies1;
	CString m_sMsgSpecies2;

	CString m_sMsgNoDelete;

	CString m_sMsgPosetiveValue;

	BOOL m_bEnableAdd;
	BOOL m_bEnableDelete;
	BOOL m_bEnableSave;

	CVecPricelist m_vecVecPricelist;
	CVecUserVolTables m_vecUserVolTables;

	CMyReportControl m_repSpecies;
	DB_CONNECTION_DATA m_dbConnectionData;
	
	// Methods

	void setupReport();

	void populateReport();

	void Add();
	void Delete();
	BOOL Save(BOOL populate,BOOL do_check);

	BOOL Check();

	BOOL isSpcCodeUnique(LPCTSTR spc_code);

	BOOL CheckDuplicate(LPCTSTR spc_code,int row);
public:
	CSpeciesDlgView(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSpeciesDlgView();

// Dialog Data
	enum { IDD = IDD_FORMVIEW12 };

	BOOL doSave(BOOL populate);
protected:
	//{{AFX_VIRTUAL(CSpecisDlg)
//	virtual BOOL OnInitDialog();
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CRegisterDlg)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy(void);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnReportValueChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/);
	afx_msg void OnReportItemClick(NMHDR*  pNotifyStruct, LRESULT* /*result*/);

	afx_msg void OnCommand(UINT nID);
	afx_msg void OnUpdateToolbar(CCmdUI* pCmdUI);

	//}}AFX_MSG


	DECLARE_MESSAGE_MAP()
};
