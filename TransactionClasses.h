#if !defined(__TRANSACTIONCLASSES_H__)
#define __TRANSACTIONCLASSES_H__
#pragma once

#include <vector>


//////////////////////////////////////////////////////////////////////////////////////////
// CTickets
class CTickets
{
//private:
	int m_pkLoadID;
	CString m_sSourceDate;
	CString m_sSourceID;
	CString m_sScaler;
	CString m_sLocation;
	CString m_sBuyer;
	CString m_sOtherInfo;
	CString m_sLoadID;
	CString m_sTicket;
	CString m_sTractID;
	CString m_sHauler;
	CString m_sSupplier;
	CString m_sVendor;
	CString m_sLoadType;
	CString m_sDate;
	double m_fWeight;
	int m_nNumOfLogs;
	CString m_sNotes;
	int m_nSawlogVolumeUnit;
	int m_nPulpwoodVolumeUnit;
	int m_nRoundScaling;
	int m_nUseSouthernDoyle;
	CString m_sDeductionType;
	double m_fIBTaper;
	double m_fBarkRatio;
	int m_nMeasuringMode;
	double m_fVolWeight;
	double m_fSumVolPricelist;	// Volym f�r funktion i prislistan
	double m_fSumPrice;					//Summa pris fr�n prislistan
	//---------------------------------------------------------
	CString m_sFileVersion;
	CString m_sTemplateUsed;
	CString m_sPricelistUsed;
	int m_nPricelistID;
	CString m_sGISCoord1;
	CString m_sGISCoord2;
	int m_nJAS;
	double m_fTrimFT;
	double m_fTrimCM;
	int m_nTemplateID;	// aka Contract
	int m_nLocked;
	CString m_sCreated;
	CString m_sUpdated;
public:
	CTickets()
	{
		m_pkLoadID = -1;
		m_sSourceDate = L"";
		m_sSourceID = L"";
		m_sScaler = L"";
		m_sLocation = L"";
		m_sBuyer = L"";
		m_sOtherInfo = L"";
		m_sLoadID = L"";
		m_sTicket = L"";
		m_sTractID = L"";
		m_sHauler = L"";
		m_sSupplier = L"";
		m_sVendor = L"";
		m_sLoadType = L"";
		m_sDate = L"";
		m_fWeight = 0.0;
		m_nNumOfLogs = 0;
		m_sNotes = L"";
		m_nSawlogVolumeUnit = -1;
		m_nPulpwoodVolumeUnit = -1;
		m_nRoundScaling = -1;
		m_nUseSouthernDoyle = -1;
		m_sDeductionType = L"";
		m_fIBTaper = 0.0;
		m_fBarkRatio = 0.0;
		m_nMeasuringMode = -1;
		m_fVolWeight = 0.0;
		m_fSumVolPricelist = 0.0;
		m_fSumPrice = 0.0;
		//---------------------------------------------------------
		m_sFileVersion = L"";
		m_sTemplateUsed = L"";
		m_sPricelistUsed = L"";
		m_nPricelistID = -1;
		m_sGISCoord1 = L"";
		m_sGISCoord2 = L"";
		m_nJAS = -1;
		m_fTrimFT = 0.0;
		m_fTrimCM = 0.0;
		m_nTemplateID = 0;
		m_nLocked = 0;	// 0 = Not locked, 1 = Locked
		m_sCreated = L"";
		m_sUpdated = L"";
	}

	CTickets(int pk,LPCTSTR source_date,LPCTSTR source_id,LPCTSTR scaler,LPCTSTR location,LPCTSTR buyer,LPCTSTR other_info,LPCTSTR load_id,
					 LPCTSTR ticket,LPCTSTR tract_id,LPCTSTR hauler,LPCTSTR supplier,LPCTSTR vendor,LPCTSTR load_type,LPCTSTR date,double weight,int num_of_logs,LPCTSTR notes,
					 int sawlog_vol_unit,int pulp_vol_unit,int round_scaling,int use_southern_doyle,LPCTSTR deduction_type,
					 double ib_taper,double bark_ratio,int measuring_mode,double vol_weight,
					 double vol_price,double price,LPCTSTR file_version,LPCTSTR tmpl_used,LPCTSTR pricelist_used,int pricelist_id,
					 LPCTSTR gis_coord1,LPCTSTR gis_coord2,int jas,double trim_ft,double trim_cm,int tmpl_id,int locked,LPCTSTR created = L"",LPCTSTR updated = L"")
	{
		m_pkLoadID = pk;
		m_sSourceDate = source_date;
		m_sSourceID = source_id;
		m_sScaler = scaler;
		m_sLocation = location;
		m_sBuyer = buyer;
		m_sOtherInfo = other_info;
		m_sLoadID = load_id;
		m_sTicket = ticket;
		m_sTractID = tract_id;
		m_sHauler = hauler;
		m_sSupplier = supplier;
		m_sVendor = vendor;
		m_sLoadType = load_type;
		m_sDate = date;
		m_fWeight = weight;
		m_nNumOfLogs = num_of_logs;
		m_sNotes = notes;
		m_nSawlogVolumeUnit = sawlog_vol_unit;
		m_nPulpwoodVolumeUnit = pulp_vol_unit;
		m_nRoundScaling = round_scaling;
		m_nUseSouthernDoyle = use_southern_doyle;
		m_sDeductionType = deduction_type;
		m_fIBTaper = ib_taper;
		m_fBarkRatio = bark_ratio;
		m_nMeasuringMode = measuring_mode;
		m_fVolWeight = vol_weight;
		m_fSumVolPricelist = vol_price;
		m_fSumPrice = price;
		//---------------------------------------------------------
		m_sFileVersion = file_version;
		m_sTemplateUsed = tmpl_used;
		m_sPricelistUsed = pricelist_used;
		m_nPricelistID = pricelist_id;
		m_sGISCoord1 = gis_coord1;
		m_sGISCoord2 = gis_coord2;
		m_nJAS = jas;
		m_fTrimFT = trim_ft;
		m_fTrimCM = trim_cm;
		m_nTemplateID = tmpl_id;
		m_nLocked = locked;
		m_sCreated = created;
		m_sUpdated = updated;
	}
	CTickets(const CTickets& c)
	{
		*this = c;
	}

	// GET
	int getPKID() const { return m_pkLoadID; }
	CString getSourceDate() const { return m_sSourceDate; }
	CString getSourceID() const { return m_sSourceID; }
	CString getScaler() const { return m_sScaler; }
	CString getLocation() const { return m_sLocation; }
	CString getBuyer() const { return m_sBuyer; }
	CString getOtherInfo() const { return m_sOtherInfo; }
	CString getLoadID() const { return m_sLoadID; }
	CString getTicket() const { return m_sTicket; }
	CString getTractID() const { return m_sTractID; }
	CString getHauler() const { return m_sHauler; }
	CString getSupplier() const { return m_sSupplier; }
	CString getVendor() const { return m_sVendor; }
	CString getLoadType() const { return m_sLoadType; }
	CString getDate() const { return m_sDate; }
	double getWeight() const { return m_fWeight; }
	int getNumOfLogs() const { return m_nNumOfLogs; }
	CString getNotes() const { return m_sNotes; }
	int getSawlogVolUnit() const { return m_nSawlogVolumeUnit; }
	int getPulpVolUnit() const { return m_nPulpwoodVolumeUnit; }
	int getRoundScaling() const { return m_nRoundScaling; }
	int getUseSouthernDoyle() const { return m_nUseSouthernDoyle; }
	CString getDeductionType() const { return m_sDeductionType; }
	double getIBTaper() const { return m_fIBTaper; }
	double getBarkRation() const { return m_fBarkRatio; }
	int getMeasuringMode() const { return m_nMeasuringMode; }
	double getVolWeight() const { return m_fVolWeight; }
	double getSumVolPricelist() const { return m_fSumVolPricelist; }
	double getSumPrice() const { return m_fSumPrice; }
	//---------------------------------------------------------
	CString getFileVersion() const { return m_sFileVersion; }
	CString getTmplUsed() const { return m_sTemplateUsed; }
	CString getPrlUsed() const { return m_sPricelistUsed; }
	int getPrlID() const { return m_nPricelistID; }
	CString getGISCoord1() const { return m_sGISCoord1; }
	CString getGISCoord2() const { return m_sGISCoord2; }
	int getJAS() const { return m_nJAS; }
	double getTrimFT() const { return m_fTrimFT; }
	double getTrimCM() const { return m_fTrimCM; }
	int getTemplateID() const { return m_nTemplateID; }
	int getLocked() const { return m_nLocked; }
  CString getCreated() const { return m_sCreated; }
	CString getUpdated() const { return m_sUpdated; }

	// SET
	void setPKID(int v) { m_pkLoadID = v; }
	void setSourceDate(LPCTSTR v) {  m_sSourceDate = v; }
	void setSourceID(LPCTSTR v) 	{  m_sSourceID = v; }
	void setScaler(LPCTSTR v) 	{  m_sScaler = v; }
	void setLocation(LPCTSTR v) 	{  m_sLocation = v; }
	void setBuyer(LPCTSTR v) 	{  m_sBuyer = v; }
	void setOtherInfo(LPCTSTR v) 	{  m_sOtherInfo = v; }
	void setLoadID(LPCTSTR v) 	{  m_sLoadID = v; }
	void setTicket(LPCTSTR v) 	{  m_sTicket = v; }
	void setTractID(LPCTSTR v) {  m_sTractID = v; }
	void setHauler(LPCTSTR v) 	{  m_sHauler = v; }
	void setSupplier(LPCTSTR v) { m_sSupplier = v; }
	void setVendor(LPCTSTR v) { m_sVendor = v; }
	void setLoadType(LPCTSTR v) {  m_sLoadType = v; }
	void setDate(LPCTSTR v) 	{  m_sDate = v; }
	void setWeight(double v) {  m_fWeight = v; }
	void setNumOfLogs(int v) 	{  m_nNumOfLogs = v; }
	void setNotes(LPCTSTR v) 	{  m_sNotes = v; }
	void setSawlogVolUnit(int v) 	{  m_nSawlogVolumeUnit = v; }
	void setPulpVolUnit(int v) 	{  m_nPulpwoodVolumeUnit = v; }
	void setRoundScaling(int v) 	{  m_nRoundScaling = v; }
	void setUseSouthernDoyle(int v) 	{  m_nUseSouthernDoyle = v; }
	void setDeductionType(LPCTSTR v) 	{  m_sDeductionType = v; }
	void setIBTaper(double v)	{ m_fIBTaper = v; }
	void setBarkRation(double v) 	{  m_fBarkRatio = v; }
	void setMeasuringMode(int v) 	{  m_nMeasuringMode = v; }
	void setVolWeight(double v)	{ m_fVolWeight = v; }
	void setSumVolPricelist(double v) { m_fSumVolPricelist = v; }
	void setSumPrice(double v)	{ m_fSumPrice = v; }
	void setTmplUsed(LPCTSTR v) { m_sTemplateUsed = v; }
	void setPrlUsed(LPCTSTR v) { m_sPricelistUsed = v; }
	void setPrlID(int v) { m_nPricelistID = v; }
	void setGISCoord1(LPCTSTR v) { m_sGISCoord1 = v; }
	void setGISCoord2(LPCTSTR v) { m_sGISCoord2 = v; }
	void setJAS(int v) { m_nJAS = v; }
	void setTrimFT(double v) { m_fTrimFT = v; }
	void setTrimCM(double v) { m_fTrimCM = v; }
	void setTemplateID(int v) { m_nTemplateID = v; }
	void setLocked(int v) { m_nLocked = v; }

};

//////////////////////////////////////////////////////////////////////////////////////////
// CRegister
class CRegister
{
//private:
	int pkID;								// Primary key
	int nTypeID;						// ID f�r typ (ex. Byer,Vendor,Location etc. )
	CString sName;					// Namn p� typ (ex Name of byer or vendor etc.)
	CString sAbbrevation;		// F�rkortning, att anv�nda i klave
	CString sOtherInfo;			// 15 i klave
	CString sID;						// 
	/*************************************************/
	CString sAddress;				// Adress
	CString sAddress2;			// Adress2
	CString sZipCode;				// ZipCode
	CString sCity;					// Stad
	CString sPhone;					// Telefonnummer
	CString sCellPhone;			// Mobilnummer
	CString sWWW;						// Hemsida
	/*************************************************/
	CString dCreated;				// created,
	CString dUpdated;				// last updated
public:
	CRegister()
	{
		pkID = -1;			
		nTypeID = -1;				
		sName = L"";					
		sAbbrevation = L"";		
		sID = L""; 
		sOtherInfo = L"";			
		sAddress = L"";				
		sAddress2 = L"";			
		sZipCode = L"";				
		sCity = L"";					
		sPhone = L"";					
		sCellPhone = L"";			
		sWWW = L"";						
		dCreated = L"";				
		dUpdated = L"";				
	}
	CRegister(int id,int type_id,LPCTSTR name,LPCTSTR abbrev,LPCTSTR id_name,LPCTSTR other_info,LPCTSTR address,LPCTSTR address2,
						LPCTSTR zipcode,LPCTSTR city,LPCTSTR phone,LPCTSTR cell,LPCTSTR www,LPCTSTR created = L"",LPCTSTR updated = L"")
	{
		pkID = id;			
		nTypeID = type_id;				
		sName = name;					
		sAbbrevation = abbrev;		
		sID = id_name; 
		sOtherInfo = other_info;			
		sAddress = address;				
		sAddress2 = address2;			
		sZipCode = zipcode;				
		sCity = city;					
		sPhone = phone;					
		sCellPhone = cell;			
		sWWW = www;						
		dCreated = created;				
		dUpdated = updated;				
	}
	CRegister(const CRegister& c)
	{
		*this = c;
	}
	// GET
	int getPKID() const { return pkID; }
	int getTypeID() const { return nTypeID;	}	
	CString getName() const { return sName;	}				
	CString getAbbrevation() const { return sAbbrevation; }
	CString getID() const { return sID; }
	CString getOtherInfo() const { return sOtherInfo; }	
	CString getAddress() const { return sAddress; }		
	CString getAddress2() const { return sAddress2;	}		
	CString getZipCode() const { return sZipCode; }			
	CString getCity() const { return sCity; }		
	CString getPhone() const { return sPhone; }
	CString getCellPhone() const { return sCellPhone; }
	CString getWWW() const { return sWWW; }		
	CString getCreated() const { return dCreated; }
	CString getUpdated() const { return dUpdated; }			
	// SET
	void setTypeID(int v) { nTypeID = v; }	
	void setName(LPCTSTR v) { sName = v; }				
	void setAbbrevation(LPCTSTR v) { sAbbrevation = v; }
	void setID(LPCTSTR v) { sID = v; }
	void setOtherInfo(LPCTSTR v) { sOtherInfo = v; }	
	void setAddress(LPCTSTR v) { sAddress = v; }		
	void setAddress2(LPCTSTR v) { sAddress2 = v;	}		
	void setZipCode(LPCTSTR v) { sZipCode = v; }			
	void setCity(LPCTSTR v) { sCity = v; }		
	void setPhone(LPCTSTR v) { sPhone = v; }
	void setCellPhone(LPCTSTR v) { sCellPhone = v; }
	void setWWW(LPCTSTR v) { sWWW = v; }		
};

//////////////////////////////////////////////////////////////////////////////////////////
// CSpecies
class CSpecies
{
//private:
	int pkSpeciesID;			// Primary key
	CString sSpeciesCode; // Tr�dslagskod
	CString sSpeciesName;	// Namn p� tr�dslag
	double fBarkReductionInch;	// Barkavdrag/Tr�dslag
	double fBarkReductionMM;	// Barkavdrag/Tr�dslag
	CString dCreated;			// created,
	CString dUpdated;			// last updated
public:
	CSpecies()
	{
		pkSpeciesID = -1;	
		sSpeciesCode = L"";	
		sSpeciesName = L"";
		fBarkReductionInch = 0.0;
		fBarkReductionMM = 0.0;
		dCreated = L"";
		dUpdated = L"";
	}
	CSpecies(int id,LPCTSTR code,LPCTSTR name,double bark_reduction_inch,double bark_reduction_mm,LPCTSTR created = L"",LPCTSTR updated = L"")
	{
		pkSpeciesID = id;	
		sSpeciesCode = code;	
		sSpeciesName = name;
		fBarkReductionInch = bark_reduction_inch;
		fBarkReductionMM = bark_reduction_mm;
		dCreated = created;
		dUpdated = updated;
	}
	CSpecies(const CSpecies& c)
	{
		*this = c;
	}
	// GET
	int getSpcID() const { return pkSpeciesID; }
	CString getSpcCode() const { return sSpeciesCode; }	
	CString getSpcName() const { return sSpeciesName; }
	double getBarkReductionInch() const { return fBarkReductionInch; }
	double getBarkReductionMM() const { return fBarkReductionMM; }
	CString getCreated() const { return dCreated; }
	CString getUpdated() const { return dUpdated; }			
	// SET
	void setSpcID(int v) { pkSpeciesID = v; }
	void setSpcCode(LPCTSTR v) { sSpeciesCode = v; }	
	void setSpcName(LPCTSTR v) { sSpeciesName = v; }
	void setBarkReductionInch(double v) { fBarkReductionInch = v; }
	void setBarkReductionMM(double v) { fBarkReductionMM = v; }
};

//////////////////////////////////////////////////////////////////////////////////////////
// CGrades
class CGrades
{
//private:
	int pkGradesID;			// Primary key
	CString sGradesCode;	// Kvalitetsskod
	CString sGradesName;	// Namn p� kvalitet
	int nPulpwood;				// 1 = pulpwood
	CString dCreated;			// created,
	CString dUpdated;			// last updated
public:
	CGrades()
	{
		pkGradesID = -1;	
		sGradesCode = L"";	
		sGradesName = L"";
		nPulpwood = -1;
		dCreated = L"";
		dUpdated = L"";
	}
	CGrades(int id,LPCTSTR code,LPCTSTR name,int pulp_wood,LPCTSTR created = L"",LPCTSTR updated = L"")
	{
		pkGradesID = id;	
		sGradesCode = code;	
		sGradesName = name;
		nPulpwood = pulp_wood;
		dCreated = created;
		dUpdated = updated;
	}
	CGrades(const CGrades& c)
	{
		*this = c;
	}
	// GET
	int getGradesID() const { return pkGradesID; }
	CString getGradesCode() const { return sGradesCode; }	
	CString getGradesName() const { return sGradesName; }
	int getIsPulpwood() const { return nPulpwood; }
	CString getCreated() const { return dCreated; }
	CString getUpdated() const { return dUpdated; }			
	// SET
	void setGradesID(int v) { pkGradesID = v; }
	void setGradesCode(LPCTSTR v ) { sGradesCode = v; }	
	void setGradesName(LPCTSTR v) { sGradesName = v; }
	void setIsPulpwood(int v) { nPulpwood = v; }
};

//////////////////////////////////////////////////////////////////////////////////////////
// CGradesExt
class CGradesExt
{
//private:
	int pkGradesID;			// Primary key
	CString sSpcCode;			// Tr�dslagsskod
	CString sGradesCode;	// Kvalitetsskod
	CString sGradesName;	// Namn p� kvalitet
	int nPulpwood;				// 1 = pulpwood
	int nFuncID;
	double fPrice;
public:
	CGradesExt()
	{
		pkGradesID = -1;	
		sSpcCode = L"";
		sGradesCode = L"";	
		sGradesName = L"";
		nPulpwood = -1;				// 1 = pulpwood
		nFuncID = -1;
		fPrice = 0.0;
	}
	CGradesExt(int id,LPCTSTR spc_code,LPCTSTR grd_code,LPCTSTR name,int pulp_wood,int func_id,double price)
	{
		pkGradesID = id;	
		sSpcCode = spc_code;
		sGradesCode = grd_code;	
		sGradesName = name;
		nPulpwood = pulp_wood;
		nFuncID = func_id;
		fPrice = price;
	}
	CGradesExt(const CGrades& c)
	{
		*this = c;
	}
	// GET
	int getGradesID() const { return pkGradesID; }
	CString getSpcCode() const { return sSpcCode; }	
	CString getGradesCode() const { return sGradesCode; }	
	CString getGradesName() const { return sGradesName; }
	int getIsPulpwood() const { return nPulpwood; }
	int getFuncID() const { return nFuncID; }
	double getPrice() const { return fPrice; }
	// SET
	void setGradesID(int v) { pkGradesID = v; }
	void setSpcCode(LPCTSTR v ) { sSpcCode = v; }	
	void setGradesCode(LPCTSTR v ) { sGradesCode = v; }	
	void setGradesName(LPCTSTR v) { sGradesName = v; }
	void setIsPulpwood(int v) { nPulpwood = v; }
	void setFuncID(int v) { nFuncID = v; }
	void setPrice(double v) { fPrice = v; }
};


//////////////////////////////////////////////////////////////////////////////////////////
// CSpeciesGradesAndPrice
class CSpeciesGradesAndPrice
{
//private:
	int nSpeciesID;
	int nGradesID;	
	double fPrice;
public:
	CSpeciesGradesAndPrice()
	{
		nSpeciesID = -1;
		nGradesID = -1; 	
		fPrice = 0.0;
	}
	CSpeciesGradesAndPrice(int spc_id,int grade_id,double price)
	{
		nSpeciesID = spc_id;
		nGradesID = grade_id; 	
		fPrice = price;
	}
	CSpeciesGradesAndPrice(const CSpeciesGradesAndPrice& c)
	{
		*this = c;
	}
	// GET
	int getSpeciesID() const { return nSpeciesID; }
	int getGradesID() const { return nGradesID; }
	double getPrice() const { return fPrice; }
	// SET
	void setSpeciesID(int v) { nSpeciesID = v; }
	void setGradesID(int v) { nGradesID = v; }
	void setPrice(double v) { fPrice = v; }
};

//////////////////////////////////////////////////////////////////////////////////////////
// CPricelist
class CPricelist
{
//private:
	int pkPrlID;		// Primary key
	CString sName;
	CString sCreatedBy;
	CString sDate;
	CString sPricelist;
	int nLocked;					// 1 = Locked, 0 = Unlocked
	CString sNotes;
	CString dCreated;			// created,
	CString dUpdated;			// last updated
public:
	CPricelist()
	{
		pkPrlID = -1;
		sName = L"";
		sCreatedBy = L"";
		sDate = L"";
		sPricelist = L"";
		nLocked = 0;	// Default = Unlocked
		sNotes = L"";
		dCreated = L"";
		dUpdated = L"";
	}
	CPricelist(int prl_id,LPCTSTR name,LPCTSTR created_by,LPCTSTR date,LPCTSTR pricelist,int locked,LPCTSTR notes,LPCTSTR created = L"",LPCTSTR updated = L"")
	{
		pkPrlID = prl_id;
		sName = name;
		sCreatedBy = created_by;
		sDate = date;
		sPricelist = pricelist;
		nLocked = locked;
		sNotes = notes;
		dCreated = created;
		dUpdated = updated;
	}
	CPricelist(const CPricelist& c)
	{
		*this = c;
	}
	// GET
	int getPrlID() const { return pkPrlID; }
	CString getName() const { return sName; }
	CString getCreatedBy() const { return sCreatedBy; }
	CString getDate() const { return sDate; }
	CString getPricelist() const { return sPricelist; }
	int getLocked() const { return nLocked; }
	CString getNotes() const { return sNotes; }
	CString getCreated() const { return dCreated; }
	CString getUpdated() const { return dUpdated; }			
	// SET
	void setPrlID(int v) { pkPrlID = v; }
	void setName(LPCTSTR v) { sName = v; }
	void setCreatedBy(LPCTSTR v) { sCreatedBy = v; }
	void setDate(LPCTSTR v) { sDate = v; }
	void setPricelist(LPCTSTR v) { sPricelist = v; }
	void setLocked(int v) { nLocked = v; }
	void setNotes(LPCTSTR v) { sNotes = v; }
};


//////////////////////////////////////////////////////////////////////////////////////////
// CTicketSpc
class CTicketSpc
{
//private:
	int fkTicketID;			// Primary key
	int fkSpeciesID;			// Primary key
	CString sSpeciesCode;	
	CString sSpeciesName;	
	double fBarkReduction;
public:
	CTicketSpc()
	{
		fkTicketID = -1;			// Primary key
		fkSpeciesID = -1;			// Primary key
		sSpeciesCode = L"";	
		sSpeciesName = L"";	
		fBarkReduction = 0.0;
	}
	CTicketSpc(int ticket_id,int species_id,LPCTSTR spc_code,LPCTSTR spc_name,double bark_reduction)
	{
		fkTicketID = ticket_id;			// Primary key
		fkSpeciesID = species_id;			// Primary key
		sSpeciesCode = spc_code;	
		sSpeciesName = spc_name;	
		fBarkReduction = bark_reduction;
	}
	CTicketSpc(const CTicketSpc& c)
	{
		*this = c;
	}
	// GET
	int getTicketID() const { return fkTicketID; }
	int getSpeciesID() const { return fkSpeciesID; }
	CString getSpeciesCode() const { return sSpeciesCode; }
	CString getSpeciesName() const { return sSpeciesName; }
	double getBarkReduction() const { return fBarkReduction; }
	// SET
	void setSpeciesCode(LPCTSTR v) { sSpeciesCode = v; }
	void setSpeciesName(LPCTSTR v) { sSpeciesName = v; }
	void setBarkReduction(double v) { fBarkReduction = v; }
};


//////////////////////////////////////////////////////////////////////////////////////////
// CTicketPrl
class CTicketPrl
{
//private:
	int fkTicketID;				// Primary key
	int fkSpeciesID;			// Primary key
	int fkGradeID;				// Primary key
	CString sSpeciesCode;	
	CString sGradesCode;	
	double fPrice;
	int nFuncID;
	CString sCalcType;
	int nPulpwood;				// 1 = pulpwood
public:
	CTicketPrl()
	{
		fkTicketID = -1;
		fkSpeciesID = -1;	
		fkGradeID = -1;
		sSpeciesCode = L"";	
		sGradesCode = L"";	
		fPrice = 0.0;
		nFuncID = -1;
		sCalcType = L"";
		nPulpwood = -1;
	}
	CTicketPrl(int ticket_id,int spc_id,int grade_id,LPCTSTR spc_code,LPCTSTR grade_code,double price,int func_id,LPCTSTR calc_type ,int pulp_wood)
	{
		fkTicketID = ticket_id;
		fkSpeciesID = spc_id;	
		fkGradeID = grade_id;	
		sSpeciesCode = spc_code;	
		sGradesCode = grade_code;	
		fPrice = price;
		nFuncID = func_id;
		sCalcType = calc_type;
		nPulpwood = pulp_wood;
	}
	CTicketPrl(const CTicketPrl& c)
	{
		*this = c;
	}
	// GET
	int getTicketID() const { return fkTicketID; }
	int getSpeciesID() const { return fkSpeciesID; }
	int getGradesID() const { return fkGradeID; }
	CString getSpeciesCode() const { return sSpeciesCode; }
	CString getGradesCode() const { return sGradesCode; }
	double getPrice() const { return fPrice; }
	int getFuncID() const { return nFuncID; }
	CString getCalcType() const { return sCalcType; }
	int getIsPulpwood() const { return nPulpwood; }
	// SET
//	void setTicketID(int v) { fkTicketID = v; }
//	void setSpeciesID(int v) { fkSpeciesID = v; }
//	void setGradesID(int v) { fkGradesID = v; }
	void setSpeciesCode(LPCTSTR v) { sSpeciesCode = v; }
	void setGradesCode(LPCTSTR v) { sGradesCode = v; }
	void setPrice(double v) { fPrice = v; }
	void setFuncID(int v) { nFuncID = v; }
	void setCalcType(LPCTSTR v) { sCalcType = v; }
	void setIsPulpwood(int v) { nPulpwood = v; }
};



//////////////////////////////////////////////////////////////////////////////////////////
// CPricelistInTicket
class CPricelistInTicket
{
//private:
	int nSpeciesID;		// Key
	int nGradesID;		// Key
	double fPrice;
	int nFuncID;
public:
	CPricelistInTicket()
	{
		nSpeciesID = -1;	
		nGradesID = -1;	
		fPrice = 0.0;
		nFuncID = -1;
	}
	CPricelistInTicket(int spc_id,int grade_id,double price,int func_id)
	{
		nSpeciesID = spc_id;	
		nGradesID = grade_id;	
		fPrice = price;
		nFuncID = func_id;
	}
	CPricelistInTicket(const CPricelistInTicket& c)
	{
		*this = c;
	}
	// GET
	int getSpeciesID() const { return nSpeciesID; }
	int getGradesID() const { return nGradesID; }
	double getPrice() const { return fPrice; }
	int getFuncID() const { return nFuncID; }
	// SET
	void setSpeciesID(int v) { nSpeciesID = v; }
	void setGradesID(int v) { nGradesID = v; }
	void setPrice(double v) { fPrice = v; }
	void setFuncID(int v) { nFuncID = v; }
};

//////////////////////////////////////////////////////////////////////////////////////////
// CCalcTypes
class CCalcTypes
{
//private:
	int pkCalcTypeID;		// Primary key
	CString sCalcType;	
	CString sCalcBasis;	
	int nOnlySW;
	CString dCreated;			// created,
	CString dUpdated;			// last updated
public:
	CCalcTypes()
	{
		pkCalcTypeID = -1;		// Primary key
		sCalcType = L"";	
		sCalcBasis = L"";	
		nOnlySW = -1;
		dCreated = L"";
		dUpdated = L"";
	}
	CCalcTypes(int id,LPCTSTR ctype,LPCTSTR cbasis,int only_sw,LPCTSTR created = L"",LPCTSTR updated = L"")
	{
		pkCalcTypeID = id;		// Primary key
		sCalcType = ctype;	
		sCalcBasis = cbasis;	
		nOnlySW = only_sw;
		dCreated = created;
		dUpdated = updated;
	}
	CCalcTypes(const CCalcTypes& c)
	{
		*this = c;
	}
	// GET
	int getCalcTypeID() const { return pkCalcTypeID; }
	CString getCalcType() const { return sCalcType; }
	CString getCalcBasis() const { return sCalcBasis; }
	int getOnlySW() const { return nOnlySW; }
	CString getCreated() const { return dCreated; }
	CString getUpdated() const { return dUpdated; }			
	// SET
	void getCalcType(LPCTSTR v) { sCalcType = v; }
	void getCalcBasis(LPCTSTR v) { sCalcBasis = v; }
	void getOnlySW(int v) { nOnlySW = v; }
};


//////////////////////////////////////////////////////////////////////////////////////////
// CSumLogData
class CSumLogData
{
//private:
	int pkID;
	int nNumOfLogs;
	double fVolPricelist;
	double fLogPrice;
	double fUVolume;
	double fUTopDia;
	double fURootDia;
	double fULength;
	double fUPrice;
public:
	CSumLogData()
	{
		pkID = -1;
		nNumOfLogs = -1;
		fVolPricelist = 0.0;
		fLogPrice = 0.0;
		fUVolume = 0.0;
		fUTopDia = 0.0;
		fURootDia = 0.0;
		fULength = 0.0;
		fUPrice = 0.0;
	}
	CSumLogData(int pkid,int num_of_logs,double vol_pricelist,double price,double uvol,double utop_dia,double uroot_dia,double ulength,double uprice)
	{
		pkID = pkid;
		nNumOfLogs = num_of_logs;
		fVolPricelist = vol_pricelist;
		fLogPrice = price;
		fUVolume = uvol;
		fUTopDia = utop_dia;
		fURootDia = uroot_dia;
		fULength = ulength;
		fUPrice = uprice;
	}

	// GET
	int getPKID()	const	{ return pkID; }
	int getNumOfLogs() const		{ return nNumOfLogs; }
	double getVolPricelist() const	{ return fVolPricelist; }
	double getLogPrice() const	{ return fLogPrice; }
	double getUVol() const	{ return fUVolume; }
	double getUTopDia()	const { return fUTopDia; }
	double getURootDia()	const { return fURootDia; }
	double getULength()	const { return fULength; }
	double getUPrice() const { return fUPrice; }
};

//////////////////////////////////////////////////////////////////////////////////////////
// CDefaults
class CDefaults
{
//private:
	int nDefValueID;	
	int nDefFuncID;	//#4258 ny variabel, nDefFuncID 
	CString sDefValue;	
	CString sDefBasisValue;	
	CString dCreated;			// created,
	CString dUpdated;			// last updated
public:
	CDefaults()
	{
		nDefValueID = -1;
		nDefFuncID = -1;
		sDefValue = L"";	
		sDefBasisValue = L"";	
		dCreated = L"";
		dUpdated = L"";
	}
	CDefaults(int def_value_id,LPCTSTR def_str,LPCTSTR def_basis_str,LPCTSTR created = L"",LPCTSTR updated = L"", int def_func_id = -1)
	{
		nDefValueID = def_value_id;
		nDefFuncID = def_func_id;
		sDefValue = def_str;	
		sDefBasisValue = def_basis_str;	
		dCreated = created;
		dUpdated = updated;
	}
	CDefaults(const CDefaults& c)
	{
		*this = c;
	}
	// GET
	int getDefvalueID() const { return nDefValueID; }
	int getDefFuncID() const { return nDefFuncID;}
	CString getDefStr() const { return sDefValue; }
	CString getDefBasisStr() const { return sDefBasisValue; }
	CString getCreated() const { return dCreated; }
	CString getUpdated() const { return dUpdated; }			
	// SET
	void setDefvalueID(int v) { nDefValueID = v; }
	void setDefFuncID(int v) { nDefFuncID = v;}
	void setDefStr(LPCTSTR v) { sDefValue = v; }
	void setDefBasisStr(LPCTSTR v) { sDefBasisValue = v; }
};

/* Moved to LogScaleVolTransactionClasses.h in \include directory
//////////////////////////////////////////////////////////////////////////////////////////
// CUserVolTables
class CUserVolTables
{
//private:
	int pkID;
	int nFuncID;
	CString sFullName;	
	CString sAbbrevName;	
	CString sBasisName;	
	CString sCreatedBy;
	CString sDate;
	int nTableType;
	int nMeasuringMode;		// Inch or Metric
	CString sNotes;				// Notes for volume-table
	CString sVolTable;		// Actual semicolon-seperated volume-table
	int nLocked;					// Locked = 1, Unlocked = 0
	CString dCreated;			// created,
	CString dUpdated;			// last updated
public:
	CUserVolTables()
	{
		pkID = -1;
		nFuncID = -1;
		sFullName = L"";	
		sAbbrevName = L"";	
		sBasisName = L"";	
		sCreatedBy = L"";
		sDate = L"";
		nTableType = -1;
		nMeasuringMode = -1;
		sNotes = L"";
		sVolTable = L"";
		nLocked = 0;				// Locked = 1, Unlocked = 0
		dCreated = L"";			// created,
		dUpdated = L"";			// last updated
	}
	CUserVolTables(int pk_id,int func_id,LPCTSTR full_name,LPCTSTR abbrev_name,LPCTSTR basis_name,
								 LPCTSTR created_by,LPCTSTR date,int table_type,int measure_mode,
								 LPCTSTR notes,LPCTSTR vol_table,int locked,LPCTSTR created = L"",LPCTSTR updated = L"")
	{
		pkID = pk_id;
		nFuncID = func_id;
		sFullName = full_name;	
		sAbbrevName = abbrev_name;	
		sBasisName = basis_name;	
		sCreatedBy = created_by;
		sDate = date;
		nTableType = table_type;
		nMeasuringMode = measure_mode;
		sNotes = notes;
		sVolTable = vol_table;
		nLocked = locked;
		dCreated = created;			// created,
		dUpdated = updated;			// last updated
	}
	CUserVolTables(const CUserVolTables& c)
	{
		*this = c;
	}
	// GET
	int getID() const { return pkID; }
	int getFuncID() const { return nFuncID; }
	CString getFullName() const { return sFullName; }
	CString getAbbrevName() const { return sAbbrevName; }
	CString getBasisName() const { return sBasisName; }
	CString getCreatedBy() const { return sCreatedBy; }
	CString getDate() const { return sDate; }
	int getTableType() const { return nTableType; }
	int getMeasuringMode() const { return nMeasuringMode; }
	CString getNotes() const { return sNotes; }
	CString getVolTable() const { return sVolTable; }
	int getLocked() const { return nLocked; }
	CString getCreated() const { return dCreated; }
	CString getUpdated() const { return dUpdated; }			
	// SET
	void setFuncID(int v) { nFuncID = v; }
	void setFullName(LPCTSTR v) { sFullName = v; }
	void setAbbrevName(LPCTSTR v) { sAbbrevName = v; }
	void setBasisName(LPCTSTR v) { sBasisName = v; }
	void setCreatedBy(LPCTSTR v) { sCreatedBy = v; }
	void setDate(LPCTSTR v) { sDate = v; }
	void setTableType(int v) { nTableType = v; }
	void setMeasuringMode(int v) { nMeasuringMode = v; }
	void setNotes(LPCTSTR v) { sNotes = v; }
	void setVolTable(LPCTSTR v) { sVolTable = v; }
	void setLocked(int v) { nLocked = v; }
};
*/
//////////////////////////////////////////////////////////////////////////////////////////
// CSelectedVolFuncs
class CSelectedVolFuncs
{
//private:
	int pkID;
	int fkTicketID;
	int nFuncID;
	CString sFullName;	
	CString sAbbrevName;	
	CString sBasisName;	
public:
	CSelectedVolFuncs()
	{
		pkID = -1;
		fkTicketID = -1;
		nFuncID = -1;
		sFullName = L"";	
		sAbbrevName = L"";	
		sBasisName = L"";	
	}
	CSelectedVolFuncs(int pk_id,int fk_id,int func_id,LPCTSTR full_name,LPCTSTR abbrev_name,LPCTSTR basis_name)
	{
		pkID = pk_id;
		fkTicketID = fk_id;
		nFuncID = func_id;
		sFullName = full_name;	
		sAbbrevName = abbrev_name;	
		sBasisName = basis_name;	
	}
	CSelectedVolFuncs(const CSelectedVolFuncs& c)
	{
		*this = c;
	}
	// GET
	int getID() const { return pkID; }
	int getTicketID() const { return fkTicketID; }
	int getFuncID() const { return nFuncID; }
	CString getFullName() const { return sFullName; }
	CString getAbbrevName() const { return sAbbrevName; }
	CString getBasisName() const { return sBasisName; }
	// SET
	void setFuncID(int v) { nFuncID = v; }
	void setFullName(LPCTSTR v) { sFullName = v; }
	void setAbbrevName(LPCTSTR v) { sAbbrevName = v; }
	void setBasisName(LPCTSTR v) { sBasisName = v; }
};


//////////////////////////////////////////////////////////////////////////////////////////
// CLogScaleTemplates
class CLogScaleTemplates
{
//private:
	int pkTmplID;
	CString sTmplName;	
	CString sCreatedBy;	
	CString sDate;	
	int nBuyer;
	int nHauler;
	int nLocation;
	int nTractID;
	int nScaler;
	int nSourceID;
	int nVendor;
	int nSupplier;
	CString sNotes;
	CString	sPricelist;
	int	nPrlID;
	CString	sPrlName;
	double fTrimFT;
	double fTrimCM;
	CString dCreated;			// created,
	CString dUpdated;			// last updated
	int nMeasureMode;			// 0 = Inch, 1 = metric
	int nDefSpecies;
	int nDefGrade;
public:
	CLogScaleTemplates()
	{
		pkTmplID = -1;
		sTmplName = L"";	
		sCreatedBy = L"";	
		sDate = L"";	
		nBuyer = -1;
		nHauler = -1;
		nLocation = -1;
		nTractID = -1;
		nScaler = -1;
		nSourceID = -1;
		nVendor = -1;
		nSupplier = -1;
		sNotes = L"";
		sPricelist = L"";
		nPrlID = -1;
		sPrlName = L"";
		fTrimFT = 0.0;
		fTrimCM = 0.0;
		nMeasureMode = -1;
		nDefSpecies = -1;
		nDefGrade = -1;
		dCreated = L"";			// created,
		dUpdated = L"";			// last updated
	}
	CLogScaleTemplates(int pk_id,LPCTSTR tmpl_name,LPCTSTR created_by,LPCTSTR date,
										 int buyer,int hauler,int location,int tract_id,int scaler,int source_id,int vendor,int supplier,
										 LPCTSTR notes,LPCTSTR pricelist,int prl_id,LPCTSTR prl_name,double trim_ft,double trim_cm,int measure_mode,
										 int def_spc,int def_grade,LPCTSTR created = L"",LPCTSTR updated = L"")
	{
		pkTmplID = pk_id;
		sTmplName = tmpl_name;	
		sCreatedBy = created_by;	
		sDate = date;	
		nBuyer = buyer;
		nHauler = hauler;
		nLocation = location;
		nTractID = tract_id;
		nScaler = scaler;
		nSourceID = source_id;
		nVendor = vendor;
		nSupplier = supplier;
		sNotes = notes;
		sPricelist = pricelist;
		nPrlID = prl_id;
		sPrlName = prl_name;
		fTrimFT = trim_ft;
		fTrimCM = trim_cm;
		nMeasureMode = measure_mode;
		nDefSpecies = def_spc;
		nDefGrade = def_grade;
		dCreated = created;			// created,
		dUpdated = updated;			// last updated
	}
	CLogScaleTemplates(const CSelectedVolFuncs& c)
	{
		*this = c;
	}
	// GET
	int getTmplID() const { return pkTmplID; }
	CString getTmplName() const { return sTmplName; }
	CString getCreatedBy() const { return sCreatedBy; }
	CString getDate() const { return sDate; }
	int getBuyer() const { return nBuyer; }
	int getHauler() const { return nHauler; }
	int getLocation() const { return nLocation; }
	int getTractID() const { return nTractID; }
	int getScaler() const { return nScaler; }
	int getSourceID() const { return nSourceID; }
	int getVendor() const { return nVendor; }
	int getSupplier() const { return nSupplier; }
	CString getNotes() const { return sNotes; }
	CString getPricelist() const { return sPricelist; }
	int getPrlID() const { return nPrlID; }
	CString getPrlName() const { return sPrlName; }
	double getTrimFT() const { return fTrimFT; }
	double getTrimCM() const { return fTrimCM; }
	int getMeasureMode() const { return nMeasureMode; }
	int getDefSpecies() const { return nDefSpecies; }
	int getDefGrade() const { return nDefGrade; }
	CString getCreated() const { return dCreated; }
	CString getUpdated() const { return dUpdated; }			
	// SET
	void setTmplID(int v) { pkTmplID = v; }
	void setTmplName(LPCTSTR v) { sTmplName = v; }
	void setCreatedBy(LPCTSTR v) { sCreatedBy = v; }
	void setDate(LPCTSTR v) { sDate = v; }
	void setBuyer(int v) { nBuyer = v; }
	void setHauler(int v) { nHauler = v; }
	void setLocation(int v) { nLocation = v; }
	void setTractID(int v) { nTractID = v; }
	void setScaler(int v) { nScaler = v; }
	void setSourceID(int v) { nSourceID = v; }
	void setVendor(int v) { nVendor = v; }
	void setSupplier(int v) { nSupplier = v; }
	void setNotes(LPCTSTR v) { sNotes = v; }
	void setPricelist(LPCTSTR v) { sPricelist = v; }
	void setPrlID(int v) { nPrlID = v; }
	void setPrlName(LPCTSTR v) { sPrlName = v; }
	void setTrimFT(double v) { fTrimFT = v; }
	void setTrimCM(double v) { fTrimCM = v; }
	void setMeasureMode(int v) { nMeasureMode = v; }
	void setDefSpecies(int v) { nDefSpecies = v; }
	void setDefGrade(int v) { nDefGrade = v; }

};

//////////////////////////////////////////////////////////////////////////////////////////
// CListContract
class CListContract
{
//private:
	int nPKID;
	CString sTmplName;	
	CString sCreatedBy;	
	CString sDate;	
	CString sBuyer;
	CString sHauler;
	CString sLocation;
	CString sTractID;
	CString sScaler;
	CString sSourceID;
	CString sVendor;
	CString sSupplier;
	CString sNotes;
	CString	sPrlName;
	double fTrimFT;
	double fTrimCM;
	CString sMeasureMode;			// 0 = Inch, 1 = metric
public:
	CListContract()
	{
		nPKID = -1;
		sTmplName = L"";	
		sCreatedBy = L"";	
		sDate = L"";	
		sBuyer = L"";
		sHauler = L"";
		sLocation = L"";
		sTractID = L"";
		sScaler = L"";
		sSourceID = L"";
		sVendor = L"";
		sSupplier = L"";
		sNotes = L"";
		sPrlName = L"";
		fTrimFT = 0.0;
		fTrimCM = 0.0;
		sMeasureMode = L"";
	}
	CListContract(int pk_id,LPCTSTR tmpl_name,LPCTSTR created_by,LPCTSTR date,LPCTSTR buyer,LPCTSTR hauler,LPCTSTR location,
								LPCTSTR tract_id,LPCTSTR scaler,LPCTSTR source_id,LPCTSTR vendor,LPCTSTR supplier,
								LPCTSTR notes,LPCTSTR prl_name,double trim_ft,double trim_cm,LPCTSTR measure_mode)
	{
		nPKID = pk_id;
		sTmplName = tmpl_name;	
		sCreatedBy = created_by;	
		sDate = date;	
		sBuyer = buyer;
		sHauler = hauler;
		sLocation = location;
		sTractID = tract_id;
		sScaler = scaler;
		sSourceID = source_id;
		sVendor = vendor;
		sSupplier = supplier;
		sNotes = notes;
		sPrlName = prl_name;
		fTrimFT = trim_ft;
		fTrimCM = trim_cm;
		sMeasureMode = measure_mode;
	}
	CListContract(const CListContract& c)
	{
		*this = c;
	}
	// GET
	int getPKID() const { return nPKID; }
	CString getTmplName() const { return sTmplName; }
	CString getCreatedBy() const { return sCreatedBy; }
	CString getDate() const { return sDate; }
	CString getBuyer() const { return sBuyer; }
	CString getHauler() const { return sHauler; }
	CString getLocation() const { return sLocation; }
	CString getTractID() const { return sTractID; }
	CString getScaler() const { return sScaler; }
	CString getSourceID() const { return sSourceID; }
	CString getVendor() const { return sVendor; }
	CString getSupplier() const { return sSupplier; }
	CString getNotes() const { return sNotes; }
	CString getPrlName() const { return sPrlName; }
	double getTrimFT() const { return fTrimFT; }
	double getTrimCM() const { return fTrimCM; }
	CString getMeasureMode() const { return sMeasureMode; }

};

//////////////////////////////////////////////////////////////////////////////////////////
// CUserVolTables
class CUserVT
{
//private:
	int pkID;
	CString sFullName;	
	CString sAbbrevName;	
	CString sBasisName;	
	CString sCreatedBy;
	CString sDate;
	CString sMeasuringMode;		
	CString sNotes;				// Notes for volume-table
public:
	CUserVT()
	{
		pkID = -1;
		sFullName = L"";	
		sAbbrevName = L"";	
		sBasisName = L"";	
		sCreatedBy = L"";
		sDate = L"";
		sMeasuringMode = L"";
		sNotes = L"";
	}
	CUserVT(int pk_id,LPCTSTR full_name,LPCTSTR abbrev_name,LPCTSTR basis_name,LPCTSTR created_by,LPCTSTR date,LPCTSTR measure_mode,LPCTSTR notes)
	{
		pkID = pk_id;
		sFullName = full_name;	
		sAbbrevName = abbrev_name;	
		sBasisName = basis_name;	
		sCreatedBy = created_by;
		sDate = date;
		sMeasuringMode = measure_mode;
		sNotes = notes;
	}
	CUserVT(const CUserVT& c)
	{
		*this = c;
	}
	// GET
	int getID() const { return pkID; }
	CString getFullName() const { return sFullName; }
	CString getAbbrevName() const { return sAbbrevName; }
	CString getBasisName() const { return sBasisName; }
	CString getCreatedBy() const { return sCreatedBy; }
	CString getDate() const { return sDate; }
	CString getMeasuringMode() const { return sMeasuringMode; }
	CString getNotes() const { return sNotes; }
};

//////////////////////////////////////////////////////////////////////////////////////////
// CInventoty
class CInventory
{
//private:
	int pkInventID;
	int nType;
	CString sName;	
	CString sID;	// And ID-number i.g. Sales number etc.
	CString sCreatedBy;	
	CString sDate;	
	int nNumOfLogs;
	double fSumPrice;
	int nBuyerID;
	int nHauler;
	int nLocation;
	int nTractID;
	int nScaler;
	int nSourceID;
	int nVendor;
	int nSupplier;
	CString sNotes;
	CString sGPSCoord1;
	CString sGPSCoord2;
public:
	CInventory()
	{
		pkInventID = -1;
		nType = -1;
		sName = L"";	
		sID = L"";	// And ID-number i.g. Sales number etc.
		sCreatedBy = L"";	
		sDate = L"";	
		nNumOfLogs = -1;
		fSumPrice = 0.0;
		nBuyerID = -1;
		nHauler = -1;
		nLocation = -1;
		nTractID = -1;
		nScaler = -1;
		nSourceID = -1;
		nVendor = -1;
		nSupplier = -1;
		sNotes = L"";
		sGPSCoord1 = L"";
		sGPSCoord2 = L"";
	}
	CInventory(int pk_id,int type,LPCTSTR name,LPCTSTR id,LPCTSTR created_by,LPCTSTR date,int num_of_logs,double sum_price,
						int buyer_id,int hauler,int location,int tract_id,int scaler,int source_id,int vendor,int supplier,
						LPCTSTR notes,LPCTSTR gps_coord1,LPCTSTR gps_coord2)
	{
		pkInventID = pk_id;
		nType = type;
		sName = name;	
		sID = id;	// And ID-number i.g. Sales number etc.
		sCreatedBy = created_by;	
		sDate = date;	
		nNumOfLogs = num_of_logs;
		fSumPrice = sum_price;
		nBuyerID = buyer_id;
		nHauler = hauler;
		nLocation = location;
		nTractID = tract_id;
		nScaler = scaler;
		nSourceID = source_id;
		nVendor = vendor;
		nSupplier = supplier;
		sNotes = notes;
		sGPSCoord1 = gps_coord1;
		sGPSCoord2 = gps_coord2;
	}
	CInventory(const CInventory& c)
	{
		*this = c;
	}
	// GET
	int getPKID() const { return pkInventID; }
	int getType() const { return nType; }
	CString getName() const { return sName; }
	CString getID() const { return sID; }
	CString getCreatedBy() const { return sCreatedBy; }
	CString getDate() const { return sDate; }
	int getNumOfLogs() const { return nNumOfLogs; }
	double getSumPrice() const { return fSumPrice; }
	int getBuyerID() const { return nBuyerID; }
	int getHaulerID() const { return nHauler; }
	int getLocationID() const { return nLocation; }
	int getTractID() const { return nTractID; }
	int getScalerID() const { return nScaler; }
	int getSourceID() const { return nSourceID; }
	int getVendorID() const { return nVendor; }
	int getSupplierID() const { return nSupplier; }
	CString getNotes() const { return sNotes; }
	CString getGPSCoord1() const { return sGPSCoord1; }
	CString getGPSCoord2() const { return sGPSCoord2; }
	// SET
	void setType(int v) { nType = v; }
	void setName(LPCTSTR v) { sName = v; }
	void setID(LPCTSTR v) { sID = v; }
	void setCreatedBy(LPCTSTR v) { sCreatedBy = v; }
	void setDate(LPCTSTR v) { sDate = v; }
	void setNumOfLogs(int v) { nNumOfLogs = v; }
	void setSumPrice(double v) { fSumPrice = v; }
	void setBuyerID(int v) { nBuyerID = v; }
	void setHaulerID(int v) { nHauler = v; }
	void setLocationID(int v) { nLocation = v; }
	void setTractID(int v) { nTractID = v; }
	void setScalerID(int v) { nScaler = v; }
	void setSourceID(int v) { nSourceID = v; }
	void setVendorID(int v) { nVendor = v; }
	void setSupplier(int v) { nSupplier = v; }
	void setNotes(LPCTSTR v) { sNotes = v; }
	void setGPSCoord1(LPCTSTR v) { sGPSCoord1 = v; }
	void setGPSCoord2(LPCTSTR v) { sGPSCoord2 = v; }
};

//////////////////////////////////////////////////////////////////////////////////////////
// CInventotyLogs
class CInventoryLogs
{
//private:
	int fkInventID;
	int fkLoadID;	// TraktID
	int fkLogID;
public:
	CInventoryLogs()
	{
		fkInventID = -1;
		fkLoadID = -1;
		fkLogID = -1;
	}
	CInventoryLogs(int fk_invent,int fk_load,int fk_log)
	{
		fkInventID = fk_invent;
		fkLoadID = fk_load;
		fkLogID = fk_log;
	}
	CInventoryLogs(const CInventoryLogs& c)
	{
		*this = c;
	}
	// GET
	int getFKInventID() const { return fkInventID; }
	int getFKLoadID() const { return fkLoadID; }
	int getFKLogID() const { return fkLogID; }
};

//////////////////////////////////////////////////////////////////////////////////////////
// Class for scanning duplicate logs
class CDuplicateLogs
{
public:
	BOOL bDuplicate;
	int nLoadID;
	int nLogID;
	CString sTicketNumber;
	CString sTagNumber;
	int nStatus;

	CDuplicateLogs(void)
	{
		bDuplicate = FALSE;
		nLoadID = -1;
		nLogID = -1;
		sTicketNumber = L"";
		sTagNumber = L"";
		nStatus = -1;
	}

	CDuplicateLogs(BOOL duplicate,int load_id,int log_id,LPCTSTR ticket_num,LPCTSTR tag_num,int status)
	{
		bDuplicate = duplicate;
		nLoadID = load_id;
		nLogID = log_id;
		sTicketNumber = ticket_num;
		sTagNumber = tag_num;
		nStatus = status;
	}
};

//////////////////////////////////////////////////////////////////////////////////////////
// Class for search
class CSearch
{
//public:
	int nTicketID;
	int nLogID;
	CString sTicket;
	CString sTagNumber;
	int nStatus;
	CString sSpcCode;
	CString sSpcName;
	CString sGradeCode;
	double fVolume;
	double fPrice;
	CString sFuncName;
	double fLengthMeas;
	CString sStoreDate;
	CString sUpdateDate;
	CString sSalesName;
	int nInventID;
public:
	CSearch()
	{
		nTicketID = -1;
		nLogID = -1;
		sTicket = L"";
		sTagNumber = L"";
		nStatus = -1;
		sSpcCode = L"";
		sSpcName = L"";
		sGradeCode = L"";
		fVolume = 0.0;
		fPrice = 0.0;
		sFuncName = L"";
		fLengthMeas = 0.0;
		sStoreDate = L"";
		sUpdateDate = L"";
		sSalesName = L"";
		nInventID = -1;
	}
	CSearch(int ticket_id,int log_id,LPCTSTR ticket,LPCTSTR tag,int status,
					LPCTSTR spc_code,LPCTSTR spc_name,LPCTSTR grade_code,double volume,
					double price,LPCTSTR func_name,double length,LPCTSTR c_date,LPCTSTR u_date,LPCTSTR sales = L"",int inv_id = -1)
	{
		nTicketID = ticket_id;
		nLogID = log_id;
		sTicket = ticket;
		sTagNumber = tag;
		nStatus = status;
		sSpcCode = spc_code;
		sSpcName = spc_name;
		sGradeCode = grade_code;
		fVolume = volume;
		fPrice = price;
		sFuncName = func_name;
		fLengthMeas = length;
		sStoreDate = c_date;
		sUpdateDate = u_date;
		sSalesName = sales;
		nInventID = inv_id;
	}
	// GET
	int getTicketID()	const { return	nTicketID; }
	int getLogID() const { return nLogID; }
	CString getTicket() const { return sTicket; }
	CString getTag() const { return sTagNumber; }
	int getStatus() const { return nStatus; }
	CString getSpcCode() const { return sSpcCode; }
	CString getSpcName() const { return	sSpcName; }
	CString getGradeCode() const { return	sGradeCode; }
	double getVolume() const { return fVolume; }
	double getPrice() const { return fPrice; }
	CString getFuncName() const { return sFuncName; }
	double getLengthMeas() const { return fLengthMeas; }
	CString getStoreDate() const { return sStoreDate; }
	CString getUpdDate() const { return sUpdateDate; }
	CString getSalesName() const { return sSalesName; }
	int getInvID() const { return nInventID; }
	// SET
	void setSalesName(LPCTSTR v) { sSalesName = v; }
	void setInventID(int v) { nInventID = v; }
};

//////////////////////////////////////////////////////////////////////////////////////////
// Class for search for Sales
class CSearchSales
{
//public:
	int nTicketID;
	int nLogID;
	int nInventID;
	CString sTicketName;
	CString sTagNumber;
	CString sSalesName;
public:
	CSearchSales()
	{
		nTicketID = -1;
		nLogID = -1;
		nInventID = -1;
		sTicketName = L"";
		sTagNumber = L"";
		sSalesName = L"";
	}
	CSearchSales(int ticket_id,int log_id,int inv_id,LPCTSTR ticket,LPCTSTR tag,LPCTSTR sales)
	{
		nTicketID = ticket_id;
		nLogID = log_id;
		nInventID = inv_id;
		sTicketName = ticket;
		sTagNumber = tag;
		sSalesName = sales;
	}
	// GET
	int getTicketID()	const { return	nTicketID; }
	int getLogID() const { return nLogID; }
	int getInvID() const { return nInventID; }
	CString getTicketName() const { return sTicketName; }
	CString getTag() const { return sTagNumber; }
	CString getSales() const { return sSalesName; }
};


//////////////////////////////////////////////////////////////////////////////////////////
// Declare vectors for TransactionClasses
typedef std::vector<CTickets> CVecTickets;
typedef std::vector<CTicketSpc> CVecTicketSpc;
typedef std::vector<CTicketPrl> CVecTicketPrl;
//typedef std::vector<CLogs> CVecLogs; Moved to LogScaleVolTransactionClasses.h in \include directory
typedef std::vector<CRegister> CVecRegister;
typedef std::vector<CSpecies> CVecSpecies;
typedef std::vector<CGrades> CVecGrades;
typedef std::vector<CGradesExt> CVecGradesExt;
typedef std::vector<CSpeciesGradesAndPrice> CVecSpeciesGradesAndPrice;
typedef std::vector<CPricelist> CVecPricelist;
typedef std::vector<CCalcTypes> CVecCalcTypes;
typedef std::vector<CDefaults> CVecDefaults;
//typedef std::vector<CUserVolTables> CVecUserVolTables; Moved to LogScaleVolTransactionClasses.h in \include directory
typedef std::vector<CSelectedVolFuncs> vecSelectedVolFuncs;
typedef std::vector<CLogScaleTemplates> vecLogScaleTemplates;
//typedef std::vector<CDefUserVolFuncs> vecDefUserVolFuncs; Moved to LogScaleVolTransactionClasses.h in \include directory
typedef std::vector<CPricelistInTicket> vecPricelistInTicket;

typedef std::vector<CInventory> vecInventory;
typedef std::vector<CInventoryLogs> vecInventoryLogs;

typedef std::vector<CDuplicateLogs> vecDuplicateLogs;

typedef std::vector<CSearch> vecSearch;
typedef std::vector<CSearchSales> vecSearchSales;

typedef std::vector<double> vecDouble;
typedef std::vector<CString> vecString;

typedef CList<CString,CString&> clistStrings;
#endif