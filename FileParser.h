#if !defined(__FILEPARSER_H__)
#define __FILEPARSER_H__
#pragma once

#include <iostream>
#include <fstream>

//#include <stdio.h>

#include <map>

#include "TransactionClasses.h"


class CFileParser
{
//private:

	// Data members
	//FILE *m_fp;
	std::fstream fs;
	CStringArray m_sarrFileContent;
	CString m_sFile;
	bool m_bSUCCESS;
	short m_nNumOfTicketsInFile;
	int m_nLoadID;
	int m_nMeasureMode;
	double m_fBarkRatio;
	CString m_sFileVersion;
	std::map<int,CString> m_mapHeaderData;
	CTickets m_recTickets;
	CVecLogs m_vecLogs;

	// Methods
	bool parsIdentifer(void);
	void parsHeader(void);
	void parsLogs(void);
	bool isIdentifer(LPCTSTR str);
public:
	// Default constructor
	CFileParser();
	CFileParser(LPCTSTR file);
	// Destructor
	virtual ~CFileParser() 
	{ 
		if (fs.good())
			fs.close();
	}

	bool getTicket(CTickets& rec);
	bool getLogs(int measure_mode,int load_id,CVecLogs& vec);

};


#endif
