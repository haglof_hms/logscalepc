#pragma once

#include "Resource.h"

// COwerwriteTmplDlg dialog

class COwerwriteTmplDlg : public CXTResizeDialog
{
	DECLARE_DYNAMIC(COwerwriteTmplDlg)

	BOOL m_bInitialized;
	CString m_sLangFN;

	CString m_sMsgCap;
	CString m_sMsgFileNameExists;

	CMyExtStatic m_lbl18_1;
	CMyExtStatic m_lbl18_2;
	CMyExtStatic m_lbl18_3;
	CMyExtStatic m_lbl18_4;
	CMyExtStatic m_lbl18_5;

	CMyExtEdit m_edit18_1;

	CButton m_btnOwerWrite;
	CButton m_btnSaveAs;
	CButton m_btnCancel;

	CString m_sNewPathAndFilenName;
	CString m_sPath;
	CString m_sFileName;
	short m_nReturnValue;
public:
	COwerwriteTmplDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~COwerwriteTmplDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG18 };

	void setPath(LPCTSTR v);

	short getReturnValue()	{ return m_nReturnValue; }

	CString getNewPathAndFilename() { return m_sNewPathAndFilenName; }
protected:
	//{{AFX_VIRTUAL(CDefaultsDlg)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL


	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton182();
	afx_msg void OnEnChangeEdit181();
	afx_msg void OnBnClickedButton181();
};
