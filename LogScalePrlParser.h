#ifndef _LOGSCALEPRLPARSER_H_
#define _LOGSCALEPRLPARSER_H_

#include "pad_transaction_classes.h"

class LogScalePrlParser
{
	MSXML2::IXMLDOMDocumentPtr pDomDoc;
protected:
	CString getAttrText(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
	double getAttrDouble(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
	int getAttrInt(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
	BOOL getAttrBool(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name);
public:
	LogScalePrlParser(void);

	virtual ~LogScalePrlParser();

	BOOL LoadFromFile(LPCTSTR);
	BOOL LoadFromBuffer(LPCTSTR);
	BOOL SaveToFile(LPCTSTR);

	BOOL isALogScalePricelist();

	BOOL getHeaderName(LPTSTR);
	BOOL getHeaderDoneBy(LPTSTR);
	BOOL getHeaderDate(LPTSTR);
	BOOL getHeaderNotes(LPTSTR);

	BOOL getDataFile(LPTSTR);
	long getDataFileSize();

	BOOL getXML(CString &xml);
};


#endif