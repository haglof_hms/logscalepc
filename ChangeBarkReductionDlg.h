#pragma once

#include "Resource.h"

// CChangeBarkReductionDlg dialog

class CChangeBarkReductionDlg : public CXTResizeDialog
{
	DECLARE_DYNAMIC(CChangeBarkReductionDlg)
	BOOL m_bInitialized;
	CString m_sLangFN;
	
	CString m_sMeasureMode;

	CButton m_btnOK;
	CButton m_btnCancel;

	CTickets m_recTicket;
	CVecTicketSpc m_vecVecTicketSpc;
	CVecTicketSpc m_vecReturnVecTicketSpc;

	CMyExtStatic m_lbl12_1;
	CMyReportControl m_repSpecies;

	void setupReport();

	void populateReport();
public:
	CChangeBarkReductionDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CChangeBarkReductionDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG12 };

	void setData(CTickets& rec,CVecTicketSpc& vec,LPCTSTR mmode)
	{
		m_recTicket = rec;
		m_vecVecTicketSpc = vec;
		m_sMeasureMode = mmode;
	}

	CVecTicketSpc getTicketSpc()	{ return m_vecReturnVecTicketSpc; }
protected:
	//{{AFX_VIRTUAL(CChangeBarkReductionDlg)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CChangeBarkReductionDlg)
	afx_msg void OnShowWindow(BOOL bShow,UINT nStatus);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};
