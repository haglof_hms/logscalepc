#pragma once

#include "Resource.h"

// CSelectExcelSheetDlg dialog

class CSelectExcelSheetDlg : public CDialog
{
	DECLARE_DYNAMIC(CSelectExcelSheetDlg)
	BOOL m_bInitialized;
	CString m_sLangFN;

	CButton m_btnOK;
	CButton m_btnCancel;
	CButton m_btnSelectAll;
	CButton m_btnDeSelectAll;

	CMyExtStatic m_lbl17_1;
	CMyExtStatic m_lbl17_2;
	CMyExtStatic m_lbl17_3;
	CMyExtStatic m_lbl17_4;

	CButton m_check17_1;

	CComboBox m_cbx17_2;
	CComboBox m_cbx17_3;

	CListCtrl m_list17_1;

	CStringArray m_sarrSheets;
	CStringArray m_sarrTableTypes;
	CStringArray m_sarrMeasuringMode;

	bool m_bSelectedIndexSheets;
	int m_nSelectedIndexTableType;
	int m_nSelectedIndexMeasMode;
	std::vector<int> m_vecSheetsSelected;
	short m_nOpenAs;
	int m_nLastCheckedItem;

public:
	CSelectExcelSheetDlg(CWnd* pParent = NULL,short open_as = 0);   // standard constructor
	virtual ~CSelectExcelSheetDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG17 };


	void setSheetNames(CStringArray& names) { m_sarrSheets.Append(names);	}
	//int getSelectedSheet()	{ return m_nSelectedIndexSheets; }
	void getSelectedSheets(std::vector<int>& vec)	{ vec = m_vecSheetsSelected; }
	int getSelectedTableType()	{ return m_nSelectedIndexTableType; }
	int getSelectedMeasMode()	{ return m_nSelectedIndexMeasMode; }

protected:
	//{{AFX_VIRTUAL(CChangeBarkReductionDlg)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnCbnSelchangeCombo171();
	afx_msg void OnCbnSelchangeCombo172();
	afx_msg void OnCbnSelchangeCombo173();
	afx_msg void OnLvnItemchangedList171(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButton171();
	afx_msg void OnBnClickedButton172();
	afx_msg void OnLvnItemchangingList171(NMHDR *pNMHDR, LRESULT *pResult);
};
