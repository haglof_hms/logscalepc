// LogSalesFormView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "LogSalesFormView.h"
#include "reportclasses.h"

#include "logsalesview.h"

#include "ResLangFileReader.h"

#include "ChangeStatusDlg.h"

#include "XTPPreviewView.h"

#include "TagNoMatchDlg.h"

#include "ViewProgress.h"

#include "libxl.h"

#include "ExcelExportDlg.h"

using namespace libxl;

///////////////////////////////////////////////////////////////////////////////////////////
// CLogSalesDoc


IMPLEMENT_DYNCREATE(CLogSalesDoc, CDocument)

BEGIN_MESSAGE_MAP(CLogSalesDoc, CDocument)
	//{{AFX_MSG_MAP(CLogSalesFrame)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLogSalesFrame construction/destruction

CLogSalesDoc::CLogSalesDoc()
{
	// TODO: add one-time construction code here

}

CLogSalesDoc::~CLogSalesDoc()
{
}


BOOL CLogSalesDoc::OnNewDocument()
{
	// CHECK FOR LICENSE HERE!!!!! 2011-10-18 P�D
	if (!License())
	{
		return FALSE;
	}


	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CLogSalesDoc serialization

void CLogSalesDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CLogSalesFrame diagnostics

#ifdef _DEBUG
void CLogSalesDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CLogSalesDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


///////////////////////////////////////////////////////////////////////////////////////////
// CLogSalesFrame

IMPLEMENT_DYNCREATE(CLogSalesFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CLogSalesFrame, CMDIChildWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_SYSCOMMAND()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_COMMAND_RANGE(ID_BUTTON32799,ID_BUTTON32804, OnCommand)

	ON_COMMAND(ID_BUTTON32805, OnTBBtnPrintOut)
	ON_UPDATE_COMMAND_UI(ID_BUTTON32805, OnUpdatePrintOutTBtn)

	ON_CONTROL(CBN_SELCHANGE,ID_BUTTON32804, OnPrintOutCBox)

	ON_XTP_CREATECONTROL()
END_MESSAGE_MAP()


// CLogSalesFrame construction/destruction

XTPDockingPanePaintTheme CLogSalesFrame::m_themeCurrent = xtpPaneThemeOffice2003;

CLogSalesFrame::CLogSalesFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);

	m_bSysCommand = FALSE;
	m_bInitReports = FALSE;
	m_bIsPrintOutTBtn = FALSE;
}

CLogSalesFrame::~CLogSalesFrame()
{
}

void CLogSalesFrame::OnDestroy(void)
{
	if (m_fnt1)
		delete m_fnt1;
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6006_KEY);
	SavePlacement(this, csBuf);
}

void CLogSalesFrame::OnSysCommand(UINT nID,LPARAM lParam)
{
	if ((nID & 0xFFF0) == SC_CLOSE)
	{
		CLogSalesFormView* pView = (CLogSalesFormView *)getFormViewByID(IDD_FORMVIEW6);
		if (pView != NULL)
		{
			m_bSysCommand = TRUE;

			if (pView->doSave(CHECK_SAVE_TYPES::CHECK_SAVE_ON_QUIT) != QUIT_TYPES::DO_NOT_QUIT)
			{
				CMDIChildWnd::OnSysCommand(nID,lParam);
			}
		}
	}
	else
		CMDIChildWnd::OnSysCommand(nID,lParam);
}

void CLogSalesFrame::OnClose(void)
{
	if (!m_bSysCommand)
	{
		CLogSalesFormView* pView = (CLogSalesFormView *)getFormViewByID(IDD_FORMVIEW6);
		if (pView != NULL)
		{
			if (pView->doSave(CHECK_SAVE_TYPES::CHECK_SAVE_ON_QUIT) != QUIT_TYPES::DO_NOT_QUIT)
			{
				CMDIChildWnd::OnClose();
			}
		}
	}
	else
	{
		CMDIChildWnd::OnClose();
	}
}


int CLogSalesFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	EnableDocking(CBRS_ALIGN_ANY);

	// Create and Load toolbar; 090107 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR6);
	m_wndToolBar.EnableDocking(xtpFlagAlignTop|xtpFlagStretched);

	// Initialize dialog bar m_wndFieldSelectionDlg
	if (!m_wndFieldSelectionDlg.Create(this, IDD_FIELD_SELECTION2,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN_SELECTION_DLG2))
	{
		return -1;      // fail to create
	}

	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_wndFieldSelectionDlg.SetWindowText(xml.str(IDS_STRING1100));

			//***************************************************************************************
			// Setup commandbars and manues; 051114 p�d
			CXTPToolBar* pToolBar = &m_wndToolBar;
			if (pToolBar->IsBuiltIn())
			{
				if (pToolBar->GetType() != xtpBarTypeMenuBar)
				{
					UINT nBarID = pToolBar->GetBarID();
					pToolBar->LoadToolBar(nBarID, FALSE);
					CXTPControls *p = pToolBar->GetControls();
					// Setup icons on toolbars, using resource dll; 051208 p�d
					if (nBarID == IDR_TOOLBAR6)
					{		
						setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_IMPORT,xml.str(IDS_STRING3001),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(1),RES_TB_SAVE,xml.str(IDS_STRING3002),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(2),RES_TB_DEL,xml.str(IDS_STRING3005),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(3),RES_TB_EXPORT,xml.str(IDS_STRING3004),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(5),RES_TB_PRINT,L"CBOX",TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(6),RES_TB_PRINT,xml.str(IDS_STRING3552),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(7),RES_TB_PREVIEW,xml.str(IDS_STRING3003),TRUE);	//

						p->GetAt(4)->SetVisible(FALSE);
						p->GetAt(7)->SetVisible(FALSE);
						p->GetAt(8)->SetVisible(FALSE);
					}	// if (nBarID == IDR_TOOLBAR6)
				}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
			}	// if (pToolBar->IsBuiltIn())
			//***************************************************************************************
			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

	// docking for field chooser
	m_wndFieldSelectionDlg.EnableDocking(0);

	// Don't show fieldchooser at this stadge
	ShowControlBar(&m_wndFieldSelectionDlg, FALSE, FALSE);
	FloatControlBar(&m_wndFieldSelectionDlg, CPoint(100, GetSystemMetrics(SM_CYSCREEN) / 3));

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

int CLogSalesFrame::OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl)
{
	if (lpCreateControl->nID == ID_BUTTON32804)
	{
		if (!m_cboxPrintOut.Create(WS_CHILD|WS_VISIBLE|CBS_DROPDOWNLIST|WS_CLIPCHILDREN,CRect(0,0,0,200), this, ID_BUTTON32804) )
		{
			AfxMessageBox(_T("ERROR:\nOnCreateControl"));
		}
		else
		{
			m_fnt1 = new CFont();
			LOGFONT lf;
			memset(&lf,0,sizeof(LOGFONT));
			lf.lfHeight = 16;
			lf.lfWeight = FW_NORMAL;
			m_fnt1->CreateFontIndirect(&lf);


			m_cboxPrintOut.SetOwner(this);
			m_cboxPrintOut.MoveWindow(45, 3, 150, 20);
			m_cboxPrintOut.SetFont(m_fnt1);

	    CXTPControlCustom * pCB = CXTPControlCustom::CreateControlCustom(&m_cboxPrintOut);

			lpCreateControl->buttonStyle = xtpButtonIconAndCaption;
      lpCreateControl->pControl = pCB;
				
		}
		return TRUE;
	}

	return FALSE;
}


void CLogSalesFrame::OnUpdatePrintOutTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsPrintOutTBtn );
}


void CLogSalesFrame::OnTBBtnPrintOut()
{

	CString sReportPathAndFN = L"";
	CString sFileExtension = L"";
	CString sArgStr = L"";
	int nTractID = -1; // LoadID
	getSTDReports(m_sShellDataFile,PROGRAM_NAME,m_nShellDataIndex,m_vecReports);

	int nIdx = m_cboxPrintOut.GetCurSel();

	if (nIdx > -1 && m_vecReports.size() > 0)
	{

		sArgStr = L"";
		CLogSalesView *pView = (CLogSalesView*)getFormViewByID(IDD_FORMVIEW11);
		if (pView != NULL)
		{
			nTractID = pView->getActiveLoadID();
			sArgStr.Format(_T("%d;"),nTractID);
		}

		sReportPathAndFN.Format(_T("%s%s\\%s"),
														getReportsDir(),
														getLangSet(),
														m_vecReports[nIdx].getFileName());
		if (fileExists(sReportPathAndFN))
		{
			AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,WM_USER+4,
				(LPARAM)&_user_msg(333,	// ID = 333 for CrystalReport, ID = 300 for FastReports
				_T("OpenSuiteEx"),			// Exported/Imported function
				_T("Reports2.dll"),			// Suite to call; Report2.dll = Reportgenerator for CrystalReports (A.G.), Report.dll = Reportgenerator for FastReports
				(sReportPathAndFN),			// Use this report
				(sReportPathAndFN),
				sArgStr));
		}
	}	// if (nIdx > -1 && nIdx < m_vecReports.size())

}

void CLogSalesFrame::OnPrintOutCBox()
{
	m_bIsPrintOutTBtn = m_cboxPrintOut.GetCurSel() > CB_ERR;
}


BOOL CLogSalesFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CLogSalesFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CLogSalesFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  { 
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6006_KEY);
		LoadPlacement(this, csBuf);
  }

}

void CLogSalesFrame::OnSetFocus(CWnd* pWnd)
{
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,ID_DO_SOMETHING_IN_SHELL,ID_LPARAM_COMMAND2);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CMDIChildWnd::OnSetFocus(pWnd);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CLogSalesFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	CString sStr = L"";
	if (wParam == (ID_DO_SOMETHING_IN_SHELL + ID_LPARAM_COMMAND2))
	{
		if (!m_bInitReports)
		{
			// The return message holds a _user_msg structure, collected in the
			// OnMessageFromShell( WPARAM wParam, LPARAM lParam ); 070410 p�d
			// In this case the szFileName item in _user_msg structure holds
			// the path and filename of the ShellData file used and the szArgStr
			// holds the Suite/UserModule name; 070410 p�d
			_user_msg *msg = (_user_msg*)lParam;
			if (msg != NULL)
			{
				m_sShellDataFile = msg->getFileName();
				//m_nShellDataIndex = msg->getIndex();
				m_nShellDataIndex = 6007; // explicit set Identifer, macth id in ShellData file; 120306 p�d
				getSTDReports(m_sShellDataFile,PROGRAM_NAME,m_nShellDataIndex,m_vecReports);
				m_cboxPrintOut.ResetContent();
				RLFReader xml; // = new RLFReader;
				if (xml.Load(m_sLangFN))
				{

					for (UINT i = 0;i < m_vecReports.size();i++)
					{
						if (m_vecReports[i].getCaption().IsEmpty())
							sStr = xml.str(m_vecReports[i].getStrID()); 
						else
							sStr = m_vecReports[i].getCaption();
						m_cboxPrintOut.AddString(sStr);
					}
				}
			}	// if (msg != NULL)

			m_bInitReports = TRUE;
		}
	}

	return 0L;
}


void CLogSalesFrame::OnCommand(UINT nID)
{
	CLogSalesFormView* pView = (CLogSalesFormView *)getFormViewByID(IDD_FORMVIEW6);
	if (pView != NULL)
	{
		switch(nID)
		{
			case ID_BUTTON32799 : pView->doOpenTagFile(); break;
			case ID_BUTTON32800 :	pView->doSave(CHECK_SAVE_TYPES::CHECK_SAVE_NO_QUIT); break;
			case ID_BUTTON32801 :	pView->doDelete(); break;
			case ID_BUTTON32802 : pView->doExportToEXCEL(); break;
			case ID_BUTTON32806 : pView->doPrintOut(); break;

		};
	}
}


// CLogSalesFrame diagnostics

#ifdef _DEBUG
void CLogSalesFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CLogSalesFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CLogSalesFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

//////////////////////////////////////////////////////////////////////////////////////////
// CLogSalesFormView

IMPLEMENT_DYNCREATE(CLogSalesFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CLogSalesFormView, CXTResizeFormView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_CLOSE()
	ON_WM_COPYDATA()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
END_MESSAGE_MAP()

CLogSalesFormView::CLogSalesFormView()
	: CXTResizeFormView(CLogSalesFormView::IDD),
		m_pDB(NULL),
		m_bInitialized(FALSE)
		, m_csDate(_T(""))
{
}

CLogSalesFormView::~CLogSalesFormView()
{
}

void CLogSalesFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLogSalesReportView)
	DDX_Control(pDX, IDC_LBL6_1, m_lbl6_1);
	DDX_Control(pDX, IDC_LBL6_20, m_lbl6_20);
	DDX_Control(pDX, IDC_LBL6_21, m_lbl6_21);
	DDX_Control(pDX, IDC_LBL6_22, m_lbl6_22);
	DDX_Control(pDX, IDC_LBL6_23, m_lbl6_23);
	DDX_Control(pDX, IDC_LBL6_2, m_lbl6_2);
	DDX_Control(pDX, IDC_LBL6_3, m_lbl6_3);
	DDX_Control(pDX, IDC_LBL6_24, m_lbl6_24);
	DDX_Control(pDX, IDC_LBL6_25, m_lbl6_25);
	DDX_Control(pDX, IDC_LBL6_26, m_lbl6_26);
	DDX_Control(pDX, IDC_LBL6_27, m_lbl6_27);
	DDX_Control(pDX, IDC_LBL6_28, m_lbl6_28);
	DDX_Control(pDX, IDC_LBL6_29, m_lbl6_29);
	DDX_Control(pDX, IDC_LBL6_30, m_lbl6_30);
	DDX_Control(pDX, IDC_LBL6_31, m_lbl6_31);


	DDX_Control(pDX, IDC_COMBO6_1, m_cbx6_1);
	DDX_Control(pDX, IDC_COMBO6_14, m_cbx6_14);
	DDX_Control(pDX, IDC_COMBO6_15, m_cbx6_15);
	DDX_Control(pDX, IDC_COMBO6_16, m_cbx6_16);
	DDX_Control(pDX, IDC_COMBO6_17, m_cbx6_17);
	DDX_Control(pDX, IDC_COMBO6_18, m_cbx6_18);
	DDX_Control(pDX, IDC_COMBO6_19, m_cbx6_19);
	DDX_Control(pDX, IDC_COMBO6_20, m_cbx6_20);

	DDX_Control(pDX, IDC_EDIT6_1, m_ed6_1);
	DDX_Control(pDX, IDC_EDIT6_15, m_ed6_15);
	DDX_Control(pDX, IDC_EDIT6_16, m_ed6_16);
	DDX_Control(pDX, IDC_EDIT6_17, m_ed6_17);
	DDX_Control(pDX, IDC_EDIT6_18, m_ed6_18);
	DDX_Control(pDX, IDC_EDIT6_19, m_ed6_19);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_DATE, m_dtDate);
	DDX_DateTimeCtrl(pDX, IDC_DATE, m_csDate);
}

void CLogSalesFormView::OnClose(void)
{
	CXTResizeFormView::OnClose();
}

void CLogSalesFormView::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	if (m_wndTabControl.GetSafeHwnd())
	{
		m_wndTabControl.MoveWindow(0, 170, cx, cy-174);
	}
}

int CLogSalesFormView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS, CRect(0, 0, 0, 0), this, ID_TABCONTROL);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearancePropertyPage2003);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = TRUE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = TRUE;
	m_wndTabControl.GetPaintManager()->DisableLunaColors(FALSE);

	// Setup language filename; 051214 p�d
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;

		if (xml.Load(m_sLangFN))
		{

			AddView(RUNTIME_CLASS(CLogSalesReportView), xml.str(IDS_STRING3560),-1);
		}
	}

	return 0; // creation ok
}

BOOL CLogSalesFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{

	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		if (m_dbConnectionData.conn->isConnected())
		{
			m_pDB = new CDBHandling(m_dbConnectionData);
			if (m_pDB != NULL)
			{
				m_pDB->getRegister(m_vecRegister);
				m_pDB->getInventories(m_vecInventory);
			}
		}
	}

	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

BOOL CLogSalesFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}


void CLogSalesFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	if (! m_bInitialized )
	{

		m_cbx6_1.LimitText(25);
		m_cbx6_19.LimitText(50);

		m_ed6_1.LimitText(25);
		m_ed6_15.LimitText(25);
		m_ed6_16.LimitText(25);
		m_ed6_17.LimitText(25);
		m_ed6_18.LimitText(50);
		m_ed6_19.LimitText(50);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{

				m_sMsgCap = xml.str(IDS_STRING99);
				m_sMsgDelete1 = xml.str(IDS_STRING3580);
				m_sMsgDelete2 = xml.str(IDS_STRING3581);
				m_sMsgCheck1 = xml.str(IDS_STRING3590);
				m_sMsgCheck2.Format(L"%s\n\n%s\n\n",xml.str(IDS_STRING3590),xml.str(IDS_STRING3591));
				m_sMsgCheck3 = xml.str(IDS_STRING3593);
				m_sMsgOpenEXCEL = xml.str(IDS_STRING3598);
				
				m_sMsgConfirmSave.Format(L"%s\n\n%s\n\n",xml.str(IDS_STRING3050),xml.str(IDS_STRING3051));

				m_sTicket = xml.str(IDS_STRING2001);
				m_sTagNumber = xml.str(IDS_STRING1700);

				m_sMsgIllegalChars = xml.str(IDS_STRING18442);

				m_lbl6_1.SetWindowTextW(xml.str(IDS_STRING3020));
				m_lbl6_20.SetWindowTextW(xml.str(IDS_STRING3021));
				m_lbl6_21.SetWindowTextW(xml.str(IDS_STRING3022));
				m_lbl6_22.SetWindowTextW(xml.str(IDS_STRING3023));
				m_lbl6_23.SetWindowTextW(xml.str(IDS_STRING3024));
				m_lbl6_2.SetWindowTextW(xml.str(IDS_STRING3025));
				m_lbl6_3.SetWindowTextW(xml.str(IDS_STRING3026));
				m_lbl6_24.SetWindowTextW(xml.str(IDS_STRING3027));
				m_lbl6_25.SetWindowTextW(xml.str(IDS_STRING3028));
				m_lbl6_26.SetWindowTextW(xml.str(IDS_STRING3029));
				m_lbl6_27.SetWindowTextW(xml.str(IDS_STRING3030));
				m_lbl6_28.SetWindowTextW(xml.str(IDS_STRING30200));
				m_lbl6_29.SetWindowTextW(xml.str(IDS_STRING3031));
				m_lbl6_30.SetWindowTextW(xml.str(IDS_STRING3032));
				m_lbl6_31.SetWindowTextW(xml.str(IDS_STRING3033));

				m_sarrHeadLines.Add(L"");
				m_sarrHeadLines.Add(L"");
				m_sarrHeadLines.Add(L"");
				m_sarrHeadLines.Add(L"");
				m_sarrHeadLines.Add(L"");
				m_sarrHeadLines.Add(xml.str(IDS_STRING1744));
				m_sarrHeadLines.Add(xml.str(IDS_STRING1705));
				m_sarrHeadLines.Add(xml.str(IDS_STRING17050));
				m_sarrHeadLines.Add(xml.str(IDS_STRING1706));
				m_sarrHeadLines.Add(xml.str(IDS_STRING17060));
				m_sarrHeadLines.Add(xml.str(IDS_STRING1707));
				m_sarrHeadLines.Add(xml.str(IDS_STRING17070));
				m_sarrHeadLines.Add(xml.str(IDS_STRING1708));
				m_sarrHeadLines.Add(xml.str(IDS_STRING17080));
				m_sarrHeadLines.Add(xml.str(IDS_STRING17090));
				m_sarrHeadLines.Add(xml.str(IDS_STRING17091));
				m_sarrHeadLines.Add(xml.str(IDS_STRING1710));
				m_sarrHeadLines.Add(xml.str(IDS_STRING1711));
				m_sarrHeadLines.Add(xml.str(IDS_STRING1716));
				m_sarrHeadLines.Add(xml.str(IDS_STRING17160));
				m_sarrHeadLines.Add(xml.str(IDS_STRING1719));
				m_sarrHeadLines.Add(xml.str(IDS_STRING17190));
				m_sarrHeadLines.Add(xml.str(IDS_STRING1717));
				m_sarrHeadLines.Add(xml.str(IDS_STRING1718));
				m_sarrHeadLines.Add(xml.str(IDS_STRING17180));
				m_sarrHeadLines.Add(xml.str(IDS_STRING1720));
				m_sarrHeadLines.Add(xml.str(IDS_STRING1721));
				m_sarrHeadLines.Add(xml.str(IDS_STRING1722));
				m_sarrHeadLines.Add(xml.str(IDS_STRING1723));

				m_sarrHeadLines.Add(L"");
				m_sarrHeadLines.Add(L"");

			
				xml.clean();
			}
		}	

		// Add data to comboboxes
		cbRegisterData(REGISTER_TYPES::BUYER,&m_cbx6_1,m_vecRegister);
		cbRegisterData(REGISTER_TYPES::HAULER,&m_cbx6_14,m_vecRegister);
		cbRegisterData(REGISTER_TYPES::SCALER,&m_cbx6_15,m_vecRegister);
		cbRegisterData(REGISTER_TYPES::SUPPLIER,&m_cbx6_16,m_vecRegister);
		cbRegisterData(REGISTER_TYPES::SOURCEID,&m_cbx6_17,m_vecRegister);
		cbRegisterData(REGISTER_TYPES::LOCATION,&m_cbx6_18,m_vecRegister);
		cbRegisterData(REGISTER_TYPES::TRACTID,&m_cbx6_19,m_vecRegister);
		cbRegisterData(REGISTER_TYPES::VENDOR,&m_cbx6_20,m_vecRegister);

		m_bInitialized = TRUE;
	}
}

LRESULT CLogSalesFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch(wParam)
	{
		case ID_OPEN_NEW_SALE :	
		{
			m_recInventory = CInventory();
			clearView();
#ifdef _AUTOGENERATE_SALE_NUM
			int nNum = 0;
			if (m_pDB != NULL)
			{
				nNum = m_pDB->getLastInventoryID_generate();
			}
/*
			// Set a SalesID
			if (m_vecInventory.size() > 0)
				nNum = m_vecInventory[m_vecInventory.size()-1].getPKID();
*/
			m_ed6_1.SetWindowTextW(generateSalesNumber(nNum+1));
#endif
			break;
		}	// case ID_MSG_FROM :

		case ID_OPEN_SALE :	
		{
			CInventory *rec = (CInventory*)lParam;
			if (sizeof(*rec) == sizeof(CInventory))
			{
				m_recInventory = *rec;
				populateReport();
			}

			break;
		}	// case ID_MSG_FROM :

		case ID_OPEN_SALE_SEARCH :	
		{
			CSearch *rec = (CSearch*)lParam;
			if (sizeof(*rec) == sizeof(CSearch))
			{
				if (m_pDB != NULL)
				{
					m_pDB->getInventory(rec->getInvID(),-1,m_recInventory);
				}
				populateReport(true,rec->getLogID());
			}

			break;
		}	// case ID_MSG_FROM :


		case ID_OPEN_SALE_TAG_FILE :	
		{
			LPCTSTR str = (LPCTSTR)lParam;
			m_recInventory = CInventory();
			clearView();

#ifdef _AUTOGENERATE_SALE_NUM
			// Set a SalesID
			int nNum = 0;
			if (m_pDB != NULL)
			{
				nNum = m_pDB->getLastInventoryID_generate();
			}
/*
			if (m_vecInventory.size() > 0)
			{
				nNum = m_vecInventory[m_vecInventory.size()-1].getPKID();
				if (nNum == 0)
					nNum = 1;
			}
*/
			m_ed6_1.SetWindowTextW(generateSalesNumber(nNum+1));
#endif

			CLogSalesReportView *pLogView = getSalesView();
			if (pLogView != NULL)
			{
				pLogView->handleTagFile(str,TRUE);
			}
			break;
		}	// case ID_MSG_FROM :

	};

	return 0L;
}

// CLogSalesFormView diagnostics

#ifdef _DEBUG
void CLogSalesFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CLogSalesFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG

void CLogSalesFormView::clearView()
{
	m_ed6_1.SetWindowTextW(L"");
	m_ed6_15.SetWindowTextW(getUserName().MakeUpper());
	m_ed6_16.SetWindowTextW(L"");
	m_ed6_17.SetWindowTextW(L"");
	m_ed6_18.SetWindowTextW(L"");
	m_ed6_19.SetWindowTextW(L"");

	m_cbx6_1.SetCurSel(-1);
	m_cbx6_14.SetCurSel(-1);
	m_cbx6_15.SetCurSel(-1);
	m_cbx6_16.SetCurSel(-1);
	m_cbx6_17.SetCurSel(-1);
	m_cbx6_18.SetCurSel(-1);
	m_cbx6_19.SetCurSel(-1);
	m_cbx6_20.SetCurSel(-1);

	m_ed6_1.SetFocus();
}

void CLogSalesFormView::enableView(BOOL enable)
{
	m_ed6_1.EnableWindow(enable);
	m_ed6_15.EnableWindow(enable);
	m_ed6_16.EnableWindow(enable);
	m_ed6_17.EnableWindow(enable);
	m_ed6_18.EnableWindow(enable);
	m_ed6_19.EnableWindow(enable);

	m_dtDate.EnableWindow(enable);
	m_cbx6_1.EnableWindow(enable);
	m_cbx6_14.EnableWindow(enable);
	m_cbx6_15.EnableWindow(enable);
	m_cbx6_16.EnableWindow(enable);
	m_cbx6_17.EnableWindow(enable);
	m_cbx6_18.EnableWindow(enable);
	m_cbx6_19.EnableWindow(enable);
	m_cbx6_20.EnableWindow(enable);
}


void CLogSalesFormView::populateReport(bool set_row,int log_id)
{
	double fAvg[11];
	double fSumVol = 0.0,fSumValue = 0.0;

	CString sValue = L"";
	int nRowCnt = 0;
	CXTPReportRow *pRowSet = NULL;
	CLogSalesReportView *pReportView = getSalesView();
	if (m_pDB != NULL && pReportView != NULL)
	{

		for (int i = 0;i < 11;i++)
		{
			fAvg[i] = 0.0;
		}

		m_ed6_1.SetWindowTextW(m_recInventory.getID());
		m_ed6_19.SetWindowTextW(m_recInventory.getName());

		m_ed6_15.SetWindowTextW(m_recInventory.getCreatedBy());
		m_ed6_16.SetWindowTextW(m_recInventory.getGPSCoord1());
		m_ed6_17.SetWindowTextW(m_recInventory.getGPSCoord2());
		m_ed6_18.SetWindowTextW(m_recInventory.getNotes());

		m_csDate = m_recInventory.getDate();
		m_cbx6_1.SetCurSel(m_cbx6_1.FindStringExact(0,getRegisterValue(REGISTER_TYPES::BUYER,m_recInventory.getBuyerID(),m_vecRegister)));
		m_cbx6_14.SetCurSel(m_cbx6_14.FindStringExact(0,getRegisterValue(REGISTER_TYPES::HAULER,m_recInventory.getHaulerID(),m_vecRegister)));
		m_cbx6_15.SetCurSel(m_cbx6_15.FindStringExact(0,getRegisterValue(REGISTER_TYPES::SCALER,m_recInventory.getScalerID(),m_vecRegister)));
		m_cbx6_16.SetCurSel(m_cbx6_16.FindStringExact(0,getRegisterValue(REGISTER_TYPES::SUPPLIER,m_recInventory.getSupplierID(),m_vecRegister)));
		m_cbx6_17.SetCurSel(m_cbx6_17.FindStringExact(0,getRegisterValue(REGISTER_TYPES::SOURCEID,m_recInventory.getSourceID(),m_vecRegister)));
		m_cbx6_18.SetCurSel(m_cbx6_18.FindStringExact(0,getRegisterValue(REGISTER_TYPES::LOCATION,m_recInventory.getLocationID(),m_vecRegister)));
		m_cbx6_19.SetCurSel(m_cbx6_19.FindStringExact(0,getRegisterValue(REGISTER_TYPES::TRACTID,m_recInventory.getTractID(),m_vecRegister)));
		m_cbx6_20.SetCurSel(m_cbx6_20.FindStringExact(0,getRegisterValue(REGISTER_TYPES::VENDOR,m_recInventory.getVendorID(),m_vecRegister)));
		
		UpdateData(FALSE);

		m_pDB->getLogsSale(m_vecLogs,m_recInventory.getPKID());
		if (m_vecLogs.size() > 0)
		{
			pReportView->GetReportCtrl().BeginUpdate();
			pReportView->GetReportCtrl().ResetContent();

			for (UINT i = 0;i < m_vecLogs.size();i++)
			{	
				pReportView->GetReportCtrl().AddRecord(new CLogSalesReportRec(m_vecLogs[i]));
				if (m_vecLogs[i].getLogID() == log_id)
				{
					nRowCnt = i;
				}
			}

			pReportView->GetReportCtrl().Populate();
			pReportView->GetReportCtrl().UpdateWindow();

			int nNumOfRows = 0;
			CXTPReportRows *pRows = pReportView->GetReportCtrl().GetRows();
			if (pRows != NULL)
			{
				nNumOfRows = pRows->GetCount();
				for (int i = 0;i < pRows->GetCount();i++)
				{
					CLogSalesReportRec *pRec = (CLogSalesReportRec*)pRows->GetAt(i)->GetRecord();
					if (pRec != NULL)
					{
						CLogs recLog = pRec->getRecord();
						fAvg[0] += recLog.getDOBTop();
						fAvg[1] += recLog.getDOBTop2();
						fAvg[2] += recLog.getDIBTop();
						fAvg[3] += recLog.getDIBTop2();
						fAvg[4] += recLog.getDOBRoot();
						fAvg[5] += recLog.getDOBRoot2();
						fAvg[6] += recLog.getDIBRoot();
						fAvg[7] += recLog.getDIBRoot2();
						fAvg[8] += recLog.getLengthMeas();
						fAvg[9] += recLog.getLengthCalc();
						fAvg[10] += recLog.getBark();
						
						fSumVol += recLog.getVolPricelist();
						fSumValue += recLog.getLogPrice();
					}
				}
			}	// if (pRows != NULL)
			CLogs recSumLog = CLogs();
			recSumLog.setDOBTop(fAvg[0] > 0.0 ? fAvg[0]/nNumOfRows:0.0);	
			recSumLog.setDOBTop2(fAvg[1] > 0.0 ? fAvg[1]/nNumOfRows:0.0);	
			recSumLog.setDIBTop(fAvg[2] > 0.0 ? fAvg[2]/nNumOfRows:0.0);
			recSumLog.setDIBTop2(fAvg[3] > 0.0 ? fAvg[3]/nNumOfRows:0.0);
			recSumLog.setDOBRoot(fAvg[4] > 0.0 ? fAvg[4]/nNumOfRows:0.0);
			recSumLog.setDOBRoot2(fAvg[5] > 0.0 ? fAvg[5]/nNumOfRows:0.0);
			recSumLog.setDIBRoot(fAvg[6] > 0.0 ? fAvg[6]/nNumOfRows:0.0);
			recSumLog.setDIBRoot2(fAvg[7] > 0.0 ? fAvg[7]/nNumOfRows:0.0);
			recSumLog.setLengthMeas(fAvg[8] > 0.0 ? fAvg[8]/nNumOfRows:0.0);
			recSumLog.setLengthCalc(fAvg[9] > 0.0 ? fAvg[9]/nNumOfRows:0.0);
			recSumLog.setBark(fAvg[10] > 0.0 ? fAvg[10]/nNumOfRows:0.0);
			recSumLog.setVolPricelist(fSumVol);
			recSumLog.setLogPrice(fSumValue);

			pReportView->GetReportCtrl().GetFooterRecords()->RemoveAll();
			pReportView->GetReportCtrl().GetFooterRecords()->Add(new CLogSalesReportRec(CLogs(),m_sarrHeadLines,true,1));
			pReportView->GetReportCtrl().GetFooterRecords()->Add(new CLogSalesReportRec(recSumLog,m_sarrHeadLines,true,2));
			pReportView->GetReportCtrl().ShowFooterRows(TRUE);

			pReportView->GetReportCtrl().Populate();
			pReportView->GetReportCtrl().UpdateWindow();

			pReportView->GetReportCtrl().EndUpdate();

			if (set_row)
			{				
				if ((pRowSet = pReportView->GetReportCtrl().GetRows()->GetAt(nRowCnt)) != NULL)
				{
					if (pRowSet->GetIndex() > -1 )
					{
						// Focus on set row
						pReportView->GetReportCtrl().SetFocusedRow(pRowSet);
						pRowSet->SetSelected(TRUE);
						pReportView->GetReportCtrl().UpdateWindow();
						pReportView->GetReportCtrl().SetFocus();
					}
				}	// if ((pRowSet = pView->GetReportCtrl().GetRows()->GetAt(nRowCnt)) != NULL)
			}	// if (set_row)

		}
	}

}

BOOL CLogSalesFormView::Save()
{
	BOOL bAddLog = FALSE,bAreThereLogs = TRUE,bYes = TRUE,bLogsToAdd = FALSE;
	int nLastPKID = -1;
	int nLastType = -1;
	int nNumOfLogs = 0;
	double fPrice = 0.0;
	CString sDate = L"",sSQLBuffer = L"",sSQL = L"",sSQLBuffer2 = L"",sSQL2 = L"";
	CInventory rec = CInventory();
	CXTPReportRows *pRows = NULL;
	CLogSalesReportRec *pRec = NULL;
	CLogSalesReportView *pReportView = getSalesView();
	CViewProgress wndPrg;

	UpdateData(TRUE);

	if (m_pDB != NULL && pReportView != NULL)
	{
		if (m_recInventory.getPKID() < 0)
		{
			//bAreThereLogs = (pReportView->GetReportCtrl().GetRows()->GetCount() > 0);
			//if (bAreThereLogs)
			//{
				bYes = (::MessageBox(GetSafeHwnd(),m_sMsgConfirmSave,m_sMsgCap,MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON1) == IDYES);
			//}

			if (bYes) // && bAreThereLogs)
			{
				pReportView->GetReportCtrl().Populate();
				// Count number of logs and sum. value
				if ((pRows = pReportView->GetReportCtrl().GetRows()) != NULL)
				{
					for (int i = 0;i < pRows->GetCount();i++)
					{
						if ((pRec = (CLogSalesReportRec*)pRows->GetAt(i)->GetRecord()) != NULL)
						{
							nNumOfLogs += pRec->getColInt(COLUMN_4);
							fPrice += pRec->getColFloat(COLUMN_19);
						}
					}
				}

				rec = CInventory(-1,LOGSTATUS::SOLD,
														m_ed6_19.getText(),
														m_ed6_1.getText(),
														m_ed6_15.getText(),
														m_csDate,
														nNumOfLogs,
														fPrice,
														cbGetRegisterID(REGISTER_TYPES::BUYER, &m_cbx6_1,m_vecRegister),
														cbGetRegisterID(REGISTER_TYPES::HAULER, &m_cbx6_14,m_vecRegister),
														cbGetRegisterID(REGISTER_TYPES::LOCATION, &m_cbx6_18,m_vecRegister),
														cbGetRegisterID(REGISTER_TYPES::TRACTID, &m_cbx6_19,m_vecRegister),
														cbGetRegisterID(REGISTER_TYPES::SCALER, &m_cbx6_15,m_vecRegister),
														cbGetRegisterID(REGISTER_TYPES::SOURCEID, &m_cbx6_17,m_vecRegister),
														cbGetRegisterID(REGISTER_TYPES::VENDOR, &m_cbx6_20,m_vecRegister),
														cbGetRegisterID(REGISTER_TYPES::SUPPLIER, &m_cbx6_16,m_vecRegister),
														m_ed6_18.getText(),
														m_ed6_16.getText(),
														m_ed6_17.getText());
				m_pDB->newInventory(rec);

				m_pDB->getLastInventoryAndTypeID(&nLastPKID,&nLastType);

				// Reload data from last entered item. I.e. New Item
				m_pDB->getInventory(nLastPKID, /*nLastType*/ -1, m_recInventory);	// #4163

				// Use this data to add Logs into the InventoryLogs table
				// and set Logs in Logs-table to sold
				if ((pRows = pReportView->GetReportCtrl().GetRows()) != NULL)
				{
					// Create the edit control and add it to the status bar
					if (!wndPrg.Create(CViewProgress::IDD))
					{
						TRACE0("Failed to create edit control.\n");
						return FALSE;
					}

					if (wndPrg.GetSafeHwnd())
					{
						wndPrg.setRange(0,pRows->GetCount());
						wndPrg.setStep();
						wndPrg.ShowWindow(SW_NORMAL);
						wndPrg.UpdateWindow();
					}

					sSQL.Format(L"INSERT INTO %s (fkInventID,fkLoadID,fkLogID) ", TBL_INVENTORY_LOGS);
					sSQLBuffer = sSQL;

					sSQL2.Format(L"UPDATE %s SET nStatusFlag=%d WHERE ",TBL_LOGS,LOGSTATUS::SOLD);
					sSQLBuffer2 = sSQL2;
					for (int i = 0;i < pRows->GetCount();i++)
					{
						if ((pRec = (CLogSalesReportRec*)pRows->GetAt(i)->GetRecord()) != NULL)
						{
							// First we'll add the log to InventoryLogs table
							// Second, we'll change status on Log in logs-table
							CLogs rec = pRec->getRecord();
							/*
							if (m_pDB->newInventoryLogs(CInventoryLogs(nLastPKID,rec.getLoadID(),rec.getLogID())))
							{
								//rec.setStatusFlag(LOGSTATUS::SOLD);
								//m_pDB->setLogStatus(rec);
							}
							*/

							if (i < pRows->GetCount()-1)
							{
								sSQL.Format(L"SELECT %d,%d,%d UNION ALL ",nLastPKID,rec.getLoadID(),rec.getLogID());
								sSQL2.Format(L"(pkLogID=%d AND fkLoadID=%d) OR ",rec.getLogID(),rec.getLoadID());
							}
							else if (i == pRows->GetCount()-1)
							{
								sSQL.Format(L"SELECT %d,%d,%d",nLastPKID,rec.getLoadID(),rec.getLogID());
								sSQL2.Format(L"(pkLogID=%d AND fkLoadID=%d);",rec.getLogID(),rec.getLoadID());
							}

							sSQLBuffer += sSQL;
							sSQLBuffer2 += sSQL2;
							if (wndPrg.GetSafeHwnd())
								wndPrg.setPos(i+1);
						}	
					}
					if (wndPrg.GetSafeHwnd())
					{
						wndPrg.setRange(0,100);
						wndPrg.setPos(40);
					}
					if (pRows->GetCount() > 0)
					{
						if (m_pDB->newInventoryLogs(sSQLBuffer))
						{
							if (wndPrg.GetSafeHwnd())
								wndPrg.setPos(60);
							m_pDB->setLogStatus(sSQLBuffer2);
						}
					}
					sSQLBuffer.Empty();
					sSQLBuffer2.Empty();
					if (wndPrg.GetSafeHwnd())
					{
						wndPrg.setPos(100);
						wndPrg.ShowWindow(SW_HIDE);
						wndPrg.DestroyWindow();
					}
				}
			}
		}
		else
		{		

			m_pDB->getInventoryLogs(m_vecInvLogs,m_recInventory.getPKID());

			// Count number of logs and sum. value
			if ((pRows = pReportView->GetReportCtrl().GetRows()) != NULL)
			{
				for (int i = 0;i < pRows->GetCount();i++)
				{
					if ((pRec = (CLogSalesReportRec*)pRows->GetAt(i)->GetRecord()) != NULL)
					{
						nNumOfLogs += pRec->getColInt(COLUMN_4);
						fPrice += pRec->getColFloat(COLUMN_19);
					}
				}
			}

			rec = CInventory(m_recInventory.getPKID(),
											 m_recInventory.getType(),
											 m_ed6_19.getText(),
											 m_ed6_1.getText(),
											 m_ed6_15.getText(),
											 m_csDate,
											 nNumOfLogs,
											 fPrice,
											 cbGetRegisterID(REGISTER_TYPES::BUYER, &m_cbx6_1,m_vecRegister),
											 cbGetRegisterID(REGISTER_TYPES::HAULER, &m_cbx6_14,m_vecRegister),
											 cbGetRegisterID(REGISTER_TYPES::LOCATION, &m_cbx6_18,m_vecRegister),
											 cbGetRegisterID(REGISTER_TYPES::TRACTID, &m_cbx6_19,m_vecRegister),
											 cbGetRegisterID(REGISTER_TYPES::SCALER, &m_cbx6_15,m_vecRegister),
											 cbGetRegisterID(REGISTER_TYPES::SOURCEID, &m_cbx6_17,m_vecRegister),
											 cbGetRegisterID(REGISTER_TYPES::VENDOR, &m_cbx6_20,m_vecRegister),
											 cbGetRegisterID(REGISTER_TYPES::SUPPLIER, &m_cbx6_16,m_vecRegister),
											 m_ed6_18.getText(),
											 m_ed6_16.getText(),
											 m_ed6_17.getText());
			m_pDB->updInventory(rec);

			//#3611 uppdaterar m_recInventory efter sparning
			m_pDB->getInventory(m_recInventory.getPKID(),m_recInventory.getType(),m_recInventory);

			// Use this data to add Logs into the InventoryLogs table
			// and set Logs in Logs-table to sold
			if ((pRows = pReportView->GetReportCtrl().GetRows()) != NULL)
			{
				// Create the edit control and add it to the status bar
				if (!wndPrg.Create(CViewProgress::IDD))
				{
					TRACE0("Failed to create edit control.\n");
					return FALSE;
				}

				if (wndPrg.GetSafeHwnd())
				{
					wndPrg.setRange(0,pRows->GetCount());
					wndPrg.setStep();
					wndPrg.ShowWindow(SW_NORMAL);
					wndPrg.UpdateWindow();
				}

				sSQL.Format(L"INSERT INTO %s (fkInventID,fkLoadID,fkLogID) ", TBL_INVENTORY_LOGS);
				sSQLBuffer = sSQL;

				sSQL2.Format(L"UPDATE %s SET nStatusFlag=%d WHERE ",TBL_LOGS,LOGSTATUS::SOLD);
				sSQLBuffer2 = sSQL2;

				for (int i = 0;i < pRows->GetCount();i++)
				{
					if ((pRec = (CLogSalesReportRec*)pRows->GetAt(i)->GetRecord()) != NULL)
					{
						// First we'll add the log to InventoryLogs table
						// Second, we'll change status on Log in logs-table
						CLogs rec = pRec->getRecord();
						// We need to check that Log isn't already in SaleTable
						// If so do nothing. Only add logs that might have been added
						// to the Sale.
						bAddLog = TRUE;
						if (m_vecInvLogs.size() > 0)
						{
							for (UINT i1 = 0;i1 < m_vecInvLogs.size();i1++)
							{
								if (m_vecInvLogs[i1].getFKInventID() == m_recInventory.getPKID() &&
									m_vecInvLogs[i1].getFKLoadID() == rec.getLoadID() &&
									m_vecInvLogs[i1].getFKLogID() == rec.getLogID())
								{
									bAddLog = FALSE;
									break;
								}									  
							}
						}

						if (bAddLog)
						{
							bLogsToAdd = TRUE;

							if (i < pRows->GetCount()-1)
							{
								sSQL.Format(L"SELECT %d,%d,%d UNION ALL ",m_recInventory.getPKID(),rec.getLoadID(),rec.getLogID());
								sSQL2.Format(L"(pkLogID=%d AND fkLoadID=%d) OR ",rec.getLogID(),rec.getLoadID());
							}
							else if (i == pRows->GetCount()-1)
							{
								sSQL.Format(L"SELECT %d,%d,%d",m_recInventory.getPKID(),rec.getLoadID(),rec.getLogID());
								sSQL2.Format(L"(pkLogID=%d AND fkLoadID=%d);",rec.getLogID(),rec.getLoadID());
							}

							sSQLBuffer += sSQL;
							sSQLBuffer2 += sSQL2;
							if (wndPrg.GetSafeHwnd())
								wndPrg.setPos(i+1);
						}
					}	// if ((pRec = (CLogSalesReportRec*)pRows->GetAt(i)->GetRecord()) != NULL)
				}	// for (int i = 0;i < pRows->GetCount();i++)

				if (wndPrg.GetSafeHwnd())
				{
					wndPrg.setRange(0,100);
					wndPrg.setPos(40);
				}
				if (bLogsToAdd)
				{

					if (m_pDB->newInventoryLogs(sSQLBuffer))
					{
						if (wndPrg.GetSafeHwnd())
							wndPrg.setPos(60);
						m_pDB->setLogStatus(sSQLBuffer2);
					}
				}
				wndPrg.setPos(100);
				if (wndPrg.GetSafeHwnd())
				{
					wndPrg.ShowWindow(SW_HIDE);
					wndPrg.DestroyWindow();
				}
			}
		}

		CLogSalesView *pLogSalesView = (CLogSalesView*)getFormViewByID(IDD_FORMVIEW11);
		if (pLogSalesView != NULL)
		{
			pLogSalesView->populateReport(true);
		}


		return TRUE;
	}	// if (checkData() && m_pDB != NULL && pReportView != NULL)
	else
		return FALSE;

}

void CLogSalesFormView::DeleteLog()
{
	doSave(CHECK_SAVE_TYPES::CHECK_SAVE_NO_QUIT);
	CString sMsg;
	CLogSalesReportView *pLogReportView = getSalesView();
	if (pLogReportView != NULL)
	{
		CXTPReportRow *pRow = pLogReportView->GetReportCtrl().GetFocusedRow();
		CLogSalesReportRec *pRec = NULL;
		CChangeStatusDlg *pDlg = new CChangeStatusDlg();
		if (pRow != NULL && pDlg != NULL && m_pDB != NULL)
		{
			if ((pRec = (CLogSalesReportRec*)pRow->GetRecord()) != NULL)
			{

				sMsg.Format(m_sMsgDelete1,pRec->getColText(COLUMN_1));
				if (::MessageBox(GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
				{
					return;
				}
#ifdef _ASK_TWICE_ON_DELETE_LOG_ON_SALE
				else if (::MessageBox(GetSafeHwnd(),m_sMsgDelete2,m_sMsgCap,MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
				{
					return;
				}
#endif
				if (pDlg->DoModal() == IDOK)
				{
					CLogs rec = pRec->getRecord();
					if (m_pDB->delInventoryLogs(CInventoryLogs(m_recInventory.getPKID(),rec.getLoadID(),rec.getLogID())))
					{
						rec.setStatusFlag(pDlg->getStatus());
						m_pDB->setLogStatus(rec);
						populateReport();						
					}
				}
			}
		}
	}
}

CLogSalesReportView *CLogSalesFormView::getSalesView(void)
{
	m_tabManager = m_wndTabControl.getTabPage(0);
	if (m_tabManager)
	{
		CLogSalesReportView* pView = DYNAMIC_DOWNCAST(CLogSalesReportView, CWnd::FromHandle(m_tabManager->GetHandle()));
		ASSERT_KINDOF(CLogSalesReportView, pView);
		return pView;
	}
	return NULL;
}

QUIT_TYPES::Q_T_RETURN CLogSalesFormView::checkData(CHECK_SAVE_TYPES::CHECK_SAVE cs)
{
	BOOL bRows = TRUE;
	// Criteria for save and confirm is
	// that an id is entered
	CString sSalesID(m_ed6_1.getText());
	
	// We'll check that name isn't already used
	if (m_vecInventory.size() > 0 && !sSalesID.IsEmpty())
	{
		if (m_pDB != NULL)
		{
			if (!m_pDB->isSalesIDUnique(sSalesID,m_recInventory))
			{
				::MessageBox(GetSafeHwnd(),m_sMsgCheck3,m_sMsgCap,MB_ICONQUESTION | MB_OK);
				m_ed6_1.SetFocus();
				return QUIT_TYPES::NO_SAVE;
			}
		}
	}

	//#3611, l�gg in koll inga ogiltiga tecken i Ticket numret?	\ / ? : * " > < |
		if(checkIllegalChars(m_ed6_1.getText()))
		{
			::MessageBox(GetSafeHwnd(),_T("\\ / ? : * \" > < |")+m_sMsgIllegalChars,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
			m_ed6_1.SetFocus();
				return QUIT_TYPES::NO_SAVE;
		}

#ifdef _FORCE_ERROR_CORRECTION_SALE
	if (sSalesID.IsEmpty())
	{
		::MessageBox(GetSafeHwnd(),m_sMsgCheck1,m_sMsgCap,MB_ICONQUESTION | MB_OK);
		m_ed6_1.SetFocus();
		return QUIT_TYPES::NO_SAVE;
	}
#else
	if (cs == CHECK_SAVE_TYPES::CHECK_SAVE_ON_QUIT)
	{
		CLogSalesReportView *pReportView = getSalesView();
		if (pReportView != NULL)
		{
			CXTPReportRows *pRows = pReportView->GetReportCtrl().GetRows();
			if (pRows != NULL)
				bRows = (pRows->GetCount() > 0);
		}
	}

	if (sSalesID.IsEmpty() || bRows == FALSE)
	{
		if (cs == CHECK_SAVE_TYPES::CHECK_SAVE_NO_QUIT)
		{
			::MessageBox(GetSafeHwnd(),m_sMsgCheck1,m_sMsgCap,MB_ICONQUESTION | MB_OK);
			m_ed6_1.SetFocus();
			return QUIT_TYPES::NO_SAVE;
		}
		else if (cs == CHECK_SAVE_TYPES::CHECK_SAVE_ON_QUIT)
		{
			if (::MessageBox(GetSafeHwnd(),m_sMsgCheck2,m_sMsgCap,MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
			{
				return QUIT_TYPES::QUIT_ANYWAY;
			}
			else
			{
				return QUIT_TYPES::DO_NOT_QUIT;
			}
		}

	}
#endif
	
	return QUIT_TYPES::DO_SAVE;

}

QUIT_TYPES::Q_T_RETURN CLogSalesFormView::doSave(CHECK_SAVE_TYPES::CHECK_SAVE cs)	
{ 
	QUIT_TYPES::Q_T_RETURN ret = checkData(cs);
	if (ret == QUIT_TYPES::DO_SAVE)
	{
		Save(); 
	}

	return ret;
}

void CLogSalesFormView::doDelete()	
{ 
	DeleteLog();
}

void CLogSalesFormView::doOpenTagFile()
{
	if (doSave(CHECK_SAVE_TYPES::CHECK_SAVE_NO_QUIT) == QUIT_TYPES::DO_SAVE)
	{
		CLogSalesReportView *pLogView = getSalesView();
		if (pLogView != NULL)
		{
			pLogView->doOpenTagFile();
		}
	}
}

void CLogSalesFormView::doPrintOut()
{
	CLogSalesReportView *pLogView = getSalesView();
	if (pLogView != NULL)
	{
		pLogView->doPrintOut();
	}
}


void CLogSalesFormView::doExportToEXCEL()
{
	if (doSave(CHECK_SAVE_TYPES::CHECK_SAVE_NO_QUIT) == QUIT_TYPES::DO_SAVE)
	{
		exportToEXCEL();
	}
}

void CLogSalesFormView::exportToEXCEL()
{
	CString sRegKey = L"";
	int f[4];
	CVecLogs vecLogs;
	CLogs recLog = CLogs();
	CLogs recLogAvg = CLogs();
	CString sValue = L"",sFileName = L"",sPath = L"";
	int nRow = 0,nColCnt = 0;
	RLFReader xml;
	CViewProgress wndPrg;
	vecColumnsSet vec;

	// Create the edit control and add it to the status bar
	if (!wndPrg.Create(CViewProgress::IDD))
	{
		TRACE0("Failed to create edit control.\n");
		return;
	}

	doEvents();

	CLogSalesFormView* pView1 = (CLogSalesFormView *)getFormViewByID(IDD_FORMVIEW6);
	if (pView1 != NULL)
	{
		sRegKey.Format(L"%s_%d",m_recInventory.getID(),m_recInventory.getPKID());
		if(checkIllegalChars(m_recInventory.getID())==false)	//#3611, lagt in koll inga ogiltiga tecken i Ticket numret	\ / ? : * " > < |
		{
			saveColumnsSettingToFile(sRegKey,pView1->getSalesView()->GetReportCtrl(),COLSET_SALES);
		}
		else
		{
			::MessageBox(GetSafeHwnd(),_T("\\ / ? : * \" > < |")+m_sMsgIllegalChars,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
			return;
		}
	}

	CInventory recInv = CInventory();
	CLogSalesFormView* pView = (CLogSalesFormView *)getFormViewByID(IDD_FORMVIEW6);
	if (pView != NULL && m_pDB != NULL)
	{
		recInv = pView->getInventoryRec();
		m_pDB->getLogsSale(vecLogs,pView->getInventoryRec().getPKID());
		m_pDB->avgSaleLogData(recLogAvg,pView->getInventoryRec().getPKID());
		sFileName = recInv.getID();
	}

	if (wndPrg.GetSafeHwnd())
	{
		wndPrg.setRange(0,vecLogs.size());
		wndPrg.setStep();
		wndPrg.ShowWindow(SW_NORMAL);
	}

	CExcelExportDlg *pDlg = new CExcelExportDlg();
	if (pDlg != NULL)
	{
		sRegKey.Format(L"%s_%d",m_recInventory.getID(),m_recInventory.getPKID());
		pDlg->getColumnsSetting(sRegKey,COLSET_SALES);
		if (pDlg->DoModal() != IDOK) return;
		pDlg->getColumnsSelected(vec);

		delete pDlg;
	}

	Book* book = xlCreateBook();
	if(book)
	{
		book->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0hfeflb");
		f[0] = book->addCustomNumFormat(L"0.0");
		f[1] = book->addCustomNumFormat(L"0.00");
		f[2] = book->addCustomNumFormat(L"0.000");
		Format* fmt[3];
		fmt[0] = book->addFormat();
		fmt[0]->setNumFormat(f[0]);
		fmt[1] = book->addFormat();
		fmt[1]->setNumFormat(f[1]);
		fmt[2] = book->addFormat();
		fmt[2]->setNumFormat(f[2]);
		fmt[3] = book->addFormat();
		fmt[3]->setWrap();
		Sheet* sheet = book->addSheet(L"Sheet1");
		if(sheet)
		{
			if (xml.Load(m_sLangFN))
			{

				// Logs 
				for (int i = 0;i < vec.size();i++)
				{
					sheet->writeStr(1, i,vec[i].getColName(),fmt[3]);
				}

				for (int col = 0;col < vec.size();col++)
				{
					nColCnt = col;

					for (UINT i = 0;i < vecLogs.size();i++)
					{
						recLog = vecLogs[i];
						if (vec[nColCnt].getColIndex() == 0) sheet->writeStr(i+2, nColCnt,recLog.getTicket());
						if (vec[nColCnt].getColIndex() == 1) sheet->writeStr(i+2, nColCnt,recLog.getTagNumber());
						if (vec[nColCnt].getColIndex() == 2) sheet->writeStr(i+2, nColCnt,recLog.getSpcCode());
						if (vec[nColCnt].getColIndex() == 3) sheet->writeStr(i+2, nColCnt,recLog.getSpcName());
						sValue.Format(L"%d",recLog.getFrequency());
						if (vec[nColCnt].getColIndex() == 4) sheet->writeStr(i+2, nColCnt,sValue);
						if (vec[nColCnt].getColIndex() == 5) sheet->writeStr(i+2, nColCnt,recLog.getGrade());
						if (vec[nColCnt].getColIndex() == 6) sheet->writeNum(i+2, nColCnt,recLog.getDeduction(),fmt[0]);
						if (vec[nColCnt].getColIndex() == 7) sheet->writeNum(i+2, nColCnt,recLog.getDOBTop(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 8) sheet->writeNum(i+2, nColCnt,recLog.getDOBTop2(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 9) sheet->writeNum(i+2, nColCnt,recLog.getDIBTop(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 10) sheet->writeNum(i+2, nColCnt,recLog.getDIBTop2(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 11) sheet->writeNum(i+2, nColCnt,recLog.getDOBRoot(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 12) sheet->writeNum(i+2, nColCnt,recLog.getDOBRoot2(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 13) sheet->writeNum(i+2, nColCnt,recLog.getDIBRoot(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 14) sheet->writeNum(i+2, nColCnt,recLog.getDIBRoot2(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 15) sheet->writeNum(i+2, nColCnt,recLog.getLengthMeas(),fmt[0]);
						if (vec[nColCnt].getColIndex() == 16) sheet->writeNum(i+2, nColCnt,recLog.getLengthCalc(),fmt[0]);
						if (vec[nColCnt].getColIndex() == 17) sheet->writeNum(i+2, nColCnt,recLog.getBark(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 18) sheet->writeNum(i+2, nColCnt,recLog.getVolPricelist(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 19) sheet->writeNum(i+2, nColCnt,recLog.getLogPrice(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 20) sheet->writeStr(i+2, nColCnt,recLog.getVolFuncName(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 21) sheet->writeNum(i+2, nColCnt,recLog.getURootDia(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 22) sheet->writeNum(i+2, nColCnt,recLog.getURootDia2(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 23) sheet->writeNum(i+2, nColCnt,recLog.getUVol(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 24) sheet->writeNum(i+2, nColCnt,recLog.getUTopDia(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 25) sheet->writeNum(i+2, nColCnt,recLog.getUTopDia2(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 26) sheet->writeNum(i+2, nColCnt,recLog.getULength(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 27) sheet->writeNum(i+2, nColCnt,recLog.getUPrice(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 28) sheet->writeStr(i+2, nColCnt,recLog.getReason());
						if (vec[nColCnt].getColIndex() == 29) sheet->writeStr(i+2, nColCnt,recLog.getNotes());
						wndPrg.setPos(i+1);
						doEvents();
					}
				}

				nRow = vecLogs.size();
				for (int col = 0;col < vec.size();col++)
				{
					nColCnt = col;

					//---------------------------------------------------------------------------------------------------
					// Footer headlines
					if (vec[nColCnt].getColIndex() == 0) sheet->writeStr(nRow+2, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 1) sheet->writeStr(nRow+2, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 2) sheet->writeStr(nRow+2, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 3) sheet->writeStr(nRow+2, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 4) sheet->writeStr(nRow+2, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 5) sheet->writeStr(nRow+2, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 6) sheet->writeStr(nRow+2, nColCnt,L"");
					
					if (vec[nColCnt].getColIndex() == 7) sheet->writeStr(nRow+2, nColCnt,xml.str(IDS_STRING1705) + L" " + xml.str(IDS_STRING119),fmt[3]);
					if (vec[nColCnt].getColIndex() == 8) sheet->writeStr(nRow+2, nColCnt,xml.str(IDS_STRING17050) + L" " + xml.str(IDS_STRING119),fmt[3]);
					if (vec[nColCnt].getColIndex() == 9) sheet->writeStr(nRow+2, nColCnt,xml.str(IDS_STRING1706) + L" " + xml.str(IDS_STRING119),fmt[3]);
					if (vec[nColCnt].getColIndex() == 10) sheet->writeStr(nRow+2, nColCnt,xml.str(IDS_STRING17060) + L" " + xml.str(IDS_STRING119),fmt[3]);
					if (vec[nColCnt].getColIndex() == 11) sheet->writeStr(nRow+2, nColCnt,xml.str(IDS_STRING1707) + L" " + xml.str(IDS_STRING119),fmt[3]);
					if (vec[nColCnt].getColIndex() == 12) sheet->writeStr(nRow+2, nColCnt,xml.str(IDS_STRING17070) + L" " + xml.str(IDS_STRING119),fmt[3]);
					if (vec[nColCnt].getColIndex() == 13) sheet->writeStr(nRow+2, nColCnt,xml.str(IDS_STRING1708) + L" " + xml.str(IDS_STRING119),fmt[3]);
					if (vec[nColCnt].getColIndex() == 14) sheet->writeStr(nRow+2, nColCnt,xml.str(IDS_STRING17080) + L" " + xml.str(IDS_STRING119),fmt[3]);
					if (vec[nColCnt].getColIndex() == 15) sheet->writeStr(nRow+2, nColCnt,xml.str(IDS_STRING17090) + L" " + xml.str(IDS_STRING119),fmt[3]);
					if (vec[nColCnt].getColIndex() == 16) sheet->writeStr(nRow+2, nColCnt,xml.str(IDS_STRING17091) + L" " + xml.str(IDS_STRING119),fmt[3]);
					if (vec[nColCnt].getColIndex() == 17) sheet->writeStr(nRow+2, nColCnt,xml.str(IDS_STRING1710) + L" " + xml.str(IDS_STRING119),fmt[3]);
					if (vec[nColCnt].getColIndex() == 18) sheet->writeStr(nRow+2, nColCnt,xml.str(IDS_STRING1711) + L" " + xml.str(IDS_STRING120),fmt[3]);
					if (vec[nColCnt].getColIndex() == 19) sheet->writeStr(nRow+2, nColCnt,xml.str(IDS_STRING1716) + L" " + xml.str(IDS_STRING120),fmt[3]);
					if (vec[nColCnt].getColIndex() == 20) sheet->writeStr(nRow+2, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 21) sheet->writeStr(nRow+2, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 22) sheet->writeStr(nRow+2, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 23) sheet->writeStr(nRow+2, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 24) sheet->writeStr(nRow+2, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 25) sheet->writeStr(nRow+2, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 26) sheet->writeStr(nRow+2, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 27) sheet->writeStr(nRow+2, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 28) sheet->writeStr(nRow+2, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 29) sheet->writeStr(nRow+2, nColCnt,L"");

					//---------------------------------------------------------------------------------------------------
					// Footer data
					if (vec[nColCnt].getColIndex() == 0) sheet->writeStr(nRow+3, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 1) sheet->writeStr(nRow+3, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 2) sheet->writeStr(nRow+3, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 3) sheet->writeStr(nRow+3, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 4) sheet->writeStr(nRow+3, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 5) sheet->writeStr(nRow+3, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 6) sheet->writeStr(nRow+3, nColCnt,L"");
					
					if (vec[nColCnt].getColIndex() == 7) sheet->writeNum(nRow+3, nColCnt,recLogAvg.getDOBTop(),fmt[2]);
					if (vec[nColCnt].getColIndex() == 8) sheet->writeNum(nRow+3, nColCnt,recLogAvg.getDOBTop2(),fmt[2]);
					if (vec[nColCnt].getColIndex() == 9) sheet->writeNum(nRow+3, nColCnt,recLogAvg.getDIBTop(),fmt[2]);
					if (vec[nColCnt].getColIndex() == 10) sheet->writeNum(nRow+3, nColCnt,recLogAvg.getDIBTop2(),fmt[2]);
					if (vec[nColCnt].getColIndex() == 11) sheet->writeNum(nRow+3, nColCnt,recLogAvg.getDOBRoot(),fmt[2]);
					if (vec[nColCnt].getColIndex() == 12) sheet->writeNum(nRow+3, nColCnt,recLogAvg.getDOBRoot2(),fmt[2]);
					if (vec[nColCnt].getColIndex() == 13) sheet->writeNum(nRow+3, nColCnt,recLogAvg.getDIBRoot(),fmt[2]);
					if (vec[nColCnt].getColIndex() == 14) sheet->writeNum(nRow+3, nColCnt,recLogAvg.getDIBRoot2(),fmt[2]);
					if (vec[nColCnt].getColIndex() == 15) sheet->writeNum(nRow+3, nColCnt,recLogAvg.getLengthMeas(),fmt[0]);
					if (vec[nColCnt].getColIndex() == 16) sheet->writeNum(nRow+3, nColCnt,recLogAvg.getLengthCalc(),fmt[0]);
					if (vec[nColCnt].getColIndex() == 17) sheet->writeNum(nRow+3, nColCnt,recLogAvg.getBark(),fmt[2]);
					if (vec[nColCnt].getColIndex() == 18) sheet->writeNum(nRow+3, nColCnt,recLogAvg.getVolPricelist(),fmt[2]);
					if (vec[nColCnt].getColIndex() == 19) sheet->writeNum(nRow+3, nColCnt,recLogAvg.getLogPrice(),fmt[2]);
					if (vec[nColCnt].getColIndex() == 20) sheet->writeStr(nRow+3, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 21) sheet->writeStr(nRow+3, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 22) sheet->writeStr(nRow+3, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 23) sheet->writeStr(nRow+3, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 24) sheet->writeStr(nRow+3, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 25) sheet->writeStr(nRow+3, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 26) sheet->writeStr(nRow+3, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 27) sheet->writeStr(nRow+3, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 28) sheet->writeStr(nRow+3, nColCnt,L"");
					if (vec[nColCnt].getColIndex() == 29) sheet->writeStr(nRow+3, nColCnt,L"");
				}
			}
		}
		
		if (wndPrg.GetSafeHwnd())
		{
			wndPrg.ShowWindow(SW_HIDE);
			wndPrg.DestroyWindow();
		}

		sPath = regGetStr(REG_ROOT,SALE_EXPORT_TO_EXCEL_DIR,DIRECTORY_KEY);
		sPath += sFileName;
		
		CFileDialog dlg(FALSE,L".xls",sFileName,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,L"EXCEL (*.xls)|*.xls|");
		if (dlg.DoModal() == IDOK)
		{
			if(book->save(dlg.GetPathName())) 
			{
#ifdef _OPEN_EXCEL_IN_SALE

				if (::MessageBox(GetSafeHwnd(),m_sMsgOpenEXCEL,m_sMsgCap,MB_ICONQUESTION | MB_YESNO) == IDYES)
						::ShellExecute(NULL, L"open", dlg.GetPathName(), NULL, NULL, SW_SHOW);        
#endif
			}
		
			regSetStr(REG_ROOT,SALE_EXPORT_TO_EXCEL_DIR,DIRECTORY_KEY,getFilePath(dlg.GetPathName()));
		}
	}

}

BOOL CLogSalesFormView::AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon)
{
	CCreateContext contextT;
	contextT.m_pCurrentDoc     = GetDocument();
	contextT.m_pNewViewClass   = pViewClass;
	contextT.m_pNewDocTemplate = GetDocument()->GetDocTemplate();

	CWnd* pWnd;
	TRY
	{
		pWnd = (CWnd*)pViewClass->CreateObject();
		if (pWnd == NULL)
		{
			AfxThrowMemoryException();
		}
	}
	CATCH_ALL(e)
	{
		TRACE0( "Out of memory creating a view.\n" );
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;

	int nTab = m_wndTabControl.GetItemCount();

	// Create with the right size (wrong position)
	CRect rect(0,0,0,0);
	if (!pWnd->Create(NULL, NULL, dwStyle,
		rect, &m_wndTabControl, (AFX_IDW_PANE_FIRST + nTab), &contextT))
	{
		TRACE0( "Warning: couldn't create client tab for view.\n" );
		// pWnd will be cleaned up by PostNcDestroy
		return NULL;
	}
	m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);

	//pWnd->SendMessage(WM_INITIALUPDATE);
	pWnd->SetOwner(this);

	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CLogSalesReportView

IMPLEMENT_DYNCREATE(CLogSalesReportView,  CXTPReportView) //CXTResizeFormView)

BEGIN_MESSAGE_MAP(CLogSalesReportView,  CXTPReportView) //CXTResizeFormView)
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_WM_COPYDATA()
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, XTP_ID_REPORT_CONTROL, OnReportItemValueClick)
	ON_NOTIFY(NM_CLICK, XTP_ID_REPORT_CONTROL, OnReportItemClick)
	ON_NOTIFY(XTP_NM_REPORT_HEADER_RCLICK, XTP_ID_REPORT_CONTROL, OnReportColumnRClick)
	ON_NOTIFY(NM_KEYDOWN, XTP_ID_REPORT_CONTROL, OnReportKeyDown)

	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	
	ON_COMMAND(ID_TBBTN_SELECTION_DLG2, OnFieldSelection)
END_MESSAGE_MAP()

CLogSalesReportView::CLogSalesReportView()
	: CXTPReportView(),
	m_bInitialized(FALSE),
	m_sLangFN(L""),
	m_sFieldSelection(L""),
	m_pDB(NULL),
	m_bPrintOutEnabled(FALSE),
	m_bExportToExcel(FALSE)

{

}

CLogSalesReportView::~CLogSalesReportView()
{
}

void CLogSalesReportView::OnDestroy()
{
	CString sRegKey = L"";
	CLogSalesFormView* pView = (CLogSalesFormView *)getFormViewByID(IDD_FORMVIEW6);
	if (pView != NULL)
	{
		//#3611, lagt in koll inga ogiltiga tecken i Ticket numret	\ / ? : * " > < |
		if(checkIllegalChars(pView->getInventoryRec().getID()) == false)
		{
			sRegKey.Format(L"%s_%d",pView->getInventoryRec().getID(),pView->getInventoryRec().getPKID());
			saveColumnsSettingToFile(sRegKey,GetReportCtrl(),COLSET_SALES);
		}
	}

	SaveReportState();

	if (m_pDB != NULL)
		delete m_pDB;

	// Try to clear records on exit (for memory deallocation); 080215 p�d
	GetReportCtrl().ResetContent();
	GetReportCtrl().GetRows()->ReserveSize(0);

	CXTPReportView::OnDestroy();	

}

void CLogSalesReportView::OnSetFocus(CWnd*)
{

	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);
/*
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
*/
}

BOOL CLogSalesReportView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
//	if( !CXTResizeFormView::PreCreateWindow(cs) )
//		return FALSE;
	if( ! CXTPReportView::PreCreateWindow(cs) )
		return FALSE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

#ifdef _DEBUG
void CLogSalesReportView::AssertValid() const
{
	CXTPReportView::AssertValid();
}

void CLogSalesReportView::Dump(CDumpContext& dc) const
{
	CXTPReportView::Dump(dc);
//	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG

BOOL CLogSalesReportView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{

	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		if (m_dbConnectionData.conn->isConnected())
		{
			m_pDB = new CDBHandling(m_dbConnectionData);
		}
	}

	return CXTPReportView::OnCopyData(pWnd, pData);
}

void CLogSalesReportView::OnInitialUpdate()
{
	CXTPReportView::OnInitialUpdate();

	if (!m_bInitialized)
	{

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		CLogSalesFrame* pWnd = (CLogSalesFrame *)getFormViewParentByID(IDD_FORMVIEW6);
		if (m_wndSubList.GetSafeHwnd() == NULL && pWnd != NULL)
		{
			m_wndSubList.SubclassDlgItem(IDC_COLUMNS2, &pWnd->m_wndFieldSelectionDlg);
			GetReportCtrl().GetColumns()->GetReportHeader()->SetSubListCtrl(&m_wndSubList);
		}

		setupReport();

		LoadReportState();
		
		m_bInitialized = TRUE;

	}
}

// Create and add Assortment settings reportwindow
BOOL CLogSalesReportView::setupReport(void)
{

	CString sColText;
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	// Setup language filename; 051214 p�d
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{

			m_sMsgCap = xml.str(IDS_STRING99);

			m_sMsgAllLogsAlreadyInInventory = xml.str(IDS_STRING3594);
			m_sMsgNoMatchForTags = xml.str(IDS_STRING3592);
			m_sOpenDlgTagFilesCaliper = xml.str(IDS_STRING3595);
			m_sOpenDlgTagFilesExcel = xml.str(IDS_STRING3596);
			m_sOpenDlgTagFilesAll = xml.str(IDS_STRING3597);

			m_sarrHeadLines.Add(L"");
			m_sarrHeadLines.Add(L"");
			m_sarrHeadLines.Add(L"");
			m_sarrHeadLines.Add(L"");
			m_sarrHeadLines.Add(L"");
			m_sarrHeadLines.Add(xml.str(IDS_STRING1744));
			m_sarrHeadLines.Add(xml.str(IDS_STRING1705));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17050));
			m_sarrHeadLines.Add(xml.str(IDS_STRING1706));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17060));
			m_sarrHeadLines.Add(xml.str(IDS_STRING1707));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17070));
			m_sarrHeadLines.Add(xml.str(IDS_STRING1708));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17080));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17090));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17091));
			m_sarrHeadLines.Add(xml.str(IDS_STRING1710));
			m_sarrHeadLines.Add(xml.str(IDS_STRING1711));
			m_sarrHeadLines.Add(xml.str(IDS_STRING1716));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17160));
			m_sarrHeadLines.Add(xml.str(IDS_STRING1719));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17190));
			m_sarrHeadLines.Add(xml.str(IDS_STRING1717));
			m_sarrHeadLines.Add(xml.str(IDS_STRING1718));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17180));
			m_sarrHeadLines.Add(xml.str(IDS_STRING1720));
			m_sarrHeadLines.Add(xml.str(IDS_STRING1721));
			m_sarrHeadLines.Add(xml.str(IDS_STRING1722));
			m_sarrHeadLines.Add(xml.str(IDS_STRING1723));

			m_sarrHeadLines.Add(L"");
			m_sarrHeadLines.Add(L"");

			m_sFieldSelection = xml.str(IDS_STRING1100);

			// Get text from languagefile; 061207 p�d
			if (GetReportCtrl().GetSafeHwnd() != NULL)
			{
				GetReportCtrl().GetReportHeader()->SetAutoColumnSizing( FALSE );
				GetReportCtrl().EnableScrollBar(SB_HORZ, TRUE );
				GetReportCtrl().EnableScrollBar(SB_VERT, TRUE );

				GetReportCtrl().ShowWindow( SW_NORMAL );
				GetReportCtrl().ShowGroupBy( TRUE );

				// Ticketnumber
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_0, xml.str(IDS_STRING20010), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->SetAlignment(DT_CENTER);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// TagNumber
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_1, xml.str(IDS_STRING1700), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->SetAlignment(DT_CENTER);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Speciescode
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_2, xml.str(IDS_STRING1701), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Speciesname
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_3, xml.str(IDS_STRING1724), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Frequency
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_4, xml.str(IDS_STRING1702), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->SetAlignment(DT_CENTER);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->SetShowInFieldChooser(FALSE);
				pCol->SetVisible(FALSE);
				// Grade
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_5, xml.str(IDS_STRING1703), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Deduction
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_6, xml.str(IDS_STRING1704), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Top-diameter on bark (DOB) 1
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_7, xml.str(IDS_STRING1705), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Top-diameter on bark (DOB) 2
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_8, xml.str(IDS_STRING17050), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				// Top-diameter inside bark (DIB) 1
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_9, xml.str(IDS_STRING1706), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				// Top-diameter inside bark (DIB) 2
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_10, xml.str(IDS_STRING17060), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;

				// Root-diameter on bark (DOB) 1
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_11, xml.str(IDS_STRING1707), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Root-diameter on bark (DOB) 2
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_12, xml.str(IDS_STRING17070), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				// Root-diameter inside bark (DIB) 1
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_13, xml.str(IDS_STRING1708), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Root-diameter inside bark (DIB) 2
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_14, xml.str(IDS_STRING17080), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Length measured
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_15, xml.str(IDS_STRING17090), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Length calculated
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_16, xml.str(IDS_STRING17091), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Bark
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_17, xml.str(IDS_STRING1710), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Volume pricelist
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_18, xml.str(IDS_STRING1711), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Value
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_19, xml.str(IDS_STRING1716), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Function used
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_20, xml.str(IDS_STRING17160), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->SetAlignment(DT_CENTER);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// User top-diamter 1
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_21, xml.str(IDS_STRING1719), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// User top-diamter 2
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_22, xml.str(IDS_STRING17190), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// User volume
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_23, xml.str(IDS_STRING1717), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// User root-diamter 1
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_24, xml.str(IDS_STRING1718), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// User root-diamter 2
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_25, xml.str(IDS_STRING17180), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// User length
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_26, xml.str(IDS_STRING1720), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// User price
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_27, xml.str(IDS_STRING1721), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Reason
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_28, xml.str(IDS_STRING1722), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Notes
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_29, xml.str(IDS_STRING1723), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				GetReportCtrl().GetReportHeader()->AllowColumnRemove(TRUE);
				GetReportCtrl().SetMultipleSelection( FALSE );
				GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );
				GetReportCtrl().SetGridStyle( FALSE, xtpReportGridSolid );
				GetReportCtrl().AllowEdit(FALSE);
				GetReportCtrl().FocusSubItems(FALSE);
				GetReportCtrl().GetPaintManager()->SetFixedRowHeight(FALSE);
				GetReportCtrl().PinFooterRows(FALSE);

			}	// if (GetReportCtrl().GetSafeHwnd() != NULL)
		}	// if (xml->Load(m_sLangFN))
		xml.clean();
	}	// if (fileExists(m_sLangFN))

	return TRUE;
}

void CLogSalesReportView::OnReportItemValueClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{
	}	// if (pItemNotify != NULL)
}

void CLogSalesReportView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
}

void CLogSalesReportView::OnReportKeyDown(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	LPNMKEY lpNMKey = (LPNMKEY)pNotifyStruct;
}


void CLogSalesReportView::OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	ASSERT(pItemNotify->pColumn);
	CPoint ptClick = pItemNotify->pt;

	CMenu menu;
	VERIFY(menu.CreatePopupMenu());

	// create main menu items
	menu.AppendMenu(MF_STRING, ID_SHOW_FIELD_SELECTION2, m_sFieldSelection);

	// track menu
	int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN |TPM_RIGHTBUTTON, ptClick.x, ptClick.y, this, NULL);

	// other general items
	switch (nMenuResult)
	{
		case ID_SHOW_FIELD_SELECTION2:
			OnFieldSelection();	
		break;
	}
	menu.DestroyMenu();
}


void CLogSalesReportView::OnPrintPreview()
{

	// In derived classes, implement special window handling here
	// Be sure to Unhook Frame Window close if hooked.

	// must not create this on the frame.  Must outlive this function
	CPrintPreviewState* pState = new CPrintPreviewState;

	// DoPrintPreview's return value does not necessarily indicate that
	// Print preview succeeded or failed, but rather what actions are necessary
	// at this point.  If DoPrintPreview returns TRUE, it means that
	// OnEndPrintPreview will be (or has already been) called and the
	// pState structure will be/has been deleted.
	// If DoPrintPreview returns FALSE, it means that OnEndPrintPreview
	// WILL NOT be called and that cleanup, including deleting pState
	// must be done here.
	if ( !DoPrintPreview( AFX_IDD_PREVIEW_TOOLBAR, this,RUNTIME_CLASS( CLangPreviewView ), pState ))
	{
		// In derived classes, reverse special window handling here for
		// Preview failure case
		TRACE0( "Error: DoPrintPreview failed.\n" );
		AfxMessageBox( AFX_IDP_COMMAND_FAILURE );
		delete pState;      // preview failed to initialize, delete State now

	}

}

void CLogSalesReportView::OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView) 
{
	// Enable print preview button on Toolbar on ending preview; 071218 p�d
	CXTPReportView::OnEndPrintPreview(pDC, pInfo, point, pView);
}

void CLogSalesReportView::doRunPrintPreview(void)
{
	OnPrintPreview();
}

void CLogSalesReportView::OnFilePrint()
{
	CXTPReportView::OnFilePrint();
}

void CLogSalesReportView::doPrintOut()
{
	OnPrintPreview();
}


void CLogSalesReportView::handleTagFile(LPCTSTR file,BOOL reset)
{
	// Start by opening the tags from shipping (EXCEL file)
	CVecLogs vecInDB,vecTagsOK,vecTagsNotOK,vecTagsNotFound;
	CVecTickets vecTickets;
	CLogs recLog = CLogs();
	CLogs	recSumLog = CLogs();
	CStringArray sarrTags;
	CString sValue = L"",sTagStr = L"",str = L"";
	double fTagNum = 0.0;
	double fAvg[11];
	double fSum[8];
	int nStatus = -1;
	short nExtLen = -1;
	Book *book = NULL;
	CLogSalesFormView *pView = (CLogSalesFormView*)getFormViewByID(IDD_FORMVIEW6);
	CString sPath(file),sFileName = L"",sBuyer = L"",sSQL = L"";
	CStrIntMap mapTags;
	CStrIntMap mapTagsDup;

	CViewProgress wndPrg;

	// Create the edit control and add it to the status bar
	if (!wndPrg.Create(CViewProgress::IDD))
	{
		TRACE0("Failed to create edit control.\n");
		return;
	}

	doEvents();

	if (m_pDB != NULL)
	{
		m_pDB->getTickets(vecTickets);
//		m_pDB->getAllLogs(vecInDB);
	}

	if (sPath.Right(4) == _T(".xls") || sPath.Right(5) == _T(".xlsx") )
	{


		if(sPath.Right(4) == _T(".xls"))
		{
			book = xlCreateBook();
			nExtLen = 4;		
		}
		else if(sPath.Right(5) == _T(".xlsx"))
		{
			book = xlCreateXMLBook();
			nExtLen = 5;		
		}

		if(book != NULL)
		{
			// Set registraton-key
			if (book->load(sPath))
			{
				book->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0hfeflb");
				Sheet *sheet = book->getSheet(0);	// First sheet
				if(sheet != NULL)
				{
					for (int i = 0;i < sheet->lastRow();i++)
					{
						if (sheet->cellType(i,0) != CELLTYPE_EMPTY && 
								sheet->cellType(i,0) != CELLTYPE_ERROR && 
								sheet->cellType(i,0) != CELLTYPE_BLANK)
						{
							if (sheet->cellType(i,0) == CELLTYPE_STRING)
							{
								sTagStr = sheet->readStr(i,0);
								str.Format(L"%s",sTagStr);
							}
							else if (sheet->cellType(i,0) == CELLTYPE_NUMBER)
							{
								fTagNum = sheet->readNum(i,0);
								str.Format(L"%.0f",fTagNum);
									
							}
							sarrTags.Add(str);
						}
					}
					sheet = NULL;
				}
				else
				{
					AfxMessageBox((LPCTSTR)book->errorMessage());
				}
			}	// if (book->load(dlg.GetPathName())
		}
		book = NULL,
		vecTagsOK.clear();
		vecTagsNotOK.clear();
		CString sInDBTagNumber = L"";
		CString sMatchTagNumber = L"";
		if (m_pDB != NULL)
		{

			if (wndPrg.GetSafeHwnd())
			{
				wndPrg.setRange(0,100);
				wndPrg.setStep();
				wndPrg.ShowWindow(SW_NORMAL);
				wndPrg.UpdateWindow();
			}

//			if (vecInDB.size() > 0 && sarrTags.GetCount() > 0)
			if (sarrTags.GetCount() > 0)
			{
				// Setup the sql-question tags
				sSQL = L"AND (";
				for (int i1 = 0;i1 < sarrTags.GetCount();i1++)
				{
					AfxExtractSubString(sMatchTagNumber,sarrTags.GetAt(i1),0,';');
					if (i1 < sarrTags.GetCount()-1)
					{
						sSQL += L" a.sTagNumber='" + sMatchTagNumber.Trim() + L"' OR ";
					}
					else
					{
						sSQL += L" a.sTagNumber='" + sMatchTagNumber.Trim() + L"' ) ";
					}
				}
				wndPrg.setPos(50);
				m_pDB->getLogsFromTagNumbers(sSQL,vecInDB);
				wndPrg.setPos(100);

				if (wndPrg.GetSafeHwnd())
				{
					wndPrg.setRange(0,sarrTags.GetCount());
					wndPrg.setStep();
					wndPrg.ShowWindow(SW_NORMAL);
				}
				
				for (int i1 = 0;i1 < sarrTags.GetCount();i1++)
				{
					AfxExtractSubString(sMatchTagNumber,sarrTags.GetAt(i1),0,';');
					mapTags[sMatchTagNumber.Trim()] = 0;
					mapTagsDup[sMatchTagNumber.Trim()] += 1;
					wndPrg.setPos(i1+1);
				}

				if (wndPrg.GetSafeHwnd())
				{
					wndPrg.setRange(0,sarrTags.GetCount());
					wndPrg.setStep();
					wndPrg.ShowWindow(SW_NORMAL);
				}

				if (wndPrg.GetSafeHwnd())
				{
					wndPrg.setRange(0,vecInDB.size());
					wndPrg.setStep();
					wndPrg.ShowWindow(SW_NORMAL);
				}

				for (UINT i2 = 0;i2 < vecInDB.size();i2++)
				{
					sInDBTagNumber = vecInDB[i2].getTagNumber().Trim();
					nStatus = vecInDB[i2].getStatusFlag();
					wndPrg.setPos(i2+1);
					doEvents();

					for (int i1 = 0;i1 < sarrTags.GetCount();i1++)
					{
						sMatchTagNumber = sarrTags.GetAt(i1);
						if (sInDBTagNumber.CompareNoCase(sMatchTagNumber.Trim()) == 0 &&
							(nStatus == LOGSTATUS::IN_STOCK || nStatus == LOGSTATUS::LOCKED))
						{
							if (mapTags[sMatchTagNumber.Trim()] == 0)
							{
								vecInDB[i2].setStatusFlag(nStatus);
								vecTagsOK.push_back(vecInDB[i2]);	
							}
							mapTags[sMatchTagNumber.Trim()] += 1; 
						}	// if (sInDBTagNumber.CompareNoCase(sMatchTagNumber.Trim()) == 0)
						else if (sInDBTagNumber.CompareNoCase(sMatchTagNumber.Trim()) == 0 &&
							nStatus != LOGSTATUS::IN_STOCK && nStatus != LOGSTATUS::LOCKED)
						{
							if (mapTags[sMatchTagNumber.Trim()] == 0)
							{
								vecTagsNotOK.push_back(vecInDB[i2]);	
							}
							mapTags[sMatchTagNumber.Trim()] += 1; 
						}	// if (sInDBTagNumber.CompareNoCase(sMatchTagNumber.Trim()) == 0)
					}	// for (UINT i2 = 0;i2 < vecInDB.size();i2++)
				}	// for (int i1 = 0;i1 < sarrTags.GetCount();i1++)
			}	// if (vecInDB.size() > 0 && sarrTags.GetCount() > 0)

		}	// if (pDB != NULL)

		m_bPrintOutEnabled = m_bExportToExcel = TRUE;
	}
	else if (sPath.Right(4) == _T(".lsb"))
	{
		nExtLen = 4;		

		// Setup sarrTags from text-file; 2011-12-06 p�d
		CStdioFile f;
		CFileException ex;

		CString sLine = L"";
		CString sToken = L"";
		CString sStr = L"";
		BOOL bStart = FALSE;
		if (!f.Open(sPath,CFile::modeRead,&ex))
		{
			TCHAR err[1024];
			ex.GetErrorMessage(err,1024);
			AfxMessageBox(err);
			return;
		}

		if (wndPrg.GetSafeHwnd())
		{
			wndPrg.setRange(0,100);
			wndPrg.setStep();
			wndPrg.ShowWindow(SW_NORMAL);
			wndPrg.setPos(50);
		}

		while (f.ReadString(sLine))
		{
			sLine.Replace(':',';');

			AfxExtractSubString(sToken,sLine,0,';');
			if (sToken.CompareNoCase(L"Name") == 0)
			{
				AfxExtractSubString(sToken,sLine,1,';');
				sStr = sToken.TrimLeft();
				if (pView != NULL)
					pView->setName(sStr.TrimRight());
			}
			if (sToken.CompareNoCase(L"Buyer") == 0)
			{
				AfxExtractSubString(sBuyer,sLine,1,';');
				sStr = sBuyer.TrimLeft();
				if (pView != NULL)
					pView->setBuyer(sStr.TrimRight());
			}
			if (sToken.CompareNoCase(L"Date") == 0)
			{
				sLine.Remove(' ');
				AfxExtractSubString(sToken,sLine,1,';');
				if (pView != NULL)
					pView->setDate(sToken.Trim());
				
			}
			if (sToken.CompareNoCase(L"Latitude") == 0)
			{
				sLine.Remove(' ');
				AfxExtractSubString(sToken,sLine,1,';');
				if (pView != NULL)
					pView->setGPSCoord1(sToken.Trim());
				
			}
			if (sToken.CompareNoCase(L"Longitude") == 0)
			{
				sLine.Remove(' ');
				AfxExtractSubString(sToken,sLine,1,';');
				if (pView != NULL)
					pView->setGPSCoord2(sToken.Trim());
				
			}
			if (sToken.CompareNoCase(L"Note") == 0)
			{
				AfxExtractSubString(sToken,sLine,1,';');
				if (pView != NULL)
					pView->setNote(sToken.Trim());
				
			}
			if (sToken.CompareNoCase(L"TypeID") == 0 || sToken.CompareNoCase(L"Type ID") == 0 )
			{
				sLine.Remove(' ');
				AfxExtractSubString(sToken,sLine,1,';');
				nStatus = _tstoi(sToken);
			}
			if (bStart && !sLine.Trim().IsEmpty())
				sarrTags.Add(sLine.Trim()+L";");
			if (sToken.CompareNoCase(L"TAG") == 0)
			{
				bStart = TRUE;
			}

		}

		wndPrg.setPos(100);

		vecTagsOK.clear();
		vecTagsNotOK.clear();
		CString sInDBTagNumber = L"";
		CString sMatchTagNumber = L"";
		if (m_pDB != NULL)
		{
//			if (vecInDB.size() > 0 && sarrTags.GetCount() > 0)
			if (sarrTags.GetCount() > 0)
			{		
				// Setup the sql-question tags
				sSQL = L"AND (";
				for (int i1 = 0;i1 < sarrTags.GetCount();i1++)
				{
					AfxExtractSubString(sMatchTagNumber,sarrTags.GetAt(i1),0,';');
					if (i1 < sarrTags.GetCount()-1)
					{
						sSQL += L" a.sTagNumber='" + sMatchTagNumber.Trim() + L"' OR ";
					}
					else
					{
						sSQL += L" a.sTagNumber='" + sMatchTagNumber.Trim() + L"' ) ";
					}
				}
				m_pDB->getLogsFromTagNumbers(sSQL,vecInDB);

				if (wndPrg.GetSafeHwnd())
				{
					wndPrg.setRange(0,sarrTags.GetCount());
					wndPrg.setStep();
					wndPrg.ShowWindow(SW_NORMAL);
				}

				for (int i1 = 0;i1 < sarrTags.GetCount();i1++)
				{
					AfxExtractSubString(sMatchTagNumber,sarrTags.GetAt(i1),0,';');
					mapTags[sMatchTagNumber.Trim()] = 0; 
					mapTagsDup[sMatchTagNumber.Trim()] += 1;
					wndPrg.setPos(i1+1);
					doEvents();
				}

				if (wndPrg.GetSafeHwnd())
				{
					wndPrg.setRange(0,vecInDB.size());
					wndPrg.setStep();
					wndPrg.ShowWindow(SW_NORMAL);
				}

				for (UINT i2 = 0;i2 < vecInDB.size();i2++)
				{
					sInDBTagNumber = vecInDB[i2].getTagNumber().Trim();
					wndPrg.setPos(i2+1);
					doEvents();
					for (int i1 = 0;i1 < sarrTags.GetCount();i1++)
					{
						AfxExtractSubString(sMatchTagNumber,sarrTags.GetAt(i1),0,';');
						if (!sMatchTagNumber.IsEmpty())
						{
							//----------------------------------------------------------------------------
							// Logs with tag-nubmers in logs-inventory and marked as 'In Stock'
							if (sInDBTagNumber.CompareNoCase(sMatchTagNumber.Trim()) == 0 &&
								(vecInDB[i2].getStatusFlag() == LOGSTATUS::IN_STOCK || vecInDB[i2].getStatusFlag() == LOGSTATUS::LOCKED))
							{
								if (mapTags[sMatchTagNumber.Trim()] == 0)
								{
									vecInDB[i2].setStatusFlag(nStatus);
									vecTagsOK.push_back(vecInDB[i2]);	
								}
								mapTags[sMatchTagNumber.Trim()] += 1; 
						
							}	// if (sInDBTagNumber.CompareNoCase(sMatchTagNumber.Trim()) == 0)
							//----------------------------------------------------------------------------
							// Logs with tag-nubmers in logs-inventory and not marked as 'In Stock'
							else if (sInDBTagNumber.CompareNoCase(sMatchTagNumber.Trim()) == 0 &&
								vecInDB[i2].getStatusFlag() != LOGSTATUS::IN_STOCK && vecInDB[i2].getStatusFlag() != LOGSTATUS::LOCKED)
							{
								if (mapTags[sMatchTagNumber.Trim()] == 0)
								{
									vecTagsNotOK.push_back(vecInDB[i2]);	
								}
								mapTags[sMatchTagNumber.Trim()] += 1; 
								
							}	// if (sInDBTagNumber.CompareNoCase(sMatchTagNumber.Trim()) == 0)
						}
						//----------------------------------------------------------------------------
					}	// for (UINT i2 = 0;i2 < vecInDB.size();i2++)
				}	// for (int i1 = 0;i1 < sarrTags.GetCount();i1++)
			}	// if (vecInDB.size() > 0 && sarrTags.GetCount() > 0)
		}	// if (pDB != NULL)

		m_bPrintOutEnabled = m_bExportToExcel = TRUE;

	}

	if (wndPrg.GetSafeHwnd())
	{
		wndPrg.ShowWindow(SW_HIDE);
		wndPrg.DestroyWindow();
	}

	int nRet = -1;
//	if (vecTagsNotOK.size() > 0) 
//	{
		int nFind = sPath.ReverseFind('\\');
		sFileName = sPath.Right(sPath.GetLength() - (nFind+1));

		CTagNoMatchDlg *pDlg = new CTagNoMatchDlg(NULL,1);
		if (pDlg != NULL)
		{
			pDlg->setLogTagsNotOK(vecTagsNotOK);
			pDlg->setLogTagsOK(vecTagsOK);
			pDlg->setLogTags(mapTags);
			pDlg->setLogTagsDup(mapTagsDup);
			pDlg->setTotalNumberOfLogs(sarrTags.GetCount());
			pDlg->setTicket(sBuyer.Trim());
			pDlg->setTicketFile(sFileName.Left(sFileName.GetLength()-nExtLen));
			nRet = pDlg->DoModal();
			delete pDlg;
		}
		if (nRet == IDOK)
		{
			if (vecTagsNotOK.size() == sarrTags.GetCount()) 
			{
				::MessageBox(GetSafeHwnd(),m_sMsgNoMatchForTags,m_sMsgCap,MB_ICONQUESTION | MB_OK);
				return;
			}
		}
		else if (nRet == IDCANCEL)
		{
			return;
		}
//	}

	if (vecTagsOK.size() == 0)
	{
		::MessageBox(GetSafeHwnd(),m_sMsgAllLogsAlreadyInInventory,m_sMsgCap,MB_ICONQUESTION | MB_OK);
		return;
	}


	if (reset)
		GetReportCtrl().ResetContent();
	GetReportCtrl().GetFooterRecords()->RemoveAll();
	RLFReader xml;
	if (xml.Load(m_sLangFN))
	{

		fAvg[0] = 0.0;
		fAvg[1] = 0.0;
		fAvg[2] = 0.0;
		fAvg[3] = 0.0;
		fAvg[4] = 0.0;
		fAvg[5] = 0.0;
		fAvg[6] = 0.0;
		fAvg[7] = 0.0;
		fAvg[8] = 0.0;
		fAvg[9] = 0.0;
		fAvg[10] = 0.0;

		fSum[0] = 0.0;
		fSum[1] = 0.0;
		fSum[2] = 0.0;
		fSum[3] = 0.0;
		fSum[4] = 0.0;
		fSum[5] = 0.0;
		fSum[6] = 0.0;
		fSum[7] = 0.0;

		for (UINT i = 0;i < vecTagsOK.size();i++)
		{
			recLog = vecTagsOK[i];
			// Find ticket based on TagNumber
			for (UINT i2 = 0;i2 < vecTickets.size();i2++)
			{
				if (recLog.getLoadID() == vecTickets[i2].getPKID())
					recLog.setTicket(vecTickets[i2].getTicket());
			}

			GetReportCtrl().AddRecord(new CLogSalesReportRec(recLog));
		}
		GetReportCtrl().Populate();
		GetReportCtrl().UpdateWindow();

		int nNumOfRows = 0;
		CXTPReportRows *pRows = GetReportCtrl().GetRows();
		if (pRows != NULL)
		{
			nNumOfRows = pRows->GetCount();
			for (int i = 0;i < pRows->GetCount();i++)
			{
				CLogSalesReportRec *pRec = (CLogSalesReportRec*)pRows->GetAt(i)->GetRecord();
				if (pRec != NULL)
				{
					recLog = pRec->getRecord();
					fAvg[0] += recLog.getDOBTop();
					fAvg[1] += recLog.getDOBTop2();
					fAvg[2] += recLog.getDIBTop();
					fAvg[3] += recLog.getDIBTop2();
					fAvg[4] += recLog.getDOBRoot();
					fAvg[5] += recLog.getDOBRoot2();
					fAvg[6] += recLog.getDIBRoot();
					fAvg[7] += recLog.getDIBRoot2();
					fAvg[8] += recLog.getLengthMeas();
					fAvg[9] += recLog.getLengthCalc();
					fAvg[10] += recLog.getBark();
					
					fSum[0] += recLog.getVolPricelist();
					fSum[1] += recLog.getLogPrice();
				}
			}
		}	// if (pRows != NULL)

		recSumLog.setDOBTop(fAvg[0] > 0.0 ? fAvg[0]/nNumOfRows:0.0);	
		recSumLog.setDOBTop2(fAvg[1] > 0.0 ? fAvg[1]/nNumOfRows:0.0);	
		recSumLog.setDIBTop(fAvg[2] > 0.0 ? fAvg[2]/nNumOfRows:0.0);
		recSumLog.setDIBTop2(fAvg[3] > 0.0 ? fAvg[3]/nNumOfRows:0.0);
		recSumLog.setDOBRoot(fAvg[4] > 0.0 ? fAvg[4]/nNumOfRows:0.0);
		recSumLog.setDOBRoot2(fAvg[5] > 0.0 ? fAvg[5]/nNumOfRows:0.0);
		recSumLog.setDIBRoot(fAvg[6] > 0.0 ? fAvg[6]/nNumOfRows:0.0);
		recSumLog.setDIBRoot2(fAvg[7] > 0.0 ? fAvg[7]/nNumOfRows:0.0);
		recSumLog.setLengthMeas(fAvg[8] > 0.0 ? fAvg[8]/nNumOfRows:0.0);
		recSumLog.setLengthCalc(fAvg[9] > 0.0 ? fAvg[9]/nNumOfRows:0.0);
		recSumLog.setBark(fAvg[10] > 0.0 ? fAvg[10]/nNumOfRows:0.0);
		recSumLog.setVolPricelist(fSum[0]);
		recSumLog.setLogPrice(fSum[1]);

		GetReportCtrl().GetFooterRecords()->Add(new CLogSalesReportRec(CLogs(),m_sarrHeadLines,true,1));
		GetReportCtrl().GetFooterRecords()->Add(new CLogSalesReportRec(recSumLog,m_sarrHeadLines,true,2));
		GetReportCtrl().ShowFooterRows(TRUE);
		
		GetReportCtrl().Populate();
		GetReportCtrl().UpdateWindow();
	}	// if (xml.Load(m_sLangFN))

}

// CLogSalesFormView message handlers
void CLogSalesReportView::openTagFile()
{

	CString  strFile = _T("*.lsb;*.xls;*.xlsx"),sPath = L"";
	//CString  strFile = _T("*.*");
	CString strFilter = L"";
	strFilter.Format(L"%s (*.lsb)|*.lsb|%s (*.xls; *.xslx)|*.xls; *.xslx|%s (*.*)|*.*|",
	m_sOpenDlgTagFilesCaliper,m_sOpenDlgTagFilesExcel,m_sOpenDlgTagFilesAll);

	sPath = regGetStr(REG_ROOT,SALE_IMPORT_TAG_DIR,DIRECTORY_KEY);
	sPath += strFile;

	CFileDialog dlg(TRUE,L"lsb",sPath,OFN_OVERWRITEPROMPT|OFN_HIDEREADONLY,strFilter);
	if(dlg.DoModal() == IDOK)
	{
		regSetStr(REG_ROOT,SALE_IMPORT_TAG_DIR,DIRECTORY_KEY,getFilePath(dlg.GetPathName()));

		handleTagFile(dlg.GetPathName(),FALSE);
	}	// if(dlg.DoModal() == IDOK)
	
}

void CLogSalesReportView::OnFieldSelection(void)
{

	CLogSalesFrame* pWnd = (CLogSalesFrame *)getFormViewParentByID(IDD_FORMVIEW6);
	if (pWnd != NULL)
	{
		BOOL bShow = !pWnd->m_wndFieldSelectionDlg.IsVisible();
		pWnd->ShowControlBar(&pWnd->m_wndFieldSelectionDlg, bShow, FALSE);
	}

}

void CLogSalesReportView::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (!AfxGetApp()->GetProfileBinary(REG_LOG_SALE_REPORT_KEY, _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		GetReportCtrl().SerializeState(ar);

	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;
}

void CLogSalesReportView::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	GetReportCtrl().SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary(REG_LOG_SALE_REPORT_KEY, _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);
}
