#pragma once

#include "resource.h"

// CTemplateInfoDlg dialog

class CTemplateInfoDlg : public CXTResizeDialog
{
	DECLARE_DYNAMIC(CTemplateInfoDlg)

		// Data members
	BOOL m_bInitialized;
	CString m_sLangFN;
	CString m_sTicketNum;

	CMyExtStatic m_lbl7_1;

	CMyExtEdit m_ed7_1;

	CButton m_btnOK;
	CButton m_btnCancel;
public:
	CTemplateInfoDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTemplateInfoDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG7 };

	void setTicketNum(LPCTSTR v)	{ m_sTicketNum = v; }
	CString getTicketNum()	{ return m_sTicketNum; }
protected:
	//{{AFX_VIRTUAL(CCorrectionDlg)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CCorrectionDlg)
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};
