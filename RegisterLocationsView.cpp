// RegisterLocationsView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "RegisterLocationsView.h"

#include "ReportClasses.h"

#include "ResLangFileReader.h"

///////////////////////////////////////////////////////////////////////////////////////////
// CRegisterLocationsDoc

IMPLEMENT_DYNCREATE(CRegisterLocationsDoc, CDocument)

BEGIN_MESSAGE_MAP(CRegisterLocationsDoc, CDocument)
	//{{AFX_MSG_MAP(CRegisterLocationsDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRegisterLocationsDoc construction/destruction

CRegisterLocationsDoc::CRegisterLocationsDoc()
{
	// TODO: add one-time construction code here

}

CRegisterLocationsDoc::~CRegisterLocationsDoc()
{
}


BOOL CRegisterLocationsDoc::OnNewDocument()
{

	// CHECK FOR LICENSE HERE!!!!! 2011-12-22 P�D
	if (!License())
	{
		return FALSE;
	}

	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CRegisterLocationsDoc serialization

void CRegisterLocationsDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CRegisterLocationsDoc diagnostics

#ifdef _DEBUG
void CRegisterLocationsDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CRegisterLocationsDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

///////////////////////////////////////////////////////////////////////////////////////////
// CRegisterLocationsFrame

IMPLEMENT_DYNCREATE(CRegisterLocationsFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CRegisterLocationsFrame, CMDIChildWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_SYSCOMMAND()
	//ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)

	ON_XTP_CREATECONTROL()
END_MESSAGE_MAP()


// CRegisterLocationsFrame construction/destruction

XTPDockingPanePaintTheme CRegisterLocationsFrame::m_themeCurrent = xtpPaneThemeOffice2003;

CRegisterLocationsFrame::CRegisterLocationsFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
	m_bInitReports = FALSE;
	m_bIsSysCommand = FALSE;
}

CRegisterLocationsFrame::~CRegisterLocationsFrame()
{
}

void CRegisterLocationsFrame::OnDestroy(void)
{
}

void CRegisterLocationsFrame::OnClose(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6507_KEY);
	SavePlacement(this, csBuf);

	if (!m_bIsSysCommand)
	{
		CRegisterLocationsView *pView = (CRegisterLocationsView*)getFormViewByID(IDD_FORMVIEW16);
		if (pView != NULL)
		{
			pView->doSave(CHECK_SAVE_TYPES::CHECK_SAVE_ON_QUIT);
		}
	}

	setNavBarButtons();
	CMDIChildWnd::OnClose();
}

void CRegisterLocationsFrame::OnSysCommand(UINT nID,LPARAM lParam)
{
	if ((nID & 0xFFF0) == SC_CLOSE)
	{
		CRegisterLocationsView *pView = (CRegisterLocationsView*)getFormViewByID(IDD_FORMVIEW16);
		if (pView != NULL)
		{
			m_bIsSysCommand = TRUE;
			if (pView->doSave(CHECK_SAVE_TYPES::CHECK_SAVE_ON_QUIT) != QUIT_TYPES::DO_NOT_QUIT)
			{

				setNavBarButtons();
				CMDIChildWnd::OnSysCommand(nID,lParam);
			}
		}

	}
	else if ((nID & 0xFFF0) != SC_CLOSE)
	{
		setNavBarButtons();
		CMDIChildWnd::OnSysCommand(nID,lParam);
	}
}

int CRegisterLocationsFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	EnableDocking(CBRS_ALIGN_ANY);

	// Create and Load toolbar; 090107 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR6);
	m_wndToolBar.EnableDocking(xtpFlagAlignTop|xtpFlagStretched);

	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			//***************************************************************************************
			// Setup commandbars and manues; 051114 p�d
			CXTPToolBar* pToolBar = &m_wndToolBar;
			if (pToolBar->IsBuiltIn())
			{
				if (pToolBar->GetType() != xtpBarTypeMenuBar)
				{
					UINT nBarID = pToolBar->GetBarID();
					pToolBar->LoadToolBar(nBarID, FALSE);
					CXTPControls *p = pToolBar->GetControls();
					// Setup icons on toolbars, using resource dll; 051208 p�d
					if (nBarID == IDR_TOOLBAR6)
					{		
						setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_NEW,xml.str(IDS_STRING1220),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(1),RES_TB_SAVE,xml.str(IDS_STRING1222),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(2),RES_TB_DEL,xml.str(IDS_STRING1221),TRUE);	//

						p->GetAt(3)->SetVisible(FALSE);
						p->GetAt(4)->SetVisible(FALSE);
						p->GetAt(5)->SetVisible(FALSE);
						p->GetAt(6)->SetVisible(FALSE);
						p->GetAt(7)->SetVisible(FALSE);
						p->GetAt(8)->SetVisible(FALSE);
					}	// if (nBarID == IDR_TOOLBAR5)
				}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
			}	// if (pToolBar->IsBuiltIn())
			//***************************************************************************************
			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

int CRegisterLocationsFrame::OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl)
{
	return TRUE;
}


LRESULT CRegisterLocationsFrame::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	return 0L;
}

BOOL CRegisterLocationsFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CRegisterLocationsFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CRegisterLocationsFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6507_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CRegisterLocationsFrame::OnSetFocus(CWnd* pWnd)
{
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,ID_DO_SOMETHING_IN_SHELL,ID_LPARAM_COMMAND2);

	setNavBarButtons();

	CMDIChildWnd::OnSetFocus(pWnd);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CRegisterLocationsFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}


// CRegisterLocationsFrame diagnostics

#ifdef _DEBUG
void CRegisterLocationsFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CRegisterLocationsFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CRegisterLocationsFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = 450;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CRegisterLocationsFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType,cx,cy);
}



// CRegisterLocationsView

IMPLEMENT_DYNCREATE(CRegisterLocationsView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CRegisterLocationsView, CXTResizeFormView)
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_WM_SIZE()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)

	ON_COMMAND_RANGE(ID_BUTTON32799,ID_BUTTON32806, OnCommand)
	ON_UPDATE_COMMAND_UI_RANGE(ID_BUTTON32799,ID_BUTTON32806, OnUpdateToolbar)
	
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, ID_REPORT_LOCATIONS, OnReportValueChanged)

END_MESSAGE_MAP()

CRegisterLocationsView::CRegisterLocationsView()
	: CXTResizeFormView(CRegisterLocationsView::IDD),
		m_bInitialized(FALSE),
		m_sLangFN(L""),
		m_pDB(NULL),
		m_nSelectedType(REGISTER_TYPES::LOCATION),
		m_bEnableToolBarBtnDelete(TRUE),
		m_bEnableToolBarBtnSave(TRUE),
		m_bOnlySave(FALSE)
{
}

CRegisterLocationsView::~CRegisterLocationsView()
{
}

void CRegisterLocationsView::OnDestroy()
{
	SaveReportState(m_repRegister);

	CXTResizeFormView::OnDestroy();
}


BOOL CRegisterLocationsView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CRegisterLocationsView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL16_1, m_lbl16_1);
	//}}AFX_DATA_MAP
}

// CRegisterLocationsView diagnostics

#ifdef _DEBUG
void CRegisterLocationsView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CRegisterLocationsView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG

// CRegisterLocationsView message handlers
void CRegisterLocationsView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitDialog();
	if (!m_bInitialized)
	{

		m_lbl16_1.SetLblFontEx(20,FW_NORMAL);
		m_lbl16_1.SetTextColor(BLUE);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupReport();

		populateReport();

		m_bInitialized = TRUE;
	}
}

BOOL CRegisterLocationsView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{

	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		if (m_dbConnectionData.conn->isConnected())
		{
			m_pDB = new CDBHandling(m_dbConnectionData);
			if (m_pDB != NULL)
			{
				m_pDB->getRegister(m_vecRegister,m_nSelectedType);
				m_pDB->getTmplTables(m_vecLogScaleTemplates);
			}
		}
	}

	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CRegisterLocationsView::OnSize(UINT nType,int cx,int cy)
{

	if (m_repRegister.GetSafeHwnd())
	{
		setResize(&m_repRegister,2,40,cx-4,cy-48);
	}

	CXTResizeFormView::OnSize(nType,cx,cy);
}

void CRegisterLocationsView::OnCommand(UINT nID)
{
	switch(nID)
	{

		case ID_BUTTON32799 : New(); break;
		case ID_BUTTON32800 :	m_bOnlySave = TRUE; Save(CHECK_SAVE_TYPES::CHECK_SAVE_NO_QUIT); m_bOnlySave = FALSE; break;
		case ID_BUTTON32801 : Delete(); break;
	};
}

void CRegisterLocationsView::OnUpdateToolbar(CCmdUI* pCmdUI)
{
	if (pCmdUI->m_nID == ID_BUTTON32800)
		pCmdUI->Enable(m_bEnableToolBarBtnSave);
	if (pCmdUI->m_nID == ID_BUTTON32801)
		pCmdUI->Enable(m_bEnableToolBarBtnDelete);
}

LRESULT CRegisterLocationsView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	if (m_pDB != NULL)
	{
		m_pDB->getTmplTables(m_vecLogScaleTemplates);
	}
	return 0L;
}

QUIT_TYPES::Q_T_RETURN CRegisterLocationsView::doSave(CHECK_SAVE_TYPES::CHECK_SAVE cs)	
{ 	
	m_bOnlySave = FALSE; 
	
	return	Save(cs);
}

void CRegisterLocationsView::setupReport()
{
	CXTPReportColumn *pCol = NULL;

	if (m_repRegister.GetSafeHwnd() == 0)
	{

		// Create the sheet1 list box.
		if (!m_repRegister.Create(this, ID_REPORT_LOCATIONS, L"Locations_report",TRUE,FALSE))
		{
			TRACE0( "Failed to create m_repRegister.\n" );
			return;
		}
	}

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{

			m_sMsgCap = xml.str(IDS_STRING99); 
			m_sMsgDelete1 = xml.str(IDS_STRING1553);
			m_sMsgDelete2 = xml.str(IDS_STRING1554);
			m_sMsgMandatoryDataMissing1 = xml.str(IDS_STRING1520) + L"\n" + xml.str(IDS_STRING1521) + L"\n" + xml.str(IDS_STRING1522);
			m_sMsgMandatoryDataMissing2 = xml.str(IDS_STRING1520) + L"\n" + xml.str(IDS_STRING1521) + L"\n" + xml.str(IDS_STRING1522) + L"\n\n" + xml.str(IDS_STRING1556);
			
			m_sMsgBuyerNameAlreadyUsed1 = xml.str(IDS_STRING15302) + L"\n\n" + xml.str(IDS_STRING1531) ;
			m_sMsgBuyerNameAlreadyUsed2 = xml.str(IDS_STRING15302) + L"\n\n" + xml.str(IDS_STRING1555) ;

			m_sMsgInTemplates1  = xml.str(IDS_STRING1558);
			m_sMsgInTemplates2  = xml.str(IDS_STRING1559);

			CString sListOfRegisterItems = xml.str(IDS_STRING200),sCap = L"";
			CStringArray sarrTokens;
			tokenizeString(sListOfRegisterItems,';',sarrTokens);
			if (sarrTokens.GetCount() > 2)
			{
				sCap.Format(L"%s %s",xml.str(IDS_STRING1557),sarrTokens[2]);
				m_lbl16_1.SetWindowTextW(sCap.MakeUpper());
			}

			m_repRegister.ShowWindow( SW_NORMAL );

			// Name/Type
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_0, xml.str(IDS_STRING1501), 200));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;
			pCol->GetEditOptions()->m_nMaxLength = 50;

			// Abbrevation
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_1, xml.str(IDS_STRING1502), 80));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;
			pCol->GetEditOptions()->m_nMaxLength = 10;	// max characters

			// ID
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_2, xml.str(IDS_STRING1503), 100));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;
			pCol->GetEditOptions()->m_nMaxLength = 20;	// max characters

			// Other info
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_3, xml.str(IDS_STRING1504), 150));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;
			pCol->GetEditOptions()->m_nMaxLength = 50;

			// Address
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_4, xml.str(IDS_STRING1505), 150));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;
			pCol->GetEditOptions()->m_nMaxLength = 50;

			// Address2
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_5, xml.str(IDS_STRING1506), 150));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;
			pCol->GetEditOptions()->m_nMaxLength = 50;

			// ZipCode
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_6, xml.str(IDS_STRING1507), 50));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;
			pCol->GetEditOptions()->m_nMaxLength = 20;

			// City
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_7, xml.str(IDS_STRING1508), 80));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;
			pCol->GetEditOptions()->m_nMaxLength = 50;

			// Phone
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_8, xml.str(IDS_STRING1509), 100));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;
			pCol->GetEditOptions()->m_nMaxLength = 30;

			// Cellphone
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_9, xml.str(IDS_STRING1510), 120));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;
			pCol->GetEditOptions()->m_nMaxLength = 30;

			// WWW
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_10, xml.str(IDS_STRING1511), 150));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;
			pCol->GetEditOptions()->m_nMaxLength = 50;

			m_repRegister.GetReportHeader()->AllowColumnRemove(FALSE);
			m_repRegister.SetMultipleSelection( FALSE );
			m_repRegister.SetGridStyle( TRUE, xtpReportGridSolid );
			m_repRegister.SetGridStyle( FALSE, xtpReportGridSmallDots );
			m_repRegister.FocusSubItems(TRUE);
			m_repRegister.AllowEdit(TRUE);
			m_repRegister.GetPaintManager()->SetFixedRowHeight(FALSE);

			CRect rect;
			GetClientRect(&rect);
			setResize(&m_repRegister,2,40,rect.right-4,rect.bottom-48);

			xml.clean();
		}
	}
}

void CRegisterLocationsView::OnReportValueChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{	
		CXTPReportRow *pRow =	pItemNotify->pRow;
		CXTPReportColumn *pCol =	pItemNotify->pColumn;
		if (pRow != NULL && pCol != NULL)
		{
			int m_nColumnChanged = pCol->GetIndex();
			CRegisterReportRec *pRec = (CRegisterReportRec*)pItemNotify->pRow->GetRecord();
			if (pRec != NULL)
			{
				// Check Buyer. No dupltes allowed
				if (m_nColumnChanged == COLUMN_0)
				{
					if (isNameOK(pRow->GetIndex(),pRec->getColText(COLUMN_0)) == QUIT_TYPES::NO_SAVE)
					{
						pItemNotify->pItem->SetTextColor(RED);
						pRec->setColText(COLUMN_0,L"");
						m_bEnableToolBarBtnSave = FALSE;
					}
					else
					{
						pItemNotify->pItem->SetTextColor(BLACK);
						m_bEnableToolBarBtnSave = TRUE;
					}
				}
				else
				{
					pItemNotify->pItem->SetTextColor(BLACK);
					m_bEnableToolBarBtnSave = TRUE;
				}

			}
		}	// if (pRow != NULL && pCol != NULL)
	}

}

void CRegisterLocationsView::populateReport()
{

	m_repRegister.ResetContent();
	if (m_vecRegister.size() > 0)
	{
		for (UINT i = 0;i < m_vecRegister.size();i++)
		{
			m_repRegister.AddRecord(new CRegisterReportRec(m_vecRegister[i]));
		}
		m_repRegister.Populate();
		m_repRegister.UpdateWindow();
	}

}
// Check only name, for now
QUIT_TYPES::Q_T_RETURN CRegisterLocationsView::isNameOK(int row,LPCTSTR name)
{
	QUIT_TYPES::Q_T_RETURN rt = QUIT_TYPES::DO_SAVE;
	CXTPReportRows *pRows = m_repRegister.GetRows();
	if (pRows != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			if (i != row)
			{
				CRegisterReportRec *pRec = (CRegisterReportRec*)pRows->GetAt(i)->GetRecord();
				if (pRec != NULL)
				{
					if (pRec->getColText(COLUMN_0).CompareNoCase(name) == 0)
					{
						::MessageBox(GetSafeHwnd(),m_sMsgBuyerNameAlreadyUsed1,m_sMsgCap,MB_ICONSTOP | MB_OK);
						rt = QUIT_TYPES::NO_SAVE;
						break;
					}
				}
			}
		}
	}

	return rt;
}

QUIT_TYPES::Q_T_RETURN CRegisterLocationsView::checkDataBeforeSave(CRegisterReportRec *rec,CHECK_SAVE_TYPES::CHECK_SAVE cs)
{
	CXTPReportRow *pRow = m_repRegister.GetFocusedRow();
	if (rec != NULL)
	{

		if (cs == CHECK_SAVE_TYPES::CHECK_SAVE_ON_QUIT)
		{

			//if (m_vecRegister.size() == 0)
			//	return QUIT_TYPES::QUIT_ANYWAY;	// Nothing to save

			if (rec->getColText(COLUMN_0).IsEmpty() || !m_bEnableToolBarBtnSave)
			{
					if (rec->GetIndex() == m_repRegister.GetRows()->GetCount()-1)
						return QUIT_TYPES::QUIT_ANYWAY;
		
					if (::MessageBox(this->GetSafeHwnd(),m_sMsgMandatoryDataMissing2,m_sMsgCap,MB_ICONSTOP | MB_YESNO | MB_DEFBUTTON2) == IDNO)
						return QUIT_TYPES::DO_NOT_QUIT;
					else
						return QUIT_TYPES::QUIT_ANYWAY;
			}
			else
			{
				return QUIT_TYPES::DO_SAVE;
			}

		}
		else if (cs == CHECK_SAVE_TYPES::CHECK_SAVE_NO_QUIT)
		{
			//if (m_vecUserVolTables.size() == 0)
		//	return QUIT_TYPES::Q_T_RETURN::NO_SAVE;

			if (rec->getColText(COLUMN_0).IsEmpty())
			{
				//::MessageBox(GetSafeHwnd(),m_sMsgMandatoryDataMissing1,m_sMsgCap,MB_ICONSTOP | MB_OK);
				return QUIT_TYPES::NO_SAVE;
			}
			else
			{
				return QUIT_TYPES::DO_SAVE;
			}

		}
	}
	return QUIT_TYPES::NO_SAVE;
}


void CRegisterLocationsView::New()
{
	// Save before adding any data
	Save(CHECK_SAVE_TYPES::CHECK_SAVE_NO_QUIT);
	// Add a record to list
	m_repRegister.AddRecord(new CRegisterReportRec());
	m_repRegister.Populate();
	m_repRegister.UpdateWindow();

	m_repRegister.SetFocusedRow(m_repRegister.GetRows()->GetAt(m_repRegister.GetRows()->GetCount()-1));
	m_repRegister.SetFocusedColumn(m_repRegister.GetColumns()->GetAt(COLUMN_0));
}

void CRegisterLocationsView::Delete()
{
	CXTPReportRow *pRow = m_repRegister.GetFocusedRow();
	CRegisterReportRec *rec = NULL;
	CString sMsg = L"";
	BOOL bDelRegItemInDB = TRUE;
	CRegister recReg = CRegister();
	if (m_pDB != NULL && pRow != NULL)
	{
		// We'll also add this new item to DB
		rec = (CRegisterReportRec*)pRow->GetRecord();
		if (rec != NULL)
		{
			recReg = rec->getRecord();
			bDelRegItemInDB = (recReg.getPKID() > -1);
			if (isIncludedInTemplate(m_nSelectedType,recReg.getPKID(),m_vecLogScaleTemplates))
			{
				sMsg.Format(m_sMsgInTemplates1,recReg.getName());
				sMsg += L"\n" + m_sMsgInTemplates2;
				::MessageBox(GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONSTOP | MB_OK);
				return;
			}	// if (m_vecLogScaleTemplates.size() > 0)

			sMsg.Format(m_sMsgDelete1,rec->getColText(COLUMN_0));
			// Ask user twice, if he realy want's to delete
			if (::MessageBox(GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
			{
				return;
			}
#ifdef _ASK_TWICE_ON_DELETE_REGISTER
			else	if (::MessageBox(GetSafeHwnd(),m_sMsgDelete2,m_sMsgCap,MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
			{
				return;
			}
#endif
			// Save before deleting any data
			Save(CHECK_SAVE_TYPES::CHECK_SAVE_NO_QUIT);
			if (bDelRegItemInDB)
			{
				if (m_pDB->delRegisterItem(recReg))
				{
					m_pDB->getRegister(m_vecRegister,m_nSelectedType);
					populateReport();
					if (m_vecRegister.size() == 0)
						m_pDB->resetRegisterIdentityField();
					// We'll check if Template is open. If so we'll update
					// the combobox holding data for this register item.
					msgToModuleWindowOpen(L"Module6501",m_nSelectedType);
				}
			}
		}
	}
}

QUIT_TYPES::Q_T_RETURN CRegisterLocationsView::Save(CHECK_SAVE_TYPES::CHECK_SAVE cs)
{
	// We'll save all items for this type
	QUIT_TYPES::Q_T_RETURN cst = QUIT_TYPES::NO_SAVE;
	CXTPReportRecords *pRecs = m_repRegister.GetRecords();
	CRegisterReportRec *rec = NULL;
	if (m_pDB != NULL && pRecs != NULL)
	{
		m_repRegister.Populate();
		for (int i = 0;i < pRecs->GetCount();i++)
		{
			rec = (CRegisterReportRec*)pRecs->GetAt(i);
			if ((cst = checkDataBeforeSave(rec,cs)) != QUIT_TYPES::DO_SAVE)
			{
				m_pDB->getRegister(m_vecRegister,m_nSelectedType);
		
				populateReport();
				return cst;
			}
				if (rec->getRecord().getPKID() == -1)
				{
					m_pDB->newRegisterItem(CRegister(-1,
																				m_nSelectedType,
																				rec->getColText(COLUMN_0),
																				rec->getColText(COLUMN_1),
																				rec->getColText(COLUMN_2),
																				rec->getColText(COLUMN_3),
																				rec->getColText(COLUMN_4),
																				rec->getColText(COLUMN_5),
																				rec->getColText(COLUMN_6),
																				rec->getColText(COLUMN_7),
																				rec->getColText(COLUMN_8),
																				rec->getColText(COLUMN_9),
																				rec->getColText(COLUMN_10)));
				}
				else if (rec->getRecord().getPKID() > 0)
				{
					m_pDB->updRegisterItem(CRegister(rec->getRecord().getPKID(),
																		 m_nSelectedType,
																		 rec->getColText(COLUMN_0),
																		 rec->getColText(COLUMN_1),
																		 rec->getColText(COLUMN_2),
																		 rec->getColText(COLUMN_3),
																		 rec->getColText(COLUMN_4),
																		 rec->getColText(COLUMN_5),
																		 rec->getColText(COLUMN_6),
																		 rec->getColText(COLUMN_7),
																		 rec->getColText(COLUMN_8),
																		 rec->getColText(COLUMN_9),
																		 rec->getColText(COLUMN_10)));
				}
			}

		m_pDB->getRegister(m_vecRegister,m_nSelectedType);
		
		populateReport();

		// We'll check if Template is open. If so we'll update
		// the combobox holding data for this register item.
		msgToModuleWindowOpen(L"Module6501",m_nSelectedType);


	}
	return cst;
}
