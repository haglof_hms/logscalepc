// SpeciesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SpeciesDlg.h"

#include "ResLangFileReader.h"

#include "reportclasses.h"

///////////////////////////////////////////////////////////////////////////////////////////
// CSpeciesDlgDoc


IMPLEMENT_DYNCREATE(CSpeciesDlgDoc, CDocument)

BEGIN_MESSAGE_MAP(CSpeciesDlgDoc, CDocument)
	//{{AFX_MSG_MAP(CSpeciesDlgDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSpeciesDlgDoc construction/destruction

CSpeciesDlgDoc::CSpeciesDlgDoc()
{
	// TODO: add one-time construction code here

}

CSpeciesDlgDoc::~CSpeciesDlgDoc()
{
}


BOOL CSpeciesDlgDoc::OnNewDocument()
{

	// CHECK FOR LICENSE HERE!!!!! 2011-12-22 P�D
	if (!License())
	{
		return FALSE;
	}

	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CSpeciesDlgDoc serialization

void CSpeciesDlgDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CSpeciesDlgDoc diagnostics

#ifdef _DEBUG
void CSpeciesDlgDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CSpeciesDlgDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

///////////////////////////////////////////////////////////////////////////////////////////
// CSpeciesDlgFrame

IMPLEMENT_DYNCREATE(CSpeciesDlgFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CSpeciesDlgFrame, CMDIChildWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_SYSCOMMAND()
	//ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)

	ON_XTP_CREATECONTROL()
END_MESSAGE_MAP()


// CSpeciesDlgFrame construction/destruction

XTPDockingPanePaintTheme CSpeciesDlgFrame::m_themeCurrent = xtpPaneThemeOffice2003;

CSpeciesDlgFrame::CSpeciesDlgFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
	m_bInitReports = FALSE;
	m_bIsPrintOutTBtn = FALSE;
	m_bSysCommand = FALSE;
}

CSpeciesDlgFrame::~CSpeciesDlgFrame()
{
}

void CSpeciesDlgFrame::OnDestroy(void)
{
}

void CSpeciesDlgFrame::OnClose(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6503_KEY);
	SavePlacement(this, csBuf);

	if (!m_bSysCommand)
	{
		CSpeciesDlgView *pView = (CSpeciesDlgView*)getFormViewByID(IDD_FORMVIEW12);
		if (pView != NULL)
		{
			if (pView->doSave(FALSE))
			{
				setNavBarButtons();
				CMDIChildWnd::OnClose();
			}
		}
	}
	else
	{
		setNavBarButtons();
		CMDIChildWnd::OnClose();
	}
}

void CSpeciesDlgFrame::OnSysCommand(UINT nID,LPARAM lParam)
{
	if ((nID & 0xFFF0) == SC_CLOSE)
	{
		m_bSysCommand = TRUE;
		CSpeciesDlgView *pView = (CSpeciesDlgView*)getFormViewByID(IDD_FORMVIEW12);
		if (pView != NULL)
		{
			if (pView->doSave(FALSE))
			{
				setNavBarButtons();
				CMDIChildWnd::OnSysCommand(nID,lParam);
			}
		}
	}
	else if ((nID & 0xFFF0) != SC_CLOSE)
	{
		setNavBarButtons();
		CMDIChildWnd::OnSysCommand(nID,lParam);
	}
}

int CSpeciesDlgFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	EnableDocking(CBRS_ALIGN_ANY);

	// Create and Load toolbar; 090107 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR6);
	m_wndToolBar.EnableDocking(xtpFlagAlignTop|xtpFlagStretched);

	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			//***************************************************************************************
			// Setup commandbars and manues; 051114 p�d
			CXTPToolBar* pToolBar = &m_wndToolBar;
			if (pToolBar->IsBuiltIn())
			{
				if (pToolBar->GetType() != xtpBarTypeMenuBar)
				{
					UINT nBarID = pToolBar->GetBarID();
					pToolBar->LoadToolBar(nBarID, FALSE);
					CXTPControls *p = pToolBar->GetControls();
					// Setup icons on toolbars, using resource dll; 051208 p�d
					if (nBarID == IDR_TOOLBAR6)
					{		
						setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_NEW,xml.str(IDS_STRING1220),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(1),RES_TB_SAVE,xml.str(IDS_STRING1222),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(2),RES_TB_DEL,xml.str(IDS_STRING1221),TRUE);	//

						p->GetAt(3)->SetVisible(FALSE);
						p->GetAt(4)->SetVisible(FALSE);
						p->GetAt(5)->SetVisible(FALSE);
						p->GetAt(6)->SetVisible(FALSE);
						p->GetAt(7)->SetVisible(FALSE);
						p->GetAt(8)->SetVisible(FALSE);
					}	// if (nBarID == IDR_TOOLBAR5)
				}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
			}	// if (pToolBar->IsBuiltIn())
			//***************************************************************************************
			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))


	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

int CSpeciesDlgFrame::OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl)
{
	return TRUE;
}


LRESULT CSpeciesDlgFrame::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	return 0L;
}

BOOL CSpeciesDlgFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CSpeciesDlgFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CSpeciesDlgFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6503_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CSpeciesDlgFrame::OnSetFocus(CWnd* pWnd)
{
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,ID_DO_SOMETHING_IN_SHELL,ID_LPARAM_COMMAND2);

	setNavBarButtons();

	CMDIChildWnd::OnSetFocus(pWnd);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CSpeciesDlgFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}


// CSpeciesDlgFrame diagnostics

#ifdef _DEBUG
void CSpeciesDlgFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CSpeciesDlgFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CSpeciesDlgFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = 450;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CSpeciesDlgFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType,cx,cy);
}


// CSpeciesDlgView dialog

//IMPLEMENT_DYNAMIC(CSpeciesDlgView, CXTResizeFormView)
IMPLEMENT_DYNCREATE(CSpeciesDlgView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CSpeciesDlgView, CXTResizeFormView)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_WM_SIZE()
	ON_COMMAND_RANGE(ID_BUTTON32799,ID_BUTTON32806, OnCommand)
	ON_UPDATE_COMMAND_UI_RANGE(ID_BUTTON32799,ID_BUTTON32806, OnUpdateToolbar)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, ID_REPORT_SPECIES, OnReportValueChanged)
	//ON_NOTIFY(NM_CLICK, ID_REPORT_SPECIES, OnReportItemClick)
END_MESSAGE_MAP()

CSpeciesDlgView::CSpeciesDlgView(CWnd* pParent /*=NULL*/)
	: CXTResizeFormView(CSpeciesDlgView::IDD),
		m_bInitialized(FALSE),
		m_sLangFN(L""),
		m_pDB(NULL),
		m_bEnableAdd(TRUE),
		m_bEnableDelete(TRUE),
		m_bEnableSave(TRUE),
		m_bOkToSave(TRUE)
{

}

CSpeciesDlgView::~CSpeciesDlgView()
{
}

void CSpeciesDlgView::OnDestroy()
{
	CXTResizeFormView::OnDestroy();
}

BOOL CSpeciesDlgView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CSpeciesDlgView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	//}}AFX_DATA_MAP
}

int CSpeciesDlgView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}

void CSpeciesDlgView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();
	if (!m_bInitialized)
	{

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupReport();

		populateReport();

		m_bInitialized = TRUE;
	}
}

BOOL CSpeciesDlgView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{

	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		if (m_dbConnectionData.conn->isConnected())
		{
			m_pDB = new CDBHandling(m_dbConnectionData);

			if (m_pDB != NULL)
			{
				m_pDB->getPricelist(m_vecVecPricelist);
				m_pDB->getUserVolTables(m_vecUserVolTables);
			}
		}
	}

	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CSpeciesDlgView::OnUpdateToolbar(CCmdUI* pCmdUI)
{
	if (pCmdUI->m_nID == ID_BUTTON32799)
		pCmdUI->Enable(m_bEnableAdd);
	if (pCmdUI->m_nID == ID_BUTTON32800)
		pCmdUI->Enable(m_bEnableDelete);
	if (pCmdUI->m_nID == ID_BUTTON32801)
		pCmdUI->Enable(m_bEnableSave);
}

void CSpeciesDlgView::OnCommand(UINT nID)
{
	switch(nID)
	{
		case ID_BUTTON32799 : Add(); break;
		case ID_BUTTON32800 : Save(TRUE,TRUE); break;
		case ID_BUTTON32801 : Delete(); break;
	};
}

void CSpeciesDlgView::OnSize(UINT nType,int cx,int cy)
{
	if (m_repSpecies.GetSafeHwnd())
	{
		setResize(&m_repSpecies,4,4,430,cy-8);
	}

	CXTResizeFormView::OnSize(nType,cx,cy);
}

BOOL CSpeciesDlgView::CheckDuplicate(LPCTSTR spc_code,int row)
{
	CSpeciesReportRec *pRec = NULL;
	CXTPReportRows *pRows = m_repSpecies.GetRows();
	if (pRows != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CSpeciesReportRec *)pRows->GetAt(i)->GetRecord();
			if (i != row && pRec->getColText(COLUMN_0).CompareNoCase(spc_code) == 0)
			{
				return TRUE;
			}
		}
	}
	return FALSE;
}


void CSpeciesDlgView::OnReportItemClick(NMHDR*  pNotifyStruct, LRESULT* /*result*/)
{
	CSpeciesReportRec *pRec = NULL;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{	
		if (pItemNotify->pRow != NULL)
		{
			m_repSpecies.Populate();
			if (pItemNotify->pColumn->GetIndex() == COLUMN_2)
			{
				if ((pRec = (CSpeciesReportRec*)pItemNotify->pRow->GetRecord()) != NULL)
				{
					if (pRec->getColFloat(COLUMN_2) < 0.0)
					{
						::MessageBox(GetSafeHwnd(),m_sMsgPosetiveValue,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);

						pRec->setColFloat(COLUMN_2,abs(pRec->getColFloat(COLUMN_2)));
						pRec->setIsValid(false);
						m_bOkToSave = FALSE;
					}
					else
					{
						m_bOkToSave = TRUE;
						pRec->setIsValid(true);
						pRec->setColFloat(COLUMN_3,pRec->getColFloat(COLUMN_2)*Inch2MM);

					}
				}			
			}
			else if (pItemNotify->pColumn->GetIndex() == COLUMN_3)
			{
				if ((pRec = (CSpeciesReportRec*)pItemNotify->pRow->GetRecord()) != NULL)
				{
					if (pRec->getColFloat(COLUMN_3) < 0.0)
					{
						::MessageBox(GetSafeHwnd(),m_sMsgPosetiveValue,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);

						pRec->setColFloat(COLUMN_3,abs(pRec->getColFloat(COLUMN_3)));

						m_bOkToSave = FALSE;
						pRec->setIsValid(false);

					}
					else
					{
						m_bOkToSave = TRUE;
						pRec->setIsValid(true);
						pRec->setColFloat(COLUMN_2,pRec->getColFloat(COLUMN_3)*MM2Inch);
					}
				}			
			}
		}
	}
}


void CSpeciesDlgView::OnReportValueChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/)
{
	int nIdx = -1;
	CSpeciesReportRec *pRec = NULL;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL)
	{	
		if (pItemNotify->pRow != NULL)
		{
			nIdx = pItemNotify->pRow->GetIndex();
			m_repSpecies.Populate();
			if (pItemNotify->pColumn->GetIndex() == COLUMN_0)
			{		
				if ((pRec = (CSpeciesReportRec*)pItemNotify->pRow->GetRecord()) != NULL)
				{
					if (CheckDuplicate(pRec->getColText(COLUMN_0),nIdx))
					{
						pItemNotify->pItem->SetTextColor(RED);
						::MessageBox(GetSafeHwnd(),m_sMsgSpecies2,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
						pRec->setIsValid(false);
						m_bOkToSave = FALSE;
					}
					else
					{
						pItemNotify->pItem->SetTextColor(BLACK);
						pRec->setIsValid(true);
						m_bOkToSave = TRUE;
					}
				}
				m_repSpecies.Populate();
				m_repSpecies.UpdateWindow();
			}
			else if (pItemNotify->pColumn->GetIndex() == COLUMN_2)
			{
				if ((pRec = (CSpeciesReportRec*)pItemNotify->pRow->GetRecord()) != NULL)
				{
					if (pRec->getColFloat(COLUMN_2) < 0.0)
					{
						::MessageBox(GetSafeHwnd(),m_sMsgPosetiveValue,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);

						pRec->setColFloat(COLUMN_2,abs(pRec->getColFloat(COLUMN_2)));
						pRec->setIsValid(false);
						m_bOkToSave = FALSE;
					}
					else
					{
						m_bOkToSave = TRUE;
						pRec->setIsValid(true);
						pRec->setColFloat(COLUMN_3,pRec->getColFloat(COLUMN_2)*Inch2MM);

					}
				}			
			}
			else if (pItemNotify->pColumn->GetIndex() == COLUMN_3)
			{
				if ((pRec = (CSpeciesReportRec*)pItemNotify->pRow->GetRecord()) != NULL)
				{
					if (pRec->getColFloat(COLUMN_3) < 0.0)
					{
						::MessageBox(GetSafeHwnd(),m_sMsgPosetiveValue,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);

						pRec->setColFloat(COLUMN_3,abs(pRec->getColFloat(COLUMN_3)));

						m_bOkToSave = FALSE;
						pRec->setIsValid(false);

					}
					else
					{
						m_bOkToSave = TRUE;
						pRec->setIsValid(true);
						pRec->setColFloat(COLUMN_2,pRec->getColFloat(COLUMN_3)*MM2Inch);
					}
				}			
			}
		}
	}
}


// CSpeciesDlgView message handlers

BOOL CSpeciesDlgView::isSpcCodeUnique(LPCTSTR spc_code)
{
	if (m_pDB != NULL)
	{
		return (!m_pDB->isSpeciesCodeInDB(spc_code));
	}

	return FALSE;
}

void CSpeciesDlgView::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;
	if (m_repSpecies.GetSafeHwnd() == 0)
	{

		// Create the sheet1 list box.
		if (!m_repSpecies.Create(this, ID_REPORT_SPECIES, L"Species_report",TRUE,FALSE))
		{
			TRACE0( "Failed to create m_repSpecies.\n" );
			return;
		}
	}

	if (m_repSpecies.GetSafeHwnd() != 0)
	{

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				pCol = m_repSpecies.AddColumn(new CXTPReportColumn(COLUMN_0, xml.str(IDS_STRING1201), 80,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_nMaxLength = 6;

				pCol = m_repSpecies.AddColumn(new CXTPReportColumn(COLUMN_1, xml.str(IDS_STRING1202), 150,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_nMaxLength = 50;

				pCol = m_repSpecies.AddColumn(new CXTPReportColumn(COLUMN_2, xml.str(IDS_STRING1207), 80,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;

				pCol = m_repSpecies.AddColumn(new CXTPReportColumn(COLUMN_3, xml.str(IDS_STRING1208), 80,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;

				m_repSpecies.GetReportHeader()->AllowColumnRemove(FALSE);
				m_repSpecies.SetMultipleSelection( FALSE );
				m_repSpecies.SetGridStyle( TRUE, xtpReportGridSolid );
				m_repSpecies.SetGridStyle( FALSE, xtpReportGridSmallDots );
				m_repSpecies.FocusSubItems(TRUE);
				m_repSpecies.AllowEdit(TRUE);
				m_repSpecies.GetPaintManager()->SetFixedRowHeight(FALSE);

				CRect rect;
				GetClientRect(&rect);
				setResize(&m_repSpecies,4,4,430,rect.bottom-8);

				// Strings
				m_sMsgCap = xml.str(IDS_STRING99);
				m_sMsgDelete1 = xml.str(IDS_STRING1203) + L"\n" + xml.str(IDS_STRING12030);
				m_sMsgDelete2 = xml.str(IDS_STRING1204);

				m_sMsgSpecies1.Format(L"%s\n\n%s\n\n",xml.str(IDS_STRING1205),xml.str(IDS_STRING1206));
				m_sMsgSpecies2.Format(L"%s\n\n%s\n\n",xml.str(IDS_STRING1205),xml.str(IDS_STRING12060));

				m_sMsgNoDelete.Format(L"%s\n%s\n\n",xml.str(IDS_STRING1230),xml.str(IDS_STRING1231));

				m_sMsgPosetiveValue = xml.str(IDS_STRING1232);

				xml.clean();

			}
		}
	}
}

void CSpeciesDlgView::populateReport()
{
	// We'll also add this new item to DB
	if (m_pDB != NULL)
	{
		m_repSpecies.ResetContent();
		m_pDB->getSpecies(m_vecSpecies);
		if (m_vecSpecies.size() > 0)
		{
			for (UINT i = 0;i < m_vecSpecies.size();i++)
			{
				m_repSpecies.AddRecord(new CSpeciesReportRec(m_vecSpecies[i],true));
			}
		}
		m_repSpecies.Populate();
		m_repSpecies.UpdateWindow();
		m_repSpecies.SetFocus();
	}
	m_bEnableAdd = (m_repSpecies.GetRows()->GetCount() < MAX_NUMBER_OF_SPECIES);
}

void CSpeciesDlgView::Add()
{
	Save(false,true);
	CXTPReportRecord *pRec = m_repSpecies.AddRecord(new CSpeciesReportRec(CSpecies(),true));
	m_repSpecies.Populate();
	m_repSpecies.UpdateWindow();

	// Goto last record
	m_repSpecies.SetFocusedRow(m_repSpecies.GetRows()->GetAt(m_repSpecies.GetRows()->GetCount()-1));
	m_repSpecies.SetFocusedColumn(m_repSpecies.GetColumns()->GetAt(COLUMN_0));

	m_bEnableAdd = (m_repSpecies.GetRows()->GetCount() < MAX_NUMBER_OF_SPECIES);
}

void CSpeciesDlgView::Delete()
{
	BOOL bSaveThis = TRUE,bIsUsed = FALSE;
	CString sMsg = L"";
	CSpeciesReportRec *rec = NULL;
	CXTPReportRow *pRow = m_repSpecies.GetFocusedRow();
	if (m_pDB != NULL && pRow != NULL)
	{
		rec = (CSpeciesReportRec*)pRow->GetRecord();
		if (rec != NULL)
		{
			if (rec->getRecord().getSpcID() > -1)
			{
				// Check if selected species is used in any pricelist or userfunction
				// Tell user that species cant be deleted
				if (m_vecVecPricelist.size() > 0)
				{
					for (UINT i = 0;i < m_vecVecPricelist.size();i++)
					{
						if (getIsSpeciesInTable(m_vecVecPricelist[i].getPricelist(),rec->getRecord().getSpcID()))
						{
							bIsUsed = TRUE;
							break;
						}
					}
				}
				
				if (m_vecUserVolTables.size() > 0 && !bIsUsed)
				{
					for (UINT i = 0;i < m_vecUserVolTables.size();i++)
					{
						if (getIsSpeciesInTable(m_vecUserVolTables[i].getVolTable(),rec->getRecord().getSpcID()))
						{
							bIsUsed = TRUE;
							break;
						}
					}
				}
				if (bIsUsed)
				{
					::MessageBox(GetSafeHwnd(),m_sMsgNoDelete,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
					return;
				}

				bSaveThis = (!rec->getColText(COLUMN_0).IsEmpty());
				if (rec->getColText(COLUMN_1).IsEmpty())
				{
					sMsg.Format(m_sMsgDelete1,rec->getColText(COLUMN_0),rec->getColText(COLUMN_0));
				}
				else
				{
					sMsg.Format(m_sMsgDelete1,rec->getColText(COLUMN_0) + L"  " + rec->getColText(COLUMN_1),rec->getColText(COLUMN_0) + L"  " + rec->getColText(COLUMN_1));
				}
				// Ask user twice, if he realy want's to delete
				if (::MessageBox(GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
				{
					return;
				}
	#ifdef _ASK_TWICE_ON_DELETE_SPC_GRADE

				else	if (::MessageBox(GetSafeHwnd(),m_sMsgDelete2,m_sMsgCap,MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
				{
					return;
				}
	#endif
				// Save before deleting any data
				Save(TRUE,FALSE);

				if (bSaveThis)
				{
					rec = (CSpeciesReportRec*)pRow->GetRecord();
					if (rec != NULL)
					{
						if (m_pDB->delSpecies(rec->getRecord()))
						{
							populateReport();
							// Check if there's no species left. If so, reset IDENTITY-field
							if (m_vecSpecies.size() == 0)
								m_pDB->resetSpeciesIdentityField();

							m_bEnableAdd = (m_repSpecies.GetRows()->GetCount() < MAX_NUMBER_OF_SPECIES);
						}	// if (m_pDB->delSpecies(rec->getRecord()))
					}
				}
			}
			else
			{
//				rec->RemoveAll();
				m_repSpecies.RemoveRecordEx(rec);
				m_repSpecies.RemoveRowEx(pRow);
				m_repSpecies.Populate();
				m_repSpecies.UpdateWindow();
			}
		}
	}
}


BOOL CSpeciesDlgView::doSave(BOOL populate)	
{ 
	return Save(populate,TRUE); 
}

BOOL CSpeciesDlgView::Save(BOOL populate,BOOL do_check)
{
	if (do_check)
	{
		if (!Check())
			return FALSE;
	}

	CString S;
	CXTPReportRecords *pRecs = NULL;
	CSpeciesReportRec *rec = NULL;
	
	// We'll also add this new item to DB
	if (m_pDB != NULL)
	{
		m_repSpecies.Populate();
		if ((pRecs = m_repSpecies.GetRecords()) != NULL)
		{
			for (int i = 0;i < pRecs->GetCount();i++)
			{
				rec = (CSpeciesReportRec*)pRecs->GetAt(i);
				if (rec->getRecord().getSpcID() == -1)
				{
					if (!rec->getColText(COLUMN_0).IsEmpty())
					{
						m_pDB->newSpecies(CSpecies(-1,rec->getColText(COLUMN_0),rec->getColText(COLUMN_1),rec->getColFloat(COLUMN_2),rec->getColFloat(COLUMN_3)));
					}
				}
				else if (rec->getRecord().getSpcID() > 0)
				{
					if (!rec->getColText(COLUMN_0).IsEmpty())
					{
						m_pDB->updSpecies(CSpecies(rec->getRecord().getSpcID(),rec->getColText(COLUMN_0),rec->getColText(COLUMN_1),rec->getColFloat(COLUMN_2),rec->getColFloat(COLUMN_3)));
					}
				}		
			}
		}
		populateReport();
	}
	return TRUE;

}

BOOL CSpeciesDlgView::Check()
{
	CXTPReportRecords *pRecs = NULL;
	CSpeciesReportRec *rec = NULL;
	m_repSpecies.Populate();
	pRecs = m_repSpecies.GetRecords();

	if (m_pDB != NULL)
	{
		if (!m_pDB->isSpeciesUnique()) 
		{
			::MessageBox(GetSafeHwnd(),m_sMsgSpecies2,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
			return FALSE;		
		}
	}
	
	if (pRecs != NULL)
	{
		for (int i = 0;i < pRecs->GetCount();i++)
		{
			rec = (CSpeciesReportRec*)pRecs->GetAt(i);
			if (!rec->getIsValid())
			{
				::MessageBox(GetSafeHwnd(),m_sMsgSpecies1,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
				return FALSE;		
			}	// if (!rec->getIsValid())
		}	// for (int i = 0;i < pRecs->GetCount();i++)
	}	// if (pRecs != NULL)
	return TRUE;		
}
