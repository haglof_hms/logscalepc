// ChangeBarkReductionDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ChangeBarkReductionDlg.h"
#include "ReportClasses.h"

#include "ResLangFileReader.h"

// CChangeBarkReductionDlg dialog

IMPLEMENT_DYNAMIC(CChangeBarkReductionDlg, CXTResizeDialog)

BEGIN_MESSAGE_MAP(CChangeBarkReductionDlg, CXTResizeDialog)
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDOK, &CChangeBarkReductionDlg::OnBnClickedOk)
END_MESSAGE_MAP()


CChangeBarkReductionDlg::CChangeBarkReductionDlg(CWnd* pParent /*=NULL*/)
	: CXTResizeDialog(CChangeBarkReductionDlg::IDD, pParent),
		m_bInitialized(FALSE),
		m_sLangFN(L"")
{

}

CChangeBarkReductionDlg::~CChangeBarkReductionDlg()
{
}

BOOL CChangeBarkReductionDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeDialog::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CChangeBarkReductionDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL12_1, m_lbl12_1);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	//}}AFX_DATA_MAP

}

BOOL CChangeBarkReductionDlg::OnInitDialog()
{
	CXTResizeDialog::OnInitDialog();
	if (!m_bInitialized)
	{

		m_lbl12_1.SetBkColor(INFOBK);
		m_lbl12_1.SetTextColor(BLUE);

		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupReport();

		populateReport();

		m_bInitialized = TRUE;
	}

	return TRUE;
}

void CChangeBarkReductionDlg::OnShowWindow(BOOL bShow,UINT nStatus)
{
	m_repSpecies.SetFocus();
	CXTResizeDialog::OnShowWindow(bShow, nStatus);
}

// CChangeBarkReductionDlg message handlers
void CChangeBarkReductionDlg::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;

	if (m_repSpecies.GetSafeHwnd() == 0)
	{

		// Create the sheet1 list box.
		if (!m_repSpecies.Create(this, ID_REPORT_SPECIES2, L"Species2_report"))
		{
			TRACE0( "Failed to create m_repSpecies.\n" );
			return;
		}
	}

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{

			SetWindowText(xml.str(IDS_STRING5000));

			m_btnOK.SetWindowText(xml.str(IDS_STRING100));
			m_btnCancel.SetWindowText(xml.str(IDS_STRING101));

			m_lbl12_1.SetWindowTextW(xml.str(IDS_STRING5020) + L": " + m_sMeasureMode);

			if (m_repSpecies.GetSafeHwnd() != NULL)
			{

				m_repSpecies.ShowWindow( SW_NORMAL );

				// Species code
				pCol = m_repSpecies.AddColumn(new CXTPReportColumn(COLUMN_0, xml.str(IDS_STRING5001), 80,FALSE,XTP_REPORT_NOICON,FALSE));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->AllowRemove(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// name od species
				pCol = m_repSpecies.AddColumn(new CXTPReportColumn(COLUMN_1, xml.str(IDS_STRING5002), 180,FALSE,XTP_REPORT_NOICON,FALSE));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->AllowRemove(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// barkreduction
				pCol = m_repSpecies.AddColumn(new CXTPReportColumn(COLUMN_2, xml.str(IDS_STRING5003), 80,FALSE,XTP_REPORT_NOICON,FALSE));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->AllowRemove(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;

				m_repSpecies.GetReportHeader()->AllowColumnRemove(FALSE);
				m_repSpecies.SetMultipleSelection( FALSE );
				m_repSpecies.SetGridStyle( TRUE, xtpReportGridSolid );
				m_repSpecies.SetGridStyle( FALSE, xtpReportGridSmallDots );
				m_repSpecies.FocusSubItems(TRUE);
				m_repSpecies.AllowEdit(TRUE);
				m_repSpecies.GetPaintManager()->SetFixedRowHeight(FALSE);

			}	// if (m_repSpecies.GetSafeHwnd() != NULL)

			xml.clean();
			
			RECT rect;
			GetClientRect(&rect);
			setResize(&m_repSpecies,3,35,rect.right-3,rect.bottom-70);

		}
	}
}

void CChangeBarkReductionDlg::populateReport()
{
	m_repSpecies.ResetContent();
	if (m_vecVecTicketSpc.size() > 0)
	{
		for (UINT i1 = 0;i1 < m_vecVecTicketSpc.size();i1++)
		{
			CTicketSpc rec = m_vecVecTicketSpc[i1];
			m_repSpecies.AddRecord(new CTicketSpcReportRec(rec));
		}	// for (UINT i1 = 0;i1 < m_vecPricelist.size();i1++)
	}	// if (m_vecPricelist.size() > 0)
	m_repSpecies.Populate();
	m_repSpecies.UpdateWindow();

	if (m_repSpecies.GetRows()->GetCount() > 0)
	{
		m_repSpecies.SetFocusedRow(m_repSpecies.GetRows()->GetAt(0));
		m_repSpecies.SetFocusedColumn(m_repSpecies.GetColumns()->GetAt(COLUMN_2));
	}

}


void CChangeBarkReductionDlg::OnBnClickedOk()
{
	CTicketSpcReportRec *pRec = NULL;
	CXTPReportRows *pRows = m_repSpecies.GetRows();
	if (pRows != NULL)
	{
		m_repSpecies.Populate();
		for (int i = 0;i < pRows->GetCount();i++)
		{
			pRec = (CTicketSpcReportRec*)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL)
			{
				m_vecReturnVecTicketSpc.push_back(CTicketSpc(pRec->getRecord().getTicketID(),
																										 pRec->getRecord().getSpeciesID(),
																										 pRec->getRecord().getSpeciesCode(),
																										 pRec->getRecord().getSpeciesName(),
																										 pRec->getColFloat(COLUMN_2)));

			}
		}

	}


	OnOK();
}
