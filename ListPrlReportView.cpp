// ListPrlReportView.cpp : implementation file
//

#include "StdAfx.h"

#include "ListPrlReportView.h"

#include "ResLangFileReader.h"

#include "ReportClasses.h"

#include "CreatePricelistsFormView.h"

#include "XTPPreviewView.h"

///////////////////////////////////////////////////////////////////////////////////////////
// CListPrlDoc

IMPLEMENT_DYNCREATE(CListPrlDoc, CDocument)

BEGIN_MESSAGE_MAP(CListPrlDoc, CDocument)
	//{{AFX_MSG_MAP(CListPrlDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CListPrlDoc construction/destruction

CListPrlDoc::CListPrlDoc()
{
	// TODO: add one-time construction code here

}

CListPrlDoc::~CListPrlDoc()
{
}


BOOL CListPrlDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CListPrlDoc serialization

void CListPrlDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CListPrlDoc diagnostics

#ifdef _DEBUG
void CListPrlDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CListPrlDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


/////////////////////////////////////////////////////////////////////////////
// CListPrlFrame

IMPLEMENT_DYNCREATE(CListPrlFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CListPrlFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CListPrlFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_PAINT()
	ON_UPDATE_COMMAND_UI(ID_BUTTON32800, OnUpdateTBBTNFilterOff)
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CListPrlFrame::CListPrlFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDD_FORMVIEW);
	m_bEnableTBBTNFilterOff = FALSE;
	LOGFONT lfIcon;
	VERIFY(::SystemParametersInfo(SPI_GETICONTITLELOGFONT, sizeof(lfIcon), &lfIcon, 0));
	VERIFY(m_fontIcon.CreateFontIndirect(&lfIcon));
}

CListPrlFrame::~CListPrlFrame()
{
}

void CListPrlFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_LIST_PRL_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;

	CMDIChildWnd::OnDestroy();

}

int CListPrlFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}
	// Setup language filename; 051214 p�d
//	m_sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR6);

	EnableDocking(CBRS_ALIGN_ANY);

	// Initialize dialog bar m_wndFieldChooser
	if (!m_wndFieldChooserDlg.Create(this, IDD_FIELD_SELECTION4,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_SHOW_FIELD_SELECTION))
		return -1;      // fail to create

	// Initialize dialog bar m_wndFilterEdit
	if (!m_wndFilterEdit.Create(this, IDD_FILTEREDIT20,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_BUTTON32799))
		return -1;      // fail to create

	// docking for field chooser
	m_wndFieldChooserDlg.EnableDocking(0);
	setLanguage();

	ShowControlBar(&m_wndFieldChooserDlg, FALSE, FALSE);
	FloatControlBar(&m_wndFieldChooserDlg, CPoint(100, GetSystemMetrics(SM_CYSCREEN) / 3));

	// docking for filter editing
	m_wndFilterEdit.EnableDocking(CBRS_ALIGN_TOP);

	ShowControlBar(&m_wndFilterEdit, FALSE, FALSE);
	FloatControlBar(&m_wndFilterEdit, CPoint(400, GetSystemMetrics(SM_CYSCREEN) / 3));

	setupToolBarIcons();

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CListPrlFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_LIST_PRL_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CListPrlFrame::OnSetFocus(CWnd *pWnd)
{
	CMDIChildWnd::OnSetFocus(pWnd);
}

BOOL CListPrlFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CListPrlFrame diagnostics

#ifdef _DEBUG
void CListPrlFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CListPrlFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CListPrlFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = 400;
	lpMMI->ptMinTrackSize.y = 200;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CListPrlFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CListPrlFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if (m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, /*LM_HIDEWRAP|*/ LM_HORZDOCK|LM_HORZ | LM_COMMIT);

		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}	// if (m_wndToolBar.GetSafeHwnd())
	
	CMDIChildWnd::OnPaint();
}

void CListPrlFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CListPrlFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}

void CListPrlFrame::OnUpdateTBBTNFilterOff(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBBTNFilterOff);
}

// MY METHODS

void  CListPrlFrame::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
			m_sToolTipFilter = xml->str(IDS_STRING5950);
			m_sToolTipFilterOff = xml->str(IDS_STRING5952);
			m_sToolTipPrintOut = xml->str(IDS_STRING5958);
			m_sToolTipRefresh = xml->str(IDS_STRING5953);

			m_wndFieldChooserDlg.SetWindowText((xml->str(IDS_STRING5955)));
			m_wndFilterEdit.SetWindowText((xml->str(IDS_STRING5950)));
		}	// if (xml->Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

}

void CListPrlFrame::setupToolBarIcons(void)
{
	HICON hIcon = NULL;
	HMODULE hResModule = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	if (fileExists(sTBResFN))
	{
		// Setup commandbars and manues; 051114 p�d
		CXTPToolBar* pToolBar = &m_wndToolBar;
		if (pToolBar->IsBuiltIn())
		{
			if (pToolBar->GetType() != xtpBarTypeMenuBar)
			{

				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				// Setup icons on toolbars, using resource dll; 051208 p�d
				if (nBarID == IDR_TOOLBAR6)
				{		
					setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_FILTER,m_sToolTipFilter,TRUE);	//
					setToolbarBtn(sTBResFN,p->GetAt(1),RES_TB_FILTER_OFF,m_sToolTipFilterOff,TRUE);	//
					setToolbarBtn(sTBResFN,p->GetAt(2),RES_TB_PRINT,m_sToolTipPrintOut,TRUE);	//
					setToolbarBtn(sTBResFN,p->GetAt(3),RES_TB_UPDATE,m_sToolTipRefresh,TRUE);	//
					setToolbarBtn(sTBResFN,p->GetAt(4),-1,_T(""),FALSE);	//
					p->GetAt(5)->SetVisible(FALSE);
					p->GetAt(6)->SetVisible(FALSE);
					p->GetAt(7)->SetVisible(FALSE);
					p->GetAt(8)->SetVisible(FALSE);
				}	// if (nBarID == IDR_TOOLBAR6)
			}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
		}	// if (pToolBar->IsBuiltIn())
	}	// if (fileExists(sTBResFN))
}


/////////////////////////////////////////////////////////////////////////////
// CPrlReportFilterEditControl

IMPLEMENT_DYNCREATE(CPrlReportFilterEditControl, CXTPReportFilterEditControl)

BEGIN_MESSAGE_MAP(CPrlReportFilterEditControl, CXTPReportFilterEditControl)
	ON_WM_KEYUP()
END_MESSAGE_MAP()

void CPrlReportFilterEditControl::OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	CString S;
	CListPrlFrame* pWnd = (CListPrlFrame *)getFormViewParentByID(ID_LIST_PRL);
	if (pWnd != NULL)
	{
		GetWindowText(S);
		pWnd->setEnableTBBTNFilterOff(S != "");
	}

	CXTPReportFilterEditControl::OnKeyUp(nChar,nRepCnt,nFlags);
}

// CListPrlView

IMPLEMENT_DYNCREATE(CListPrlView,  CXTPReportView) //CXTResizeFormView)

BEGIN_MESSAGE_MAP(CListPrlView,  CXTPReportView) //CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_NOTIFY(NM_CLICK, XTP_ID_REPORT_CONTROL, OnReportItemClick)
	ON_NOTIFY(NM_DBLCLK, XTP_ID_REPORT_CONTROL, OnReportItemDblClick)
	ON_NOTIFY(XTP_NM_REPORT_HEADER_RCLICK, XTP_ID_REPORT_CONTROL, OnReportColumnRClick)
	ON_COMMAND(ID_SHOW_FIELD_SELECTION, OnShowFieldChooser)
	ON_COMMAND(ID_BUTTON32799, OnShowFieldFilter)
	ON_COMMAND(ID_BUTTON32800, OnShowFieldFilterOff)
	ON_COMMAND(ID_BUTTON32801, OnPrintPreview)
	ON_COMMAND(ID_BUTTON32802, OnRefresh)
END_MESSAGE_MAP()

CListPrlView::CListPrlView()
	: CXTPReportView()
{
	m_pDB = NULL;
	m_nSelectedColumn = -1;
}

CListPrlView::~CListPrlView()
{
}

void CListPrlView::OnInitialUpdate()
{
	CXTPReportView::OnInitialUpdate();

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	CListPrlFrame* pWnd = (CListPrlFrame *)getFormViewParentByID(ID_LIST_PRL);
	if (m_wndSubList.GetSafeHwnd() == NULL)
	{
		m_wndSubList.SubclassDlgItem(IDC_COLUMNS4, &pWnd->m_wndFieldChooserDlg);
		GetReportCtrl().GetColumns()->GetReportHeader()->SetSubListCtrl(&m_wndSubList);
	}

	if (m_wndFilterEdit.GetSafeHwnd() == NULL)
	{
		m_wndFilterEdit.SubclassDlgItem(IDC_FILTEREDIT20_1, &pWnd->m_wndFilterEdit);
		GetReportCtrl().GetColumns()->GetReportHeader()->SetFilterEditCtrl(&m_wndFilterEdit);
	}

	if (m_lbl20_1.GetSafeHwnd() == NULL)
	{
		m_lbl20_1.SubclassDlgItem(IDC_FILTERLBL20_1, &pWnd->m_wndFilterEdit);
		m_lbl20_1.SetBkColor(INFOBK);
	}

	if (m_lbl20_2.GetSafeHwnd() == NULL)
	{
		m_lbl20_2.SubclassDlgItem(IDC_FILTERLBL20_2, &pWnd->m_wndFilterEdit);
		m_lbl20_2.SetBkColor(INFOBK);
		m_lbl20_2.SetLblFont(14,FW_BOLD);
	}

	setupReport();

	LoadReportState();

}

BOOL CListPrlView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		if (m_dbConnectionData.conn->isConnected())
		{
			if ((m_pDB = new CDBHandling(m_dbConnectionData)) != NULL)
			{
				m_pDB->getPricelist(m_vecPricelist);
			}

		}
	}
	return CXTPReportView::OnCopyData(pWnd, pData);
}

void CListPrlView::OnDestroy()
{
	SaveReportState();
	// Try to clear records on exit (for memory deallocation); 080215 p�d
	CXTPReportRecords *pRecs = GetReportCtrl().GetRecords();
	if (pRecs != NULL)
	{
		pRecs->RemoveAll();
	}

	if (m_pDB != NULL)
		delete m_pDB;


	CXTPReportView::OnDestroy();	
}

BOOL CListPrlView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
//	if( !CXTResizeFormView::PreCreateWindow(cs) )
//		return FALSE;
	if( ! CXTPReportView::PreCreateWindow(cs) )
		return FALSE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CListPrlView diagnostics

#ifdef _DEBUG
void CListPrlView::AssertValid() const
{
	CXTPReportView::AssertValid();
}

void CListPrlView::Dump(CDumpContext& dc) const
{
	CXTPReportView::Dump(dc);
//	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CListPrlView message handlers

// CListPrlView message handlers
void CListPrlView::OnSize(UINT nType,int cx,int cy)
{
	CXTPReportView::OnSize(nType,cx,cy);
}

void CListPrlView::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
}

// Create and add Assortment settings reportwindow
BOOL CListPrlView::setupReport(void)
{
	CString sColText;
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	GetReportCtrl().ShowGroupBy(TRUE);
	// Add these 3 lines to add scrollbars for View; 070319 p�d

	GetReportCtrl().GetReportHeader()->SetAutoColumnSizing( FALSE );
	GetReportCtrl().EnableScrollBar(SB_HORZ, TRUE );
	GetReportCtrl().EnableScrollBar(SB_VERT, TRUE );

	// Setup language filename; 051214 p�d
	CString sLangFN;
//	sLangFN.Format("%s%s%s%s",getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT);
	sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(sLangFN))
		{
				m_sGroupByThisField	= (xml->str(IDS_STRING5957));
				m_sGroupByBox				= (xml->str(IDS_STRING5956));
				m_sFieldChooser			= (xml->str(IDS_STRING5955));

				m_sFilterOn					= (xml->str(IDS_STRING5951));

				// Get text from languagefile; 061207 p�d
				if (GetReportCtrl().GetSafeHwnd() != NULL)
				{
					GetReportCtrl().ShowWindow( SW_NORMAL );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_0, (xml->str(IDS_STRING4500)), 100));
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING4501)), 120));
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING4502)), 100));
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_3, (xml->str(IDS_STRING4503)), 200));
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					GetReportCtrl().GetReportHeader()->AllowColumnRemove(TRUE);
					GetReportCtrl().SetMultipleSelection( FALSE );
					GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );
					GetReportCtrl().SetGridStyle( FALSE, xtpReportGridSmallDots );
					GetReportCtrl().FocusSubItems(TRUE);
					GetReportCtrl().AllowEdit(FALSE);
					GetReportCtrl().GetPaintManager()->SetFixedRowHeight(FALSE);

					populateReport();		

				}	// if (GetReportCtrl().GetSafeHwnd() != NULL)
			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}	// if (fileExists(m_sLangFN))
	return TRUE;
}

void CListPrlView::populateReport(void)
{
	int nTraktID = -1;
	CXTPReportRecord *pRec = NULL;
	GetReportCtrl().GetRecords()->RemoveAll();
	if (m_vecPricelist.size() > 0)
	{
		for (UINT i = 0;i < m_vecPricelist.size();i++)
		{
			GetReportCtrl().AddRecord(new CListPrlReportRec(m_vecPricelist[i]));
		}	// for (UINT i = 0;i < m_vecPricelist.size();i++)
		GetReportCtrl().Populate();
		GetReportCtrl().UpdateWindow();
	}
}

void CListPrlView::setFilterWindow(void)
{
	m_lbl20_1.SetWindowText(m_sFilterOn + _T(" :"));
	if (m_nSelectedColumn > -1 && m_nSelectedColumn < GetReportCtrl().GetColumns()->GetCount())
	{
		CXTPReportColumns *pCols = GetReportCtrl().GetColumns();
		CXTPReportColumn *pColumn = pCols->GetAt(m_nSelectedColumn);
		int nColumn = pColumn->GetIndex();
		if (pCols && nColumn < pCols->GetCount())
		{
			for (int i = 0;i < pCols->GetCount();i++)
			{
				pCols->GetAt(i)->SetFiltrable( i == nColumn );
			}	// for (int i = 0;i < pCols->GetCount();i++)
		}	// if (pCols && nColumn < pCols->GetCount())
		m_lbl20_2.SetWindowText(pColumn->GetCaption());
	}	// if (m_nSelectedColumn > -1 && m_nSelectedColumn < GetReportCtrl().GetColumns()->GetCount())
	else
		m_lbl20_2.SetWindowText(L"");
}

void CListPrlView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify->pColumn)
	{
		m_nSelectedColumn = pItemNotify->pColumn->GetIndex();
	}

	if (pItemNotify->pRow)
	{

		CListPrlReportRec *pRec = (CListPrlReportRec*)pItemNotify->pItem->GetRecord();
		CCreatePricelistsFormView *pView = (CCreatePricelistsFormView *)getFormViewByID(IDD_FORMVIEW9);
		if (pView != NULL && pRec != NULL)
		{
			pView->doPopulate(pRec->getRecord());
		}	// if (pTabView)

	}	// if (pItemNotify->pRow)
	// Update filter column, if filterwindow is visible; 090224 p�d
	CListPrlFrame* pWnd = (CListPrlFrame *)getFormViewParentByID(ID_LIST_PRL);
	if (pWnd != NULL)
	{
		if (pWnd->m_wndFilterEdit.IsVisible())
		{
			setFilterWindow();
			pWnd->setEnableTBBTNFilterOff(GetReportCtrl().GetFilterText() != _T(""));
		}	// if (pWnd->m_wndFilterEdit.IsVisible())
	}	// if (pWnd != NULL)

}

void CListPrlView::OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	PostMessage(WM_COMMAND, ID_FILE_CLOSE);
}

void CListPrlView::OnShowFieldChooser()
{
	CListPrlFrame* pWnd = (CListPrlFrame *)getFormViewParentByID(ID_LIST_PRL);
	if (pWnd != NULL)
	{
		BOOL bShow = !pWnd->m_wndFieldChooserDlg.IsVisible();
		pWnd->ShowControlBar(&pWnd->m_wndFieldChooserDlg, bShow, FALSE);
	}	// if (pWnd != NULL)
}

void CListPrlView::OnShowFieldFilter()
{
	CListPrlFrame* pWnd = (CListPrlFrame *)getFormViewParentByID(ID_LIST_PRL);
	if (pWnd != NULL)
	{
		setFilterWindow();
		BOOL bShow = !pWnd->m_wndFilterEdit.IsVisible();
		pWnd->ShowControlBar(&pWnd->m_wndFilterEdit, bShow, FALSE);
		pWnd->setEnableTBBTNFilterOff(GetReportCtrl().GetFilterText() != _T(""));
	}

}

void CListPrlView::OnShowFieldFilterOff()
{
	GetReportCtrl().SetFilterText(_T(""));
	GetReportCtrl().Populate();
	m_wndFilterEdit.SetWindowText(_T(""));
	CListPrlFrame* pWnd = (CListPrlFrame *)getFormViewParentByID(ID_LIST_PRL);
	if (pWnd != NULL)
	{
		pWnd->setEnableTBBTNFilterOff(FALSE);
	}	// if (pWnd != NULL)
}

void CListPrlView::OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	ASSERT(pItemNotify->pColumn);
	CPoint ptClick = pItemNotify->pt;

	CMenu menu;
	VERIFY(menu.CreatePopupMenu());

	// create main menu items
	menu.AppendMenu(MF_SEPARATOR, (UINT)-1, (LPCTSTR)NULL);
	menu.AppendMenu(MF_STRING, ID_GROUP_BYTHIS, m_sGroupByThisField);
	menu.AppendMenu(MF_STRING, ID_SHOW_GROUPBOX, m_sGroupByBox);
	menu.AppendMenu(MF_SEPARATOR, (UINT)-1, (LPCTSTR)NULL);
	menu.AppendMenu(MF_STRING, ID_SHOW_FIELDCHOOSER, m_sFieldChooser);

	if (GetReportCtrl().GetReportHeader()->IsShowItemsInGroups())
	{
		menu.CheckMenuItem(ID_GROUP_BYTHIS, MF_BYCOMMAND|MF_CHECKED);
	}

	if (GetReportCtrl().IsGroupByVisible())
	{
		menu.CheckMenuItem(ID_SHOW_GROUPBOX, MF_BYCOMMAND|MF_CHECKED);
	}

	CXTPReportColumns* pColumns = GetReportCtrl().GetColumns();
	CXTPReportColumn* pColumn = pItemNotify->pColumn;
	m_nSelectedColumn = pItemNotify->pColumn->GetIndex();

	// track menu
	int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN |TPM_RIGHTBUTTON, ptClick.x, ptClick.y, this, NULL);

	// other general items
	switch (nMenuResult)
	{
		case ID_GROUP_BYTHIS:

			if (pColumns->GetGroupsOrder()->IndexOf(pColumn) < 0)
			{
				pColumns->GetGroupsOrder()->Add(pColumn);
			}
			GetReportCtrl().GetReportHeader()->ShowItemsInGroups(!GetReportCtrl().GetReportHeader()->IsShowItemsInGroups());
			GetReportCtrl().Populate();
			break;
		case ID_SHOW_GROUPBOX:
			GetReportCtrl().ShowGroupBy(!GetReportCtrl().IsGroupByVisible());
			break;
		case ID_SHOW_FIELDCHOOSER:
			OnShowFieldChooser();
			break;
	}

}

void CListPrlView::OnPrintPreview()
{
	// In derived classes, implement special window handling here
	// Be sure to Unhook Frame Window close if hooked.

	// must not create this on the frame.  Must outlive this function
	CPrintPreviewState* pState = new CPrintPreviewState;

	// DoPrintPreview's return value does not necessarily indicate that
	// Print preview succeeded or failed, but rather what actions are necessary
	// at this point.  If DoPrintPreview returns TRUE, it means that
	// OnEndPrintPreview will be (or has already been) called and the
	// pState structure will be/has been deleted.
	// If DoPrintPreview returns FALSE, it means that OnEndPrintPreview
	// WILL NOT be called and that cleanup, including deleting pState
	// must be done here.
	if ( !DoPrintPreview( AFX_IDD_PREVIEW_TOOLBAR, this,
		RUNTIME_CLASS( CLangPreviewView ), pState ))
	{
		// In derived classes, reverse special window handling here for
		// Preview failure case
		TRACE0( "Error: DoPrintPreview failed.\n" );
		AfxMessageBox( AFX_IDP_COMMAND_FAILURE );
		delete pState;      // preview failed to initialize, delete State now

	}

}

void CListPrlView::OnRefresh()
{
	if (m_pDB != NULL)
	{
		m_pDB->getPricelist(m_vecPricelist);	
		populateReport();
	}
}

// CListPrlView message handlers

void CListPrlView::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (!AfxGetApp()->GetProfileBinary((REG_WP_LIST_PRL_STATE_KEY), _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		GetReportCtrl().SerializeState(ar);
	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;
	// Get filtertext for this Report
	sFilterText = AfxGetApp()->GetProfileString(REG_WP_LIST_PRL_STATE_KEY, _T("FilterText"), _T(""));
	// Get selected column index into registry; 070125 p�d
	m_nSelectedColumn = AfxGetApp()->GetProfileInt(REG_WP_LIST_PRL_STATE_KEY, _T("SelColIndex"),0);

	GetReportCtrl().SetFilterText(sFilterText);
	GetReportCtrl().Populate();
	m_wndFilterEdit.SetWindowText(sFilterText);

	CListPrlFrame* pWnd = (CListPrlFrame *)getFormViewParentByID(ID_LIST_PRL);
	if (pWnd != NULL)
	{
		pWnd->setEnableTBBTNFilterOff(GetReportCtrl().GetFilterText() != _T(""));
	}
}

void CListPrlView::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	GetReportCtrl().SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary(REG_WP_LIST_PRL_STATE_KEY, _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);

	sFilterText = GetReportCtrl().GetFilterText();
	AfxGetApp()->WriteProfileString(REG_WP_LIST_PRL_STATE_KEY, _T("FilterText"), sFilterText);

	// Set selected column index into registry; 070125 p�d
	AfxGetApp()->WriteProfileInt(REG_WP_LIST_PRL_STATE_KEY, _T("SelColIndex"), m_nSelectedColumn);

}

