// GradesperSpcView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "GradesperSpcView.h"

#include "ResLangFileReader.h"

#include "reportclasses.h"
// CGradesperSpcView

IMPLEMENT_DYNCREATE(CGradesperSpcView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CGradesperSpcView, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_CREATE()
	
	ON_NOTIFY(NM_CLICK, ID_REPORT_GRADES2,OnReportClick)
END_MESSAGE_MAP()

CGradesperSpcView::CGradesperSpcView()
	: CXTResizeFormView(CGradesperSpcView::IDD),
		m_bInitialized(FALSE),m_sLangFN(L"")

{

}

CGradesperSpcView::~CGradesperSpcView()
{
}

void CGradesperSpcView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL1, m_lblSpecies);
	DDX_Control(pDX, IDC_LBL2, m_lblGrades);
	//}}AFX_DATA_MAP

}

void CGradesperSpcView::OnDestroy()
{

	CXTResizeFormView::OnDestroy();
}

void CGradesperSpcView::OnClose()
{
	CXTResizeFormView::OnClose();
}

void CGradesperSpcView::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	if (m_repSpecies.GetSafeHwnd() != NULL)
	{
		setResize(&m_repSpecies,1,20,cx/2-50,cy-22);
	}

	if (m_lblSpecies.GetSafeHwnd() != NULL)
	{
		setResize(&m_lblSpecies,1,2,cx/2-50,20);
	}

	if (m_repGrades.GetSafeHwnd() != NULL)
	{
		setResize(&m_repGrades,cx/2-45,20,cx-2,cy-22);
	}

	if (m_lblGrades.GetSafeHwnd() != NULL)
	{
		setResize(&m_lblGrades,cx/2-45,2,cx/2,20);
	}

}

int CGradesperSpcView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}

BOOL CGradesperSpcView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CGradesperSpcView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();
	if (! m_bInitialized )
	{
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupReports();
		m_bInitialized = TRUE;
	}

}

void CGradesperSpcView::OnReportClick(NMHDR* pNotifyStruct,LRESULT * /*result*/)
{
	int nHeaderIndex = -1;
	XTP_NM_REPORTRECORDITEM* item = (XTP_NM_REPORTRECORDITEM*)pNotifyStruct;
	if (item != NULL)
	{
		nHeaderIndex = m_repGrades.GetReportHeader()->HitTestHeaderColumnIndex(item->pt);
		if (nHeaderIndex == 0)
		{
			if (item->pColumn->GetIconID() == 15)
				item->pColumn->SetIconID(16);
			else if (item->pColumn->GetIconID() == 16)
				item->pColumn->SetIconID(15);
			// Check all items in Grades list
			if (item->pColumn->GetIconID() == 15)
			{
				CXTPReportRecords *pRecs = (CXTPReportRecords *)m_repGrades.GetRecords();
				if (pRecs != NULL)
				{
					for (int i = 0;i < pRecs->GetCount();i++)
					{
						CGradesPerSpeciesReportRec *rec = (CGradesPerSpeciesReportRec*)pRecs->GetAt(i);
						if (rec != NULL)
						{
							rec->setColChecked(COLUMN_0,TRUE);
						}
					}
				}
			} // if (item->pColumn->GetIconID() == 15)
			// UnCheck all items in Grades list
			else if (item->pColumn->GetIconID() == 16)
			{
				CXTPReportRecords *pRecs = (CXTPReportRecords *)m_repGrades.GetRecords();
				if (pRecs != NULL)
				{
					for (int i = 0;i < pRecs->GetCount();i++)
					{
						CGradesPerSpeciesReportRec *rec = (CGradesPerSpeciesReportRec*)pRecs->GetAt(i);
						if (rec != NULL)
						{
							rec->setColChecked(COLUMN_0,FALSE);
						}
					}
				}
			} // if (item->pColumn->GetIconID() == 15)

		}
	}
}

// CGradesperSpcView diagnostics

#ifdef _DEBUG
void CGradesperSpcView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CGradesperSpcView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CGradesperSpcView message handlers
void CGradesperSpcView::setupReports()
{
	CXTPReportColumn *pCol = NULL;
	if (m_repSpecies.GetSafeHwnd() == 0)
	{

		// Create the sheet1 list box.
		if (!m_repSpecies.Create(this, ID_REPORT_SPECIES2, L"Species_report"))
		{
			TRACE0( "Failed to create m_repSpecies.\n" );
			return;
		}
	}

	if (m_repGrades.GetSafeHwnd() == 0)
	{

		// Create the sheet1 list box.
		if (!m_repGrades.Create(this, ID_REPORT_GRADES2, L"Grades_report"))
		{
			TRACE0( "Failed to create m_repGrades.\n" );
			return;
		}
	}

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{

			if (m_repSpecies.GetSafeHwnd() != NULL)
			{
				m_repSpecies.ShowWindow( SW_NORMAL );

				pCol = m_repSpecies.AddColumn(new CXTPReportColumn(COLUMN_0, xml.str(IDS_STRING1201), 80,FALSE,XTP_REPORT_NOICON,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				pCol = m_repSpecies.AddColumn(new CXTPReportColumn(COLUMN_1, xml.str(IDS_STRING1202), 215,FALSE,XTP_REPORT_NOICON,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				m_repSpecies.GetReportHeader()->AllowColumnRemove(FALSE);
				m_repSpecies.SetMultipleSelection( FALSE );
				m_repSpecies.SetGridStyle( TRUE, xtpReportGridSolid );
				m_repSpecies.SetGridStyle( FALSE, xtpReportGridSolid );
				m_repSpecies.FocusSubItems(FALSE);
				m_repSpecies.AllowEdit(FALSE);
				m_repSpecies.GetPaintManager()->SetFixedRowHeight(FALSE);
			}	// if (m_repSpecies.GetSafeHwnd() != NULL)

			if (m_repGrades.GetSafeHwnd() != NULL)
			{

				VERIFY(m_ilIcons.Create(16,13, ILC_COLOR24|ILC_MASK, 0, 1));
				CBitmap bmp;
				VERIFY(bmp.LoadBitmap(IDB_BITMAP1));
				m_ilIcons.Add(&bmp, RGB(255, 0, 255));

				m_repGrades.SetImageList(&m_ilIcons);

				m_repGrades.ShowWindow( SW_NORMAL );

				pCol = m_repGrades.AddColumn(new CXTPReportColumn(COLUMN_0, L"", 30,FALSE,16,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;

				pCol = m_repGrades.AddColumn(new CXTPReportColumn(COLUMN_1, xml.str(IDS_STRING1211), 80,FALSE,XTP_REPORT_NOICON,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				pCol = m_repGrades.AddColumn(new CXTPReportColumn(COLUMN_2, xml.str(IDS_STRING1212), 215,FALSE,XTP_REPORT_NOICON,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				m_repGrades.GetReportHeader()->AllowColumnRemove(FALSE);
				m_repGrades.SetMultipleSelection( FALSE );
				m_repGrades.SetGridStyle( TRUE, xtpReportGridSolid );
				m_repGrades.SetGridStyle( FALSE, xtpReportGridSolid );
				m_repGrades.FocusSubItems(TRUE);
				m_repGrades.AllowEdit(FALSE);
				m_repGrades.GetPaintManager()->SetFixedRowHeight(FALSE);
			}	// if (m_repSpecies.GetSafeHwnd() != NULL)

			// Strings
			m_lblSpecies.SetTextColor(BLUE);
			m_lblSpecies.SetLblFontEx(-1,FW_BOLD);
			m_lblSpecies.SetWindowTextW(xml.str(IDS_STRING1200));
			m_lblGrades.SetTextColor(BLUE);
			m_lblGrades.SetLblFontEx(-1,FW_BOLD);
			m_lblGrades.SetWindowTextW(xml.str(IDS_STRING1210));


			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

}
