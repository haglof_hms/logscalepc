// PricelistFormView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "PricelistFormView.h"

#include "selectcalctypedlg.h"

#include "ResLangFileReader.h"

///////////////////////////////////////////////////////////////////////////////////////////
// CPricelistDoc


IMPLEMENT_DYNCREATE(CPricelistDoc, CDocument)

BEGIN_MESSAGE_MAP(CPricelistDoc, CDocument)
	//{{AFX_MSG_MAP(CPricelistDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPricelistDoc construction/destruction

CPricelistDoc::CPricelistDoc()
{
	// TODO: add one-time construction code here

}

CPricelistDoc::~CPricelistDoc()
{
}


BOOL CPricelistDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CPricelistDoc serialization

void CPricelistDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CPricelistDoc diagnostics

#ifdef _DEBUG
void CPricelistDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CPricelistDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


///////////////////////////////////////////////////////////////////////////////////////////
// CPricelistFrame

IMPLEMENT_DYNCREATE(CPricelistFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CPricelistFrame, CMDIChildWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()

	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_UPDATE_COMMAND_UI(ID_BUTTON32778, OnUpdateToolbarBtn)		// New button
	ON_UPDATE_COMMAND_UI(ID_BUTTON32779, OnUpdateToolbarBtn)		// Delete button
//	ON_UPDATE_COMMAND_UI(ID_BUTTON32777, OnUpdateToolbarBtn)	// Savebutton

END_MESSAGE_MAP()


// CPricelistFrame construction/destruction

XTPDockingPanePaintTheme CPricelistFrame::m_themeCurrent = xtpPaneThemeOffice2003;

CPricelistFrame::CPricelistFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
	m_bEnableToolBar = TRUE;
}

CPricelistFrame::~CPricelistFrame()
{
}

void CPricelistFrame::OnDestroy(void)
{

	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6004_KEY);
	SavePlacement(this, csBuf);

}

void CPricelistFrame::OnClose(void)
{
	CMDIChildWnd::OnClose();
}


int CPricelistFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	EnableDocking(CBRS_ALIGN_ANY);

	// Create and Load toolbar; 090107 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR3);
	m_wndToolBar.EnableDocking(xtpFlagAlignTop|xtpFlagStretched);

	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	CString sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
	if (fileExists(sLangFN))
	{
		RLFReader xml;
		if (xml.Load(sLangFN))
		{
			//***************************************************************************************
			// Setup commandbars and manues; 051114 p�d
			CXTPToolBar* pToolBar = &m_wndToolBar;
			if (pToolBar->IsBuiltIn())
			{
				if (pToolBar->GetType() != xtpBarTypeMenuBar)
				{
					UINT nBarID = pToolBar->GetBarID();
					pToolBar->LoadToolBar(nBarID, FALSE);
					CXTPControls *p = pToolBar->GetControls();
					// Setup icons on toolbars, using resource dll; 051208 p�d
					if (nBarID == IDR_TOOLBAR3)
					{		
						setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_ADD,xml.str(IDS_STRING1350),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(1),RES_TB_DEL2,xml.str(IDS_STRING1351),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(2),RES_TB_SAVE,xml.str(IDS_STRING1352),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(3),RES_TB_SAVE,xml.str(IDS_STRING1355),TRUE);	//

						p->GetAt(0)->SetVisible(FALSE);
						p->GetAt(1)->SetVisible(FALSE);
					}	// if (nBarID == IDR_TOOLBAR3)
				}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
			}	// if (pToolBar->IsBuiltIn())
			//***************************************************************************************
			xml.clean();
		}	// if (xml.Load(sLangFN))
	}	// if (fileExists(sLangFN))


	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

void CPricelistFrame::OnUpdateToolbarBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableToolBar);
}


BOOL CPricelistFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CPricelistFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CPricelistFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6004_KEY);
		LoadPlacement(this, csBuf);
  }

}

void CPricelistFrame::OnSetFocus(CWnd* pWnd)
{
	CMDIChildWnd::OnSetFocus(pWnd);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CPricelistFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}


// CPricelistFrame diagnostics

#ifdef _DEBUG
void CPricelistFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CPricelistFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CPricelistFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE2;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE2;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CPricelistFrame::toolbarEnableDisable(BOOL enable)
{
	m_bEnableToolBar = enable;
}

// CPricelistFormView

IMPLEMENT_DYNCREATE(CPricelistFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CPricelistFormView, CXTResizeFormView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_COPYDATA()
	ON_WM_DESTROY()
	ON_CBN_SELCHANGE(IDC_COMBO1, &CPricelistFormView::OnCbnSelchangeCombo1)
	ON_NOTIFY(NM_CLICK, ID_REPORT_PRICELIST, OnReportItemClick)

	ON_COMMAND(ID_BUTTON32780,OnSavePricelist)
	ON_COMMAND(ID_BUTTON32784,OnUpdateGrades)
END_MESSAGE_MAP()

CPricelistFormView::CPricelistFormView()
	: CXTResizeFormView(CPricelistFormView::IDD),
		m_bInitialized(FALSE),m_sLangFN(L""),m_pDB(NULL)

{

}

CPricelistFormView::~CPricelistFormView()
{
}

BOOL CPricelistFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CPricelistFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL_SPECIES, m_lblSpecies);
	DDX_Control(pDX, IDC_COMBO1, m_cboxSpecies);
	//}}AFX_DATA_MAP

}

int CPricelistFormView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}

void CPricelistFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();
	if (!m_bInitialized)
	{
		m_lblSpecies.SetLblFontEx(14,FW_NORMAL);
		m_cboxSpecies.SetLblFont(16,FW_BOLD);
		m_cboxSpecies.SetEnabledColor(BLUE,WHITE);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		setupReport();

		populateSpeciesCB();

		m_bInitialized = TRUE;
	}

}

void CPricelistFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	if (m_repPricelist.GetSafeHwnd() != NULL)
	{
		setResize(&m_repPricelist,2,50,cx-4,cy-52);
	}
}

void CPricelistFormView::OnDestroy()
{
	OnSavePricelist();
	CXTResizeFormView::OnDestroy();
}

BOOL CPricelistFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{

	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		if (m_dbConnectionData.conn->isConnected())
		{
			m_pDB = new CDBHandling(m_dbConnectionData);
		}
	}

	return CXTResizeFormView::OnCopyData(pWnd, pData);
}


// CPricelistFormView diagnostics

#ifdef _DEBUG
void CPricelistFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CPricelistFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG


void CPricelistFormView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	CRect rect;
	POINT pt;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;

	// prevents us from crashing when clicking inside the report, but not on an item
	if(pItemNotify == NULL) 
		return;

	// prevents us from crashing when clicking inside the report, but not on an item
	if(pItemNotify->pItem == NULL)
		return;

	switch (pItemNotify->pColumn->GetItemIndex())
	{
			case COLUMN_2 :
			{
				// Do a hit-test; 080513 p�d
				rect = pItemNotify->pColumn->GetRect();		
				pt = pItemNotify->pt;
				// Check if the user clicked on the Icon or not; 080513 p�d
				if (hitTest_X(pt.x,rect.left,13))
				{
					CXTPReportRow *pRow = m_repPricelist.GetFocusedRow();
					if (pRow != NULL)
					{
						CPricelistReportRec *pRec = (CPricelistReportRec *)pRow->GetRecord();
						if (pRec != NULL)
						{
							CSelectCalcTypeDlg *pDlg = new CSelectCalcTypeDlg((pRec->getRecord().getIsPulpwood() == 1));
							if (pDlg != NULL)
							{
								pDlg->setVecCalcTypes(m_vecCalcTypes,pRec->getIconColText(COLUMN_2));
								if (pDlg->DoModal() == IDOK)
								{
									pRec->getRecord().setCalcType(pDlg->getSelectedCalcBasis());
									m_pDB->updPricelistCalcBasis(pRec->getRecord());
									populateReport();
								}
								delete pDlg;
							}
						}
					}
				}	// if (hitTest_X(pt.x,rect.left,13))
			}
			break;
	};
}


// CPricelistFormView message handlers
// Create and add Assortment settings reportwindow
void CPricelistFormView::setupReport(void)
{
	CXTPReportColumn *pCol = NULL;

	if (m_repPricelist.GetSafeHwnd() == 0)
	{

		// Create the sheet1 list box.
		if (!m_repPricelist.Create(this, ID_REPORT_PRICELIST, L"Pricelist_report"))
		{
			TRACE0( "Failed to create m_repPricelist.\n" );
			return;
		}
	}

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{

			if (m_repPricelist.GetSafeHwnd() != NULL)
			{

				VERIFY(m_ilIcons.Create(16,13, ILC_COLOR24|ILC_MASK, 0, 1));
				CBitmap bmp;
				VERIFY(bmp.LoadBitmap(IDB_BITMAP1));
				m_ilIcons.Add(&bmp, RGB(255, 0, 255));

				m_repPricelist.SetImageList(&m_ilIcons);

				m_repPricelist.ShowWindow( SW_NORMAL );

				// Grade code
				pCol = m_repPricelist.AddColumn(new CXTPReportColumn(COLUMN_0, xml.str(IDS_STRING1301), 80,FALSE,XTP_REPORT_NOICON,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Price
				pCol = m_repPricelist.AddColumn(new CXTPReportColumn(COLUMN_1, xml.str(IDS_STRING1302), 80,FALSE,XTP_REPORT_NOICON,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->SetAlignment(DT_CENTER);
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				// Basis (Calculate type)
				pCol = m_repPricelist.AddColumn(new CXTPReportColumn(COLUMN_2, xml.str(IDS_STRING1303), 100,FALSE,XTP_REPORT_NOICON,FALSE));
				pCol->AllowRemove(FALSE);
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				m_repPricelist.GetReportHeader()->AllowColumnRemove(FALSE);
				m_repPricelist.SetMultipleSelection( FALSE );
				m_repPricelist.SetGridStyle( TRUE, xtpReportGridSolid );
				m_repPricelist.SetGridStyle( FALSE, xtpReportGridSmallDots );
				m_repPricelist.FocusSubItems(TRUE);
				m_repPricelist.AllowEdit(TRUE);
				m_repPricelist.GetPaintManager()->SetFixedRowHeight(FALSE);

				RECT rect;
				GetClientRect(&rect);
				setResize(&m_repPricelist,2,50,rect.right-4,rect.bottom-52);

			}	// if (m_repPricelist.GetSafeHwnd() != NULL)

			// Strings
			m_lblSpecies.SetWindowTextW(xml.str(IDS_STRING1300));
			m_sMsgCap = xml.str(IDS_STRING99);
			m_sMsgDelete1 = xml.str(IDS_STRING1353);
			m_sMsgDelete2 = xml.str(IDS_STRING1354);


			xml.clean();
		}
	}
}

void CPricelistFormView::populateSpeciesCB()
{
	CSpecies rec = CSpecies();
	CString sStr = L"";
	if (m_pDB != NULL)
	{
		m_pDB->getSpecies(m_vecSpecies);
		if (m_vecSpecies.size() > 0)
		{
			m_cboxSpecies.Clear();
			for (UINT i = 0;i < m_vecSpecies.size();i++)
			{
				rec = m_vecSpecies[i];
				if (rec.getSpcName().IsEmpty() && !rec.getSpcCode().IsEmpty())
					sStr.Format(L"%s",rec.getSpcCode());
				else if (!rec.getSpcName().IsEmpty() && !rec.getSpcCode().IsEmpty())
					sStr.Format(L"%s - %s",rec.getSpcCode(),rec.getSpcName());

				m_cboxSpecies.AddString(sStr);

			}
		}
		// We'll also get caclulationTypes timsCalcTypes table
		m_pDB->getCalcTypes(m_vecCalcTypes);
		// ... and defaults values
		m_pDB->getDefaults(m_vecDefaults);

	}
}

void CPricelistFormView::populateReport()
{
	m_repPricelist.ResetContent();
	if (m_pDB != NULL)
	{
		m_pDB->getPricelist(m_vecPricelist,m_recSpeciesSelected.getSpcID());

		if (m_vecPricelist.size() > 0)
		{
			for (UINT i1 = 0;i1 < m_vecPricelist.size();i1++)
			{
				CPricelist rec = m_vecPricelist[i1];
				//addBasisConstraints(rec.getIsPulpwood());
				m_repPricelist.AddRecord(new CPricelistReportRec(rec));
			}	// for (UINT i1 = 0;i1 < m_vecPricelist.size();i1++)
		}	// if (m_vecPricelist.size() > 0)
		m_repPricelist.Populate();
		m_repPricelist.UpdateWindow();
	}	// if (m_pDB != NULL)
}

void CPricelistFormView::OnCbnSelchangeCombo1()
{
	CVecGrades vec;
	CVecSpecies vecSpc;
	CPricelist rec;
	CString sDefValue = L"";
	OnSavePricelist();	// Save before any chage
	int nIndex = m_cboxSpecies.GetCurSel();
	if (nIndex != CB_ERR && nIndex < m_vecSpecies.size() && m_pDB != NULL)
	{
		m_recSpeciesSelected = m_vecSpecies[nIndex];
		// Check if this species is in pricelist. I.g. there are grades added.
		// If not add grades for species to timsPricelist table
		if (!m_pDB->isSpeciesInPricelist(m_recSpeciesSelected.getSpcID()))
		{
			m_pDB->getGrades(vec);
			// There are grades
			if (vec.size() > 0)
			{
				for (UINT i = 0;i < vec.size();i++)
				{
					if (vec[i].getIsPulpwood() == 0)
						sDefValue = getDefaults(DEF_SAWLOG,m_vecDefaults);
					else if (vec[i].getIsPulpwood() == 1)
						sDefValue = getDefaults(DEF_PULPWOOD,m_vecDefaults);
					rec = CPricelist(-1,m_recSpeciesSelected.getSpcID(),
															vec[i].getGradesID(),
															m_recSpeciesSelected.getSpcCode(),
															vec[i].getGradesCode(),0.0,
															sDefValue,
															vec[i].getIsPulpwood());
					m_pDB->newPricelist(rec);
				}
			}
		}
		populateReport();
	}
	else
		m_recSpeciesSelected = CSpecies();	// Reset
}

void CPricelistFormView::OnSavePricelist()
{
	int nPricelistID = -1;
	int nSpeciesID = -1;
	int nGradesID = -1;
	int nBasisID = -1;
	CXTPReportRows *pRows = m_repPricelist.GetRows();
	if (pRows != NULL && m_pDB != NULL)
	{
		for (int i = 0;i < pRows->GetCount();i++)
		{
			CPricelistReportRec *pRec = (CPricelistReportRec *)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL)
			{
				CPricelist rec = pRec->getRecord();
				rec.setPrice(pRec->getColDbl(COLUMN_1));
				rec.setCalcType(pRec->getIconColText(COLUMN_2));
				
				m_pDB->updPricelist(rec);
			}	// if (pRec != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)
	}	// if (pRows != NULL)
}

void CPricelistFormView::OnUpdateGrades()
{
	CXTPReportRecords *pRecs = m_repPricelist.GetRecords();
	CGrades recGrade = CGrades();
	CVecGrades vec;
	CVecSpecies vecSpc;
	CPricelist rec = CPricelist();
	CString sDefValue = L"",S;
	int nIndex = m_cboxSpecies.GetCurSel();
	BOOL bFoundGrade = FALSE;
	if (nIndex != CB_ERR && nIndex < m_vecSpecies.size() && m_pDB != NULL && pRecs != NULL)
	{
		m_recSpeciesSelected = m_vecSpecies[nIndex];
		if (m_pDB->isSpeciesInPricelist(m_recSpeciesSelected.getSpcID()))
		{
			m_pDB->getGrades(vec);
					bFoundGrade = FALSE;
					for (UINT n = 0;n < vec.size();n++)
					{
						recGrade = vec[n];
						for (int i = 0;i < pRecs->GetCount();i++)
						{
							CPricelistReportRec *pRec = (CPricelistReportRec *)pRecs->GetAt(i);
							if (pRec != NULL)
							{
							}	// if (pRec != NULL)

						
						S.Format(L"%s GradeID %s  SpcID %d",pRec->getColText(COLUMN_0),recGrade.getGradesCode(),m_recSpeciesSelected.getSpcID());
						AfxMessageBox(S);
						// Check if any grades isn't included in pricelist
						if (pRec->getColText(COLUMN_0).CompareNoCase(recGrade.getGradesCode()) == 0)
						{
							bFoundGrade = TRUE;
							break;
						}	// if (pRec->getRecord().getGradesID() != vec[n].getGradesID())
					}	// for (UINT n = 0;n < vec.size();n++)				

				if (!bFoundGrade)
				{
					S.Format(L"GradeID %s  SpcID %d",recGrade.getGradesCode(),m_recSpeciesSelected.getSpcID());
					AfxMessageBox(S);
/*
						if (recGrade.getIsPulpwood() == 0)
							sDefValue = getDefaults(DEF_SAWLOG,m_vecDefaults);
						else if (recGrade.getIsPulpwood() == 1)
							sDefValue = getDefaults(DEF_PULPWOOD,m_vecDefaults);
						rec = CPricelist(-1,m_recSpeciesSelected.getSpcID(),
																recGrade.getGradesID(),
																m_recSpeciesSelected.getSpcCode(),
																recGrade.getGradesCode(),0.0,
																sDefValue,
																recGrade.getIsPulpwood());
						m_pDB->newPricelist(rec);
*/
				}	// if (!bFoundGrade)

			}	// for (int i = 0;i < pRecs->GetCount();i++)
		}	// if (m_pDB->isSpeciesInPricelist(m_recSpeciesSelected.getSpcID()))
		populateReport();
	}
	else
		m_recSpeciesSelected = CSpecies();	// Reset
}
