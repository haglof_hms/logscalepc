// CreatePricelistsFormView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "CreatePricelistsFormView.h"
#include "SelectSpeciesDlg.h"
#include "SelectSpeciesAndGradesDlg.h"
#include "selectcalctypedlg.h"
#include "ReportClasses.h"
#include "MatchPrlGrdDlg.h"

#include "LogScalePrlParser.h"

#include "ResLangFileReader.h"

#include <sstream>

///////////////////////////////////////////////////////////////////////////////////////////
// CCreatePricelistsDoc


IMPLEMENT_DYNCREATE(CCreatePricelistsDoc, CDocument)

BEGIN_MESSAGE_MAP(CCreatePricelistsDoc, CDocument)
	//{{AFX_MSG_MAP(CCreatePricelistsDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCreatePricelistsDoc construction/destruction

CCreatePricelistsDoc::CCreatePricelistsDoc()
{
	// TODO: add one-time construction code here

}

CCreatePricelistsDoc::~CCreatePricelistsDoc()
{
}


BOOL CCreatePricelistsDoc::OnNewDocument()
{
	// CHECK FOR LICENSE HERE!!!!! 2011-12-22 P�D
	if (!License())
	{
		return FALSE;
	}

	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CCreatePricelistsDoc serialization

void CCreatePricelistsDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CCreatePricelistsDoc diagnostics

#ifdef _DEBUG
void CCreatePricelistsDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CCreatePricelistsDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

///////////////////////////////////////////////////////////////////////////////////////////
// CCreatePricelistsFrame

IMPLEMENT_DYNCREATE(CCreatePricelistsFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CCreatePricelistsFrame, CMDIChildWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_SYSCOMMAND()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)

	ON_XTP_CREATECONTROL()
END_MESSAGE_MAP()


// CCreatePricelistsFrame construction/destruction

XTPDockingPanePaintTheme CCreatePricelistsFrame::m_themeCurrent = xtpPaneThemeOffice2003;

CCreatePricelistsFrame::CCreatePricelistsFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
	m_bInitReports = FALSE;
	m_bIsPrintOutTBtn = FALSE;
	m_bIsSysCommand = FALSE;
}

CCreatePricelistsFrame::~CCreatePricelistsFrame()
{
}

void CCreatePricelistsFrame::OnDestroy(void)
{
}

void CCreatePricelistsFrame::OnClose(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6502_KEY);
	SavePlacement(this, csBuf);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	if (!m_bIsSysCommand)
	{
		CCreatePricelistsFormView *pView = (CCreatePricelistsFormView*)getFormViewByID(IDD_FORMVIEW9);
		if (pView != NULL)
		{
			QUIT_TYPES::Q_T_RETURN qtt;
			qtt = pView->checkDataBeforeSave(CHECK_SAVE_TYPES::CHECK_SAVE_ON_QUIT);
			if (qtt == QUIT_TYPES::DO_SAVE)
			{
				pView->doSave();
				setNavBarButtons(false);
				CMDIChildWnd::OnClose();
			}
			else if (qtt == QUIT_TYPES::QUIT_ANYWAY)
			{
				setNavBarButtons(false);
				CMDIChildWnd::OnClose();
			}
		}
	}
	else
	{
		setNavBarButtons(false);
		CMDIChildWnd::OnClose();
	}
}

void CCreatePricelistsFrame::OnSysCommand(UINT nID,LPARAM lParam)
{

	if ((nID & 0xFFF0) == SC_CLOSE)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
		CCreatePricelistsFormView *pView = (CCreatePricelistsFormView*)getFormViewByID(IDD_FORMVIEW9);
		if (pView != NULL)
		{
			QUIT_TYPES::Q_T_RETURN qtt;
			qtt = pView->checkDataBeforeSave(CHECK_SAVE_TYPES::CHECK_SAVE_ON_QUIT);
			if (qtt == QUIT_TYPES::DO_SAVE)
			{
				m_bIsSysCommand = TRUE;
	
				pView->doSave();
			
				setNavBarButtons();

				CMDIChildWnd::OnSysCommand(nID,lParam);
			}
			else if (qtt == QUIT_TYPES::QUIT_ANYWAY)
			{
				setNavBarButtons();
				CMDIChildWnd::OnClose();
			}
		}
	}
	else if ((nID & 0xFFF0) != SC_CLOSE)
	{
		CMDIChildWnd::OnSysCommand(nID,lParam);
	}
}

int CCreatePricelistsFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	EnableDocking(CBRS_ALIGN_ANY);

	// Create and Load toolbar; 090107 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR6);
	m_wndToolBar.EnableDocking(xtpFlagAlignTop|xtpFlagStretched);

	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			//***************************************************************************************
			// Setup commandbars and manues; 051114 p�d
			CXTPToolBar* pToolBar = &m_wndToolBar;
			if (pToolBar->IsBuiltIn())
			{
				if (pToolBar->GetType() != xtpBarTypeMenuBar)
				{
					UINT nBarID = pToolBar->GetBarID();
					pToolBar->LoadToolBar(nBarID, FALSE);
					CXTPControls *p = pToolBar->GetControls();
					// Setup icons on toolbars, using resource dll; 051208 p�d
					if (nBarID == IDR_TOOLBAR6)
					{		
						setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_NEW,xml.str(IDS_STRING45550),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(1),RES_TB_SAVE,xml.str(IDS_STRING45551),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(2),RES_TB_DEL,xml.str(IDS_STRING45552),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(3),RES_TB_TOOLS,xml.str(IDS_STRING45553),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(4),RES_TB_DEL2,xml.str(IDS_STRING45554),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(5),RES_TB_IMPORT,xml.str(IDS_STRING45555),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(6),RES_TB_EXPORT,xml.str(IDS_STRING45556),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(7),RES_TB_PREVIEW,xml.str(IDS_STRING45557),TRUE);	//
						p->GetAt(4)->SetVisible(FALSE);
						//p->GetAt(5)->SetVisible(FALSE);
						//p->GetAt(6)->SetVisible(FALSE);
						p->GetAt(7)->SetVisible(FALSE);
						p->GetAt(8)->SetVisible(FALSE);
					}	// if (nBarID == IDR_TOOLBAR5)
				}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
			}	// if (pToolBar->IsBuiltIn())
			//***************************************************************************************
			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

int CCreatePricelistsFrame::OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl)
{
	if (lpCreateControl->nID == ID_BUTTON32802)
	{
		CMyControlPopup *m_pToolsPopup = new CMyControlPopup();
		m_pToolsPopup->SetStyle(xtpButtonIcon);
		if (fileExists(m_sLangFN))
		{
			RLFReader xml; // = new RLFReader;
			if (xml.Load(m_sLangFN))
			{
				m_pToolsPopup->addMenuIDAndText(ID_TOOLS_PRICELIST,xml.str(IDS_STRING45558));
			}	// if (xml->Load(m_sLangFN))
			xml.clean();
		}

		lpCreateControl->pControl = m_pToolsPopup;
		return TRUE;
	}

	return TRUE;
}

BOOL CCreatePricelistsFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CCreatePricelistsFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CCreatePricelistsFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6502_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CCreatePricelistsFrame::OnSetFocus(CWnd* pWnd)
{
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,ID_DO_SOMETHING_IN_SHELL,ID_LPARAM_COMMAND2);

	setNavBarButtons();

	CCreatePricelistsFormView *pView = (CCreatePricelistsFormView*)getFormViewByID(IDD_FORMVIEW9);
	if (pView != NULL)
	{
		pView->doSetNavigationButtons();
	}

	CMDIChildWnd::OnSetFocus(pWnd);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CCreatePricelistsFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	CCreatePricelistsFormView *pView = (CCreatePricelistsFormView*)getFormViewByID(IDD_FORMVIEW9);
	if (pView != NULL)
	{
		::SendMessage(pView->GetSafeHwnd(),WM_USER_MSG_SUITE,wParam,lParam);
	}

	return 0L;
}


// CCreatePricelistsFrame diagnostics

#ifdef _DEBUG
void CCreatePricelistsFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CCreatePricelistsFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CCreatePricelistsFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CCreatePricelistsFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType,cx,cy);
}




// CCreatePricelistsFormView

IMPLEMENT_DYNCREATE(CCreatePricelistsFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CCreatePricelistsFormView, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_CREATE()
	ON_WM_COPYDATA()

	ON_MESSAGE(MSG_OPEN_SUITE_ARG, OnOpenSuiteArgMessage)

	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)

	ON_COMMAND_RANGE(ID_BUTTON32799,ID_BUTTON32806, OnCommand)
	ON_COMMAND_RANGE(ID_TOOLS_PRICELIST,ID_TOOLS_PRICELIST, OnCommandTools)

	ON_UPDATE_COMMAND_UI_RANGE(ID_BUTTON32799,ID_BUTTON32806, OnUpdateToolbar)

	ON_EN_CHANGE(IDC_EDIT9_1, &CCreatePricelistsFormView::OnEnChangeEdit91)
END_MESSAGE_MAP()

CCreatePricelistsFormView::CCreatePricelistsFormView()
	: CXTResizeFormView(CCreatePricelistsFormView::IDD),
		m_bInitialized(FALSE),
		m_nSelectedIndex(0),
		m_nSelectedSpeciesTab(0),
		m_bOnlySave(FALSE),
		m_bIsNameOfPricelistOK(TRUE)
		, m_csDate(_T(""))
{

}

CCreatePricelistsFormView::~CCreatePricelistsFormView()
{
}

void CCreatePricelistsFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCreatePricelistsFormView)
	DDX_Control(pDX, IDC_LBL9_1, m_lbl9_1);
	DDX_Control(pDX, IDC_LBL9_2, m_lbl9_2);
	DDX_Control(pDX, IDC_LBL9_3, m_lbl9_3);
	DDX_Control(pDX, IDC_LBL9_4, m_lbl9_4);

	DDX_Control(pDX, IDC_EDIT9_1, m_edit9_1);
	DDX_Control(pDX, IDC_EDIT9_2, m_edit9_2);
	DDX_Control(pDX, IDC_EDIT9_3, m_edit9_3);

	//}}AFX_DATA_MAP

	DDX_Control(pDX, IDC_DATE, m_dtDate);
	DDX_DateTimeCtrl(pDX, IDC_DATE, m_csDate);
}

BOOL CCreatePricelistsFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

int CCreatePricelistsFormView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	CFont fnt;
	LOGFONT lf;
	VERIFY(fnt.CreateFont(
   18,                        // nHeight
   0,                         // nWidth
   0,                         // nEscapement
   0,                         // nOrientation
   FW_NORMAL,                 // nWeight
   FALSE,                     // bItalic
   FALSE,                     // bUnderline
   0,                         // cStrikeOut
   ANSI_CHARSET,              // nCharSet
   OUT_DEFAULT_PRECIS,        // nOutPrecision
   CLIP_DEFAULT_PRECIS,       // nClipPrecision
   DEFAULT_QUALITY,           // nQuality
   DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
   _T("Times New Roman")));   // lpszFacename

	fnt.GetLogFont( &lf );

	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS, CRect(0, 0, 0, 0), this, ID_TABCONTROL2);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearanceVisualStudio2005);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = TRUE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = TRUE;
	m_wndTabControl.GetPaintManager()->m_bBoldSelected = TRUE;
	m_wndTabControl.GetPaintManager()->SetFontIndirect( &lf );
	m_wndTabControl.GetPaintManager()->DisableLunaColors( FALSE );
	return 0;
}


// CCreatePricelistsFormView message handlers
void CCreatePricelistsFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (! m_bInitialized )
	{

		m_edit9_1.SetLimitText(50);
		m_edit9_2.SetLimitText(25);
		m_edit9_3.SetLimitText(2000);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				m_lbl9_1.SetWindowTextW(xml.str(IDS_STRING4500));
				m_lbl9_2.SetWindowTextW(xml.str(IDS_STRING4501));
				m_lbl9_3.SetWindowTextW(xml.str(IDS_STRING4502));
				m_lbl9_4.SetWindowTextW(xml.str(IDS_STRING4503));

				m_sMsgCap = xml.str(IDS_STRING99);

				m_sMsgRemoveSpecies1 = xml.str(IDS_STRING4257);
				m_sMsgRemoveSpecies2 = xml.str(IDS_STRING4258);
				m_sMsgRemoveTable1 = xml.str(IDS_STRING45552);
				m_sMsgRemoveTable2 = xml.str(IDS_STRING45560);
				m_sMsgMissingData1.Format(L"%s\n%s\n\n%s\n\n%s\n\n%s\n\n",
					xml.str(IDS_STRING4571),
					xml.str(IDS_STRING4572),
					xml.str(IDS_STRING4560),
					xml.str(IDS_STRING4561),
					xml.str(IDS_STRING4562));
				m_sMsgMissingData2.Format(L"%s\n%s\n\n%s\n\n%s\n\n",
					xml.str(IDS_STRING4571),
					xml.str(IDS_STRING4572),
					xml.str(IDS_STRING4560),
					xml.str(IDS_STRING4561));
				m_sMsgPricelistInTmpl.Format(L"%s\n%s\n\n",
					xml.str(IDS_STRING4563),
					xml.str(IDS_STRING4564));

				m_sMsgPricelistNameAlreadyUsed1.Format(L"%s\n%s\n\n%s\n\n",
					xml.str(IDS_STRING4565),
					xml.str(IDS_STRING4566),
					xml.str(IDS_STRING4567));
				m_sMsgPricelistNameAlreadyUsed2.Format(L"%s\n%s\n\n",
					xml.str(IDS_STRING4565),
					xml.str(IDS_STRING4566));
				m_sMsgDeafultSettingsMissing.Format(L"%s\n%s\n%s\n\n",
					xml.str(IDS_STRING4568),
					xml.str(IDS_STRING4569),
					xml.str(IDS_STRING4570));
				m_sMsgPricelistIsMissingData.Format(L"%s\n%s\n\n%s\n\n",
					xml.str(IDS_STRING4573),
					xml.str(IDS_STRING4574),
					xml.str(IDS_STRING4575));
				m_sMsgNotAPricelistFile.Format(L"%s\n%s\n\n",
					xml.str(IDS_STRING4579),
					xml.str(IDS_STRING4580));


				m_sExportPrlFilter = xml.str(IDS_STRING4577);
				m_sImportPrlFilter = xml.str(IDS_STRING4578);

			}		
		}

		getDLLVolumeFuncDesc(m_vecFuncDesc,m_vecUserVolTables);

		if (m_vecVecPricelist.size() > 0)
		{
			m_nSelectedIndex = m_vecVecPricelist.size()-1;
			enableView(TRUE);
			setNavigationButtons(m_nSelectedIndex > 0,m_nSelectedIndex < (m_vecVecPricelist.size()-1));

			m_bEnableToolBarBtnSave = TRUE;
			m_bEnableToolBarBtnDelete = TRUE;
			m_bEnableToolBarBtnAddSpc = TRUE;
			m_bEnableToolBarBtnDelSpc = TRUE;

			m_bEnableToolBarBtnImport = TRUE;
			m_bEnableToolBarBtnExport = TRUE;
			m_bEnableToolBarBtnPreview = TRUE;

		}
		else
		{
			m_nSelectedIndex = -1;
			enableView(FALSE);
			setNavigationButtons(FALSE,FALSE);

			m_bEnableToolBarBtnSave = FALSE;
			m_bEnableToolBarBtnDelete = FALSE;
			m_bEnableToolBarBtnAddSpc = FALSE;
			m_bEnableToolBarBtnDelSpc = FALSE;
			m_bEnableToolBarBtnImport = TRUE;
			m_bEnableToolBarBtnExport = FALSE;
			m_bEnableToolBarBtnPreview = FALSE;

		}

		populateData();

		m_bInitialized = TRUE;
	}
}

void CCreatePricelistsFormView::OnSize(UINT nType, int cx, int cy)
{
	if (m_wndTabControl.GetSafeHwnd())
	{
		setResize(&m_wndTabControl,2,130,cx-2,cy-132);
	}

	CXTResizeFormView::OnSize(nType, cx, cy);
}

BOOL CCreatePricelistsFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{

	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		if (m_dbConnectionData.conn->isConnected())
		{
			m_pDB = new CDBHandling(m_dbConnectionData);

			if (m_pDB != NULL)
			{
				m_pDB->getSpecies(m_vecSpecies);
				m_pDB->getGrades(m_vecGrades);
				m_pDB->getDefaults(m_vecDefaults);
				m_pDB->getPricelist(m_vecVecPricelist);
				m_pDB->getUserVolTables(m_vecUserVolTables);
				m_pDB->getTmplTables(m_vecLogScaleTemplates);
			}
		}
	}

	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CCreatePricelistsFormView::OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult)
{
	UNUSED_ALWAYS(pNMHDR);
	*pResult = 0;

	CXTPTabManagerItem *pItem = m_wndTabControl.getSelectedTabPage();
	if (pItem != NULL)
	{
		m_nSelectedSpeciesTab = pItem->GetIndex();
	}
}

void CCreatePricelistsFormView::OnCommand(UINT nID)
{
	switch(nID)
	{
		case ID_BUTTON32799 : New(); break;
		case ID_BUTTON32800 :	m_bOnlySave = TRUE; Save(); m_bOnlySave = FALSE; break;
		case ID_BUTTON32801 : Delete(); break;
		case ID_BUTTON32804 : Import(); break;
		case ID_BUTTON32805 : Export(); break;
	};
}

void CCreatePricelistsFormView::OnCommandTools(UINT nID)
{
	switch(nID)
	{
		case ID_TOOLS_PRICELIST : UpdateSpeciesGrades(); break;
	};
}

void CCreatePricelistsFormView::OnUpdateToolbar(CCmdUI* pCmdUI)
{
	if (pCmdUI->m_nID == ID_BUTTON32800)
		pCmdUI->Enable(m_bEnableToolBarBtnSave);
	if (pCmdUI->m_nID == ID_BUTTON32801)
		pCmdUI->Enable(m_bEnableToolBarBtnDelete);
	if (pCmdUI->m_nID == ID_BUTTON32802)
		pCmdUI->Enable(m_bEnableToolBarBtnAddSpc);
	if (pCmdUI->m_nID == ID_BUTTON32803)
		pCmdUI->Enable(m_bEnableToolBarBtnDelSpc);
	if (pCmdUI->m_nID == ID_BUTTON32804)
		pCmdUI->Enable(m_bEnableToolBarBtnImport);
	if (pCmdUI->m_nID == ID_BUTTON32805)
		pCmdUI->Enable(m_bEnableToolBarBtnExport);
	if (pCmdUI->m_nID == ID_BUTTON32806)
		pCmdUI->Enable(m_bEnableToolBarBtnPreview);
}

LRESULT CCreatePricelistsFormView::OnOpenSuiteArgMessage(WPARAM wParam,LPARAM lParam)
{
	return 0L;
}

LRESULT CCreatePricelistsFormView::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	switch (wParam)
	{
		case ID_DBNAVIG_LIST:
		{
			showFormView(ID_LIST_PRL,m_sLangFN,0,0);
			break;
		}
		case ID_NEW_ITEM :
		{
			break;
		}	// case ID_NEW_ITEM :

		case ID_SAVE_ITEM :
		{
			break;
		}	// case ID_SAVE_ITEM :
		
		case ID_DELETE_ITEM :
		{
			break;
		}	// case ID_DELETE_ITEM :

		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		{
			m_bOnlySave = FALSE;
			if (Save())
			{
				m_nSelectedIndex = 0;
				populateData();
				setNavigationButtons(FALSE,TRUE);
			}
			break;
		}
		case ID_DBNAVIG_PREV :
		{
			m_bOnlySave = FALSE;
			if (Save())
			{
				m_nSelectedIndex--;
				if (m_nSelectedIndex < 0)
					m_nSelectedIndex = 0;

				populateData();

				if (m_nSelectedIndex == 0)
				{
					setNavigationButtons(FALSE,TRUE);
				}
				else
				{
					setNavigationButtons(TRUE,TRUE);
				}
			}
			break;
		}
		case ID_DBNAVIG_NEXT :
		{
			m_bOnlySave = FALSE;
			if (Save())
			{
				m_nSelectedIndex++;
				if (m_nSelectedIndex > ((long)m_vecVecPricelist.size() - 1))
					m_nSelectedIndex = (long)m_vecVecPricelist.size() - 1;

				populateData();
						
				if (m_nSelectedIndex == (long)m_vecVecPricelist.size() - 1)
				{
					setNavigationButtons(TRUE,FALSE);
				}
				else
				{
					setNavigationButtons(TRUE,TRUE);
				}
			}
			break;
		}
		case ID_DBNAVIG_END :
		{
			m_bOnlySave = FALSE;
			if (Save())
			{
				m_nSelectedIndex = (long)m_vecVecPricelist.size()-1;
				populateData();
				setNavigationButtons(TRUE,FALSE);	
			}
			break;
		}	// case ID_NEW_ITEM :
	}	// switch (wParam)
	return 0L;
}

// CCreatePricelistsFormView diagnostics

#ifdef _DEBUG
void CCreatePricelistsFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CCreatePricelistsFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CCreatePricelistsFormView message handlers

void CCreatePricelistsFormView::doReportViewClick(NMHDR * pNotifyStruct)
{
	CRect rect;
	POINT pt;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	// prevents us from crashing when clicking inside the report, but not on an item
	if(pItemNotify == NULL) 
		return;

	// prevents us from crashing when clicking inside the report, but not on an item
	if(pItemNotify->pItem == NULL)
		return;

	switch (pItemNotify->pColumn->GetItemIndex())
	{
			case COLUMN_2 :
			{
				// Do a hit-test; 080513 p�d
				rect = pItemNotify->pColumn->GetRect();		
				pt = pItemNotify->pt;
				// Check if the user clicked on the Icon or not; 080513 p�d
				if (hitTest_X(pt.x,rect.left,13))
				{
					CXTPReportRow *pRow = getReportView()->GetReportCtrl().GetFocusedRow();
					if (pRow != NULL)
					{
						CPricelistReportRec *pRec = (CPricelistReportRec *)pRow->GetRecord();
						if (pRec != NULL)
						{
							CSelectCalcTypeDlg *pDlg = new CSelectCalcTypeDlg();
							if (pDlg != NULL)
							{
								pDlg->setIfPulpwood(isGradePulpwood(pRec->getIconColText(COLUMN_0)));
								pDlg->setVecFuncDesc(m_vecFuncDesc,pRec->getIconColText(COLUMN_2),pRec->getFuncID());	//#4258 skickar med FuncID
								if (pDlg->DoModal() == IDOK)
								{
									pRec->setIconColText(COLUMN_2,pDlg->getSelectedCalcBasis());
									pRec->setFuncID(pDlg->getFuncID());	//#4258 sparar FuncID
								}
								delete pDlg;
								getReportView()->GetReportCtrl().Populate();
								getReportView()->GetReportCtrl().UpdateWindow();
							}
						}
					}
				}	// if (hitTest_X(pt.x,rect.left,13))
			}
			break;
	};

}

void CCreatePricelistsFormView::doSave()
{
	m_bOnlySave = FALSE;
	createData();
}

void CCreatePricelistsFormView::doSetNavigationButtons()
{
	setNavigationButtons(m_nSelectedIndex > 0,m_nSelectedIndex < (m_vecVecPricelist.size()-1));
}

void CCreatePricelistsFormView::doPopulate(CPricelist& rec)
{
	if (m_vecVecPricelist.size() > 0)
	{
		for (UINT i = 0;i < m_vecVecPricelist.size();i++)
		{
			if (rec.getPrlID() == m_vecVecPricelist[i].getPrlID())
			{
				m_nSelectedIndex = i;
				break;
			}
		}
		populateData();
	}
}


void CCreatePricelistsFormView::New()
{
	if (m_vecVecPricelist.size() > 0)
	{
		createData();
	}
	// We'll check if there's data set for default volume-functions
	// for sawlog and pulpwood

	if (getDefaults(DEF_SAWLOG,m_vecDefaults,m_vecFuncDesc).IsEmpty() || 
			getDefaults(DEF_PULPWOOD,m_vecDefaults,m_vecFuncDesc).IsEmpty())
	{
		::MessageBox(GetSafeHwnd(),m_sMsgDeafultSettingsMissing,m_sMsgCap,MB_ICONSTOP | MB_OK);
	}

	AddSpeciesAndGrades();
	m_bEnableToolBarBtnSave = TRUE;
	m_bEnableToolBarBtnDelete = TRUE;
	m_bEnableToolBarBtnAddSpc = TRUE;
	m_bEnableToolBarBtnDelSpc = TRUE;

	m_nSelectedIndex = (long)m_vecVecPricelist.size()-1;
	setNavigationButtons(TRUE,FALSE);	

}

BOOL CCreatePricelistsFormView::Save()
{
	BOOL bReturn = FALSE;
	QUIT_TYPES::Q_T_RETURN cst;
	if (cst = checkDataBeforeSave(CHECK_SAVE_TYPES::CHECK_SAVE_NO_QUIT))
	{
		if (cst == QUIT_TYPES::DO_SAVE)
		{
			createData();
			bReturn = TRUE;
		}
		else if (cst == QUIT_TYPES::QUIT_ANYWAY)
		{
			bReturn = TRUE;
		}
	}

	return bReturn;
}

void CCreatePricelistsFormView::Delete()
{
	CString sMsg;

	if (isPricelistInTemplate(m_recPricelist.getPrlID()))
	{
		return;
	}

	sMsg.Format(L"%s : %s",m_sMsgRemoveTable1,m_edit9_1.getText());
	if (::MessageBox(GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONSTOP | MB_YESNO | MB_DEFBUTTON2) == IDYES)
	{
		if (::MessageBox(GetSafeHwnd(),m_sMsgRemoveTable2,m_sMsgCap,MB_ICONSTOP | MB_YESNO | MB_DEFBUTTON2) == IDYES)
		{
			if (m_pDB != NULL)
			{
				if (m_pDB->delPricelist(m_recPricelist.getPrlID()))
				{
					m_pDB->getPricelist(m_vecVecPricelist);
					m_nSelectedIndex = m_vecVecPricelist.size()-1;
					populateData();
					// Check if table is empty
					if (m_vecVecPricelist.size() == 0)
					{
						m_bEnableToolBarBtnSave = FALSE;
						m_bEnableToolBarBtnDelete = FALSE;

						// Remove Tabs
						removeSpecies(CCreatePricelistsFormView::DEL_ALL_SPECIES);

						clearView();

						enableView(FALSE);

						// Reseed IDENTITY-field (i.e. reset)
						m_pDB->resetPricelistIdentityField();
					}
					setNavigationButtons(m_nSelectedIndex > 0,m_nSelectedIndex < (m_vecVecPricelist.size()-1));
					m_bEnableToolBarBtnSave = m_vecVecPricelist.size() > 0;
					m_bEnableToolBarBtnDelete = m_vecVecPricelist.size() > 0;
					m_bEnableToolBarBtnAddSpc = m_vecVecPricelist.size() > 0;
					m_bEnableToolBarBtnExport = m_vecVecPricelist.size() > 0;

				}
			}
		}
	}
}

BOOL CCreatePricelistsFormView::AddSpeciesAndGrades()
{
	BOOL bReturn = FALSE;
	CString sSpc = L"";
	CSelectSpeciesAndGradesDlg *dlg = new CSelectSpeciesAndGradesDlg();
	if (dlg != NULL)
	{
		m_vecSpeciesGradesAndPrice.clear();
		m_vecSpeciesSelected.clear();
		m_vecGradesSelected.clear();
		dlg->setSpecies(m_vecSpecies);
		dlg->setGrades(m_vecGrades);
		dlg->setSelectedSpecies(m_vecSpeciesSelected);
		dlg->setSelectedGrades(m_vecGradesSelected);
		if (dlg->DoModal() == IDOK)
		{
			bReturn = TRUE;
			m_recPricelist = CPricelist();
			enableView(TRUE);
			clearView();

			dlg->getSelectedSpecies(m_vecSpeciesSelected);
			dlg->getSelectedGrades(m_vecGradesSelected);
			addSpeciesToTabs();
		}

		delete dlg;
	}
	
	return bReturn;
}

void CCreatePricelistsFormView::UpdateSpeciesGrades()
{
	CXTPReportRows *pRows = NULL;
	CPricelistReportRec* pRec = NULL;
	BOOL bReturn = FALSE;
	CString sSpc = L"";

	//createData();
	// We'll check if there's data set for default volume-functions
	// for sawlog and pulpwood

	if (getDefaults(DEF_SAWLOG,m_vecDefaults,m_vecFuncDesc).IsEmpty() || 
			getDefaults(DEF_PULPWOOD,m_vecDefaults,m_vecFuncDesc).IsEmpty())
	{
		::MessageBox(GetSafeHwnd(),m_sMsgDeafultSettingsMissing,m_sMsgCap,MB_ICONSTOP | MB_OK);
	}

	// Get selected species and grades from TabControl and ReportViews
	m_vecSpeciesSelected.clear();
	m_vecGradesSelected.clear();

	for (int tab = 0;tab < m_wndTabControl.getNumOfTabPages();tab++)
	{
		CReportCreatePricelistView *pSelectedView = getReportView(tab);
		if (pSelectedView != NULL)
		{
			CSpecies recSpc;
			getSpecies(m_mapSpcID[tab],recSpc);
			m_vecSpeciesSelected.push_back(recSpc);

			if ((pRows = pSelectedView->GetReportCtrl().GetRows()) != NULL)
			{
				for (int row = 0;row < pRows->GetCount();row++)
				{

					if ((pRec = (CPricelistReportRec*)pRows->GetAt(row)->GetRecord()) != NULL)
					{
						CGrades recGrade;
						getGrades(pRec->getGradesID(),recGrade);
						m_vecGradesSelected.push_back(recGrade);
						m_vecSpeciesGradesAndPrice.push_back(CSpeciesGradesAndPrice(recSpc.getSpcID(),recGrade.getGradesID(),pRec->getColDbl(COLUMN_1)));
					}
				}
			}
		}
	}

	if (m_pDB != NULL)
	{
		m_pDB->getSpecies(m_vecSpecies);
		m_pDB->getGrades(m_vecGrades);
	}

	CSelectSpeciesAndGradesDlg *dlg = new CSelectSpeciesAndGradesDlg();
	if (dlg != NULL)
	{
		dlg->setSpecies(m_vecSpecies);
		dlg->setGrades(m_vecGrades);
		dlg->setSelectedSpecies(m_vecSpeciesSelected);
		dlg->setSelectedGrades(m_vecGradesSelected);
		if (dlg->DoModal() == IDOK)
		{
			bReturn = TRUE;
			//m_recPricelist = CPricelist();
			//enableView(TRUE);
			//clearView();
		
			removeSpecies(CCreatePricelistsFormView::DEL_ALL_SPECIES);

			dlg->getSelectedSpecies(m_vecSpeciesSelected);
			dlg->getSelectedGrades(m_vecGradesSelected);
			addSpeciesToTabs();
		}

		delete dlg;
	}


	m_bEnableToolBarBtnSave = TRUE;
	m_bEnableToolBarBtnDelete = TRUE;
	m_bEnableToolBarBtnAddSpc = TRUE;
	m_bEnableToolBarBtnDelSpc = TRUE;
}

BOOL CCreatePricelistsFormView::AddSpecies()
{
	BOOL bReturn = FALSE;
	CString sSpc = L"";
	CSelectSpeciesDlg *dlg = new CSelectSpeciesDlg();
	if (dlg != NULL)
	{
		dlg->setSpecies(m_vecSpecies);
		if (dlg->DoModal() == IDOK)
		{
			bReturn = TRUE;
			dlg->getSelectedSpecies(m_vecSpeciesSelected);
			addSpeciesToTabs();
		}

		delete dlg;
	}

	return bReturn;
}

void CCreatePricelistsFormView::DelSpecies()
{
	removeSpecies(CCreatePricelistsFormView::DEL_SELECTED_SPECIES);
	// Check if there's no species left and advice the user
	if (m_wndTabControl.getNumOfTabPages() == 0)
	{
		::MessageBox(GetSafeHwnd(),m_sMsgPricelistIsMissingData,m_sMsgCap,MB_ICONSTOP | MB_OK);
	}
}

void CCreatePricelistsFormView::Import()
{
	BOOL bIsNameUsed = TRUE;
	long nDataFileSize = 0;
	TCHAR szName[128],szCreatedBy[128],szDate[32],szNotes[2048];
//	TCHAR szDataFile[1024*20];
	CString sTmp = L"",sName = L"",sData = L"";
	CString sToken = L"",sSpcID = L"",sSpcCode = L"",sSpcName = L"",sBarkInch = L"",sBarkMM = L"";
	CString sGrdID = L"",sGrdCode = L"",sGrdName = L"",sIsPulpwood = L"",sFuncID = L"",sPrice = L"";
	int nCnt = 1;
	CVecSpecies vecImportSpecies;
	CVecGradesExt vecImportGrades;

	sTmp.Format(L"%s (*.xml)|*.xml|",m_sImportPrlFilter);
	
	CFileDialog dlg(TRUE,L".xml",m_edit9_1.getText(),OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,sTmp);
	szName[0] = '\0';
	szCreatedBy [0] = '\0';
	szDate [0] = '\0';
	szNotes [0] = '\0';
//	szDataFile[0] = '\0';
	if (dlg.DoModal() == IDOK)
	{
		LogScalePrlParser *pars = new LogScalePrlParser();
		if (pars != NULL)
		{
			if (pars->LoadFromFile(dlg.GetPathName()))
			{
				if (!pars->isALogScalePricelist())
				{
					::MessageBox(GetSafeHwnd(),m_sMsgNotAPricelistFile,m_sMsgCap,MB_ICONSTOP | MB_OK);
					return;
				}

				pars->getHeaderName(szName);
				sName.Format(L"%s",szName);
				pars->getHeaderDate(szDate);
				pars->getHeaderDoneBy(szCreatedBy);
				pars->getHeaderNotes(szNotes);

				// Get name of pricelist in datafile and compare
				// to pricelists already in database
				while (bIsNameUsed)
				{
					bIsNameUsed = m_pDB->isPricelistNameAlreadyUsed(sName);
					if (bIsNameUsed)
					{
						sTmp.Format(L"%s(%d)",szName,nCnt);
						sName = sTmp;
						nCnt++;
					}
				}

				// * Header *
				sTmp.Format(L"%d;%s;%s;%s;",
					FUNC_INDEX::HEADER,
					sName,									// Name of pricelist
					szCreatedBy,						// Created by
					szDate);								// Date

//				nDataFileSize = pars->getDataFileSize();
//				if (nDataFileSize > 0)
//					szDataFile = (TCHAR*)calloc(nDataFileSize+4,sizeof(*szDataFile));

				TCHAR *szDataFile;
				nDataFileSize = pars->getDataFileSize() + 100;
				szDataFile = (TCHAR *)malloc(nDataFileSize * sizeof(TCHAR));

				pars->getDataFile(szDataFile);

#ifdef _DO_PRICELIST_CHECK_ON_IMPORT
				//----------------------------------------------------------------------
				// Check species in imported pricelist compared to species in DB
				// If they don't match ask user if he likes to proceed, and if so,
				// match species to already exisiting species
				CSpecies recSpc = CSpecies();
				CGrades recGrd = CGrades();
				BOOL bFound = FALSE;
				std::wstringstream ss;
				std::wstring line;
				ss << szDataFile; // sData.GetBuffer();
				while (std::getline(ss,line))
				{
					AfxExtractSubString(sToken,line.c_str(),0,';');

					// Check Species
					if (_tstoi(sToken) == FUNC_INDEX::SPECIES)
					{
						bFound = FALSE;

						sSpcID.Empty();
						sSpcCode.Empty();
						sSpcName.Empty();
						AfxExtractSubString(sSpcID,line.c_str(),1,';');
						AfxExtractSubString(sSpcCode,line.c_str(),2,';');
						AfxExtractSubString(sSpcName,line.c_str(),3,';');
						AfxExtractSubString(sBarkInch,line.c_str(),4,';');
						AfxExtractSubString(sBarkMM,line.c_str(),5,';');
						for (int i = 0;i < m_vecSpecies.size();i++)
						{
							recSpc = m_vecSpecies[i];
							vecImportSpecies.push_back(CSpecies(_tstoi(sSpcID),sSpcCode,sSpcName,_tstof(localeToStr(sBarkInch)),_tstof(localeToStr(sBarkMM))));
						}

					}	// if (_tstoi(sToken) == FUNC_INDEX::SPECIES)

					// Check Grades
					if (_tstoi(sToken) == FUNC_INDEX::DATA)
					{
						bFound = FALSE;

						AfxExtractSubString(sGrdID,line.c_str(),2,';');
						AfxExtractSubString(sPrice,line.c_str(),3,';');
						AfxExtractSubString(sFuncID,line.c_str(),4,';');
						AfxExtractSubString(sGrdCode,line.c_str(),5,';');
						AfxExtractSubString(sGrdName,line.c_str(),6,';');
						AfxExtractSubString(sIsPulpwood,line.c_str(),7,';');
						vecImportGrades.push_back(CGradesExt(_tstoi(sGrdID),sSpcCode,sGrdCode,sGrdName,_tstoi(sIsPulpwood),_tstoi(sFuncID),_tstof(localeToStr(sPrice))));
					}	// if (_tstoi(sToken) == FUNC_INDEX::DATA)
				}	// while (std::getline(ss,line))


				if (vecImportGrades.size() > 0 || vecImportSpecies.size() > 0)
				{
					CMatchPrlGrdDlg *pDlg = new CMatchPrlGrdDlg();
					if (pDlg != NULL)
					{
						pDlg->setDB(m_pDB);
						pDlg->setSpecies(m_vecSpecies);
						pDlg->setGrades(m_vecGrades);
						pDlg->setImportSpecies(vecImportSpecies);
						pDlg->setImportGrades(vecImportGrades);
						if (pDlg->DoModal() == IDOK)
						{
							enableView(TRUE);
							sData = pDlg->getData();
							m_vecVecPricelist.push_back(CPricelist(-1,sName,szCreatedBy,szDate,sData,0,szNotes));
							m_nSelectedIndex = m_vecVecPricelist.size() - 1;
							// Need to reload Species and Grades
							m_pDB->getSpecies(m_vecSpecies);
							m_pDB->getGrades(m_vecGrades);
							populateData();
							Save();
							setNavigationButtons(m_nSelectedIndex > 0,m_nSelectedIndex < (m_vecVecPricelist.size()-1));
							m_bEnableToolBarBtnSave = m_vecVecPricelist.size() > 0;
							m_bEnableToolBarBtnDelete = m_vecVecPricelist.size() > 0;
							m_bEnableToolBarBtnAddSpc = m_vecVecPricelist.size() > 0;
							m_bEnableToolBarBtnImport = m_vecVecPricelist.size() > 0;
							m_bEnableToolBarBtnExport = m_vecVecPricelist.size() > 0;


						}	// if (pDlg->DoModal() == IDOK)
					}	// if (pDlg != NULL)
				}	// if (vecImportGrades.size() > 0 || vecImportSpecies.size() > 0)

#elif
				m_vecVecPricelist.push_back(CPricelist(-1,sName,szCreatedBy,szDate,sData,0,szNotes));
				m_nSelectedIndex = m_vecVecPricelist.size() - 1;
				populateData();
				Save();
				setNavigationButtons(m_nSelectedIndex > 0,m_nSelectedIndex < (m_vecVecPricelist.size()-1));
				enableView( m_vecVecPricelist.size() > 0);
#endif
				free(szDataFile);
			}
			delete pars;
		}
	}
}

void CCreatePricelistsFormView::Export()
{
	int nFuncID = -99;
	CStringArray sarrXML;
	CString sXML = L"",sTmp = L"",sPrice = L"";

	UpdateData(TRUE);

	//------------------------------------------------------------------
	// Start creating xml pricelist file
	sarrXML.RemoveAll();
	sarrXML.Add(TAG_FIRST_ROW);
	sarrXML.Add(TAG_START_PRICELIST);
	
	// Add header information
	sarrXML.Add(TAG_START_HEADER);
	sTmp.Format(TAG_HEADER_NAME,m_edit9_1.getText());
	sarrXML.Add(sTmp);
	sTmp.Format(TAG_HEADER_DONE_BY,m_edit9_2.getText());
	sarrXML.Add(sTmp);
	sTmp.Format(TAG_HEADER_DATE,m_csDate);
	sarrXML.Add(sTmp);
	sTmp.Format(TAG_HEADER_NOTES,m_edit9_3.getText());
	sarrXML.Add(sTmp);
	sarrXML.Add(TAG_END_HEADER);

	createData(TRUE);
	sTmp.Format(TAG_DATA_FILE,m_sDataPRL);
	sarrXML.Add(sTmp);

	// End of xml file
	sarrXML.Add(TAG_END_PRICELIST);
	//------------------------------------------------------------------
	// Add data from sarrXML to sXML
	sXML.Empty();
	for (int i = 0;i < sarrXML.GetCount();i++)
	{
		sXML += sarrXML.GetAt(i);
	}

	sTmp.Format(L"%s (*.xml)|*.xml|",m_sExportPrlFilter);
	CFileDialog dlg(FALSE,L".xml",m_edit9_1.getText(),OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,sTmp);
	if (dlg.DoModal() == IDOK)
	{
		LogScalePrlParser pars;
		if (pars.LoadFromBuffer(sXML))
		{
			pars.SaveToFile(dlg.GetPathName());
		}
	}

	
}

void CCreatePricelistsFormView::PreView()
{
	AfxMessageBox(L"PreView()");
}

void CCreatePricelistsFormView::populateData()
{
	if (m_vecVecPricelist.size() > 0)
	{
		if (m_nSelectedIndex >= 0 && m_nSelectedIndex < m_vecVecPricelist.size())
		{
			removeSpecies(CCreatePricelistsFormView::DEL_ALL_SPECIES);

			m_recPricelist	= m_vecVecPricelist[m_nSelectedIndex];

			m_edit9_1.SetWindowTextW(m_recPricelist.getName());
			m_edit9_2.SetWindowTextW(m_recPricelist.getCreatedBy());
			m_csDate = m_recPricelist.getDate();
			m_edit9_3.SetWindowTextW(m_recPricelist.getNotes());

			UpdateData(FALSE);

			// Interpret template data and setup tabs

			setupSpeciesTabs();

			setupPricelistPerSpecies();
		}
	}	// if (m_vecLogScaleTemplates.size() > 0)
}

int CCreatePricelistsFormView::getGradesIDFromCode(LPCTSTR code)
{
	CString sTmp(code);
	if (m_vecGrades.size() > 0)
	{
		for (UINT i = 0;i < m_vecGrades.size();i++)
		{
			if (sTmp.CompareNoCase(m_vecGrades[i].getGradesCode()) == 0)
				return m_vecGrades[i].getGradesID();
		}
	}
	return -1;
}

CString CCreatePricelistsFormView::getGradesNameFromCode(LPCTSTR code)
{
	CString sTmp(code);
	if (m_vecGrades.size() > 0)
	{
		for (UINT i = 0;i < m_vecGrades.size();i++)
		{
			if (sTmp.CompareNoCase(m_vecGrades[i].getGradesCode()) == 0)
				return m_vecGrades[i].getGradesName();
		}
	}
	return L"";
}

int CCreatePricelistsFormView::getGradesIsPulpwood(LPCTSTR code)
{
	CString sTmp(code);
	if (m_vecGrades.size() > 0)
	{
		for (UINT i = 0;i < m_vecGrades.size();i++)
		{
			if (sTmp.CompareNoCase(m_vecGrades[i].getGradesCode()) == 0)
				return m_vecGrades[i].getIsPulpwood();
		}
	}
	return 0;
}

int CCreatePricelistsFormView::getFuncIDFromName(LPCTSTR namn)
{
	CString sTmp(namn);
	if (m_vecFuncDesc.size() > 0)
	{
		for (UINT i = 0;i < m_vecFuncDesc.size();i++)
		{
			if (sTmp.CompareNoCase(m_vecFuncDesc[i].getBasis()) == 0)
				return m_vecFuncDesc[i].getFuncID();
		}
	}
	return -1;
}

// Add tabs to tabcontrol
// Setup species from pricelist-table data
// Setup grades from pricelist-table data
void CCreatePricelistsFormView::setupSpeciesTabs()
{
	CSpecies recSpc = CSpecies();
	CGrades recGrade = CGrades();
	CString sData;
	int nCounter = 0,nSpcID = -1,nFirstSpcID = -1;
	BOOL bFirstSpc = FALSE;
	std::wstringstream ss;
	CString sToken = L"";
	std::wstring line;
	m_vecSpeciesSelected.clear();
	m_vecGradesSelected.clear();
	sData = m_recPricelist.getPricelist();
	ss << sData.GetBuffer();
	while (std::getline(ss,line))
	{
		AfxExtractSubString(sToken,line.c_str(),0,';');
		if (_tstoi(sToken) == FUNC_INDEX::SPECIES)
		{
			AfxExtractSubString(sToken,line.c_str(),1,';');
			if (getSpecies(_tstoi(sToken),recSpc))
			{
				nSpcID = _tstoi(sToken);
				m_vecSpeciesSelected.push_back(recSpc);
			}
		}
		if (_tstoi(sToken) == FUNC_INDEX::DATA)
		{
			AfxExtractSubString(sToken,line.c_str(),2,';');
			if (getGrades(_tstoi(sToken),recGrade))
			{
				if (nFirstSpcID == -1 || nFirstSpcID == nSpcID)
				{
					m_vecGradesSelected.push_back(recGrade);
					nFirstSpcID= nSpcID;
				}
			}
		}
	}
	addSpeciesToTabs();

}

void CCreatePricelistsFormView::setupPricelistPerSpecies()
{
	CStringArray sarrData;
	int nRowCnt = 0;
	std::wstringstream ss;
	CString sData = L"";
	CString sToken = L"";
	CString sSpcID = L"";
	CString sPrice = L"";
	CString sBasis = L"";
	std::wstring line;
	BOOL bSpeciesMatchTab = FALSE;

	int nNumOfTabs = m_wndTabControl.getNumOfTabPages();

	CXTPReportRow *pRow = NULL;

	CPricelistReportRec *pRec = NULL;

	sData = m_recPricelist.getPricelist();
	ss << sData.GetBuffer();
	while (std::getline(ss,line))
	{
		AfxExtractSubString(sToken,line.c_str(),0,';');
		if (_tstoi(sToken) == FUNC_INDEX::DATA)
		{
			sarrData.Add(line.c_str());
		}
	}

	for (int nTab = 0;nTab < nNumOfTabs;nTab++)
	{
		nRowCnt = 0;
		CReportCreatePricelistView *pSelectedView = getReportView(nTab);
		if (pSelectedView != NULL)
		{
			for (int i = 0;i < sarrData.GetCount();i++)
			{

				AfxExtractSubString(sToken,sarrData.GetAt(i),0,';');
				if (_tstoi(sToken) == FUNC_INDEX::DATA)
				{
					AfxExtractSubString(sSpcID,sarrData.GetAt(i),1,';');
					if (_tstoi(sSpcID) == m_mapSpcID[nTab])
					{
						AfxExtractSubString(sPrice,sarrData.GetAt(i),3,';');
						AfxExtractSubString(sBasis,sarrData.GetAt(i),4,';');
						pRec = (CPricelistReportRec*)pSelectedView->GetReportCtrl().GetRecords()->GetAt(nRowCnt);
						if (pRec != NULL)
						{
							pRec->setColDbl(COLUMN_1,_tstof(sPrice));
							pRec->setIconColText(COLUMN_2,getCalculationBasis(_tstoi(sBasis)));
							pRec->setFuncID(_tstoi(sBasis));	//#4258 spara undan v�rdet p� FuncID p� pRec
						}
						nRowCnt++;
					}
				}
			}

			pSelectedView->GetReportCtrl().Populate();
			pSelectedView->GetReportCtrl().UpdateWindow();
		}
	}
}

BOOL CCreatePricelistsFormView::getSpecies(int id,CSpecies& rec)
{
	BOOL bFound = FALSE;
	if (m_vecSpecies.size() > 0)
	{
		for (UINT i = 0;i < m_vecSpecies.size();i++)
		{
			rec = m_vecSpecies[i];
			if (rec.getSpcID() == id)
			{
				bFound = TRUE;			
				break;
			}
		}
	}

	return bFound;
}

BOOL CCreatePricelistsFormView::getGrades(int id,CGrades& rec)
{
	BOOL bFound = FALSE;
	if (m_vecGrades.size() > 0)
	{
		for (UINT i = 0;i < m_vecGrades.size();i++)
		{
			rec = m_vecGrades[i];
			if (rec.getGradesID() == id)
			{
				bFound = TRUE;			
				break;
			}
		}
	}

	return bFound;
}

CString CCreatePricelistsFormView::getCalculationBasis(int id)
{
	CString sBasis = L"";
	if (m_vecFuncDesc.size() > 0)
	{
		for (UINT i = 0;i < m_vecFuncDesc.size();i++)
		{
			if (m_vecFuncDesc[i].getFuncID() == id)
			{
				sBasis = m_vecFuncDesc[i].getBasis();
				break;
			}
		}
	}

	return sBasis;
}

BOOL CCreatePricelistsFormView::isPricelistInTemplate(int prl_id)
{
	if (m_vecLogScaleTemplates.size() > 0)
	{
		for (UINT i = 0;i < m_vecLogScaleTemplates.size();i++)
		{
			if (m_vecLogScaleTemplates[i].getPrlID() == prl_id)
			{
				::MessageBox(GetSafeHwnd(),m_sMsgPricelistInTmpl,m_sMsgCap,MB_ICONSTOP | MB_OK);
				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CCreatePricelistsFormView::isGradePulpwood(LPCTSTR  grade_code)
{
	if (m_vecGrades.size() > 0)
	{
		for (UINT i = 0;i < m_vecGrades.size();i++)
		{
			if (m_vecGrades[i].getGradesCode().CompareNoCase( grade_code) == 0)
				return (m_vecGrades[i].getIsPulpwood() == 1);
		}
	}
	return FALSE;
}

BOOL CCreatePricelistsFormView::isVolumeFunctionsMissing()
{
	int nFuncID = -99;
	CXTPReportRows *pRows = NULL;
	CReportCreatePricelistView *pSelectedView = NULL;
	CPricelistReportRec *pRec = NULL;
	if (m_wndTabControl.getNumOfTabPages() > 0)
	{

		for (int nTab = 0;nTab < m_wndTabControl.getNumOfTabPages();nTab++)
		{
			if ((pSelectedView = (CReportCreatePricelistView*)getReportView(nTab)) != NULL)
			{
				pRows = pSelectedView->GetReportCtrl().GetRows();
				if (pRows != NULL)
				{
					for (int ii = 0;ii < pRows->GetCount();ii++)
					{
						if ((pRec = (CPricelistReportRec*)pRows->GetAt(ii)->GetRecord()) != NULL)
						{
							//nFuncID = getFuncIDFromName(pRec->getIconColText(COLUMN_2)); 
							nFuncID = pRec->getFuncID();	//#4258 h�mtar FuncID
							if(nFuncID <= 0)
							{
								//blir inte riktigt bra n�r man g�r �ver fr�n gammal databas, kolla om kan h�mta ut FuncID fr�n basisnamnet ist�llet, finns liten risk att det kan bli fel, g�ra om?
								nFuncID = getFuncIDFromName(pRec->getIconColText(COLUMN_2));
							}
							if (nFuncID == -1)	
							{
								return TRUE;
							}
						}	// if ((pRec = (CPricelistReportRec*)pRows->GetAt(ii)->GetRecord()) != NULL)										
					}	// for (int ii = 0;ii < pRows->GetCount();ii++)
				}	// if (pRows != NULL)
			}
		}
	}
	return FALSE;
}

QUIT_TYPES::Q_T_RETURN CCreatePricelistsFormView::isNameOfPricelistOK(LPCTSTR name)
{
	if (m_vecVecPricelist.size() > 0)
	{
		for (UINT i = 0;i < m_vecVecPricelist.size();i++)
		{
			if (m_vecVecPricelist[i].getName().CompareNoCase(name) == 0 &&
					m_recPricelist.getPrlID() != m_vecVecPricelist[i].getPrlID())
			{
				::MessageBox(GetSafeHwnd(),m_sMsgPricelistNameAlreadyUsed2,m_sMsgCap,MB_ICONSTOP | MB_OK);
				return QUIT_TYPES::NO_SAVE;
			}
		}	// for (UINT i = 0;i < m_vecLogScaleTemplates.size();i++)
	}	// if (m_vecLogScaleTemplates.size() > 0)

	return QUIT_TYPES::DO_SAVE;
}


QUIT_TYPES::Q_T_RETURN CCreatePricelistsFormView::checkDataBeforeSave(CHECK_SAVE_TYPES::CHECK_SAVE cs)
{
	QUIT_TYPES::Q_T_RETURN qt;

	CString sMsg = L"";

	if (m_nSelectedIndex == -1 && !m_wndTabControl.IsWindowEnabled())
		return QUIT_TYPES::QUIT_ANYWAY;


	if ((qt = isNameOfPricelistOK(m_edit9_1.getText())) != QUIT_TYPES::DO_SAVE)
		return qt;


#ifdef _FORCE_ERROR_CORRECTION_PRICELIST

		sMsg = m_sMsgMissingData2;

		if (m_edit9_1.getText().IsEmpty() || m_wndTabControl.getNumOfTabPages() == 0 || isVolumeFunctionsMissing())
		{
			::MessageBox(GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONSTOP | MB_OK);
			return QUIT_TYPES::NO_SAVE;
		}
		else
		{
			return QUIT_TYPES::DO_SAVE;
		}
#else

	if (cs == CHECK_SAVE_TYPES::CHECK_SAVE_ON_QUIT)
	{
		sMsg = m_sMsgMissingData1;
		if (m_vecVecPricelist.size() == 0 && !isViewEnabled())
			return QUIT_TYPES::QUIT_ANYWAY;	// Nothing to save


		if (m_edit9_1.getText().IsEmpty() || m_wndTabControl.getNumOfTabPages() == 0 || isVolumeFunctionsMissing() || !m_bIsNameOfPricelistOK)
		{
			if (::MessageBox(GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONSTOP | MB_YESNO | MB_DEFBUTTON2) == IDNO)
				return QUIT_TYPES::NO_SAVE;
			else
				return QUIT_TYPES::QUIT_ANYWAY;
		}
		else
		{
			return QUIT_TYPES::DO_SAVE;
		}

	}
	else if (cs == CHECK_SAVE_TYPES::CHECK_SAVE_NO_QUIT)
	{
		if (!m_bIsNameOfPricelistOK)
		{
			sMsg.Format(L"%s%s\n\n",sMsg,m_sMsgPricelistNameAlreadyUsed2);
		}
		else
		{
			sMsg = m_sMsgMissingData2;
		}

		//if (m_vecUserVolTables.size() == 0)
	//	return QUIT_TYPES::Q_T_RETURN::NO_SAVE;

		if (m_edit9_1.getText().IsEmpty() || m_wndTabControl.getNumOfTabPages() == 0 || isVolumeFunctionsMissing() || !m_bIsNameOfPricelistOK)
		{
			::MessageBox(GetSafeHwnd(),m_sMsgMissingData2,m_sMsgCap,MB_ICONSTOP | MB_OK);
			return QUIT_TYPES::NO_SAVE;
		}
		else
		{
			return QUIT_TYPES::DO_SAVE;
		}

	}
#endif

	return QUIT_TYPES::NO_SAVE;
}

CReportCreatePricelistView *CCreatePricelistsFormView::getReportView(void)
{
	CXTPTabManagerItem *pManager = m_wndTabControl.getSelectedTabPage();
	if (pManager)
	{
		CReportCreatePricelistView* pView = DYNAMIC_DOWNCAST(CReportCreatePricelistView, CWnd::FromHandle(pManager->GetHandle()));
		ASSERT_KINDOF(CReportCreatePricelistView, pView);
		return pView;
	}
	return NULL;
}

CReportCreatePricelistView *CCreatePricelistsFormView::getReportView(int idx)
{
	if (idx >= 0 && idx < m_wndTabControl.getNumOfTabPages())
	{
		CXTPTabManagerItem *pManager = m_wndTabControl.getTabPage(idx);
		if (pManager)
		{
			CReportCreatePricelistView* pView = DYNAMIC_DOWNCAST(CReportCreatePricelistView, CWnd::FromHandle(pManager->GetHandle()));
			ASSERT_KINDOF(CReportCreatePricelistView, pView);
			return pView;
		}
	}
	return NULL;
}


// Skapa data-filen som sparas i databasen
void CCreatePricelistsFormView::createData(BOOL only_create)
{
	int nFuncID = -1;
	CStringArray sarr;	// Get column-data (e.g. length(s) etc.)
	CString sDataUVTables = L"",sTmp = L"";
	CXTPReportRows *pRows = NULL;
	CPricelistReportRec *pRec = NULL;
	CReportCreatePricelistView *pSelectedView = NULL;

	UpdateData(TRUE);

	m_sDataPRL.Empty();
	if (m_wndTabControl.getNumOfTabPages() > 0)
	{

		if (!only_create)
		{
			// * Header *+
			sTmp.Format(L"%d;%s;%s;%s;",
				FUNC_INDEX::HEADER,
				m_edit9_1.getText(),		// Name of pricelist
				m_edit9_2.getText(),		// Created by
				m_csDate);									// Date
			sarr.Add(sTmp);
		}

		for (int nTab = 0;nTab < m_wndTabControl.getNumOfTabPages();nTab++)
		{
			if ((pSelectedView = (CReportCreatePricelistView*)getReportView(nTab)) != NULL)
			{

				// * Species *
				if (nTab >= 0 && nTab < m_mapSpcID.size())
				{
					CSpecies recSpc = CSpecies();
					getSpecies(m_mapSpcID[nTab],recSpc);
					sTmp.Format(L"%d;%d;%s;%s;%.1f;%.0f;",FUNC_INDEX::SPECIES,m_mapSpcID[nTab],recSpc.getSpcCode(),recSpc.getSpcName(),recSpc.getBarkReductionInch(),recSpc.getBarkReductionMM());
					sarr.Add(sTmp);
				}
			
				// * Pricelist for specie *
				pRows = pSelectedView->GetReportCtrl().GetRows();
				if (pRows != NULL)
				{
					for (int ii = 0;ii < pRows->GetCount();ii++)
					{
					
						if ((pRec = (CPricelistReportRec*)pRows->GetAt(ii)->GetRecord()) != NULL)
						{
							//nFuncID = getFuncIDFromName(pRec->getIconColText(COLUMN_2));
							nFuncID = pRec->getFuncID();	//#4258 h�mtar FuncID
							if(nFuncID <= 0)
							{
								//blir inte riktigt bra n�r man g�r �ver fr�n gammal databas, kolla om kan h�mta ut FuncID fr�n basisnamnet ist�llet, finns liten risk att det kan bli fel, g�ra om?
								nFuncID = getFuncIDFromName(pRec->getIconColText(COLUMN_2));
							}

							if (!only_create)
							{
								sTmp.Format(L"%d;%d;%d;%.3f;%d;",
									FUNC_INDEX::DATA,
									m_mapSpcID[nTab],
									getGradesIDFromCode(pRec->getColText(COLUMN_0)),
									pRec->getColDbl(COLUMN_1),
									nFuncID);
							}
							else
							{
								sTmp.Format(L"%d;%d;%d;%.3f;%d;%s;%s;%d;",
									FUNC_INDEX::DATA,
									m_mapSpcID[nTab],
									getGradesIDFromCode(pRec->getColText(COLUMN_0)),
									pRec->getColDbl(COLUMN_1),
									nFuncID,
									pRec->getColText(COLUMN_0),
									getGradesNameFromCode(pRec->getColText(COLUMN_0)),
									getGradesIsPulpwood(pRec->getColText(COLUMN_0)));
							}
							sarr.Add(sTmp);
						}	// if ((pRec = (CPricelistReportRec*)pRows->GetAt(ii)->GetRecord()) != NULL)										
					}	// for (int ii = 0;ii < pRows->GetCount();ii++)
				}	// if (pRows != NULL)
			}	// if ((pSelectedView = (CReportSpcView*)getReportSpcView(i)) != NULL)
		}	// for (int i = 0;i < m_wndTabControl.getNumOfTabPages();i++)

		for (int i1 = 0;i1 < sarr.GetCount();i1++)
			m_sDataPRL += sarr.GetAt(i1) + L"\n";

		if (only_create) return;

		if (m_pDB != NULL)
		{
			if (m_recPricelist.getPrlID() < 0)
			{
				m_pDB->newPricelist(CPricelist(-1,m_edit9_1.getText(),m_edit9_2.getText(),m_csDate,m_sDataPRL,0,m_edit9_3.getText()));
				m_pDB->getPricelist(m_vecVecPricelist);
				m_nSelectedIndex = m_vecVecPricelist.size()-1;
			}
			else
			{
				m_pDB->updPricelist(CPricelist(m_recPricelist.getPrlID(),m_edit9_1.getText(),m_edit9_2.getText(),m_csDate,m_sDataPRL,0,m_edit9_3.getText()));
				m_pDB->getPricelist(m_vecVecPricelist);
		}

			if (m_nSelectedIndex >= 0 && m_nSelectedIndex < m_vecVecPricelist.size())
				m_recPricelist = m_vecVecPricelist[m_nSelectedIndex];
	
			setNavigationButtons(m_nSelectedIndex > 0,m_nSelectedIndex < (m_vecVecPricelist.size()-1));

			msgToModuleWindowOpen(L"Module6501",IDD_FORMVIEW9);

		}
	}	// if (m_wndTabControl.getNumOfTabPages() > 0)

}

void CCreatePricelistsFormView::addSpeciesToTabs()
{
	CString sSpc = L"";
	if (m_vecSpeciesSelected.size() > 0)
	{
		for (UINT i = 0;i < m_vecSpeciesSelected.size();i++)
		{
			if (!m_vecSpeciesSelected[i].getSpcName().IsEmpty())
				sSpc.Format(L"%s - %s",m_vecSpeciesSelected[i].getSpcCode(),m_vecSpeciesSelected[i].getSpcName());
			else
				sSpc.Format(L"%s",m_vecSpeciesSelected[i].getSpcCode());

			// Check that spceis isn't already in TabControl
			if (!isAlreadyOnTab(sSpc))
			{
				AddView(RUNTIME_CLASS(CReportCreatePricelistView), sSpc,-1,m_vecSpeciesSelected[i].getSpcID());
				addGradesToSpecies();
			}
		}
		if (m_nSelectedSpeciesTab >= 0 && m_nSelectedSpeciesTab < m_wndTabControl.getNumOfTabPages())
			m_wndTabControl.SetCurSel(m_nSelectedSpeciesTab);
	}
}

void CCreatePricelistsFormView::addGradesToSpecies()
{
	double fPrice = 0.0;
	int nSpcID = -1,nDefWood = -1;
	int nNumOfTabs = m_wndTabControl.getNumOfTabPages();
	CReportCreatePricelistView *pSelectedView = getReportView(nNumOfTabs-1);
	if (pSelectedView != NULL)
	{
		int nTabIndex = m_wndTabControl.getTabPage(nNumOfTabs-1)->GetIndex();
		if (m_vecGradesSelected.size() > 0)
		{
			for (UINT i = 0;i < m_vecGradesSelected.size();i++)
			{
				if (nTabIndex >= 0 && nTabIndex < m_mapSpcID.size())
					nSpcID = m_mapSpcID[nTabIndex];
				nDefWood = ((m_vecGradesSelected[i].getIsPulpwood() == 0) ? DEF_SAWLOG : DEF_PULPWOOD);
				// Also try to find price for species and grades
				if (m_vecSpeciesGradesAndPrice.size() > 0)
				{
					for (UINT i1 = 0;i1 < m_vecSpeciesGradesAndPrice.size();i1++)
					{
						if (m_vecSpeciesGradesAndPrice[i1].getSpeciesID() == nSpcID &&
								m_vecSpeciesGradesAndPrice[i1].getGradesID() == m_vecGradesSelected[i].getGradesID())
						{
							fPrice = m_vecSpeciesGradesAndPrice[i1].getPrice();
						}
					}
				}


				pSelectedView->GetReportCtrl().AddRecord(new CPricelistReportRec(m_vecGradesSelected[i].getGradesID(),
																																				 m_vecGradesSelected[i].getGradesCode(),
																																				 fPrice,
																																				 getDefaults(nDefWood,m_vecDefaults,m_vecFuncDesc),	
																																				 getDefaultFuncID(nDefWood,m_vecDefaults)));	//#4258 lagt till funcid
			}
		}
		pSelectedView->GetReportCtrl().Populate();
		pSelectedView->GetReportCtrl().UpdateWindow();
	}
}

void CCreatePricelistsFormView::OnEnChangeEdit91()
{
	// Check name of template
//	m_bIsNameOfPricelistOK = (isNameOfPricelistOK(m_edit9_1.getText()) == QUIT_TYPES::DO_SAVE);
}


void CCreatePricelistsFormView::enableView(BOOL enable)
{
	m_edit9_1.EnableWindow(enable);
	m_edit9_1.SetReadOnly(!enable);
	m_edit9_2.EnableWindow(enable);
	m_edit9_2.SetReadOnly(!enable);
	m_edit9_3.EnableWindow(enable);
	m_edit9_3.SetReadOnly(!enable);
	m_dtDate.EnableWindow(enable);
	m_wndTabControl.EnableWindow(enable);
}

void CCreatePricelistsFormView::clearView()
{
	m_edit9_1.SetWindowTextW(L"");
	m_edit9_2.SetWindowTextW(getUserName().MakeUpper());
	m_edit9_3.SetWindowTextW(L"");

	removeSpecies(CCreatePricelistsFormView::DEL_ALL_SPECIES);
}

// Set navigation buttons in shell, depending on index of item to display; 061002 p�d
void CCreatePricelistsFormView::setNavigationButtons(BOOL start_prev,BOOL end_next)
{
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
	
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);
	AfxGetMainWnd()->UpdateWindow();
}

void CCreatePricelistsFormView::removeSpecies(DEL_SPECIES del_species)
{
	CString sMsg = L"";
	CXTPTabManagerItem *pItem = NULL;

	if (del_species == CCreatePricelistsFormView::DEL_SELECTED_SPECIES)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.getSelectedTabPage();
		if (pItem != NULL)
		{
			sMsg.Format(m_sMsgRemoveSpecies1,pItem->GetCaption());
			if (::MessageBox(GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONSTOP | MB_YESNO | MB_DEFBUTTON2) == IDYES)
			{
				if (::MessageBox(GetSafeHwnd(),m_sMsgRemoveSpecies2,m_sMsgCap,MB_ICONSTOP | MB_YESNO | MB_DEFBUTTON2) == IDYES)
				{
					pItem->Remove();
				}
			}
		}
	}
	else if (del_species == CCreatePricelistsFormView::DEL_ALL_SPECIES)
	{
		int nNumOfSpecies = m_wndTabControl.getNumOfTabPages();
		if (nNumOfSpecies > 0)
		{
			for (int i = 0;i < nNumOfSpecies;i++)
			{
				CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(0);
				if (pItem != NULL)
				{
					pItem->Remove();
				}
			}
		}
	}
}


BOOL CCreatePricelistsFormView::isAlreadyOnTab(LPCTSTR spc)
{
	if (m_wndTabControl.getNumOfTabPages() > 0)
	{
		for (int i = 0;i < m_wndTabControl.getNumOfTabPages();i++)
		{
			CXTPTabManagerItem *pManagerItem =	m_wndTabControl.getTabPage(i);
			if (pManagerItem != NULL)
			{
				if (pManagerItem->GetCaption().CompareNoCase(spc) == 0)
					return TRUE;
			}
		}
	}
	return FALSE;

}

BOOL CCreatePricelistsFormView::AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon,int spc_id)
{
	CCreateContext contextT;
	contextT.m_pCurrentDoc     = GetDocument();
	contextT.m_pNewViewClass   = pViewClass;
	contextT.m_pNewDocTemplate = GetDocument()->GetDocTemplate();

	CWnd* pWnd;
	TRY
	{
		pWnd = (CWnd*)pViewClass->CreateObject();
		if (pWnd == NULL)
		{
			AfxThrowMemoryException();
		}
	}
	CATCH_ALL(e)
	{
		TRACE0( "Out of memory creating a view.\n" );
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;

	int nTab = m_wndTabControl.GetItemCount();

	// Create with the right size (wrong position)
	CRect rect(0,0,0,0);
	if (!pWnd->Create(NULL, NULL, dwStyle,rect, &m_wndTabControl, (AFX_IDW_PANE_FIRST + nTab), &contextT))
	{
		TRACE0( "Warning: couldn't create client tab for view.\n" );
		// pWnd will be cleaned up by PostNcDestroy
		return NULL;
	}
	m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);
	m_mapSpcID[nTab] = spc_id;

	pWnd->SetOwner(this);

	return TRUE;
}


//////////////////////////////////////////////////////////////////////////////////////////////
// CReportCreatePricelistView

IMPLEMENT_DYNCREATE(CReportCreatePricelistView,  CXTPReportView)

BEGIN_MESSAGE_MAP(CReportCreatePricelistView, CXTPReportView)
//	ON_COMMAND_RANGE(ID_BUTTON32807,ID_BUTTON32814, OnCommand)
	ON_COMMAND_RANGE(ID_BUTTON32799,ID_BUTTON32806, OnCommand)
	ON_COMMAND_RANGE(ID_TOOLS_PRICELIST,ID_TOOLS_PRICELIST, OnCommandTools)

	ON_NOTIFY(NM_CLICK, XTP_ID_REPORT_CONTROL, OnReportItemClick)
END_MESSAGE_MAP()


CReportCreatePricelistView::CReportCreatePricelistView()
{
	CString sLangFN(getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV));
	if (fileExists(sLangFN))
	{
		RLFReader xml;
		if (xml.Load(sLangFN))
		{

			VERIFY(m_ilIcons.Create(16,13, ILC_COLOR24|ILC_MASK, 0, 1));
			CBitmap bmp;
			VERIFY(bmp.LoadBitmap(IDB_BITMAP1));
			m_ilIcons.Add(&bmp, RGB(255, 0, 255));
			GetReportCtrl().SetImageList(&m_ilIcons);

			// Grade code
			CXTPReportColumn *pCol = NULL;
			pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_0,xml.str(IDS_STRING4530),90,FALSE));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_LEFT);
			pCol->SetAlignment(DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = FALSE;

			// Prices
			pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_1,xml.str(IDS_STRING4531),120,FALSE));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_LEFT);
			pCol->SetAlignment(DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;
			pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;

			// Basis for calculation
			pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_2,xml.str(IDS_STRING4532),150,FALSE));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_LEFT);
			pCol->SetAlignment(DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = FALSE;

			xml.clean();
		}
	}

	GetReportCtrl().GetReportHeader()->AllowColumnRemove(FALSE);
	GetReportCtrl().GetReportHeader()->SetAutoColumnSizing( FALSE );
	GetReportCtrl().SetMultipleSelection( FALSE );
	GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );
	GetReportCtrl().SetGridStyle( FALSE, xtpReportGridSmallDots );
	GetReportCtrl().FocusSubItems(TRUE);
	GetReportCtrl().AllowEdit(TRUE);
	GetReportCtrl().GetPaintManager()->SetFixedRowHeight(FALSE);

}

CReportCreatePricelistView::~CReportCreatePricelistView()
{
}

void CReportCreatePricelistView::OnCommand(UINT nID)	
{ 
	CCreatePricelistsFormView *pView = (CCreatePricelistsFormView*)getFormViewByID(IDD_FORMVIEW9);
	if (pView != NULL)
	{
		::SendMessage(pView->GetSafeHwnd(),WM_COMMAND,nID,0);
	}
}

void CReportCreatePricelistView::OnCommandTools(UINT nID)	
{ 
	CCreatePricelistsFormView *pView = (CCreatePricelistsFormView*)getFormViewByID(IDD_FORMVIEW9);
	if (pView != NULL)
	{
		::SendMessage(pView->GetSafeHwnd(),WM_COMMAND,nID,0);
	}
}

void CReportCreatePricelistView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	CRect rect;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	// prevents us from crashing when clicking inside the report, but not on an item
	if(pItemNotify == NULL) 
		return;

	// prevents us from crashing when clicking inside the report, but not on an item
	if(pItemNotify->pItem == NULL)
		return;

	CCreatePricelistsFormView *pView = (CCreatePricelistsFormView*)getFormViewByID(IDD_FORMVIEW9);
	if (pView != NULL)
	{
		pView->doReportViewClick(pNotifyStruct);
	}
}

