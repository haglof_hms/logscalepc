// RegisterFormView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "RegisterFormView.h"

#include "reportclasses.h"

#include "ResLangFileReader.h"

///////////////////////////////////////////////////////////////////////////////////////////
// CRegisterDoc


IMPLEMENT_DYNCREATE(CRegisterDoc, CDocument)

BEGIN_MESSAGE_MAP(CRegisterDoc, CDocument)
	//{{AFX_MSG_MAP(CRegisterDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRegisterDoc construction/destruction

CRegisterDoc::CRegisterDoc()
{
	// TODO: add one-time construction code here

}

CRegisterDoc::~CRegisterDoc()
{
}


BOOL CRegisterDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CRegisterDoc serialization

void CRegisterDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CRegisterDoc diagnostics

#ifdef _DEBUG
void CRegisterDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CRegisterDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


///////////////////////////////////////////////////////////////////////////////////////////
// CRegisterFrame

IMPLEMENT_DYNCREATE(CRegisterFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CRegisterFrame, CMDIChildWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()

	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_UPDATE_COMMAND_UI(ID_BUTTON32778, OnUpdateToolbarBtn)		// New button
	ON_UPDATE_COMMAND_UI(ID_BUTTON32779, OnUpdateToolbarBtn)		// Delete button
//	ON_UPDATE_COMMAND_UI(ID_BUTTON32777, OnUpdateToolbarBtn)	// Savebutton

END_MESSAGE_MAP()


// CRegisterFrame construction/destruction

XTPDockingPanePaintTheme CRegisterFrame::m_themeCurrent = xtpPaneThemeOffice2003;

CRegisterFrame::CRegisterFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
	m_bEnableToolBar = TRUE;
}

CRegisterFrame::~CRegisterFrame()
{
}

void CRegisterFrame::OnDestroy(void)
{

	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6003_KEY);
	SavePlacement(this, csBuf);

}

void CRegisterFrame::OnClose(void)
{
	CMDIChildWnd::OnClose();
}


int CRegisterFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	EnableDocking(CBRS_ALIGN_ANY);

	// Create and Load toolbar; 090107 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR4);
	m_wndToolBar.EnableDocking(xtpFlagAlignTop|xtpFlagStretched);

	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();
	CString sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
	if (fileExists(sLangFN))
	{
		RLFReader xml;
		if (xml.Load(sLangFN))
		{
			//***************************************************************************************
			// Setup commandbars and manues; 051114 p�d
			CXTPToolBar* pToolBar = &m_wndToolBar;
			if (pToolBar->IsBuiltIn())
			{
				if (pToolBar->GetType() != xtpBarTypeMenuBar)
				{
					UINT nBarID = pToolBar->GetBarID();
					pToolBar->LoadToolBar(nBarID, FALSE);
					CXTPControls *p = pToolBar->GetControls();
					// Setup icons on toolbars, using resource dll; 051208 p�d
					if (nBarID == IDR_TOOLBAR4)
					{		
						setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_ADD,xml.str(IDS_STRING1550),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(1),RES_TB_DEL2,xml.str(IDS_STRING1551),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(2),RES_TB_SAVE,xml.str(IDS_STRING1552),TRUE);	//
					}	// if (nBarID == IDR_TOOLBAR3)
				}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
			}	// if (pToolBar->IsBuiltIn())
			//***************************************************************************************

			xml.clean();
		}	// if (xml.Load(sLangFN))
	}	// if (fileExists(sLangFN))


	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

void CRegisterFrame::OnUpdateToolbarBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableToolBar);
}


BOOL CRegisterFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CRegisterFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CRegisterFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6003_KEY);
		LoadPlacement(this, csBuf);
  }

}

void CRegisterFrame::OnSetFocus(CWnd* pWnd)
{
	CMDIChildWnd::OnSetFocus(pWnd);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CRegisterFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}


// CRegisterFrame diagnostics

#ifdef _DEBUG
void CRegisterFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CRegisterFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CRegisterFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE2;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE2;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CRegisterFrame::toolbarEnableDisable(BOOL enable)
{
	m_bEnableToolBar = enable;
}

// CRegisterFormView

IMPLEMENT_DYNCREATE(CRegisterFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CRegisterFormView, CXTResizeFormView)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_COPYDATA()
	ON_COMMAND(ID_BUTTON32781, OnAdd)
	ON_COMMAND(ID_BUTTON32782, OnDelete)
	ON_COMMAND(ID_BUTTON32783, OnSave)
	ON_UPDATE_COMMAND_UI(ID_BUTTON32781, OnUpdateToolbarBtn)		// New button
	ON_UPDATE_COMMAND_UI(ID_BUTTON32782, OnUpdateToolbarBtn)		// Delete button

	ON_CBN_SELCHANGE(IDC_COMBO4_1, OnCbnSelchangeCombo41)
	ON_CBN_DROPDOWN(IDC_COMBO4_1, OnCbnDropdownCombo41)
END_MESSAGE_MAP()

CRegisterFormView::CRegisterFormView()
	: CXTResizeFormView(CRegisterFormView::IDD),
		m_bInitialized(FALSE),
		m_sLangFN(L""),
		m_pDB(NULL),
		m_bEnableToolBar(FALSE),
		m_nSelectedType(-1)

{
}

CRegisterFormView::~CRegisterFormView()
{
}

void CRegisterFormView::OnDestroy()
{
	SaveReportState(m_repRegister);

	OnSave();

	CXTResizeFormView::OnDestroy();
}


BOOL CRegisterFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CRegisterFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_COMBO4_1, m_cboxRegisterTypes);
	DDX_Control(pDX, IDC_LBL4_1, m_lbl4_1);
	//}}AFX_DATA_MAP

}

int CRegisterFormView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}


// CRegisterFormView diagnostics

#ifdef _DEBUG
void CRegisterFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CRegisterFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG

void CRegisterFormView::OnSize(UINT nType,int cx,int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	if (m_repRegister.GetSafeHwnd() != NULL)
	{
		setResize(&m_repRegister,2,50,cx-4,cy-52);
	}
}

// CRegisterFormView message handlers
void CRegisterFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();
	if (!m_bInitialized)
	{

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		m_lbl4_1.SetLblFontEx(14,FW_NORMAL);
		m_cboxRegisterTypes.SetLblFont(16,FW_BOLD);
		m_cboxRegisterTypes.SetEnabledColor(BLUE,WHITE);

		m_nSelectedType = -1;

		setupReport();

		LoadReportState(m_repRegister);

		m_bInitialized = TRUE;
	}

}

BOOL CRegisterFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{

	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		if (m_dbConnectionData.conn->isConnected())
		{
			m_pDB = new CDBHandling(m_dbConnectionData);
		}
	}

	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CRegisterFormView::setupReport()
{
	CXTPReportColumn *pCol = NULL;

	if (m_repRegister.GetSafeHwnd() == 0)
	{

		// Create the sheet1 list box.
		if (!m_repRegister.Create(this, ID_REPORT_REGISTER, L"Register_report",TRUE,FALSE))
		{
			TRACE0( "Failed to create m_repRegister.\n" );
			return;
		}
	}

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_repRegister.ShowWindow( SW_NORMAL );

			// Name/Type
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_0, xml.str(IDS_STRING1501), 200));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;

			// Abbrevation
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_1, xml.str(IDS_STRING1502), 80));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;
			pCol->GetEditOptions()->m_nMaxLength = 5;	// Abbrevation max 5 characters

			// Other info
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_2, xml.str(IDS_STRING1503), 150));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;

			// Address
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_3, xml.str(IDS_STRING1504), 150));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;

			// Address2
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_4, xml.str(IDS_STRING1505), 150));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;

			// ZipCode
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_5, xml.str(IDS_STRING1506), 50));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;

			// City
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_6, xml.str(IDS_STRING1507), 80));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;

			// Phone
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_7, xml.str(IDS_STRING1508), 100));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;

			// Cellphone
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_8, xml.str(IDS_STRING1509), 120));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;

			// WWW
			pCol = m_repRegister.AddColumn(new CXTPReportColumn(COLUMN_9, xml.str(IDS_STRING1510), 150));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_WORDBREAK | DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;

			m_repRegister.GetReportHeader()->AllowColumnRemove(FALSE);
			m_repRegister.SetMultipleSelection( FALSE );
			m_repRegister.SetGridStyle( TRUE, xtpReportGridSolid );
			m_repRegister.SetGridStyle( FALSE, xtpReportGridSmallDots );
			m_repRegister.FocusSubItems(TRUE);
			m_repRegister.AllowEdit(TRUE);
			m_repRegister.GetPaintManager()->SetFixedRowHeight(FALSE);

			CString sListOfRegisterItems = xml.str(IDS_STRING200);
			CStringArray sarrTokens;
			tokenizeString(sListOfRegisterItems,';',sarrTokens);
			m_cboxRegisterTypes.Clear();
			for (int i = 0;i < sarrTokens.GetCount();i++)
			{
				
				m_cboxRegisterTypes.AddString(sarrTokens.GetAt(i));

			}
			m_lbl4_1.SetWindowText(xml.str(IDS_STRING1500));

			m_sMsgCap = xml.str(IDS_STRING99); 
			m_sMsgDelete1 = xml.str(IDS_STRING1553);
			m_sMsgDelete2 = xml.str(IDS_STRING1554);
			m_sMsgMandatoryDataMissing = xml.str(IDS_STRING1520) + L"\n\n" + xml.str(IDS_STRING1521);

			xml.clean();
		}
	}
}

void CRegisterFormView::populateReport()
{
	m_repRegister.ResetContent();
	if (m_pDB == NULL) return;
	m_pDB->getRegister(m_vecRegister,m_nSelectedType);

	if (m_vecRegister.size() > 0)
	{

		for (UINT i = 0;i < m_vecRegister.size();i++)
		{
			m_repRegister.AddRecord(new CRegisterReportRec(m_vecRegister[i]));
		}
		m_repRegister.Populate();
		m_repRegister.UpdateWindow();
	}
}

void CRegisterFormView::OnCbnSelchangeCombo41()
{
	OnSave();
	int nIdx = m_cboxRegisterTypes.GetCurSel();
	if (nIdx != CB_ERR && m_pDB != NULL)
	{
		m_bEnableToolBar = TRUE;
		m_nSelectedType = nIdx+1;
		populateReport();
	}
	else
	{
		m_bEnableToolBar = FALSE;	// Disable toolbar buttons
		m_nSelectedType = -1;
	}
}

void CRegisterFormView::OnCbnDropdownCombo41()
{
	// Save data if combobox is dropped down
	OnSave();
}


void CRegisterFormView::OnUpdateToolbarBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableToolBar);
}

void CRegisterFormView::OnAdd()
{
	// Add a record to list
	m_repRegister.AddRecord(new CRegisterReportRec());
	m_repRegister.Populate();
	m_repRegister.UpdateWindow();
}

void CRegisterFormView::OnDelete()
{
	CString sMsg = L"";
	sMsg.Format(m_sMsgDelete1,m_cboxRegisterTypes.getText());
	// Ask user twice, if he realy want's to delete
	if (::MessageBox(GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONQUESTION | MB_YESNO) == IDNO)
	{
		return;
	}
	else	if (::MessageBox(GetSafeHwnd(),m_sMsgDelete2,m_sMsgCap,MB_ICONQUESTION | MB_YESNO) == IDNO)
	{
		return;
	}

	CXTPReportRow *pRow = m_repRegister.GetFocusedRow();
	CRegisterReportRec *rec = NULL;
	// We'll also add this new item to DB
	if (m_pDB != NULL && pRow != NULL)
	{
		rec = (CRegisterReportRec*)pRow->GetRecord();
		if (m_pDB->delRegisterItem(rec->getRecord()))
		{
			populateReport();
		}
	}
}

void CRegisterFormView::OnSave()
{
	// We'll save all items for this type
	BOOL bMandatoryDataMissing = FALSE;
	CXTPReportRecords *pRecs = m_repRegister.GetRecords();
	CRegisterReportRec *rec = NULL;
	if (m_pDB != NULL && pRecs != NULL)
	{
		m_repRegister.Populate();
		// We'll check that user entered mandatory data
		for (int i = 0;i < pRecs->GetCount();i++)
		{
			rec = (CRegisterReportRec*)pRecs->GetAt(i);
			if (rec->getColText(COLUMN_0).IsEmpty() ||
					rec->getColText(COLUMN_1).IsEmpty())
			{
				::MessageBox(GetSafeHwnd(),m_sMsgMandatoryDataMissing,m_sMsgCap,MB_ICONASTERISK | MB_OK);
				bMandatoryDataMissing = FALSE;
				break;
			}
		}
		if (!bMandatoryDataMissing)
		{

			for (int i = 0;i < pRecs->GetCount();i++)
			{
				rec = (CRegisterReportRec*)pRecs->GetAt(i);
				if (rec->getRecord().getID() == -1)
				{
					m_pDB->newRegisterItem(CRegister(-1,m_nSelectedType,
																				rec->getColText(COLUMN_0),
																				rec->getColText(COLUMN_1),
																				rec->getColText(COLUMN_2),
																				rec->getColText(COLUMN_3),
																				rec->getColText(COLUMN_4),
																				rec->getColText(COLUMN_5),
																				rec->getColText(COLUMN_6),
																				rec->getColText(COLUMN_7),
																				rec->getColText(COLUMN_8),
																				rec->getColText(COLUMN_9)));
				}
				else if (rec->getRecord().getID() > 0)
				{
					m_pDB->updRegisterItem(CRegister(rec->getRecord().getID(),
																		 m_nSelectedType,
																		 rec->getColText(COLUMN_0),
																		 rec->getColText(COLUMN_1),
  																	 rec->getColText(COLUMN_2),
 	 																	 rec->getColText(COLUMN_3),
																		 rec->getColText(COLUMN_4),
																		 rec->getColText(COLUMN_5),
																		 rec->getColText(COLUMN_6),
																		 rec->getColText(COLUMN_7),
																		 rec->getColText(COLUMN_8),
																		 rec->getColText(COLUMN_9)));
				}
			}
			populateReport();
		}
	}

}
