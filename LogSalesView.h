#if !defined(__LOGSALESVIEW_H__)
#define __LOGSALESVIEW_H__

#include "DerivedClasses.h"

#include "ReportClasses.h"

#include "DBHandling.h"

#pragma once


//////////////////////////////////////////////////////////////////////////////
// Derived class from CXTPReportFilterEditControl to handle
// OnKeyUp() event, setting value for toolbar button
// FilterOff in CTraktSelListFrame; 070108 p�d

class CSalesReportFilterEditControl : public CXTPReportFilterEditControl
{
	DECLARE_DYNCREATE(CSalesReportFilterEditControl)
public:
	CSalesReportFilterEditControl(void)
		: CXTPReportFilterEditControl()
	{}

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	afx_msg void OnKeyUp(UINT,UINT,UINT);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

///////////////////////////////////////////////////////////////////////////////////////////
// CLogScaleSalesDoc

class CLogScaleSalesDoc : public CDocument
{
protected: // create from serialization only
	CLogScaleSalesDoc();
	DECLARE_DYNCREATE(CLogScaleSalesDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLogScaleSalesDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CLogScaleSalesDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CLogScaleSalesDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};



#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
/////////////////////////////////////////////////////////////////////////////
// class CLogScaleSalesFrame

class CLogScaleSalesFrame : public CChildFrameBase
{
//private:

	DECLARE_DYNCREATE(CLogScaleSalesFrame)
	CXTPStatusBar m_wndStatusBar;
	CXTPToolBar m_wndToolBar;
	CXTPDockingPaneManager m_paneManager;
	CString m_sLangFN;

	BOOL m_bFirstOpen;
	BOOL m_bInitReports;
	BOOL m_bIsPrintOutTBtn;
	BOOL m_bIsFilterOffTBtn;
//	CString m_sShellDataFile;
	int m_nShellDataIndex;
	CDialogBar m_wndFieldSelectionDlg;		// Sample Field chooser window

	vecSTDReports m_vecReports;
	CComboBox m_cboxPrintOut;

	CFont *m_fnt1;

protected:
	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	HICON m_hIcon;
public:
	CLogScaleSalesFrame();

	CDialogBar m_wndFilterEdit;     // Sample Filter editing window

	inline void setEnableFilterOff(BOOL v)	{	m_bIsFilterOffTBtn = v;	}


	static XTPDockingPanePaintTheme m_themeCurrent;

// Attributes

// Operations

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	virtual ~CLogScaleSalesFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	
	//{{AFX_MSG(CLogScaleSalesFrame)
	afx_msg int OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl);
	afx_msg void OnSysCommand(UINT nID,LPARAM lParam);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg void OnDestroy(void);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );

	afx_msg void OnUpdatePrintOutTBtn(CCmdUI* pCmdUI);
	afx_msg void OnTBBtnPrintOut();
	afx_msg void OnPrintOutCBox();
	afx_msg void OnFilter(void);
	afx_msg void OnFilterOff(void);

	afx_msg void OnMatchTags(void);

	afx_msg void OnUpdateFilterOff(CCmdUI* pCmdUI);

	//}}AFX_MSG
	
	DECLARE_MESSAGE_MAP()
};


///////////////////////////////////////////////////////////////////////////////////////////
// CLogSalesView form view

class CLogSalesView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CLogSalesView)

	// Data members
	BOOL m_bInitialized;
	CString m_sLangFN;
	CString m_sFieldSelection;
	CString m_sMsgCap;
	CString m_sMsgDelete1;
	CString m_sMsgDelete2;
	CString m_sMsgOpenEXCEL;
	CString m_sOpenDlgTagFilesCaliper;
	CString m_sOpenDlgTagFilesExcel;
	CString m_sOpenDlgTagFilesAll;
	CString m_sFilterOn;

	CComboBox m_cboxPrintOut;
	CString m_sMsgErrorInTagFile;

	CXTPReportSubListControl m_wndSubList;

	CMyReportControl m_repLogSales;

	CTickets m_recTicket;
	CVecTickets m_vecTickets;
	CVecLogs m_vecLogs;

	vecInventory m_vecInventory;

	CVecRegister m_vecRegister;

	CDBHandling *m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;

	CImageList m_ilIcons;

	CMyExtStatic m_lbl20_1;
	CMyExtStatic m_lbl20_2;

	CSalesReportFilterEditControl m_wndFilterEdit;

	int	m_nSelectedColumn;
	int m_nFilteredColumn;
	// Methods
	void setFilterWindow(bool bOld = false);

	void setupReportControl(void);
	BOOL checkLogData(CVecLogs& vec,CTickets& rec,bool ticket_num_ok);

	void addConstraint(REGISTER_TYPES::REGS type,int col_num);

	void Delete();

	BOOL SaveSale(CInventory& rec);


protected:
	CLogSalesView();           // protected constructor used by dynamic creation
	virtual ~CLogSalesView();

	void LoadReportState(void);
	void SaveReportState(void);

public:
	enum { IDD = IDD_FORMVIEW11 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

	// Also call upon in CLogScaleFrame()
	BOOL Save(bool populate);
	void populateReport(bool focus_row);

	int getActiveLoadID();

	BOOL isTicketAlreadyUsed(CString& ticket_num);

	CDBHandling *getDB()	{ return m_pDB; }

	void doExportToExcel()	{ OnExportToEXCEL(); }

protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLogSalesView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CLogSalesView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnClose();
	afx_msg void OnDestroy(void);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg LRESULT OnSuiteMessge(WPARAM wParam, LPARAM lParam);
	afx_msg void OnReportItemRClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnFieldSelection(void);

	afx_msg void OnNewSale(void);
	afx_msg void OnSaveSale(void);
	afx_msg void OnDeleteSale(void);

	afx_msg void OnImportTags();
	afx_msg void OnExportToEXCEL();
	afx_msg void OnFilter(void);
	afx_msg void OnFilterOff(void);

	afx_msg void OnUpdatePrintOutTBtn(CCmdUI* pCmdUI);
	afx_msg void OnTBBtnPrintOut();
	afx_msg void OnPrintOutCBox();

	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


#endif