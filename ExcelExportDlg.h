#pragma once

#include "Resource.h"

// CExcelExportDlg dialog

class CExcelExportDlg : public CXTResizeDialog
{
	DECLARE_DYNAMIC(CExcelExportDlg)

	BOOL m_bInitialized;
	CString m_sLangFN;

	CString m_sMsgCap;
	CString m_sMsgNoColumnsSelected;

	CMyExtStatic m_lbl10_1;
	CButton m_btnSelectAll;
	CButton m_btnDeSelectAll;
	CButton m_btnReset;
	CButton m_btnPrevPage;
	CButton m_btnNextPage;
	CButton m_btnOK;
	CButton m_btnCancel;
	CListCtrl m_list10_1;

	vecColumnsSet m_vecColumnsSet;
	vecColumnsSet m_vecColumnsSelected;
public:
	CExcelExportDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CExcelExportDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG10 };

	inline void getColumnsSetting(LPCTSTR reg_key,ENUM_COL_SETTINGS_TYPES save_as)	{ readColumnsSettingFromFile(reg_key,m_vecColumnsSet,save_as); }
	inline void getColumnsSelected(vecColumnsSet &vec) { vec = m_vecColumnsSelected; }

protected:
	//{{AFX_VIRTUAL(CGradesReportView)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL


	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton103();
	afx_msg void OnBnClickedButton104();
	afx_msg void OnBnClickedButton101();
	afx_msg void OnBnClickedButton102();
	afx_msg void OnBnClickedButton105();
	afx_msg void OnBnClickedOk();
};
