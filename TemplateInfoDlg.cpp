// TemplateInfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "TemplateInfoDlg.h"

#include "ResLangFileReader.h"

// CTemplateInfoDlg dialog

IMPLEMENT_DYNAMIC(CTemplateInfoDlg, CXTResizeDialog)

BEGIN_MESSAGE_MAP(CTemplateInfoDlg, CXTResizeDialog)
	ON_BN_CLICKED(IDOK, &CTemplateInfoDlg::OnBnClickedOk)
END_MESSAGE_MAP()

CTemplateInfoDlg::CTemplateInfoDlg(CWnd* pParent /*=NULL*/)
	: CXTResizeDialog(CTemplateInfoDlg::IDD, pParent),
		m_bInitialized(FALSE)
{

}

CTemplateInfoDlg::~CTemplateInfoDlg()
{
}

void CTemplateInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCorrectionDlg)
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_LBL7_1, m_lbl7_1);

	DDX_Control(pDX, IDC_EDIT7_1, m_ed7_1);
	//}}AFX_DATA_MAP
}

BOOL CTemplateInfoDlg::OnInitDialog()
{
	CXTResizeDialog::OnInitDialog();
	if (!m_bInitialized)
	{
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				// Set dialog caption
				SetWindowText(xml.str(IDS_STRING1900));
				m_lbl7_1.SetWindowTextW(xml.str(IDS_STRING1901));
				
				m_btnOK.SetWindowText(xml.str(IDS_STRING100));
				m_btnCancel.SetWindowText(xml.str(IDS_STRING101));
			}
		}

		m_ed7_1.SetWindowTextW(m_sTicketNum);

		m_bInitialized = TRUE;
	}

	return TRUE;
}

// CTemplateInfoDlg message handlers

void CTemplateInfoDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	m_sTicketNum = m_ed7_1.getText();
	OnOK();
}
