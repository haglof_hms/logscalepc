#pragma once


#if !defined(__PRICLISTFORMVIEW_H__)
#define __PRICELISTFORMVIEW_H__

#include "DerivedClasses.h"

#include "ReportClasses.h"

#include "DBHandling.h"

#pragma once

///////////////////////////////////////////////////////////////////////////////////////////
// CPricelistDoc

class CPricelistDoc : public CDocument
{
protected: // create from serialization only
	CPricelistDoc();
	DECLARE_DYNCREATE(CPricelistDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPricelistDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CPricelistDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CPricelistDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
/////////////////////////////////////////////////////////////////////////////
// class CPricelistFrame

class CPricelistFrame : public CChildFrameBase
{
//private:

	DECLARE_DYNCREATE(CPricelistFrame)
	CXTPStatusBar m_wndStatusBar;
	CXTPToolBar m_wndToolBar;
	CXTPDockingPaneManager m_paneManager;

	BOOL m_bFirstOpen;

	BOOL m_bEnableToolBar;

protected:
	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	HICON m_hIcon;
public:
	CPricelistFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

	void toolbarEnableDisable(BOOL enable);

// Attributes

// Operations

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	virtual ~CPricelistFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif


// Generated message map functions
protected:
	
	//{{AFX_MSG(CPricelistFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg void OnDestroy(void);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );

	afx_msg void OnUpdateToolbarBtn(CCmdUI* pCmdUI);

	//}}AFX_MSG
	
	DECLARE_MESSAGE_MAP()
};


// CPricelistFormView form view

class CPricelistFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CPricelistFormView)

	// Data members
	BOOL m_bInitialized;
	CString m_sLangFN;
	
	CString m_sMsgCap;
	CString m_sMsgDelete1;
	CString m_sMsgDelete2;

	CMyExtStatic m_lblSpecies;
	CMyComboBox m_cboxSpecies;
	CMyReportControl m_repPricelist;

	CSpecies m_recSpeciesSelected;
	CVecSpecies m_vecSpecies;

	CDefaults m_recDefaults;
	CVecDefaults m_vecDefaults;

	CVecPricelist m_vecPricelist;
	CVecCalcTypes m_vecCalcTypes;

	CDBHandling *m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;

	CImageList m_ilIcons;

	// Methods
	void setupReport();

	void populateSpeciesCB();

	void populateReport();
protected:
	CPricelistFormView();           // protected constructor used by dynamic creation
	virtual ~CPricelistFormView();

public:
	enum { IDD = IDD_FORMVIEW3 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	//{{AFX_VIRTUAL(CGradesReportView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CGradesperSpcView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnDestroy();
	afx_msg void OnSavePricelist();
	afx_msg void OnUpdateGrades();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnCbnSelchangeCombo1();
};


#endif