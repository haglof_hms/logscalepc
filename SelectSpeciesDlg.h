#pragma once

#include "Resource.h"

// CSelectSpeciesDlg dialog

class CSelectSpeciesDlg : public CDialog
{
	DECLARE_DYNAMIC(CSelectSpeciesDlg)

	BOOL m_bInitialized;

	CString m_sLangFN;

	CXTListCtrl m_wndListCtrl;
	CXTHeaderCtrl   m_header;

	CButton m_btnOK;
	CButton m_btnCancel;

	CVecSpecies m_vecSpecies;
	CVecSpecies m_vecSelectedSpecies;

	void setup(void);

public:
	CSelectSpeciesDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSelectSpeciesDlg();

	void setSpecies(CVecSpecies& vec)	{ m_vecSpecies = vec; }
	void getSelectedSpecies(CVecSpecies& vec)	{ vec = m_vecSelectedSpecies; }

// Dialog Data
	enum { IDD = IDD_DIALOG11 };

protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectSpeciesDlg)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnLvnItemchangedList2(NMHDR *pNMHDR, LRESULT *pResult);
};
