#pragma once

#include "DBHandling.h"

class CTicketExport
{
public:
	CTicketExport(void);
	~CTicketExport(void);

	void ExportToExcel(CDBHandling *pDB, CTickets *ticketExport);

private:
	CVecLogs m_vecLogs;
	vecSelectedVolFuncs m_vecSelectedVolFuncs;
	CStringArray m_sarrMeasureTypes;
	CStringArray m_sarrStatus;
};
