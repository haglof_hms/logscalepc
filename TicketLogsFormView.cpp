// TicketLogsFormView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "TicketLogsFormView.h"
#include "logscalevolume.h"
#include "reportclasses.h"
#include "logscaleview.h"
#include "ResLangFileReader.h"
#include "PricelistDlg.h"
#include "CorrectionDlg.h"
#include "ChangeUserFuncDlg.h"
#include "ChangeBarkReductionDlg.h"
#include "ChangeStatusDlg.h"
#include "fileparser.h"
#include "ViewProgress.h"
#include "TemplateInfoDlg.h"
#include "TagNoMatchDlg.h"
#include "libxl.h"
#include "TicketExport.h"

using namespace libxl;

extern CString m_sShellDataFile;

///////////////////////////////////////////////////////////////////////////////////////////
// CTicketLogsDoc


IMPLEMENT_DYNCREATE(CTicketLogsDoc, CDocument)

BEGIN_MESSAGE_MAP(CTicketLogsDoc, CDocument)
	//{{AFX_MSG_MAP(CTicketLogsDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTicketLogsDoc construction/destruction

CTicketLogsDoc::CTicketLogsDoc()
{
	// TODO: add one-time construction code here

}

CTicketLogsDoc::~CTicketLogsDoc()
{
}


BOOL CTicketLogsDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CTicketLogsDoc serialization

void CTicketLogsDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CTicketLogsDoc diagnostics

#ifdef _DEBUG
void CTicketLogsDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CTicketLogsDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


///////////////////////////////////////////////////////////////////////////////////////////
// CTicketLogsFrame

IMPLEMENT_DYNCREATE(CTicketLogsFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CTicketLogsFrame, CMDIChildWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	//ON_WM_CLOSE()
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_SYSCOMMAND()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)

	ON_COMMAND(ID_BUTTON32791,OnAddLogs)
	ON_COMMAND(ID_BUTTON32792,OnDelLogs)
	ON_COMMAND(ID_BUTTON32793,OnUpdLogs)

	ON_COMMAND(ID_BUTTON32795, OnTBBtnPrintOut)
	ON_UPDATE_COMMAND_UI(ID_BUTTON32795, OnUpdatePrintOutTBtn)

	ON_CONTROL(CBN_SELCHANGE,ID_BUTTON32794, OnPrintOutCBox)

	ON_COMMAND(ID_BUTTON32796,OnImportTicket)

	ON_COMMAND(ID_TOOLS_EXPORT_EXCEL2 ,OnExportToExcel)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_EXPORT_EXCEL2, OnUpdateExportToExcelTBtn)

	ON_COMMAND(ID_EXPORT_TMPL2 ,OnExportTicket)
	ON_UPDATE_COMMAND_UI(ID_EXPORT_TMPL2, OnUpdateExportToExcelTBtn)

	ON_COMMAND(ID_TOOLS_CONTRACT ,OnContract)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_CONTRACT, OnUpdateContractTBtn)
	ON_COMMAND(ID_TOOLS_PRICELIST2 ,OnPricelist)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_PRICELIST2, OnUpdatePricelistTBtn)

	ON_COMMAND(ID_TOOLS_BARK_REDUCTION2 ,OnBarkReduction)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_BARK_REDUCTION2, OnUpdateBarkReductionTBtn)

	ON_COMMAND(ID_TOOLS_EXTRA_VFUNC2 ,OnExtraVFunc)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_EXTRA_VFUNC2, OnUpdateExtraVFuncTBtn)

	ON_COMMAND(ID_TOOLS_CHANGE_STATUS ,OnChangeLogStatus)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_CHANGE_STATUS, OnUpdateChangeLogStatusTBtn)

	ON_XTP_CREATECONTROL()
END_MESSAGE_MAP()


// CTicketLogsFrame construction/destruction

XTPDockingPanePaintTheme CTicketLogsFrame::m_themeCurrent = xtpPaneThemeOffice2003;

CTicketLogsFrame::CTicketLogsFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
	m_bIsPrintOutTBtn = FALSE;
	m_bIsContractTBtn = FALSE;
	m_bIsExtraVFuncTBtn = FALSE;
	m_bIsExportToExcelTBtn = TRUE;
	m_bIsBarkReductionTBtn = FALSE;
	m_bIsChangeLogStatusTBtn = TRUE;
	m_nShellDataIndex = 6006;
	m_bSysCommand = FALSE;
	m_bInitReports = FALSE;
}

CTicketLogsFrame::~CTicketLogsFrame()
{
}

void CTicketLogsFrame::OnDestroy(void)
{

	if (m_fnt1)
		delete m_fnt1;
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6006_KEY);
	SavePlacement(this, csBuf);

}

void CTicketLogsFrame::OnSysCommand(UINT nID,LPARAM lParam)
{
	m_bReturn = TRUE;
	if ((nID & 0xFFF0) == SC_CLOSE)
	{
		m_bSysCommand = TRUE;

		CTicketLogsFormView *pView = (CTicketLogsFormView*)getFormViewByID(IDD_FORMVIEW5);
		if (pView != NULL)
		{
			CLogsReportView *pLogReportView = (CLogsReportView*)pView->getLogsView();
			if (pLogReportView != NULL)
			{
				// Save and calculate focused row
				//pLogReportView->SaveLog(TRUE /* Recalulate logs * /);
				// Update ticket-data displayed on Logs view
				if (pView->getTicket().getPKID() == -1)
					m_bReturn = pView->newTicket(true);
				else
					m_bReturn = pView->updTicket(true);
				// We'll save ALL logs on quit
				if (m_bReturn == 1)
				{
					// Commented out 2012-04-18 p�d, for optimization
					//if (pLogReportView->SaveAllLogs(TRUE) == 1)
					//{
						CMDIChildWnd::OnSysCommand(nID,lParam);
					//}
				}
				else if (m_bReturn == 0)
				{
					m_bSysCommand = FALSE;
				}
				else if (m_bReturn < 0)
				{
					CMDIChildWnd::OnSysCommand(nID,lParam);
				}		
			}
		}
	}
	else
		CMDIChildWnd::OnSysCommand(nID,lParam);
}

void CTicketLogsFrame::OnClose(void)
{

	m_bReturn = TRUE;
	if (!m_bSysCommand)
	{
		m_bSysCommand = TRUE;
		CTicketLogsFormView *pView = (CTicketLogsFormView*)getFormViewByID(IDD_FORMVIEW5);
		if (pView != NULL)
		{
			CLogsReportView *pLogReportView = (CLogsReportView*)pView->getLogsView();
			if (pLogReportView != NULL)
			{
				// Save and calculate focused row
				//pLogReportView->SaveLog(TRUE /* Recalulate logs */);
				// Update ticket-data displayed on Logs view
				if (pView->getTicket().getPKID() == -1)
					m_bReturn = pView->newTicket(true);
				else
					m_bReturn = pView->updTicket(true);
				// We'll save ALL logs on quit
				if (m_bReturn == 1)
				{
					// Commented out 2012-04-18 p�d, for optimization
					// 
					//if (pLogReportView->SaveAllLogs(TRUE) == 1)
					CMDIChildWnd::OnClose();
				}
				else if (m_bReturn < 0)
				{
					CMDIChildWnd::OnClose();
				}		
			}
		}
		else
			CMDIChildWnd::OnClose();
	}
	else
		CMDIChildWnd::OnClose();
}

void CTicketLogsFrame::enableToolsMenu(BOOL enabled)	
{ 
	m_bIsContractTBtn = enabled; 
	m_bIsExtraVFuncTBtn = enabled; 
	m_bIsBarkReductionTBtn = enabled;
	CTicketLogsFormView *pView = (CTicketLogsFormView*)getFormViewByID(IDD_FORMVIEW5);
	if (pView != NULL)
	{
		pView->enableContactAndPricelistBtn(enabled);
	}
}


int CTicketLogsFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	EnableDocking(CBRS_ALIGN_ANY);

	// Create and Load toolbar; 090107 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR5);
	m_wndToolBar.EnableDocking(xtpFlagAlignTop|xtpFlagStretched);

	// Initialize dialog bar m_wndFieldSelectionDlg
	if (!m_wndFieldSelectionDlg.Create(this, IDD_FIELD_SELECTION3,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN_SELECTION_DLG3))
	{
		return -1;      // fail to create
	}

	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_wndFieldSelectionDlg.SetWindowText(xml.str(IDS_STRING1100));

			//***************************************************************************************
			// Setup commandbars and manues; 051114 p�d
			CXTPToolBar* pToolBar = &m_wndToolBar;
			if (pToolBar->IsBuiltIn())
			{
				if (pToolBar->GetType() != xtpBarTypeMenuBar)
				{
					UINT nBarID = pToolBar->GetBarID();
					pToolBar->LoadToolBar(nBarID, FALSE);
					CXTPControls *p = pToolBar->GetControls();
					// Setup icons on toolbars, using resource dll; 051208 p�d
					if (nBarID == IDR_TOOLBAR5)
					{		
						setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_NEW,xml.str(IDS_STRING1750),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(1),RES_TB_DEL,xml.str(IDS_STRING1751),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(2),RES_TB_UPDATE,xml.str(IDS_STRING1752),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(3),RES_TB_PRINT,L"CBOX",TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(4),RES_TB_PRINT,xml.str(IDS_STRING1753),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(5),RES_TB_IMPORT,xml.str(IDS_STRING1754),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(6),RES_TB_EXPORT,xml.str(IDS_STRING1755),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(7),RES_TB_TOOLS,xml.str(IDS_STRING1756),TRUE);	//
						//p->GetAt(5)->SetVisible(FALSE);
						//p->GetAt(6)->SetVisible(FALSE);
					}	// if (nBarID == IDR_TOOLBAR5)
				}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
			}	// if (pToolBar->IsBuiltIn())
			//***************************************************************************************
			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

	// docking for field chooser
	m_wndFieldSelectionDlg.EnableDocking(0);

	// Don't show fieldchooser at this stadge
	ShowControlBar(&m_wndFieldSelectionDlg, FALSE, FALSE);
	FloatControlBar(&m_wndFieldSelectionDlg, CPoint(100, GetSystemMetrics(SM_CYSCREEN) / 3));

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

int CTicketLogsFrame::OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl)
{
	CString sStr = L"";
	if (lpCreateControl->nID == ID_BUTTON32794)
	{
		if (!m_cboxPrintOut.Create(WS_CHILD|WS_VISIBLE|CBS_DROPDOWNLIST|WS_CLIPCHILDREN,CRect(0,0,0,200), this, ID_BUTTON32794) )
		{
			AfxMessageBox(_T("ERROR:\nOnCreateControl"));
		}
		else
		{
			m_fnt1 = new CFont();
			LOGFONT lf;
			memset(&lf,0,sizeof(LOGFONT));
			lf.lfHeight = 16;
			lf.lfWeight = FW_NORMAL;
			m_fnt1->CreateFontIndirect(&lf);


			m_cboxPrintOut.SetOwner(this);
			m_cboxPrintOut.MoveWindow(45, 3, 150, 20);
			m_cboxPrintOut.SetFont(m_fnt1);

	    CXTPControlCustom * pCB = CXTPControlCustom::CreateControlCustom(&m_cboxPrintOut);

			lpCreateControl->buttonStyle = xtpButtonIconAndCaption;
      lpCreateControl->pControl = pCB;

		}
		return TRUE;
	}
 
	if (lpCreateControl->nID == ID_BUTTON32797)
	{
		CMyControlPopup *m_pToolsPopup = new CMyControlPopup();
		m_pToolsPopup->SetStyle(xtpButtonIcon);
		if (fileExists(m_sLangFN))
		{
			RLFReader xml; // = new RLFReader;
			if (xml.Load(m_sLangFN))
			{
				m_pToolsPopup->addMenuIDAndText(ID_TOOLS_EXPORT_EXCEL2,xml.str(IDS_STRING11070));
				//m_pToolsPopup->addMenuIDAndText();	// Add separator
				//m_pToolsPopup->addMenuIDAndText(ID_EXPORT_TMPL2,xml.str(IDS_STRING11071));
			}	// if (xml->Load(m_sLangFN))
			xml.clean();
		}

		lpCreateControl->pControl = m_pToolsPopup;
		return TRUE;
	}

	if (lpCreateControl->nID == ID_BUTTON32798)
	{
		CMyControlPopup *m_pToolsPopup = new CMyControlPopup();
		m_pToolsPopup->SetStyle(xtpButtonIcon);
		if (fileExists(m_sLangFN))
		{
			RLFReader xml; // = new RLFReader;
			if (xml.Load(m_sLangFN))
			{
				m_pToolsPopup->addMenuIDAndText(ID_TOOLS_CONTRACT,xml.str(IDS_STRING17560));
				m_pToolsPopup->addMenuIDAndText(ID_TOOLS_PRICELIST2,xml.str(IDS_STRING17563));
				m_pToolsPopup->addMenuIDAndText();	// Add separator
				m_pToolsPopup->addMenuIDAndText(ID_TOOLS_EXTRA_VFUNC2,xml.str(IDS_STRING17561));
				m_pToolsPopup->addMenuIDAndText();	// Add separator
				m_pToolsPopup->addMenuIDAndText(ID_TOOLS_BARK_REDUCTION2,xml.str(IDS_STRING17562));
				m_pToolsPopup->addMenuIDAndText();	// Add separator
				m_pToolsPopup->addMenuIDAndText(ID_TOOLS_CHANGE_STATUS,xml.str(IDS_STRING17564));
			}	// if (xml->Load(m_sLangFN))
			xml.clean();
		}

		lpCreateControl->pControl = m_pToolsPopup;
		return TRUE;
	}

	return FALSE;
}


BOOL CTicketLogsFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CTicketLogsFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CTicketLogsFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  { 
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6006_KEY);
		LoadPlacement(this, csBuf);
  }

}

void CTicketLogsFrame::OnSetFocus(CWnd* pWnd)
{
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,ID_DO_SOMETHING_IN_SHELL,ID_LPARAM_COMMAND2);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	//AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CMDIChildWnd::OnSetFocus(pWnd);
}

void CTicketLogsFrame::OnAddLogs()
{
	int	nReturn = -1;
	CTicketLogsFormView *pView = (CTicketLogsFormView*)getFormViewByID(IDD_FORMVIEW5);
	if (pView != NULL)
	{
		// Save and calculate focused row
		//pLogReportView->SaveLog(TRUE /* Recalulate logs */);
		// Update ticket-data displayed on Logs view
		if (pView->getTicket().getPKID() == -1)
			nReturn = pView->newTicket();
		else
			nReturn = pView->updTicket();

		if (nReturn == 1)
			pView->addLogs();

		enableToolsMenu(nReturn == 1);
	}
}

void CTicketLogsFrame::OnDelLogs()
{
	CTicketLogsFormView *pView = (CTicketLogsFormView*)getFormViewByID(IDD_FORMVIEW5);
	if (pView != NULL)
	{
		pView->delLogs();
	}
}

// Do SaveAlLogs; save and calculate
// Run this method to update Logs on i.e. change in pricelist etc.
void CTicketLogsFrame::OnUpdLogs()
{
	int	nReturn = -1;
	CTicketLogsFormView *pView = (CTicketLogsFormView*)getFormViewByID(IDD_FORMVIEW5);
	if (pView != NULL)
	{
		// Save and calculate focused row
		// Update ticket-data displayed on Logs view
		if (pView->getTicket().getPKID() == -1)
			nReturn = pView->newTicket();
		else
			nReturn = pView->updTicket();

		if (nReturn == 1)
			pView->updLogs();	// SaveAllLogs() in CLogsReportView

		enableToolsMenu(nReturn == 1);
	}
}

void CTicketLogsFrame::OnUpdatePrintOutTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsPrintOutTBtn );
}


void CTicketLogsFrame::OnUpdateContractTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsContractTBtn );	// Same enable/disable as for pricelist
}

void CTicketLogsFrame::OnUpdatePricelistTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsContractTBtn );	// Same enable/disable as for contract
}

void CTicketLogsFrame::OnUpdateExtraVFuncTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsExtraVFuncTBtn );
}

void CTicketLogsFrame::OnUpdateExportToExcelTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsExportToExcelTBtn );
}

void CTicketLogsFrame::OnUpdateBarkReductionTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsBarkReductionTBtn );
}

void CTicketLogsFrame::OnUpdateChangeLogStatusTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsChangeLogStatusTBtn );
}

void CTicketLogsFrame::OnTBBtnPrintOut()
{
	CString sReportPathAndFN = L"";
	CString sFileExtension = L"";
	CString sArgStr = L"";
	int nTractID = -1; // LoadID
	getSTDReports(m_sShellDataFile,PROGRAM_NAME,m_nShellDataIndex,m_vecReports);

	int nIdx = m_cboxPrintOut.GetCurSel();

	if (nIdx > -1 && m_vecReports.size() > 0)
	{

		sArgStr = L"";
		CLogScaleView *pView = (CLogScaleView*)getFormViewByID(IDD_FORMVIEW);
		if (pView != NULL)
		{
			nTractID = pView->getActiveLoadID();
			sArgStr.Format(_T("%d;"),nTractID);
		}

		sReportPathAndFN.Format(_T("%s%s\\%s"),
														getReportsDir(),
														getLangSet(),
														m_vecReports[nIdx].getFileName());
		if (fileExists(sReportPathAndFN))
		{
			AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,WM_USER+4,
				(LPARAM)&_user_msg(333,	// ID = 333 for CrystalReport, ID = 300 for FastReports
				_T("OpenSuiteEx"),			// Exported/Imported function
				_T("Reports2.dll"),			// Suite to call; Report2.dll = Reportgenerator for CrystalReports (A.G.), Report.dll = Reportgenerator for FastReports
				(sReportPathAndFN),			// Use this report
				(sReportPathAndFN),
				sArgStr));
		}
	}	// if (nIdx > -1 && nIdx < m_vecReports.size())
}

void CTicketLogsFrame::OnPrintOutCBox()
{
	m_bIsPrintOutTBtn = m_cboxPrintOut.GetCurSel() > CB_ERR;
}


void CTicketLogsFrame::OnImportTicket(void)
{
	CTicketLogsFormView *pView = (CTicketLogsFormView*)getFormViewByID(IDD_FORMVIEW5);
	if (pView != NULL)
	{
		pView->ImportTicket();
	}
	OnUpdLogs();
}


// �ndra prislista f�r ticket; 2011-08-18 p�d
void CTicketLogsFrame::OnContract()
{
	CTicketLogsFormView *pView = (CTicketLogsFormView*)getFormViewByID(IDD_FORMVIEW5);
	if (pView != NULL)
	{
		pView->changeContract();
	}

}

void CTicketLogsFrame::OnPricelist()
{
	CTicketLogsFormView *pView = (CTicketLogsFormView*)getFormViewByID(IDD_FORMVIEW5);
	if (pView != NULL)
	{
		pView->changePricelist();
	}

}

// �ndra extra volyms-funktioner
void CTicketLogsFrame::OnExtraVFunc()
{
	CTicketLogsFormView *pView = (CTicketLogsFormView*)getFormViewByID(IDD_FORMVIEW5);
	if (pView != NULL)
	{
		pView->changeExtraVolFunc();
	}
}

void CTicketLogsFrame::OnBarkReduction()
{
	CTicketLogsFormView *pView = (CTicketLogsFormView*)getFormViewByID(IDD_FORMVIEW5);
	if (pView != NULL)
	{
		pView->changeBarkReduction();
	}

}

void CTicketLogsFrame::OnChangeLogStatus()
{
	CTicketLogsFormView *pView = (CTicketLogsFormView*)getFormViewByID(IDD_FORMVIEW5);
	if (pView != NULL)
	{
		pView->changeLogStatus();
	}
}

void CTicketLogsFrame::OnExportToExcel()
{
	CString sRegKey = L"";
	CTicketLogsFormView *pView = (CTicketLogsFormView*)getFormViewByID(IDD_FORMVIEW5);
	if (pView != NULL)
	{
		//CLogScaleView *pView1 = (CLogScaleView*)getFormViewByID(IDD_FORMVIEW);
		//if (pView1 != NULL)
		//{
			sRegKey.Format(L"%s_%d",pView->getTicket().getTicket(),pView->getTicket().getPKID());
			if(checkIllegalChars(pView->getTicket().getTicket()) == false)	//#3611, lagt in koll inga ogiltiga tecken i Ticket numret	\ / ? : * " > < |
			{
				saveColumnsSettingToFile(sRegKey,pView->getLogsView()->GetReportCtrl(),COLSET_TICKET);
				//pView1->doExportToExcel(pView->getTicket().getTicket(),pView->getTicket().getPKID());

				CTicketExport cExport;
				cExport.ExportToExcel(pView->m_pDB, &pView->getTicket());
			}
			else
			{
				::MessageBox(GetSafeHwnd(),_T("\\ / ? : * \" > < |")+pView->m_sMsgIllegalChars,pView->m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
			}
		//}
	}
}

void CTicketLogsFrame::OnExportTicket(void)
{
	// Use method in LogScaleView
	CLogScaleView *pView1 = (CLogScaleView*)getFormViewByID(IDD_FORMVIEW);
	if (pView1 != NULL)
	{
		pView1->doExportTemplate();
	}
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CTicketLogsFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	CString sStr = L"";
	if (wParam == (ID_DO_SOMETHING_IN_SHELL + ID_LPARAM_COMMAND2))
	{
		if (!m_bInitReports)
		{
			// The return message holds a _user_msg structure, collected in the
			// OnMessageFromShell( WPARAM wParam, LPARAM lParam ); 070410 p�d
			// In this case the szFileName item in _user_msg structure holds
			// the path and filename of the ShellData file used and the szArgStr
			// holds the Suite/UserModule name; 070410 p�d
			_user_msg *msg = (_user_msg*)lParam;
			if (msg != NULL)
			{
				m_sShellDataFile = msg->getFileName();
				//m_nShellDataIndex = msg->getIndex();
				m_nShellDataIndex = 6006; // explicit set Identifer, macth id in ShellData file; 090212 p�d
				getSTDReports(m_sShellDataFile,PROGRAM_NAME,m_nShellDataIndex,m_vecReports);
				m_cboxPrintOut.ResetContent();
				RLFReader xml; // = new RLFReader;
				if (xml.Load(m_sLangFN))
				{

					for (UINT i = 0;i < m_vecReports.size();i++)
					{
						if (m_vecReports[i].getCaption().IsEmpty())
							sStr = xml.str(m_vecReports[i].getStrID()); 
						else
							sStr = m_vecReports[i].getCaption();
						m_cboxPrintOut.AddString(sStr);
					}
				}
			}	// if (msg != NULL)

			m_bInitReports = TRUE;
		}
	}
	return 0L;
}


// CTicketLogsFrame diagnostics

#ifdef _DEBUG
void CTicketLogsFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CTicketLogsFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CTicketLogsFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}


// PRIVATE

// CTicketLogsFormView

IMPLEMENT_DYNCREATE(CTicketLogsFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CTicketLogsFormView, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_COPYDATA()
	ON_WM_CREATE()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
//	ON_COMMAND(ID_TOOLS_EXPORT_EXCEL2,OnExportTicket)

ON_EN_CHANGE(IDC_EDIT5_1, &CTicketLogsFormView::OnEnChangeEdit51)
ON_BN_CLICKED(IDC_BUTTON5_1, &CTicketLogsFormView::OnBnClickedButton51)
ON_BN_CLICKED(IDC_BUTTON5_2, &CTicketLogsFormView::OnBnClickedButton52)
ON_BN_CLICKED(IDC_CHECK5_1, &CTicketLogsFormView::OnBnClickedCheck51)
END_MESSAGE_MAP()

CTicketLogsFormView::CTicketLogsFormView()
	: CXTResizeFormView(CTicketLogsFormView::IDD),
		m_bInitialized(FALSE),
		m_bIsTicketNumberOK(TRUE),
		m_pDB(NULL),
		m_sLangFN(L""),
		m_nOpenLogsViewAs(-1)
{
		m_recTicket = CTickets();
		m_recLogsTotal = CLogs();
}

CTicketLogsFormView::~CTicketLogsFormView()
{
}

void CTicketLogsFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL5_1, m_lbl5_1);
	DDX_Control(pDX, IDC_LBL5_2, m_lbl5_2);
	DDX_Control(pDX, IDC_LBL5_3, m_lbl5_3);
	DDX_Control(pDX, IDC_LBL5_4, m_lbl5_4);
	DDX_Control(pDX, IDC_LBL5_5, m_lbl5_5);
	DDX_Control(pDX, IDC_LBL5_6, m_lbl5_6);
	DDX_Control(pDX, IDC_LBL5_7, m_lbl5_7);
	DDX_Control(pDX, IDC_LBL5_8, m_lbl5_8);
	DDX_Control(pDX, IDC_LBL5_9, m_lbl5_9);
	DDX_Control(pDX, IDC_LBL5_10, m_lbl5_10);
	DDX_Control(pDX, IDC_LBL5_11, m_lbl5_11);
	DDX_Control(pDX, IDC_LBL5_12, m_lbl5_12);
	//DDX_Control(pDX, IDC_LBL5_13, m_lbl5_13); 	Barkavdrag anv�nds inte
	DDX_Control(pDX, IDC_LBL5_14, m_lbl5_14);
	DDX_Control(pDX, IDC_LBL5_15, m_lbl5_15);
	DDX_Control(pDX, IDC_LBL5_16, m_lbl5_16);
	DDX_Control(pDX, IDC_LBL5_17, m_lbl5_17);
	DDX_Control(pDX, IDC_LBL5_18, m_lbl5_18);
	DDX_Control(pDX, IDC_LBL5_19, m_lbl5_19);
	DDX_Control(pDX, IDC_LBL5_20, m_lbl5_20);
	DDX_Control(pDX, IDC_LBL5_21, m_lbl5_21);
	DDX_Control(pDX, IDC_LBL5_22, m_lbl5_22);
	DDX_Control(pDX, IDC_LBL5_23, m_lbl5_23);
	DDX_Control(pDX, IDC_LBL5_24, m_lbl5_24);
	DDX_Control(pDX, IDC_LBL5_25, m_lbl5_25);

	DDX_Control(pDX, IDC_EDIT5_1, m_edit5_1);
	DDX_Control(pDX, IDC_EDIT5_2, m_edit5_2);
	DDX_Control(pDX, IDC_EDIT5_3, m_edit5_3);
	//DDX_Control(pDX, IDC_EDIT5_4, m_edit5_4); Anv�nds inte (Barkavdrag)
	DDX_Control(pDX, IDC_EDIT5_6, m_edit5_5);
	DDX_Control(pDX, IDC_EDIT5_7, m_edit5_6);
	DDX_Control(pDX, IDC_EDIT5_8, m_edit5_7);
	DDX_Control(pDX, IDC_EDIT5_9, m_edit5_8);
	DDX_Control(pDX, IDC_EDIT5_10, m_edit5_10);

	//DDX_Control(pDX, IDC_COMBO5_1, m_dt5_1);
	DDX_Control(pDX, IDC_DATE, m_dtDate);
	DDX_DateTimeCtrl(pDX, IDC_DATE, m_csDate);
	DDX_Control(pDX, IDC_COMBO5_2, m_cbox5_1);
	DDX_Control(pDX, IDC_COMBO5_3, m_cbox5_2);
	DDX_Control(pDX, IDC_COMBO5_4, m_cbox5_3);
	DDX_Control(pDX, IDC_COMBO5_5, m_cbox5_4);
	DDX_Control(pDX, IDC_COMBO5_6, m_cbox5_5);
	DDX_Control(pDX, IDC_COMBO5_7, m_cbox5_6);
	DDX_Control(pDX, IDC_COMBO5_8, m_cbox5_7);
	DDX_Control(pDX, IDC_COMBO5_9, m_cbox5_8);
	DDX_Control(pDX, IDC_COMBO5_10, m_cbox5_9);
	DDX_Control(pDX, IDC_COMBO5_11, m_cbox5_10);
	DDX_Control(pDX, IDC_COMBO5_12, m_cbox5_12);

	DDX_Control(pDX, IDC_BUTTON5_1, m_btnContract);
	DDX_Control(pDX, IDC_BUTTON5_2, m_btnPricelist);

	DDX_Control(pDX, IDC_CHECK5_1, m_check5_1);
	//DDX_Control(pDX, IDC_BTNLOCK5_1, m_btnLock5_1);
	//}}AFX_DATA_MAP
}

void CTicketLogsFormView::OnDestroy()
{
	CXTResizeFormView::OnDestroy();
}

void CTicketLogsFormView::OnClose()
{
	CXTResizeFormView::OnClose();
}

int CTicketLogsFormView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS, CRect(0, 0, 0, 0), this, ID_TABCONTROL);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearancePropertyPage2003);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = TRUE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = TRUE;
	m_wndTabControl.GetPaintManager()->DisableLunaColors(FALSE);

	// Setup language filename; 051214 p�d
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;

		if (xml.Load(m_sLangFN))
		{
			m_sarrHeadLines.Add(L"");
			m_sarrHeadLines.Add(L"");
			m_sarrHeadLines.Add(L"");
			m_sarrHeadLines.Add(L"");
			m_sarrHeadLines.Add(L"");
			m_sarrHeadLines.Add(L"");
			m_sarrHeadLines.Add(xml.str(IDS_STRING1744));
			m_sarrHeadLines.Add(xml.str(IDS_STRING1727));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17270));
			m_sarrHeadLines.Add(xml.str(IDS_STRING1728));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17280));
			m_sarrHeadLines.Add(xml.str(IDS_STRING1729));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17290));
			m_sarrHeadLines.Add(xml.str(IDS_STRING1730));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17300));		
			m_sarrHeadLines.Add(xml.str(IDS_STRING17310));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17311));
			m_sarrHeadLines.Add(xml.str(IDS_STRING1732));
			m_sarrHeadLines.Add(xml.str(IDS_STRING1733));
			m_sarrHeadLines.Add(xml.str(IDS_STRING1738));
			m_sarrHeadLines.Add(L"");

			m_sarrHeadLines.Add(xml.str(IDS_STRING17120));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17120));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17120));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17120));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17120));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17120));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17120));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17120));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17120));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17120));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17120));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17120));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17120));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17120));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17120));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17120));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17120));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17120));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17120));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17120));
			
			m_sarrHeadLines.Add(xml.str(IDS_STRING1739));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17390));
			m_sarrHeadLines.Add(xml.str(IDS_STRING1740));
			m_sarrHeadLines.Add(xml.str(IDS_STRING1741));
			m_sarrHeadLines.Add(xml.str(IDS_STRING17410));
			m_sarrHeadLines.Add(xml.str(IDS_STRING1742));
			m_sarrHeadLines.Add(xml.str(IDS_STRING1743));
			m_sarrHeadLines.Add(L"");
			m_sarrHeadLines.Add(L"");

			AddView(RUNTIME_CLASS(CLogsReportView), xml.str(IDS_STRING1725),-1);

		}

		xml.clean();
	}

	return 0;
}

BOOL CTicketLogsFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CTicketLogsFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	if (! m_bInitialized )
	{
		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		m_edit5_1.SetLimitText(50);
		m_edit5_1.SetDisabledColor();
		m_edit5_2.SetLimitText(50);
		m_edit5_2.SetDisabledColor();
		m_edit5_3.SetDisabledColor();
//		m_edit5_4.SetDisabledColor();
		m_edit5_5.SetDisabledColor();
		m_edit5_6.SetLimitText(128);
		m_edit5_6.SetDisabledColor();
		m_edit5_7.SetLimitText(50);
		m_edit5_7.SetDisabledColor();
		m_edit5_8.SetLimitText(50);
		m_edit5_10.SetDisabledColor();

		//m_dt5_1.EnableWindow(TRUE);
		m_dtDate.EnableWindow(TRUE);

		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{

			m_sLblInch = xml.str(IDS_STRING109);
			m_sLblMM = xml.str(IDS_STRING110);
			m_sLblFeet = xml.str(IDS_STRING111);
			m_sLblDM = xml.str(IDS_STRING112);
			m_sLblBF  = xml.str(IDS_STRING113);
			m_sLblDM3  = xml.str(IDS_STRING114);
			m_sLblFT3  = xml.str(IDS_STRING115);

			m_btnContract.SetBitmap(CSize(18,14),IDB_BITMAP2);
			m_btnContract.SetXButtonStyle( BS_XT_WINXP_COMPAT );
			m_btnPricelist.SetBitmap(CSize(18,14),IDB_BITMAP3);
			m_btnPricelist.SetXButtonStyle( BS_XT_WINXP_COMPAT );

			m_sMsgCap = xml.str(IDS_STRING99);
			m_sMsgDataMissing = xml.str(IDS_STRING1125) + L"\n\n" + xml.str(IDS_STRING1126) + L"\n\n" + xml.str(IDS_STRING1127);
			m_sMsgCloseAnyway = xml.str(IDS_STRING1129);
			m_sMsgTickenumberErr = xml.str(IDS_STRING1118);

			m_sMsgChangeUpdateContract1.Format(L"%s\n%s\n\n",
				xml.str(IDS_STRING1131),
				xml.str(IDS_STRING1132));

			m_sMsgChangeUpdateContract2.Format(L"%s\n%s\n\n",
				xml.str(IDS_STRING1133),
				xml.str(IDS_STRING1134) );

			m_sMsgBarkReductionMissing.Format(L"%s\n%s\n\n",
				 xml.str(IDS_STRING1135),
				 xml.str(IDS_STRING1136));
			
			m_sMsgUpdatePricelist1 =  xml.str(IDS_STRING1139);
			m_sMsgUpdatePricelist2 =  xml.str(IDS_STRING1140);
			
			m_sMsgDupletTagNumber.Format(L"%s\n\n",
				xml.str(IDS_STRING1765));

			m_sMsgDupletTagNumber2 = xml.str(IDS_STRING17650);

			m_sMsgMissingSpcInPricelist.Format(L"%s\n%s\n\n%s\n\n",
				xml.str(IDS_STRING1767),
				xml.str(IDS_STRING1768),
				xml.str(IDS_STRING1769));

			m_sMsgMissingGradesInPricelist.Format(L"%s\n%s\n\n%s\n\n",
				xml.str(IDS_STRING1770),
				xml.str(IDS_STRING1771),
				xml.str(IDS_STRING1772));
			
			m_sMsgDeleteLogFromTicket.Format(L"%s\n%s\n%s\n%s\n\n",
				xml.str(IDS_STRING1773),
				xml.str(IDS_STRING1774),
				xml.str(IDS_STRING1775),
				xml.str(IDS_STRING1776));

			m_sMsgDuplicateTagNumbers.Format(L"%s\n%s\n\n%s\n\n",
				xml.str(IDS_STRING1141),
				xml.str(IDS_STRING1142),
				xml.str(IDS_STRING1143));
			
			m_sMsgStatusSoldNotChange.Format(L"%s\n%s\n\n",
				xml.str(IDS_STRING1777),
				xml.str(IDS_STRING1778));
			
			m_sMsgStatusSawmillNotChange.Format(L"%s\n%s\n\n",
				xml.str(IDS_STRING1779),
				xml.str(IDS_STRING1780));
			
			m_sMsgErrorMeasuringMode = xml.str(IDS_STRING1781);
			m_sMsgErrorMeasuringMode2 = xml.str(IDS_STRING1782);

			m_sMsgIllegalChars = xml.str(IDS_STRING18441);

			tokenizeString(xml.str(IDS_STRING201),';',m_sarrLoadType);
			tokenizeString(xml.str(IDS_STRING202),';',m_sarrDeductionType);
			tokenizeString(xml.str(IDS_STRING203),';',m_sarrLogStatus);

			m_lbl5_1.SetWindowTextW(xml.str(IDS_STRING2001));
			m_lbl5_2.SetWindowTextW(xml.str(IDS_STRING2002));
			m_lbl5_3.SetWindowTextW(xml.str(IDS_STRING2003));
			m_lbl5_4.SetWindowTextW(xml.str(IDS_STRING2004));
			m_lbl5_5.SetWindowTextW(xml.str(IDS_STRING2005)); 
			m_lbl5_6.SetWindowTextW(xml.str(IDS_STRING2006));
			m_lbl5_7.SetWindowTextW(xml.str(IDS_STRING2007));
			m_lbl5_8.SetWindowTextW(xml.str(IDS_STRING2008));
			m_lbl5_9.SetWindowTextW(xml.str(IDS_STRING2009));
			m_lbl5_10.SetWindowTextW(xml.str(IDS_STRING2011));
			m_lbl5_11.SetWindowTextW(xml.str(IDS_STRING2012));
			m_lbl5_12.SetWindowTextW(xml.str(IDS_STRING2013));
//			m_lbl5_13.SetWindowTextW(xml.str(IDS_STRING2014)); Anv�nds inte
			m_lbl5_14.SetWindowTextW(xml.str(IDS_STRING2015));
			m_lbl5_15.SetWindowTextW(xml.str(IDS_STRING2010));
			m_lbl5_16.SetWindowTextW(xml.str(IDS_STRING2016));
			m_lbl5_17.SetWindowTextW(xml.str(IDS_STRING2017));
			m_lbl5_18.SetWindowTextW(xml.str(IDS_STRING2018));
			m_lbl5_19.SetWindowTextW(xml.str(IDS_STRING2019));
			m_lbl5_20.SetWindowTextW(xml.str(IDS_STRING2024));
			m_lbl5_22.SetWindowTextW(xml.str(IDS_STRING2021));
			m_lbl5_24.SetWindowTextW(xml.str(IDS_STRING2020));
			
			m_check5_1.SetWindowTextW(xml.str(IDS_STRING1785));
			m_sTrimFT = xml.str(IDS_STRING2022);
			m_sTrimCM = xml.str(IDS_STRING2023);

			m_lbl5_21.SetBkColor(INFOBK);
			m_lbl5_21.SetTextColor(BLUE);
			m_lbl5_25.SetBkColor(INFOBK);
			m_lbl5_25.SetTextColor(BLUE);

			m_sMsgCap = xml.str(IDS_STRING99);
			m_sMsgDelete1 = xml.str(IDS_STRING1760);
			m_sMsgDelete2 = xml.str(IDS_STRING1761);

			m_edit5_3.SetAsNumeric();
			m_edit5_3.ModifyStyle(0,ES_RIGHT);
			m_edit5_5.SetAsNumeric();
			m_edit5_5.ModifyStyle(0,ES_RIGHT);
			m_edit5_6.SetLimitText(128);
			m_edit5_7.SetLimitText(50);
			m_edit5_8.SetLimitText(50);
			m_edit5_10.SetAsNumeric();
			m_edit5_10.ModifyStyle(0,ES_RIGHT);

			m_dtDate.SetTime(&CTime::GetCurrentTime());

			tokenizeString(xml.str(IDS_STRING205),';',m_sarrMeasuringMode);

			getDLLVolumeFuncDesc(m_vecFuncDesc,m_vecUserVolTables);

			m_cbox5_9.ResetContent();
			for (int i = 0;i < m_sarrMeasuringMode.GetCount();i++)
			{
				m_cbox5_9.AddString(m_sarrMeasuringMode.GetAt(i));
			}

			CLogsReportView *pView = getLogsView();
			CTicketLogsFrame* pWnd = (CTicketLogsFrame *)getFormViewParentByID(IDD_FORMVIEW5);
			if (m_wndSubList.GetSafeHwnd() == NULL && pView != NULL)
			{
				m_wndSubList.SubclassDlgItem(IDC_COLUMNS3, &pWnd->m_wndFieldSelectionDlg);
				pView->GetReportCtrl().GetColumns()->GetReportHeader()->SetSubListCtrl(&m_wndSubList);
			}

			UpdateData(FALSE);
		}
		xml.clean();


		m_bInitialized = TRUE;
	}

}

BOOL CTicketLogsFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{

	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		if (m_dbConnectionData.conn->isConnected())
		{
			m_pDB = new CDBHandling(m_dbConnectionData);

			if (m_pDB != NULL)
			{
				m_pDB->getPricelist(m_vecPricelist);
				m_pDB->getSpecies(m_vecSpecies);
				m_pDB->getGrades(m_vecGrades);
				m_pDB->getRegister(m_vecRegister);
				m_pDB->getDefaults(m_vecDefaults);
				m_pDB->getTickets(m_vecTickets);
				m_pDB->getUserVolTables(m_vecUserVolTables);
				m_pDB->getDefaultUserVolFuncSel(m_vecDefUserVolFuncs);
				m_pDB->getTmplTables(m_vecLogScaleTemplates);		
//				m_pDB->getAllLogs(m_vecAllLogs);
			}
		}
	}

	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CTicketLogsFormView::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	if (m_wndTabControl.GetSafeHwnd())
	{
		m_wndTabControl.MoveWindow(0, 180, cx, cy-185);
	}
}

LRESULT CTicketLogsFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	CString S;
	m_nOpenLogsViewAs = wParam;
	switch(wParam)
	{
		case ID_OPEN_LOGS :	
		{
			CTickets *rec = (CTickets*)lParam;
			if (m_pDB != NULL && rec != NULL)
			{
				if (sizeof(*rec) == sizeof(CTickets))
				{
					m_recTicket = *rec;
					m_selectedTemplate = getTemplate(m_recTicket.getTemplateID());

					// We'll try to clean the logstable
					m_pDB->cleanLogsTable(m_recTicket.getPKID());

					// Add logs to report
					populateLogsReport();
					// Add Ticket info
					populateData(FALSE /* Reload data */,FALSE /* Save logs */);
					//updLogs();
					CTicketLogsFrame *pFrame = (CTicketLogsFrame*)getFormViewParentByID(IDD_FORMVIEW5);
					if (pFrame != NULL)
					{
						pFrame->enableToolsMenu(m_recTicket.getPKID() > -1);
					}

					m_edit5_1.SetFocus();
				}
			}
			break;
		}	// case ID_MSG_FROM :

		case ID_OPEN_LOGS_SEARCH :	
		{
			CSearch *rec = (CSearch*)lParam;
			if (m_pDB != NULL && rec != NULL)
			{
				if (sizeof(*rec) == sizeof(CSearch))
				{
					// Get ticket from data sent from Search, i.e. ticket id
					m_pDB->getTicket(m_recTicket,rec->getTicketID());
					m_selectedTemplate = getTemplate(m_recTicket.getTemplateID());

					// We'll try to clean the logstable
					m_pDB->cleanLogsTable(m_recTicket.getPKID());

					// Add logs to report
					populateLogsReport(true,rec->getLogID());
					// Add Ticket info
					populateData(FALSE /* Reload data */,FALSE /* Save logs */);
					//updLogs();
					CTicketLogsFrame *pFrame = (CTicketLogsFrame*)getFormViewParentByID(IDD_FORMVIEW5);
					if (pFrame != NULL)
					{
						pFrame->enableToolsMenu(m_recTicket.getPKID() > -1);
					}

					//m_edit5_1.SetFocus();

					// We should try to point to the selected log in Search

				}
			}
			break;
		}	// case ID_MSG_FROM :

		case ID_OPEN_LOGS_FILE :	
		{
			CTickets *rec = (CTickets*)lParam;
			if (m_pDB != NULL && rec != NULL)
			{
				if (sizeof(*rec) == sizeof(CTickets))
				{
				
					m_recTicket = *rec;
					m_selectedTemplate = getTemplate(m_recTicket.getTemplateID());

					// We'll try to clean the logstable
					m_pDB->cleanLogsTable(m_recTicket.getPKID());
					
					m_selectedTemplate = CLogScaleTemplates(-1,m_recTicket.getTmplUsed(),L"",L"",
						getRegisterID(REGISTER_TYPES::BUYER,m_recTicket.getBuyer(),m_vecRegister),
						getRegisterID(REGISTER_TYPES::HAULER,m_recTicket.getHauler(),m_vecRegister),
						getRegisterID(REGISTER_TYPES::LOCATION,m_recTicket.getLocation(),m_vecRegister),
						getRegisterID(REGISTER_TYPES::TRACTID,m_recTicket.getTractID(),m_vecRegister),
						getRegisterID(REGISTER_TYPES::SCALER,m_recTicket.getScaler(),m_vecRegister),
						getRegisterID(REGISTER_TYPES::SOURCEID,m_recTicket.getSourceID(),m_vecRegister),
						getRegisterID(REGISTER_TYPES::VENDOR,m_recTicket.getVendor(),m_vecRegister),
						getRegisterID(REGISTER_TYPES::SUPPLIER,m_recTicket.getSupplier(),m_vecRegister),
						L"",
						m_selectedTemplate.getPricelist(),
						m_selectedTemplate.getPrlID(),
						m_selectedTemplate.getPrlName(),
						m_selectedTemplate.getTrimFT(),
						m_selectedTemplate.getTrimCM(),
						m_selectedTemplate.getMeasureMode(),
						m_selectedTemplate.getDefSpecies(),
						m_selectedTemplate.getDefGrade());

					m_recTicket.setPrlUsed(m_selectedTemplate.getPrlName());
					m_recTicket.setPrlID(m_selectedTemplate.getPrlID());
					m_recTicket.setTrimFT(m_selectedTemplate.getTrimFT());
					m_recTicket.setTrimCM(m_selectedTemplate.getTrimCM());
					m_recTicket.setMeasuringMode(m_selectedTemplate.getMeasureMode());

					// We need to update ticket; Trim etc.
					m_pDB->updTicket(m_recTicket);

					setTicketDataPricelist();
					setTicketDataExtraVFunc();
					// Add logs to report
					populateLogsReport();
					// Add Ticket info
					populateData(FALSE /* Reload data */,FALSE /* Save logs */);
					// Set barkreduction to barkreduction set in Ticket-file
					matchBarkReductionForSpeciesInTicket();
					updLogs();
					CTicketLogsFrame *pFrame = (CTicketLogsFrame*)getFormViewParentByID(IDD_FORMVIEW5);
					if (pFrame != NULL)
					{
						pFrame->enableToolsMenu(m_recTicket.getPKID() > -1);
					}
					m_edit5_1.SetFocus();
				}

			}
			break;
		}	// case ID_MSG_FROM :


		case ID_OPEN_LOGS_TMPL :	
		{
			CLogScaleTemplates *rec = (CLogScaleTemplates*)lParam;
			if (m_pDB != NULL && rec != NULL)
			{
				if (sizeof(*rec) == sizeof(CLogScaleTemplates))
				{
					m_recTicket = CTickets();
					m_selectedTemplate = *rec;

					// We'll try to clean the logstable
					m_pDB->cleanLogsTable(m_recTicket.getPKID());

					CString csDate;
					CTime ctDate;
					m_dtDate.GetTime(ctDate);
					csDate.Format(_T("%04d-%02d-%02d"), ctDate.GetYear(), ctDate.GetMonth(), ctDate.GetDay());

					m_recTicket.setDate(csDate);	//m_dt5_1.getDate());
					m_recTicket.setTractID(getRegisterValue(REGISTER_TYPES::TRACTID,m_selectedTemplate.getTractID(),m_vecRegister));
					m_recTicket.setSourceID(getRegisterValue(REGISTER_TYPES::SOURCEID,m_selectedTemplate.getSourceID(),m_vecRegister));
					m_recTicket.setScaler(getRegisterValue(REGISTER_TYPES::SCALER,m_selectedTemplate.getScaler(),m_vecRegister));
					m_recTicket.setLocation(getRegisterValue(REGISTER_TYPES::LOCATION,m_selectedTemplate.getLocation(),m_vecRegister));
					m_recTicket.setSupplier(getRegisterValue(REGISTER_TYPES::SUPPLIER,m_selectedTemplate.getSupplier(),m_vecRegister));
					m_recTicket.setHauler(getRegisterValue(REGISTER_TYPES::HAULER,m_selectedTemplate.getHauler(),m_vecRegister));
					m_recTicket.setBuyer(getRegisterValue(REGISTER_TYPES::BUYER,m_selectedTemplate.getBuyer(),m_vecRegister));
					m_recTicket.setVendor(getRegisterValue(REGISTER_TYPES::VENDOR,m_selectedTemplate.getVendor(),m_vecRegister));
					m_recTicket.setPrlUsed(m_selectedTemplate.getPrlName());
					m_recTicket.setPrlID(m_selectedTemplate.getPrlID());
					m_recTicket.setTrimFT(m_selectedTemplate.getTrimFT());
					m_recTicket.setTrimCM(m_selectedTemplate.getTrimCM());
					m_recTicket.setMeasuringMode(m_selectedTemplate.getMeasureMode());
					m_recTicket.setTmplUsed(m_selectedTemplate.getTmplName());
					m_recTicket.setTemplateID(m_selectedTemplate.getTmplID());

					// Add logs to report
					//populateLogsReport();
					// Add Ticket info
					populateData(FALSE /* Reload data */,FALSE /* Save logs */);
					//updLogs();
					CTicketLogsFrame *pFrame = (CTicketLogsFrame*)getFormViewParentByID(IDD_FORMVIEW5);
					if (pFrame != NULL)
					{
						pFrame->enableToolsMenu(m_recTicket.getPKID() > -1);
					}
#ifdef _AUTOGENERATE_TICKET_NUM
					int nNum = m_pDB->getLastLoadID_generate();
					/*
					// Set a SalesID
					if (m_vecTickets.size() > 0)
					{
						nNum = m_vecTickets[m_vecTickets.size()-1].getPKID();
						if (nNum == 0)
							nNum = 1;
					}
					*/

					m_edit5_1.SetWindowTextW(generateTicketNumber(nNum+1));
#endif
					m_edit5_1.SetFocus();
				}

				newTicket();
			}
			break;
		}	// case ID_MSG_FROM :
	};

	return 0L;
}

void CTicketLogsFormView::setEnable(BOOL enable)
{
	m_edit5_1.SetReadOnly(!enable);
	m_edit5_1.EnableWindow(enable);

	m_edit5_2.SetReadOnly(!enable);
	m_edit5_2.EnableWindow(enable);

	m_edit5_3.SetReadOnly(!enable);
	m_edit5_3.EnableWindow(enable);
	//m_edit5_4.EnableWindow(enable);
	
	m_edit5_5.SetReadOnly(!enable);
	m_edit5_5.EnableWindow(enable);
	
	m_edit5_6.SetReadOnly(!enable);
	m_edit5_6.EnableWindow(enable);
	
	m_edit5_7.SetReadOnly(!enable);
	m_edit5_7.EnableWindow(enable);

	m_edit5_8.SetReadOnly(!enable);
	m_edit5_8.EnableWindow(enable);

	m_edit5_10.SetReadOnly(!enable);
	m_edit5_10.EnableWindow(enable);

	m_lbl5_21.SetBkColor((!enable ? COL3DFACE : INFOBK));
	m_lbl5_25.SetBkColor((!enable ? COL3DFACE : INFOBK));
	
	//m_dt5_1.EnableWindow(enable);
	m_dtDate.EnableWindow(enable);

	m_cbox5_1.EnableWindow(enable);
	m_cbox5_2.EnableWindow(enable);
	m_cbox5_3.EnableWindow(enable);
	m_cbox5_4.EnableWindow(enable);
	m_cbox5_5.EnableWindow(enable);
	m_cbox5_6.EnableWindow(enable);
	m_cbox5_7.EnableWindow(enable);
	m_cbox5_8.EnableWindow(enable);
	m_cbox5_9.EnableWindow(FALSE);
	m_cbox5_10.EnableWindow(enable);
	m_cbox5_12.EnableWindow(enable);

	m_btnContract.EnableWindow(enable);
	m_btnPricelist.EnableWindow(enable);
}

void CTicketLogsFormView::addLogs()
{
	CLogsReportView *pView = getLogsView();
	CLogsReportRec *pRec = NULL;
	CString sStr = L"";
	CSpecies recSpc = CSpecies();
	CGrades recGrade = CGrades();
	if (pView != NULL)
	{
		recSpc = getSpeciesData(m_selectedTemplate.getDefSpecies());
		recGrade = getGradesData(m_selectedTemplate.getDefGrade());


		CXTPReportColumn* pCol = pView->GetReportCtrl().GetColumns()->GetAt(COLUMNS::LOG_TAG_NUMBER);
		if (pCol != NULL)
			pCol->SetSortIncreasing(TRUE);

		pView->GetReportCtrl().GetColumns()->ResetSortOrder();

		CLogs rec = CLogs(-1,	// Indicates a new log
											m_recTicket.getPKID(),
											L"",	// Empty field, user must enter
											recSpc.getSpcCode(), // getDefaults(DEF_SPC_CODE,m_vecDefaults,m_vecCalcTypes),
											recSpc.getSpcName(), // speciesName(getDefaults(DEF_SPC_CODE,m_vecDefaults,m_vecCalcTypes)),
											_tstoi(getDefaults(DEF_FREQUENCY,m_vecDefaults,m_vecCalcTypes)),
											recGrade.getGradesCode(), // getDefaults(DEF_LOG_GRADE,m_vecDefaults,m_vecCalcTypes),
											0.0,0.0,0.0,0.0,0.0,0.0,0.0,
											getTicketSpcBarkReduction( recSpc.getSpcCode()), // getDefaults(DEF_SPC_CODE,m_vecDefaults,m_vecCalcTypes)),
											0.0,0.0,
											getTicketPrlFunctionID(recSpc.getSpcCode(),recGrade.getGradesCode()), //getDefaults(DEF_SPC_CODE,m_vecDefaults,m_vecCalcTypes),getDefaults(DEF_LOG_GRADE,m_vecDefaults,m_vecCalcTypes)),
											getTicketPrlFunction(recSpc.getSpcCode(),recGrade.getGradesCode()), //getDefaults(DEF_SPC_CODE,m_vecDefaults,m_vecCalcTypes),getDefaults(DEF_LOG_GRADE,m_vecDefaults,m_vecCalcTypes)),
											0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,
											0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,
											0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,
											L"",L"",L"",L"",LOGSTATUS::IN_STOCK,-1,LOGORIGIN::MANUALLY);
		pView->GetReportCtrl().AddRecord(new CLogsReportRec(rec,m_sarrLogStatus,m_recTicket.getMeasuringMode(),m_vecSelectedVolFuncs));
		pView->GetReportCtrl().Populate();
		pView->GetReportCtrl().UpdateWindow();

		if (m_pDB != NULL)
		{
			m_cbox5_9.EnableWindow(!m_pDB->newLog(rec));
		}

		populateLogsReport();

		CXTPReportRow *pRow = NULL;
		if (pView->GetReportCtrl().GetRows()->GetCount() > 0)
			pRow = pView->GetReportCtrl().GetRows()->GetAt(pView->GetReportCtrl().GetRows()->GetCount()-1);
		if (pRow != NULL)
		{
			pView->GetReportCtrl().SetFocusedRow(pRow);
		}
	}
}

void CTicketLogsFormView::delLogs()
{
	CLogsReportRec *pRec = NULL;
	CXTPReportRow *pRow = NULL;
	CLogsReportView *pView = NULL;
	if ((pView = getLogsView()) != NULL && m_pDB != NULL)
	{
		if ((pRow = pView->GetReportCtrl().GetFocusedRow()) != NULL)
		{
			if ((pRec = (CLogsReportRec *)pRow->GetRecord()) != NULL)
			{	
				// Add a check on the logs status. Only status "In Stock" will
				// allow for deletion
				if (pRec->getRecord().getStatusFlag() == LOGSTATUS::IN_STOCK)
				{	
					if (::MessageBox(GetSafeHwnd(),m_sMsgDelete1,m_sMsgCap,MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
					{
						return;
					}
#ifdef _ASK_TWICE_ON_DELETE_LOG_ON_TICKET
					else if (::MessageBox(GetSafeHwnd(),m_sMsgDelete2,m_sMsgCap,MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
					{
						return;
					}
#endif
					if (m_pDB->delLog(pRec->getRecord()))
					{
						populateLogsReport();
					}	// if (m_pDB->delLog(pRec->getRecord()))
				}
				else
				{
					::MessageBox(GetSafeHwnd(),m_sMsgDeleteLogFromTicket,m_sMsgCap,MB_ICONQUESTION | MB_OK);					
				}
			}	// if (pRec = (CLogsReportRec *)pRow->GetRecord()) != NULL)
		}	// if ((pRow = pView->GetReportCtrl().GetFocusedRow()) != NULL)
	}	// if ((pView = getLogsView()) != NULL)
}
// Called from: CTicketLogsFrame::OnUpdLogs()
void CTicketLogsFormView::updLogs()
{
	CLogsReportView *pView = getLogsView();
	if (pView != NULL)
	{
		pView->SaveAllLogs(FALSE);
	}
}


//BOOL CTicketLogsFormView::checkLogTagNumbers(CVecLogs &vec_in_ticket)
BOOL CTicketLogsFormView::checkLogTagNumbers(LPCTSTR ticket,LPCTSTR ticket_file,CVecLogs &vec_in_ticket,CVecLogs &vec_logs_to_add_to_inventory)
{
	BOOL bFound = FALSE,bQuit = TRUE;
	CString sTmp = L"";
	CString sSQL = L"";
	vecDuplicateLogs vec_duplicates;
	CLogs recLog = CLogs();
	CVecLogs vec_match;	// Logs already in logs-inventory
	CStrIntMap mapTags;	// Actual number of valid tags in file, also counts duplicates
	if (vec_in_ticket.size() > 0)
	{
		for (UINT i1 = 0;i1 < vec_in_ticket.size();i1++)
		{
			sTmp.Format(L"%s.sTagNumber='%s' ",TBL_LOGS,vec_in_ticket[i1].getTagNumber());
			if (i1 < vec_in_ticket.size()-1)
				sSQL += sTmp + L" or ";
			else
				sSQL += sTmp;

			mapTags[vec_in_ticket[i1].getTagNumber()] += 1;
		}

		if (m_pDB != NULL)
		{
			m_pDB->checkLogTagNumbers(sSQL,vec_duplicates);
		}
	}

	// Setup Match vector
	if (vec_duplicates.size() > 0 && vec_in_ticket.size() > 0)
	{
		for (UINT i = 0;i < vec_duplicates.size();i++)
		{
			for (UINT i1 = 0;i1 < vec_in_ticket.size();i1++)
			{
				recLog = vec_in_ticket[i1];
				if (recLog.getTagNumber().CompareNoCase(vec_duplicates[i].sTagNumber) == 0)
				{
					recLog.setTicket(vec_duplicates[i].sTicketNumber);
					vec_match.push_back(recLog);
					break;
				}	// if (vec_in_ticket[i1].getTagNumber().CompareNoCase(vec_duplicates[i].sTagNumber) == 0)
			}	// for (UINT i1 = 0;i1 < vec_in_ticket.size();i1++)
		}	// for (UINT i = 0;i < vec_duplicates.size();i++)

		for (UINT i1 = 0;i1 < vec_in_ticket.size();i1++)
		{
			recLog = vec_in_ticket[i1];
			bFound = FALSE;
			if (vec_duplicates.size() > 0)
			{
				for (UINT i = 0;i < vec_duplicates.size();i++)
				{
					if (recLog.getTagNumber().CompareNoCase(vec_duplicates[i].sTagNumber) == 0)
					{
						bFound = TRUE;
						break;
					}	// if (vec_in_ticket[i1].getTagNumber().CompareNoCase(vec_duplicates[i].sTagNumber) == 0)
				}	// for (UINT i1 = 0;i1 < vec_in_ticket.size();i1++)
			}
			if (!bFound)
			{
				vec_logs_to_add_to_inventory.push_back(recLog);
			}
		}	// for (UINT i = 0;i < vec_duplicates.size();i++)
	}
	else if (vec_in_ticket.size() > 0 && vec_duplicates.size() == 0)
	{
		bQuit = TRUE;
		for (UINT i1 = 0;i1 < vec_in_ticket.size();i1++)
		{
			recLog = vec_in_ticket[i1];
			vec_logs_to_add_to_inventory.push_back(recLog);
		}	// for (UINT i = 0;i < vec_duplicates.size();i++)
	}

//	if (vec_duplicates.size() > 0)
//	{

		CTagNoMatchDlg *pDlg = new CTagNoMatchDlg(NULL,0);
		if (pDlg != NULL)
		{	
			pDlg->setLogTagsNotOK(vec_match);
			pDlg->setLogTagsOK(vec_in_ticket);
			pDlg->setTotalNumberOfLogs(vec_in_ticket.size());
			pDlg->setLogTags(mapTags);
			pDlg->setLogTagsDup(mapTags);
			pDlg->setTicket(ticket);
			pDlg->setTicketFile(ticket_file);
			if (pDlg->DoModal() == IDOK)
			{
				bQuit = TRUE;
				if (vec_logs_to_add_to_inventory.size() == vec_match.size())
				{
					//::MessageBox(GetSafeHwnd(),m_sMsgDuplicateTagNumbers,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
					bQuit = FALSE;
				}
			}
			else
			{
				bQuit = FALSE;
			}
			delete pDlg;
		}
//	}

	if (bQuit)
		return ((vec_duplicates.size() == 0) || (vec_logs_to_add_to_inventory.size() > 0));	// No matches
	else
		return FALSE;
}

BOOL CTicketLogsFormView::checkLogTagNumber(int row,LPCTSTR tag_num)
{
	if (m_vecLogs.size() == 0)
		return TRUE;
	for (UINT i = 0;i < m_vecLogs.size();i++)
	{
		if (i != row)
		{
			if (m_vecLogs[i].getTagNumber().CompareNoCase(tag_num) == 0)
			{
				::MessageBox(GetSafeHwnd(),m_sMsgDupletTagNumber,m_sMsgCap,MB_ICONASTERISK | MB_OK);
				return FALSE;
			}
		}
	}
/*
	// We'll also check the logtag-number against all logtags
	// to see that the tag-number haven been used already.
	if (m_vecAllLogs.size() > 0)
	{
		for (UINT i = 0;i < m_vecAllLogs.size();i++)
		{
			if (m_vecAllLogs[i].getTagNumber().CompareNoCase(tag_num) == 0)
			{
				::MessageBox(GetSafeHwnd(),m_sMsgDupletTagNumber2,m_sMsgCap,MB_ICONASTERISK | MB_OK);
				return FALSE;
			}
		}
	}
*/
	if (m_pDB != NULL)
	{
		if (!m_pDB->isLogTagUnique3(tag_num))
		{
			::MessageBox(GetSafeHwnd(),m_sMsgDupletTagNumber2,m_sMsgCap,MB_ICONASTERISK | MB_OK);
			return FALSE;
		}
	}


	return TRUE;
}

// Only check against ALL Logs in Log-inventory
BOOL CTicketLogsFormView::checkLogTagNumber(CLogs& rec)
{
	BOOL bReturn = TRUE;
/*
	if (m_pDB != NULL)
	{
		m_pDB->getAllLogs(m_vecAllLogs);
	}

	// We'll also check the logtag-number against all logtags
	// to see that the tag-number haven been used already.
	if (m_vecAllLogs.size() > 0)
	{
		for (UINT i = 0;i < m_vecAllLogs.size();i++)
		{
			if (m_vecAllLogs[i].getTagNumber().CompareNoCase(rec.getTagNumber()) == 0 &&
					m_vecAllLogs[i].getLoadID() != rec.getLoadID() &&
					m_vecAllLogs[i].getLogID() != rec.getLogID())
			{
//				::MessageBox(GetSafeHwnd(),m_sMsgDupletTagNumber2,m_sMsgCap,MB_ICONASTERISK | MB_OK);
				return FALSE;
			}
		}
	}
*/

	if (m_pDB != NULL)
	{
		bReturn = m_pDB->isLogTagUnique2(rec);
	}

	return bReturn;
}


BOOL CTicketLogsFormView::checkLogData(CVecLogs& vec,CTickets& rec,bool ticket_num_ok,LPCTSTR file_name)
{
	BOOL bReturn = FALSE;
	CSpecies recSpecies = CSpecies();
	CGrades recGrades = CGrades();
	CLogs recLog = CLogs();
	CVecLogs vec_to_add_to_inventory;
	BOOL bFoundSpc = FALSE;
	BOOL bFoundGrade = FALSE;
	CString sLogSpecies = L"";
	CString sSpecies = L"";
	CString sLogGrade = L"";
	CString sGrade = L"";
	CString sGenerateTicketNum = L"";


	if (vec.size() > 0)
	{
		if (!checkLogTagNumbers(rec.getTicket(),file_name,vec,vec_to_add_to_inventory))
		{
			//::MessageBox(GetSafeHwnd(),m_sMsgDuplicateTagNumbers,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
			return FALSE;
		}

		for (UINT log = 0;log < vec_to_add_to_inventory.size();log++)
		{
			recLog = vec_to_add_to_inventory[log];
			sLogSpecies = recLog.getSpcCode().Trim();
			sLogGrade = recLog.getGrade().Trim();
			bFoundSpc = FALSE;
			// First check that speciecodes in logs from file
			// are included in logscaleSpecies-table
			if (m_vecSpecies.size() > 0)
			{
				for (UINT spc = 0;spc < m_vecSpecies.size();spc++)
				{
					recSpecies = m_vecSpecies[spc];
					sSpecies = recSpecies.getSpcCode().Trim();
					if (sLogSpecies.CompareNoCase(sSpecies) == 0)
					{
						bFoundSpc = TRUE;
						break;
					}
				}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
			}	// if (m_vecSpecies.size() > 0)

			bFoundGrade = FALSE;
			// First check that speciecodes in logs from file
			// are included in logscaleSpecies-table
			if (m_vecGrades.size() > 0)
			{
				for (UINT grd = 0;grd < m_vecGrades.size();grd++)
				{
					recGrades = m_vecGrades[grd];
					sGrade = recGrades.getGradesCode().Trim();
					if (sLogGrade.CompareNoCase(sGrade) == 0)
					{
						bFoundGrade = TRUE;
						break;
					}
				}	// for (UINT i = 0;i < m_vecSpecies.size();i++)
			}	// if (m_vecSpecies.size() > 0)

			if (!bFoundSpc || !bFoundGrade)
			{
				if (!bFoundSpc && bFoundGrade)
					vec_to_add_to_inventory[log].setFlag(1);
				else if (bFoundSpc && !bFoundGrade)
					vec_to_add_to_inventory[log].setFlag(2);
				else if (!bFoundSpc && !bFoundGrade)
					vec_to_add_to_inventory[log].setFlag(3);
			}

		}	// for (UINT log = 0;log < vec.size();log++)
	}
	if (vec.size() > 0)
	{
		if (m_pDB != NULL)
		{
			sGenerateTicketNum = generateTicketNumber(m_pDB->getLastLoadID_generate());
		}

		CCorrectionDlg *pDlg = new CCorrectionDlg(NULL,1);
		if (pDlg != NULL)
		{
			pDlg->setErrorLogs(vec_to_add_to_inventory);
			pDlg->setSpecies(m_vecSpecies);
			pDlg->setGrades(m_vecGrades);
			pDlg->setRegister(m_vecRegister);
			pDlg->setTicket(rec,m_vecTickets);
			pDlg->setTemplates(m_vecLogScaleTemplates);		
			//pDlg->setTicketNumOK(true);
			pDlg->setGeneratedTicketNum(sGenerateTicketNum);
			pDlg->setOpenLogsViewAs(m_nOpenLogsViewAs);
			if (pDlg->DoModal() == IDOK)
			{
				vec = pDlg->getLogs();
				rec = pDlg->getTicket();
				bReturn = TRUE;
				UpdateWindow();
			}
			delete pDlg;
		}
	}
	return bReturn;
}

BOOL CTicketLogsFormView::checkTicketExists(LPCTSTR ticket_num)
{
	CTickets rec;
	CString sTicketA = L"";
	CString sTicketB = ticket_num;

	if (m_vecTickets.size() > 0)
	{
		for (UINT i = 0;i < m_vecTickets.size();i++)
		{
			rec = m_vecTickets[i];
			sTicketA = rec.getTicket().Trim();
			if (sTicketA.CompareNoCase(sTicketB.Trim()) == 0 && 
					m_recTicket.getTicket().Trim().CompareNoCase(ticket_num) != 0 &&
					rec.getPKID() != m_recTicket.getPKID())	// Don't check the active ticket
			{
				m_bIsTicketNumberOK = FALSE;
				return TRUE;
			}
		}
	}

	m_bIsTicketNumberOK = TRUE;
	return FALSE;
}

BOOL CTicketLogsFormView::checkBarkReduction()
{
	BOOL bReturn = TRUE;
	if (m_vecTicketSpc.size() > 0)
	{
		for (UINT i = 0;i < m_vecTicketSpc.size();i++)
		{
			if (m_vecTicketSpc[i].getBarkReduction() == 0.0)
			{
				bReturn = FALSE;
				break;
			}
		}
		return bReturn;
	}

	return FALSE;
}

CGrades CTicketLogsFormView::getGradesData(int grades_id)
{
	if (m_vecGrades.size() == 0) return CGrades();
	for (UINT i = 0;i < m_vecGrades.size();i++)
	{
		if (m_vecGrades[i].getGradesID() == grades_id)
			return m_vecGrades[i];
	}
	return CGrades();
}

CSpecies CTicketLogsFormView::getSpeciesData(int spc_id)
{
	if (m_vecSpecies.size() == 0) return CSpecies();
	for (UINT i = 0;i < m_vecSpecies.size();i++)
	{
		if (m_vecSpecies[i].getSpcID() == spc_id)
			return m_vecSpecies[i];
	}
	return CSpecies();
}

CFuncDesc CTicketLogsFormView::getFuncData(int func_id)
{
	if (m_vecFuncDesc.size() == 0) return CFuncDesc();
	for (UINT i = 0;i < m_vecFuncDesc.size();i++)
	{
		if (m_vecFuncDesc[i].getFuncID() == func_id)
			return m_vecFuncDesc[i];
	}

	return CFuncDesc();
}


void CTicketLogsFormView::setSpeciesIDOnLogs(void)
{
	if (m_vecSpecies.size() > 0 && m_vecLogs.size() > 0)
	{
		for (UINT i1 = 0;i1 <  m_vecLogs.size();i1++)
		{
			for (UINT i2 = 0;i2 <  m_vecSpecies.size();i2++)
			{
				if (m_vecLogs[i1].getSpcCode().CompareNoCase(m_vecSpecies[i2].getSpcCode()) == 0 &&
					  m_vecLogs[i1].getSpcName().CompareNoCase(m_vecSpecies[i2].getSpcName()) == 0)
				{
					m_vecLogs[i1].setSpcID(m_vecSpecies[i2].getSpcID());
					break;
				}
			}

		}
	}
}


double CTicketLogsFormView::getTicketSpcBarkReduction(LPCTSTR spc_code)
{
	if (m_vecTicketSpc.size() == 0)
		return 0.0;
	for (UINT i = 0;i < m_vecTicketSpc.size();i++)
	{
		if (m_vecTicketSpc[i].getSpeciesCode().CompareNoCase(spc_code) == 0)
		{
			return m_vecTicketSpc[i].getBarkReduction()*DOUBLE_BARK_FACTOR;
		}
	}

	return 0.0;
}

double CTicketLogsFormView::getLengthTrim(double length,int func_id)
{
/*
	char s[10];
	int nLength = 0;
	double fIntFrac = 0.0;
	double fLength = 0.0;
	double fTrim = 0.0;
*/
	return length;
/*
	// Check functionid. If functionid equals id for JAS
	// we need to round the length before trim
	fLength = length;
	if (func_id == ID_JAS && m_recTicket.getMeasuringMode() == 0)
	{
		// Omvandla fr�n feet till meter
		fLength = length*FT2M;
		fTrim = m_edit5_10.getFloat()*FT2M;
		
		modf(fLength*10.0,&fIntFrac);
		sprintf(s,"%.0f",fIntFrac);
		nLength = atoi(s);
		if ((nLength % 2) == 1)
			fLength -= 1.0;

		fLength = ((fIntFrac/10.0) - fTrim);

		return (fLength*M2FT);
	}
	else if (func_id == ID_JAS && m_recTicket.getMeasuringMode() == 1)
	{
		fTrim = m_edit5_10.getFloat()/100.0;

		modf(fLength*10.0,&fIntFrac);
		sprintf(s,"%.0f",fIntFrac);
		nLength = atoi(s);
		if ((nLength % 2) == 1)
			nLength -= 1;

		fLength = ((nLength/10.0) - fTrim);

		return fLength;

	}
	else if (func_id != ID_JAS && m_recTicket.getMeasuringMode() == 0)
		return (fLength - m_edit5_10.getFloat());
	else if (func_id != ID_JAS && m_recTicket.getMeasuringMode() == 1)
		return (fLength - m_edit5_10.getFloat()/100.0);
*/
}

int CTicketLogsFormView::getTicketPrlFunctionID(LPCTSTR spc_code,LPCTSTR grade_code)
{
	CString sSpc(spc_code),sGrade(grade_code);
	if (m_vecTicketPrl.size() == 0)
		return -1;
	for (UINT i = 0;i < m_vecTicketPrl.size();i++)
	{
		if (m_vecTicketPrl[i].getGradesCode().CompareNoCase(sGrade.Trim()) == 0 &&
			m_vecTicketPrl[i].getSpeciesCode().CompareNoCase(sSpc.Trim()) == 0)
		{
			return m_vecTicketPrl[i].getFuncID();
		}
	}

	return -1;
}

CString CTicketLogsFormView::getTicketPrlFunction(LPCTSTR spc_code,LPCTSTR grade_code)
{
	CString sSpc(spc_code),sGrade(grade_code);
	if (m_vecTicketPrl.size() == 0)
		return L"";
	for (UINT i = 0;i < m_vecTicketPrl.size();i++)
	{
		if (m_vecTicketPrl[i].getGradesCode().CompareNoCase(sGrade.Trim()) == 0 &&
			m_vecTicketPrl[i].getSpeciesCode().CompareNoCase(sSpc.Trim()) == 0)
		{
			return m_vecTicketPrl[i].getCalcType();
		}
	}
	return L"";
}
/*
CLogScaleTemplates CTicketLogsFormView::getTemplate(LPCTSTR tmpl_name)
{
	if (m_vecLogScaleTemplates.size() > 0)
	{
		for (UINT i = 0;i < m_vecLogScaleTemplates.size();i++)
		{
			if (m_vecLogScaleTemplates[i].getTmplName().CompareNoCase(tmpl_name) == 0)
			{
				return m_vecLogScaleTemplates[i];
			}
		}
	}

	return CLogScaleTemplates();
}
*/
CLogScaleTemplates CTicketLogsFormView::getTemplate(int tmpl_id)
{
	if (m_vecLogScaleTemplates.size() > 0)
	{
		for (UINT i = 0;i < m_vecLogScaleTemplates.size();i++)
		{
			if (m_vecLogScaleTemplates[i].getTmplID() == tmpl_id)
			{
				return m_vecLogScaleTemplates[i];
			}
		}
	}

	return CLogScaleTemplates();
}

void CTicketLogsFormView::ImportTicket(void)
{
	bool bTicketNumOK = true;
	int nLoadID = -1;
	CVecLogs vec;
	CLogs recLog = CLogs();
	CString  strFile = L"";
	CString strFilter = _T("LSA(*.lsa)|*.lsa|");

	strFile = regGetStr(REG_ROOT,TICKET_IMPORT_DIRECTORY,DIRECTORY_KEY);
	strFile += L"*.lsa";

	CFileDialog dlg(TRUE,L"lsa",strFile,4|2,strFilter);
	CTickets rec = CTickets();
	int nMode = -1;

	if(dlg.DoModal() == IDOK)
	{
		doEvents();

		regSetStr(REG_ROOT,TICKET_IMPORT_DIRECTORY,DIRECTORY_KEY,getFilePath(dlg.GetPathName()));

		CFileParser *pars = new CFileParser(dlg.GetPathName());
		if (m_pDB != NULL)
		{
			m_pDB->getTicketPrl(m_vecTicketPrl,m_recTicket.getPKID());
			if (m_recTicket.getPKID() > -1)
				{
					pars->getTicket(rec);
					
					// Nothing to do, measuremode the same
					if (rec.getMeasuringMode() == m_recTicket.getMeasuringMode() && rec.getMeasuringMode() == 0) 
						nMode = 4;				
					else if (rec.getMeasuringMode() == m_recTicket.getMeasuringMode() && rec.getMeasuringMode() == 1) 
						nMode = 5;				
					// Already entered data have inch and added data has meter
					else if (rec.getMeasuringMode() == 1 && m_recTicket.getMeasuringMode() == 0) 
						nMode = 1;	
					// Already entered data have meter and added data has inch
					else if (rec.getMeasuringMode() == 0 && m_recTicket.getMeasuringMode() == 1) 
						nMode = 2;	

					if (rec.getMeasuringMode() != m_recTicket.getMeasuringMode())
					{
						::MessageBox(GetSafeHwnd(),m_sMsgErrorMeasuringMode2,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
						return;
					}

					m_recTicket.setVendor(rec.getVendor());
					m_recTicket.setTractID(rec.getTractID());
					m_recTicket.setBuyer(rec.getBuyer());
					m_recTicket.setSupplier(rec.getSupplier());
					m_recTicket.setSourceID(rec.getSourceID());
					m_recTicket.setScaler(rec.getScaler());
					m_recTicket.setHauler(rec.getHauler());
					m_recTicket.setLocation(rec.getLocation());

					if (pars->getLogs(nMode, m_recTicket.getPKID(),vec))
					{
						// We'll do a check of data; speciecode and grades, in logs.
						// 
						if (checkLogData(vec,m_recTicket,bTicketNumOK,dlg.GetFileTitle()))
						{
							
							doEvents();
							/*
							CCalcVolumes *pCalc = new CCalcVolumes();
							if (pCalc != NULL)
							{
								pCalc->calculate(vec,m_recTicket);
								delete pCalc;
							}
							*/
							// Add logs to datbase table (timsLogs) for ticket (timsTickets)
							if (vec.size() > 0)
							{
								for (int i = 0;i < vec.size();i++)
								{
									recLog = vec[i];
									m_pDB->newLog(recLog);
								}	// for (int i = 0;i < vec.size();i++)
							}	// if (vec.size() > 0)

						}	// if (checkLogData(vec))
					}	// if (pars->getLogs(nLoadID,vec))
			}	// if (nLoadID > -1)
		}	// if (m_pDB != NULL)
		delete pars;
		// Do an update; sum. log data, and save it to ticket
		m_pDB->updSumLogData(nLoadID);

		updLogs();// SaveAllLogs() in CLogsReportView

	}	// if(dlg.DoModal() == IDOK)
}

void CTicketLogsFormView::ExportToEXCEL(void)
{
	// Use method in LogScaleView
	
	CString sRegKey = L"";
	CLogsReportView *pView = getLogsView();
	CLogScaleView *pView1 = (CLogScaleView*)getFormViewByID(IDD_FORMVIEW);
	if (pView != NULL && pView1 != NULL)
	{
		sRegKey.Format(L"%s_%d",m_recTicket.getTicket(),m_recTicket.getPKID());
		if(checkIllegalChars(m_recTicket.getTicket()) == false)	//#3611, lagt in koll inga ogiltiga tecken i Ticket numret	\ / ? : * " > < |
		{
			saveColumnsSettingToFile(sRegKey,pView->GetReportCtrl(),COLSET_TICKET);
			pView1->doExportToExcel(m_recTicket.getTicket(),m_recTicket.getPKID());
		}
		else
		{
			::MessageBox(GetSafeHwnd(),_T("\\ / ? : * \" > < |")+m_sMsgIllegalChars,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
		}
	}
}

double CTicketLogsFormView::getLogValue(BYTE nVolumeIndex,int spc_id,LPCTSTR grade_code, double dVolume)
{
	double dValue = 0.0,dPrice = 0.0;
	CString sGradeCode(grade_code);
	if (m_vecTicketPrl.size() > 0)
	{
		for (UINT i = 0;i < m_vecTicketPrl.size();i++)
		{
			if (m_vecTicketPrl[i].getSpeciesID() == spc_id && 
				m_vecTicketPrl[i].getGradesCode().CompareNoCase(sGradeCode.Trim()) == 0)
			{
				dPrice = m_vecTicketPrl[i].getPrice();
			}
		}

		dValue = dVolume*dPrice;

		switch(nVolumeIndex)
		{
			case ID_INT14:
			case ID_INT18:
			case ID_DOYLE:
			case ID_BRUCE_SHUM:
			case ID_SCRIB_W:
			//case ID_JAS:
				dValue /= 1000.0;
				break;
			case ID_CFIB:
				dValue /= 100.0;
				break;
			case ID_CFIB_100:
				break;
			case ID_CFOB:
				dValue /= 2000.0;
				break;
		};
	}
	return dValue;
}

// Create template
void CTicketLogsFormView::ExportTicket(void)
{
	// Use method in LogScaleView
	CLogScaleView *pView = (CLogScaleView*)getFormViewByID(IDD_FORMVIEW);
	if (pView != NULL)
	{
		pView->doExportTemplate();
	}

}

void CTicketLogsFormView::changeContract()
{
	vecPricelistInTicket vecTemplatePrlSpc;
	BOOL bFound = FALSE;
	BOOL bContinue = TRUE;
	// NB! PricelistDlg is used to change/update contract
	CPricelistDlg *dlg = new CPricelistDlg(m_recTicket);
	if (dlg != NULL)
	{
		if (dlg->DoModal() == IDOK)
		{

			// We'll check that the selected Contract is in the asme measuringmode as the ticket
			if (dlg->getSelTmpl().getMeasureMode() != m_recTicket.getMeasuringMode())
			{
				::MessageBox(GetSafeHwnd(),m_sMsgErrorMeasuringMode,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
				return;
			}


			if (::MessageBox(GetSafeHwnd(),m_sMsgChangeUpdateContract1,m_sMsgCap,MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
			{
				if (::MessageBox(GetSafeHwnd(),m_sMsgChangeUpdateContract2,m_sMsgCap,MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
				{
					delete dlg;
					return;
				}
			}
			else
			{
				delete dlg;
				return;
			}

			m_selectedTemplate.setPrlName(dlg->getSelTmpl().getPrlName());
			m_selectedTemplate.setPrlID(dlg->getSelTmpl().getPrlID());
			m_selectedTemplate.setPricelist(dlg->getSelTmpl().getPricelist());


		vecPricelistInTicket vec;
		// get ALL data for pricelist set in template
		getPricelistFromTemplate(m_selectedTemplate.getPricelist(),-1,vec);
#ifdef _CHECK_CONTRACT_SPECIES
		if (m_vecTicketSpc.size() > 0 && m_vecLogs.size() > 0)
		{
			for (UINT i3 = 0;i3 < m_vecLogs.size();i3++)
			{
				bFound = FALSE;
				for (UINT i4 = 0;i4 < vec.size();i4++)
				{
					if (vec[i4].getSpeciesID() == m_vecLogs[i3].getSpcID())
					{
						bFound = TRUE;
						break;
					}	// if (m_vecTicketSpc[i3].getSpeciesID() == m_vecTicketSpc[i4].getSpeciesID())
				}	// for (UINT i4 = 0;i4 < m_vecTicketSpc.size();i4++)
				if (!bFound)
				{
					::MessageBox(GetSafeHwnd(),m_sMsgMissingSpcInPricelist,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
					return;
				}
			}	// for (UINT i3 = 0;i3 < vecTemplatePrlSpc.size();i3++)
		}

#endif

#ifdef _CHECK_CONTRACT_GRADES
		if (vec.size() > 0 && m_vecLogs.size() > 0)
		{
			for (UINT i3 = 0;i3 < m_vecLogs.size();i3++)
			{
				bFound = FALSE;
				for (UINT i4 = 0;i4 < vec.size();i4++)
				{
					CGrades recGrades = getGradesData(vec[i4].getGradesID());
					if (recGrades.getGradesCode().CompareNoCase(m_vecLogs[i3].getGrade().Trim()) == 0 && 
						vec[i4].getSpeciesID() == m_vecLogs[i3].getSpcID())
					{
						bFound = TRUE;
						break;
					}	// if (m_vecTicketSpc[i3].getSpeciesID() == m_vecTicketSpc[i4].getSpeciesID())
				}	// for (UINT i4 = 0;i4 < m_vecTicketSpc.size();i4++)
				if (!bFound)
				{
					::MessageBox(GetSafeHwnd(),m_sMsgMissingGradesInPricelist,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
					return;
				}
			}	// for (UINT i3 = 0;i3 < vecTemplatePrlSpc.size();i3++)
		}
#endif


			m_selectedTemplate.setBuyer(dlg->getSelTmpl().getBuyer());
			m_selectedTemplate.setHauler(dlg->getSelTmpl().getHauler());
			m_selectedTemplate.setScaler(dlg->getSelTmpl().getScaler());
			m_selectedTemplate.setSupplier(dlg->getSelTmpl().getSupplier());
			m_selectedTemplate.setSourceID(dlg->getSelTmpl().getSourceID());
			m_selectedTemplate.setLocation(dlg->getSelTmpl().getLocation());
			m_selectedTemplate.setTractID(dlg->getSelTmpl().getTractID());
			m_selectedTemplate.setVendor(dlg->getSelTmpl().getVendor());

			m_recTicket.setPrlUsed(dlg->getSelTmpl().getPrlName());
			m_recTicket.setTmplUsed(dlg->getSelTmpl().getTmplName());
			m_recTicket.setTemplateID(dlg->getSelTmpl().getTmplID());
			m_recTicket.setTrimCM(dlg->getSelTmpl().getTrimCM());
			m_recTicket.setTrimFT(dlg->getSelTmpl().getTrimFT());
			m_recTicket.setBuyer(getRegisterValue(REGISTER_TYPES::BUYER,dlg->getSelTmpl().getBuyer(),m_vecRegister));
			m_recTicket.setHauler(getRegisterValue(REGISTER_TYPES::HAULER,dlg->getSelTmpl().getHauler(),m_vecRegister));
			m_recTicket.setScaler(getRegisterValue(REGISTER_TYPES::SCALER,dlg->getSelTmpl().getScaler(),m_vecRegister));
			m_recTicket.setSupplier(getRegisterValue(REGISTER_TYPES::SUPPLIER,dlg->getSelTmpl().getSupplier(),m_vecRegister));
			m_recTicket.setSourceID(getRegisterValue(REGISTER_TYPES::SOURCEID,dlg->getSelTmpl().getSourceID(),m_vecRegister));
			m_recTicket.setLocation(getRegisterValue(REGISTER_TYPES::LOCATION,dlg->getSelTmpl().getLocation(),m_vecRegister));
			m_recTicket.setTractID(getRegisterValue(REGISTER_TYPES::TRACTID,dlg->getSelTmpl().getTractID(),m_vecRegister));
			m_recTicket.setVendor(getRegisterValue(REGISTER_TYPES::VENDOR,dlg->getSelTmpl().getVendor(),m_vecRegister));
		
			setTicketDataPricelist();
			
			m_recTicket.setPrlUsed(m_selectedTemplate.getPrlName());
			m_recTicket.setPrlID(m_selectedTemplate.getPrlID());
			
//			updTicket();

			populateLogsReport();

			updLogs();	// SaveAllLogs

			populateData(TRUE,FALSE);
		}
		delete dlg;

	}
}

void CTicketLogsFormView::changePricelist()
{
	
	if (::MessageBox(GetSafeHwnd(),m_sMsgUpdatePricelist1,m_sMsgCap,MB_ICONASTERISK | MB_YESNO | MB_DEFBUTTON2) == IDYES)
	{
		if (::MessageBox(GetSafeHwnd(),m_sMsgUpdatePricelist2,m_sMsgCap,MB_ICONASTERISK | MB_YESNO | MB_DEFBUTTON2) == IDNO)
		{
			return;
		}
	}
	else
	{
		return;
	}

	// Make sure we have the most resent pricelist-information
	if (m_pDB != NULL)
	{
		m_pDB->getPricelist(m_vecPricelist);
	}
	// Find pricelist used for this contract
	if (m_vecPricelist.size() > 0)
	{
		for (UINT i = 0;i < m_vecPricelist.size();i++)
		{
			if (m_vecPricelist[i].getPrlID() == m_recTicket.getPrlID())
			{
				m_selectedTemplate.setPrlName(m_recTicket.getPrlUsed());
				m_selectedTemplate.setPrlID(m_recTicket.getPrlID());
				m_selectedTemplate.setPricelist(m_vecPricelist[i].getPricelist());

				setTicketDataPricelist();
					
				updTicket();

				populateLogsReport();

				updLogs();	// SaveAllLogs

				populateData(TRUE,FALSE);

				break;
			}
		}
	}
}

void CTicketLogsFormView::changeExtraVolFunc()
{
	CChangeUserFuncDlg *pDlg = new CChangeUserFuncDlg();
	if (pDlg != NULL && m_pDB != NULL)
	{
		pDlg->setData(m_recTicket.getPKID(),m_vecFuncDesc,m_vecSelectedVolFuncs);
		if (pDlg->DoModal() == IDOK)
		{
			m_pDB->delSelVolFuncs(m_recTicket.getPKID());
			m_vecSelectedVolFuncs.clear();
			m_vecSelectedVolFuncs = pDlg->selectedFunctions();
			// We'll also add default extra volumefunctions to database-table
			if (m_vecSelectedVolFuncs.size() > 0)
			{
				for (UINT i3 = 0;i3 < m_vecSelectedVolFuncs.size();i3++)
				{
					m_pDB->addSelVolFuncs(m_vecSelectedVolFuncs[i3]);
				}
			}
			updLogs();
		}

		delete pDlg;
	}
}

void CTicketLogsFormView::changeBarkReduction()
{
	CString sMMode = L"";
	if (m_recTicket.getMeasuringMode() >= 0 && m_recTicket.getMeasuringMode() < m_sarrMeasuringMode.GetCount())
	{
		if (m_recTicket.getMeasuringMode() == 0)	// Inch
		{
			sMMode =	m_sarrMeasuringMode.GetAt(0);
		}
		else if (m_recTicket.getMeasuringMode() == 1)
		{
			sMMode =	m_sarrMeasuringMode.GetAt(1) + L" " + m_sLblMM;
		}
	}

	CVecTicketSpc vecReturnVecTicketSpc;

	CChangeBarkReductionDlg *pDlg = new CChangeBarkReductionDlg();
	if (pDlg != NULL && m_pDB != NULL)
	{
		pDlg->setData(m_recTicket,m_vecTicketSpc,sMMode);
		if (pDlg->DoModal() == IDOK)
		{
			vecReturnVecTicketSpc = pDlg->getTicketSpc();
			if (vecReturnVecTicketSpc.size() > 0)
			{
				for (UINT i = 0;i < vecReturnVecTicketSpc.size();i++)
				{
					m_pDB->updTicketSpc(vecReturnVecTicketSpc[i]);
				}
			}
			populateLogsReport();
			updLogs();
		}

		delete pDlg;

	}
}

void CTicketLogsFormView::changeLogStatus()
{
	CXTPReportRow *pRow = NULL;
	CLogsReportRec *pRec = NULL;
	CLogsReportView *pLogReportView = (CLogsReportView*)getLogsView();
	if (pLogReportView != NULL && m_pDB != NULL)
	{
		if ((pRow = pLogReportView->GetReportCtrl().GetFocusedRow()) != NULL)
		{
			pRec = (CLogsReportRec*)pRow->GetRecord();
			CChangeStatusDlg *pDlg = new CChangeStatusDlg();
			if (pDlg != NULL && pRec != NULL)
			{
				// Check LogStatus
				if (pRec->getRecord().getStatusFlag() == LOGSTATUS::SOLD)
				{
					::MessageBox(GetSafeHwnd(),m_sMsgStatusSoldNotChange,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
					return;
				}
				if (pRec->getRecord().getStatusFlag() == LOGSTATUS::IN_MILL)
				{
					::MessageBox(GetSafeHwnd(),m_sMsgStatusSawmillNotChange,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
					return;
				}

				if (pDlg->DoModal() == IDOK)
				{
					CLogs rec = pRec->getRecord();
					rec.setStatusFlag(pDlg->getStatus());
					m_pDB->setLogStatus(rec);

					populateLogsReport();
					updLogs();

				}
				delete pDlg;
			}

		}
	}

}

// Set Species,pricelist and extra volumwe-functions
void CTicketLogsFormView::setTicketDataPricelist()
{
	// New ticket's created. We'll start to setup data for
	// Species and pricelist for this ticket.
	vecPricelistInTicket vec;
	CTicketSpc recTicketSpc = CTicketSpc();
	CGrades recGrades = CGrades();
	CSpecies recSpc = CSpecies();
	CFuncDesc recFunc = CFuncDesc();
	if (m_pDB != NULL)
	{


#ifdef _CHECK_SPECIES_ON_TICKET

		for (UINT i1 = 0;i1 < m_vecTicketSpc.size();i1++)
		{
			for (UINT i2 = 0;i2 < m_vecSpecies.size();i2++)
			{
				if (m_vecTicketSpc[i1].getSpeciesID() == m_vecSpecies[i2].getSpcID())
				{
					if (m_recTicket.getMeasuringMode() == 0)	// Inch
					{
						m_vecSpecies[i2].setBarkReductionInch(m_vecTicketSpc[i1].getBarkReduction());
					}
					else if (m_recTicket.getMeasuringMode() == 1)	// Metric
					{
						m_vecSpecies[i2].setBarkReductionMM(m_vecTicketSpc[i1].getBarkReduction());
					}
				}
			}
		}
		

#endif


		m_pDB->delTicketSpc(m_recTicket.getPKID());
		m_pDB->delTicketPrl(m_recTicket.getPKID());

		for (UINT i1 = 0;i1 < m_vecSpecies.size();i1++)
		{
			recSpc = m_vecSpecies[i1];
			if (getPricelistFromTemplate(m_selectedTemplate.getPricelist(),recSpc.getSpcID(),vec))
			{
				if (m_recTicket.getMeasuringMode() == 0)
				{
					recTicketSpc = CTicketSpc(m_recTicket.getPKID(),recSpc.getSpcID(),recSpc.getSpcCode(),recSpc.getSpcName(),recSpc.getBarkReductionInch());
				}
				else if (m_recTicket.getMeasuringMode() == 1)
				{
					recTicketSpc = CTicketSpc(m_recTicket.getPKID(),recSpc.getSpcID(),recSpc.getSpcCode(),recSpc.getSpcName(),recSpc.getBarkReductionMM());
				}
				m_pDB->newTicketSpc(recTicketSpc);
				//---------------------------------------------------------------------
				// We'll also add to TicketPrl
				if (vec.size() > 0)
				{
					for (UINT i2 = 0;i2 < vec.size();i2++)
					{
						recGrades = getGradesData(vec[i2].getGradesID());
						recFunc = getFuncData(vec[i2].getFuncID());
						m_pDB->newTicketPrl(CTicketPrl(m_recTicket.getPKID(),
																					 vec[i2].getSpeciesID(),
																					 vec[i2].getGradesID(),
																					 recSpc.getSpcCode(),
																					 recGrades.getGradesCode(),
																					 vec[i2].getPrice(),
																					 recFunc.getFuncID(),
																					 recFunc.getBasis(),
																					 recGrades.getIsPulpwood()) );
																					 
					}
				}
			}
		}
	}

}

void CTicketLogsFormView::setTicketDataExtraVFunc()
{
	//---------------------------------------------------------------------
	// We'll also add default extra volumefunctions to database-table
	if (m_vecDefUserVolFuncs.size() > 0)
	{
		for (UINT i3 = 0;i3 < m_vecDefUserVolFuncs.size();i3++)
		{
			m_pDB->addSelVolFuncs(CSelectedVolFuncs(-1,
																							m_recTicket.getPKID(),
																							m_vecDefUserVolFuncs[i3].getFuncID(),
																							m_vecDefUserVolFuncs[i3].getFullName(),
																							m_vecDefUserVolFuncs[i3].getAbbrevName(),
																							m_vecDefUserVolFuncs[i3].getBasisName()));
		}
	}
}

void CTicketLogsFormView::matchBarkReductionForSpeciesInTicket()
{
	if (m_vecLogs.size() > 0 && m_vecTicketSpc.size() > 0)
	{
		for (UINT i1 = 0;i1 < m_vecLogs.size();i1++)
		{
			for (UINT i2 = 0;i2 < m_vecTicketSpc.size();i2++)
			{
				if (m_vecLogs[i1].getBark() > 0.0)
				{
					if (m_recTicket.getMeasuringMode() == 0)	// Inch
					{
						if (m_vecLogs[i1].getSpcCode().CompareNoCase(m_vecTicketSpc[i2].getSpeciesCode()) == 0)
						{
							m_vecTicketSpc[i2].setBarkReduction(m_vecLogs[i1].getBark()/DOUBLE_BARK_FACTOR);
							m_pDB->updTicketSpc(m_vecTicketSpc[i2]);
						}
					}
					else if (m_recTicket.getMeasuringMode() == 1)	// MM
					{
						if (m_vecLogs[i1].getSpcCode().CompareNoCase(m_vecTicketSpc[i2].getSpeciesCode()) == 0)
						{
							m_vecTicketSpc[i2].setBarkReduction(m_vecLogs[i1].getBark());
							m_pDB->updTicketSpc(m_vecTicketSpc[i2]);
						}
					}
				}
			}			
		}
	}
}

short CTicketLogsFormView::newTicket(bool on_close)
{

	CLogsReportRec *pRec = NULL;
	int nCountNumberOfTrees = 0;	// based on Frequency
	CString sCBStr = L"",S;
	CLogScaleView *pLogScaleView = (CLogScaleView*)getFormViewByID(IDD_FORMVIEW);
	CLogsReportView *pLogsView = getLogsView();
	CXTPReportRows *pRows = NULL;
	CXTPReportRows *pFooterRows = NULL;
	if (m_pDB != NULL && pLogsView != NULL && pLogScaleView != NULL)
	{
		m_recTicket.setTicket(m_edit5_1.getText().Trim());
		m_recTicket.setLoadID(m_edit5_2.getText());
		m_recTicket.setWeight(m_edit5_3.getFloat());
		m_recTicket.setGISCoord1(m_edit5_7.getText());
		m_recTicket.setGISCoord2(m_edit5_8.getText());
		if (m_recTicket.getMeasuringMode() == 0)
			m_recTicket.setTrimFT(m_edit5_10.getFloat());
		else if (m_recTicket.getMeasuringMode() == 1)
			m_recTicket.setTrimCM(m_edit5_10.getFloat());
		//m_recTicket.setBarkRation(m_edit5_4.getFloat());	Anv�nds inte
		m_recTicket.setBarkRation(-1.0);
		m_recTicket.setIBTaper(m_edit5_5.getFloat());
		m_edit5_6.GetWindowTextW(sCBStr);
		m_recTicket.setOtherInfo(sCBStr);
		m_cbox5_1.GetWindowText(sCBStr);
		m_recTicket.setHauler(sCBStr);
		m_cbox5_2.GetWindowText(sCBStr);
		m_recTicket.setScaler(sCBStr);
		m_cbox5_3.GetWindowText(sCBStr);
		m_recTicket.setSourceID(sCBStr);
		m_cbox5_4.GetWindowText(sCBStr);
		m_recTicket.setLocation(sCBStr);
		m_cbox5_5.GetWindowText(sCBStr);
		m_recTicket.setBuyer(sCBStr);
		m_cbox5_6.GetWindowText(sCBStr);
		m_recTicket.setTractID(sCBStr);
		m_cbox5_7.GetWindowText(sCBStr);
		m_recTicket.setLoadType(sCBStr);
		m_cbox5_12.GetWindowText(sCBStr);
		m_recTicket.setVendor(sCBStr);
		m_cbox5_8.GetWindowText(sCBStr);
		m_recTicket.setDeductionType(sCBStr);		
		//m_dt5_1.GetWindowText(sCBStr);
		m_recTicket.setDate(m_csDate);
		if (m_cbox5_9.GetCurSel() == 0)
			m_recTicket.setMeasuringMode(0);
		else if (m_cbox5_9.GetCurSel() == 1)
			m_recTicket.setMeasuringMode(1);
		m_cbox5_10.GetWindowText(sCBStr);
		m_recTicket.setSupplier(sCBStr);		

		// Check if ticketnumber already been used
		if (pLogScaleView->isTicketAlreadyUsed(m_recTicket.getTicket()))
		{
			if (on_close)
			{
				if (::MessageBox(GetSafeHwnd(),m_sMsgTickenumberErr+L"\n\n"+m_sMsgCloseAnyway,m_sMsgCap,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
					return -1;
				else
					return 0;
			}
			else
			{
				::MessageBox(GetSafeHwnd(),m_sMsgTickenumberErr,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
				return 0;
			}
		}

		if (m_recTicket.getTicket().IsEmpty() ||
			  m_recTicket.getLoadType().IsEmpty() ||
			  m_recTicket.getDeductionType().IsEmpty() ||
				m_recTicket.getIBTaper() == 0.0 ||
				m_recTicket.getMeasuringMode() < 0)
		{
			if (on_close)
			{
				if (::MessageBox(GetSafeHwnd(),m_sMsgDataMissing+L"\n\n"+m_sMsgCloseAnyway,m_sMsgCap,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
					return -1;
				else
					return 0;
			}
			else
			{
				::MessageBox(GetSafeHwnd(),m_sMsgDataMissing,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
				return 0;
			}
		}

		//#3611, lagt in koll inga ogiltiga tecken i Ticket numret	\ / ? : * " > < |
		if(checkIllegalChars(m_edit5_1.getText().Trim()))
		{
			if (on_close)
			{
				if (::MessageBox(GetSafeHwnd(),_T("\\ / ? : * \" > < |")+m_sMsgIllegalChars+L"\n\n"+m_sMsgCloseAnyway,m_sMsgCap,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
					return -1;
				else
					return 0;
			}
			else
			{
				::MessageBox(GetSafeHwnd(),_T("\\ / ? : * \" > < |")+m_sMsgIllegalChars,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
				return 0;
			}
		}

		// If data entered ok, measuring mode can't be changed
		m_cbox5_9.EnableWindow(m_cbox5_9.GetCurSel() >= 0);

		// Get number of rows. We'll try to sum. number of logs
		// based on frequency (i.e. number equal logs)
		pRows = pLogsView->GetReportCtrl().GetRows();
		pFooterRows = pLogsView->GetReportCtrl().GetFooterRows();
		if (pRows != NULL)
		{
			for (int i = 0;i < pRows->GetCount();i++)
			{
				pRec = (CLogsReportRec *)pRows->GetAt(i)->GetRecord();
				if (pRec != NULL)
				{
					if (pRec->getColInt(COLUMNS::LOG_FREQUENCY) > 0)
						nCountNumberOfTrees += pRec->getColInt(COLUMNS::LOG_FREQUENCY);
					else
						nCountNumberOfTrees += 1;
				}	// if (pRec != NULL)
			}	// for (int i = 0;i < pRows->GetCount();i++)
			m_recTicket.setNumOfLogs(nCountNumberOfTrees);
		}	// if (pRows != NULL)
		// Add Totals to ticket
		if (pFooterRows != NULL)
		{
			if (pFooterRows->GetCount() == 2)
			{
				pRec = (CLogsReportRec *)pFooterRows->GetAt(1)->GetRecord();
				if (pRec != NULL)
				{
					m_recTicket.setSumVolPricelist(pRec->getColFloat(COLUMNS::LOG_VOL_PRICELIST));
					m_recTicket.setSumPrice(pRec->getColFloat(COLUMNS::LOG_PRICE));
				}
			}
		}	// if (pFooterRows != NULL)
		// Update ticket and ReportControl for tickets
		if (m_pDB->newTicket(m_recTicket))
		{
			setTicketDataPricelist();
			setTicketDataExtraVFunc();
			populateLogsReport();
			pLogScaleView->populateReport(false);
			if (!checkBarkReduction())
			{
				::MessageBox(GetSafeHwnd(),m_sMsgBarkReductionMissing,m_sMsgCap,MB_ICONASTERISK | MB_OK);
			}
		}	// if (m_pDB->newTicket(m_recTicket))
	}

	return 1;

}

short CTicketLogsFormView::updTicket(bool on_close)
{
	UpdateData(TRUE);

	CLogsReportRec *pRec = NULL;
	int nCountNumberOfTrees = 0;	// based on Frequency
	CString sCBStr = L"";
	CLogScaleView *pLogScaleView = (CLogScaleView*)getFormViewByID(IDD_FORMVIEW);
	CLogsReportView *pLogsView = getLogsView();
	CXTPReportRows *pRows = NULL;
	CXTPReportRows *pFooterRows = NULL;
	if (m_pDB != NULL && pLogsView != NULL && pLogScaleView != NULL)
	{
		m_recTicket.setLoadID(m_edit5_2.getText());
		m_recTicket.setWeight(m_edit5_3.getFloat());
		m_recTicket.setBarkRation(-1.0);
		m_recTicket.setIBTaper(m_edit5_5.getFloat());
		m_recTicket.setGISCoord1(m_edit5_7.getText());
		m_recTicket.setGISCoord2(m_edit5_8.getText());
		if (m_recTicket.getMeasuringMode() == 0)
			m_recTicket.setTrimFT(m_edit5_10.getFloat());
		else if (m_recTicket.getMeasuringMode() == 1)
			m_recTicket.setTrimCM(m_edit5_10.getFloat());
		m_edit5_6.GetWindowTextW(sCBStr);
		m_recTicket.setOtherInfo(sCBStr);
		m_cbox5_1.GetWindowText(sCBStr);
		m_recTicket.setHauler(sCBStr);
		m_cbox5_2.GetWindowText(sCBStr);
		m_recTicket.setScaler(sCBStr);
		m_cbox5_3.GetWindowText(sCBStr);
		m_recTicket.setSourceID(sCBStr);
		m_cbox5_4.GetWindowText(sCBStr);
		m_recTicket.setLocation(sCBStr);
		m_cbox5_5.GetWindowText(sCBStr);
		m_recTicket.setBuyer(sCBStr);
		m_cbox5_6.GetWindowText(sCBStr);
		m_recTicket.setTractID(sCBStr);
		m_cbox5_12.GetWindowText(sCBStr);
		m_recTicket.setVendor(sCBStr);
		m_cbox5_7.GetWindowText(sCBStr);
		m_recTicket.setLoadType(sCBStr);
		m_cbox5_8.GetWindowText(sCBStr);
		m_recTicket.setDeductionType(sCBStr);		
		//m_dt5_1.GetWindowText(sCBStr);
		m_recTicket.setDate(m_csDate);
		if (m_cbox5_9.GetCurSel() == 0)
			m_recTicket.setMeasuringMode(0);
		else if (m_cbox5_9.GetCurSel() == 1)
			m_recTicket.setMeasuringMode(1);
		m_cbox5_10.GetWindowText(sCBStr);
		m_recTicket.setSupplier(sCBStr);		
		m_recTicket.setLocked(m_check5_1.GetCheck());


		if (m_edit5_1.getText().IsEmpty() ||
			  m_recTicket.getLoadType().IsEmpty() ||
			  m_recTicket.getDeductionType().IsEmpty() ||
				m_recTicket.getBarkRation() == 0.0 ||
				m_recTicket.getIBTaper() == 0.0 ||
				!m_bIsTicketNumberOK)
		{
			if (on_close)
			{
				if (::MessageBox(GetSafeHwnd(),m_sMsgDataMissing+L"\n\n"+m_sMsgCloseAnyway,m_sMsgCap,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
					return -1;
				else
					return 0;
			}
			else
			{
				::MessageBox(GetSafeHwnd(),m_sMsgDataMissing,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
				return 0;
			}
		}
		else
		{
			//#3611, lagt in koll inga ogiltiga tecken i Ticket numret	\ / ? : * " > < |
			if(checkIllegalChars(m_edit5_1.getText().Trim()))
			{
				if (on_close)
				{
					if (::MessageBox(GetSafeHwnd(),_T("\\ / ? : * \" > < |")+m_sMsgIllegalChars+L"\n\n"+m_sMsgCloseAnyway,m_sMsgCap,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
						return -1;
					else
						return 0;
				}
				else
				{
					::MessageBox(GetSafeHwnd(),_T("\\ / ? : * \" > < |")+m_sMsgIllegalChars,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
					return 0;
				}
			}
			
			m_recTicket.setTicket(m_edit5_1.getText().Trim());
		}

		// Get number of rows. We'll try to sum. number of logs
		// based on frequency (i.e. number equal logs)
		pRows = pLogsView->GetReportCtrl().GetRows();
		pFooterRows = pLogsView->GetReportCtrl().GetFooterRows();
		if (pRows != NULL)
		{
			for (int i = 0;i < pRows->GetCount();i++)
			{
				pRec = (CLogsReportRec *)pRows->GetAt(i)->GetRecord();
				if (pRec != NULL)
				{
					if (pRec->getColInt(COLUMNS::LOG_FREQUENCY) > 0)
						nCountNumberOfTrees += pRec->getColInt(COLUMNS::LOG_FREQUENCY);
					else
						nCountNumberOfTrees += 1;
				}	// if (pRec != NULL)
			}	// for (int i = 0;i < pRows->GetCount();i++)
			m_recTicket.setNumOfLogs(nCountNumberOfTrees);
		}	// if (pRows != NULL)
		// Add Totals to ticket
		if (pFooterRows != NULL)
		{
			if (pFooterRows->GetCount() == 2)
			{
				pRec = (CLogsReportRec *)pFooterRows->GetAt(1)->GetRecord();
				if (pRec != NULL)
				{
					m_recTicket.setSumVolPricelist(pRec->getColFloat(COLUMNS::LOG_VOL_PRICELIST));				
					m_recTicket.setSumPrice(pRec->getColFloat(COLUMNS::LOG_PRICE));
				}
			}
		}	// if (pFooterRows != NULL)
		// Update ticket and ReportControl for tickets
		if (m_pDB->updTicket(m_recTicket))
		{
			pLogScaleView->populateReport(false);
		}	// if (m_pDB->updTicket(m_recTicket))
	}

	return 1;
}

CString CTicketLogsFormView::speciesName(LPCTSTR spc_code)
{
	if (m_vecTicketSpc.size() > 0)
	{
		for (UINT it = 0;it < m_vecTicketSpc.size();it++)
		{
			if (m_vecTicketSpc[it].getSpeciesCode().CompareNoCase(spc_code) == 0)
				return m_vecTicketSpc[it].getSpeciesName();
		}
	}
	return L"";
}

int CTicketLogsFormView::speciesID(LPCTSTR spc_code)
{
	if (m_vecTicketSpc.size() > 0)
	{
		for (UINT it = 0;it < m_vecTicketSpc.size();it++)
		{
			if (m_vecTicketSpc[it].getSpeciesCode().CompareNoCase(spc_code) == 0)
				return m_vecTicketSpc[it].getSpeciesID();
		}
	}
	return -1;
}

void CTicketLogsFormView::populateLogsReport(bool set_row,int log_id)
{
	CString S;
	CLogsReportView *pView = getLogsView();
	if (m_pDB != NULL && pView != NULL)
	{
		int nColCnt = 0,nRowCnt = 0;
		CXTPReportColumns *pCols = NULL;
		// Load logs for this ticket
		m_pDB->getLogs(m_recTicket,m_vecLogs);
		m_recLogsTotal.setLoadID(m_recTicket.getPKID());
		m_pDB->getSelVolFuncs(m_vecSelectedVolFuncs,m_recTicket.getPKID());
		m_pDB->getTicketSpc(m_vecTicketSpc,m_recTicket.getPKID());
		m_pDB->getTicketPrl(m_vecTicketPrl,m_recTicket.getPKID());

		m_pDB->avgLogData(m_recLogsTotal);

		setSpeciesIDOnLogs();
		// We'll show custom-fields for selected extra
		// volume-functions
		CString sCap = L"";
		pCols = pView->GetReportCtrl().GetColumns();
		if (pCols != NULL)
		{
			nColCnt = 0;
			for (int i = 0;i < pCols->GetCount();i++)
			{
				if (pCols->GetAt(i)->GetItemIndex() >= COLUMNS::LOG_VOL_CUST_1 && pCols->GetAt(i)->GetItemIndex() <= COLUMNS::LOG_VOL_CUST_20)
				{
					pCols->GetAt(pCols->GetAt(i)->GetIndex())->SetVisible(nColCnt < m_vecSelectedVolFuncs.size());
					if (nColCnt < m_vecSelectedVolFuncs.size())
					{
						sCap.Format(L"%s\n%s",m_vecSelectedVolFuncs[nColCnt].getFullName(),m_vecSelectedVolFuncs[nColCnt].getBasisName());
						pCols->GetAt(pCols->GetAt(i)->GetIndex())->SetCaption(sCap);
						m_sarrHeadLines.ElementAt(pCols->GetAt(i)->GetItemIndex()) = m_vecSelectedVolFuncs[nColCnt].getBasisName();
					}
					nColCnt++;
				}
				
			}
		}

		CString sSpcName = L"";
		CXTPReportRow *pRow = pView->GetReportCtrl().GetFocusedRow();
		CXTPReportRow *pRowSet = NULL;
		CLogsReportRec *pRec = NULL;
		if (pRow != NULL)
		{
			pRec = (CLogsReportRec *)pRow->GetRecord();			
		}

		pView->GetReportCtrl().BeginUpdate();
		pView->GetReportCtrl().ResetContent();
		pView->GetReportCtrl().GetFooterRecords()->RemoveAll();
		if (m_vecLogs.size() > 0 && pView != NULL)
		{

			for (UINT i = 0;i < m_vecLogs.size();i++)
			{
				addConstraintSpecies();
				addConstraintGrades();
				sSpcName = speciesName(m_vecLogs[i].getSpcCode().Trim());
				m_vecLogs[i].setSpcName(sSpcName);
				m_recLogsTotal.setVolFuncID(m_vecLogs[i].getVolFuncID());
				pView->GetReportCtrl().AddRecord(new CLogsReportRec(m_vecLogs[i],m_sarrLogStatus,m_recTicket.getMeasuringMode(),m_vecSelectedVolFuncs));
				if (m_vecLogs[i].getLogID() == log_id)
				{
					nRowCnt = i;
				}

			}
			
			pView->GetReportCtrl().GetFooterRecords()->Add(new CLogsReportRec(CLogs(),m_sarrLogStatus,-1, m_vecSelectedVolFuncs,m_sarrHeadLines,true,1));
			pView->GetReportCtrl().GetFooterRecords()->Add(new CLogsReportRec(m_recLogsTotal,m_sarrLogStatus,m_recTicket.getMeasuringMode(),m_vecSelectedVolFuncs,m_sarrHeadLines,true,2));
			pView->GetReportCtrl().ShowFooterRows(TRUE);

			pView->GetReportCtrl().Populate();
			pView->GetReportCtrl().UpdateWindow();

			if (pRec != NULL && pRow != NULL)
			{
				//if (!pRec->getIsFooter())
				if (pRow->GetIndex() > -1 )
				{
					// Focus on selected row
					pRow->SetSelected(TRUE);
					pView->GetReportCtrl().SetFocusedRow(pRow);
				}
			}
			
		}
		else
		{
			pView->GetReportCtrl().GetFooterRecords()->Add(new CLogsReportRec(CLogs(),m_sarrLogStatus,-1,m_vecSelectedVolFuncs,m_sarrHeadLines,true,1));
			pView->GetReportCtrl().GetFooterRecords()->Add(new CLogsReportRec(m_recLogsTotal,m_sarrLogStatus,m_recTicket.getMeasuringMode(),m_vecSelectedVolFuncs,m_sarrHeadLines,true,2));
			pView->GetReportCtrl().ShowFooterRows(TRUE);

			pView->GetReportCtrl().Populate();
			pView->GetReportCtrl().UpdateWindow();

		}
		pView->GetReportCtrl().EndUpdate();
		if (set_row)
		{				
			if ((pRowSet = pView->GetReportCtrl().GetRows()->GetAt(nRowCnt)) != NULL)
			{
				if (pRowSet->GetIndex() > -1 )
				{
					// Focus on set row
					pView->GetReportCtrl().SetFocusedRow(pRowSet);
					pRowSet->SetSelected(TRUE);
					pView->GetReportCtrl().UpdateWindow();
				}
			}	// if ((pRowSet = pView->GetReportCtrl().GetRows()->GetAt(nRowCnt)) != NULL)
		}	// if (set_row)
	}
}

void CTicketLogsFormView::populateData(BOOL reload_data,BOOL save_logs)
{
	CString sStr = L"",S;
	if (reload_data && m_pDB != NULL)
	{
		m_pDB->getRegister(m_vecRegister);
	}

	m_edit5_1.SetWindowTextW(m_recTicket.getTicket().Trim());
	m_edit5_2.SetWindowTextW(m_recTicket.getLoadID().Trim());
	sStr.Format(L"%.0f",m_recTicket.getWeight());
	m_edit5_3.SetWindowTextW(sStr);
	m_lbl5_21.SetWindowTextW(m_recTicket.getTmplUsed());
	m_lbl5_25.SetWindowTextW(m_recTicket.getPrlUsed());
	
	m_check5_1.SetCheck(m_recTicket.getLocked() == 1);

	CLogsReportView *pLogsView = getLogsView();
	if (m_recTicket.getLocked() == 1)
	{
		if (pLogsView != NULL)
		{
			setEnable(FALSE);
			pLogsView->EnableWindow(FALSE);
		}
	}
	else if (m_recTicket.getLocked() == 0)
	{
		if (pLogsView != NULL)
		{
			setEnable(TRUE);
			pLogsView->EnableWindow(TRUE);
		}
	}
	
	// If new item. I.e. m_recTicket.getLoadID() == -1, use default values
/*
	if (m_recTicket.getPKID() == -1)
	{
		sStr = localeToStr(getDefaults(DEF_BARK_RATIO,m_vecDefaults,m_vecCalcTypes));
	}
	else
	{
		sStr.Format(L"%.1f",m_recTicket.getBarkRation());
	}
	m_edit5_4.SetWindowTextW(sStr);
*/

	if (m_recTicket.getIBTaper() <= 0.0)
	{
		sStr = localeToStr(getDefaults(DEF_TAPER_IB,m_vecDefaults,m_vecCalcTypes));
	}
	else
	{
		sStr.Format(L"%.3f",m_recTicket.getIBTaper());
	}
	m_edit5_5.SetWindowTextW(sStr);

	m_edit5_6.SetWindowTextW(m_recTicket.getOtherInfo());

	m_edit5_7.SetWindowTextW(m_recTicket.getGISCoord1());
	m_edit5_8.SetWindowTextW(m_recTicket.getGISCoord2());

	m_csDate = m_recTicket.getDate();

	// Add data to comboboxes
	// Hauler
	m_cbox5_1.ResetContent();
	m_cbox5_1.AddString(L"");
	for (UINT i1 = 0;i1 < m_vecRegister.size();i1++)
	{
		if (m_vecRegister[i1].getTypeID() == REGISTER_TYPES::HAULER)
			m_cbox5_1.AddString(m_vecRegister[i1].getName());
	}
	// Scaler
	m_cbox5_2.ResetContent();
	m_cbox5_2.AddString(L"");
	for (UINT i2 = 0;i2 < m_vecRegister.size();i2++)
	{
		if (m_vecRegister[i2].getTypeID() == REGISTER_TYPES::SCALER)
			m_cbox5_2.AddString(m_vecRegister[i2].getName());
	}

	// Supplier
	m_cbox5_10.ResetContent();
	m_cbox5_10.AddString(L"");
	for (UINT i2 = 0;i2 < m_vecRegister.size();i2++)
	{
		if (m_vecRegister[i2].getTypeID() == REGISTER_TYPES::SUPPLIER)
			m_cbox5_10.AddString(m_vecRegister[i2].getName());
	}

	// Vendor
	m_cbox5_12.ResetContent();
	m_cbox5_12.AddString(L"");
	for (UINT i2 = 0;i2 < m_vecRegister.size();i2++)
	{
		if (m_vecRegister[i2].getTypeID() == REGISTER_TYPES::VENDOR)
			m_cbox5_12.AddString(m_vecRegister[i2].getName());
	}

	// Source ID
	m_cbox5_3.ResetContent();
	m_cbox5_3.AddString(L"");
	for (UINT i2 = 0;i2 < m_vecRegister.size();i2++)
	{
		if (m_vecRegister[i2].getTypeID() == REGISTER_TYPES::SOURCEID)
			m_cbox5_3.AddString(m_vecRegister[i2].getName());
	}

	// Location
	m_cbox5_4.ResetContent();
	m_cbox5_4.AddString(L"");
	for (UINT i2 = 0;i2 < m_vecRegister.size();i2++)
	{
		if (m_vecRegister[i2].getTypeID() == REGISTER_TYPES::LOCATION)
			m_cbox5_4.AddString(m_vecRegister[i2].getName());
	}

	// Buyer
	m_cbox5_5.ResetContent();
	m_cbox5_5.AddString(L"");
	for (UINT i2 = 0;i2 < m_vecRegister.size();i2++)
	{
		if (m_vecRegister[i2].getTypeID() == REGISTER_TYPES::BUYER)
			m_cbox5_5.AddString(m_vecRegister[i2].getName());
	}

	// TractID
	m_cbox5_6.ResetContent();
	m_cbox5_6.AddString(L"");
	for (UINT i2 = 0;i2 < m_vecRegister.size();i2++)
	{
		if (m_vecRegister[i2].getTypeID() == REGISTER_TYPES::TRACTID)
			m_cbox5_6.AddString(m_vecRegister[i2].getName());
	}

	// Load type
	m_cbox5_7.ResetContent();
	for (int i = 0;i < m_sarrLoadType.GetCount();i++)
	{
		m_cbox5_7.AddString(m_sarrLoadType.GetAt(i));
	}

	// Deduction type
	m_cbox5_8.ResetContent();
	for (int i = 0;i < m_sarrDeductionType.GetCount();i++)
	{
		m_cbox5_8.AddString(m_sarrDeductionType.GetAt(i));
	}

	m_cbox5_1.SetCurSel(m_cbox5_1.FindStringExact(-1,m_recTicket.getHauler().Trim()));
	m_cbox5_2.SetCurSel(m_cbox5_2.FindStringExact(-1,m_recTicket.getScaler().Trim()));
	m_cbox5_3.SetCurSel(m_cbox5_3.FindStringExact(-1,m_recTicket.getSourceID().Trim()));
	m_cbox5_4.SetCurSel(m_cbox5_4.FindStringExact(-1,m_recTicket.getLocation().Trim()));
	m_cbox5_5.SetCurSel(m_cbox5_5.FindStringExact(-1,m_recTicket.getBuyer().Trim()));
	m_cbox5_6.SetCurSel(m_cbox5_6.FindStringExact(-1,m_recTicket.getTractID().Trim()));
	m_cbox5_10.SetCurSel(m_cbox5_10.FindStringExact(-1,m_recTicket.getSupplier().Trim()));
	m_cbox5_12.SetCurSel(m_cbox5_12.FindStringExact(-1,m_recTicket.getVendor().Trim()));
	// S�tt defaulta v�rden
	if (m_recTicket.getPKID() == -1)
	{
		m_cbox5_7.SetCurSel(m_cbox5_7.FindStringExact(-1,getDefaults(DEF_LOAD_TYPE,m_vecDefaults,m_vecCalcTypes).Trim()));
		m_cbox5_8.SetCurSel(m_cbox5_8.FindStringExact(-1,getDefaults(DEF_DEDUCTION,m_vecDefaults,m_vecCalcTypes).Trim()));
	}
	else
	{
		m_cbox5_7.SetCurSel(m_cbox5_7.FindStringExact(-1,m_recTicket.getLoadType().Trim()));
		m_cbox5_8.SetCurSel(m_cbox5_8.FindStringExact(-1,m_recTicket.getDeductionType().Trim()));
	}

	if (m_recTicket.getMeasuringMode() == 0)
	{
		m_cbox5_9.SetCurSel(0);
		m_cbox5_9.EnableWindow(false);
		m_edit5_10.setFloat(m_recTicket.getTrimFT(),1);
		m_lbl5_23.SetWindowTextW(m_sTrimFT);
	}
	else if (m_recTicket.getMeasuringMode() == 1)
	{
		m_cbox5_9.SetCurSel(1);
		m_cbox5_9.EnableWindow(false);
		m_edit5_10.setFloat(m_recTicket.getTrimCM(),0);
		m_lbl5_23.SetWindowTextW(m_sTrimCM);
	}

	if (save_logs)
	{
		CLogsReportView *pView = getLogsView();
		if (pView != NULL)
		{
			pView->SaveLog(TRUE);
		}
	}

	UpdateData(FALSE);
}

void CTicketLogsFormView::addConstraintSpecies()
{
	CLogsReportView *pView = getLogsView();
	if (pView != NULL)
	{
		CXTPReportRecordItemConstraints *pCons = NULL;
		CXTPReportColumns *pColumns = pView->GetReportCtrl().GetColumns();
		CXTPReportRows *pRows = pView->GetReportCtrl().GetRows();
		CXTPReportColumn *pCol = NULL;
		if (pColumns != NULL)
		{
			pCol = pColumns->Find(COLUMNS::LOG_SPC_CODE);
			if (pCol != NULL)
			{
					// Get constraints for Specie column and remove all items; 070509 p�d
				pCons = pCol->GetEditOptions()->GetConstraints();
				if (pCons != NULL)
				{
					pCons->RemoveAll();
				}	// if (pCons != NULL)
				if (m_vecTicketSpc.size() > 0 && pCol != NULL)
				{
					for (UINT i = 0;i < m_vecTicketSpc.size();i++)
					{
						pCol->GetEditOptions()->AddConstraint(m_vecTicketSpc[i].getSpeciesCode());
					}
				}
			} // if (pCol != NULL)
		}	// if (pColumns != NULL)
	}
}

CString CTicketLogsFormView::getLogStatus(int status_id)
{
	if (status_id >= 0 && status_id < m_sarrLogStatus.GetCount())
		return m_sarrLogStatus[status_id];
	
	return L"";
}


void CTicketLogsFormView::addConstraintGrades()
{
	CString sSpcCode = L"";
	CLogsReportView *pView = getLogsView();
	if (pView != NULL)
	{
		CXTPReportRecordItemConstraints *pCons = NULL;
		CXTPReportColumns *pColumns = pView->GetReportCtrl().GetColumns();
		CXTPReportRows *pRows = pView->GetReportCtrl().GetRows();
		CXTPReportColumn *pCol = NULL;
		if (pColumns != NULL)
		{
			pCol = pColumns->Find(COLUMNS::LOG_GRADE);
			if (pCol != NULL)
			{
					// Get constraints for Specie column and remove all items; 070509 p�d
				pCons = pCol->GetEditOptions()->GetConstraints();
				if (pCons != NULL)
				{
					pCons->RemoveAll();
				}	// if (pCons != NULL)
				if (m_vecTicketPrl.size() > 0 && pCol != NULL)
				{
					for (UINT i = 0;i < m_vecTicketPrl.size();i++)
					{
						if (m_selectedTemplate.getDefSpecies() == m_vecTicketPrl[i].getSpeciesID())
							pCol->GetEditOptions()->AddConstraint(m_vecTicketPrl[i].getGradesCode());	
					}
				}
			} // if (pCol != NULL)
		}	// if (pColumns != NULL)
	}
}


// CTicketLogsFormView diagnostics

#ifdef _DEBUG
void CTicketLogsFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CTicketLogsFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG


CLogsReportView *CTicketLogsFormView::getLogsView(void)
{
	m_tabManager = m_wndTabControl.getTabPage(0);
	if (m_tabManager)
	{
		CLogsReportView* pView = DYNAMIC_DOWNCAST(CLogsReportView, CWnd::FromHandle(m_tabManager->GetHandle()));
		ASSERT_KINDOF(CLogsReportView, pView);
		return pView;
	}
	return NULL;
}

void CTicketLogsFormView::OnExportTicket(void)
{
	AfxMessageBox(L"void CTicketLogsFormView::OnExportTicket(void)");
}


BOOL CTicketLogsFormView::AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon)
{
	CCreateContext contextT;
	contextT.m_pCurrentDoc     = GetDocument();
	contextT.m_pNewViewClass   = pViewClass;
	contextT.m_pNewDocTemplate = GetDocument()->GetDocTemplate();

	CWnd* pWnd;
	TRY
	{
		pWnd = (CWnd*)pViewClass->CreateObject();
		if (pWnd == NULL)
		{
			AfxThrowMemoryException();
		}
	}
	CATCH_ALL(e)
	{
		TRACE0( "Out of memory creating a view.\n" );
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;

	int nTab = m_wndTabControl.GetItemCount();

	// Create with the right size (wrong position)
	CRect rect(0,0,0,0);
	if (!pWnd->Create(NULL, NULL, dwStyle,
		rect, &m_wndTabControl, (AFX_IDW_PANE_FIRST + nTab), &contextT))
	{
		TRACE0( "Warning: couldn't create client tab for view.\n" );
		// pWnd will be cleaned up by PostNcDestroy
		return NULL;
	}
	m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);

	//pWnd->SendMessage(WM_INITIALUPDATE);
	pWnd->SetOwner(this);

	return TRUE;
}

void CTicketLogsFormView::OnEnChangeEdit51()
{
	if (m_bInitialized)
	{
		CString num = m_edit5_1.getText();
		if (checkTicketExists(num))
			::MessageBox(GetSafeHwnd(),m_sMsgTickenumberErr,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);

		//#3611, lagt in koll inga ogiltiga tecken i Ticket numret	\ / ? : * " > < |
		if(checkIllegalChars(num))
		{
			::MessageBox(GetSafeHwnd(),_T("\\ / ? : * \" > < |")+m_sMsgIllegalChars,m_sMsgCap,MB_ICONSTOP | MB_OK);
		}
	}
}

void CTicketLogsFormView::OnBnClickedButton51()
{
	changeContract();
}

void CTicketLogsFormView::OnBnClickedButton52()
{
	changePricelist();
}

void CTicketLogsFormView::OnBnClickedCheck51()
{
	CLogsReportView *pLogsView = getLogsView();
	if (m_check5_1.GetCheck() == 1)
	{
		if (pLogsView != NULL)
		{
			setEnable(FALSE);
			pLogsView->EnableWindow(FALSE);
			if (m_pDB != NULL)
			{
				m_pDB->setLockOnTicket(m_recTicket.getPKID(),1);
			}
			//m_btnLock5_1.SetXButtonStyle(BS_XT_FLAT);
		}
	}
	else if (m_check5_1.GetCheck() == 0)
	{
		if (pLogsView != NULL)
		{
			setEnable(TRUE);
			pLogsView->EnableWindow(TRUE);
			if (m_pDB != NULL)
			{
				m_pDB->setLockOnTicket(m_recTicket.getPKID(),0);
			}
			//m_btnLock5_1.SetXButtonStyle(BS_XT_WINXP_COMPAT);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////
// CLogsReportView

IMPLEMENT_DYNCREATE(CLogsReportView,  CXTPReportView) //CXTResizeFormView)

BEGIN_MESSAGE_MAP(CLogsReportView,  CXTPReportView) //CXTResizeFormView)
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_NOTIFY(NM_CLICK, XTP_ID_REPORT_CONTROL, OnReportItemClick)
	ON_NOTIFY(XTP_NM_REPORT_HEADER_RCLICK, XTP_ID_REPORT_CONTROL, OnReportColumnRClick)
	ON_NOTIFY(XTP_NM_REPORT_VALUECHANGED, XTP_ID_REPORT_CONTROL, OnReportValueChanged)
	ON_COMMAND(ID_TBBTN_SELECTION_DLG3, OnFieldSelection)
END_MESSAGE_MAP()

CLogsReportView::CLogsReportView()
	: CXTPReportView(),
	  m_pDB(NULL),
		m_sLangFN(L""),
		m_bInitialized(FALSE),
		m_nColumnChanged(-1)
{
}

CLogsReportView::~CLogsReportView()
{
}

void CLogsReportView::OnInitialUpdate()
{
	CXTPReportView::OnInitialUpdate();

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	// Setup language filename; 051214 p�d
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (!m_bInitialized)
	{

		setupReport();

		LoadReportState(GetReportCtrl(),ID_REPORT_LOGS);

		m_bInitialized = TRUE;
	}
}

BOOL CLogsReportView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		if (m_dbConnectionData.conn->isConnected())
		{
			m_pDB = new CDBHandling(m_dbConnectionData);
		}
	}
	return CXTPReportView::OnCopyData(pWnd, pData);
}

void CLogsReportView::OnDestroy()
{
	
	CString sRegKey = L"";
	CTicketLogsFormView *pView = (CTicketLogsFormView*)getFormViewByID(IDD_FORMVIEW5);
	if (pView != NULL)
	{
		sRegKey.Format(L"%s_%d",pView->getTicket().getTicket(),pView->getTicket().getPKID());
		if(checkIllegalChars(pView->getTicket().getTicket()) == false)	//#3611, lagt in koll inga ogiltiga tecken i Ticket numret	\ / ? : * " > < |
			saveColumnsSettingToFile(sRegKey,GetReportCtrl(),COLSET_TICKET);
	}

	SaveReportState(GetReportCtrl(),ID_REPORT_LOGS);

	CXTPReportView::OnDestroy();	
}

BOOL CLogsReportView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
//	if( !CXTResizeFormView::PreCreateWindow(cs) )
//		return FALSE;
	if( ! CXTPReportView::PreCreateWindow(cs) )
		return FALSE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CLogsReportView diagnostics

#ifdef _DEBUG
void CLogsReportView::AssertValid() const
{
	CXTPReportView::AssertValid();
}

void CLogsReportView::Dump(CDumpContext& dc) const
{
	CXTPReportView::Dump(dc);
//	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CLogsReportView message handlers

// Create and add Assortment settings reportwindow
BOOL CLogsReportView::setupReport(void)
{
	CString sDia = L"",sLen = L"";
	CXTPReportColumn *pCol = NULL;
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{

			tokenizeString(xml.str(IDS_STRING202),';',m_sarrDeductionType);
			tokenizeString(xml.str(IDS_STRING203),';',m_sarrLogStatus);
			// String-resource
			m_sFieldSelection = xml.str(IDS_STRING1100);

			m_sMsgCap = xml.str(IDS_STRING99);
			m_sTagNummer = xml.str(IDS_STRING1700);
			m_sMsgRemoveLogs1 = xml.str(IDS_STRING1763) + L"\n" + xml.str(IDS_STRING17630);
			m_sMsgRemoveLogs2 = xml.str(IDS_STRING1764);
			
			m_sMsgLogsContinue1 = xml.str(IDS_STRING1763);
			m_sMsgLogsContinue2 = xml.str(IDS_STRING17640);

			m_sMsgErrorFrequens = xml.str(IDS_STRING1783);
			m_sMsgErrorReduction = xml.str(IDS_STRING1784);

			int nMode = regGetInt(REG_ROOT,TICKET_MEASURINGMODE_KEY,L"MODE",-1);
			if (nMode == 0)
			{
				sDia = L" " + xml.str(IDS_STRING109);
				sLen = L" " + xml.str(IDS_STRING111);
			}
			else if (nMode == 1)
			{
				sDia = L" " + xml.str(IDS_STRING110);
				sLen = L" " + xml.str(IDS_STRING122);
			}


			if (GetReportCtrl().GetSafeHwnd() != NULL)
			{
				GetReportCtrl().GetReportHeader()->SetAutoColumnSizing( FALSE );
				GetReportCtrl().EnableScrollBar(SB_HORZ, TRUE );
				GetReportCtrl().EnableScrollBar(SB_VERT, TRUE );

				GetReportCtrl().ShowWindow( SW_NORMAL );
				GetReportCtrl().ShowGroupBy( FALSE /*TRUE*/ );
				// TagNumber
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_TAG_NUMBER, xml.str(IDS_STRING1700), 80));
				pCol->AllowRemove(FALSE);
				pCol->SetGroupable(FALSE);
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->SetAlignment(DT_CENTER);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_nMaxLength = 50;
				// Speciescode
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_SPC_CODE, xml.str(IDS_STRING1701), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->GetEditOptions()->AddComboButton();
				// Speciesname
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_SPC_NAME, xml.str(IDS_STRING1724), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Status
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_STATUS_FLAG, xml.str(IDS_STRING1762), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Frequency
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_FREQUENCY, xml.str(IDS_STRING1702), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->SetAlignment(DT_CENTER);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->SetShowInFieldChooser(FALSE);
				pCol->SetVisible(FALSE);
				// Grade
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_GRADE, xml.str(IDS_STRING1703), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				pCol->GetEditOptions()->AddComboButton();
				// Deduction
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_DEDUCTION, xml.str(IDS_STRING1704), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				// Top-diameter 1 on bark (DOB)
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_DOB_TOP, xml.str(IDS_STRING1705)+sDia, 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Top-diameter 2 on bark (DOB)
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_DOB_TOP2, xml.str(IDS_STRING17050)+sDia, 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Top-diameter 1 inside bark (DIB)
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_DIB_TOP, xml.str(IDS_STRING1706)+sDia, 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				// Top-diameter 2 inside bark (DIB)
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_DIB_TOP2, xml.str(IDS_STRING17060)+sDia, 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				// Root-diameter 1 on bark (DOB)
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_DOB_ROOT, xml.str(IDS_STRING1707)+sDia, 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Root-diameter 2 on bark (DOB)
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_DOB_ROOT2, xml.str(IDS_STRING17070)+sDia, 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Root-diameter 1 inside bark (DIB)
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_DIB_ROOT, xml.str(IDS_STRING1708)+sDia, 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				// Root-diameter 2 inside bark (DIB)
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_DIB_ROOT2, xml.str(IDS_STRING17080)+sDia, 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				// Length measured
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_LENGTH_MEAS, xml.str(IDS_STRING17090)+sLen, 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				// Length calculations
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_LENGTH_CALC, xml.str(IDS_STRING17091)+sLen, 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = FALSE;
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Bark
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_BARK, xml.str(IDS_STRING1710), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Volume Pricelist
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_VOL_PRICELIST, xml.str(IDS_STRING1711), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Price
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_PRICE, xml.str(IDS_STRING1716), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Function name
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_VOL_FUNC_NAME, xml.str(IDS_STRING17160), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_CENTER);
				pCol->SetAlignment(DT_CENTER);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;

				// Customfield 1
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_VOL_CUST_1, xml.str(IDS_STRING1712), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->SetShowInFieldChooser(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Customfield 2
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_VOL_CUST_2, xml.str(IDS_STRING1712), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->SetShowInFieldChooser(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Customfield 3
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_VOL_CUST_3, xml.str(IDS_STRING1712), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->SetShowInFieldChooser(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Customfield 4
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_VOL_CUST_4, xml.str(IDS_STRING1712), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->SetShowInFieldChooser(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Customfield 5
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_VOL_CUST_5, xml.str(IDS_STRING1712), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->SetShowInFieldChooser(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Customfield 6
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_VOL_CUST_6, xml.str(IDS_STRING1712), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->SetShowInFieldChooser(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Customfield 7
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_VOL_CUST_7, xml.str(IDS_STRING1712), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->SetShowInFieldChooser(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Customfield 8
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_VOL_CUST_8, xml.str(IDS_STRING1712), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->SetShowInFieldChooser(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Customfield 9
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_VOL_CUST_9, xml.str(IDS_STRING1712), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->SetShowInFieldChooser(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Customfield 10
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_VOL_CUST_10, xml.str(IDS_STRING1712), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->SetShowInFieldChooser(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Customfield 11
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_VOL_CUST_11, xml.str(IDS_STRING1712), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->SetShowInFieldChooser(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Customfield 12
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_VOL_CUST_12, xml.str(IDS_STRING1712), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->SetShowInFieldChooser(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Customfield 13
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_VOL_CUST_13, xml.str(IDS_STRING1712), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->SetShowInFieldChooser(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Customfield 14
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_VOL_CUST_14, xml.str(IDS_STRING1712), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->SetShowInFieldChooser(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Customfield 15
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_VOL_CUST_15, xml.str(IDS_STRING1712), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->SetShowInFieldChooser(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Customfield 16
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_VOL_CUST_16, xml.str(IDS_STRING1712), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->SetShowInFieldChooser(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Customfield 17
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_VOL_CUST_17, xml.str(IDS_STRING1712), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->SetShowInFieldChooser(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Customfield 18
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_VOL_CUST_18, xml.str(IDS_STRING1712), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->SetShowInFieldChooser(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Customfield 19
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_VOL_CUST_19, xml.str(IDS_STRING1712), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->SetShowInFieldChooser(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Customfield 20
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_VOL_CUST_20, xml.str(IDS_STRING1712), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->SetShowInFieldChooser(FALSE);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// User root-diamter 1
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_U_LARGE_DIAM, xml.str(IDS_STRING1719)+sDia, 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				// User root-diamter 2
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_U_LARGE_DIAM2, xml.str(IDS_STRING17190)+sDia, 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				// User volume
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_U_VOL, xml.str(IDS_STRING1717), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				// User top-diamter 1
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_U_SMALL_DIAM, xml.str(IDS_STRING1718)+sDia, 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				// User top-diamter 2
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_U_SMALL_DIAM2, xml.str(IDS_STRING17180)+sDia, 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				// User length
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_U_LENGTH, xml.str(IDS_STRING1720)+sLen, 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				// User price
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_U_PRICE, xml.str(IDS_STRING1721), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				// Reason
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_REASON, xml.str(IDS_STRING1722), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_nMaxLength = 15;
				// Notes
				pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMNS::LOG_NOTES, xml.str(IDS_STRING1723), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
				pCol->GetEditOptions()->m_bAllowEdit = TRUE;
				pCol->GetEditOptions()->m_nMaxLength = 15;

				GetReportCtrl().GetReportHeader()->AllowColumnRemove(TRUE);
				GetReportCtrl().SetMultipleSelection( FALSE );
				GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );
				GetReportCtrl().SetGridStyle( FALSE, xtpReportGridSmallDots );
				GetReportCtrl().FocusSubItems(TRUE);
				GetReportCtrl().AllowEdit(TRUE);
				GetReportCtrl().GetPaintManager()->SetFixedRowHeight(FALSE);
				GetReportCtrl().PinFooterRows(FALSE);

				xml.clean();

			}	// if (GetReportCtrl().GetSafeHwnd() != NULL)
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

	return TRUE;
}

void CLogsReportView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	//XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
}

void CLogsReportView::OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	ASSERT(pItemNotify->pColumn);
	CPoint ptClick = pItemNotify->pt;

	CMenu menu;
	VERIFY(menu.CreatePopupMenu());

	// create main menu items
	menu.AppendMenu(MF_STRING, ID_SHOW_FIELD_SELECTION3, m_sFieldSelection);

	// track menu
	int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN |TPM_RIGHTBUTTON, ptClick.x, ptClick.y, this, NULL);

	// other general items
	switch (nMenuResult)
	{
		case ID_SHOW_FIELD_SELECTION3:
			OnFieldSelection();	
		break;
	}
	menu.DestroyMenu();
}

void CLogsReportView::OnFieldSelection(void)
{
	CTicketLogsFrame* pWnd = (CTicketLogsFrame *)getFormViewParentByID(IDD_FORMVIEW5);
	if (pWnd != NULL)
	{
		BOOL bShow = !pWnd->m_wndFieldSelectionDlg.IsVisible();
		pWnd->ShowControlBar(&pWnd->m_wndFieldSelectionDlg, bShow, FALSE);
	}
}

void CLogsReportView::OnReportValueChanged(NMHDR*  pNotifyStruct, LRESULT* /*result*/)
{
	BOOL bSave = TRUE;
	CString sSpcName = L"";
	CTicketLogsFormView *pView = (CTicketLogsFormView*)getFormViewByID(IDD_FORMVIEW5);
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	if (pItemNotify != NULL && pView != NULL)
	{	
		CXTPReportRow *pRow =	pItemNotify->pRow;
		CXTPReportColumn *pCol =	pItemNotify->pColumn;
		if (pRow != NULL && pCol != NULL)
		{
			m_nColumnChanged = pCol->GetIndex();
			CLogsReportRec *pRec = (CLogsReportRec*)pItemNotify->pRow->GetRecord();
			if (pRec != NULL)
			{
//				if (!pRec->getIsFooter()) 
				if (pRow->GetIndex() > -1) 
				{
					if (m_nColumnChanged == COLUMNS::LOG_SPC_CODE)
					{
						sSpcName = pView->speciesName(pRec->getColText(COLUMNS::LOG_SPC_CODE).Trim());
						pRec->setColText(COLUMNS::LOG_SPC_NAME,sSpcName);
					}
					//-------------------------------------------------------------------------------------------
					// Check ticketnumber. No dupltes allowed
					if (m_nColumnChanged == COLUMNS::LOG_TAG_NUMBER)
					{
						CTicketLogsFormView *pTicketLogsView = (CTicketLogsFormView*)getFormViewByID(IDD_FORMVIEW5);
						if (pTicketLogsView != NULL)
						{
							if ((bSave = pTicketLogsView->checkLogTagNumber(pRow->GetIndex(), pRec->getColText(COLUMNS::LOG_TAG_NUMBER)))== FALSE)
							{
								pItemNotify->pItem->SetTextColor(RED);
								pRec->setColText(COLUMNS::LOG_TAG_NUMBER,L"");
							}
							else
								pItemNotify->pItem->SetTextColor(BLACK);
						}
					}	// if (m_nColumnChanged == COLUMNS::LOG_TAG_NUMBER)

					//-------------------------------------------------------------------------------------------
					// Check frequency
					if (m_nColumnChanged == COLUMNS::LOG_FREQUENCY)
					{
						CTicketLogsFormView *pTicketLogsView = (CTicketLogsFormView*)getFormViewByID(IDD_FORMVIEW5);
						if (pTicketLogsView != NULL)
						{
							if (pRec->getColInt(COLUMNS::LOG_FREQUENCY) < 1)
							{
								::MessageBox(GetSafeHwnd(),m_sMsgErrorFrequens,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
								pRec->setColInt(COLUMNS::LOG_FREQUENCY,1);
							}
						}
					}	// if (m_nColumnChanged == COLUMNS::LOG_TAG_NUMBER)

					//-------------------------------------------------------------------------------------------
					// Check reduction
					if (m_nColumnChanged == COLUMNS::LOG_DEDUCTION)
					{
						CTicketLogsFormView *pTicketLogsView = (CTicketLogsFormView*)getFormViewByID(IDD_FORMVIEW5);
						if (pTicketLogsView != NULL)
						{
							if (pRec->getColFloat(COLUMNS::LOG_DEDUCTION) < 0.0 || pRec->getColFloat(COLUMNS::LOG_DEDUCTION) > 100.0)
							{
								::MessageBox(GetSafeHwnd(),m_sMsgErrorReduction,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
								pRec->setColFloat(COLUMNS::LOG_DEDUCTION,pRec->getRecord().getDeduction());
							}
						}
					}	// if (m_nColumnChanged == COLUMNS::LOG_DEDUCTION)

					GetReportCtrl().Populate();
					GetReportCtrl().UpdateWindow();
					if (bSave)
					{
						SaveLog(TRUE /* Recalulate */,(m_nColumnChanged != COLUMNS::LOG_TAG_NUMBER));
					}
				}
			}
		}	// if (pRow != NULL && pCol != NULL)
	}

}

// CLogsReportView message handlers
BOOL CLogsReportView::checkLog(CLogs& rec)
{

	// Check different criteria for log to be ok
	if ((rec.getDIBTop() > 0.0 || rec.getUTopDia() > 0.0) &&
			(rec.getLengthMeas() > 0.0 || rec.getULength() > 0.0) &&
			!rec.getTagNumber().IsEmpty())
	{
		return TRUE;
	}
	if ((rec.getDIBTop2() > 0.0 || rec.getUTopDia2() > 0.0) &&
		 (rec.getLengthMeas() > 0.0 || rec.getULength() > 0.0) &&
			!rec.getTagNumber().IsEmpty())
	{
		return TRUE;
	}

	return FALSE;

}


void CLogsReportView::SaveLog(BOOL recalc,BOOL check)
{
	BOOL bSave = TRUE;
	double fTrim = 0.0;
	double fUserLength = 0.0;
	CTicketLogsFormView *pView = (CTicketLogsFormView*)getFormViewByID(IDD_FORMVIEW5);
	CXTPReportRow *pRow = GetReportCtrl().GetFocusedRow();

	typedef CRuntimeClass *(*Func1)(CLogs &,vecExtraVolFuncID &,CVecUserVolTables &,double,int,int,int,double);
  Func1 proc1;

	CString sModule;
	
	sModule.Format(_T("%s%s\\%s"),getProgDir(),SUBDIR_MODULES,LOGSCALE_VOLUME_MODULE);

	HINSTANCE hModule = AfxLoadLibrary(sModule);
	if (hModule != NULL)
	{
		proc1 = (Func1)GetProcAddress((HMODULE)hModule, LOGSCALE_CALCULATE_METHOD2);
	}

	if (pRow != NULL && pView != NULL && m_pDB != NULL)
	{
		m_vecNoDataLogs.clear();
		CLogsReportRec *pRec = (CLogsReportRec*)pRow->GetRecord();
		if (pRec != NULL)
		{
//			if (!pRec->getIsFooter() && pRec->getRecord().getStatusFlag() == LOGSTATUS::IN_STOCK)
			if (pRow->GetIndex() > -1 && pRec->getRecord().getStatusFlag() == LOGSTATUS::IN_STOCK)
			{
				CLogs rec = pRec->getRecord();	
				fUserLength = pRec->getColFloat(COLUMNS::LOG_U_LENGTH);
				rec.setTagNumber(pRec->getColText(COLUMNS::LOG_TAG_NUMBER));
				rec.setSpcCode(pRec->getColText(COLUMNS::LOG_SPC_CODE));
				rec.setSpcName(pRec->getColText(COLUMNS::LOG_SPC_NAME));
				rec.setFrequency(pRec->getColInt(COLUMNS::LOG_FREQUENCY));
				rec.setGrade(pRec->getColText(COLUMNS::LOG_GRADE));
				rec.setDeduction(pRec->getColFloat(COLUMNS::LOG_DEDUCTION));
				rec.setDOBTop(pRec->getColFloat(COLUMNS::LOG_DOB_TOP));
				rec.setDOBTop2(pRec->getColFloat(COLUMNS::LOG_DOB_TOP2));
				rec.setDIBTop(pRec->getColFloat(COLUMNS::LOG_DIB_TOP));
				rec.setDIBTop2(pRec->getColFloat(COLUMNS::LOG_DIB_TOP2));
				rec.setDOBRoot(pRec->getColFloat(COLUMNS::LOG_DOB_ROOT));
				rec.setDOBRoot2(pRec->getColFloat(COLUMNS::LOG_DOB_ROOT2));
				rec.setDIBRoot(pRec->getColFloat(COLUMNS::LOG_DIB_ROOT));
				rec.setDIBRoot2(pRec->getColFloat(COLUMNS::LOG_DIB_ROOT2));
				rec.setVolFuncID(pView->getTicketPrlFunctionID(rec.getSpcCode(),rec.getGrade()));
				rec.setVolFuncName(pView->getTicketPrlFunction(rec.getSpcCode(),rec.getGrade()));

				rec.setLengthMeas(pRec->getColFloat(COLUMNS::LOG_LENGTH_MEAS));
				if (rec.getLengthMeas() > 0.0)
				{
					if (fUserLength <= 0.0)
						rec.setLengthCalc(pView->getLengthTrim(rec.getLengthMeas(),rec.getVolFuncID())); // pRec->getColFloat(COLUMNS::LOG_LENGTH_CALC));
					else
						rec.setLengthCalc(pView->getLengthTrim(fUserLength,rec.getVolFuncID()));
				}
				else
				{
					rec.setLengthCalc(0.0);
				}

				rec.setBark(pView->getTicketSpcBarkReduction(rec.getSpcCode()));

				rec.setVolPricelist(pRec->getColFloat(COLUMNS::LOG_VOL_PRICELIST));
				rec.setLogPrice(pRec->getColFloat(COLUMNS::LOG_PRICE));

				rec.setVolCust1(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_1));
				rec.setVolCust2(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_2));
				rec.setVolCust3(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_3));
				rec.setVolCust4(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_4));
				rec.setVolCust5(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_5));
				rec.setVolCust6(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_6));
				rec.setVolCust7(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_7));
				rec.setVolCust8(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_8));
				rec.setVolCust9(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_9));
				rec.setVolCust10(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_10));
				rec.setVolCust11(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_11));
				rec.setVolCust12(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_12));
				rec.setVolCust13(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_13));
				rec.setVolCust14(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_14));
				rec.setVolCust15(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_15));
				rec.setVolCust16(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_16));
				rec.setVolCust17(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_17));
				rec.setVolCust18(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_18));
				rec.setVolCust19(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_19));
				rec.setVolCust20(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_20));

				rec.setURootDia(pRec->getColFloat(COLUMNS::LOG_U_LARGE_DIAM));
				rec.setURootDia2(pRec->getColFloat(COLUMNS::LOG_U_LARGE_DIAM2));
				rec.setUVol(pRec->getColFloat(COLUMNS::LOG_U_VOL));
				rec.setUTopDia(pRec->getColFloat(COLUMNS::LOG_U_SMALL_DIAM));
				rec.setUTopDia2(pRec->getColFloat(COLUMNS::LOG_U_SMALL_DIAM2));
				rec.setULength(fUserLength);
				rec.setUPrice(pRec->getColFloat(COLUMNS::LOG_U_PRICE));
				rec.setReason(pRec->getColText(COLUMNS::LOG_REASON));
				rec.setNotes(pRec->getColText(COLUMNS::LOG_NOTES));

				if (recalc)
				{
					// Load Pricelist and CaclulationType, make sure we're using
					// the latest entered data.
					m_pDB->getPricelist(m_vecPricelist);
					m_pDB->getTicketPrl(m_vecTicketPrl,pView->getTicket().getPKID());
//						m_pDB->getCalcTypes(m_vecCalcTypes);
					m_pDB->getDefaults(m_vecDefaults);
					m_pDB->getUserVolTables(m_vecUserVolTables);
					m_pDB->getSelVolFuncs(m_vecSelectedVolFuncs,pView->getTicket().getPKID());

					// Setup extra volumefunctions
					m_vecExtraVolFuncID.clear();
					for (UINT i = 0;i < m_vecSelectedVolFuncs.size();i++)
					{
						m_vecExtraVolFuncID.push_back(m_vecSelectedVolFuncs[i].getFuncID());
					}

					double fOmfNum = 1.0; // _tstof(getDefaults(DEF_POUNDS_CUBIC_FOOT,m_vecDefaults)); NOT USED FOR NOW,ACCORDING TO Brian M. 2011-08-19 P�D
					// Enkelt barkavdrag => dubbelt barkavdrag
					double fDoubleBark = rec.getBark(); //*DOUBLE_BARK_FACTOR;
					//*****************************************************************************************
					//*******************   NEED TO CHANGE THIS. BARKRATIO IS SET PER SPECIES ******************

					//---------------------------------------------------------------
					// Topdiameter 1 or 2 inside bark or species changed
					if (m_nColumnChanged == COLUMNS::LOG_SPC_CODE || 
							m_nColumnChanged == COLUMNS::LOG_DIB_TOP || 
							m_nColumnChanged == COLUMNS::LOG_DIB_TOP2)
					{
						// We'll also need to chjeck measuringmode (0 = Inch, 1 = Metric)
						if (pView->getTicket().getMeasuringMode() == 0)
						{

							// Set Top-diameter on bark, based on diameter inside bark.
							// If we also have rootdiameter
							if (rec.getDIBRoot() > 0.0)
								rec.setDOBRoot(rec.getDIBRoot()+(fDoubleBark));
							// If we also have rootdiameter
							if (rec.getDIBRoot2() > 0.0)
								rec.setDOBRoot2(rec.getDIBRoot2()+(fDoubleBark));

							if (rec.getDIBTop() == 0.0)
								rec.setDOBTop(0.0);
							else
								rec.setDOBTop(rec.getDIBTop()+(fDoubleBark));

							if (rec.getDIBTop2() == 0.0)
								rec.setDOBTop2(0.0);
							else
								rec.setDOBTop2(rec.getDIBTop2()+(fDoubleBark));
						}
						else if (pView->getTicket().getMeasuringMode() == 1)
						{
							// Set Top-diameter on bark, based on diameter inside bark.
							// If we also have rootdiameter
							if (rec.getDIBRoot() > 0.0)
								rec.setDOBRoot(rec.getDIBRoot()+fDoubleBark);
							// If we also have rootdiameter
							if (rec.getDIBRoot2() > 0.0)
								rec.setDOBRoot2(rec.getDIBRoot2()+fDoubleBark);

							if (rec.getDIBTop() == 0.0)
								rec.setDOBTop(0.0);
							else
								rec.setDOBTop(rec.getDIBTop()+fDoubleBark);

							if (rec.getDIBTop2() == 0.0)
								rec.setDOBTop2(0.0);
							else
								rec.setDOBTop2(rec.getDIBTop2()+fDoubleBark);
						}
					}
					//---------------------------------------------------------------
					// Rootdiameter 1 inside bark changed
					if (m_nColumnChanged == COLUMNS::LOG_DIB_ROOT)
					{
						// We'll also need to chjeck measuringmode (0 = Inch, 1 = Metric)
						if (pView->getTicket().getMeasuringMode() == 0)
						{
							// If user entered a diameter 
							if (rec.getDIBRoot() > 0.0)
							{
								// Recalculate barkreduction
								rec.setDOBRoot(rec.getDIBRoot()+(fDoubleBark));
							}
							else
							{
								rec.setDOBRoot(0.0);
							}
						}
						else if (pView->getTicket().getMeasuringMode() == 1)
						{
							// If user entered a diameter 
							if (rec.getDIBRoot() > 0.0)
							{
								// Recalculate barkreduction
								rec.setDOBRoot(rec.getDIBRoot()+fDoubleBark);
							}
							else
							{
								rec.setDOBRoot(0.0);
							}
						}

					}
					//---------------------------------------------------------------
					// Rootdiameter 2 inside bark changed
					if (m_nColumnChanged == COLUMNS::LOG_DIB_ROOT2)
					{
						// We'll also need to chjeck measuringmode (0 = Inch, 1 = Metric)
						if (pView->getTicket().getMeasuringMode() == 0)
						{
							// If user entered a diameter 
							if (rec.getDIBRoot2() > 0.0)
							{
								// Recalculate barkreduction
								rec.setDOBRoot2(rec.getDIBRoot2()+(fDoubleBark));
							}
							else
							{
								rec.setDOBRoot2(0.0);
							}
						}
						else if (pView->getTicket().getMeasuringMode() == 1)
						{
							// If user entered a diameter 
							if (rec.getDIBRoot2() > 0.0)
							{
								// Recalculate barkreduction
								rec.setDOBRoot2(rec.getDIBRoot2()+fDoubleBark);
							}
							else
							{
								rec.setDOBRoot2(0.0);
							}
						}

					}
					//*****************************************************************************************/

					rec.setLoadID(pView->getTicket().getPKID());
					rec.setSpcID(pView->speciesID(rec.getSpcCode()));
					rec.setLogPrice(0.0);
					rec.setVolPricelist(0.0);

					// Set trim depending on measuringmode
					if (pView->getTicket().getMeasuringMode() == 0)	// Inch
					{
						fTrim = pView->getTicket().getTrimFT();
					}
					else if (pView->getTicket().getMeasuringMode() == 1)	// Metric
					{
						fTrim = pView->getTicket().getTrimCM();
					}
					if (proc1 != NULL)
					{
						proc1(rec,m_vecExtraVolFuncID,
											m_vecUserVolTables,
											pView->getTicket().getIBTaper(),
											pView->getTicket().getUseSouthernDoyle(),
											pView->getTicket().getMeasuringMode(),
											_tstoi(pView->getTicket().getDeductionType()),
											fTrim);
					}	// if (proc != NULL)

/*
					calculateDLLVolumeLogs(rec,m_vecExtraVolFuncID,
																		 m_vecUserVolTables,
																		 pView->getTicket().getIBTaper(),
																		 pView->getTicket().getUseSouthernDoyle(),
																		 pView->getTicket().getMeasuringMode(),
																		 _tstoi(pView->getTicket().getDeductionType()),
																		 fTrim);
*/
					// Check if price already set. I.e. userprice
					if (rec.getLogPrice() == 0.0)
					{
						rec.setLogPrice(pView->getLogValue(rec.getVolFuncID(),rec.getSpcID(),rec.getGrade(),rec.getVolPricelist()));
					}
/*
					// We'll check if the Log holds data to save the log
					// if not, we'll delete the log
					if (check)
					{
						if (!checkLog(rec))
						{
							m_vecNoDataLogs.push_back(rec);
						}
					}
*/
					if (checkLog(rec))
					{
						if (rec.getTicketOrigin() != LOGORIGIN::FROM_FILE)
							rec.setTicketOrigin(LOGORIGIN::MANUALLY_ON_FILE);
					}

					if (rec.getLogID() == -1)
						m_pDB->newLog(rec);
					else if (rec.getLogID() >= 0)
						m_pDB->updLog(rec);

					pView->populateLogsReport();
				} // if (recalc)
			}
		}
	}

	if (hModule != NULL)
	{
		AfxFreeLibrary(hModule);
	}	// if (hModule != NULL)

}

// Used on Toolbar button
short CLogsReportView::SaveAllLogs(BOOL on_quit)
{
	double fTrim = 0.0;
	short dReturn = -1;
	BOOL bSave = TRUE;
	double fUserLength = 0.0;
	CString sFunction = L"";

	typedef CRuntimeClass *(*Func1)(CLogs &,vecExtraVolFuncID &,CVecUserVolTables &,double,int,int,int,double);
  Func1 proc1;

	CString sModule;
	
	sModule.Format(_T("%s%s\\%s"),getProgDir(),SUBDIR_MODULES,LOGSCALE_VOLUME_MODULE);

	HINSTANCE hModule = AfxLoadLibrary(sModule);
	if (hModule != NULL)
	{
		proc1 = (Func1)GetProcAddress((HMODULE)hModule, LOGSCALE_CALCULATE_METHOD2);
	}

	CViewProgress wndPrg;

	// Create the edit control and add it to the status bar

	if (!wndPrg.Create(CViewProgress::IDD))
	{
		TRACE0("Failed to create edit control.\n");
	}

	CXTPReportRows *pRows = GetReportCtrl().GetRows();
	CTicketLogsFormView *pView = (CTicketLogsFormView*)getFormViewByID(IDD_FORMVIEW5);
	if (pRows != NULL && pView != NULL && m_pDB != NULL)
	{
		m_vecNoDataLogs.clear();
		// Load Pricelist and CaclulationType, make sure we're using
		// the latest entered data.
		m_pDB->getPricelist(m_vecPricelist);
		m_pDB->getTicketPrl(m_vecTicketPrl,pView->getTicket().getPKID());
//		m_pDB->getCalcTypes(m_vecCalcTypes);
		m_pDB->getDefaults(m_vecDefaults);
		m_pDB->getUserVolTables(m_vecUserVolTables);
		m_pDB->getSelVolFuncs(m_vecSelectedVolFuncs,pView->getTicket().getPKID());
		
		// Setup extra volumefunctions
		m_vecExtraVolFuncID.clear();
		for (UINT i = 0;i < m_vecSelectedVolFuncs.size();i++)
		{
			m_vecExtraVolFuncID.push_back(m_vecSelectedVolFuncs[i].getFuncID());
		}

		if (wndPrg.GetSafeHwnd())
		{
			wndPrg.setRange(0,pRows->GetCount());
			wndPrg.setStep();
			wndPrg.ShowWindow(SW_NORMAL);
			wndPrg.UpdateWindow();
		}

		for (int i = 0;i < pRows->GetCount();i++)
		{
			CLogsReportRec *pRec = (CLogsReportRec*)pRows->GetAt(i)->GetRecord();
			if (pRec != NULL)
			{
//				if (!pRec->getIsFooter() && pRec->getRecord().getStatusFlag() == LOGSTATUS::IN_STOCK)
				if (pRec->getRecord().getStatusFlag() == LOGSTATUS::IN_STOCK)
				{
					CLogs rec = pRec->getRecord();
					fUserLength = pRec->getColFloat(COLUMNS::LOG_U_LENGTH);
					rec.setTagNumber(pRec->getColText(COLUMNS::LOG_TAG_NUMBER));
					rec.setSpcCode(pRec->getColText(COLUMNS::LOG_SPC_CODE));
					rec.setSpcName(pRec->getColText(COLUMNS::LOG_SPC_NAME));
					rec.setFrequency(pRec->getColInt(COLUMNS::LOG_FREQUENCY));
					rec.setGrade(pRec->getColText(COLUMNS::LOG_GRADE));
					rec.setDeduction(pRec->getColFloat(COLUMNS::LOG_DEDUCTION));
					rec.setDOBTop(pRec->getColFloat(COLUMNS::LOG_DOB_TOP));
					rec.setDOBTop2(pRec->getColFloat(COLUMNS::LOG_DOB_TOP2));
					rec.setDIBTop(pRec->getColFloat(COLUMNS::LOG_DIB_TOP));
					rec.setDIBTop2(pRec->getColFloat(COLUMNS::LOG_DIB_TOP2));
					rec.setDOBRoot(pRec->getColFloat(COLUMNS::LOG_DOB_ROOT));
					rec.setDOBRoot2(pRec->getColFloat(COLUMNS::LOG_DOB_ROOT2));
					rec.setDIBRoot(pRec->getColFloat(COLUMNS::LOG_DIB_ROOT));
					rec.setDIBRoot2(pRec->getColFloat(COLUMNS::LOG_DIB_ROOT2));
					rec.setVolFuncID(pView->getTicketPrlFunctionID(rec.getSpcCode(),rec.getGrade()));
					rec.setVolFuncName(pView->getTicketPrlFunction(rec.getSpcCode(),rec.getGrade()));

					rec.setLengthMeas(pRec->getColFloat(COLUMNS::LOG_LENGTH_MEAS));
					if (rec.getLengthMeas() > 0.0)
					{
						if (fUserLength <= 0.0)
							rec.setLengthCalc(pView->getLengthTrim(rec.getLengthMeas(),rec.getVolFuncID())); // pRec->getColFloat(COLUMNS::LOG_LENGTH_CALC));
						else
							rec.setLengthCalc(pView->getLengthTrim(fUserLength,rec.getVolFuncID()));
					}
					else
					{
						rec.setLengthCalc(0.0);
					}

					rec.setBark(pView->getTicketSpcBarkReduction(rec.getSpcCode()));
					rec.setVolPricelist(pRec->getColFloat(COLUMNS::LOG_VOL_PRICELIST));
					rec.setLogPrice(pRec->getColFloat(COLUMNS::LOG_PRICE));

					rec.setVolCust1(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_1));
					rec.setVolCust2(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_2));
					rec.setVolCust3(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_3));
					rec.setVolCust4(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_4));
					rec.setVolCust5(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_5));
					rec.setVolCust6(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_6));
					rec.setVolCust7(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_7));
					rec.setVolCust8(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_8));
					rec.setVolCust9(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_9));
					rec.setVolCust10(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_10));
					rec.setVolCust11(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_11));
					rec.setVolCust12(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_12));
					rec.setVolCust13(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_13));
					rec.setVolCust14(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_14));
					rec.setVolCust15(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_15));
					rec.setVolCust16(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_16));
					rec.setVolCust17(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_17));
					rec.setVolCust18(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_18));
					rec.setVolCust19(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_19));
					rec.setVolCust20(pRec->getColFloat(COLUMNS::LOG_VOL_CUST_20));

					rec.setURootDia(pRec->getColFloat(COLUMNS::LOG_U_LARGE_DIAM));
					rec.setURootDia2(pRec->getColFloat(COLUMNS::LOG_U_LARGE_DIAM2));
					rec.setUVol(pRec->getColFloat(COLUMNS::LOG_U_VOL));
					rec.setUTopDia(pRec->getColFloat(COLUMNS::LOG_U_SMALL_DIAM));
					rec.setUTopDia2(pRec->getColFloat(COLUMNS::LOG_U_SMALL_DIAM2));
					rec.setULength(fUserLength);
					rec.setUPrice(pRec->getColFloat(COLUMNS::LOG_U_PRICE));
					rec.setReason(pRec->getColText(COLUMNS::LOG_REASON));
					rec.setNotes(pRec->getColText(COLUMNS::LOG_NOTES));

					//*****************************************************************************************
					//*******************   NEED TO CHANGE THIS. BARKRATIO IS SET PER SPECIES ******************

					// Enkelt barkavdrag => dubbelt barkavdrag
					double fDoubleBark = rec.getBark(); //*DOUBLE_BARK_FACTOR;
					// We'll also need to chjeck measuringmode (0 = Inch, 1 = Metric)
					if (pView->getTicket().getMeasuringMode() == 0)
					{
						// Recalculate barkreduction
						if (rec.getDIBTop() > 0.0)
							rec.setDOBTop(rec.getDIBTop()+(fDoubleBark));
						else
							rec.setDOBTop(0.0);
						if (rec.getDIBTop2() > 0.0)
							rec.setDOBTop2(rec.getDIBTop2()+(fDoubleBark));
						else
							rec.setDOBTop2(0.0);

						// If we also have rootdiameter
						if (rec.getDIBRoot() > 0.0)
							rec.setDOBRoot(rec.getDIBRoot()+(fDoubleBark));
						// If we also have rootdiameter
						if (rec.getDIBRoot2() > 0.0)
							rec.setDOBRoot2(rec.getDIBRoot2()+(fDoubleBark));

						// Recalculate barkreduction based on Top-diameter inside bark.
						//rec.setDOBTop(rec.getDIBTop()/(1.0-pView->getTicket().getBarkRation()));
						//rec.setBark(rec.getDOBTop()-rec.getDIBTop());
						// If we also have rootdiameter
						if (rec.getDIBRoot() > 0.0)
							rec.setDOBRoot(rec.getDIBRoot()+(fDoubleBark));
					}
					else if (pView->getTicket().getMeasuringMode() == 1)
					{
						// Recalculate barkreduction
						if (rec.getDIBTop() > 0.0)
							rec.setDOBTop(rec.getDIBTop()+fDoubleBark);
						else
							rec.setDOBTop(0.0);
						if (rec.getDIBTop2() > 0.0)
							rec.setDOBTop2(rec.getDIBTop2()+fDoubleBark);
						else
							rec.setDOBTop2(0.0);

						// If we also have rootdiameter
						if (rec.getDIBRoot() > 0.0)
							rec.setDOBRoot(rec.getDIBRoot()+fDoubleBark);
						// If we also have rootdiameter
						if (rec.getDIBRoot2() > 0.0)
							rec.setDOBRoot2(rec.getDIBRoot2()+fDoubleBark);

						// Recalculate barkreduction based on Top-diameter inside bark.
						//rec.setDOBTop(rec.getDIBTop()/(1.0-pView->getTicket().getBarkRation()));
						//rec.setBark(rec.getDOBTop()-rec.getDIBTop());
						// If we also have rootdiameter
						if (rec.getDIBRoot() > 0.0)
							rec.setDOBRoot(rec.getDIBRoot()+fDoubleBark);

						// Recalculate barkreduction based on Top-diameter inside bark.
						//rec.setDOBTop(rec.getDIBTop()/(1.0-pView->getTicket().getBarkRation()));
						//rec.setBark(rec.getDOBTop()-rec.getDIBTop());
						// If we also have rootdiameter
						if (rec.getDIBRoot() > 0.0)
							rec.setDOBRoot(rec.getDIBRoot()+fDoubleBark);
					}

					//*****************************************************************************************/

					CTicketLogsFormView *pView = (CTicketLogsFormView*)getFormViewByID(IDD_FORMVIEW5);
					rec.setLoadID(pView->getTicket().getPKID());
					rec.setSpcID(pView->speciesID(rec.getSpcCode()));
					rec.setLogPrice(0.0);
					rec.setVolPricelist(0.0);
					// Set trim depending on measuringmode
					if (pView->getTicket().getMeasuringMode() == 0)	// Inch
					{
						fTrim = pView->getTicket().getTrimFT();
					}
					else if (pView->getTicket().getMeasuringMode() == 1)	// Metric
					{
						fTrim = pView->getTicket().getTrimCM();
					}
					if (proc1 != NULL)
					{
						proc1(rec,m_vecExtraVolFuncID,
											m_vecUserVolTables,
											pView->getTicket().getIBTaper(),
											pView->getTicket().getUseSouthernDoyle(),
											pView->getTicket().getMeasuringMode(),
											_tstoi(pView->getTicket().getDeductionType()),
											fTrim);
					}	// if (proc != NULL)

/*
					calculateDLLVolumeLogs(rec,m_vecExtraVolFuncID,
																		 m_vecUserVolTables,
																		 pView->getTicket().getIBTaper(),
																		 pView->getTicket().getUseSouthernDoyle(),
																		 pView->getTicket().getMeasuringMode(),
																		 _tstoi(pView->getTicket().getDeductionType()),
																		 fTrim);
*/
					// Check if price already set. I.e. userprice
					if (rec.getLogPrice() == 0.0)
					{
						rec.setLogPrice(pView->getLogValue(rec.getVolFuncID(),rec.getSpcID(),rec.getGrade(),rec.getVolPricelist()));
					}

					bSave = TRUE;

					// We'll check if the Log holds data to save the log
					// if not, we'll delete the log
					
					if (checkLog(rec))
					{
						if (rec.getTicketOrigin() != LOGORIGIN::FROM_FILE)
							rec.setTicketOrigin(LOGORIGIN::MANUALLY_ON_FILE);					
					}
					else
					{
						m_vecNoDataLogs.push_back(rec);

						GetReportCtrl().SetFocusedRow(pRows->GetAt(i));
						break;
					}

					if (rec.getLogID() == -1)
						m_pDB->newLog(rec);
					else if (rec.getLogID() >= 0)
						m_pDB->updLog(rec);
			
				}	// if (!pRec->getIsFooter())

				if (wndPrg.GetSafeHwnd())
					wndPrg.setPos(i+1);
			}	// if (pRec != NULL)
		}	// for (int i = 0;i < pRows->GetCount();i++)

		if (hModule != NULL)
		{
			AfxFreeLibrary(hModule);
		}	// if (hModule != NULL)

		dReturn = 1;	// Return and quit
		
		if (m_vecNoDataLogs.size() > 0)
		{
			if (on_quit)
			{
				/*
				CString sTags = m_sTagNummer + L"\n";
				for (UINT i = 0;i < m_vecNoDataLogs.size();i++)
				{
					sTags += m_vecNoDataLogs[i].getTagNumber() + L",";
				}
				sTags.Delete(sTags.GetLength()-1);
				*/
				::MessageBox(GetSafeHwnd(),m_sMsgRemoveLogs1 + L"\n\n" + m_sMsgRemoveLogs2,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
				dReturn = 0;	// Return and don't quit
			}
			else
			{
				/*
				CString sTags = m_sTagNummer + L"\n";
				for (UINT i = 0;i < m_vecNoDataLogs.size();i++)
				{
					sTags += m_vecNoDataLogs[i].getTagNumber() + L",";
				}
				sTags.Delete(sTags.GetLength()-1);
				*/
				::MessageBox(GetSafeHwnd(),m_sMsgRemoveLogs1 + L"\n\n" + m_sMsgRemoveLogs2,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
				dReturn = 0;	// Return and don't quit
/*
				if (::MessageBox(GetSafeHwnd(),m_sMsgRemoveLogs1 + L"\n\n" + m_sMsgRemoveLogs2,m_sMsgCap,MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
				{
					for (UINT i = 0;i < m_vecNoDataLogs.size();i++)
					{
						m_pDB->delLog(m_vecNoDataLogs[i]);
					}
					
					m_pDB->updTicketNumOfLogs(m_vecNoDataLogs[0].getLoadID(),pRows->GetCount() - m_vecNoDataLogs.size());
					CLogScaleView *pLogScaleView = (CLogScaleView*)getFormViewByID(IDD_FORMVIEW);
					if (pLogScaleView != NULL)
					{
						pLogScaleView->populateReport(true);
					}
					dReturn = 0;	// Return and don't quit
				}
*/
			}
		}

		pView->populateLogsReport();
	} // if (pRows != NULL && pView != NULL && m_pDB != NULL)

	if (wndPrg.GetSafeHwnd())
	{
		wndPrg.ShowWindow(SW_HIDE);
		wndPrg.DestroyWindow();
	}

	return dReturn;	
}
