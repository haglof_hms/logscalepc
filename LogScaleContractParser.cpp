#include "StdAfx.h"
#include "LogScaleContractParser.h"

inline void CHECK( HRESULT _hr ) 
{ 
  if FAILED(_hr) throw(_hr); 
}


LogScaleContractParser::LogScaleContractParser(void)
{
	CHECK(CoInitialize(NULL));

	pDomDoc = NULL;

	// Create MSXML2 DOM Document
	pDomDoc.CreateInstance("Msxml2.DOMDocument.3.0");
	
	// Set parser in NON async mode
	pDomDoc->async	= VARIANT_FALSE;
	
	// Validate during parsing
	pDomDoc->validateOnParse = VARIANT_TRUE;
}

LogScaleContractParser::~LogScaleContractParser()
{
	pDomDoc->loadXML(_bstr_t(""));
	pDomDoc = NULL;
  CoUninitialize();
}

// Methods for Loagins and Saving xml file(s); 060407 p�d

BOOL LogScaleContractParser::LoadFromFile(LPCTSTR file)
{
	return pDomDoc->load(file);
}

BOOL LogScaleContractParser::LoadFromBuffer(LPCTSTR buffer)
{
	return pDomDoc->loadXML(buffer);
}

BOOL LogScaleContractParser::SaveToFile(LPCTSTR file)
{
	return pDomDoc->save(file);
}

// Protected
CString LogScaleContractParser::getAttrText(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[128];

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
	}	// if (pAttr)

	return szData;
}

double LogScaleContractParser::getAttrDouble(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[128];
	double fValue;

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
		fValue = _tstof(szData);
	}	// if (pAttr)

	return fValue;
}

int LogScaleContractParser::getAttrInt(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[128];
	int nValue;

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
		nValue = _tstoi(szData);
	}	// if (pAttr)

	return nValue;
}

BOOL LogScaleContractParser::getAttrBool(MSXML2::IXMLDOMNamedNodeMap *attr,LPCTSTR attr_name)
{
	CComBSTR bstrData;
	TCHAR szData[128];
	BOOL bValue;

	MSXML2::IXMLDOMNodePtr pAttr = attr->getNamedItem(_bstr_t(attr_name));

	if (pAttr)
	{
		pAttr->get_text( &bstrData );
		_tcscpy(szData,_bstr_t(bstrData));
		bValue = (_tcscmp(szData,_T("0")) == 0 ? FALSE : TRUE);
	}	// if (pAttr)

	return bValue;
}


// Public

BOOL  LogScaleContractParser::isALogScaleContract()
{
	CComBSTR bstrData;
	TCHAR szData[128];

	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	if (pRoot)
	{	
		_tcscpy(szData,_bstr_t(pRoot->baseName));
		if (_tcscmp(szData,CONTRACT_IDENTIFICATION) == 0)
			return TRUE;
		else
			return FALSE;
	}

	return FALSE;
}

// Methods for reading Header information
BOOL LogScaleContractParser::getHeaderName(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(TAG_GET_HEADER_NAME2);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));
		return TRUE;
	}

	return FALSE;
}

BOOL LogScaleContractParser::getHeaderDoneBy(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(TAG_GET_HEADER_DONE_BY2);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));

		return TRUE;
	}

	return FALSE;
}

BOOL LogScaleContractParser::getHeaderDate(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(TAG_GET_HEADER_DATE2);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));
		return TRUE;
	}

	return FALSE;
}

BOOL LogScaleContractParser::getHeaderMode(int *mode)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(TAG_GET_HEADER_MODE2);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		*mode = _tstoi(bstrData);
		return TRUE;
	}

	return FALSE;
}

BOOL LogScaleContractParser::getHeaderTrimFt(double *trim)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(TAG_GET_HEADER_TRIM_FT2);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		*trim = _tstof(bstrData);
		return TRUE;
	}

	return FALSE;
}

BOOL LogScaleContractParser::getHeaderTrimCM(double *trim)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(TAG_GET_HEADER_TRIM_CM2);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		*trim = _tstof(bstrData);
		return TRUE;
	}

	return FALSE;
}

BOOL LogScaleContractParser::getHeaderNotes(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(TAG_GET_HEADER_NOTES2);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));
		return TRUE;
	}

	return FALSE;
}

BOOL LogScaleContractParser::getHeaderPrlName(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(TAG_GET_HEADER_PRL_NAME2);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));
		return TRUE;
	}

	return FALSE;
}

BOOL LogScaleContractParser::getHeaderDefSpcCode(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(TAG_GET_HEADER_DEF_SPC2);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));
		return TRUE;
	}

	return FALSE;
}

BOOL LogScaleContractParser::getHeaderDefGrdCode(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(TAG_GET_HEADER_DEF_GRD2);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));
		return TRUE;
	}

	return FALSE;
}

BOOL LogScaleContractParser::getDataFile(LPTSTR data)
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(TAG_GET_DATA_FILE2);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		_tcscpy(data,_bstr_t(bstrData));
		return TRUE;
	}

	return FALSE;
}

long LogScaleContractParser::getDataFileSize()
{
	CComBSTR bstrData;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	MSXML2::IXMLDOMNodePtr pNode = pRoot->selectSingleNode(TAG_GET_DATA_FILE2);
	if (pNode)
	{	
		pNode->get_text(&bstrData);
		return bstrData.Length();
	}

	return 0;
}


BOOL LogScaleContractParser::getXML(CString &xml)
{
	CComBSTR bstrBuffer;
	MSXML2::IXMLDOMElementPtr pRoot = pDomDoc->documentElement;
	if (pRoot)
	{
		pRoot->get_xml( &bstrBuffer );
		CW2CT szBuffer( bstrBuffer );
		xml = szBuffer;
		return TRUE;
	}

	return FALSE;
}
