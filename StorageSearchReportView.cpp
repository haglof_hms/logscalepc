// ListPrlReportView.cpp : implementation file
//

#include "StdAfx.h"

#include "StorageSearchReportView.h"

#include "ResLangFileReader.h"

#include "ReportClasses.h"

#include "XTPPreviewView.h"

#include "libxl.h"

using namespace libxl;


///////////////////////////////////////////////////////////////////////////////////////////
// CStorageSearchDoc

IMPLEMENT_DYNCREATE(CStorageSearchDoc, CDocument)

BEGIN_MESSAGE_MAP(CStorageSearchDoc, CDocument)
	//{{AFX_MSG_MAP(CStorageSearchDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CStorageSearchDoc construction/destruction

CStorageSearchDoc::CStorageSearchDoc()
{
	// TODO: add one-time construction code here

}

CStorageSearchDoc::~CStorageSearchDoc()
{
}


BOOL CStorageSearchDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CStorageSearchDoc serialization

void CStorageSearchDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CStorageSearchDoc diagnostics

#ifdef _DEBUG
void CStorageSearchDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CStorageSearchDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


/////////////////////////////////////////////////////////////////////////////
// CStorageSearchFrame

IMPLEMENT_DYNCREATE(CStorageSearchFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CStorageSearchFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CStorageSearchFrame)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_PAINT()
	ON_UPDATE_COMMAND_UI(ID_BUTTON32824, OnUpdateTBBTNFilterOff)
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

CStorageSearchFrame::CStorageSearchFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDD_FORMVIEW);
	m_bEnableTBBTNFilterOff = FALSE;
	LOGFONT lfIcon;
	VERIFY(::SystemParametersInfo(SPI_GETICONTITLELOGFONT, sizeof(lfIcon), &lfIcon, 0));
	VERIFY(m_fontIcon.CreateFontIndirect(&lfIcon));
}

CStorageSearchFrame::~CStorageSearchFrame()
{
}

void CStorageSearchFrame::OnDestroy(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_STORAGE_SEARCH_KEY);
	SavePlacement(this, csBuf);
	m_bFirstOpen = TRUE;

	CMDIChildWnd::OnDestroy();

}

int CStorageSearchFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	if (m_hIcon)
	{
		SetIcon(m_hIcon,TRUE);
		SetIcon(m_hIcon,FALSE);
	}
	// Setup language filename; 051214 p�d
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	// Create and Load toolbar; 051219 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR9);
	m_wndToolBar.EnableDocking(xtpFlagAlignTop|xtpFlagStretched);

	EnableDocking(CBRS_ALIGN_ANY);

	// Initialize dialog bar m_wndFieldChooser
	if (!m_wndFieldChooserDlg.Create(this, IDD_FIELD_SELECTION4,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_SHOW_FIELD_SELECTION))
		return -1;      // fail to create

	// Initialize dialog bar m_wndFilterEdit
	if (!m_wndFilterEdit.Create(this, IDD_FILTEREDIT20,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_BUTTON32799))
		return -1;      // fail to create

	// docking for field chooser
	m_wndFieldChooserDlg.EnableDocking(0);
	setLanguage();

	ShowControlBar(&m_wndFieldChooserDlg, FALSE, FALSE);
	FloatControlBar(&m_wndFieldChooserDlg, CPoint(100, GetSystemMetrics(SM_CYSCREEN) / 3));

	// docking for filter editing
	m_wndFilterEdit.EnableDocking(CBRS_ALIGN_TOP);

	ShowControlBar(&m_wndFilterEdit, FALSE, FALSE);
	FloatControlBar(&m_wndFilterEdit, CPoint(400, GetSystemMetrics(SM_CYSCREEN) / 3));

	setupToolBarIcons();

	m_bFirstOpen = TRUE;
	return 0; // creation ok
}

// load the placement in OnShowWindow()
void CStorageSearchFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_WP_STORAGE_SEARCH_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CStorageSearchFrame::OnSetFocus(CWnd *pWnd)
{
	CMDIChildWnd::OnSetFocus(pWnd);
}

BOOL CStorageSearchFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CStorageSearchFrame diagnostics

#ifdef _DEBUG
void CStorageSearchFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CStorageSearchFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG


void CStorageSearchFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{

	lpMMI->ptMinTrackSize.x = 400;
	lpMMI->ptMinTrackSize.y = 200;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CStorageSearchFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CStorageSearchFrame::OnPaint()
{
	CMDIChildWnd::OnPaint();
}

void CStorageSearchFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType, cx, cy);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CStorageSearchFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}

void CStorageSearchFrame::OnUpdateTBBTNFilterOff(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableTBBTNFilterOff);
}

// MY METHODS

void  CStorageSearchFrame::setLanguage(void)
{
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_wndFieldChooserDlg.SetWindowText(xml.str(IDS_STRING5955));
			m_wndFilterEdit.SetWindowText(xml.str(IDS_STRING5950));

			xml.clean();
		}	// if (xml->Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

}

void CStorageSearchFrame::setupToolBarIcons(void)
{
	HICON hIcon = NULL;
	HMODULE hResModule = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	if (fileExists(sTBResFN))
	{
		// Setup commandbars and manues; 051114 p�d
		CXTPToolBar* pToolBar = &m_wndToolBar;
		if (pToolBar->IsBuiltIn())
		{
			if (pToolBar->GetType() != xtpBarTypeMenuBar)
			{

				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				if (fileExists(m_sLangFN))
				{
					RLFReader xml;
					if (xml.Load(m_sLangFN))
					{

						// Setup icons on toolbars, using resource dll; 051208 p�d
						if (nBarID == IDR_TOOLBAR9)
						{		
							setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_EXPORT,xml.str(IDS_STRING60200),TRUE);	//
							setToolbarBtn(sTBResFN,p->GetAt(1),RES_TB_PRINT,xml.str(IDS_STRING60201),TRUE);	//
							setToolbarBtn(sTBResFN,p->GetAt(2),RES_TB_FILTER,xml.str(IDS_STRING60202),TRUE);	//
							setToolbarBtn(sTBResFN,p->GetAt(3),RES_TB_FILTER_OFF,xml.str(IDS_STRING60203),TRUE);	//
						}	// if (nBarID == IDR_TOOLBAR9)
						xml.clean();
					}
				}
			}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
		}	// if (pToolBar->IsBuiltIn())
	}	// if (fileExists(sTBResFN))
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
// CStorageSearchView

IMPLEMENT_DYNCREATE(CStorageSearchView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CStorageSearchView, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_COPYDATA()
	ON_WM_CREATE()
	ON_WM_SETFOCUS()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_COMMAND(ID_BUTTON32821, OnExportToExcel)
	ON_COMMAND(ID_BUTTON32822, OnPrintPreview)
	ON_COMMAND(ID_BUTTON32823, OnShowFieldFilter)
	ON_COMMAND(ID_BUTTON32824, OnShowFieldFilterOff)

	ON_BN_CLICKED(IDC_BUTTON6516_1, &CStorageSearchView::OnBnClickedButton65161)
	ON_BN_CLICKED(IDC_BUTTON6516_2, &CStorageSearchView::OnBnClickedButton65162)
END_MESSAGE_MAP()

CStorageSearchView::CStorageSearchView()
	: CXTResizeFormView(CStorageSearchView::IDD),
		m_bInitialized(FALSE),
		m_pDB(NULL),
		m_sLangFN(L""),
		m_LanguageSet(-1)
		, m_csDate1(_T(""))
{
}

CStorageSearchView::~CStorageSearchView()
{
}

void CStorageSearchView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_GROUP6516_1, m_wndGrp6516_1);
	DDX_Control(pDX, IDC_GROUP6516_2, m_wndGrp6516_2);
	DDX_Control(pDX, IDC_GROUP6516_3, m_wndGrp6516_3);

	DDX_Control(pDX, IDC_BUTTON6516_1, m_btn6516_1);
	DDX_Control(pDX, IDC_BUTTON6516_2, m_btn6516_2);

	DDX_Control(pDX, IDC_RADIO6516_1, m_rb6516_1);
	DDX_Control(pDX, IDC_RADIO6516_2, m_rb6516_2);
	DDX_Control(pDX, IDC_RADIO6516_3, m_rb6516_3);

	DDX_Control(pDX, IDC_LBL6516_1, m_lbl6516_1);
	DDX_Control(pDX, IDC_LBL6516_2, m_lbl6516_2);
	DDX_Control(pDX, IDC_LBL6516_3, m_lbl6516_3);
	DDX_Control(pDX, IDC_LBL6516_4, m_lbl6516_4);
	DDX_Control(pDX, IDC_LBL6516_5, m_lbl6516_5);
	DDX_Control(pDX, IDC_LBL6516_6, m_lbl6516_6);
	DDX_Control(pDX, IDC_LBL6516_7, m_lbl6516_7);
	DDX_Control(pDX, IDC_LBL6516_8, m_lbl6516_8);
	DDX_Control(pDX, IDC_LBL6516_9, m_lbl6516_9);
	DDX_Control(pDX, IDC_LBL6516_10, m_lbl6516_10);
	DDX_Control(pDX, IDC_LBL6516_11, m_lbl6516_11);
	DDX_Control(pDX, IDC_LBL6516_13, m_lbl6516_13);
	DDX_Control(pDX, IDC_LBL6516_15, m_lbl6516_15);

	DDX_Control(pDX, IDC_EDIT6516_1, m_ed6516_1);
	DDX_Control(pDX, IDC_EDIT6516_2, m_ed6516_2);
	DDX_Control(pDX, IDC_EDIT6516_3, m_ed6516_3);
	DDX_Control(pDX, IDC_EDIT6516_10, m_ed6516_10);

	DDX_Control(pDX, IDC_COMBO6516_1, m_cb6516_1);
	DDX_Control(pDX, IDC_COMBO6516_2, m_cb6516_2);
	DDX_Control(pDX, IDC_COMBO6516_3, m_cb6516_3);
	DDX_Control(pDX, IDC_COMBO6516_4, m_cb6516_4);
	DDX_Control(pDX, IDC_COMBO6516_5, m_cb6516_5);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_DATE_6516_1, m_dtDate1);
	DDX_DateTimeCtrl(pDX, IDC_DATE_6516_1, m_csDate1);
	DDX_Control(pDX, IDC_DATE_6516_2, m_dtDate2);
	DDX_DateTimeCtrl(pDX, IDC_DATE_6516_2, m_csDate2);
	DDX_Control(pDX, IDC_DATE_6516_3, m_dtDate3);
	DDX_DateTimeCtrl(pDX, IDC_DATE_6516_3, m_csDate3);
	DDX_Control(pDX, IDC_DATE_6516_4, m_dtDate4);
	DDX_DateTimeCtrl(pDX, IDC_DATE_6516_4, m_csDate4);
}

void CStorageSearchView::OnDestroy()
{
	CXTResizeFormView::OnDestroy();
}

void CStorageSearchView::OnClose()
{
	CXTResizeFormView::OnClose();
}

int CStorageSearchView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	CFont fnt;
	LOGFONT lf;
	VERIFY(fnt.CreateFont(
   20,                        // nHeight
   0,                         // nWidth
   0,                         // nEscapement
   0,                         // nOrientation
   FW_BOLD,                 // nWeight
   FALSE,                     // bItalic
   FALSE,                     // bUnderline
   0,                         // cStrikeOut
   ANSI_CHARSET,              // nCharSet
   OUT_DEFAULT_PRECIS,        // nOutPrecision
   CLIP_DEFAULT_PRECIS,       // nClipPrecision
   DEFAULT_QUALITY,           // nQuality
   DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
   _T("Times New Roman")));   // lpszFacename

	fnt.GetLogFont( &lf );

	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS, CRect(0, 0, 0, 0), this, ID_TABCONTROL);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearancePropertyPage2003);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = TRUE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = TRUE;
	m_wndTabControl.GetPaintManager()->DisableLunaColors(FALSE);
	m_wndTabControl.GetPaintManager()->SetFontIndirect( &lf );

	// Setup language filename; 051214 p�d
	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;

		if (xml.Load(m_sLangFN))
		{
			m_sTabCaption = xml.str(IDS_STRING60113);
			m_sNumOfHits = xml.str(IDS_STRING60127);

			AddView(RUNTIME_CLASS(CStorageSearchReportView), m_sTabCaption,-1);
		}

		xml.clean();
	}

	// We'll chack if the directory for columnsettings is created
	CString sDirectory;
	sDirectory.Format(L"%s\\%s",getMyDocumentsDir(),SEARCH_SETTINGS_DIRECTORY);
	// Check if directory exists, on disk. If not create it; 080205 p�d
	if (!isDirectory(sDirectory))
	{
		createDirectory(sDirectory);
	}



	return 0;
}

BOOL CStorageSearchView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CStorageSearchView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	if (! m_bInitialized )
	{

		char *c[] = { "<",">","=","&","|",",",".","0","1","2","3","4","5","6","7","8","9" };
		m_ed6516_1.setValidChar(c,sizeof(c)/sizeof(*c));

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_wndGrp6516_1.SetWindowTextW(xml.str(IDS_STRING60220));
			m_wndGrp6516_2.SetWindowTextW(xml.str(IDS_STRING60122));
			m_wndGrp6516_3.SetWindowTextW(xml.str(IDS_STRING60125));

			m_btn6516_1.SetWindowTextW(xml.str(IDS_STRING60221));
			m_btn6516_2.SetWindowTextW(xml.str(IDS_STRING60222));

			m_lbl6516_1.SetWindowTextW(xml.str(IDS_STRING60114));
			m_lbl6516_2.SetWindowTextW(xml.str(IDS_STRING60115));
			m_lbl6516_3.SetWindowTextW(xml.str(IDS_STRING60116));
			m_lbl6516_4.SetWindowTextW(xml.str(IDS_STRING60117));
			m_lbl6516_5.SetWindowTextW(xml.str(IDS_STRING60118));
			m_lbl6516_6.SetWindowTextW(xml.str(IDS_STRING60119));
			m_lbl6516_7.SetWindowTextW(xml.str(IDS_STRING60120));
			m_lbl6516_8.SetWindowTextW(xml.str(IDS_STRING60121));
			m_lbl6516_15.SetWindowTextW(xml.str(IDS_STRING60129));

			m_lbl6516_9.SetWindowTextW(xml.str(IDS_STRING60130));
			m_lbl6516_13.SetWindowTextW(xml.str(IDS_STRING60128));

			m_lbl6516_10.SetWindowTextW(xml.str(IDS_STRING60123));
			m_lbl6516_11.SetWindowTextW(xml.str(IDS_STRING60124));
			
			m_rb6516_1.SetWindowTextW(xml.str(IDS_STRING116));
			m_rb6516_2.SetWindowTextW(xml.str(IDS_STRING117));
			m_rb6516_3.SetWindowTextW(xml.str(IDS_STRING128));

			tokenizeString(xml.str(IDS_STRING203),';',m_sarrStatus);
			tokenizeString(xml.str(IDS_STRING207),';',m_sarrSearchAs);

			m_sMsgCap = xml.str(IDS_STRING99);
			m_sMsgLengthSearch = xml.str(IDS_STRING60250);
			m_sMsgDayMonthCheck = xml.str(IDS_STRING60251);
			m_sMsgMeasuringmodeCheck = xml.str(IDS_STRING60252);

			if (getLangSet().CompareNoCase(L"ENU") == 0)
			{
				m_LanguageSet = 0;
			}
			else
			{
				m_LanguageSet = 1;
			}

		}
		xml.clean();


		// Setup comboboxes
		m_cb6516_1.AddString(L"");	// Empty line
		for (short i = 0;i < m_sarrStatus.GetCount();i++)
		{
			m_cb6516_1.AddString(m_sarrStatus.GetAt(i));
		}

		m_cb6516_2.AddString(L"");	// Empty line
		for ( itSpc = m_vecSpecies.begin();itSpc < m_vecSpecies.end();++itSpc)
		{
			if (itSpc->getSpcName().IsEmpty())
			{
				m_cb6516_2.AddString(itSpc->getSpcCode());
			}
			else
			{
				m_cb6516_2.AddString(itSpc->getSpcCode() + L" - " + itSpc->getSpcName());
			}
		}
		m_cb6516_3.AddString(L"");	// Empty line
		for ( itGrades = m_vecGrades.begin();itGrades < m_vecGrades.end();++itGrades)
		{
			if (itGrades->getGradesName().IsEmpty())
			{
				m_cb6516_3.AddString(itGrades->getGradesCode());
			}
			else
			{
				m_cb6516_3.AddString(itGrades->getGradesCode() + L" - " + itGrades->getGradesName());
			}
		}

		cbRegisterData(REGISTER_TYPES::SCALER,&m_cb6516_4,m_vecRegister);
		cbRegisterData(REGISTER_TYPES::BUYER,&m_cb6516_5,m_vecRegister);

		if (m_cb6516_1.GetCount() > 0)
		{
			m_cb6516_1.SetCurSel(0);
		}
		m_bInitialized = TRUE;


	}

	setupLastSeach();

}

void CStorageSearchView::OnSetFocus(CWnd* pOldWnd)
{
	m_cb6516_1.SetFocus();

	CXTResizeFormView::OnSetFocus(pOldWnd);
}

BOOL CStorageSearchView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{

	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		if (m_dbConnectionData.conn->isConnected())
		{
			if ((m_pDB = new CDBHandling(m_dbConnectionData)) != NULL)
			{
				m_pDB->getRegister(m_vecRegister);
				m_pDB->getSpecies(m_vecSpecies);
				m_pDB->getGrades(m_vecGrades);
			}
		}
	}

	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CStorageSearchView::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	if (m_wndTabControl.GetSafeHwnd())
	{
		m_wndTabControl.MoveWindow(0, 170, cx, cy-174);
	}
	if (m_wndGrp6516_1.GetSafeHwnd())
	{
		setResize(&m_wndGrp6516_1,4,4,cx-8,155);
	}

/*	if (m_dtDate1.GetSafeHwnd())
	{
		CRect rect;
		m_dtDate1.GetWindowRect(&rect);
		ScreenToClient(&rect);
		setResize(&m_dtDate1,rect.left,rect.top,20,20);
	}

	if (m_dtDate2.GetSafeHwnd())
	{
		CRect rect;
		m_dtDate2.GetWindowRect(&rect);
		ScreenToClient(&rect);
		setResize(&m_dtDate2,rect.left,rect.top,20,20);
	}

	if (m_dtDate3.GetSafeHwnd())
	{
		CRect rect;
		m_dtDate3.GetWindowRect(&rect);
		ScreenToClient(&rect);
		setResize(&m_dtDate3,rect.left,rect.top,20,20);
	}

	if (m_dtDate4.GetSafeHwnd())
	{
		CRect rect;
		m_dtDate4.GetWindowRect(&rect);
		ScreenToClient(&rect);
		setResize(&m_dtDate4,rect.left,rect.top,20,20);
	}*/

}

LRESULT CStorageSearchView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	switch(wParam)
	{
	};

	return 0L;
}



// CStorageSearchView diagnostics

#ifdef _DEBUG
void CStorageSearchView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CStorageSearchView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG


void CStorageSearchView::OnExportToExcel()
{
	m_tabManager = m_wndTabControl.getTabPage(0);
	if (m_tabManager)
	{
		CStorageSearchReportView* pView = DYNAMIC_DOWNCAST(CStorageSearchReportView, CWnd::FromHandle(m_tabManager->GetHandle()));
		if (pView != NULL)
		{
			pView->exportToExcel();
		}
	}
}

void CStorageSearchView::OnPrintPreview()
{
	m_tabManager = m_wndTabControl.getTabPage(0);
	if (m_tabManager)
	{
		CStorageSearchReportView* pView = DYNAMIC_DOWNCAST(CStorageSearchReportView, CWnd::FromHandle(m_tabManager->GetHandle()));
		if (pView != NULL)
		{
			pView->SendMessage(WM_COMMAND,ID_FILE_PRINT);
		}
	}
}

void CStorageSearchView::OnShowFieldFilter()
{
	m_tabManager = m_wndTabControl.getTabPage(0);
	if (m_tabManager)
	{
		CStorageSearchReportView* pView = DYNAMIC_DOWNCAST(CStorageSearchReportView, CWnd::FromHandle(m_tabManager->GetHandle()));
		if (pView != NULL)
		{
			pView->showFieldFilter();
		}
	}
}

void CStorageSearchView::OnShowFieldFilterOff()
{
	m_tabManager = m_wndTabControl.getTabPage(0);
	if (m_tabManager)
	{
		CStorageSearchReportView* pView = DYNAMIC_DOWNCAST(CStorageSearchReportView, CWnd::FromHandle(m_tabManager->GetHandle()));
		if (pView != NULL)
		{
			pView->showFieldFilterOff();
		}
	}
}

CStorageSearchReportView *CStorageSearchView::getStorageSearchReportView(void)
{
	m_tabManager = m_wndTabControl.getTabPage(0);
	if (m_tabManager)
	{
		CStorageSearchReportView* pView = DYNAMIC_DOWNCAST(CStorageSearchReportView, CWnd::FromHandle(m_tabManager->GetHandle()));
		ASSERT_KINDOF(CStorageSearchReportView, pView);
		return pView;
	}
	return NULL;
}

CString CStorageSearchView::getSpcCode(int idx)
{
	if (idx > 0 && idx <= m_vecSpecies.size())
	{
		return m_vecSpecies[idx-1].getSpcCode();
	}

	return L"";
}

CString CStorageSearchView::getGradesCode(int idx)
{
	if (idx > 0 && idx <= m_vecGrades.size())
	{
		return m_vecGrades[idx-1].getGradesCode();
	}

	return L"";
}

CString CStorageSearchView::getCBText(CComboBox& cb)
{
	CString sText = L"";
	cb.GetWindowTextW(sText);

	return sText;
}

CString CStorageSearchView::interpretMacro(CString column,CString macro)
{
	CString sToken1 = L"",sToken2 = L"",sReturn = L"";
	macro.Trim();
	// Check if macro includes & or |
	if (macro.FindOneOf(_T("&")) > -1)
	{
		macro += L"&";
		AfxExtractSubString(sToken1,macro,0,'&');
		AfxExtractSubString(sToken2,macro,1,'&');
		sToken1.Trim();
		sToken2.Trim();
		if (sToken1.FindOneOf(L"=><") == -1)
		{
			sToken1 = L"=" + sToken1;
		}
		if (sToken2.FindOneOf(L"=><") == -1)
		{
			sToken2 = L"=" + sToken2;
		}
		sReturn.Format(L"%s%s AND %s%s",column,sToken1,column,sToken2);
	}

	else if (macro.FindOneOf(_T("|")) > -1)
	{
		macro += L"|";
		AfxExtractSubString(sToken1,macro,0,'|');
		AfxExtractSubString(sToken2,macro,1,'|');
		sToken1.Trim();
		sToken2.Trim();
		if (sToken1.FindOneOf(L"=><") == -1)
		{
			sToken1 = L"=" + sToken1;
		}
		if (sToken2.FindOneOf(L"=><") == -1)
		{
			sToken2 = L"=" + sToken2;
		}
		sReturn.Format(L"%s%s OR %s%s",column,sToken1,column,sToken2);
	}
	else
	{
		if (macro.FindOneOf(L"=><") == -1)
		{
			macro = L"=" + macro;
		}
		sReturn.Format(L"%s%s",column,macro);
	}

	return sReturn;
}

BOOL CStorageSearchView::validateEnteredDate(int year,int month,int day)
{
	if (month < 1 || month > 12)
		return FALSE;

	switch (month)
	{
		case 1:	// Januari
		case 3:	// Mars
		case 5:	// Maj
		case 7:	// Juli
		case 8:	// Augusti
		case 10:	// Oktober
		case 12:	// December
			if (day > 31) 
				return FALSE;
			else
				return TRUE;
		break;
		case 4:	// April
		case 6:	// Juni
		case 9:	// September
		case 11:	// November
			if (day > 30) 
				return FALSE;
			else
				return TRUE;
		break;
		case 2 :	// Februari
			// Leapyear
			if (((year % 4) == 0) && ((year % 100) != 0) && ((year % 400) == 0))
			{
				if (day > 29)
					return FALSE;
				else
					return TRUE;
			}
			else
			{
				if (day > 28)
					return FALSE;
				else
					return TRUE;
			}
			break;
	};
}

void CStorageSearchView::setupLastSeach()
{
	int nID = 0;
  CStdioFile f;
	CString sPath = L"",sLine = L"",sId = L"",sToken = L"";
	CString sYearFrom1 = L"",sMonthFrom1 = L"",sDayFrom1 = L"";
	CString sYearTo1 = L"",sMonthTo1 = L"",sDayTo1 = L"";
	// Save serach settings
	sPath.Format(L"%s\\%s\\search_settings.txt",getMyDocumentsDir(),SEARCH_SETTINGS_DIRECTORY);
	if (fileExists(sPath))
	{
		if (f.Open(sPath, CFile::modeRead | CFile::shareDenyWrite))
		{
			while (f.ReadString(sLine))
			{
				AfxExtractSubString(sId,sLine,0,';');
				nID = _tstoi(sId);

				switch(nID)
				{
					// Measuringmode
					case 1000 :
						AfxExtractSubString(sToken,sLine,1,';');
						if (_tstoi(sToken) == 1)
							m_rb6516_1.SetCheck(1);
						else if (_tstoi(sToken) == 2)
							m_rb6516_2.SetCheck(1);
						else
						{
							m_rb6516_1.SetCheck(0);
							m_rb6516_2.SetCheck(0);
							m_rb6516_3.SetCheck(1);
						}
					break;
					// Status
					case 1001 :
						AfxExtractSubString(sToken,sLine,1,';');
						m_cb6516_1.SetCurSel(_tstoi(sToken));
					break;
					// Species
					case 1002 :
						AfxExtractSubString(sToken,sLine,1,';');
						m_cb6516_2.SetCurSel(_tstoi(sToken));
					break;
					// Grades
					case 1003 :
						AfxExtractSubString(sToken,sLine,1,';');
						m_cb6516_3.SetCurSel(_tstoi(sToken));
					break;
					// Scaler
					case 1004 :
						AfxExtractSubString(sToken,sLine,1,';');
						m_cb6516_4.SetCurSel(m_cb6516_4.FindStringExact(0,sToken));
					break;
					// Buyer
					case 1005 :
						AfxExtractSubString(sToken,sLine,1,';');
						m_cb6516_5.SetCurSel(m_cb6516_5.FindStringExact(0,sToken));
					break;
					// Length
					case 1006 :
						AfxExtractSubString(sToken,sLine,1,';');
						m_ed6516_1.SetWindowText(sToken);
					break;
					// Ticket
					case 1007 :
						AfxExtractSubString(sToken,sLine,1,';');
						m_ed6516_2.SetWindowText(sToken);
					break;
					// Tagnr.
					case 1101 :
						AfxExtractSubString(sToken,sLine,1,';');
						m_ed6516_10.SetWindowText(sToken);
					break;
					// Sales
					case 1200 :
						AfxExtractSubString(sToken,sLine,1,';');
						m_ed6516_3.SetWindowText(sToken);
					break;
					// Date
					case 1010 :
						AfxExtractSubString(sYearFrom1,sLine,1,';');
						AfxExtractSubString(sMonthFrom1,sLine,2,';');
						AfxExtractSubString(sDayFrom1,sLine,3,';');
						m_csDate1.Format(_T("%s-%s-%s"), sYearFrom1, sMonthFrom1, sDayFrom1);
					break;
					case 1011 :
						AfxExtractSubString(sYearTo1,sLine,1,';');
						AfxExtractSubString(sMonthTo1,sLine,2,';');
						AfxExtractSubString(sDayTo1,sLine,3,';');
						m_csDate2.Format(_T("%s-%s-%s"), sYearTo1, sMonthTo1, sDayTo1);
					break;
					// Date
					case 1012 :
						AfxExtractSubString(sYearFrom1,sLine,1,';');
						AfxExtractSubString(sMonthFrom1,sLine,2,';');
						AfxExtractSubString(sDayFrom1,sLine,3,';');
						m_csDate3.Format(_T("%s-%s-%s"), sYearFrom1, sMonthFrom1, sDayFrom1);
					break;
					case 1013 :
						AfxExtractSubString(sYearTo1,sLine,1,';');
						AfxExtractSubString(sMonthTo1,sLine,2,';');
						AfxExtractSubString(sDayTo1,sLine,3,';');
						m_csDate4.Format(_T("%s-%s-%s"), sYearTo1, sMonthTo1, sDayTo1);
					break;
				};
			}
			f.Close();

			UpdateData(FALSE);
		}
	}	// if (fileExists(sPath))

}

void CStorageSearchView::populateReport(void)
{
	int nTraktID = -1;
	CStorageSearchReportView *pView = getStorageSearchReportView();
	if (pView == NULL)
		return;

	CXTPReportRecord *pRec = NULL;
	pView->GetReportCtrl().GetRecords()->RemoveAll();
	for (UINT it = 0;it < m_vecSearchPopulate.size();it++)
	{
		pView->GetReportCtrl().AddRecord(new CSearchReportRec(m_vecSearchPopulate[it],m_sarrStatus));
	}
	pView->GetReportCtrl().Populate();
	pView->GetReportCtrl().UpdateWindow();
}

// Create the where clause in sql search
BOOL CStorageSearchView::setupSQLWhereClause()
{
	BOOL bFirstAdded = TRUE;
	BOOL bFirstAddedSales = TRUE;
	CString sSQL = L"",sTmp = L"",sSettings = L"";
	m_sSQLWhereClause.Empty();
	m_sSQLWhereClauseSales.Empty();
  CStdioFile f;
	CString sPath = L"";

	UpdateData(TRUE);

	//---------------------------------------------------------------------------
	// Measuringmode
	int nMeasMode = -1;
	if (m_rb6516_1.GetCheck() == 1)	// Inch
	{
		nMeasMode = 0;
	}
	else if (m_rb6516_2.GetCheck() == 1)	// Metric
	{
		nMeasMode = 1;
	}
	if (nMeasMode > -1)
	{
		if (!bFirstAdded)
		{
			sSQL.Format(L" AND logscaleTicketsNew.nMeasuringMode=%d ",nMeasMode);
			m_sSQLWhereClause += sSQL;
		}
		else
		{
			sSQL.Format(L" logscaleTicketsNew.nMeasuringMode=%d ",nMeasMode);
			m_sSQLWhereClause = sSQL;
		}
		bFirstAdded = FALSE;
	}
	if (m_rb6516_1.GetCheck() == 1)
		sTmp.Format(L"1000;%d;\n",1);
	else if (m_rb6516_2.GetCheck() == 1)
		sTmp.Format(L"1000;%d;\n",2);
	sSettings += sTmp;


	//---------------------------------------------------------------------------
	// Status
	if (m_cb6516_1.GetCurSel() > 0 /* 0 = Empty */)
	{
		if (!bFirstAdded)
		{
			sSQL.Format(L" AND logscaleLogsNew.nStatusFlag=%d ",m_cb6516_1.GetCurSel()-1);
			m_sSQLWhereClause += sSQL;
		}
		else
		{
			sSQL.Format(L" logscaleLogsNew.nStatusFlag=%d ",m_cb6516_1.GetCurSel()-1);
			m_sSQLWhereClause = sSQL;
		}
		bFirstAdded = FALSE;
		sTmp.Format(L"1001;%d;\n",m_cb6516_1.GetCurSel());
		sSettings += sTmp;
	}	// if (m_cb6516_1.GetCurSel() > 0)

	//---------------------------------------------------------------------------
	// Tr�dslag
	if (m_cb6516_2.GetCurSel() > 0 /* 0 = Empty */)
	{
		if (!bFirstAdded)
		{
			sSQL.Format(L" AND logscaleLogsNew.sSpcCode='%s' ",getSpcCode(m_cb6516_2.GetCurSel()));
			m_sSQLWhereClause += sSQL;
		}
		else
		{
			sSQL.Format(L" logscaleLogsNew.sSpcCode='%s' ",getSpcCode(m_cb6516_2.GetCurSel()));
			m_sSQLWhereClause = sSQL;
		}
		bFirstAdded = FALSE;
		sTmp.Format(L"1002;%d;\n",m_cb6516_2.GetCurSel());
		sSettings += sTmp;
	}	// if (m_cb6516_2.GetCurSel() > 0)

	//---------------------------------------------------------------------------
	// Kvalitet
	if (m_cb6516_3.GetCurSel() > 0 /* 0 = Empty */)
	{
		if (!bFirstAdded)
		{
			sSQL.Format(L" AND logscaleLogsNew.sGrade='%s' ",getGradesCode(m_cb6516_3.GetCurSel()));
			m_sSQLWhereClause += sSQL;
		}
		else
		{
			sSQL.Format(L" logscaleLogsNew.sGrade='%s' ",getGradesCode(m_cb6516_3.GetCurSel()));
			m_sSQLWhereClause = sSQL;
		}
		bFirstAdded = FALSE;
		sTmp.Format(L"1003;%d;\n",m_cb6516_3.GetCurSel());
		sSettings += sTmp;
	}	// if (m_cb6516_3.GetCurSel() > 0)

	//---------------------------------------------------------------------------
	// M�tare
	if (m_cb6516_4.GetCurSel() > 0 /* 0 = Empty */)
	{
		if (!bFirstAdded)
		{
			sSQL.Format(L" AND logscaleTicketsNew.sScaler='%s' ",getCBText(m_cb6516_4));
			m_sSQLWhereClause += sSQL;
		}
		else
		{
			sSQL.Format(L" logscaleTicketsNew.sScaler='%s' ",getCBText(m_cb6516_4));
			m_sSQLWhereClause = sSQL;
		}
		bFirstAdded = FALSE;
		// Add name, not index
		sTmp.Format(L"1004;%s;\n",getCBText(m_cb6516_4));
		sSettings += sTmp;
	}	// if (m_cb6516_4.GetCurSel() > 0)

	//---------------------------------------------------------------------------
	// K�pare
	if (m_cb6516_5.GetCurSel() > 0 /* 0 = Empty */)
	{
		if (!bFirstAdded)
		{
			sSQL.Format(L" AND logscaleTicketsNew.sBuyer='%s' ",getCBText(m_cb6516_5));
			m_sSQLWhereClause += sSQL;
		}
		else
		{
			sSQL.Format(L" logscaleTicketsNew.sBuyer='%s' ",getCBText(m_cb6516_5));
			m_sSQLWhereClause = sSQL;
		}
		bFirstAdded = FALSE;
		// Add name, not index
		sTmp.Format(L"1005;%s;\n",getCBText(m_cb6516_5));
		sSettings += sTmp;
	}	// if (m_cb6516_5.GetCurSel() > 0)

	//---------------------------------------------------------------------------
	// L�ngd and also check measuringmode
	CString sMacro = m_ed6516_1.getText();
	sMacro.Replace(L",",L".");
	if (!m_ed6516_1.getText().IsEmpty())
	{
		// Check that user selected a measuringmode if a length is entered.
		if (m_rb6516_1.GetCheck() == 0 && m_rb6516_2.GetCheck() == 0)
		{
			::MessageBox(GetSafeHwnd(),m_sMsgMeasuringmodeCheck,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
			return FALSE;
		}
			if (!bFirstAdded)
			{
//				sSQL.Format(L" AND logscaleLogsNew.fLengthMeas%s ",sMacro);
				sSQL.Format(L" AND %s",interpretMacro(L"logscaleLogsNew.fLengthMeas",sMacro));
				m_sSQLWhereClause += sSQL;
			}
			else
			{
//				sSQL.Format(L" logscaleLogsNew.fLengthMeas%s ",sMacro);
				sSQL.Format(L" %s",interpretMacro(L"logscaleLogsNew.fLengthMeas",sMacro));
				m_sSQLWhereClause = sSQL;
			}
			bFirstAdded = FALSE;
			sTmp.Format(L"1006;%s;\n",sMacro);
			sSettings += sTmp;
	}

	//---------------------------------------------------------------------------
	// Ticket including wildcard
	if (!m_ed6516_2.getText().IsEmpty())
	{
		sTmp = m_ed6516_2.getText();
		sTmp.Replace(L"*",L"%");
		if (!bFirstAdded)
		{
//				sSQL.Format(L" AND logscaleLogsNew.fLengthMeas%s ",sMacro);
			sSQL.Format(L" AND logscaleTicketsNew.sTicket LIKE '%s' ",sTmp);
			m_sSQLWhereClause += sSQL;
		}
		else
		{
//				sSQL.Format(L" logscaleLogsNew.fLengthMeas%s ",sMacro);
			sSQL.Format(L" logscaleTicketsNew.sTicket LIKE '%s' ",sTmp);
			m_sSQLWhereClause = sSQL;
		}
		bFirstAdded = FALSE;
		sTmp.Format(L"1007;%s;\n",m_ed6516_2.getText());
		sSettings += sTmp;
	}

	//---------------------------------------------------------------------------
	// Datum
	CString sFromDate = L"",sToDate = L"",sYearFrom = L"",sMonthFrom = L"",sDayFrom = L"",sYearTo = L"",sMonthTo = L"",sDayTo = L"",sDateFrom = L"",sDateTo = L"";
	CString sYearFrom1 = L"",sMonthFrom1 = L"",sDayFrom1 = L"",sYearTo1 = L"",sMonthTo1 = L"",sDayTo1 = L"",sDateFrom1 = L"",sDateTo1 = L"";
	// From date
	COleDateTime ctTemp;
	m_dtDate1.GetTime(ctTemp);
	if (ctTemp.GetStatus() == COleDateTime::valid)
	{
		sDayFrom.Format(_T("%02d"), ctTemp.GetDay());
		sMonthFrom.Format(_T("%02d"), ctTemp.GetMonth());
		sYearFrom.Format(_T("%04d"), ctTemp.GetYear());
		sDateFrom = m_csDate1;
		sTmp.Format(L"1010;%s;%s;%s;\n",sYearFrom,sMonthFrom,sDayFrom);
		sSettings += sTmp;
	}
	// To date
	m_dtDate2.GetTime(ctTemp);
	if (ctTemp.GetStatus() == COleDateTime::valid)
	{
		sDayTo.Format(_T("%02d"), ctTemp.GetDay());
		sMonthTo.Format(_T("%02d"), ctTemp.GetMonth());
		sYearTo.Format(_T("%04d"), ctTemp.GetYear());
		sDateTo = m_csDate2;
		sTmp.Format(L"1011;%s;%s;%s;\n",sYearTo,sMonthTo,sDayTo);
		sSettings += sTmp;
	}
	//---------------------------------------------------------------------------------------
	// ONLY FROM DATE
	if (!sDayFrom.IsEmpty() && sDayTo.IsEmpty() && !sMonthFrom.IsEmpty() && sMonthTo.IsEmpty() && !sYearFrom.IsEmpty() && sYearTo.IsEmpty())
	{
		if (!validateEnteredDate(_tstoi(sYearFrom),_tstoi(sMonthFrom),_tstoi(sDayFrom)))
		{
			::MessageBox(GetSafeHwnd(),m_sMsgDayMonthCheck,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
			return FALSE;
		}
		if (!bFirstAdded)
		{
			sSQL.Format(L" AND logscaleLogsNew.dCreated>='%s 00:00:00' ",sDateFrom);
			m_sSQLWhereClause += sSQL;
		}
		else
		{
			sSQL.Format(L" logscaleLogsNew.dCreated>='%s 00:00:00' ",sDateFrom);
			m_sSQLWhereClause = sSQL;
		}
		bFirstAdded = FALSE;
	}

	//---------------------------------------------------------------------------------------
	// ONLY TO DATE
	if (!sDayTo.IsEmpty() && sDayFrom.IsEmpty() && !sMonthTo.IsEmpty() && sMonthFrom.IsEmpty() && !sYearTo.IsEmpty() && sYearFrom.IsEmpty())
	{
		if (!validateEnteredDate(_tstoi(sYearTo),_tstoi(sMonthTo),_tstoi(sDayTo)))
		{
			::MessageBox(GetSafeHwnd(),m_sMsgDayMonthCheck,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
			return FALSE;
		}
		if (!bFirstAdded)
		{
			sSQL.Format(L" AND logscaleLogsNew.dCreated<='%s 23:59:59' ",sDateTo);
			m_sSQLWhereClause += sSQL;
		}
		else
		{
			sSQL.Format(L" logscaleLogsNew.dCreated<='%s 23:59:59' ",sDateTo);
			m_sSQLWhereClause = sSQL;
		}
		bFirstAdded = FALSE;
	}

	//---------------------------------------------------------------------------------------
	// BOTH FROM  AND TO DATE
	if (!sDayFrom.IsEmpty() && !sDayTo.IsEmpty() && !sMonthFrom.IsEmpty() && !sMonthTo.IsEmpty() && !sYearFrom.IsEmpty() && !sYearTo.IsEmpty())
	{
		if (!validateEnteredDate(_tstoi(sYearFrom),_tstoi(sMonthFrom),_tstoi(sDayFrom)) ||
			  !validateEnteredDate(_tstoi(sYearTo),_tstoi(sMonthTo),_tstoi(sDayTo)))
		{
			::MessageBox(GetSafeHwnd(),m_sMsgDayMonthCheck,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
			return FALSE;
		}

		if (!bFirstAdded)
		{
			sSQL.Format(L" AND logscaleLogsNew.dCreated BETWEEN '%s 00:00:00' AND '%s 23:59:59' ",sDateFrom,sDateTo);
			m_sSQLWhereClause += sSQL;
		}
		else
		{
			sSQL.Format(L" logscaleLogsNew.dCreated BETWEEN '%s 00:00:00' AND '%s 23:59:59' ",sDateFrom,sDateTo);
			m_sSQLWhereClause = sSQL;
		}
		bFirstAdded = FALSE;
	}
	//---------------------------------------------------------------------------
	// Tagnumber
	if (!m_ed6516_10.getText().IsEmpty())
	{
		sTmp = m_ed6516_10.getText();
		sTmp.Replace(L"*",L"%");
		if (!bFirstAdded)
		{
			sSQL.Format(L" AND logscaleLogsNew.sTagNumber LIKE '%s' ",sTmp);
			m_sSQLWhereClause += sSQL;
		}
		else
		{
			sSQL.Format(L" logscaleLogsNew.sTagNumber LIKE '%s' ",sTmp);
			m_sSQLWhereClause += sSQL;
		}
		bFirstAdded = FALSE;
		sTmp.Format(L"1101;%s;\n",m_ed6516_10.getText());
		sSettings += sTmp;
	}

	//***************************************************************************
	// Sales ID
	if (!m_ed6516_3.getText().IsEmpty())
	{
		sTmp = m_ed6516_3.getText();
		sTmp.Replace(L"*",L"%");
		if (!bFirstAddedSales)
		{
			sSQL.Format(L" AND logscaleSalesInventNew.sID LIKE '%s' ",sTmp);
			m_sSQLWhereClauseSales += sSQL;
		}
		else
		{
			sSQL.Format(L" logscaleSalesInventNew.sID LIKE '%s' ",sTmp);
			m_sSQLWhereClauseSales = sSQL;
		}
		bFirstAddedSales = FALSE;
		sTmp.Format(L"1200;%s;\n",m_ed6516_3.getText());
		sSettings += sTmp;
	}
	// From date
	m_dtDate3.GetTime(ctTemp);
	if (ctTemp.GetStatus() == COleDateTime::valid)
	{
		sDayFrom1.Format(_T("%02d"), ctTemp.GetDay());
		sMonthFrom1.Format(_T("%02d"), ctTemp.GetMonth());
		sYearFrom1.Format(_T("%04d"), ctTemp.GetYear());
		sDateFrom1 = m_csDate3;
		sTmp.Format(L"1012;%s;%s;%s;\n",sYearFrom1,sMonthFrom1,sDayFrom1);
		sSettings += sTmp;
	}
	// To date
	m_dtDate4.GetTime(ctTemp);
	if (ctTemp.GetStatus() == COleDateTime::valid)
	{
		sDayTo1.Format(_T("%02d"), ctTemp.GetDay());
		sMonthTo1.Format(_T("%02d"), ctTemp.GetMonth());
		sYearTo1.Format(_T("%04d"), ctTemp.GetYear());
		sDateTo1 = m_csDate4;
		sTmp.Format(L"1013;%s;%s;%s;\n",sYearTo1,sMonthTo1,sDayTo1);
		sSettings += sTmp;
	}

	//---------------------------------------------------------------------------------------
	// ONLY FROM DATE
	if (!sDayFrom1.IsEmpty() && sDayTo1.IsEmpty())
	{
		if (!validateEnteredDate(_tstoi(sYearFrom1),_tstoi(sMonthFrom1),_tstoi(sDayFrom1)))
		{
			::MessageBox(GetSafeHwnd(),m_sMsgDayMonthCheck,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
			return FALSE;
		}
		if (!bFirstAddedSales)
		{
			sSQL.Format(L" AND DAY(logscaleSalesInventNew.dCreated)>='%s' ",sDayFrom1);
			m_sSQLWhereClauseSales += sSQL;
		}
		else
		{
			sSQL.Format(L" DAY(logscaleSalesInventNew.dCreated)>='%s' ",sDayFrom1);
			m_sSQLWhereClauseSales = sSQL;
		}
		bFirstAddedSales = FALSE;
	}
	if (!sMonthFrom1.IsEmpty() && sMonthTo1.IsEmpty())
	{
		if (!validateEnteredDate(_tstoi(sYearFrom1),_tstoi(sMonthFrom1),_tstoi(sDayFrom1)))
		{
			::MessageBox(GetSafeHwnd(),m_sMsgDayMonthCheck,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
			return FALSE;
		}
		if (!bFirstAddedSales)
		{
			sSQL.Format(L" AND MONTH(logscaleSalesInventNew.dCreated)>='%s' ",sMonthFrom1);
			m_sSQLWhereClauseSales += sSQL;
		}
		else
		{
			sSQL.Format(L" MONTH(logscaleSalesInventNew.dCreated)>='%s' ",sMonthFrom1);
			m_sSQLWhereClauseSales = sSQL;
		}
		bFirstAddedSales = FALSE;
	}
	if (!sYearFrom1.IsEmpty() && sYearTo1.IsEmpty())
	{
		if (!bFirstAddedSales)
		{
			sSQL.Format(L" AND YEAR(logscaleSalesInventNew.dCreated)>='%s' ",sYearFrom1);
			m_sSQLWhereClauseSales += sSQL;
		}
		else
		{
			sSQL.Format(L" YEAR(logscaleSalesInventNew.dCreated)>='%s' ",sYearFrom1);
			m_sSQLWhereClauseSales = sSQL;
		}
		bFirstAddedSales = FALSE;
	}

	//---------------------------------------------------------------------------------------
	// ONLY TO DATE
	if (!sDayTo1.IsEmpty() && sDayFrom1.IsEmpty())
	{
		if (!validateEnteredDate(_tstoi(sYearTo1),_tstoi(sMonthTo1),_tstoi(sDayTo1)))
		{
			::MessageBox(GetSafeHwnd(),m_sMsgDayMonthCheck,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
			return FALSE;
		}

		if (!bFirstAddedSales)
		{
			sSQL.Format(L" AND DAY(logscaleSalesInventNew.dCreated)<='%s' ",sDayTo1);
			m_sSQLWhereClauseSales += sSQL;
		}
		else
		{
			sSQL.Format(L" DAY(logscaleSalesInventNew.dCreated)<='%s' ",sDayTo1);
			m_sSQLWhereClauseSales = sSQL;
		}
		bFirstAddedSales = FALSE;
	}
	if (!sMonthTo1.IsEmpty() && sMonthFrom1.IsEmpty())
	{
		if (!validateEnteredDate(_tstoi(sYearTo1),_tstoi(sMonthTo1),_tstoi(sDayTo1)))
		{
			::MessageBox(GetSafeHwnd(),m_sMsgDayMonthCheck,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
			return FALSE;
		}

		if (!bFirstAddedSales)
		{
			sSQL.Format(L" AND MONTH(logscaleSalesInventNew.dCreated)<='%s' ",sMonthTo1);
			m_sSQLWhereClauseSales += sSQL;
		}
		else
		{
			sSQL.Format(L" MONTH(logscaleSalesInventNew.dCreated)<='%s' ",sMonthTo1);
			m_sSQLWhereClauseSales = sSQL;
		}
		bFirstAddedSales = FALSE;
	}
	if (!sYearTo1.IsEmpty() && sYearFrom1.IsEmpty())
	{
		if (!bFirstAddedSales)
		{
			sSQL.Format(L" AND YEAR(logscaleSalesInventNew.dCreated)<='%s' ",sYearTo1);
			m_sSQLWhereClauseSales += sSQL;
		}
		else
		{
			sSQL.Format(L" YEAR(logscaleSalesInventNew.dCreated)<='%s' ",sYearTo1);
			m_sSQLWhereClauseSales = sSQL;
		}
		bFirstAddedSales = FALSE;
	}

	//---------------------------------------------------------------------------------------
	// BOTH FROM  AND TO DATE
	if (!sDayFrom1.IsEmpty() && !sDayTo1.IsEmpty() && !sMonthFrom1.IsEmpty() && !sMonthTo1.IsEmpty() && !sYearFrom1.IsEmpty() && !sYearTo1.IsEmpty())
	{
		if (!validateEnteredDate(_tstoi(sYearFrom1),_tstoi(sMonthFrom1),_tstoi(sDayFrom1)) ||
			  !validateEnteredDate(_tstoi(sYearTo1),_tstoi(sMonthTo1),_tstoi(sDayTo1)))
		{
			::MessageBox(GetSafeHwnd(),m_sMsgDayMonthCheck,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
			return FALSE;
		}

		if (!bFirstAddedSales)
		{
			sSQL.Format(L" AND logscaleSalesInventNew.dCreated BETWEEN '%s 00:00:00' AND '%s 23:59:59' ",sDateFrom1,sDateTo1);
			m_sSQLWhereClauseSales += sSQL;
		}
		else
		{
			sSQL.Format(L" logscaleSalesInventNew.dCreated BETWEEN '%s 00:00:00' AND '%s 23:59:59' ",sDateFrom1,sDateTo1);
			m_sSQLWhereClauseSales = sSQL;
		}
		bFirstAddedSales = FALSE;
	}

	//***************************************************************************

	// Save serach settings
	sPath.Format(L"%s\\%s\\search_settings.txt",getMyDocumentsDir(),SEARCH_SETTINGS_DIRECTORY);
	if (f.Open(sPath, CFile::modeWrite | CFile::modeCreate))
	{
		f.WriteString(sSettings);

		f.Close();
	}	// if (f.Open(sPath, CFile::modeWrite | CFile::modeCreate))

	return TRUE;
}

void CStorageSearchView::OnBnClickedButton65161()
{
	CString sTmp = L"";
	int nMeasMode = -1;

	if (!setupSQLWhereClause())
		return;

	if (m_pDB != NULL)
	{
		if (m_rb6516_1.GetCheck() == 1)	// Inch
		{
			nMeasMode = 0;
		}
		else if (m_rb6516_2.GetCheck() == 1)	// Metric
		{
			nMeasMode = 1;
		}

		if (m_pDB->runSearchQuestion(m_vecSearch,m_sSQLWhereClause,nMeasMode,m_LanguageSet) &&
				m_pDB->runSearchQuestionSales(m_vecSearchSales,m_sSQLWhereClauseSales))
		{
			// Try to match m_vecSearchSales hits (name) into m_vecSerach.
			// If m_sSQLWhereClauseSales isn't empty only display hits
			// based on Sales search
			m_vecSearchPopulate.clear();
			if (m_sSQLWhereClauseSales.IsEmpty())
			{
				for (int i1 = 0;i1 < m_vecSearch.size();i1++)
				{
					for (int i2 = 0;i2 < m_vecSearchSales.size();i2++)
					{
						if (m_vecSearchSales[i2].getLogID() == m_vecSearch[i1].getLogID() &&
								m_vecSearchSales[i2].getTicketID() == m_vecSearch[i1].getTicketID())
						{
							m_vecSearch[i1].setSalesName(m_vecSearchSales[i2].getSales());
							m_vecSearch[i1].setInventID(m_vecSearchSales[i2].getInvID());
						}
					}	// for (int i2 = 0;i2 < m_vecSearch.size();i2++)

					m_vecSearchPopulate.push_back(m_vecSearch[i1]);
				}	// for (int i1 = 0;i1 < m_vecSearch.size();i1++)
			}
			else
			{
				for (int i1 = 0;i1 < m_vecSearch.size();i1++)
				{
					for (int i2 = 0;i2 < m_vecSearchSales.size();i2++)
					{
						if (m_vecSearchSales[i2].getLogID() == m_vecSearch[i1].getLogID() &&
								m_vecSearchSales[i2].getTicketID() == m_vecSearch[i1].getTicketID())
						{
							m_vecSearch[i1].setSalesName(m_vecSearchSales[i2].getSales());
							m_vecSearch[i1].setInventID(m_vecSearchSales[i2].getInvID());
							m_vecSearchPopulate.push_back(m_vecSearch[i1]);
						}
					}	// for (int i2 = 0;i2 < m_vecSearch.size();i2++)
				}	// for (int i1 = 0;i1 < m_vecSearch.size();i1++)
			}


			populateReport();


			if ((m_tabManager = m_wndTabControl.getTabPage(0) )!= NULL)
			{
				sTmp.Format(L"%s " + m_sNumOfHits,m_sTabCaption,m_vecSearchPopulate.size());
				m_tabManager->SetCaption(sTmp);
			}
		}
	}

}


void CStorageSearchView::OnBnClickedButton65162()
{
	// Reset data
	m_rb6516_1.SetCheck(0);
	m_rb6516_2.SetCheck(0);
	m_rb6516_3.SetCheck(1);

	m_cb6516_1.SetCurSel(0);
	m_cb6516_2.SetCurSel(0);
	m_cb6516_3.SetCurSel(0);
	m_cb6516_4.SetCurSel(0);
	m_cb6516_5.SetCurSel(0);
	m_ed6516_1.SetWindowTextW(L"");
	m_ed6516_2.SetWindowTextW(L"");
	m_ed6516_3.SetWindowTextW(L"");

	COleDateTime dt; 
	dt.SetStatus(COleDateTime::null); 
	m_dtDate1.SetTime(dt); 
	m_dtDate2.SetTime(dt); 
	m_dtDate3.SetTime(dt); 
	m_dtDate4.SetTime(dt); 

	m_ed6516_10.SetWindowTextW(L"");



	m_tabManager = m_wndTabControl.getTabPage(0);
	if (m_tabManager)
	{
		CStorageSearchReportView* pView = DYNAMIC_DOWNCAST(CStorageSearchReportView, CWnd::FromHandle(m_tabManager->GetHandle()));
		ASSERT_KINDOF(CStorageSearchReportView, pView);
		pView->GetReportCtrl().ResetContent();
		m_tabManager->SetCaption(m_sTabCaption);
	}

}


BOOL CStorageSearchView::AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon)
{
	CCreateContext contextT;
	contextT.m_pCurrentDoc     = GetDocument();
	contextT.m_pNewViewClass   = pViewClass;
	contextT.m_pNewDocTemplate = GetDocument()->GetDocTemplate();

	CWnd* pWnd;
	TRY
	{
		pWnd = (CWnd*)pViewClass->CreateObject();
		if (pWnd == NULL)
		{
			AfxThrowMemoryException();
		}
	}
	CATCH_ALL(e)
	{
		TRACE0( "Out of memory creating a view.\n" );
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;

	int nTab = m_wndTabControl.GetItemCount();

	// Create with the right size (wrong position)
	CRect rect(0,0,0,0);
	if (!pWnd->Create(NULL, NULL, dwStyle,
		rect, &m_wndTabControl, (AFX_IDW_PANE_FIRST + nTab), &contextT))
	{
		TRACE0( "Warning: couldn't create client tab for view.\n" );
		// pWnd will be cleaned up by PostNcDestroy
		return NULL;
	}
	m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);

	//pWnd->SendMessage(WM_INITIALUPDATE);
	pWnd->SetOwner(this);

	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CStorageSearchReportFilterEditControl

IMPLEMENT_DYNCREATE(CStorageSearchReportFilterEditControl, CXTPReportFilterEditControl)

BEGIN_MESSAGE_MAP(CStorageSearchReportFilterEditControl, CXTPReportFilterEditControl)
	ON_WM_KEYUP()
END_MESSAGE_MAP()

void CStorageSearchReportFilterEditControl::OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	CString S;
	CStorageSearchFrame* pWnd = (CStorageSearchFrame *)getFormViewParentByID(IDD_REPORTVIEW6516);
	if (pWnd != NULL)
	{
		GetWindowText(S);
		pWnd->setEnableTBBTNFilterOff(S != "");
	}

	CXTPReportFilterEditControl::OnKeyUp(nChar,nRepCnt,nFlags);
}

// CStorageSearchReportView

IMPLEMENT_DYNCREATE(CStorageSearchReportView,  CXTPReportView) //CXTResizeFormView)

BEGIN_MESSAGE_MAP(CStorageSearchReportView,  CXTPReportView) //CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_NOTIFY(NM_CLICK, XTP_ID_REPORT_CONTROL, OnReportItemClick)
	ON_NOTIFY(NM_DBLCLK, XTP_ID_REPORT_CONTROL, OnReportItemDblClick)
	ON_NOTIFY(XTP_NM_REPORT_HEADER_RCLICK, XTP_ID_REPORT_CONTROL, OnReportColumnRClick)
	ON_COMMAND(ID_SHOW_FIELD_SELECTION, OnShowFieldChooser)
	ON_COMMAND(ID_BUTTON32821, OnExportToExcel)
	ON_COMMAND(ID_BUTTON32822, OnPrintPreview)
	ON_COMMAND(ID_BUTTON32823, OnShowFieldFilter)
	ON_COMMAND(ID_BUTTON32824, OnShowFieldFilterOff)
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
END_MESSAGE_MAP()

CStorageSearchReportView::CStorageSearchReportView()
	: CXTPReportView()
{
	m_pDB = NULL;
	m_nSelectedColumn = -1;
}

CStorageSearchReportView::~CStorageSearchReportView()
{
}

void CStorageSearchReportView::OnInitialUpdate()
{
	CXTPReportView::OnInitialUpdate();

	setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

	CStorageSearchFrame* pWnd = (CStorageSearchFrame *)getFormViewParentByID(IDD_REPORTVIEW6516);
	if (m_wndSubList.GetSafeHwnd() == NULL)
	{
		m_wndSubList.SubclassDlgItem(IDC_COLUMNS4, &pWnd->m_wndFieldChooserDlg);
		GetReportCtrl().GetColumns()->GetReportHeader()->SetSubListCtrl(&m_wndSubList);
	}

	if (m_wndFilterEdit.GetSafeHwnd() == NULL)
	{
		m_wndFilterEdit.SubclassDlgItem(IDC_FILTEREDIT20_1, &pWnd->m_wndFilterEdit);
		GetReportCtrl().GetColumns()->GetReportHeader()->SetFilterEditCtrl(&m_wndFilterEdit);
	}

	if (m_lbl20_1.GetSafeHwnd() == NULL)
	{
		m_lbl20_1.SubclassDlgItem(IDC_FILTERLBL20_1, &pWnd->m_wndFilterEdit);
		m_lbl20_1.SetBkColor(INFOBK);
	}

	if (m_lbl20_2.GetSafeHwnd() == NULL)
	{
		m_lbl20_2.SubclassDlgItem(IDC_FILTERLBL20_2, &pWnd->m_wndFilterEdit);
		m_lbl20_2.SetBkColor(INFOBK);
		m_lbl20_2.SetLblFont(14,FW_BOLD);
	}

	setupReport();

	LoadReportState();

}

BOOL CStorageSearchReportView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData) 
{
	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		if (m_dbConnectionData.conn->isConnected())
		{
			if ((m_pDB = new CDBHandling(m_dbConnectionData)) != NULL)
			{
			}

		}
	}
	return CXTPReportView::OnCopyData(pWnd, pData);
}

void CStorageSearchReportView::OnDestroy()
{
	SaveReportState();
	// Try to clear records on exit (for memory deallocation); 120503 p�d
	CXTPReportRecords *pRecs = GetReportCtrl().GetRecords();
	if (pRecs != NULL)
	{
		pRecs->RemoveAll();
	}

	if (m_pDB != NULL)
		delete m_pDB;


	CXTPReportView::OnDestroy();	
}

BOOL CStorageSearchReportView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
//	if( !CXTResizeFormView::PreCreateWindow(cs) )
//		return FALSE;
	if( ! CXTPReportView::PreCreateWindow(cs) )
		return FALSE;
	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

// CStorageSearchReportView diagnostics

#ifdef _DEBUG
void CStorageSearchReportView::AssertValid() const
{
	CXTPReportView::AssertValid();
}

void CStorageSearchReportView::Dump(CDumpContext& dc) const
{
	CXTPReportView::Dump(dc);
//	CXTResizeFormView::Dump(dc);
}
#endif //_DEBUG


// CStorageSearchReportView message handlers

// CStorageSearchReportView message handlers
void CStorageSearchReportView::OnSize(UINT nType,int cx,int cy)
{
	CXTPReportView::OnSize(nType,cx,cy);
}

void CStorageSearchReportView::OnSetFocus(CWnd*)
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
}

// Create and add Assortment settings reportwindow
BOOL CStorageSearchReportView::setupReport(void)
{
	CString sColText;
	int nNumOfTabs = 0;
	CXTPReportColumn *pCol = NULL;

	GetReportCtrl().ShowGroupBy(TRUE);
	// Add these 3 lines to add scrollbars for View; 070319 p�d

	GetReportCtrl().GetReportHeader()->SetAutoColumnSizing( FALSE );
	GetReportCtrl().EnableScrollBar(SB_HORZ, TRUE );
	GetReportCtrl().EnableScrollBar(SB_VERT, TRUE );

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader *xml = new RLFReader;
		if (xml->Load(m_sLangFN))
		{
				m_sGroupByThisField	= (xml->str(IDS_STRING5957));
				m_sGroupByBox				= (xml->str(IDS_STRING5956));
				m_sFieldChooser			= (xml->str(IDS_STRING5955));

				m_sFilterOn					= (xml->str(IDS_STRING5951));

				m_sMsgCap = xml->str(IDS_STRING99);
				m_sMsgOpenEXCEL	= xml->str(IDS_STRING60253);
				m_sTabCaption = xml->str(IDS_STRING60113);

				// Get text from languagefile; 061207 p�d
				if (GetReportCtrl().GetSafeHwnd() != NULL)
				{

					VERIFY(m_ilIcons.Create(16,13, ILC_COLOR24|ILC_MASK, 0, 1));
					CBitmap bmp;
					VERIFY(bmp.LoadBitmap(IDB_BITMAP1));
					m_ilIcons.Add(&bmp, RGB(255, 0, 255));

					GetReportCtrl().SetImageList(&m_ilIcons);

					GetReportCtrl().ShowWindow( SW_NORMAL );

					// Tickets
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_0, (xml->str(IDS_STRING60100)), 100));
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
					// Tagnummer
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_1, (xml->str(IDS_STRING60101)), 120));
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
					// Sales
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_2, (xml->str(IDS_STRING60128)), 100));
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
					// Status
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_3, (xml->str(IDS_STRING60102)), 100));
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
					// Tr�dslag kod
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_4, (xml->str(IDS_STRING60103)), 100));
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
					// Tr�dslag Namn
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_5, (xml->str(IDS_STRING60126)), 100));
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
					// Kvalitet
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_6, (xml->str(IDS_STRING60104)), 100));
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
					// Volym
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_7, (xml->str(IDS_STRING60105)), 100));
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
					// Pris
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_8, (xml->str(IDS_STRING60106)), 100));
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
					// Funktion
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_9, (xml->str(IDS_STRING60107)), 100));
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
					// L�ngd
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_10, (xml->str(IDS_STRING60108)), 100));
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
					// Lagerdatum
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_11, (xml->str(IDS_STRING60109)), 100));
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );
					// Uppdateringsdatum
					pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_12, (xml->str(IDS_STRING60110)), 100));
					pCol->SetHeaderAlignment( DT_CENTER );
					pCol->SetAlignment( DT_CENTER );

					GetReportCtrl().GetReportHeader()->AllowColumnRemove(TRUE);
					GetReportCtrl().SetMultipleSelection( FALSE );
					GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );
					GetReportCtrl().SetGridStyle( FALSE, xtpReportGridSmallDots );
					GetReportCtrl().FocusSubItems(TRUE);
					GetReportCtrl().AllowEdit(FALSE);
					GetReportCtrl().GetPaintManager()->SetFixedRowHeight(FALSE);

				}	// if (GetReportCtrl().GetSafeHwnd() != NULL)
			}	// if (xml->Load(m_sLangFN))
			delete xml;
		}	// if (fileExists(m_sLangFN))
	return TRUE;
}

void CStorageSearchReportView::setFilterWindow(void)
{
	m_lbl20_1.SetWindowText(m_sFilterOn + _T(" :"));
	if (m_nSelectedColumn > -1 && m_nSelectedColumn < GetReportCtrl().GetColumns()->GetCount())
	{
		CXTPReportColumns *pCols = GetReportCtrl().GetColumns();
		CXTPReportColumn *pColumn = pCols->GetAt(m_nSelectedColumn);
		int nColumn = pColumn->GetIndex();
		if (pCols && nColumn < pCols->GetCount())
		{
			for (int i = 0;i < pCols->GetCount();i++)
			{
				pCols->GetAt(i)->SetFiltrable( i == nColumn );
			}	// for (int i = 0;i < pCols->GetCount();i++)
		}	// if (pCols && nColumn < pCols->GetCount())
		m_lbl20_2.SetWindowText(pColumn->GetCaption());
	}	// if (m_nSelectedColumn > -1 && m_nSelectedColumn < GetReportCtrl().GetColumns()->GetCount())
	else
		m_lbl20_2.SetWindowText(L"");
}

void CStorageSearchReportView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	CRect rect;
	POINT pt;
	CSearchReportRec *pRec = NULL;

	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;

	// prevents us from crashing when clicking inside the report, but not on an item
	if(pItemNotify == NULL) 
		return;

	// prevents us from crashing when clicking inside the report, but not on an item
	if(pItemNotify->pItem == NULL)
		return;


	if (pItemNotify->pColumn)
	{
		m_nSelectedColumn = pItemNotify->pColumn->GetIndex();
	}

	// Update filter column, if filterwindow is visible; 090224 p�d
	CStorageSearchFrame* pWnd = (CStorageSearchFrame *)getFormViewParentByID(IDD_REPORTVIEW6516);
	if (pWnd != NULL)
	{
		if (pWnd->m_wndFilterEdit.IsVisible())
		{
			setFilterWindow();
			pWnd->setEnableTBBTNFilterOff(GetReportCtrl().GetFilterText() != _T(""));
		}	// if (pWnd->m_wndFilterEdit.IsVisible())
	}	// if (pWnd != NULL)

	switch (pItemNotify->pColumn->GetIndex())
	{

		// �ppna Ticket logs
		case COLUMN_0 :
			{
				// Do a hit-test; 080513 p�d
				rect = pItemNotify->pColumn->GetRect();		
				pt = pItemNotify->pt;
				// Check if the user clicked on the Icon or not; 080513 p�d
				if (hitTest_X(pt.x,rect.left,13))
				{
					if ((pRec = (CSearchReportRec *)pItemNotify->pRow->GetRecord()) != NULL)
					{
						if (!pRec->getIconColText(COLUMN_0).IsEmpty())
						{
							showFormView(IDD_FORMVIEW5,m_sLangFN,(LPARAM)&pRec->getRecord(),ID_OPEN_LOGS_SEARCH);
						}
					}
				}
			}
			break;
		// �ppna Sales logs
		case COLUMN_2 :
			{
				// Do a hit-test; 080513 p�d
				rect = pItemNotify->pColumn->GetRect();		
				pt = pItemNotify->pt;
				// Check if the user clicked on the Icon or not; 080513 p�d
				if (hitTest_X(pt.x,rect.left,13))
				{
					if ((pRec = (CSearchReportRec *)pItemNotify->pRow->GetRecord()) != NULL)
					{
						if (!pRec->getIconColText(COLUMN_2).IsEmpty())
						{
							showFormView(IDD_FORMVIEW6,m_sLangFN,(LPARAM)&pRec->getRecord(),ID_OPEN_SALE_SEARCH);
						}
					}
				}
			}
			break;
	};


}

void CStorageSearchReportView::OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
}

void CStorageSearchReportView::OnExportToExcel()
{
	int f[4];
	CString sTmp = L"",sPath = L"";
	CSearchReportRec *pRec = NULL;
	int nColCnt = 0,nLangSet = -1;
	
	sPath.Format(L"%s%s\\",getMyDocumentsDir(),SEARCH_SETTINGS_DIRECTORY);
	CFileDialog dlg(FALSE,L"xls",L"*.xls",OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,L"EXCEL *.xls|*.xls|");
	dlg.m_ofn.lpstrInitialDir = sPath;
	if (dlg.DoModal() != IDOK) return;

	CStorageSearchView* pView = (CStorageSearchView *)getFormViewByID(IDD_REPORTVIEW6516);
	if (pView != NULL)
	{
		nLangSet = pView->getLanguageSet();
	}	// if (pWnd != NULL)

	// Check Columns in report and it's order
	CXTPReportColumns *pCols = GetReportCtrl().GetColumns();
	CXTPReportRows *pRows = GetReportCtrl().GetRows();
	if (pCols != NULL && pRows != NULL)
	{
		Book* book = xlCreateBook();
		if(book)
		{
			// Set registraton-key
			book->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0hfeflb");
			f[0] = book->addCustomNumFormat(L"0.0");
			f[1] = book->addCustomNumFormat(L"0.00");
			f[2] = book->addCustomNumFormat(L"0.000");

			Format* fmt[5];
			libxl::Font *fnt = book->addFont();
			fnt->setSize(8);
			fnt->setBold(true);
			libxl::Font *fnt1 = book->addFont();
			fnt1->setSize(12);
			fnt1->setBold(true);
			fmt[0] = book->addFormat();
			fmt[0]->setWrap();
			fmt[0]->setFont(fnt);
			fmt[1] = book->addFormat();
			fmt[1]->setFont(fnt1);
			fmt[2] = book->addFormat();
			fmt[2]->setNumFormat(f[0]);
			fmt[3] = book->addFormat();
			fmt[3]->setNumFormat(f[1]);
			fmt[4] = book->addFormat();
			fmt[4]->setNumFormat(f[2]);
			Sheet* sheet = book->addSheet(L"Sheet1");
			if(sheet)
			{
				sheet->writeStr(0, 0,m_sTabCaption + L" " + getDateExt(nLangSet),fmt[1]);
				// Setup column headlines
				for (int col = 0;col < pCols->GetCount();col++)
				{
					if (pCols->GetAt(col)->IsVisible())
					{
						sheet->writeStr(2, nColCnt,pCols->GetAt(col)->GetCaption(),fmt[0]);

						for (int row = 0;row < pRows->GetCount();row++)
						{

							if ((pRec = (CSearchReportRec*)pRows->GetAt(row)->GetRecord()) != NULL)
							{
								if (pCols->GetAt(col)->GetItemIndex() == 7)
								{
									sheet->writeNum(row+3,nColCnt,_tstof(pRec->getColText(pCols->GetAt(col)->GetItemIndex())),fmt[2]);
								}
								else if (pCols->GetAt(col)->GetItemIndex() == 8)
								{
									sheet->writeNum(row+3,nColCnt,_tstof(pRec->getColText(pCols->GetAt(col)->GetItemIndex())),fmt[3]);
								}
								else if (pCols->GetAt(col)->GetItemIndex() == 10)
								{
									sheet->writeNum(row+3,nColCnt,pRec->getColFloat(pCols->GetAt(col)->GetItemIndex()),fmt[3]);
								}
								else if (pCols->GetAt(col)->GetItemIndex() == 0 || pCols->GetAt(col)->GetItemIndex() == 2)
								{
									sheet->writeStr(row+3,nColCnt,pRec->getIconColText(pCols->GetAt(col)->GetItemIndex()));
								}
								else
								{
									sheet->writeStr(row+3,nColCnt,pRec->getColText(pCols->GetAt(col)->GetItemIndex()));
								}
							}	// if ((pRec = (CSearchReportRec*)pRows->GetAt(row)->GetRecord()) != NULL)
						}	// for (int row = 0;row < pRows->GetCount();row++)
						nColCnt++;
					}	// if (pCols->GetAt(col)->IsVisible())
				}	// // for (int i = 0;i < pCols->GetCount();i++)
			}


			if(book->save(dlg.GetPathName())) 
			{
	#ifdef _OPEN_EXCEL_IN_TICKET

				//*	Uncomment to open excel after saving file
					if (::MessageBox(GetSafeHwnd(),m_sMsgOpenEXCEL,m_sMsgCap,MB_ICONQUESTION | MB_YESNO) == IDYES)
						::ShellExecute(NULL, L"open", dlg.GetPathName(), NULL, NULL, SW_SHOW);        
				//*/
	#endif
			}
			else
			{
				CString S = L"";
				S.Format(L"%S",book->errorMessage());
				AfxMessageBox(S);
			}
			
			book->release();		
		
		}	// if(book)

	}	// if (pCols != NULL)
}

void CStorageSearchReportView::OnShowFieldChooser()
{
	CStorageSearchFrame* pWnd = (CStorageSearchFrame *)getFormViewParentByID(IDD_REPORTVIEW6516);
	if (pWnd != NULL)
	{
		BOOL bShow = !pWnd->m_wndFieldChooserDlg.IsVisible();
		pWnd->ShowControlBar(&pWnd->m_wndFieldChooserDlg, bShow, FALSE);
	}	// if (pWnd != NULL)
}

void CStorageSearchReportView::OnShowFieldFilter()
{
	CStorageSearchFrame* pWnd = (CStorageSearchFrame *)getFormViewParentByID(IDD_REPORTVIEW6516);
	if (pWnd != NULL)
	{
		setFilterWindow();
		BOOL bShow = !pWnd->m_wndFilterEdit.IsVisible();
		pWnd->ShowControlBar(&pWnd->m_wndFilterEdit, bShow, FALSE);
		pWnd->setEnableTBBTNFilterOff(GetReportCtrl().GetFilterText() != _T(""));
	}

}

void CStorageSearchReportView::OnShowFieldFilterOff()
{
	GetReportCtrl().SetFilterText(_T(""));
	GetReportCtrl().Populate();
	m_wndFilterEdit.SetWindowText(_T(""));
	CStorageSearchFrame* pWnd = (CStorageSearchFrame *)getFormViewParentByID(IDD_REPORTVIEW6516);
	if (pWnd != NULL)
	{
		pWnd->setEnableTBBTNFilterOff(FALSE);
	}	// if (pWnd != NULL)
}

void CStorageSearchReportView::OnReportColumnRClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	ASSERT(pItemNotify->pColumn);
	CPoint ptClick = pItemNotify->pt;

	CMenu menu;
	VERIFY(menu.CreatePopupMenu());

	// create main menu items
	menu.AppendMenu(MF_SEPARATOR, (UINT)-1, (LPCTSTR)NULL);
	menu.AppendMenu(MF_STRING, ID_GROUP_BYTHIS, m_sGroupByThisField);
	menu.AppendMenu(MF_STRING, ID_SHOW_GROUPBOX, m_sGroupByBox);
	menu.AppendMenu(MF_SEPARATOR, (UINT)-1, (LPCTSTR)NULL);
	menu.AppendMenu(MF_STRING, ID_SHOW_FIELDCHOOSER, m_sFieldChooser);

	if (GetReportCtrl().GetReportHeader()->IsShowItemsInGroups())
	{
		menu.CheckMenuItem(ID_GROUP_BYTHIS, MF_BYCOMMAND|MF_CHECKED);
	}

	if (GetReportCtrl().IsGroupByVisible())
	{
		menu.CheckMenuItem(ID_SHOW_GROUPBOX, MF_BYCOMMAND|MF_CHECKED);
	}

	CXTPReportColumns* pColumns = GetReportCtrl().GetColumns();
	CXTPReportColumn* pColumn = pItemNotify->pColumn;
	m_nSelectedColumn = pItemNotify->pColumn->GetIndex();

	// track menu
	int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN |TPM_RIGHTBUTTON, ptClick.x, ptClick.y, this, NULL);

	// other general items
	switch (nMenuResult)
	{
		case ID_GROUP_BYTHIS:

			if (pColumns->GetGroupsOrder()->IndexOf(pColumn) < 0)
			{
				pColumns->GetGroupsOrder()->Add(pColumn);
			}
			GetReportCtrl().GetReportHeader()->ShowItemsInGroups(!GetReportCtrl().GetReportHeader()->IsShowItemsInGroups());
			GetReportCtrl().Populate();
			break;
		case ID_SHOW_GROUPBOX:
			GetReportCtrl().ShowGroupBy(!GetReportCtrl().IsGroupByVisible());
			break;
		case ID_SHOW_FIELDCHOOSER:
			OnShowFieldChooser();
			break;
	}

}

void CStorageSearchReportView::OnPrintPreview()
{
	OnFilePrint();
/*
	// In derived classes, implement special window handling here
	// Be sure to Unhook Frame Window close if hooked.

	// must not create this on the frame.  Must outlive this function
	CPrintPreviewState* pState = new CPrintPreviewState;

	// DoPrintPreview's return value does not necessarily indicate that
	// Print preview succeeded or failed, but rather what actions are necessary
	// at this point.  If DoPrintPreview returns TRUE, it means that
	// OnEndPrintPreview will be (or has already been) called and the
	// pState structure will be/has been deleted.
	// If DoPrintPreview returns FALSE, it means that OnEndPrintPreview
	// WILL NOT be called and that cleanup, including deleting pState
	// must be done here.
	if ( !DoPrintPreview( AFX_IDD_PREVIEW_TOOLBAR, this,
		RUNTIME_CLASS( CLangPreviewView ), pState ))
	{
		// In derived classes, reverse special window handling here for
		// Preview failure case
		TRACE0( "Error: DoPrintPreview failed.\n" );
		AfxMessageBox( AFX_IDP_COMMAND_FAILURE );
		delete pState;      // preview failed to initialize, delete State now

	}
*/
}

void CStorageSearchReportView::OnFilePrint()
{
	CXTPReportView::OnFilePrint();
}


void CStorageSearchReportView::OnRefresh()
{
	if (m_pDB != NULL)
	{
//		populateReport();
	}
}

// CStorageSearchReportView message handlers

void CStorageSearchReportView::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (!AfxGetApp()->GetProfileBinary((REG_WP_STORAGE_SEARCH_STATE_KEY), _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		GetReportCtrl().SerializeState(ar);
	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;
	// Get filtertext for this Report
	sFilterText = AfxGetApp()->GetProfileString(REG_WP_STORAGE_SEARCH_STATE_KEY, _T("FilterText"), _T(""));
	// Get selected column index into registry; 070125 p�d
	m_nSelectedColumn = AfxGetApp()->GetProfileInt(REG_WP_STORAGE_SEARCH_STATE_KEY, _T("SelColIndex"),0);

	GetReportCtrl().SetFilterText(sFilterText);
	GetReportCtrl().Populate();
	m_wndFilterEdit.SetWindowText(sFilterText);

	CStorageSearchFrame* pWnd = (CStorageSearchFrame *)getFormViewParentByID(IDD_REPORTVIEW6516);
	if (pWnd != NULL)
	{
		pWnd->setEnableTBBTNFilterOff(GetReportCtrl().GetFilterText() != _T(""));
	}
}

void CStorageSearchReportView::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	GetReportCtrl().SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary(REG_WP_STORAGE_SEARCH_STATE_KEY, _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);

	sFilterText = GetReportCtrl().GetFilterText();
	AfxGetApp()->WriteProfileString(REG_WP_STORAGE_SEARCH_STATE_KEY, _T("FilterText"), sFilterText);

	// Set selected column index into registry; 070125 p�d
	AfxGetApp()->WriteProfileInt(REG_WP_STORAGE_SEARCH_STATE_KEY, _T("SelColIndex"), m_nSelectedColumn);

}

