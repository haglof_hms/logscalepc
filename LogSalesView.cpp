#include "stdafx.h"
#include "resource.h"

#include "logsalesview.h"

#include "ChangeStatusDlg.h"

#include "ResLangFileReader.h"

#include "ViewProgress.h"

#include "libxl.h"

#include "ExcelExportDlg.h"

using namespace libxl;

extern CString m_sShellDataFile;

/////////////////////////////////////////////////////////////////////////////
// CSalesReportFilterEditControl

IMPLEMENT_DYNCREATE(CSalesReportFilterEditControl, CXTPReportFilterEditControl)

BEGIN_MESSAGE_MAP(CSalesReportFilterEditControl, CXTPReportFilterEditControl)
	ON_WM_KEYUP()
END_MESSAGE_MAP()

void CSalesReportFilterEditControl::OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags)
{
	CString S;
	CLogScaleSalesFrame* pWnd = (CLogScaleSalesFrame *)getFormViewParentByID(IDD_FORMVIEW11);
	if (pWnd != NULL)
	{
		GetWindowText(S);
		pWnd->setEnableFilterOff(S != "");
	}

	CXTPReportFilterEditControl::OnKeyUp(nChar,nRepCnt,nFlags);
}

///////////////////////////////////////////////////////////////////////////////////////////
// CLogScaleSalesDoc


IMPLEMENT_DYNCREATE(CLogScaleSalesDoc, CDocument)

BEGIN_MESSAGE_MAP(CLogScaleSalesDoc, CDocument)
	//{{AFX_MSG_MAP(CLogScaleSalesDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLogScaleSalesDoc construction/destruction

CLogScaleSalesDoc::CLogScaleSalesDoc()
{
	// TODO: add one-time construction code here

}

CLogScaleSalesDoc::~CLogScaleSalesDoc()
{
}


BOOL CLogScaleSalesDoc::OnNewDocument()
{
	// CHECK FOR LICENSE HERE!!!!! 2011-08-24 P�D
	if (!License())
	{
		return FALSE;
	}

	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CLogScaleSalesDoc serialization

void CLogScaleSalesDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CLogScaleSalesDoc diagnostics

#ifdef _DEBUG
void CLogScaleSalesDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CLogScaleSalesDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


///////////////////////////////////////////////////////////////////////////////////////////
// CLogScaleSalesFrame

IMPLEMENT_DYNCREATE(CLogScaleSalesFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CLogScaleSalesFrame, CMDIChildWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_SYSCOMMAND()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)

	ON_COMMAND(ID_TBBTN_PRINT, OnTBBtnPrintOut)
	ON_UPDATE_COMMAND_UI(ID_TBBTN_PRINT, OnUpdatePrintOutTBtn)

	ON_CONTROL(CBN_SELCHANGE,ID_TBBTN_REPORTCBOX, OnPrintOutCBox)

	ON_UPDATE_COMMAND_UI(ID_TBBTN_DEL_FILTER, OnUpdateFilterOff)

	ON_XTP_CREATECONTROL()
END_MESSAGE_MAP()


// CLogScaleSalesFrame construction/destruction

XTPDockingPanePaintTheme CLogScaleSalesFrame::m_themeCurrent = xtpPaneThemeOffice2003;

CLogScaleSalesFrame::CLogScaleSalesFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
	m_bInitReports = FALSE;
	//m_sShellDataFile = L"";
	m_nShellDataIndex = -1;
	m_bIsPrintOutTBtn = FALSE;
}

CLogScaleSalesFrame::~CLogScaleSalesFrame()
{
}

void CLogScaleSalesFrame::OnDestroy(void)
{
	if (m_fnt1)
		delete m_fnt1;
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6515_KEY);
	SavePlacement(this, csBuf);

}

void CLogScaleSalesFrame::OnClose(void)
{
	CMDIChildWnd::OnClose();
}

void CLogScaleSalesFrame::OnSysCommand(UINT nID,LPARAM lParam)
{
	if ((nID & 0xFFF0) == SC_CLOSE)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
		CMDIChildWnd::OnSysCommand(nID,lParam);
	}
	else
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
		CMDIChildWnd::OnSysCommand(nID,lParam);
	}
}

int CLogScaleSalesFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	EnableDocking(CBRS_ALIGN_ANY);

	// Create and Load toolbar; 090107 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR1);
	m_wndToolBar.EnableDocking(xtpFlagAlignTop|xtpFlagStretched);

	// Initialize dialog bar m_wndFieldSelectionDlg
	if (!m_wndFieldSelectionDlg.Create(this, IDD_FIELD_SELECTION,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN_SELECTION_DLG))
	{
		return -1;      // fail to create
	}

	// Initialize dialog bar m_wndFilterEdit
	if (!m_wndFilterEdit.Create(this, IDD_FILTEREDIT20,
		CBRS_LEFT|CBRS_TOOLTIPS|CBRS_FLYBY|CBRS_HIDE_INPLACE, ID_TBBTN_FILTER))
	{
		return -1;      // fail to create
	}

	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_wndFieldSelectionDlg.SetWindowText(xml.str(IDS_STRING1100));

			//***************************************************************************************
			// Setup commandbars and manues; 051114 p�d
			CXTPToolBar* pToolBar = &m_wndToolBar;
			if (pToolBar->IsBuiltIn())
			{
				if (pToolBar->GetType() != xtpBarTypeMenuBar)
				{
					UINT nBarID = pToolBar->GetBarID();
					pToolBar->LoadToolBar(nBarID, FALSE);
					CXTPControls *p = pToolBar->GetControls();
					// Setup icons on toolbars, using resource dll; 051208 p�d
					if (nBarID == IDR_TOOLBAR1)
					{		
						setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_NEW,xml.str(IDS_STRING3550),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(2),RES_TB_DEL,xml.str(IDS_STRING3551),TRUE);	//

						setToolbarBtn(sTBResFN,p->GetAt(3),RES_TB_IMPORT,xml.str(IDS_STRING3553),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(4),RES_TB_EXPORT,xml.str(IDS_STRING3554),TRUE);	//

						setToolbarBtn(sTBResFN,p->GetAt(5),RES_TB_PRINT,L"CBOX",TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(6),RES_TB_PRINT,xml.str(IDS_STRING3552),TRUE);	//

						setToolbarBtn(sTBResFN,p->GetAt(7),RES_TB_FILTER,xml.str(IDS_STRING1108),TRUE);	//
						//p->GetAt(7)->SetVisible(FALSE);
						setToolbarBtn(sTBResFN,p->GetAt(8),RES_TB_FILTER_OFF,xml.str(IDS_STRING1109),TRUE);	//
						//p->GetAt(8)->SetVisible(FALSE);

						p->GetAt(1)->SetVisible(FALSE);
						p->GetAt(9)->SetVisible(FALSE);
					}	// if (nBarID == IDR_TOOLBAR1)
				}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
			}	// if (pToolBar->IsBuiltIn())
			//***************************************************************************************
			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

	// docking for field chooser
	m_wndFieldSelectionDlg.EnableDocking(0);

	// Don't show fieldchooser at this stadge
	ShowControlBar(&m_wndFieldSelectionDlg, FALSE, FALSE);
	FloatControlBar(&m_wndFieldSelectionDlg, CPoint(100, GetSystemMetrics(SM_CYSCREEN) / 3));

	// docking for filter editing
	m_wndFilterEdit.EnableDocking(CBRS_ALIGN_TOP);

	ShowControlBar(&m_wndFilterEdit, FALSE, FALSE);
	FloatControlBar(&m_wndFilterEdit, CPoint(400, GetSystemMetrics(SM_CYSCREEN) / 3));

	// We'll chack if the directory for columnsettings is created
	CString sDirectory;
	sDirectory.Format(L"%s\\%s",getMyDocumentsDir(),SALES_COLUMNS_DIRECTORY);
	// Check if directory exists, on disk. If not create it; 080205 p�d
	if (!isDirectory(sDirectory))
	{
		createDirectory(sDirectory);
	}

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

int CLogScaleSalesFrame::OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl)
{
	if (lpCreateControl->nID == ID_TBBTN_REPORTCBOX)
	{
		if (!m_cboxPrintOut.Create(WS_CHILD|WS_VISIBLE|CBS_DROPDOWNLIST|WS_CLIPCHILDREN,CRect(0,0,0,200), this, ID_TBBTN_REPORTCBOX) )
		{
			AfxMessageBox(_T("ERROR:\nOnCreateControl"));
		}
		else
		{
			m_fnt1 = new CFont();
			LOGFONT lf;
			memset(&lf,0,sizeof(LOGFONT));
			lf.lfHeight = 16;
			lf.lfWeight = FW_NORMAL;
			m_fnt1->CreateFontIndirect(&lf);


			m_cboxPrintOut.SetOwner(this);
			m_cboxPrintOut.MoveWindow(45, 3, 150, 20);
			m_cboxPrintOut.SetFont(m_fnt1);

	    CXTPControlCustom * pCB = CXTPControlCustom::CreateControlCustom(&m_cboxPrintOut);

			lpCreateControl->buttonStyle = xtpButtonIconAndCaption;
      lpCreateControl->pControl = pCB;
				
		}
		return TRUE;
	}

	return FALSE;
}


BOOL CLogScaleSalesFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CLogScaleSalesFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CLogScaleSalesFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6515_KEY);
		LoadPlacement(this, csBuf);
  }

}

void CLogScaleSalesFrame::OnSetFocus(CWnd* pWnd)
{
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,ID_DO_SOMETHING_IN_SHELL,ID_LPARAM_COMMAND2);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	CMDIChildWnd::OnSetFocus(pWnd);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CLogScaleSalesFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{

	CString sStr = L"";
	if (wParam == (ID_DO_SOMETHING_IN_SHELL + ID_LPARAM_COMMAND2))
	{
		if (!m_bInitReports)
		{
			// The return message holds a _user_msg structure, collected in the
			// OnMessageFromShell( WPARAM wParam, LPARAM lParam ); 070410 p�d
			// In this case the szFileName item in _user_msg structure holds
			// the path and filename of the ShellData file used and the szArgStr
			// holds the Suite/UserModule name; 070410 p�d
			_user_msg *msg = (_user_msg*)lParam;
			if (msg != NULL)
			{
				m_sShellDataFile = msg->getFileName();
				//m_nShellDataIndex = msg->getIndex();
				m_nShellDataIndex = 6515; // explicit set Identifer, macth id in ShellData file; 090212 p�d
				getSTDReports(m_sShellDataFile,PROGRAM_NAME,m_nShellDataIndex,m_vecReports);
				m_cboxPrintOut.ResetContent();
				RLFReader xml; // = new RLFReader;
				if (xml.Load(m_sLangFN))
				{

					for (UINT i = 0;i < m_vecReports.size();i++)
					{
						if (m_vecReports[i].getCaption().IsEmpty())
							sStr = xml.str(m_vecReports[i].getStrID()); 
						else
							sStr = m_vecReports[i].getCaption();
						m_cboxPrintOut.AddString(sStr);
					}
				}

				m_bInitReports = TRUE;
			}
		}
	}

	return 0L;
}


void CLogScaleSalesFrame::OnUpdatePrintOutTBtn(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsPrintOutTBtn );
}

void CLogScaleSalesFrame::OnUpdateFilterOff(CCmdUI* pCmdUI)
{
	pCmdUI->Enable( m_bIsFilterOffTBtn );
}


void CLogScaleSalesFrame::OnTBBtnPrintOut()
{

	CString sReportPathAndFN = L"";
	CString sFileExtension = L"";
	CString sArgStr = L"";
	int nTractID = -1; // LoadID
	getSTDReports(m_sShellDataFile,PROGRAM_NAME,m_nShellDataIndex,m_vecReports);

	int nIdx = m_cboxPrintOut.GetCurSel();

	if (nIdx > -1 && m_vecReports.size() > 0)
	{

		sArgStr = L"";
		CLogSalesView *pView = (CLogSalesView*)getFormViewByID(IDD_FORMVIEW11);
		if (pView != NULL)
		{
			nTractID = pView->getActiveLoadID();
			sArgStr.Format(_T("%d;"),nTractID);
		}

		sReportPathAndFN.Format(_T("%s%s\\%s"),
														getReportsDir(),
														getLangSet(),
														m_vecReports[nIdx].getFileName());
		if (fileExists(sReportPathAndFN))
		{
			AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,WM_USER+4,
				(LPARAM)&_user_msg(333,	// ID = 333 for CrystalReport, ID = 300 for FastReports
				_T("OpenSuiteEx"),			// Exported/Imported function
				_T("Reports2.dll"),			// Suite to call; Report2.dll = Reportgenerator for CrystalReports (A.G.), Report.dll = Reportgenerator for FastReports
				(sReportPathAndFN),			// Use this report
				(sReportPathAndFN),
				sArgStr));
		}
	}	// if (nIdx > -1 && nIdx < m_vecReports.size())

}

void CLogScaleSalesFrame::OnPrintOutCBox()
{
	m_bIsPrintOutTBtn = m_cboxPrintOut.GetCurSel() > CB_ERR;
}

void CLogScaleSalesFrame::OnMatchTags()
{
}

// CLogScaleSalesFrame diagnostics

#ifdef _DEBUG
void CLogScaleSalesFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CLogScaleSalesFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CLogScaleSalesFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}


// PRIVATE


///////////////////////////////////////////////////////////////////////////////////////////
// CLogSalesView

IMPLEMENT_DYNCREATE(CLogSalesView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CLogSalesView, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_COPYDATA()
	ON_WM_CREATE()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessge)
	ON_NOTIFY(XTP_NM_REPORT_HEADER_RCLICK, ID_REPORT_TICKETS, OnReportItemRClick)
	ON_NOTIFY(NM_CLICK, ID_REPORT_TICKETS, OnReportItemClick)
	ON_NOTIFY(NM_DBLCLK, ID_REPORT_TICKETS, OnReportItemDblClick)
	ON_COMMAND(ID_TBBTN_SELECTION_DLG, OnFieldSelection)

	ON_COMMAND(ID_TBBTN_NEW,OnNewSale)
	ON_COMMAND(ID_TBBTN_SAVE,OnSaveSale)
	ON_COMMAND(ID_TBBTN_DEL,OnDeleteSale)

	ON_COMMAND(ID_TBBTN_FILTER,OnFilter)
	ON_COMMAND(ID_TBBTN_DEL_FILTER,OnFilterOff)

	ON_COMMAND(ID_TBBTN_IMPORT,OnImportTags)

	ON_COMMAND(ID_TBBTN_EXPORT,OnExportToEXCEL)
//	ON_COMMAND(ID_EXPORT_TMPL,OnExportTemplate)

//	ON_COMMAND_RANGE(ID_TOOLS_SPECIES,ID_TOOLS_DEFAULTS,OnSettings)
END_MESSAGE_MAP()

CLogSalesView::CLogSalesView()
	: CXTResizeFormView(CLogSalesView::IDD)
{
	m_bInitialized = FALSE;
	m_nSelectedColumn = -1;
	m_nFilteredColumn = -1;
}

CLogSalesView::~CLogSalesView()
{
}
// CLogSalesView diagnostics

#ifdef _DEBUG
void CLogSalesView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CLogSalesView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG


void CLogSalesView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
}

void CLogSalesView::OnDestroy()
{

// Not used for now; 2011-11-09 p�d
//	setColumnsSettingInReg(REG_TICKETS_COLUMNS_KEY,m_repLogSales);
	
	SaveReportState();

	CXTResizeFormView::OnDestroy();
}

void CLogSalesView::OnClose()
{
	CXTResizeFormView::OnClose();
}

int CLogSalesView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;
	return 0;
}

BOOL CLogSalesView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CLogSalesView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();
	//	SetScaleToFitSize(CSize(90, 1));

	if (! m_bInitialized )
	{
		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		CLogScaleSalesFrame* pWnd = (CLogScaleSalesFrame *)getFormViewParentByID(IDD_FORMVIEW11);
		if (m_wndSubList.GetSafeHwnd() == NULL)
		{
		//	m_wndSubList.SubclassDlgItem(IDC_COLUMNS, &pWnd->m_wndFieldSelectionDlg);
			m_repLogSales.GetColumns()->GetReportHeader()->SetSubListCtrl(&m_wndSubList);
		}

		if (m_wndFilterEdit.GetSafeHwnd() == NULL)
		{
			m_wndFilterEdit.SubclassDlgItem(IDC_FILTEREDIT20_1, &pWnd->m_wndFilterEdit);
			m_repLogSales.GetColumns()->GetReportHeader()->SetFilterEditCtrl(&m_wndFilterEdit);
		}

		if (m_lbl20_1.GetSafeHwnd() == NULL)
		{
			m_lbl20_1.SubclassDlgItem(IDC_FILTERLBL20_1, &pWnd->m_wndFilterEdit);
			m_lbl20_1.SetBkColor(INFOBK);
		}

		if (m_lbl20_2.GetSafeHwnd() == NULL)
		{
			m_lbl20_2.SubclassDlgItem(IDC_FILTERLBL20_2, &pWnd->m_wndFilterEdit);
			m_lbl20_2.SetBkColor(INFOBK);
			m_lbl20_2.SetLblFont(14,FW_BOLD);
		}

		setupReportControl();

		LoadReportState();

		populateReport(false);

		m_bInitialized = TRUE;
	}

}

BOOL CLogSalesView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{

	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		if (m_dbConnectionData.conn->isConnected())
		{
			m_pDB = new CDBHandling(m_dbConnectionData);
			if (m_pDB != NULL)
			{
				m_pDB->getInventories(m_vecInventory);
			}
		}
	}

	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CLogSalesView::OnSize(UINT nType, int cx, int cy)
{
	CXTResizeFormView::OnSize(nType, cx, cy);

	if (m_repLogSales.GetSafeHwnd() != NULL)
	{
		setResize(&m_repLogSales,1,1,cx-2,cy-2);
	}
}

LRESULT CLogSalesView::OnSuiteMessge( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}

void CLogSalesView::OnReportItemRClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	ASSERT(pItemNotify->pColumn);
	CPoint ptClick = pItemNotify->pt;

	CMenu menu;
	VERIFY(menu.CreatePopupMenu());

	// create main menu items
	menu.AppendMenu(MF_STRING, ID_SHOW_FIELD_SELECTION, m_sFieldSelection);

	// track menu
	int nMenuResult = CXTPCommandBars::TrackPopupMenu(&menu, TPM_NONOTIFY | TPM_RETURNCMD | TPM_LEFTALIGN |TPM_RIGHTBUTTON, ptClick.x, ptClick.y, this, NULL);

	// other general items
	switch (nMenuResult)
	{
		case ID_SHOW_FIELD_SELECTION:
			OnFieldSelection();	
		break;
	}
	menu.DestroyMenu();
}

void CLogSalesView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	CRect rect;
	POINT pt;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;

	// prevents us from crashing when clicking inside the report, but not on an item
	if(pItemNotify == NULL) 
		return;

	if(pItemNotify->pColumn != NULL) 
	{
		m_nSelectedColumn = pItemNotify->pColumn->GetIndex();
		//setFilterWindow();	#4164
	}

	// prevents us from crashing when clicking inside the report, but not on an item
	if(pItemNotify->pItem == NULL)
		return;

	switch (pItemNotify->pColumn->GetItemIndex())
	{

		// �ppna Logs; spara ticketdata och f�ra �ver ticket till logs
		// Changed from COLUMN_5 to COLUMN_0; 110701 P�D
		case COLUMNS::SALE_ID :
			{
				// Do a hit-test; 080513 p�d
				rect = pItemNotify->pColumn->GetRect();		
				pt = pItemNotify->pt;
				// Check if the user clicked on the Icon or not; 080513 p�d
				if (hitTest_X(pt.x,rect.left,13))
				{
					CInventoryReportRec* pRec = NULL;
					pRec = (CInventoryReportRec*)pItemNotify->pItem->GetRecord();
					if (pRec != NULL)
					{
						showFormView(IDD_FORMVIEW6,m_sLangFN,(LPARAM)&pRec->getRecord(),ID_OPEN_SALE);
					}
				}
			}
			break;
	};
}

void CLogSalesView::OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;

	// prevents us from crashing when clicking inside the report, but not on an item
	if(pItemNotify == NULL) 
		return;

	// prevents us from crashing when clicking inside the report, but not on an item
	if(pItemNotify->pItem == NULL)
		return;

	CInventoryReportRec *pRec = (CInventoryReportRec*)m_repLogSales.GetFocusedRow()->GetRecord();
	if (pRec == NULL) 
		return;

	if (pRec != NULL)
	{
		showFormView(IDD_FORMVIEW6,m_sLangFN,(LPARAM)&pRec->getRecord(),ID_OPEN_SALE);
	}
}

void CLogSalesView::OnFilter(void)
{
	CLogScaleSalesFrame* pWnd = (CLogScaleSalesFrame *)getFormViewParentByID(IDD_FORMVIEW11);
	if (pWnd != NULL)
	{
		setFilterWindow(false);
		BOOL bShow = !pWnd->m_wndFilterEdit.IsVisible();
		pWnd->ShowControlBar(&pWnd->m_wndFilterEdit, bShow, FALSE);
		pWnd->setEnableFilterOff(!m_repLogSales.GetFilterText().IsEmpty());
	}

}

void CLogSalesView::OnFilterOff(void)
{
	m_repLogSales.SetFilterText(_T(""));
	m_repLogSales.Populate();
	m_wndFilterEdit.SetWindowText(_T(""));
	CLogScaleSalesFrame* pWnd = (CLogScaleSalesFrame *)getFormViewParentByID(IDD_FORMVIEW11);
	if (pWnd != NULL)
	{
		pWnd->setEnableFilterOff(FALSE);
	}	// if (pWnd != NULL)
}

void CLogSalesView::setFilterWindow(bool bOld)
{
	// H�mta ut vilken kolumn som �r vald. #4164
	if(!bOld)
	{
		CXTPReportColumn *pCol = m_repLogSales.GetFocusedColumn();
		if (pCol != NULL)
			m_nFilteredColumn = pCol->GetIndex();
	}

	m_lbl20_1.SetWindowText(m_sFilterOn + _T(" :"));
	if (m_nFilteredColumn > -1 && m_nFilteredColumn < m_repLogSales.GetColumns()->GetCount())
	{
		CXTPReportColumns *pCols = m_repLogSales.GetColumns();
		CXTPReportColumn *pColumn = pCols->GetAt(m_nFilteredColumn);
		int nColumn = pColumn->GetIndex();
		if (pCols && nColumn < pCols->GetCount())
		{
			for (int i = 0;i < pCols->GetCount();i++)
			{
				pCols->GetAt(i)->SetFiltrable( i == nColumn );
			}
		}

		m_lbl20_2.SetWindowText(pColumn->GetCaption());
	}
	else
	{
		m_lbl20_2.SetWindowText(L"");
	}
}


void CLogSalesView::setupReportControl(void)
{
	CXTPReportColumn *pCol = NULL;
	if (m_repLogSales.GetSafeHwnd() == 0)
	{

		// Create the sheet1 list box.
		if (!m_repLogSales.Create(this, ID_REPORT_TICKETS, L"Tickets_report", TRUE, FALSE))
		{
			TRACE0( "Failed to create m_repLogSales.\n" );
			return;
		}
	}

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{

			// String-resource
			m_sFieldSelection = xml.str(IDS_STRING1100);

			m_sMsgCap = xml.str(IDS_STRING99);
			m_sMsgDelete1 = xml.str(IDS_STRING3570);
			m_sMsgDelete2 = xml.str(IDS_STRING3571);
			m_sOpenDlgTagFilesCaliper = xml.str(IDS_STRING3595);
			m_sOpenDlgTagFilesExcel = xml.str(IDS_STRING3596);
			m_sOpenDlgTagFilesAll = xml.str(IDS_STRING3597);

			m_sMsgOpenEXCEL = xml.str(IDS_STRING1124);
			m_sMsgErrorInTagFile  = xml.str(IDS_STRING3599);

			m_sFilterOn	= xml.str(IDS_STRING5951);

			if (m_repLogSales.GetSafeHwnd() != NULL)
			{

				VERIFY(m_ilIcons.Create(16,13, ILC_COLOR24|ILC_MASK, 0, 1));
				CBitmap bmp;
				VERIFY(bmp.LoadBitmap(IDB_BITMAP1));
				m_ilIcons.Add(&bmp, RGB(255, 0, 255));

				m_repLogSales.SetImageList(&m_ilIcons);

				m_repLogSales.ShowWindow( SW_NORMAL );
				m_repLogSales.ShowGroupBy( TRUE );

				// ID
				pCol = m_repLogSales.AddColumn(new CXTPReportColumn(COLUMNS::SALE_ID, xml.str(IDS_STRING3501), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE; // TRUE;
				// name
				pCol = m_repLogSales.AddColumn(new CXTPReportColumn(COLUMNS::SALE_NAME, xml.str(IDS_STRING3502), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE; // TRUE;
				// Num of logs
				pCol = m_repLogSales.AddColumn(new CXTPReportColumn(COLUMNS::SALE_NUMOF_LOGS, xml.str(IDS_STRING35020), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE; // TRUE;
				// Sum. price
				pCol = m_repLogSales.AddColumn(new CXTPReportColumn(COLUMNS::SALE_SUM_PRICE, xml.str(IDS_STRING35021), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK | DT_RIGHT);
				pCol->SetAlignment(DT_RIGHT);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE; // TRUE;
				// Buyer
				pCol = m_repLogSales.AddColumn(new CXTPReportColumn(COLUMNS::SALE_BUYER, xml.str(IDS_STRING3503), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE; //TRUE;
				// Created by
				pCol = m_repLogSales.AddColumn(new CXTPReportColumn(COLUMNS::SALE_CREATED_BY, xml.str(IDS_STRING3504), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Date
				pCol = m_repLogSales.AddColumn(new CXTPReportColumn(COLUMNS::SALE_DATE, xml.str(IDS_STRING3505), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Hauler
				pCol = m_repLogSales.AddColumn(new CXTPReportColumn(COLUMNS::SALE_HAULER, xml.str(IDS_STRING3506), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Scaler
				pCol = m_repLogSales.AddColumn(new CXTPReportColumn(COLUMNS::SALE_SCALER, xml.str(IDS_STRING3507), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Supplier
				pCol = m_repLogSales.AddColumn(new CXTPReportColumn(COLUMNS::SALE_SUPPLIER, xml.str(IDS_STRING3508), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE; //TRUE;
				// Sourceid
				pCol = m_repLogSales.AddColumn(new CXTPReportColumn(COLUMNS::SALE_SOURCEID, xml.str(IDS_STRING3509), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Location
				pCol = m_repLogSales.AddColumn(new CXTPReportColumn(COLUMNS::SALE_LOCATION, xml.str(IDS_STRING3510), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Tract id
				pCol = m_repLogSales.AddColumn(new CXTPReportColumn(COLUMNS::SALE_TRCTID, xml.str(IDS_STRING3511), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// Vendor
				pCol = m_repLogSales.AddColumn(new CXTPReportColumn(COLUMNS::SALE_VENDOR, xml.str(IDS_STRING3512), 80));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE;
				// GPS coord 1
				pCol = m_repLogSales.AddColumn(new CXTPReportColumn(COLUMNS::SALE_GPS_COORD1, xml.str(IDS_STRING3513), 100));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE; //TRUE;
				// GPS coord 2
				pCol = m_repLogSales.AddColumn(new CXTPReportColumn(COLUMNS::SALE_GPS_COORD2, xml.str(IDS_STRING3514), 100));
				pCol->SetHeaderAlignment(DT_WORDBREAK);
				pCol->GetEditOptions()->m_bAllowEdit = FALSE; //TRUE;

				m_repLogSales.GetReportHeader()->AllowColumnRemove(TRUE);
				m_repLogSales.SetMultipleSelection( FALSE );
				m_repLogSales.SetGridStyle( TRUE, xtpReportGridSolid );
				m_repLogSales.SetGridStyle( FALSE, xtpReportGridSmallDots );
				m_repLogSales.FocusSubItems(TRUE);
				m_repLogSales.AllowEdit(TRUE);
				m_repLogSales.GetPaintManager()->SetFixedRowHeight(FALSE);

				xml.clean();

				RECT rect;
				GetClientRect(&rect);
				setResize(&m_repLogSales,1,1,rect.right-2,rect.bottom-2);

			}	// if (m_repLogSales.GetSafeHwnd() != NULL)
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))
}

void CLogSalesView::populateReport(bool focus_row)
{
	if (m_pDB != NULL)
	{
		// Get data from database and also add to report
		m_pDB->getInventories(m_vecInventory);
		// Get register data
		m_pDB->getRegister(m_vecRegister);
	}
	
	m_repLogSales.ResetContent();
	if (m_vecInventory.size()> 0)
	{
		for (UINT i = 0;i < m_vecInventory.size();i++)
		{
			CInventory rec = m_vecInventory[i];
			m_repLogSales.AddRecord(new CInventoryReportRec(rec,rec.getPKID(),rec.getType(),
										rec.getID(),rec.getName(),
										rec.getNumOfLogs(),
										rec.getSumPrice(),
										getRegisterValue(REGISTER_TYPES::BUYER,rec.getBuyerID(),m_vecRegister),
										rec.getCreatedBy(),
										rec.getDate(),
										getRegisterValue(REGISTER_TYPES::HAULER,rec.getHaulerID(),m_vecRegister),
										getRegisterValue(REGISTER_TYPES::SCALER,rec.getScalerID(),m_vecRegister),
										getRegisterValue(REGISTER_TYPES::SUPPLIER,rec.getSupplierID(),m_vecRegister),
										getRegisterValue(REGISTER_TYPES::SOURCEID,rec.getSourceID(),m_vecRegister),
										getRegisterValue(REGISTER_TYPES::LOCATION,rec.getLocationID(),m_vecRegister),
										getRegisterValue(REGISTER_TYPES::TRACTID,rec.getTractID(),m_vecRegister),
										getRegisterValue(REGISTER_TYPES::VENDOR,rec.getVendorID(),m_vecRegister),
										rec.getGPSCoord1(),
										rec.getGPSCoord2()));

		}
		m_repLogSales.Populate();
		m_repLogSales.UpdateWindow();

		if (focus_row)
			m_repLogSales.SetFocusedRow(m_repLogSales.GetRows()->GetAt(m_repLogSales.GetRows()->GetCount()-1));
	}
}

int CLogSalesView::getActiveLoadID()
{
	CXTPReportRow *pRow = m_repLogSales.GetFocusedRow();
	if (pRow != NULL)
	{
		CInventoryReportRec *pRec = (CInventoryReportRec*)pRow->GetRecord();
		if (pRec != NULL)
			return pRec->getRecord().getPKID();
	}
	return -1;
}

void CLogSalesView::OnFieldSelection(void)
{
	CLogScaleSalesFrame* pWnd = (CLogScaleSalesFrame *)getFormViewParentByID(IDD_FORMVIEW11);
	if (pWnd != NULL)
	{
		BOOL bShow = !pWnd->m_wndFieldSelectionDlg.IsVisible();
		pWnd->ShowControlBar(&pWnd->m_wndFieldSelectionDlg, bShow, FALSE);
	}
}

// Saves a specific ticket (focused row)
BOOL CLogSalesView::SaveSale(CInventory& rec)
{

	return TRUE;

}
// Save ALL tickets
BOOL CLogSalesView::Save(bool populate)
{
	return TRUE;
}

void CLogSalesView::Delete()
{
	CXTPReportRow *pRow = m_repLogSales.GetFocusedRow();
	CInventoryReportRec* pRec = NULL;
	CString sMsg = L"",sSQL = L"",sSQLBuffer = L"";
	vecInventoryLogs vecInvLogs;
	CChangeStatusDlg *pDlg = new CChangeStatusDlg();

	CViewProgress wndPrg;

	// Create the edit control and add it to the status bar
	if (!wndPrg.Create(CViewProgress::IDD))
	{
		TRACE0("Failed to create edit control.\n");
		return;
	}

	if (pRow != NULL && m_pDB != NULL && pDlg != NULL)
	{
		pRec = (CInventoryReportRec*)pRow->GetRecord();
		if (pRec != NULL)
		{
			sMsg.Format(m_sMsgDelete1,pRec->getIconColText(COLUMNS::SALE_ID));
			if (::MessageBox(GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
			{
				return;
			}
#ifdef _ASK_TWICE_ON_DELETE_SALE
			else if (::MessageBox(GetSafeHwnd(),m_sMsgDelete2,m_sMsgCap,MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
			{
				return;
			}
#endif

			CString sRegKey = L"";
			sRegKey.Format(L"%s_%d",pRec->getIconColText(COLUMNS::SALE_ID),pRec->getPKID());
			delColumnsSettingFile(sRegKey,COLSET_SALES);

			// Collect Logs in InventLogs table
			// so we can reset status
			m_pDB->getInventoryLogs(vecInvLogs,pRec->getPKID());
			if (vecInvLogs.size() > 0)
			{
				if (pDlg->DoModal() == IDOK)
				{
					if (wndPrg.GetSafeHwnd())
					{
						wndPrg.setRange(0,vecInvLogs.size());
						wndPrg.setStep();
						wndPrg.ShowWindow(SW_NORMAL);
						wndPrg.UpdateWindow();
					}
					sSQL.Format(L"UPDATE %s SET nStatusFlag=%d WHERE ",TBL_LOGS,pDlg->getStatus());
					sSQLBuffer = sSQL;

					for (UINT i = 0;i < vecInvLogs.size();i++)
					{
						CLogs rec = CLogs();
						rec.setLoadID(vecInvLogs[i].getFKLoadID());
						rec.setLogID(vecInvLogs[i].getFKLogID());
						rec.setStatusFlag(pDlg->getStatus());
						if (i < vecInvLogs.size()-1)
						{
							sSQL.Format(L"(pkLogID=%d AND fkLoadID=%d) OR ",rec.getLogID(),rec.getLoadID());
						}
						else if (i == vecInvLogs.size()-1)
						{
							sSQL.Format(L"(pkLogID=%d AND fkLoadID=%d);",rec.getLogID(),rec.getLoadID());
						}
						sSQLBuffer += sSQL;

						//sSQL.Format(L"UPDATE %s SET nStatusFlag=%d,dUpdated=GETDATE() WHERE pkLogID=%d AND fkLoadID=%d;",
						//	TBL_LOGS,rec.getStatusFlag(),rec.getLogID(),rec.getLoadID());
						//sSQLBuffer += sSQL;
						//m_pDB->setLogStatus(rec);
						if (wndPrg.GetSafeHwnd())
							wndPrg.setPos(i+1);
					}
					wndPrg.setRange(0,100);
					wndPrg.setPos(40);
					m_pDB->setLogStatus(sSQLBuffer);
					sSQLBuffer.Empty();
					wndPrg.setPos(60);
					m_pDB->delInventory(pRec->getPKID(),pRec->getType());
					wndPrg.setPos(80);
					populateReport(false);
					// Check if there's any Sales left. If not reset identity-field
					if (m_vecInventory.size() == 0)
					{
						m_pDB->resetInventoryIdentityField();
					}
					if (wndPrg.GetSafeHwnd())
					{
						wndPrg.ShowWindow(SW_HIDE);
						wndPrg.DestroyWindow();
					}

				}
			}	// if (pDlg->DoModal())
			else
			{
				m_pDB->delInventory(pRec->getPKID(),pRec->getType());
				populateReport(false);
				// Check if there's any Sales left. If not reset identity-field
				if (m_vecInventory.size() == 0)
				{
					m_pDB->resetInventoryIdentityField();
				}
			}
				
		}	// if (pRec != NULL)
	}	// if (pRow != NULL)
}

void CLogSalesView::OnNewSale(void)
{
	CInventory rec = CInventory();
	showFormView(IDD_FORMVIEW6,m_sLangFN,(LPARAM)&rec,ID_OPEN_NEW_SALE);
}

void CLogSalesView::OnSaveSale(void)
{
	// Save ALL tsales
	Save(true);
}

void CLogSalesView::OnDeleteSale(void)
{
	Delete();
}

void CLogSalesView::OnImportTags()
{
	TCHAR szStr[MAX_PATH];
	CString  strFile = _T("*.lsb;*.xls;*.xlsx"),sTagStr = L"",sPath = L"";
//	CString  strFile = _T("*.*"),sTagStr,str;
	CString strFilter = L"";
	strFilter.Format(L"%s (*.lsb)|*.lsb|%s (*.xls; *.xslx)|*.xls; *.xslx|%s (*.*)|*.*|",
	m_sOpenDlgTagFilesCaliper,m_sOpenDlgTagFilesExcel,m_sOpenDlgTagFilesAll);

	sPath = regGetStr(REG_ROOT,SALE_IMPORT_TAG_DIR,DIRECTORY_KEY);
	sPath += strFile;


	CFileDialog dlg(TRUE,L"lsb",sPath,OFN_OVERWRITEPROMPT|OFN_HIDEREADONLY,strFilter);
	if(dlg.DoModal() == IDOK)
	{

		// Check status selected tag-file
		if (getStatusFromTagFile(dlg.GetPathName(),LOGSTATUS::SOLD) != LOGSTATUS::SOLD)
		{
			::MessageBox(GetSafeHwnd(),m_sMsgErrorInTagFile,m_sMsgCap,MB_ICONEXCLAMATION | MB_OK);
			return;
		}

		regSetStr(REG_ROOT,SALE_IMPORT_TAG_DIR,DIRECTORY_KEY,getFilePath(dlg.GetPathName()));

		swprintf(szStr,L"%s",dlg.GetPathName());
		showFormView(IDD_FORMVIEW6,m_sLangFN,(LPARAM)(LPCTSTR)&szStr,ID_OPEN_SALE_TAG_FILE);
	}
}

void CLogSalesView::OnExportToEXCEL()
{
	CString sRegKey = L"";
	int f[4];
	CVecLogs vecLogs;
	CLogs recLog = CLogs();
	CLogs recLogAvg = CLogs();
	CString sValue = L"",sPath = L"",sFileName = L"";
	int nRow = 0,nColCnt = 0;
	RLFReader xml;
	CXTPReportRow *pRow = m_repLogSales.GetFocusedRow();
	CInventoryReportRec* pRec = NULL;
	vecInventoryLogs vecInvLogs;
	CInventory recInv = CInventory();
	vecColumnsSet vec;

	if (pRow != NULL && m_pDB != NULL)
	{
		pRec = (CInventoryReportRec*)pRow->GetRecord();
		if (pRec != NULL)
		{
			recInv = pRec->getRecord();
			m_pDB->getLogsSale(vecLogs,pRec->getRecord().getPKID());
			m_pDB->avgSaleLogData(recLogAvg,pRec->getRecord().getPKID());
			sFileName = recInv.getID();
		}
	}

	if (xml.Load(m_sLangFN))
	{
		//#3611, lagt in koll inga ogiltiga tecken i Ticket numret	\ / ? : * " > < |
		if(checkIllegalChars(recInv.getID()))
		{
			::MessageBox(GetSafeHwnd(),_T("\\ / ? : * \" > < |")+xml.str(IDS_STRING18442),xml.str(IDS_STRING99),MB_ICONEXCLAMATION | MB_OK);
		}
		else
		{	

			CExcelExportDlg *pDlg = new CExcelExportDlg();
			if (pDlg != NULL)
			{
				sRegKey.Format(L"%s_%d",recInv.getID(),recInv.getPKID());
				pDlg->getColumnsSetting(sRegKey,COLSET_SALES);	
				if (pDlg->DoModal() != IDOK) return;
				pDlg->getColumnsSelected(vec);

				delete pDlg;
			}


			Book* book = xlCreateBook();
			if(book)
			{
				book->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0hfeflb");
				f[0] = book->addCustomNumFormat(L"0.0");
				f[1] = book->addCustomNumFormat(L"0.00");
				f[2] = book->addCustomNumFormat(L"0.000");
				Format* fmt[3];
				fmt[0] = book->addFormat();
				fmt[0]->setNumFormat(f[0]);
				fmt[1] = book->addFormat();
				fmt[1]->setNumFormat(f[1]);
				fmt[2] = book->addFormat();
				fmt[2]->setNumFormat(f[2]);
				fmt[3] = book->addFormat();
				fmt[3]->setWrap();
				Sheet* sheet = book->addSheet(L"Sheet1");
				if(sheet)
				{
					// Logs data
					for (int i = 0;i < vec.size();i++)
					{
						sheet->writeStr(1, i,vec[i].getColName(),fmt[3]);
					}

					for (int col = 0;col < vec.size();col++)
					{
						nColCnt = col;

						for (UINT i = 0;i < vecLogs.size();i++)
						{
							recLog = vecLogs[i];
							if (vec[nColCnt].getColIndex() == 0) sheet->writeStr(i+2, nColCnt,recLog.getTicket());
							if (vec[nColCnt].getColIndex() == 1) sheet->writeStr(i+2, nColCnt,recLog.getTagNumber());
							if (vec[nColCnt].getColIndex() == 2) sheet->writeStr(i+2, nColCnt,recLog.getSpcCode());
							if (vec[nColCnt].getColIndex() == 3) sheet->writeStr(i+2, nColCnt,recLog.getSpcName());
							sValue.Format(L"%d",recLog.getFrequency());
							if (vec[nColCnt].getColIndex() == 4) sheet->writeStr(i+2, nColCnt,sValue);
							if (vec[nColCnt].getColIndex() == 5) sheet->writeStr(i+2, nColCnt,recLog.getGrade());
							if (vec[nColCnt].getColIndex() == 6) sheet->writeNum(i+2, nColCnt,recLog.getDeduction(),fmt[0]);
							if (vec[nColCnt].getColIndex() == 7) sheet->writeNum(i+2, nColCnt,recLog.getDOBTop(),fmt[2]);
							if (vec[nColCnt].getColIndex() == 8) sheet->writeNum(i+2, nColCnt,recLog.getDOBTop2(),fmt[2]);
							if (vec[nColCnt].getColIndex() == 9) sheet->writeNum(i+2, nColCnt,recLog.getDIBTop(),fmt[2]);
							if (vec[nColCnt].getColIndex() == 10) sheet->writeNum(i+2, nColCnt,recLog.getDIBTop2(),fmt[2]);
							if (vec[nColCnt].getColIndex() == 11) sheet->writeNum(i+2, nColCnt,recLog.getDOBRoot(),fmt[2]);
							if (vec[nColCnt].getColIndex() == 12) sheet->writeNum(i+2, nColCnt,recLog.getDOBRoot2(),fmt[2]);
							if (vec[nColCnt].getColIndex() == 13) sheet->writeNum(i+2, nColCnt,recLog.getDIBRoot(),fmt[2]);
							if (vec[nColCnt].getColIndex() == 14) sheet->writeNum(i+2, nColCnt,recLog.getDIBRoot2(),fmt[2]);
							if (vec[nColCnt].getColIndex() == 15) sheet->writeNum(i+2, nColCnt,recLog.getLengthMeas(),fmt[0]);
							if (vec[nColCnt].getColIndex() == 16) sheet->writeNum(i+2, nColCnt,recLog.getLengthCalc(),fmt[0]);
							if (vec[nColCnt].getColIndex() == 17) sheet->writeNum(i+2, nColCnt,recLog.getBark(),fmt[2]);
							if (vec[nColCnt].getColIndex() == 18) sheet->writeNum(i+2, nColCnt,recLog.getVolPricelist(),fmt[2]);
							if (vec[nColCnt].getColIndex() == 19) sheet->writeNum(i+2, nColCnt,recLog.getLogPrice(),fmt[2]);
							if (vec[nColCnt].getColIndex() == 20) sheet->writeStr(i+2, nColCnt,recLog.getVolFuncName(),fmt[2]);
							if (vec[nColCnt].getColIndex() == 21) sheet->writeNum(i+2, nColCnt,recLog.getURootDia(),fmt[2]);
							if (vec[nColCnt].getColIndex() == 22) sheet->writeNum(i+2, nColCnt,recLog.getURootDia2(),fmt[2]);
							if (vec[nColCnt].getColIndex() == 23) sheet->writeNum(i+2, nColCnt,recLog.getUVol(),fmt[2]);
							if (vec[nColCnt].getColIndex() == 24) sheet->writeNum(i+2, nColCnt,recLog.getUTopDia(),fmt[2]);
							if (vec[nColCnt].getColIndex() == 25) sheet->writeNum(i+2, nColCnt,recLog.getUTopDia2(),fmt[2]);
							if (vec[nColCnt].getColIndex() == 26) sheet->writeNum(i+2, nColCnt,recLog.getULength(),fmt[2]);
							if (vec[nColCnt].getColIndex() == 27) sheet->writeNum(i+2, nColCnt,recLog.getUPrice(),fmt[2]);
							if (vec[nColCnt].getColIndex() == 28) sheet->writeStr(i+2, nColCnt,recLog.getReason());
							if (vec[nColCnt].getColIndex() == 29) sheet->writeStr(i+2, nColCnt,recLog.getNotes());
						}
					}

					nRow = vecLogs.size();
					for (int col = 0;col < vec.size();col++)
					{
						nColCnt = col;

						//---------------------------------------------------------------------------------------------------
						// Footer headlines
						if (vec[nColCnt].getColIndex() == 0) sheet->writeStr(nRow+2, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 1) sheet->writeStr(nRow+2, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 2) sheet->writeStr(nRow+2, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 3) sheet->writeStr(nRow+2, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 4) sheet->writeStr(nRow+2, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 5) sheet->writeStr(nRow+2, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 6) sheet->writeStr(nRow+2, nColCnt,L"");

						if (vec[nColCnt].getColIndex() == 7) sheet->writeStr(nRow+2, nColCnt,xml.str(IDS_STRING1705) + L" " + xml.str(IDS_STRING119),fmt[3]);
						if (vec[nColCnt].getColIndex() == 8) sheet->writeStr(nRow+2, nColCnt,xml.str(IDS_STRING17050) + L" " + xml.str(IDS_STRING119),fmt[3]);
						if (vec[nColCnt].getColIndex() == 9) sheet->writeStr(nRow+2, nColCnt,xml.str(IDS_STRING1706) + L" " + xml.str(IDS_STRING119),fmt[3]);
						if (vec[nColCnt].getColIndex() == 10) sheet->writeStr(nRow+2, nColCnt,xml.str(IDS_STRING17060) + L" " + xml.str(IDS_STRING119),fmt[3]);
						if (vec[nColCnt].getColIndex() == 11) sheet->writeStr(nRow+2, nColCnt,xml.str(IDS_STRING1707) + L" " + xml.str(IDS_STRING119),fmt[3]);
						if (vec[nColCnt].getColIndex() == 12) sheet->writeStr(nRow+2, nColCnt,xml.str(IDS_STRING17070) + L" " + xml.str(IDS_STRING119),fmt[3]);
						if (vec[nColCnt].getColIndex() == 13) sheet->writeStr(nRow+2, nColCnt,xml.str(IDS_STRING1708) + L" " + xml.str(IDS_STRING119),fmt[3]);
						if (vec[nColCnt].getColIndex() == 14) sheet->writeStr(nRow+2, nColCnt,xml.str(IDS_STRING17080) + L" " + xml.str(IDS_STRING119),fmt[3]);
						if (vec[nColCnt].getColIndex() == 15) sheet->writeStr(nRow+2, nColCnt,xml.str(IDS_STRING17090) + L" " + xml.str(IDS_STRING119),fmt[3]);
						if (vec[nColCnt].getColIndex() == 16) sheet->writeStr(nRow+2, nColCnt,xml.str(IDS_STRING17091) + L" " + xml.str(IDS_STRING119),fmt[3]);
						if (vec[nColCnt].getColIndex() == 17) sheet->writeStr(nRow+2, nColCnt,xml.str(IDS_STRING1710) + L" " + xml.str(IDS_STRING119),fmt[3]);
						if (vec[nColCnt].getColIndex() == 18) sheet->writeStr(nRow+2, nColCnt,xml.str(IDS_STRING1711) + L" " + xml.str(IDS_STRING120),fmt[3]);
						if (vec[nColCnt].getColIndex() == 19) sheet->writeStr(nRow+2, nColCnt,xml.str(IDS_STRING1716) + L" " + xml.str(IDS_STRING120),fmt[3]);
						if (vec[nColCnt].getColIndex() == 20) sheet->writeStr(nRow+2, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 21) sheet->writeStr(nRow+2, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 22) sheet->writeStr(nRow+2, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 23) sheet->writeStr(nRow+2, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 24) sheet->writeStr(nRow+2, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 25) sheet->writeStr(nRow+2, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 26) sheet->writeStr(nRow+2, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 27) sheet->writeStr(nRow+2, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 28) sheet->writeStr(nRow+2, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 29) sheet->writeStr(nRow+2, nColCnt,L"");

						//---------------------------------------------------------------------------------------------------
						// Footer data
						if (vec[nColCnt].getColIndex() == 0) sheet->writeStr(nRow+3, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 1) sheet->writeStr(nRow+3, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 2) sheet->writeStr(nRow+3, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 3) sheet->writeStr(nRow+3, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 4) sheet->writeStr(nRow+3, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 5) sheet->writeStr(nRow+3, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 6) sheet->writeStr(nRow+3, nColCnt,L"");

						if (vec[nColCnt].getColIndex() == 7) sheet->writeNum(nRow+3, nColCnt,recLogAvg.getDOBTop(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 8) sheet->writeNum(nRow+3, nColCnt,recLogAvg.getDOBTop2(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 9) sheet->writeNum(nRow+3, nColCnt,recLogAvg.getDIBTop(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 10) sheet->writeNum(nRow+3, nColCnt,recLogAvg.getDIBTop2(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 11) sheet->writeNum(nRow+3, nColCnt,recLogAvg.getDOBRoot(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 12) sheet->writeNum(nRow+3, nColCnt,recLogAvg.getDOBRoot2(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 13) sheet->writeNum(nRow+3, nColCnt,recLogAvg.getDIBRoot(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 14) sheet->writeNum(nRow+3, nColCnt,recLogAvg.getDIBRoot2(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 15) sheet->writeNum(nRow+3, nColCnt,recLogAvg.getLengthMeas(),fmt[0]);
						if (vec[nColCnt].getColIndex() == 16) sheet->writeNum(nRow+3, nColCnt,recLogAvg.getLengthCalc(),fmt[0]);
						if (vec[nColCnt].getColIndex() == 17) sheet->writeNum(nRow+3, nColCnt,recLogAvg.getBark(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 18) sheet->writeNum(nRow+3, nColCnt,recLogAvg.getVolPricelist(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 19) sheet->writeNum(nRow+3, nColCnt,recLogAvg.getLogPrice(),fmt[2]);
						if (vec[nColCnt].getColIndex() == 20) sheet->writeStr(nRow+3, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 21) sheet->writeStr(nRow+3, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 22) sheet->writeStr(nRow+3, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 23) sheet->writeStr(nRow+3, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 24) sheet->writeStr(nRow+3, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 25) sheet->writeStr(nRow+3, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 26) sheet->writeStr(nRow+3, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 27) sheet->writeStr(nRow+3, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 28) sheet->writeStr(nRow+3, nColCnt,L"");
						if (vec[nColCnt].getColIndex() == 29) sheet->writeStr(nRow+3, nColCnt,L"");
					}
				}
			}

			sPath = regGetStr(REG_ROOT,SALE_EXPORT_TO_EXCEL_DIR,DIRECTORY_KEY);
			sPath += sFileName;

			CFileDialog dlg(FALSE,L".xls",sPath,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,L"EXCEL *.xls|*.xls|");
			if (dlg.DoModal() == IDOK)
			{
				if(book->save(dlg.GetPathName())) 
				{
#ifdef _OPEN_EXCEL_IN_SALE

					if (::MessageBox(GetSafeHwnd(),m_sMsgOpenEXCEL,m_sMsgCap,MB_ICONQUESTION | MB_YESNO) == IDYES)
						::ShellExecute(NULL, L"open", dlg.GetPathName(), NULL, NULL, SW_SHOW);        
#endif

				}
				regSetStr(REG_ROOT,SALE_EXPORT_TO_EXCEL_DIR,DIRECTORY_KEY,getFilePath(dlg.GetPathName()));
			}
		}

	}
	xml.clean();
}

void CLogSalesView::LoadReportState()
{
	CString sFilterText;
	UINT nBytes = 0;
	LPBYTE pData = 0;

	if (!AfxGetApp()->GetProfileBinary(REG_LOG_SALE_REPORT_KEY, _T("State"), &pData, &nBytes))
		return;

	CMemFile memFile(pData, nBytes);
	CArchive ar (&memFile,CArchive::load);

	try
	{
		m_repLogSales.SerializeState(ar);
	}
	catch (COleException* pEx)
	{
		pEx->Delete ();
	}
	catch (CArchiveException* pEx)
	{
		pEx->Delete ();
	}

	ar.Close();
	memFile.Close();
	delete[] pData;

	// Get filtertext for this Report
	sFilterText = AfxGetApp()->GetProfileString(REG_LOG_SALE_REPORT_KEY, _T("FilterText"), _T(""));
	// Get selected column index into registry; 120418 p�d
	m_nFilteredColumn = AfxGetApp()->GetProfileInt(REG_LOG_SALE_REPORT_KEY, _T("SelColIndex"),0);

	m_repLogSales.SetFilterText(sFilterText);
	setFilterWindow(true);
	m_repLogSales.Populate();
	m_wndFilterEdit.SetWindowText(sFilterText);

	CLogScaleSalesFrame* pWnd = (CLogScaleSalesFrame *)getFormViewParentByID(IDD_FORMVIEW11);
	if (pWnd != NULL)
	{
		pWnd->setEnableFilterOff(!sFilterText.IsEmpty());
	}

}

void CLogSalesView::SaveReportState()
{
	CString sFilterText;
	CMemFile memFile;
	CArchive ar (&memFile,CArchive::store);

	m_repLogSales.SerializeState(ar);

	ar.Flush();

	DWORD nBytes = (DWORD)memFile.GetPosition();
	LPBYTE pData = memFile.Detach();

	AfxGetApp()->WriteProfileBinary(REG_LOG_SALE_REPORT_KEY, _T("State"), pData, nBytes);

	ar.Close();
	memFile.Close();
	free(pData);

	sFilterText = m_repLogSales.GetFilterText();
	AfxGetApp()->WriteProfileString(REG_LOG_SALE_REPORT_KEY, _T("FilterText"), sFilterText);

	// Set selected column index into registry; 120419 p�d
	AfxGetApp()->WriteProfileInt(REG_LOG_SALE_REPORT_KEY, _T("SelColIndex"), m_nFilteredColumn);

}

