#pragma once

#include "dbhandling.h"
#include "registerformview.h"

///////////////////////////////////////////////////////////////////////////////////////////
// CRegisterDoc

class CRegisterDoc : public CDocument
{
protected: // create from serialization only
	CRegisterDoc();
	DECLARE_DYNCREATE(CRegisterDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRegisterDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CRegisterDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CRegisterDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
/////////////////////////////////////////////////////////////////////////////
// class CRegisterFrame

class CRegisterFrame : public CChildFrameBase
{
//private:

	DECLARE_DYNCREATE(CRegisterFrame)
	CXTPStatusBar m_wndStatusBar;
	CXTPToolBar m_wndToolBar;
	CXTPDockingPaneManager m_paneManager;

	BOOL m_bFirstOpen;

	BOOL m_bEnableToolBar;

protected:
	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	HICON m_hIcon;
public:
	CRegisterFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

	void toolbarEnableDisable(BOOL enable);

// Attributes

// Operations

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	virtual ~CRegisterFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif


// Generated message map functions
protected:
	
	//{{AFX_MSG(CRegisterFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg void OnDestroy(void);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );

	afx_msg void OnUpdateToolbarBtn(CCmdUI* pCmdUI);

	//}}AFX_MSG
	
	DECLARE_MESSAGE_MAP()
};


// CRegisterFormView form view

class CRegisterFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CRegisterFormView)

	// Data members
	BOOL m_bInitialized;
	BOOL m_bEnableToolBar;
	CString m_sLangFN;

	CString m_sMsgCap;
	CString m_sMsgDelete1;
	CString m_sMsgDelete2;
	CString m_sMsgMandatoryDataMissing;

	CMyExtStatic m_lbl4_1;
	CMyComboBox m_cboxRegisterTypes;

	CMyReportControl m_repRegister;

	CDBHandling *m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;

	CVecRegister m_vecRegister;
	int m_nSelectedType;

	// Methods
	void setupReport();
	void populateReport();

protected:
	CRegisterFormView();           // protected constructor used by dynamic creation
	virtual ~CRegisterFormView();

public:
	enum { IDD = IDD_FORMVIEW4 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	//{{AFX_VIRTUAL(CGradesReportView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CRegisterFormView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnDestroy(void);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);

	afx_msg void OnUpdateToolbarBtn(CCmdUI* pCmdUI);

	afx_msg void OnAdd();
	afx_msg void OnDelete();
	afx_msg void OnSave();

	afx_msg void OnCbnSelchangeCombo41();
	afx_msg void OnCbnDropdownCombo41();

	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


