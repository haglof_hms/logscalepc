#if !defined(__LOGSCALEVIEW_H__)
#define __LOGSCALEVIEW_H__

#include "DerivedClasses.h"

#include "ReportClasses.h"

#include "DBHandling.h"

#pragma once


//////////////////////////////////////////////////////////////////////////////
// Derived class from CXTPReportFilterEditControl to handle
// OnKeyUp() event, setting value for toolbar button
// FilterOff in CTraktSelListFrame; 070108 p�d

class CTicketReportFilterEditControl : public CXTPReportFilterEditControl
{
	DECLARE_DYNCREATE(CTicketReportFilterEditControl)
public:
	CTicketReportFilterEditControl(void)
		: CXTPReportFilterEditControl()
	{}

protected:
	//{{AFX_VIRTUAL(CPageOneFormView)
	afx_msg void OnKeyUp(UINT,UINT,UINT);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CACBox; used in Toolbar for CMDIStandEntryFormFrame to hold
// e.g. Reports; 070323 p�d

class CACBox : public CComboBox
{
public:
	CACBox();

	void SetLblFont(int size,int weight,LPCTSTR font_name = _T("Arial"));

	vecSTDReports m_vecReports;
	void setSTDReportsInCBox(LPCTSTR shell_data_file,LPCTSTR add_to,int index);
	void setLanguageFN(LPCTSTR lng_fn);

protected:
	CFont *m_fnt1;
	CString m_sLangFN;

	CString getLangStr(int id);

	//{{AFX_MSG(CACBox)
	afx_msg void OnDestroy();
	afx_msg BOOL OnCBoxChange();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

};

///////////////////////////////////////////////////////////////////////////////////////////
// CLogScaleDoc

class CLogScaleDoc : public CDocument
{
protected: // create from serialization only
	CLogScaleDoc();
	DECLARE_DYNCREATE(CLogScaleDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLogScaleDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CLogScaleDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CLogScaleDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};



#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
/////////////////////////////////////////////////////////////////////////////
// class CLogScaleFrame

class CLogScaleFrame : public CChildFrameBase
{
//private:

	DECLARE_DYNCREATE(CLogScaleFrame)
	CXTPStatusBar m_wndStatusBar;
	CXTPToolBar m_wndToolBar;
	CXTPDockingPaneManager m_paneManager;
	CString m_sLangFN;

	BOOL m_bFirstOpen;
	BOOL m_bInitReports;
	BOOL m_bIsPrintOutTBtn;
	BOOL m_bIsFilterOffTBtn;
//	CString m_sShellDataFile;
	int m_nShellDataIndex;
	CDialogBar m_wndFieldSelectionDlg;		// Sample Field chooser window

	vecSTDReports m_vecReports;
	CComboBox m_cboxPrintOut;
	//CACBox m_cboxPrintOut;

	CFont *m_fnt1;

protected:
	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	HICON m_hIcon;
public:
	CLogScaleFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

	CDialogBar m_wndFilterEdit;     // Sample Filter editing window

	inline void setEnableFilterOff(BOOL v)	{	m_bIsFilterOffTBtn = v;	}

// Attributes

// Operations

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	virtual ~CLogScaleFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	
	//{{AFX_MSG(CLogScaleFrame)
	afx_msg int OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl);
	afx_msg void OnSysCommand(UINT nID,LPARAM lParam);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg void OnDestroy(void);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );

	afx_msg void OnUpdatePrintOutTBtn(CCmdUI* pCmdUI);
	afx_msg void OnTBBtnPrintOut();
	afx_msg void OnPrintOutCBox();

	afx_msg void OnMatchTags(void);

	afx_msg void OnUpdateFilterOff(CCmdUI* pCmdUI);

	//}}AFX_MSG
	
	DECLARE_MESSAGE_MAP()
};


///////////////////////////////////////////////////////////////////////////////////////////
// CLogScaleView form view

class CLogScaleView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CLogScaleView)

	// Data members
	BOOL m_bInitialized;
	CString m_sLangFN;
	CString m_sFieldSelection;
	CString m_sMsgCap;
	CString m_sMsgDelete1;
	CString m_sMsgDelete2;
	CString m_sMsgTicketNumUsed;
	CString m_sMsgErrNewTicket1;
	CString m_sMsgErrNewTicket2;
	CString m_sMsgErrUpdTicket1;
	CString m_sMsgErrUpdTicket2;
	CString m_sMsgOpenEXCEL;
	CString m_sSaveTemplateCap;
	CString m_sFilterOn;
	//CString m_sMissingTicketData;
	CString m_sMissingTemplate1;
	CString m_sMissingTemplate2;
	CString m_sMsgDuplicateTagNumbers;
	CString m_sMsgCanNotBeRemovedSale;
	CString m_sMsgCanNotBeRemovedSawmill;
	

	CString m_sLblInch;
	CString m_sLblMM;
	CString m_sLblFeet;
	CString m_sLblDM;
	CString m_sLblBF;
	CString m_sLblDM3;
	CString m_sLblFT3;

	CXTPReportSubListControl m_wndSubList;

	CMyReportControl m_repTickets;

	CTickets m_recTicket;
	CVecTickets m_vecTickets;
	CVecLogs m_vecLogs;

	CVecSpecies m_vecSpecies;
	CVecGrades m_vecGrades;
	CVecRegister m_vecRegister;
	CVecPricelist m_vecPricelist;
	CVecTicketPrl m_vecTicketPrl;
	CVecCalcTypes m_vecCalcTypes;
	CVecDefaults m_vecDefaults;
	vecLogScaleTemplates m_vecLogScaleTemplates;

	vecSelectedVolFuncs m_vecSelectedVolFuncs;

	CDBHandling *m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;

	CImageList m_ilIcons;

	CStringArray m_sarrDeductionType;
	CStringArray m_sarrLoadType;
	CStringArray m_sarrStatus;
	CStringArray m_sarrMeasureTypes;

	int m_nSelLoadID;
	int m_nSelectedColumn;
	int m_nFilteredColumn;

	vecFuncDesc m_vecFuncDesc;
	CVecUserVolTables m_vecUserVolTables;

	CMyExtStatic m_lbl20_1;
	CMyExtStatic m_lbl20_2;

	CTicketReportFilterEditControl m_wndFilterEdit;
	// Methods
	void setupReportControl(void);
	BOOL checkLogData(CVecLogs& vec,CTickets& rec,bool ticket_num_ok);

	void addConstraint(REGISTER_TYPES::REGS type,int col_num);

	BOOL isTicketNumOK(LPCTSTR ticket_num);
	BOOL m_bIsTicketNumOK;

	void Delete();

	BOOL SaveTicket(CTickets& rec);

	void addTicket(CTicketsReportRec *pRec);

	CString getSpcName(LPCTSTR spc_code);

	void setFilterWindow(bool bOld = false);

	BOOL checkLogTagNumbers(LPCTSTR ticket,LPCTSTR ticket_file,CVecLogs &vec_in_tivket,CVecLogs &vec_logs_to_add_to_inventory);
	
protected:
	CLogScaleView();           // protected constructor used by dynamic creation
	virtual ~CLogScaleView();

	void LoadReportState(void);
	void SaveReportState(void);

public:
	enum { IDD = IDD_FORMVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

	// Also call upon in CLogScaleFrame()
	BOOL Save(bool populate);
	void populateReport(bool focus_row);

	int getActiveLoadID();

	BOOL isTicketAlreadyUsed(CString& ticket_num);

	CDBHandling *getDB()	{ return m_pDB; }

	void doExportToExcel(LPCTSTR ticket_name,int id);
	void doExportTemplate()	{ OnExportTemplate(); }

protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLogScaleView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CLogScaleView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnClose();
	afx_msg void OnDestroy(void);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg LRESULT OnSuiteMessge(WPARAM wParam, LPARAM lParam);
	afx_msg void OnReportItemRClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnReportItemDblClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	afx_msg void OnFieldSelection(void);

	afx_msg void OnNewTicket(void);
	afx_msg void OnSaveTicket(void);
	afx_msg void OnDeleteTicket(void);
	afx_msg void OnImportTicket(void);
	afx_msg void OnExportToExcel(void);
	afx_msg void OnExportTemplate(void);
	afx_msg void OnFilter(void);
	afx_msg void OnFilterOff(void);

	afx_msg void OnSettings(UINT nID);

	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

#endif