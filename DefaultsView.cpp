// DefaultsView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "DefaultsView.h"

#include "ReportClasses.h"

#include "ResLangFileReader.h"

#include <sstream>

///////////////////////////////////////////////////////////////////////////////////////////
// CMyPropertyGridItemDouble()
CMyPropertyGridItemDouble::CMyPropertyGridItemDouble() : 
			CXTPPropertyGridItemDouble(L"")	
{
}
	
CMyPropertyGridItemDouble::CMyPropertyGridItemDouble(LPCTSTR cap,double value,LPCTSTR format,int max_length,bool check) : 
			CXTPPropertyGridItemDouble(cap,value,format),nMaxLength(max_length),bCheck(check)

{
}

void CMyPropertyGridItemDouble::OnValueChanged(CString strValue)
{
	CString sVal = strValue;
	bool bIsCheckOK = false;

	//sVal.Replace(',','.');

	if (bCheck)
	{
		bIsCheckOK = (_tstof(sVal) >= 0.0);
	}
	else
	{
		bIsCheckOK = (_tstof(sVal) > 0.0);
	}

	if (!bIsCheckOK || strValue.GetLength() > nMaxLength || !isNumeric(strValue))
	{
		CString sTmp = L"";
		sTmp.Format(sToolTip,nMaxLength);
		AfxMessageBox(sTmp);
	}
	else
		CXTPPropertyGridItemDouble::OnValueChanged(strValue);
}

BOOL CMyPropertyGridItemDouble::isNumeric(LPCTSTR value)
{
	TCHAR szBuff[50];
	wcscpy(szBuff,value);
	BOOL bReturn = TRUE;
	for (TCHAR *p = szBuff;p < wcslen(szBuff)+szBuff;p++)
	{
		if (*p == '0' || *p == '1' || *p == '2' || *p == '3' || *p == '4' || *p == '5' || *p == '6' || *p == '7' || *p == '8' || *p == '9' || *p == ',' || *p == '.')
		{
			bReturn = TRUE;
		}
		else
		{
			bReturn = FALSE;
			break;
		}
	}
	return bReturn;

}


///////////////////////////////////////////////////////////////////////////////////////////
// CMyPropertyGridItemNumber()
CMyPropertyGridItemNumber::CMyPropertyGridItemNumber() : 
			CXTPPropertyGridItemNumber(L"")
{
}
	
CMyPropertyGridItemNumber::CMyPropertyGridItemNumber(LPCTSTR cap,long value,int max_length) : 
			CXTPPropertyGridItemNumber(cap,value),nMaxLength(max_length)
{
}

void CMyPropertyGridItemNumber::OnValueChanged(CString strValue)
{
	long nValue = 0;
	if (_tstoi(strValue) < 0 || strValue.GetLength() > nMaxLength || !isNumeric(strValue))
	{
		CString sTmp = L"";
		sTmp.Format(sToolTip,nMaxLength);
		AfxMessageBox(sTmp);
	}
	else
	 CXTPPropertyGridItemNumber::OnValueChanged(strValue);
}

BOOL CMyPropertyGridItemNumber::isNumeric(LPCTSTR value)
{
	TCHAR szBuff[50];
	wcscpy(szBuff,value);
	BOOL bReturn = TRUE;
	for (TCHAR *p = szBuff;p < wcslen(szBuff)+szBuff;p++)
	{
		if (*p == '0' || *p == '1' || *p == '2' || *p == '3' || *p == '4' || *p == '5' || *p == '6' || *p == '7' || *p == '8' || *p == '9')
		{
			bReturn = TRUE;
		}
		else
		{
			bReturn = FALSE;
			break;
		}
	}
	return bReturn;

}


///////////////////////////////////////////////////////////////////////////////////////////
// CDefaultsDoc

IMPLEMENT_DYNCREATE(CDefaultsDoc, CDocument)

BEGIN_MESSAGE_MAP(CDefaultsDoc, CDocument)
	//{{AFX_MSG_MAP(CDefaultsDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDefaultsDoc construction/destruction

CDefaultsDoc::CDefaultsDoc()
{
	// TODO: add one-time construction code here

}

CDefaultsDoc::~CDefaultsDoc()
{
}


BOOL CDefaultsDoc::OnNewDocument()
{

	// CHECK FOR LICENSE HERE!!!!! 2011-12-22 P�D
	if (!License())
	{
		return FALSE;
	}

	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CDefaultsDoc serialization

void CDefaultsDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CDefaultsDoc diagnostics

#ifdef _DEBUG
void CDefaultsDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CDefaultsDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

///////////////////////////////////////////////////////////////////////////////////////////
// CDefaultsFrame

IMPLEMENT_DYNCREATE(CDefaultsFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CDefaultsFrame, CMDIChildWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_SYSCOMMAND()
	//ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)

	ON_XTP_CREATECONTROL()
END_MESSAGE_MAP()


// CDefaultsFrame construction/destruction

XTPDockingPanePaintTheme CDefaultsFrame::m_themeCurrent = xtpPaneThemeOffice2003;

CDefaultsFrame::CDefaultsFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
	m_bInitReports = FALSE;
	m_bIsSysCommand = FALSE;
}

CDefaultsFrame::~CDefaultsFrame()
{
}

void CDefaultsFrame::OnDestroy(void)
{
}

void CDefaultsFrame::OnClose(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6513_KEY);
	SavePlacement(this, csBuf);

	if (!m_bIsSysCommand)
	{
		CDefaultsView *pView = (CDefaultsView*)getFormViewByID(IDD_FORMVIEW22);
		if (pView != NULL)
		{
			if (pView->doSave())
			{
				setNavBarButtons();
				CMDIChildWnd::OnClose();
			}
		}
	}
	else
	{
		setNavBarButtons();
		CMDIChildWnd::OnClose();
	}
}

void CDefaultsFrame::OnSysCommand(UINT nID,LPARAM lParam)
{
	if ((nID & 0xFFF0) == SC_CLOSE)
	{
		CDefaultsView *pView = (CDefaultsView*)getFormViewByID(IDD_FORMVIEW22);
		if (pView != NULL)
		{
			m_bIsSysCommand = TRUE;
			//QUIT_TYPES::Q_T_RETURN qtt;
			//qtt = pView->checkDataBeforeSave(CHECK_SAVE_TYPES::CHECK_SAVE_ON_QUIT);
			//if (qtt == QUIT_TYPES::QUIT_ALL_OK)
			//{
			if (pView->doSave())
			{
				setNavBarButtons();
				CMDIChildWnd::OnSysCommand(nID,lParam);
			}
		}

	}
	else if ((nID & 0xFFF0) != SC_CLOSE)
	{
		setNavBarButtons();
		CMDIChildWnd::OnSysCommand(nID,lParam);
	}
}

int CDefaultsFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	EnableDocking(CBRS_ALIGN_ANY);

	// Create and Load toolbar; 090107 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR6);
	m_wndToolBar.EnableDocking(xtpFlagAlignTop|xtpFlagStretched);

	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			//***************************************************************************************
			// Setup commandbars and manues; 051114 p�d
			CXTPToolBar* pToolBar = &m_wndToolBar;
			if (pToolBar->IsBuiltIn())
			{
				if (pToolBar->GetType() != xtpBarTypeMenuBar)
				{
					UINT nBarID = pToolBar->GetBarID();
					pToolBar->LoadToolBar(nBarID, FALSE);
					CXTPControls *p = pToolBar->GetControls();
					// Setup icons on toolbars, using resource dll; 051208 p�d
					if (nBarID == IDR_TOOLBAR6)
					{		
						setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_NEW,xml.str(IDS_STRING1220),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(1),RES_TB_DEL,xml.str(IDS_STRING1221),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(2),RES_TB_SAVE,xml.str(IDS_STRING1222),TRUE);	//

						p->GetAt(0)->SetVisible(FALSE);
						p->GetAt(1)->SetVisible(FALSE);

						p->GetAt(3)->SetVisible(FALSE);
						p->GetAt(4)->SetVisible(FALSE);
						p->GetAt(5)->SetVisible(FALSE);
						p->GetAt(6)->SetVisible(FALSE);
						p->GetAt(7)->SetVisible(FALSE);
						p->GetAt(8)->SetVisible(FALSE);
					}	// if (nBarID == IDR_TOOLBAR5)
				}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
			}	// if (pToolBar->IsBuiltIn())
			//***************************************************************************************

			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

int CDefaultsFrame::OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl)
{
	return TRUE;
}


LRESULT CDefaultsFrame::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	return 0L;
}

BOOL CDefaultsFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CDefaultsFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CDefaultsFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6513_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CDefaultsFrame::OnSetFocus(CWnd* pWnd)
{
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,ID_DO_SOMETHING_IN_SHELL,ID_LPARAM_COMMAND2);

	setNavBarButtons();

	CMDIChildWnd::OnSetFocus(pWnd);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CDefaultsFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	return 0L;
}


// CDefaultsFrame diagnostics

#ifdef _DEBUG
void CDefaultsFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CDefaultsFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CDefaultsFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = 450;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CDefaultsFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType,cx,cy);
}



// CDefaultsView

IMPLEMENT_DYNCREATE(CDefaultsView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CDefaultsView, CXTResizeFormView)
	ON_WM_DESTROY()
	ON_WM_COPYDATA()
	ON_WM_SIZE()

	ON_COMMAND_RANGE(ID_BUTTON32799,ID_BUTTON32806, OnCommand)
	ON_UPDATE_COMMAND_UI_RANGE(ID_BUTTON32799,ID_BUTTON32806, OnUpdateToolbar)

	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST22_1, OnItemchangedList)

	ON_MESSAGE(XTPWM_PROPERTYGRID_NOTIFY, OnGridNotify)

END_MESSAGE_MAP()

CDefaultsView::CDefaultsView()
	: CXTResizeFormView(CDefaultsView::IDD),
		m_bInitialized(FALSE),
		m_sLangFN(L""),
		m_pDB(NULL),
		m_bEnableToolBarBtnDelete(TRUE),
		m_bEnableToolBarBtnSave(TRUE)
{
}

CDefaultsView::~CDefaultsView()
{
}

void CDefaultsView::OnDestroy()
{
	CXTResizeFormView::OnDestroy();
}


BOOL CDefaultsView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CDefaultsView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL22_1, m_lbl22_1);
	DDX_Control(pDX, IDC_LBL22_2, m_lbl22_2);
	DDX_Control(pDX, IDC_LIST22_1, m_list22_1);
	//}}AFX_DATA_MAP
}

// CDefaultsView diagnostics

#ifdef _DEBUG
void CDefaultsView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CDefaultsView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG

// CDefaultsView message handlers
void CDefaultsView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();
	if (!m_bInitialized)
	{

		m_lbl22_1.SetLblFontEx(-1,FW_BOLD,TRUE);
		m_lbl22_2.SetLblFontEx(-1,FW_BOLD,TRUE);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		getDLLVolumeFuncDesc(m_vecFuncDesc,m_vecUserVolTables);

		setupComponents();

		setupPropGrid();

		setupUserFuncList();	

		setupUserVolFuncSelected();

		m_bInitialized = TRUE;
	}
}

BOOL CDefaultsView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{

	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		if (m_dbConnectionData.conn->isConnected())
		{
			m_pDB = new CDBHandling(m_dbConnectionData);
			if (m_pDB != NULL)
			{
				m_pDB->getUserVolTables(m_vecUserVolTables);
				m_pDB->getDefaults(m_vecDefaults);
				m_pDB->getDefaultUserVolFuncSel(m_vecDefUserVolFuncs);
				m_pDB->getGrades(m_vecGrades);
				m_pDB->getSpecies(m_vecSpecies);
			}
		}
	}

	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CDefaultsView::OnSize(UINT nType,int cx,int cy)
{
	if (m_lbl22_1.GetSafeHwnd())
	{
		setResize(&m_lbl22_1,2,4,360,30);
	}

	if (m_lbl22_2.GetSafeHwnd())
	{
		setResize(&m_lbl22_2,360,4,cx-365,30);
	}

	if (m_list22_1.GetSafeHwnd())
	{
		setResize(&m_list22_1,360,30,cx-365,cy-38);
	}

	if (m_propDefaults.GetSafeHwnd())
	{
		setResize(&m_propDefaults,2,30,350,cy-38);
	}

	CXTResizeFormView::OnSize(nType,cx,cy);
}

void CDefaultsView::OnCommand(UINT nID)
{
	switch(nID)
	{
		case ID_BUTTON32801 :	
			Save(); 
			break;
	};
}

void CDefaultsView::OnUpdateToolbar(CCmdUI* pCmdUI)
{
/*
	if (pCmdUI->m_nID == ID_BUTTON32800)
		pCmdUI->Enable(m_bEnableToolBarBtnDelete);
	if (pCmdUI->m_nID == ID_BUTTON32801)
		pCmdUI->Enable(m_bEnableToolBarBtnSave);
*/
}

BOOL CDefaultsView::doSave()	
{ 	
	return Save();
}

void CDefaultsView::OnItemchangedList(NMHDR* pNMHDR, LRESULT* pResult) 
{
	int nSelRow = -1;
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	pResult = 0;
	if (pNMLV != NULL)
	{
		nSelRow =  pNMLV->iItem;
	}
	if (!checkNumberOfUserFunctionsSelected(TRUE))
		m_list22_1.SetCheck(nSelRow,FALSE);
}


LRESULT CDefaultsView::OnGridNotify(WPARAM wParam, LPARAM lParam)
{
	return FALSE;
}

BOOL CDefaultsView::checkNumberOfUserFunctionsSelected(BOOL show_msg)
{
	BOOL bReturn = TRUE;
	int nCount = 0;
	// R�kna antal valda
	for (int i = 0;i < m_list22_1.GetItemCount();i++)
	{
		if (m_list22_1.GetCheck(i))
			nCount++;
	}
	if (nCount > MAX_NUMBER_OF_EXTRA_VFUNC)
	{
		if (show_msg)
		{
			::MessageBox(GetSafeHwnd(),m_sMsgMaxUserdefFunctions,m_sMsgCap,MB_ICONSTOP | MB_OK);
		}
		bReturn = FALSE;
	}

	return bReturn;
}

void CDefaultsView::setupUserVolFuncSelected()
{
	if (m_vecDefUserVolFuncs.size() > 0 && m_list22_1.GetItemCount() > 0)
	{
		for (int i = 0; i < m_list22_1.GetItemCount();i++)
		{
			m_list22_1.SetCheck(i,FALSE);
		}
		for (int i1 = 0; i1 < m_list22_1.GetItemCount();i1++)
		{
			for (UINT i2 = 0;i2 < m_vecDefUserVolFuncs.size();i2++)
			{
				if (m_vecDefUserVolFuncs[i2].getFuncID() == (int)m_list22_1.GetItemData(i1))
				{
					m_list22_1.SetCheck(i1,TRUE);
					break;
				}	// if 
			}	// for 
		}	// for 
	}	// if 
	else if (m_vecDefUserVolFuncs.size() == 0 && m_list22_1.GetItemCount() > 0)
	{
		for (int i1 = 0; i1 < m_list22_1.GetItemCount();i1++)
		{
			m_list22_1.SetCheck(i1,FALSE);
		}	// for (int i1 = 0; i1 < m_list22_1.GetItemCount();i1++)
	}
}

void CDefaultsView::setupComponents()
{
	if (m_propDefaults.GetSafeHwnd() == 0)
	{
		m_propDefaults.Create(CRect(0, 0, 0, 0), this, 1000);
		m_propDefaults.SetOwner(this);
		m_propDefaults.ShowHelp( FALSE );
		m_propDefaults.SetViewDivider(0.55);
		m_propDefaults.SetTheme(xtpGridThemeOffice2003);
	}
	if (m_propDefaults.GetSafeHwnd())
	{
		CRect rect;
		GetClientRect(&rect);
		setResize(&m_propDefaults,2,30,350,rect.bottom-38);
	}

}
// Also set strings
void CDefaultsView::setupPropGrid()
{
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			m_sMsgCap = xml.str(IDS_STRING99);
			m_sMsgMaxUserdefFunctions.Format(xml.str(IDS_STRING4463)+L"\n%s\n",
					MAX_NUMBER_OF_EXTRA_VFUNC,
					xml.str(IDS_STRING4464));

			tokenizeString(xml.str(IDS_STRING201),';',m_sarrLoadTypes);
			tokenizeString(xml.str(IDS_STRING202),';',m_sarrDeductionTypes);

			m_lbl22_1.SetWindowTextW(xml.str(IDS_STRING1650));
			CString S;
			S.Format(xml.str(IDS_STRING1651),MAX_NUMBER_OF_EXTRA_VFUNC);
			m_lbl22_2.SetWindowTextW(S);

			m_sYes = xml.str(IDS_STRING106);
			m_sNo = xml.str(IDS_STRING107);

			m_sMsgToolTipNoGTZero = xml.str(IDS_STRING1652);
			m_sMsgToolTipNoGTZero2 = xml.str(IDS_STRING1653);

			m_sMsgToolTipNoGTorEQZero= xml.str(IDS_STRING1654);
			m_sMsgToolTipNoGTorEQZero2 = xml.str(IDS_STRING1655);

			m_sSettingsSWtext = xml.str(IDS_STRING1601);	//#4258
			m_sSettingsPWtext = xml.str(IDS_STRING1602);

			if (m_propDefaults.GetSafeHwnd() != 0)
			{
				CXTPPropertyGridItem* pSettings  = NULL;
				CXTPPropertyGridItem *pItem = NULL;
				CXTPPropertyGridItemNumber *pItemNumber = NULL;
				CXTPPropertyGridItemDouble *pItemDouble = NULL;
				CMyPropertyGridItemNumber *pMyItemNumber = NULL;
				CMyPropertyGridItemDouble *pMyItemDouble = NULL;
				pSettings  = m_propDefaults.AddCategory(xml.str(IDS_STRING1600));
				if (pSettings != NULL)
				{
					// Sawlog volume unit
					if ((pItem = pSettings->AddChildItem(new CXTPPropertyGridItem(xml.str(IDS_STRING1601)))) != NULL)
					{
						pItem->BindToString(&m_sSawlogVolumeUnit);
						if (m_vecFuncDesc.size() > 0)
						{
							for (UINT i1 = 0;i1 < m_vecFuncDesc.size();i1++)
							{
								m_recFuncDesc = m_vecFuncDesc[i1];
								pItem->GetConstraints()->AddConstraint(m_recFuncDesc.getBasis(),m_recFuncDesc.getFuncID());	
							}
						}
						pItem->SetFlags(xtpGridItemHasComboButton);
						pItem->SetConstraintEdit( FALSE );
						m_nSawlogFuncID = getDefaultFuncID(DEF_SAWLOG,m_vecDefaults);	//#4258 h�mtar ut FuncID
						m_sSawlogVolumeUnit = getDefaults(DEF_SAWLOG,m_vecDefaults,m_vecFuncDesc);
					}
					// Pulpwood volume unit
					if ((pItem = pSettings->AddChildItem(new CXTPPropertyGridItem(xml.str(IDS_STRING1602)))) != NULL)
					{
						pItem->BindToString(&m_sPulpwoodVolumeUnit);
						if (m_vecFuncDesc.size() > 0)
						{
							for (UINT i1 = 0;i1 < m_vecFuncDesc.size();i1++)
							{
								m_recFuncDesc = m_vecFuncDesc[i1];
								if (m_recFuncDesc.getOnlySW() == 0)
								{
									pItem->GetConstraints()->AddConstraint(m_recFuncDesc.getBasis(),m_recFuncDesc.getFuncID());	
								}
							}
						}
						pItem->SetFlags(xtpGridItemHasComboButton);
						pItem->SetConstraintEdit( FALSE );
						m_nPulpwoodFuncID = getDefaultFuncID(DEF_PULPWOOD,m_vecDefaults);	//#4258 h�mtar ut FuncID
						m_sPulpwoodVolumeUnit = getDefaults(DEF_PULPWOOD,m_vecDefaults,m_vecFuncDesc);
					}
					// Deduction type
					if ((pItem = pSettings->AddChildItem(new CXTPPropertyGridItem(xml.str(IDS_STRING1603)))) != NULL)
					{
						pItem->BindToString(&m_sDeductionType);
						if (m_sarrDeductionTypes.GetCount() > 0)
						{
							for (int i1 = 0;i1 < m_sarrDeductionTypes.GetCount();i1++)
							{
								pItem->GetConstraints()->AddConstraint(m_sarrDeductionTypes.GetAt(i1),i1);
							}
						}

						pItem->SetFlags(xtpGridItemHasComboButton);
						pItem->SetConstraintEdit( FALSE );
						m_sDeductionType = getDefaults(DEF_DEDUCTION,m_vecDefaults,m_vecFuncDesc);
					}
					m_sSpeciesCode = L"1";
					m_sLogGrade = L"SW";
					/*
					// Species code
					if ((pItem = pSettings->AddChildItem(new CXTPPropertyGridItem(xml.str(IDS_STRING1604)))) != NULL)
					{
						pItem->BindToString(&m_sSpeciesCode);
						if (m_vecSpecies.size() > 0)
						{
							for (UINT i1 = 0;i1 < m_vecSpecies.size();i1++)
							{
								m_recSpecies = m_vecSpecies[i1];
								pItem->GetConstraints()->AddConstraint(m_recSpecies.getSpcCode(),m_recSpecies.getSpcID());
							}
						}
						pItem->SetFlags(xtpGridItemHasComboButton);
						pItem->SetConstraintEdit( FALSE );
						m_sSpeciesCode = getDefaults(DEF_SPC_CODE,m_vecDefaults,m_vecFuncDesc);
					}
					// Log grade
					if ((pItem = pSettings->AddChildItem(new CXTPPropertyGridItem(xml.str(IDS_STRING1605)))) != NULL)
					{
						pItem->BindToString(&m_sLogGrade);
						if (m_vecGrades.size() > 0)
						{
							for (UINT i1 = 0;i1 < m_vecGrades.size();i1++)
							{
								m_recGrades = m_vecGrades[i1];
								pItem->GetConstraints()->AddConstraint(m_recGrades.getGradesCode(),m_recGrades.getGradesID());
							}
						}
						pItem->SetFlags(xtpGridItemHasComboButton);
						pItem->SetConstraintEdit( FALSE );
						m_sLogGrade = getDefaults(DEF_LOG_GRADE,m_vecDefaults,m_vecFuncDesc);
					}
					*/
					// Load type
					if ((pItem = pSettings->AddChildItem(new CXTPPropertyGridItem(xml.str(IDS_STRING1606)))) != NULL)
					{
						pItem->BindToString(&m_sLoadType);
						if (m_sarrLoadTypes.GetCount() > 0)
						{
							for (int i1 = 0;i1 < m_sarrLoadTypes.GetCount();i1++)
							{
								pItem->GetConstraints()->AddConstraint(m_sarrLoadTypes.GetAt(i1),i1);
							}
						}
						pItem->SetFlags(xtpGridItemHasComboButton);
						pItem->SetConstraintEdit( FALSE );
						m_sLoadType = getDefaults(DEF_LOAD_TYPE,m_vecDefaults,m_vecFuncDesc);
					}
					/*
					// Frequency
					if ((pMyItemNumber = (CMyPropertyGridItemNumber*)pSettings->AddChildItem(new CMyPropertyGridItemNumber(xml.str(IDS_STRING1607),0,1))) != NULL)
					{
						pMyItemNumber->setToolTip(m_sMsgToolTipNoNegativeValues2);
						pMyItemNumber->BindToNumber(&m_nFrequency);
						m_nFrequency = _tstoi(getDefaults(DEF_FREQUENCY,m_vecDefaults,m_vecFuncDesc));

					}
					*/
					m_nFrequency = 1;
					// Log length (inch)
					if ((pMyItemDouble = (CMyPropertyGridItemDouble*)pSettings->AddChildItem(new CMyPropertyGridItemDouble(xml.str(IDS_STRING16080),0.0,L"%.1f",3,true))) != NULL)
					{
						pMyItemDouble->setToolTip(m_sMsgToolTipNoGTorEQZero);
						//pMyItemDouble->UseSystemDecimalSymbol(TRUE);
						pMyItemDouble->BindToDouble(&m_fLogLengthInch);
						m_fLogLengthInch = _tstof(localeToStr(getDefaults(DEF_LOG_LENGTH_INCH,m_vecDefaults,m_vecFuncDesc)));

					}
					// Log length (m)
					if ((pMyItemDouble = (CMyPropertyGridItemDouble*)pSettings->AddChildItem(new CMyPropertyGridItemDouble(xml.str(IDS_STRING16081),0.0,L"%.2f",5,true))) != NULL)
					{
						pMyItemDouble->setToolTip(m_sMsgToolTipNoGTorEQZero);
						//pMyItemDouble->UseSystemDecimalSymbol(TRUE);
						pMyItemDouble->BindToDouble(&m_fLogLengthMM);
						m_fLogLengthMM = _tstof(localeToStr(getDefaults(DEF_LOG_LENGTH_MM,m_vecDefaults,m_vecFuncDesc)));

					}
					// Use southern doyle
					if ((pItem = (CXTPPropertyGridItem*)pSettings->AddChildItem(new CXTPPropertyGridItem(xml.str(IDS_STRING1609)))) != NULL)
					{
						pItem->SetFlags(xtpGridItemHasComboButton);
						pItem->BindToString(&m_sUseSouthernDoyle);
						pItem->SetConstraintEdit(FALSE);
						pItem->GetConstraints()->AddConstraint(m_sYes);
						pItem->GetConstraints()->AddConstraint(m_sNo);
						if (_tstoi(getDefaults(DEF_USE_S_DOYLE,m_vecDefaults,m_vecFuncDesc)) == 0)
							m_sUseSouthernDoyle = m_sNo;
						else if (_tstoi(getDefaults(DEF_USE_S_DOYLE,m_vecDefaults,m_vecFuncDesc)) == 1)
							m_sUseSouthernDoyle = m_sYes;

					}
					// Round scaling
					if ((pItem = (CXTPPropertyGridItem*)pSettings->AddChildItem(new CXTPPropertyGridItem(xml.str(IDS_STRING1610)))) != NULL)
					{
						pItem->SetFlags(xtpGridItemHasComboButton);
						pItem->BindToString(&m_sRoundScaling);
						pItem->SetConstraintEdit(FALSE);
						pItem->GetConstraints()->AddConstraint(m_sYes);
						pItem->GetConstraints()->AddConstraint(m_sNo);
						if (_tstoi(getDefaults(DEF_ROUND_SC_DIAM,m_vecDefaults,m_vecFuncDesc)) == 0)
							m_sRoundScaling = m_sNo;
						else if (_tstoi(getDefaults(DEF_ROUND_SC_DIAM,m_vecDefaults,m_vecFuncDesc)) == 1)
							m_sRoundScaling = m_sYes;
					}
					/* NOT USED, RATIO SET PER SPECIES
					// Bark-ratio
					if ((pItem = (CXTPPropertyGridItem*)pSettings->AddChildItem(new CXTPPropertyGridItem(xml.str(IDS_STRING1612)))) != NULL)
					{
						pItem->BindToString(&m_sBarkRatio);
						m_sBarkRatio = getDefaults(DEF_BARK_RATIO,m_vecDefaults,m_vecFuncDesc);

					}
					*/
					m_sBarkRatio = L"0.0";
					// Pounds/Cubic foot

					// Taper IB
					if ((pMyItemDouble = (CMyPropertyGridItemDouble*)pSettings->AddChildItem(new CMyPropertyGridItemDouble(xml.str(IDS_STRING1613),0.0,L"%.3f",5,false))) != NULL)
					{
						pMyItemDouble->setToolTip(m_sMsgToolTipNoGTZero);
						//pMyItemDouble->UseSystemDecimalSymbol(TRUE);
						pMyItemDouble->BindToDouble(&m_fTaperIB);
						m_fTaperIB = _tstof(localeToStr(getDefaults(DEF_TAPER_IB,m_vecDefaults,m_vecFuncDesc)));
					}
/*
					if ((pItem = (CXTPPropertyGridItem*)pSettings->AddChildItem(new CXTPPropertyGridItem(xml.str(IDS_STRING1613)))) != NULL)
					{
						pItem->BindToString(&m_sTaperIB);
						m_sTaperIB = getDefaults(DEF_TAPER_IB,m_vecDefaults,m_vecFuncDesc);
					}
*/
					// Pounds/Cubic foot
/*
					if ((pItem = (CXTPPropertyGridItem*)pSettings->AddChildItem(new CXTPPropertyGridItem(xml.str(IDS_STRING1611)))) != NULL)
					{
						pItem->BindToString(&m_sPoundsPerCubicFoot);
						m_sPoundsPerCubicFoot = getDefaults(DEF_POUNDS_CUBIC_FOOT,m_vecDefaults);
					}
*/			
					pSettings->Expand();

				}
			}
			xml.clean();
		}
	}
}

void CDefaultsView::setupUserFuncList()
{
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{

			m_list22_1.SetExtendedStyle(m_list22_1.GetStyle()|LVS_EX_CHECKBOXES|LVS_EX_GRIDLINES|LVS_EX_FULLROWSELECT);
			m_list22_1.InsertColumn(0,xml.str(IDS_STRING4450),LVCFMT_LEFT,40);
			m_list22_1.InsertColumn(1,xml.str(IDS_STRING4451),LVCFMT_LEFT,200);
			m_list22_1.InsertColumn(2,xml.str(IDS_STRING4452),LVCFMT_LEFT,120);
			m_list22_1.InsertColumn(3,xml.str(IDS_STRING4453),LVCFMT_LEFT,120);
			m_list22_1.InsertColumn(4,xml.str(IDS_STRING4454),LVCFMT_LEFT,120);
			// Add volume-functions to listctrl
			if (m_vecFuncDesc.size() > 0)
			{
				for (UINT i = 0;i < m_vecFuncDesc.size();i++)
				{
					if (m_vecFuncDesc[i].getFuncID() < 1000)
					{
						InsertRow(m_list22_1,i,false,5,L"",
							m_vecFuncDesc[i].getFullName(),
							m_vecFuncDesc[i].getAbbrevName(),
							m_vecFuncDesc[i].getBasis(),
							xml.str(IDS_STRING44540));
					}
					else
					{
						InsertRow(m_list22_1,i,false,5,L"",
							m_vecFuncDesc[i].getFullName(),
							m_vecFuncDesc[i].getAbbrevName(),
							m_vecFuncDesc[i].getBasis(),
							xml.str(IDS_STRING44541));
					}
					m_list22_1.SetItemData(i,(DWORD)m_vecFuncDesc[i].getFuncID());
				}	
			}
			xml.clean();
		}
	}
}

CString CDefaultsView::getCalcTypeValue(LPCTSTR basis_value)	
{
	CString S;
	if (m_vecFuncDesc.size() > 0)
	{
		for (UINT i = 0;i < m_vecFuncDesc.size();i++)
		{
			if (wcscmp(m_vecFuncDesc[i].getBasis(),basis_value) == 0)
			{
				return m_vecFuncDesc[i].getAbbrevName();
			}
		}
	}
	return L"";
}

//#4258 h�mta mha FuncID ist�llet
CString CDefaultsView::getCalcTypeValue(int func_id)
{
	CString S;
	if (m_vecFuncDesc.size() > 0)
	{
		for (UINT i = 0;i < m_vecFuncDesc.size();i++)
		{
			if (m_vecFuncDesc[i].getFuncID() == func_id)	
			{
				return m_vecFuncDesc[i].getAbbrevName();
			}
		}
	}
	return L"";
}

CDefUserVolFuncs CDefaultsView::getSelUserFunc(int func_id)
{
	if (m_vecFuncDesc.size() > 0)
	{
		for (UINT i = 0;i < m_vecFuncDesc.size();i++)
		{
			if (m_vecFuncDesc[i].getFuncID() == func_id)
				return CDefUserVolFuncs(m_vecFuncDesc[i].getFuncID(),
																m_vecFuncDesc[i].getFullName(),
																m_vecFuncDesc[i].getAbbrevName(),
																m_vecFuncDesc[i].getBasis());
		}
	}
	return CDefUserVolFuncs();
}

BOOL CDefaultsView::Save()
{
	CString sFrequency = L"";
	CString sLogLength = L"";

/*
	if (m_fTaperIB <= 0.0)
	{
		AfxMessageBox(L"TaperIB must be GT 0.0");
		return FALSE;
	}
*/
	//////////////////////////////////////////////////////////////////////////
	// Save default settings
	if (m_pDB != NULL)	//#4258 h�mtar ut FuncID ist�llet
	{
		CXTPPropertyGridItems* pItems = NULL;
		CXTPPropertyGridItem *pItem = NULL;
		CXTPPropertyGridItemConstraint *pConstraint = NULL;
		int nCurrent;
		pItems = m_propDefaults.GetCategories();
		if(pItems != NULL)
		{
			pItem = pItems->FindItem(m_sSettingsSWtext);
			if (pItem != NULL)
			{
				if((nCurrent = pItem->GetConstraints()->GetCurrent()) >= 0)
				{
					pConstraint = pItem->GetConstraints()->GetConstraintAt(nCurrent);
					if(pConstraint != NULL)
					{
						m_nSawlogFuncID = pConstraint->m_dwData;
					}
				}
			}

			pItem = pItems->FindItem(m_sSettingsPWtext);
			if (pItem != NULL)
			{
				if((nCurrent = pItem->GetConstraints()->GetCurrent()) >= 0)
				{
					pConstraint = pItem->GetConstraints()->GetConstraintAt(nCurrent);
					if(pConstraint != NULL)
					{
						m_nPulpwoodFuncID = pConstraint->m_dwData;
					}
				}
			}
		}
		
		m_pDB->updDefaults(CDefaults(DEF_SAWLOG,m_nSawlogFuncID>0? getCalcTypeValue(m_nSawlogFuncID):getCalcTypeValue(m_sSawlogVolumeUnit),m_sSawlogVolumeUnit,_T(""),_T(""),m_nSawlogFuncID));	//#4258 spara FuncID i db ox�
		m_pDB->updDefaults(CDefaults(DEF_PULPWOOD,m_nPulpwoodFuncID>0? getCalcTypeValue(m_nPulpwoodFuncID):getCalcTypeValue(m_sPulpwoodVolumeUnit),m_sPulpwoodVolumeUnit,_T(""),_T(""),m_nPulpwoodFuncID));	//#4258 spara FuncID i db ox�
		m_pDB->updDefaults(CDefaults(DEF_DEDUCTION,m_sDeductionType,m_sDeductionType));
//		m_pDB->updDefaults(CDefaults());
//		m_pDB->updDefaults(-1);
		m_pDB->updDefaults(CDefaults(DEF_LOAD_TYPE,m_sLoadType,m_sLoadType));
		sFrequency.Format(L"%d",m_nFrequency);
		m_pDB->updDefaults(CDefaults(DEF_FREQUENCY,sFrequency,sFrequency));
		
		sLogLength.Format(L"%.1f",m_fLogLengthInch);
		if (m_pDB->isDefValueAdded(DEF_LOG_LENGTH_INCH))
			m_pDB->updDefaults(CDefaults(DEF_LOG_LENGTH_INCH,sLogLength,sLogLength));
		else
			m_pDB->addDefaults(CDefaults(DEF_LOG_LENGTH_INCH,sLogLength,sLogLength));

		sLogLength.Format(L"%.2f",m_fLogLengthMM);
		if (m_pDB->isDefValueAdded(DEF_LOG_LENGTH_MM))
			m_pDB->updDefaults(CDefaults(DEF_LOG_LENGTH_MM,sLogLength,sLogLength));
		else
			m_pDB->addDefaults(CDefaults(DEF_LOG_LENGTH_MM,sLogLength,sLogLength));
		
		if (m_sUseSouthernDoyle.CompareNoCase(m_sNo) == 0)
		{
			if (m_pDB->isDefValueAdded(DEF_USE_S_DOYLE))
				m_pDB->updDefaults(CDefaults(DEF_USE_S_DOYLE,L"0",L"0"));
			else
				m_pDB->addDefaults(CDefaults(DEF_USE_S_DOYLE,L"0",L"0"));
		}
		else if (m_sUseSouthernDoyle.CompareNoCase(m_sYes) == 0)
		{
			if (m_pDB->isDefValueAdded(DEF_USE_S_DOYLE))
				m_pDB->updDefaults(CDefaults(DEF_USE_S_DOYLE,L"1",L"1"));
			else
				m_pDB->addDefaults(CDefaults(DEF_USE_S_DOYLE,L"1",L"1"));
		}

		if (m_sRoundScaling.CompareNoCase(m_sNo) == 0)
		{
			if (m_pDB->isDefValueAdded(DEF_ROUND_SC_DIAM))
				m_pDB->updDefaults(CDefaults(DEF_ROUND_SC_DIAM,L"0",L"0"));
			else
				m_pDB->addDefaults(CDefaults(DEF_ROUND_SC_DIAM,L"0",L"0"));
		}
		else if (m_sRoundScaling.CompareNoCase(m_sYes) == 0)
		{
			if (m_pDB->isDefValueAdded(DEF_ROUND_SC_DIAM))
				m_pDB->updDefaults(CDefaults(DEF_ROUND_SC_DIAM,L"1",L"1"));
			else
				m_pDB->addDefaults(CDefaults(DEF_ROUND_SC_DIAM,L"1",L"1"));
		}

		if (m_pDB->isDefValueAdded(DEF_POUNDS_CUBIC_FOOT))
			m_pDB->updDefaults(CDefaults(DEF_POUNDS_CUBIC_FOOT,m_sPoundsPerCubicFoot,m_sPoundsPerCubicFoot));
		else
			m_pDB->addDefaults(CDefaults(DEF_POUNDS_CUBIC_FOOT,m_sPoundsPerCubicFoot,m_sPoundsPerCubicFoot));

		if (m_pDB->isDefValueAdded(DEF_BARK_RATIO))
			m_pDB->updDefaults(CDefaults(DEF_BARK_RATIO,localeToStr(m_sBarkRatio),localeToStr(m_sBarkRatio)));
		else
			m_pDB->addDefaults(CDefaults(DEF_BARK_RATIO,localeToStr(m_sBarkRatio),localeToStr(m_sBarkRatio)));

		m_sTaperIB.Format(L"%.3f",m_fTaperIB);
		if (m_pDB->isDefValueAdded(DEF_TAPER_IB))
			m_pDB->updDefaults(CDefaults(DEF_TAPER_IB,localeToStr(m_sTaperIB),localeToStr(m_sTaperIB)));
		else
			m_pDB->addDefaults(CDefaults(DEF_TAPER_IB,localeToStr(m_sTaperIB),localeToStr(m_sTaperIB)));

		//////////////////////////////////////////////////////////////////////////
		// Save default user volume-functions
		//------------------------------------------------------------
		// First; Clear table
		m_pDB->delDefaultUserVolFuncSel();
		// * Selected extra volumefunctions *
		if (m_list22_1.GetItemCount() > 0)
		{
			for (int i = 0;i < m_list22_1.GetItemCount();i++)
			{
				if (m_list22_1.GetCheck(i))
				{
					int nFuncID = (int)m_list22_1.GetItemData(i);
					m_pDB->addDefaultUserVolFuncSel(getSelUserFunc(nFuncID));
				}	// if 
			}	// for 
		}	// if 
		//------------------------------------------------------------
	}

	return TRUE;
}
