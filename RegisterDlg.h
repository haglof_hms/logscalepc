#pragma once

#include "dbhandling.h"
#include "RegisterDlg.h"



// CRegisterDlg form view

class CRegisterDlg : public CXTResizeDialog //CXTResizeFormView
{
	DECLARE_DYNCREATE(CRegisterDlg)

	// Data members
	BOOL m_bInitialized;
	CString m_sLangFN;

	CString m_sMsgCap;
	CString m_sMsgDelete1;
	CString m_sMsgDelete2;
	CString m_sMsgMandatoryDataMissing;

	CButton m_btnOK;
	CButton m_btnCancel;
	CButton m_btnAddItem;
	CButton m_btnDelItem;

	CMyExtStatic m_lbl4_1;
	CMyComboBox m_cboxRegisterTypes;

	CMyReportControl m_repRegister;

	CDBHandling *m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;

	CVecRegister m_vecRegister;
	int m_nSelectedType;

	// Methods
	void setupReport();
	void populateReport();

	void Add();
	void Delete();
	BOOL Save();

public:
	CRegisterDlg();           // protected constructor used by dynamic creation
	virtual ~CRegisterDlg();

public:
	enum { IDD = IDD_FORMVIEW4 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	//{{AFX_VIRTUAL(CRegisterDlg)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CRegisterDlg)
	afx_msg void OnDestroy(void);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);

	afx_msg void OnCbnSelchangeCombo41();
	afx_msg void OnCbnDropdownCombo41();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


