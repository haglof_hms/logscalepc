// XTPPreviewView.h : header file
//
// This file is a part of the XTREME TOOLKIT PRO MFC class library.
// (c)1998-2006 Codejock Software, All Rights Reserved.
//
// THIS SOURCE FILE IS THE PROPERTY OF CODEJOCK SOFTWARE AND IS NOT TO BE
// RE-DISTRIBUTED BY ANY MEANS WHATSOEVER WITHOUT THE EXPRESSED WRITTEN
// CONSENT OF CODEJOCK SOFTWARE.
//
// THIS SOURCE CODE CAN ONLY BE USED UNDER THE TERMS AND CONDITIONS OUTLINED
// IN THE XTREME TOOLKIT PRO LICENSE AGREEMENT. CODEJOCK SOFTWARE GRANTS TO
// YOU (ONE SOFTWARE DEVELOPER) THE LIMITED RIGHT TO USE THIS SOFTWARE ON A
// SINGLE COMPUTER.
//
// CONTACT INFORMATION:
// support@codejock.com
// http://www.codejock.com
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(_XTPPREVIEWVIEW_H__)
#define _XTPPREVIEWVIEW_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxpriv.h>

#include "ResLangFileReader.h"

class CDic  
{
public:
	static bool LoadLanguage(LPCTSTR strFilepath);
	static CString GetText(LPCTSTR strSection, LPCTSTR strID, LPCTSTR strDefaultText, bool bTransform = false);
	CDic();
	virtual ~CDic();

private:
	static void TransformSpecChars(CString &strTransform);
	static bool m_bLoaded;
protected:
	static CMapStringToString m_Map;
};


class CLangPreviewView : public CPreviewView  
{
	CString m_sLangFN;
	CString m_sLangAbrev;
	CString m_sPrintOut;
	CString m_sNextPage;
	CString m_sPrevPage;
	CString m_sZoomIn;
	CString m_sZoomOut;
	CString m_sClose;
	CString m_sOnePage;
	CString m_sTwoPages;
public:
	DECLARE_DYNCREATE(CLangPreviewView)
	CLangPreviewView();
	virtual ~CLangPreviewView();

	void OnActivateView( BOOL bActivate, CView* pActivateView, CView* pDeactiveView );

// Generierte Message-Map-Funktionen
protected:
	//{{AFX_MSG(CLangPreviewView)
	afx_msg void OnUpdateNumPageChange(CCmdUI* pCmdUI);
	afx_msg void OnPreviewPrint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(_XTPPREVIEWVIEW_H__)
