// UserCreateVolFunc.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "UserCreateVolFunc.h"

#include "ReportClasses.h"

#include "SelectSpeciesDlg.h"

#include "SelectExcelSheetDlg.h"

#include "NoMatchSpeciesDlg.h"

#include "ResLangFileReader.h"

#include <sstream>

#include "libxl.h"

using namespace libxl;

#define EXCEL 1
#define CSV 2

// "normal" constructor
CExcelImportLibXl::CExcelImportLibXl()
{
	m_Book = NULL;
}

// Open spreadsheet for reading and writing
CExcelImportLibXl::CExcelImportLibXl(CString File, CString SheetOrSeparator, bool Backup, bool Convert_Numbers)
{
}

int CExcelImportLibXl::OpenExcel(CString csFile, CString csSheetOrSeparator, bool bBackup, bool bConvert_Numbers)
{
	m_Book = NULL;

	if(csFile.Right(4) == _T(".xls") || csFile.Right(5) == _T(".xlsx"))
	{
		m_nFiletype = EXCEL;
		return Open(csFile);
	}
	else // if file is a text delimited file
	{
		m_sSeparator = csSheetOrSeparator;
		m_nFiletype = CSV;
		try
		{
			readUNICODESelectedFile(CStdioFileEx::GetCurrentLocaleCodePage(), csFile, m_aRows);

			if (m_aRows.GetCount() > 0)
			{
				// Read and store all rows in memory
				ReadRow(m_aFieldNames, 0); // Get field names i.e header row
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		catch(...)
		{
			return FALSE;
		}
	}

	return TRUE;
}

int CExcelImportLibXl::Open(CString csFile)
{
	if(csFile.Right(4) == _T(".xls") || csFile.Right(5) == _T(".xlsx"))
	{
		m_nFiletype = EXCEL;
		m_nSheetIndex = 0;	// use first sheet as default

		if(csFile.Right(4) == _T(".xls"))
			m_Book = xlCreateBook();
		else if(csFile.Right(5) == _T(".xlsx"))
			m_Book = xlCreateXMLBook();

		if(m_Book)
		{
			if( m_Book->load(csFile) )
			{
				m_Book->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0hfeflb");
				return TRUE;
			}
			else	// unable to open Excel-file
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}

	return FALSE;
}

// Perform some cleanup functions
CExcelImportLibXl::~CExcelImportLibXl()
{
	if(m_Book)
		m_Book->release();
}

long CExcelImportLibXl::GetTotalRows()
{
	if(m_nFiletype == EXCEL)
	{
		if(!m_Book) return 0;

		Sheet* sheet = m_Book->getSheet(m_nSheetIndex);
		if(sheet)
		{
			return sheet->lastRow();
		}
	}
	else if(m_nFiletype == CSV)
	{
		// Get total number of rows
		return m_aRows.GetSize();
	}

	return 0;
}

// get the top row
void CExcelImportLibXl::GetFieldNames(CStringArray &FieldNames)
{
	if(m_nFiletype == EXCEL)
	{
		CString csBuf;
		int row = 0;

		FieldNames.RemoveAll();
		if(!m_Book) return;
		Sheet* sheet = m_Book->getSheet(m_nSheetIndex);
		if(sheet)
		{
			row = sheet->firstRow();
			for(int col = sheet->firstCol(); col < sheet->lastCol(); ++col)
			{
				CellType cellType = sheet->cellType(row, col);
				if(cellType == CELLTYPE_STRING)
				{
					csBuf = sheet->readStr(row, col);
					FieldNames.Add(csBuf);
				}
			}
		}
	}
	else if(m_nFiletype == CSV)
	{
		FieldNames.Copy(m_aFieldNames);
	}
}

bool CExcelImportLibXl::ReadRow(CStringArray &RowValues, long row) // Read a row from spreadsheet. Default is read the next row
{
	if(m_nFiletype == EXCEL)
	{
		CString csBuf;

		RowValues.RemoveAll();
		if(!m_Book) return false;
		Sheet* sheet = m_Book->getSheet(m_nSheetIndex);
		if(sheet)
		{
			for(int col = sheet->firstCol(); col < sheet->lastCol(); ++col)
			{
				CellType cellType = sheet->cellType(row, col);
				if(cellType == CELLTYPE_STRING)
				{
					csBuf = sheet->readStr(row, col);
					RowValues.Add(csBuf);
				}
				else if(cellType == CELLTYPE_NUMBER)
				{
					csBuf.Format(_T("%g"), sheet->readNum(row, col));
					RowValues.Add(csBuf);
				}
				else
				{
					RowValues.Add(_T(""));
				}
			}
		}
	}
	else if(m_nFiletype == CSV)
	{
		row++;

		CString S;
		// Check if row entered is more than number of rows in sheet
		if (row <= m_aRows.GetSize())
		{
			if (row != 0)
			{
				m_dCurrentRow = row;
			}
			else if (m_dCurrentRow > m_aRows.GetSize())
			{
				return false;
			}
			// Read the desired row
			RowValues.RemoveAll();
			CString csTmp;
			csTmp = m_aRows.GetAt(m_dCurrentRow-1);
			m_dCurrentRow++;

			// Search for separator to split row
			int separatorPosition;
			CString csTmpSql;
			csTmpSql.Format(_T("\"%s\""), m_sSeparator);
			separatorPosition = csTmp.Find(csTmpSql); // If separator is "?"
			if (separatorPosition != -1)
			{
				// Save columns
				int nCount = 0;
				int stringStartingPosition = 0;
				while (separatorPosition != -1)
				{
					nCount = separatorPosition - stringStartingPosition;
					RowValues.Add(csTmp.Mid(stringStartingPosition, nCount));
					stringStartingPosition = separatorPosition + csTmpSql.GetLength();
					separatorPosition = csTmp.Find(csTmpSql, stringStartingPosition);
				}
				nCount = csTmp.GetLength() - stringStartingPosition;
				RowValues.Add(csTmp.Mid(stringStartingPosition, nCount));
				// Remove quotes from first column
				csTmp = RowValues.GetAt(0);
				csTmp.Delete(0, 1);
				RowValues.SetAt(0, csTmp);

				// Remove quotes from last column
				csTmp = RowValues.GetAt(RowValues.GetSize()-1);
				csTmp.Delete(csTmp.GetLength()-1, 1);
				RowValues.SetAt(RowValues.GetSize()-1, csTmp);
				return true;
			}
			else
			{
				// Save columns
				separatorPosition = csTmp.Find(m_sSeparator); // if separator is ?
				if (separatorPosition != -1)
				{
					int nCount = 0;
					int stringStartingPosition = 0;
					while (separatorPosition != -1)
					{
						nCount = separatorPosition - stringStartingPosition;
						RowValues.Add(csTmp.Mid(stringStartingPosition, nCount));
						stringStartingPosition = separatorPosition + m_sSeparator.GetLength();
						separatorPosition = csTmp.Find(m_sSeparator, stringStartingPosition);
					}
					nCount = csTmp.GetLength() - stringStartingPosition;
					RowValues.Add(csTmp.Mid(stringStartingPosition, nCount));
					return true;
				}
				else	// Treat spreadsheet as having one column
				{
					// Remove opening and ending quotes if any
					int quoteBegPos = csTmp.Find('\"');
					int quoteEndPos = csTmp.ReverseFind('\"');
					if ((quoteBegPos == 0) && (quoteEndPos == csTmp.GetLength()-1))
					{
						csTmp.Delete(0, 1);
						csTmp.Delete(csTmp.GetLength()-1, 1);
					}

					RowValues.Add(csTmp);
				}
			}
		}
//		m_sLastError = "Desired row is greater than total number of rows in spreadsheet\n";
		return false;
	}

	return true;
}

int CExcelImportLibXl::GetSheetCount()
{
	if(!m_Book) return 0;

	return m_Book->sheetCount();
}

CString CExcelImportLibXl::GetSheetName(int nIndex)
{
	if(!m_Book) return _T("");

	CString csSheetName;
	
	Sheet* sheet = m_Book->getSheet(nIndex);
	if(sheet)
	{
		return sheet->name();
	}

	return csSheetName;
}

int CExcelImportLibXl::SetCurrentSheet(int nIndex)
{
	if(!m_Book) return FALSE;
	
	if( nIndex <= m_Book->sheetCount())
	{
		m_nSheetIndex = nIndex;
		return TRUE;
	}

	return FALSE;
}

int CExcelImportLibXl::GetColCount()
{
	if(!m_Book) return 0;

	Sheet* sheet = m_Book->getSheet(m_nSheetIndex);
	if(sheet)
	{
		return sheet->lastCol();
	}

	return 0;
}

bool CExcelImportLibXl::GetCellValue(int nRow, int nCol,CString &value)
{
	CString csBuf = _T("");

	if(!m_Book) return false;

	Sheet* sheet = m_Book->getSheet(m_nSheetIndex);
	if(sheet)
	{
		CellType cellType = sheet->cellType(nRow, nCol);
		if(cellType == CELLTYPE_STRING)
		{
			csBuf = sheet->readStr(nRow, nCol);
		}
		else if(cellType == CELLTYPE_NUMBER)
		{
			csBuf.Format(_T("%g"), sheet->readNum(nRow, nCol));
		}
		else
		{
			csBuf = _T("");
		}
	}

	value = csBuf;

	return true;
}


bool CExcelImportLibXl::GetCellValue(int nRow, int nCol,int* value)
{
	CString csBuf = _T("");

	if(!m_Book) return false;

	Sheet* sheet = m_Book->getSheet(m_nSheetIndex);
	if(sheet)
	{
		CellType cellType = sheet->cellType(nRow, nCol);
		if(cellType == CELLTYPE_STRING)
		{
			csBuf = sheet->readStr(nRow, nCol);
		}
		else if(cellType == CELLTYPE_NUMBER)
		{
			csBuf.Format(_T("%g"), sheet->readNum(nRow, nCol));
		}
		else
		{
			csBuf = _T("");
		}
	}

	*value = _tstoi(csBuf);

	return true;
}

bool CExcelImportLibXl::GetCellValue(int nRow, int nCol,double* value)
{
	CString csBuf = _T("");

	if(!m_Book) return false;

	Sheet* sheet = m_Book->getSheet(m_nSheetIndex);
	if(sheet)
	{
		CellType cellType = sheet->cellType(nRow, nCol);
		if(cellType == CELLTYPE_STRING)
		{
			csBuf = sheet->readStr(nRow, nCol);
		}
		else if(cellType == CELLTYPE_NUMBER)
		{
			csBuf.Format(_T("%g"), sheet->readNum(nRow, nCol));
		}
		else
		{
			csBuf = _T("");
		}
	}

	*value = _tstof(csBuf);

	return true;
}

bool CExcelImportLibXl::GetComment(int nRow,int nCol,CString &value)
{
	CString csBuf = _T("");

	if(!m_Book) return false;

	Sheet* sheet = m_Book->getSheet(m_nSheetIndex);
	if(sheet)
	{
		csBuf = sheet->readComment(nRow,nCol);
	}

	value = csBuf;

	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////
// CUserCreateVolFuncDoc


IMPLEMENT_DYNCREATE(CUserCreateVolFuncDoc, CDocument)

BEGIN_MESSAGE_MAP(CUserCreateVolFuncDoc, CDocument)
	//{{AFX_MSG_MAP(CUserCreateVolFuncDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUserCreateVolFuncDoc construction/destruction

CUserCreateVolFuncDoc::CUserCreateVolFuncDoc()
{
	// TODO: add one-time construction code here

}

CUserCreateVolFuncDoc::~CUserCreateVolFuncDoc()
{
}


BOOL CUserCreateVolFuncDoc::OnNewDocument()
{
	// CHECK FOR LICENSE HERE!!!!! 2011-12-22 P�D
	if (!License())
	{
		return FALSE;
	}

	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CUserCreateVolFuncDoc serialization

void CUserCreateVolFuncDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CUserCreateVolFuncDoc diagnostics

#ifdef _DEBUG
void CUserCreateVolFuncDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CUserCreateVolFuncDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

///////////////////////////////////////////////////////////////////////////////////////////
// CUserCreateVolFuncFrame

IMPLEMENT_DYNCREATE(CUserCreateVolFuncFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CUserCreateVolFuncFrame, CMDIChildWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_SYSCOMMAND()
	//ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)

	ON_XTP_CREATECONTROL()
END_MESSAGE_MAP()


// CUserCreateVolFuncFrame construction/destruction

XTPDockingPanePaintTheme CUserCreateVolFuncFrame::m_themeCurrent = xtpPaneThemeOffice2003;

CUserCreateVolFuncFrame::CUserCreateVolFuncFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
	m_bInitReports = FALSE;
	m_bIsPrintOutTBtn = FALSE;
	m_bIsSysCommand = FALSE;
}

CUserCreateVolFuncFrame::~CUserCreateVolFuncFrame()
{
}

void CUserCreateVolFuncFrame::OnDestroy(void)
{
}

void CUserCreateVolFuncFrame::OnClose(void)
{
	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6500_KEY);
	SavePlacement(this, csBuf);
	
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	if (!m_bIsSysCommand)
	{
		CUserCreateVolFunc *pView = (CUserCreateVolFunc*)getFormViewByID(IDD_FORMVIEW7);
		if (pView != NULL)
		{
			QUIT_TYPES::Q_T_RETURN qtt;
			qtt = pView->checkDataBeforeSave(CHECK_SAVE_TYPES::CHECK_SAVE_ON_QUIT);
			if (qtt == QUIT_TYPES::QUIT_ALL_OK)
			{
				pView->doSave();
			}
		}
	}
	setNavBarButtons();
	CMDIChildWnd::OnClose();
}

void CUserCreateVolFuncFrame::OnSysCommand(UINT nID,LPARAM lParam)
{
	if ((nID & 0xFFF0) == SC_CLOSE)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

		CUserCreateVolFunc *pView = (CUserCreateVolFunc*)getFormViewByID(IDD_FORMVIEW7);
		if (pView != NULL)
		{
			m_bIsSysCommand = TRUE;
			QUIT_TYPES::Q_T_RETURN qtReturn;
			qtReturn = pView->checkDataBeforeSave(CHECK_SAVE_TYPES::CHECK_SAVE_ON_QUIT);
			if (qtReturn == QUIT_TYPES::QUIT_ALL_OK)
			{
				pView->doSave();
				setNavBarButtons();
				CMDIChildWnd::OnSysCommand(nID,lParam);
			}
			else if (qtReturn == QUIT_TYPES::QUIT_ANYWAY)
			{
				setNavBarButtons();
				CMDIChildWnd::OnSysCommand(nID,lParam);
			}
		}
	}
	else if ((nID & 0xFFF0) != SC_CLOSE)
	{
		CMDIChildWnd::OnSysCommand(nID,lParam);
	}
}

int CUserCreateVolFuncFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	EnableDocking(CBRS_ALIGN_ANY);

	// Create and Load toolbar; 090107 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR6);
	m_wndToolBar.EnableDocking(xtpFlagAlignTop|xtpFlagStretched);

	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			//***************************************************************************************
			// Setup commandbars and manues; 051114 p�d
			CXTPToolBar* pToolBar = &m_wndToolBar;
			if (pToolBar->IsBuiltIn())
			{
				if (pToolBar->GetType() != xtpBarTypeMenuBar)
				{
					UINT nBarID = pToolBar->GetBarID();
					pToolBar->LoadToolBar(nBarID, FALSE);
					CXTPControls *p = pToolBar->GetControls();
					// Setup icons on toolbars, using resource dll; 051208 p�d
					if (nBarID == IDR_TOOLBAR6)
					{		
						setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_NEW,xml.str(IDS_STRING4250),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(1),RES_TB_SAVE,xml.str(IDS_STRING4271),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(2),RES_TB_DEL,xml.str(IDS_STRING4251),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(3),RES_TB_ADD,xml.str(IDS_STRING4252),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(4),RES_TB_DEL2,xml.str(IDS_STRING4253),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(5),RES_TB_TOOLS,xml.str(IDS_STRING4254),TRUE);	//

						//p->GetAt(5)->SetVisible(FALSE);
						p->GetAt(6)->SetVisible(FALSE);
						p->GetAt(7)->SetVisible(FALSE);
						p->GetAt(8)->SetVisible(FALSE);
					}	// if (nBarID == IDR_TOOLBAR6)
				}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
			}	// if (pToolBar->IsBuiltIn())
			//***************************************************************************************
			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

int CUserCreateVolFuncFrame::OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl)
{
	if (lpCreateControl->nID == ID_BUTTON32804)
	{
		CMyControlPopup *m_pToolsPopup = new CMyControlPopup();
		m_pToolsPopup->SetStyle(xtpButtonIcon);
		if (fileExists(m_sLangFN))
		{
			RLFReader xml; // = new RLFReader;
			if (xml.Load(m_sLangFN))
			{
				m_pToolsPopup->addMenuIDAndText(ID_TOOLS_IMPORT_EXCEL,xml.str(IDS_STRING4255));
				m_pToolsPopup->addMenuIDAndText(ID_TOOLS_EXPORT_EXCEL,xml.str(IDS_STRING4256));
			}	// if (xml->Load(m_sLangFN))
			xml.clean();
		}

		lpCreateControl->pControl = m_pToolsPopup;
		return TRUE;
	}

	return TRUE;
}


LRESULT CUserCreateVolFuncFrame::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	return 0L;
}

BOOL CUserCreateVolFuncFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CUserCreateVolFuncFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CUserCreateVolFuncFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6500_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CUserCreateVolFuncFrame::OnSetFocus(CWnd* pWnd)
{
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,ID_DO_SOMETHING_IN_SHELL,ID_LPARAM_COMMAND2);

	setNavBarButtons();

	CUserCreateVolFunc *pView = (CUserCreateVolFunc*)getFormViewByID(IDD_FORMVIEW7);
	if (pView != NULL)
	{
		pView->doSetNavigationButtons();
	}


	CMDIChildWnd::OnSetFocus(pWnd);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CUserCreateVolFuncFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	CUserCreateVolFunc *pView = (CUserCreateVolFunc*)getFormViewByID(IDD_FORMVIEW7);
	if (pView != NULL)
	{
		::SendMessage(pView->GetSafeHwnd(),WM_USER_MSG_SUITE,wParam,lParam);
	}

	return 0L;
}


// CUserCreateVolFuncFrame diagnostics

#ifdef _DEBUG
void CUserCreateVolFuncFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CUserCreateVolFuncFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CUserCreateVolFuncFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CUserCreateVolFuncFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType,cx,cy);
}


// CUserCreateVolFunc

IMPLEMENT_DYNCREATE(CUserCreateVolFunc, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CUserCreateVolFunc, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_CREATE()
	ON_WM_COPYDATA()

	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)

	ON_CBN_SELCHANGE(IDC_COMBO7_3, &CUserCreateVolFunc::OnCbnSelchangeCombo73)

	ON_COMMAND_RANGE(ID_BUTTON32799,ID_BUTTON32806, OnCommand)
	ON_UPDATE_COMMAND_UI_RANGE(ID_BUTTON32799,ID_BUTTON32806, OnUpdateToolbar)

	ON_COMMAND_RANGE(ID_TOOLS_IMPORT_EXCEL,ID_TOOLS_EXPORT_EXCEL, OnToolsCommand)
	ON_UPDATE_COMMAND_UI_RANGE(ID_TOOLS_IMPORT_EXCEL,ID_TOOLS_EXPORT_EXCEL, OnUpdateToolsToolbar)


	ON_CBN_SELCHANGE(IDC_COMBO7_1, &CUserCreateVolFunc::OnCbnSelchangeCombo71)
	ON_BN_CLICKED(IDC_BUTTON7_1, &CUserCreateVolFunc::OnBnClickedButton71)
	ON_BN_CLICKED(IDC_BUTTON7_2, &CUserCreateVolFunc::OnBnClickedButton72)
	
	ON_BN_CLICKED(IDC_BUTTON7_3, &CUserCreateVolFunc::OnBnClickedButton73)
	ON_BN_CLICKED(IDC_BUTTON7_4, &CUserCreateVolFunc::OnBnClickedButton74)

	ON_NOTIFY(TCN_SELCHANGE, ID_TABCONTROL, OnSelectedChanged)

END_MESSAGE_MAP()


CUserCreateVolFunc::CUserCreateVolFunc()
	: CXTResizeFormView(CUserCreateVolFunc::IDD),
		m_bInitialized(FALSE),
		m_bMeasuringModeSelected(FALSE),
		m_nSelectedIndex(0),
		m_pDB(NULL),
		m_sLangFN(L""),
		m_bOnlySave(FALSE)

		, m_csDate(_T(""))
{
}

CUserCreateVolFunc::~CUserCreateVolFunc()
{
}

void CUserCreateVolFunc::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_GROUP7_1, m_wndGrp7_1);

	DDX_Control(pDX, IDC_LBL7_1, m_lbl7_1);
	DDX_Control(pDX, IDC_LBL7_2, m_lbl7_2);
	DDX_Control(pDX, IDC_LBL7_3, m_lbl7_3);
	DDX_Control(pDX, IDC_LBL7_4, m_lbl7_4);
	DDX_Control(pDX, IDC_LBL7_5, m_lbl7_5);
	DDX_Control(pDX, IDC_LBL7_6, m_lbl7_6);
	DDX_Control(pDX, IDC_LBL7_7, m_lbl7_7);
	DDX_Control(pDX, IDC_LBL7_8, m_lbl7_8);
	DDX_Control(pDX, IDC_LBL7_9, m_lbl7_9);
	DDX_Control(pDX, IDC_LBL7_10, m_lbl7_10);
	DDX_Control(pDX, IDC_LBL7_11, m_lbl7_11);
	DDX_Control(pDX, IDC_LBL7_12, m_lbl7_12);
	DDX_Control(pDX, IDC_LBL7_13, m_lbl7_13);
	DDX_Control(pDX, IDC_LBL7_14, m_lbl7_14);
	DDX_Control(pDX, IDC_LBL7_15, m_lbl7_15);
	DDX_Control(pDX, IDC_LBL7_16, m_lbl7_16);
	DDX_Control(pDX, IDC_LBL7_17, m_lbl7_17);
	DDX_Control(pDX, IDC_LBL7_18, m_lbl7_18);

	DDX_Control(pDX, IDC_EDIT7_1, m_edit7_1);
	DDX_Control(pDX, IDC_EDIT7_2, m_edit7_2);
	DDX_Control(pDX, IDC_EDIT7_3, m_edit7_3);
	DDX_Control(pDX, IDC_EDIT7_4, m_edit7_4);
	DDX_Control(pDX, IDC_EDIT7_6, m_edit7_6);
	DDX_Control(pDX, IDC_EDIT7_7, m_edit7_7);
	DDX_Control(pDX, IDC_EDIT7_8, m_edit7_8);
	DDX_Control(pDX, IDC_EDIT7_9, m_edit7_9);
	DDX_Control(pDX, IDC_EDIT7_10, m_edit7_10);

	DDX_Control(pDX, IDC_COMBO7_1, m_cbox7_1);
	DDX_Control(pDX, IDC_COMBO7_3, m_cbox7_2);
	//DDX_Control(pDX, IDC_COMBO7_2, m_dt7_2);

	DDX_Control(pDX, IDC_BUTTON7_1, m_btn7_1);
	DDX_Control(pDX, IDC_BUTTON7_2, m_btn7_2);
	DDX_Control(pDX, IDC_BUTTON7_3, m_btn7_3);
	DDX_Control(pDX, IDC_BUTTON7_4, m_btn7_4);

	//}}AFX_DATA_MAP

	DDX_Control(pDX, IDC_DATE, m_dtDate);
	DDX_DateTimeCtrl(pDX, IDC_DATE, m_csDate);
}

void CUserCreateVolFunc::OnUpdateToolbar(CCmdUI* pCmdUI)
{
	if (pCmdUI->m_nID == ID_BUTTON32800)
		pCmdUI->Enable(m_bEnableToolBarBtnSave);
	if (pCmdUI->m_nID == ID_BUTTON32801)
		pCmdUI->Enable(m_bEnableToolBarBtnDelete);
	if (pCmdUI->m_nID == ID_BUTTON32802)
		pCmdUI->Enable(m_bEnableToolBarBtnAddSpc);
	if (pCmdUI->m_nID == ID_BUTTON32803)
		pCmdUI->Enable(m_bEnableToolBarBtnDelSpc);
	if (pCmdUI->m_nID == ID_BUTTON32804)
		pCmdUI->Enable(m_bEnableToolBarBtnImport);
	if (pCmdUI->m_nID == ID_BUTTON32805)
		pCmdUI->Enable(m_bEnableToolBarBtnExport);
	if (pCmdUI->m_nID == ID_BUTTON32806)
		pCmdUI->Enable(m_bEnableToolBarBtnPreview);
}

void CUserCreateVolFunc::OnUpdateToolsToolbar(CCmdUI* pCmdUI)
{
}

BOOL CUserCreateVolFunc::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

int CUserCreateVolFunc::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	CFont fnt;
	LOGFONT lf;
	VERIFY(fnt.CreateFont(
   24,                        // nHeight
   0,                         // nWidth
   0,                         // nEscapement
   0,                         // nOrientation
   FW_NORMAL,                 // nWeight
   FALSE,                     // bItalic
   FALSE,                     // bUnderline
   0,                         // cStrikeOut
   ANSI_CHARSET,              // nCharSet
   OUT_DEFAULT_PRECIS,        // nOutPrecision
   CLIP_DEFAULT_PRECIS,       // nClipPrecision
   DEFAULT_QUALITY,           // nQuality
   DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
   _T("Times New Roman")));                 // lpszFacename

	fnt.GetLogFont( &lf );

	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS, CRect(0, 0, 0, 0), this, ID_TABCONTROL);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearanceVisualStudio2005);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = TRUE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = TRUE;
	m_wndTabControl.GetPaintManager()->m_bBoldSelected = TRUE;
	m_wndTabControl.GetPaintManager()->SetFontIndirect( &lf );
	m_wndTabControl.GetPaintManager()->DisableLunaColors( FALSE );

	return 0;
}

void CUserCreateVolFunc::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (! m_bInitialized )
	{

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		m_edit7_1.SetLimitText(50);
		m_edit7_2.SetLimitText(25);
		m_edit7_3.SetLimitText(25);
		m_edit7_4.SetLimitText(25);

		m_lbl7_9.SetLblFontEx(-1,FW_BOLD,TRUE);
		m_lbl7_10.SetLblFontEx(-1,FW_BOLD,TRUE);

		m_btn7_1.SetBitmap(CSize(16,16),IDB_BMP_PLUS);
		m_btn7_1.SetXButtonStyle( BS_XT_WINXP_COMPAT);

		m_btn7_2.SetBitmap(CSize(16,16),IDB_BMP_MINUS);
		m_btn7_2.SetXButtonStyle( BS_XT_WINXP_COMPAT );

		m_btn7_3.SetBitmap(CSize(16,16),IDB_BMP_PLUS);
		m_btn7_3.SetXButtonStyle( BS_XT_WINXP_COMPAT );

		m_btn7_4.SetBitmap(CSize(16,16),IDB_BMP_MINUS);
		m_btn7_4.SetXButtonStyle( BS_XT_WINXP_COMPAT );

		m_edit7_1.SetEnabledColor();
		m_edit7_1.SetDisabledColor();
		m_edit7_1.SetLimitText(50);
		m_edit7_2.SetEnabledColor();
		m_edit7_2.SetDisabledColor();
		m_edit7_2.SetLimitText(25);
		m_edit7_3.SetEnabledColor();
		m_edit7_3.SetDisabledColor();
		m_edit7_3.SetLimitText(25);
		m_edit7_4.SetEnabledColor();
		m_edit7_4.SetDisabledColor();
		m_edit7_4.SetLimitText(25);
		m_edit7_6.SetEnabledColor();
		m_edit7_6.SetDisabledColor();


		m_edit7_7.SetAsNumeric();
		m_edit7_7.SetEnabledColor();
		m_edit7_7.SetDisabledColor();
		m_edit7_8.SetAsNumeric();
		m_edit7_8.SetEnabledColor();
		m_edit7_8.SetDisabledColor();
		m_edit7_9.SetAsNumeric();
		m_edit7_9.SetEnabledColor();
		m_edit7_9.SetDisabledColor();
		m_edit7_10.SetAsNumeric();
		m_edit7_10.SetEnabledColor();
		m_edit7_10.SetDisabledColor();

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				m_sInch = xml.str(IDS_STRING109);
				m_sMM = xml.str(IDS_STRING110);
				m_sFeet = xml.str(IDS_STRING111);
				m_sDM = xml.str(IDS_STRING112);
				m_sM = xml.str(IDS_STRING122);

				m_sColumnCaps = xml.str(IDS_STRING206);

				m_sMsgCap = xml.str(IDS_STRING99);
				m_sMsgRemoveSpecies1 = xml.str(IDS_STRING4257);
				m_sMsgRemoveSpecies2 = xml.str(IDS_STRING4258);
				m_sMsgRemoveTable1 = xml.str(IDS_STRING4261);
				m_sMsgRemoveTable2 = xml.str(IDS_STRING4262);
				m_sMsgMissingData1.Format(L"%s\n\n%s\n\n%s\n\n",
					xml.str(IDS_STRING4263),
					xml.str(IDS_STRING4264),
					xml.str(IDS_STRING4265));
				m_sMsgMissingData2.Format(L"%s\n\n%s\n\n",
					xml.str(IDS_STRING4263),
					xml.str(IDS_STRING4264));

				m_sMsgChangeMeasuringMode.Format(L"%s\n\n%s\n\n",
					xml.str(IDS_STRING4259),
					xml.str(IDS_STRING4260));

				m_sMsgTableUsedInTemplates = 	xml.str(IDS_STRING4266);

				m_sMsgFunctionNameAlreadyUsed1.Format(L"%s\n%s\n\n%s\n\n",
					xml.str(IDS_STRING4268),
					xml.str(IDS_STRING4269),
					xml.str(IDS_STRING4270));
				m_sMsgFunctionNameAlreadyUsed2.Format(L"%s\n%s\n\n",
					xml.str(IDS_STRING4268),
					xml.str(IDS_STRING4269));

				m_sOpenDlgEXCEL =	xml.str(IDS_STRING4576);
				m_sMsgOpenEXCEL =	xml.str(IDS_STRING4272);
				m_sGeneralCap = 	xml.str(IDS_STRING123);

				m_lbl7_1.SetWindowTextW(xml.str(IDS_STRING4200));
				m_lbl7_2.SetWindowTextW(xml.str(IDS_STRING4201));
				m_lbl7_3.SetWindowTextW(xml.str(IDS_STRING4202));
				m_lbl7_4.SetWindowTextW(xml.str(IDS_STRING4203));
				m_lbl7_5.SetWindowTextW(xml.str(IDS_STRING4204));
				m_lbl7_6.SetWindowTextW(xml.str(IDS_STRING4205));
				m_lbl7_7.SetWindowTextW(xml.str(IDS_STRING4206));
				m_lbl7_8.SetWindowTextW(xml.str(IDS_STRING4207));
				m_lbl7_9.SetWindowTextW(xml.str(IDS_STRING4208));
				m_lbl7_10.SetWindowTextW(xml.str(IDS_STRING4209));
				m_lbl7_11.SetWindowTextW(xml.str(IDS_STRING4210));
				m_lbl7_12.SetWindowTextW(xml.str(IDS_STRING4211));
				m_lbl7_13.SetWindowTextW(xml.str(IDS_STRING4212));
				m_lbl7_14.SetWindowTextW(xml.str(IDS_STRING4213));

				tokenizeString(xml.str(IDS_STRING204),';',m_sarrTableTypes);
				tokenizeString(xml.str(IDS_STRING205),';',m_sarrMeasuringMode);

				// We'll add TableTypes to Combobox
				m_cbox7_1.ResetContent();
				for (int i = 0;i < m_sarrTableTypes.GetCount();i++)
				{
					m_cbox7_1.AddString(m_sarrTableTypes.GetAt(i));
				}
				m_edit7_4.SetWindowTextW(getUserName().MakeUpper());

				//m_dt7_2.setDateInComboBox();

				// We'll add MeasuringMode to Combobox
				m_cbox7_2.ResetContent();
				for (int i = 0;i < m_sarrMeasuringMode.GetCount();i++)
				{
					m_cbox7_2.AddString(m_sarrMeasuringMode.GetAt(i));
				}

			}
		}
		// Always enable Import
		m_bEnableToolBarBtnImport = TRUE;
		enableView(m_vecUserVolTables.size() > 0);
		if (m_vecUserVolTables.size() > 0)
		{
			m_nSelectedIndex = m_vecUserVolTables.size()-1;
			enableView(TRUE);

			m_bEnableToolBarBtnSave = TRUE;
			m_bEnableToolBarBtnDelete = TRUE;
			m_bEnableToolBarBtnAddSpc = TRUE;
			m_bEnableToolBarBtnDelSpc = TRUE;

			m_bEnableToolBarBtnExport = TRUE;
			m_bEnableToolBarBtnPreview = TRUE;

			setNavigationButtons(m_nSelectedIndex > 0,m_nSelectedIndex < (m_vecUserVolTables.size()-1));
		}
		else
		{
			m_nSelectedIndex = -1;
			enableView(FALSE);

			m_bEnableToolBarBtnSave = FALSE;
			m_bEnableToolBarBtnDelete = FALSE;
			m_bEnableToolBarBtnAddSpc = FALSE;
			m_bEnableToolBarBtnDelSpc = FALSE;
			m_bEnableToolBarBtnExport = FALSE;
			m_bEnableToolBarBtnPreview = FALSE;

			setNavigationButtons(FALSE,FALSE);

		}

		populateData();

		m_bInitialized = TRUE;
	}
}

void CUserCreateVolFunc::OnSize(UINT nType, int cx, int cy)
{

	if (m_wndGrp7_1.GetSafeHwnd())
	{
		setResize(&m_wndGrp7_1, 2, 150, 143, cy-154);
	}

	if (m_wndTabControl.GetSafeHwnd())
	{
		setResize(&m_wndTabControl,150, 150, cx-154, cy-154);
	}

	CXTResizeFormView::OnSize(nType, cx, cy);
}

BOOL CUserCreateVolFunc::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{

	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		if (m_dbConnectionData.conn->isConnected())
		{
			m_pDB = new CDBHandling(m_dbConnectionData);

			if (m_pDB != NULL)
			{
				m_pDB->getUserVolTables(m_vecUserVolTables);
				m_pDB->getSpecies(m_vecSpecies);
				m_pDB->getTmplTables(m_vecLogScaleTemplates);
			}
		}
	}

	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CUserCreateVolFunc::OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult)
{
	UNUSED_ALWAYS(pNMHDR);
	*pResult = 0;
	setStartAndStepData();
}

void CUserCreateVolFunc::OnCommand(UINT nID)
{
	switch(nID)
	{
		case ID_BUTTON32799 : New(); break;
		case ID_BUTTON32800 : m_bOnlySave = TRUE; Save(); m_bOnlySave = FALSE; break;
		case ID_BUTTON32801 : Delete(); break;

		case ID_BUTTON32802 : AddSpecies(); break;
		case ID_BUTTON32803 : DelSpecies(); break;

		case ID_BUTTON32805 : Export(); break;
		case ID_BUTTON32806 : PreView(); break;

	};
}

void CUserCreateVolFunc::OnToolsCommand(UINT nID)
{
	switch(nID)
	{
		case ID_TOOLS_IMPORT_EXCEL : Import(); break;
		case ID_TOOLS_EXPORT_EXCEL : Export(); break;
	};
}

void CUserCreateVolFunc::doSave()
{
	createData();
}

void CUserCreateVolFunc::doSetNavigationButtons()
{
	setNavigationButtons(m_nSelectedIndex > 0,m_nSelectedIndex < (m_vecUserVolTables.size()-1));
}

QUIT_TYPES::Q_T_RETURN CUserCreateVolFunc::isNameOfFunctionOK(LPCTSTR name)
{
	if (m_vecUserVolTables.size() > 0)
	{
		for (UINT i = 0;i < m_vecUserVolTables.size();i++)
		{
			if (m_vecUserVolTables[i].getFullName().CompareNoCase(name) == 0 && 
				  m_nSelectedIndex != i)
			{
				if (m_bOnlySave)
				{
					::MessageBox(GetSafeHwnd(),m_sMsgFunctionNameAlreadyUsed2,m_sMsgCap,MB_ICONSTOP | MB_OK);
					return QUIT_TYPES::NO_SAVE;
				}
				else
				{
					if (::MessageBox(GetSafeHwnd(),m_sMsgFunctionNameAlreadyUsed1,m_sMsgCap,MB_ICONSTOP | MB_YESNO | MB_DEFBUTTON2) == IDYES)
						return QUIT_TYPES::QUIT_ANYWAY;
					else
						return QUIT_TYPES::NO_SAVE;
				}
			}
		}	// for (UINT i = 0;i < m_vecLogScaleTemplates.size();i++)
	}	// if (m_vecLogScaleTemplates.size() > 0)
	return QUIT_TYPES::DO_SAVE;
}


QUIT_TYPES::Q_T_RETURN CUserCreateVolFunc::checkDataBeforeSave(CHECK_SAVE_TYPES::CHECK_SAVE cs)
{
	QUIT_TYPES::Q_T_RETURN qt;
	// Check name of template
	if ((qt = isNameOfFunctionOK(m_edit7_1.getText())) != QUIT_TYPES::DO_SAVE)
		return qt;

	if (cs == CHECK_SAVE_TYPES::CHECK_SAVE_ON_QUIT)
	{
		//if (m_vecUserVolTables.size() == 0)
		//	return QUIT_TYPES::QUIT_ANYWAY;	// Nothing to save

		if (m_cbox7_1.GetCurSel() == -1 ||
				m_cbox7_2.GetCurSel() == -1 ||
				m_edit7_1.getText().IsEmpty() ||
				m_edit7_2.getText().IsEmpty() ||
				m_edit7_3.getText().IsEmpty())
		{
			if (::MessageBox(GetSafeHwnd(),m_sMsgMissingData1,m_sMsgCap,MB_ICONSTOP | MB_YESNO | MB_DEFBUTTON2) == IDNO)
				return QUIT_TYPES::DO_NOT_QUIT;
			else
				return QUIT_TYPES::QUIT_ANYWAY;
		}
		else
		{
			return QUIT_TYPES::QUIT_ALL_OK;
		}
	}
	else if (cs == CHECK_SAVE_TYPES::CHECK_SAVE_NO_QUIT)
	{
		//if (m_vecUserVolTables.size() == 0)
		//	return QUIT_TYPES::Q_T_RETURN::NO_SAVE;

		if (m_cbox7_1.GetCurSel() == -1 ||
				m_cbox7_2.GetCurSel() == -1 ||
				m_edit7_1.getText().IsEmpty() ||
				m_edit7_2.getText().IsEmpty() ||
				m_edit7_3.getText().IsEmpty())
		{
			::MessageBox(GetSafeHwnd(),m_sMsgMissingData2,m_sMsgCap,MB_ICONSTOP | MB_OK);
			return QUIT_TYPES::NO_SAVE;
		}
		else
		{
			return QUIT_TYPES::DO_SAVE;
		}
	}
	return QUIT_TYPES::NO_SAVE;
}

LRESULT CUserCreateVolFunc::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	switch (wParam)
	{
		case ID_DBNAVIG_LIST:
		{
			showFormView(ID_LIST_USER_VTABLE,m_sLangFN,0,0);
			break;
		}

		case ID_NEW_ITEM :
		{
			break;
		}	// case ID_NEW_ITEM :

		case ID_SAVE_ITEM :
		{
			break;
		}	// case ID_SAVE_ITEM :
		
		case ID_DELETE_ITEM :
		{
			break;
		}	// case ID_DELETE_ITEM :

		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		{
			if (Save())
			{
				m_nSelectedIndex = 0;
				populateData();
				setNavigationButtons(FALSE,TRUE);
			}
			break;
		}
		case ID_DBNAVIG_PREV :
		{
			if (Save())
			{
				m_nSelectedIndex--;
				if (m_nSelectedIndex < 0)
					m_nSelectedIndex = 0;

				populateData();

				if (m_nSelectedIndex == 0)
				{
					setNavigationButtons(FALSE,TRUE);
				}
				else
				{
					setNavigationButtons(TRUE,TRUE);
				}
			}
			break;
		}
		case ID_DBNAVIG_NEXT :
		{
			if (Save())
			{
				m_nSelectedIndex++;
				if (m_nSelectedIndex > ((long)m_vecUserVolTables.size() - 1))
					m_nSelectedIndex = (long)m_vecUserVolTables.size() - 1;

				populateData();
					
				if (m_nSelectedIndex == (long)m_vecUserVolTables.size() - 1)
				{
					setNavigationButtons(TRUE,FALSE);
				}
				else
				{
					setNavigationButtons(TRUE,TRUE);
				}
			}
			break;
		}
		case ID_DBNAVIG_END :
		{
			if (Save())
			{
				m_nSelectedIndex = (long)m_vecUserVolTables.size()-1;
				populateData();
				setNavigationButtons(TRUE,FALSE);	
			}
			break;
		}	// case ID_NEW_ITEM :
	}	// switch (wParam)
	return 0L;
}

// Catch message sent from HMSShell (WM_USER_MSG_SUITE), by MDIChildWnd; 060215 p�d
LRESULT CUserCreateVolFunc::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	return 0L;
}

// Set navigation buttons in shell, depending on index of item to display; 061002 p�d
void CUserCreateVolFunc::setNavigationButtons(BOOL start_prev,BOOL end_next)
{
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
	
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);
	AfxGetMainWnd()->UpdateWindow();
}

void CUserCreateVolFunc::enableView(BOOL enable)
{
	m_edit7_1.EnableWindow(enable);
	m_edit7_1.SetReadOnly(!enable);
	m_edit7_2.EnableWindow(enable);
	m_edit7_2.SetReadOnly(!enable);
	m_edit7_3.EnableWindow(enable);
	m_edit7_3.SetReadOnly(!enable);
	m_edit7_4.EnableWindow(enable);
	m_edit7_4.SetReadOnly(!enable);
	m_edit7_6.EnableWindow(enable);
	m_edit7_6.SetReadOnly(!enable);

	m_edit7_7.EnableWindow(enable);
	m_edit7_7.SetReadOnly(!enable);
	m_edit7_8.EnableWindow(enable);
	m_edit7_8.SetReadOnly(!enable);
	m_edit7_9.EnableWindow(enable);
	m_edit7_9.SetReadOnly(!enable);
	m_edit7_10.EnableWindow(enable);
	m_edit7_10.SetReadOnly(!enable);

	m_dtDate.EnableWindow(enable);
	m_cbox7_1.EnableWindow(enable);
	m_cbox7_2.EnableWindow(enable);
}

void CUserCreateVolFunc::New()
{
	CString sSpc = L"";

	CSelectSpeciesDlg *dlg = new CSelectSpeciesDlg();
	if (dlg != NULL)
	{
		dlg->setSpecies(m_vecSpecies);
		if (dlg->DoModal() == IDOK)
		{		
			// Save data first, if there's data to save
			if (m_vecUserVolTables.size() > 0)
			{
				Save();
				clearData();
			}
			m_bMeasuringModeSelected = FALSE;
			m_recSelectedVolTable = CUserVolTables();
			dlg->getSelectedSpecies(m_vecSpeciesSelected);
			addSpeciesToTabs();

			enableView(TRUE);
			m_cbox7_1.SetFocus();

			m_bEnableToolBarBtnSave = TRUE;
			m_bEnableToolBarBtnDelete = TRUE;
			m_bEnableToolBarBtnAddSpc = TRUE;
			m_bEnableToolBarBtnDelSpc = TRUE;

		}	
		delete dlg;
	}
}

BOOL CUserCreateVolFunc::Save()
{
	BOOL bReturn = FALSE;
	QUIT_TYPES::Q_T_RETURN cst;
	if (cst = checkDataBeforeSave(CHECK_SAVE_TYPES::CHECK_SAVE_NO_QUIT))
	{
		if (cst == QUIT_TYPES::DO_SAVE)
		{
			createData();
			bReturn = TRUE;
		}
		else if (cst == QUIT_TYPES::QUIT_ANYWAY)
		{
			bReturn = TRUE;
		}
	}

	return bReturn;
}

BOOL CUserCreateVolFunc::checkIsFunctionUsed(int func_id)
{
	CStringArray sarrData;
	int nRowCnt = 0;
	std::wstringstream ss;
	CString sToken = L"",sData = L"";
	std::wstring line;

	if (m_pDB != NULL)
	{
		m_pDB->getPricelist(m_vecVecPricelist);
	}

	for (UINT i = 0;i < m_vecVecPricelist.size();i++)
	{
		sData += m_vecVecPricelist[i].getPricelist();
	}
	
	ss << sData.GetBuffer();
	while (std::getline(ss,line))
	{
		AfxExtractSubString(sToken,line.c_str(),0,';');
		if (_tstoi(sToken) == FUNC_INDEX::DATA)
		{
			sarrData.Add(line.c_str());
		}
	}

	for (int i = 0;i < sarrData.GetCount();i++)
	{

		AfxExtractSubString(sToken,sarrData.GetAt(i),0,';');
		if (_tstoi(sToken) == FUNC_INDEX::DATA)
		{
			AfxExtractSubString(sToken,sarrData.GetAt(i),4,';');
			if (_tstoi(sToken) == func_id)
				return TRUE;
		}
	}
	return FALSE;
}

void CUserCreateVolFunc::Delete()
{
	CString sMsg;
	BOOL bIsUsed = FALSE;

	if (checkIsFunctionUsed(m_recSelectedVolTable.getFuncID()))
	{
		::MessageBox(GetSafeHwnd(),m_sMsgTableUsedInTemplates,m_sMsgCap,MB_ICONSTOP | MB_OK);
		return;
	}

	sMsg.Format(L"%s : %s",m_sMsgRemoveTable1,m_edit7_1.getText());
	if (::MessageBox(GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONSTOP | MB_YESNO | MB_DEFBUTTON2) == IDYES)
	{
		if (::MessageBox(GetSafeHwnd(),m_sMsgRemoveTable2,m_sMsgCap,MB_ICONSTOP | MB_YESNO | MB_DEFBUTTON2) == IDYES)
		{
			if (m_pDB != NULL)
			{


				if (m_pDB->delUserVolTable(m_recSelectedVolTable))
				{
					m_pDB->getUserVolTables(m_vecUserVolTables);
					m_nSelectedIndex = m_vecUserVolTables.size()-1;
					populateData();
					// Check if table is empty
					if (m_vecUserVolTables.size() == 0)
					{
						m_bEnableToolBarBtnSave = FALSE;
						m_bEnableToolBarBtnDelete = FALSE;

						enableView(FALSE);
						// Reseed IDENTITY-field (i.e. reset)
						m_pDB->resetUserVolTableIdentityField();
					}
					setNavigationButtons(m_nSelectedIndex > 0,m_nSelectedIndex < (m_vecUserVolTables.size()-1));
				}

			}
		}
	}
}

void CUserCreateVolFunc::AddSpecies()
{
	CString sSpc = L"";
	CSelectSpeciesDlg *dlg = new CSelectSpeciesDlg();
	if (dlg != NULL)
	{
		dlg->setSpecies(m_vecSpecies);
		if (dlg->DoModal() == IDOK)
		{
			dlg->getSelectedSpecies(m_vecSpeciesSelected);
			addSpeciesToTabs();
			addFirstColumnCaption();
		}

		delete dlg;

	}
}

void CUserCreateVolFunc::DelSpecies()
{
	removeSpecies(CUserCreateVolFunc::DEL_SELECTED_SPECIES);
}


BOOL CUserCreateVolFunc::checkSpecie(LPCTSTR spc_code)
{
	if (m_vecSpecies.size() == 0) return FALSE;

	for (UINT i = 0;i < m_vecSpecies.size();i++)
	{
		if (m_vecSpecies[i].getSpcCode().CompareNoCase(spc_code) == 0)
			return TRUE;
	}
	return FALSE;
}

// Use this function to import from a user entered EXCEL-sheet
/*void CUserCreateVolFunc::ImportSimpleOrg(LPCTSTR file)
{
	std::vector<double> vec;
	double fLength = 0.0,fDiam = 0.0,fValue = 0.0;
	double fPrevStep = 0.0, fLength1 = 0.0, fLength2 = 0.0, fDiam1 = 0.0, fDiam2 = 0.0;
	CStringArray sarrSheets;
	std::vector<int> vecSelSheets;

	CString	sFile(file);

	CExcelImportLibXl *pExcel = new CExcelImportLibXl();
	if (pExcel != NULL)
	{
		m_recSelectedVolTable = CUserVolTables();

		if (pExcel->OpenExcel(sFile))
		{
			enableView(TRUE);
			m_bEnableToolBarBtnSave = TRUE;
			m_bEnableToolBarBtnDelete = TRUE;
			m_bEnableToolBarBtnAddSpc = TRUE;
			m_bEnableToolBarBtnDelSpc = TRUE;

			for (short i = 0;i < pExcel->GetSheetCount();i++)
			{
				sarrSheets.Add(pExcel->GetSheetName(i));
			}

			CSelectExcelSheetDlg *pSelectDlg = new CSelectExcelSheetDlg(0,1);
			if (pSelectDlg != NULL)
			{
				pSelectDlg->setSheetNames(sarrSheets);
				if (pSelectDlg->DoModal() == IDOK)
				{
					clearData();
	
					pSelectDlg->getSelectedSheets(vecSelSheets);
					if (vecSelSheets.size() == 0) return;
					pExcel->SetCurrentSheet(vecSelSheets[0]);
					m_cbox7_1.SetCurSel(pSelectDlg->getSelectedTableType());
					m_cbox7_2.SetCurSel(pSelectDlg->getSelectedMeasMode());

					// Only one Tab, general volume-table
					addSpeciesToTabs(true);	
					// Add column-headline to first column
					addFirstColumnCaption();
					
					for (int col = 0;col < pExcel->GetColCount();col++)
					{
						if (col > 0 && col < pExcel->GetColCount()-1)
						{
							pExcel->GetCellValue(0,col,&fLength1);
							pExcel->GetCellValue(0,col+1,&fLength2);
							addColumnToTab(fLength1,fLength2-fLength1);
							fPrevStep = (fLength2-fLength1);
						}
						else if (col == pExcel->GetColCount()-1)
						{
							pExcel->GetCellValue(0,col,&fLength1);
							addColumnToTab(fLength1,fPrevStep);
						}
					}

					m_edit7_7.setFloat(fLength1,1);
					m_edit7_8.setFloat(fPrevStep,1);

					fPrevStep = 0.0;
					for (int row = 0;row < pExcel->GetTotalRows();row++)
					{
						vec.clear();
						for (int col = 0;col < pExcel->GetColCount();col++)
						{

							if ((row > 0 && row <= pExcel->GetTotalRows()-1))
							{
								if (col == 0)
								{
									pExcel->GetCellValue(row,col,&fDiam1);
									pExcel->GetCellValue(row+1,col,&fDiam2);
									if (fDiam2 > 0)
										fPrevStep = fDiam2-fDiam1;
								}
								pExcel->GetCellValue(row,col,&fDiam);
								vec.push_back(fDiam);
							}	// if ((row > 0 && row < pExcel->GetTotalRows()-1) && col == 0)

						}	// for (int col = 0;col < pExcel->GetColCount();col++)
						addRowsToTab(fDiam1,fPrevStep,vec);
					}	// for (int row = 0;row < pExcel->GetTotalRows();row++)

					m_edit7_9.setFloat(fDiam1*10.0,0);
					m_edit7_10.setFloat(fPrevStep*10.0,0);

				}	// if (pSelectDlg->DoModal() == IDOK)
			}	// if (pSelectDlg != NULL)
		}	// if (pExcel->OpenExcel(sFile))
	
		delete pExcel;
	}
	sarrSheets.RemoveAll();
}*/

//#4257 Use this function to import from a user entered EXCEL-sheet
void CUserCreateVolFunc::ImportSimple(LPCTSTR file)
{
	CNoMatchSpeciesDlg *pDlg;
	std::vector<int> vecSelSheets;
	CStringArray sarr;
	double fLength = 0.0,fDiam = 0.0,fValue = 0.0;
	double fPrevStep = 0.0, fLength1 = 0.0, fLength2 = 0.0, fDiam1 = 0.0, fDiam2 = 0.0;
	int nRet = -1,nSeltableType = -1,nMeasureMode = -1;
	CStringArray sarrSheets;
	CVecMatchedSpecies vecMatchedSpecies;
	CString sFile = L"",sValue = L"",sTmp = L"",sSpcID = L"",sSpcCode = L"",sSpcName = L"";

	sFile = file;

	UpdateData(TRUE);

	CExcelImportLibXl *pExcel = new CExcelImportLibXl();
	if (pExcel != NULL)
	{

		if (pExcel->OpenExcel(sFile))
		{
			enableView(TRUE);
			m_bEnableToolBarBtnSave = TRUE;
			m_bEnableToolBarBtnDelete = TRUE;
			m_bEnableToolBarBtnAddSpc = TRUE;
			m_bEnableToolBarBtnDelSpc = TRUE;

			for (short i = 0;i < pExcel->GetSheetCount();i++)
			{
				sarrSheets.Add(pExcel->GetSheetName(i));
			}

			CSelectExcelSheetDlg *pSelectDlg = new CSelectExcelSheetDlg();
			if (pSelectDlg != NULL)
			{
				pSelectDlg->setSheetNames(sarrSheets);
				if (pSelectDlg->DoModal() == IDOK)
				{
					m_vecNonMatchingSpecies.clear();
					pSelectDlg->getSelectedSheets(vecSelSheets);
					if (vecSelSheets.size() > 0)
					{
						// Read info. on species in imported file
						// Info. is set in sheet name
						// Use this information to find out if species in
						// imported file exisits in DB.
						for (UINT i = 0;i < vecSelSheets.size();i++)
						{
							sSpcCode.Format(L"%s",pExcel->GetSheetName(vecSelSheets[i]));
							sSpcName.Format(L"");
							m_vecNonMatchingSpecies.push_back(CSpecies(-1,sSpcCode,sSpcName,0.0,0.0));
						}	// for (UINT i = 0;i < vecSelSheets.size();i++)

						if (m_vecNonMatchingSpecies.size() > 0)
						{
							if ((pDlg = new CNoMatchSpeciesDlg()) != NULL)
							{
								pDlg->setSpecies(m_vecSpecies);
								pDlg->setSpeciesFromtable(m_vecNonMatchingSpecies);
								pDlg->setDB(m_pDB);
								nRet = pDlg->DoModal();
								if (nRet == IDOK)
								{
									pDlg->getChangeToSpecies(vecMatchedSpecies);
									delete pDlg;
								}
								else
								{
									delete pDlg;
									delete pSelectDlg;
									delete pExcel;
									return;
								}
							}	// if ((pDlg = new CNoMatchSpeciesDlg()) != NULL)
						}	// if (m_vecNonMatchingSpecies.size() >= 0)

						nSeltableType = pSelectDlg->getSelectedTableType();
						nMeasureMode = pSelectDlg->getSelectedMeasMode();
						// * Header *
						sTmp.Format(L"%d;%d;%d;%s;%s;%s;%s;%s;",
								FUNC_INDEX::HEADER,
								nSeltableType,			// Index for type of function
								nMeasureMode,				// Index for type of measuring-mode
								L"",		// Name of function not set
								L"",		// Abbrevation not set
								L"",		// Basis not set
								L"",		// Created by not set
								L"");		// Date not set
						sarr.Add(sTmp + L"\n");
							
						if (vecMatchedSpecies.size() == vecSelSheets.size())
							{
						// Start building the text-file holding table-data
						for (UINT i = 0;i < vecSelSheets.size();i++)
						{
							pExcel->SetCurrentSheet(vecSelSheets[i]);
							
							sValue.Format(L"%d;%d;",FUNC_INDEX::SPECIES,vecMatchedSpecies[i].getChangeToSpcID());
							sarr.Add(sValue + L"\n");
							
							sValue.Empty();
							sValue.Format(L"%d;",FUNC_INDEX::COL_HEADER);
							fPrevStep = 0.0;
							for (int col = 0;col < pExcel->GetColCount();col++)
							{
								if (col > 0 && col < pExcel->GetColCount()-1)
								{
									pExcel->GetCellValue(0,col,&fLength1);
									pExcel->GetCellValue(0,col+1,&fLength2);
									sTmp.Format(L"%.1f$%.1f$;",fLength1,fLength2-fLength1);
									sValue += sTmp;
									fPrevStep = (fLength2-fLength1);
								}	// if (col > 0 && col < pExcel->GetColCount())
								else if (col == pExcel->GetColCount()-1)
								{
									pExcel->GetCellValue(0,col,&fLength1);
									sTmp.Format(L"%.1f$%.1f$;",fLength1,fPrevStep);
									sValue += sTmp;
								}

							}	// for (int col = 0;col < pExcel->GetColCount();col++)

							sarr.Add(sValue);
							sValue.Empty();
							fPrevStep = 0.0;
							for (int row = 0;row < pExcel->GetTotalRows();row++)
							{
								for (int col = 0;col < pExcel->GetColCount();col++)
								{

									if ((row > 0 && row <= pExcel->GetTotalRows()))
									{
										if (col == 0)
										{
											pExcel->GetCellValue(row,col,&fDiam1);
											pExcel->GetCellValue(row+1,col,&fDiam2);
											if (fDiam2 > 0)
												fPrevStep = fDiam2-fDiam1;
											sTmp.Format(L"%.1f$%.1f",fDiam1,fPrevStep);
											sValue.Format(L"%d;%s$;",FUNC_INDEX::DATA,sTmp);
										}
										else
										{
											pExcel->GetCellValue(row,col,&fDiam);
											sTmp.Format(L"%.3f",fDiam);
											sValue += sTmp + L";";
										}
									}	// if ((row > 0 && row < pExcel->GetTotalRows()-1) && col == 0)

								}	// for (int col = 0;col < pExcel->GetColCount();col++)
								sarr.Add(sValue + L"\n");
							}	// for (int row = 0;row < pExcel->GetTotalRows();row++)

						}	// for (UINT i = 0;i < vecSelSheets.size();i++)
						}//if (vecMatchedSpecies.size() == vecSelSheets.size())
					}	// if (vecSelSheets.size() > 0)

					// Setup the data
					sValue.Empty();
					for (int i = 0;i < sarr.GetCount();i++)
					{
						sValue += sarr.GetAt(i);
					}
					sarrSheets.RemoveAll();
					sarr.RemoveAll();
					//----------------------------------------------------------------------
					// Add the imported volumetable to DB and populate
					if (m_pDB != NULL)
					{
						//m_dt7_2.setDateInComboBox();

						// Save data first, if there's data to save
						if (m_vecUserVolTables.size() > 0)
						{
							Save();
							clearData();
						}
						if (m_pDB->addUserVolTable(CUserVolTables(-1,createUniqueNumber(m_vecUserVolTables),L"",L"",L"",getUserName().MakeUpper(),m_csDate,nSeltableType,nMeasureMode,L"",sValue,0)))
						{
							m_pDB->getSpecies(m_vecSpecies);
							m_pDB->getUserVolTables(m_vecUserVolTables);
							m_nSelectedIndex = m_vecUserVolTables.size() - 1;	// Point to last entry
							populateData();
							setNavigationButtons(m_nSelectedIndex > 0,m_nSelectedIndex < (m_vecUserVolTables.size()-1));
						}
					}	// if (m_pDB != NULL)
					//----------------------------------------------------------------------
				}	// if (pSelectDlg->DoModal() == IDOK)
				delete pSelectDlg;

			}	// if (pSelectDlg != NULL)
		}	// if (pExcel->OpenExcel(sFile))

		delete pExcel;
	}

}


// Use this function to import from an Exported table.
void CUserCreateVolFunc::ImportFromExported(LPCTSTR file)
{
	CNoMatchSpeciesDlg *pDlg;
	std::vector<int> vecSelSheets;
	CStringArray sarr;
	double fDiam = 0.0;
	int nRet = -1,nSeltableType = -1,nMeasureMode = -1;
	CStringArray sarrSheets;
	CVecMatchedSpecies vecMatchedSpecies;
	CString sFile = L"",sValue = L"",sTmp = L"",sSpcID = L"",sSpcCode = L"",sSpcName = L"";

	sFile = file;

	UpdateData(TRUE);

	CExcelImportLibXl *pExcel = new CExcelImportLibXl();
	if (pExcel != NULL)
	{

		if (pExcel->OpenExcel(sFile))
		{
			enableView(TRUE);
			m_bEnableToolBarBtnSave = TRUE;
			m_bEnableToolBarBtnDelete = TRUE;
			m_bEnableToolBarBtnAddSpc = TRUE;
			m_bEnableToolBarBtnDelSpc = TRUE;

			for (short i = 0;i < pExcel->GetSheetCount();i++)
			{
				sarrSheets.Add(pExcel->GetSheetName(i));
			}

			CSelectExcelSheetDlg *pSelectDlg = new CSelectExcelSheetDlg();
			if (pSelectDlg != NULL)
			{
				pSelectDlg->setSheetNames(sarrSheets);
				if (pSelectDlg->DoModal() == IDOK)
				{
					m_vecNonMatchingSpecies.clear();
					pSelectDlg->getSelectedSheets(vecSelSheets);
					if (vecSelSheets.size() > 0)
					{
						// Read info. on species in imported file
						// Info. is set in row=0 and column=0 on each sheet
						// Use this information to find out if species in
						// imported file exisits in DB.
						for (UINT i = 0;i < vecSelSheets.size();i++)
						{
							pExcel->SetCurrentSheet(vecSelSheets[i]);
							// Get information on species, set as comment on row=0 and col=0
							pExcel->GetComment(0,0,sValue);
							AfxExtractSubString(sSpcID,sValue,0,';');
							AfxExtractSubString(sSpcCode,sValue,1,';');
							AfxExtractSubString(sSpcName,sValue,2,';');
							//if (!checkSpecie(sSpcCode) && _tstoi(sSpcID) > -1)
							//{
								m_vecNonMatchingSpecies.push_back(CSpecies(-1,sSpcCode,sSpcName,0.0,0.0));
							//}
						}	// for (UINT i = 0;i < vecSelSheets.size();i++)

						if (m_vecNonMatchingSpecies.size() > 0)
						{
							if ((pDlg = new CNoMatchSpeciesDlg()) != NULL)
							{
								pDlg->setSpecies(m_vecSpecies);
								pDlg->setSpeciesFromtable(m_vecNonMatchingSpecies);
								pDlg->setDB(m_pDB);
								nRet = pDlg->DoModal();
								if (nRet == IDOK)
								{
									pDlg->getChangeToSpecies(vecMatchedSpecies);
									delete pDlg;
								}
								else
								{
									delete pDlg;
									delete pSelectDlg;
									delete pExcel;
									return;
								}
							}	// if ((pDlg = new CNoMatchSpeciesDlg()) != NULL)
						}	// if (m_vecNonMatchingSpecies.size() >= 0)

						nSeltableType = pSelectDlg->getSelectedTableType();
						nMeasureMode = pSelectDlg->getSelectedMeasMode();
						// * Header *
						sTmp.Format(L"%d;%d;%d;%s;%s;%s;%s;%s;",
								FUNC_INDEX::HEADER,
								nSeltableType,			// Index for type of function
								nMeasureMode,				// Index for type of measuring-mode
								L"",		// Name of function not set
								L"",		// Abbrevation not set
								L"",		// Basis not set
								L"",		// Created by not set
								L"");		// Date not set
						sarr.Add(sTmp + L"\n");

						if (vecMatchedSpecies.size() == vecSelSheets.size())
							{
						// Start building the text-file holding table-data
						for (UINT i = 0;i < vecSelSheets.size();i++)
						{
							pExcel->SetCurrentSheet(vecSelSheets[i]);
							pExcel->GetComment(0,0,sValue);
							AfxExtractSubString(sTmp,sValue,0,';');	
							sValue.Empty();
							
								sValue.Format(L"%d;%d;",FUNC_INDEX::SPECIES,vecMatchedSpecies[i].getChangeToSpcID());
								sarr.Add(sValue + L"\n");
							
							sValue.Empty();
							sValue.Format(L"%d;",FUNC_INDEX::COL_HEADER);
							for (int col = 0;col < pExcel->GetColCount();col++)
							{
								if (col > 0 && col < pExcel->GetColCount())
								{
									pExcel->GetComment(0,col,sTmp);
									sValue += sTmp;
								}	// if (col > 0 && col < pExcel->GetColCount())
							}	// for (int col = 0;col < pExcel->GetColCount();col++)

							sarr.Add(sValue);
							sValue.Empty();

							for (int row = 0;row < pExcel->GetTotalRows();row++)
							{
								for (int col = 0;col < pExcel->GetColCount();col++)
								{

									if ((row > 0 && row <= pExcel->GetTotalRows()))
									{
										if (col == 0)
										{
											pExcel->GetComment(row,col,sTmp);
											sValue.Format(L"%d;%s$;",FUNC_INDEX::DATA,sTmp);
										}
										else
										{
											pExcel->GetCellValue(row,col,&fDiam);
											sTmp.Format(L"%.3f",fDiam);
											sValue += sTmp + L";";
										}
									}	// if ((row > 0 && row < pExcel->GetTotalRows()-1) && col == 0)

								}	// for (int col = 0;col < pExcel->GetColCount();col++)
								sarr.Add(sValue + L"\n");
							}	// for (int row = 0;row < pExcel->GetTotalRows();row++)

						}	// for (UINT i = 0;i < vecSelSheets.size();i++)
						}//if (vecMatchedSpecies.size() == vecSelSheets.size())
					}	// if (vecSelSheets.size() > 0)

					// Setup the data
					sValue.Empty();
					for (int i = 0;i < sarr.GetCount();i++)
					{
						sValue += sarr.GetAt(i);
					}
					sarrSheets.RemoveAll();
					sarr.RemoveAll();
					//----------------------------------------------------------------------
					// Add the imported volumetable to DB and populate
					if (m_pDB != NULL)
					{
						//m_dt7_2.setDateInComboBox();

						// Save data first, if there's data to save
						if (m_vecUserVolTables.size() > 0)
						{
							Save();
							clearData();
						}
						if (m_pDB->addUserVolTable(CUserVolTables(-1,createUniqueNumber(m_vecUserVolTables),L"",L"",L"",getUserName().MakeUpper(),m_csDate,nSeltableType,nMeasureMode,L"",sValue,0)))
						{
							m_pDB->getSpecies(m_vecSpecies);
							m_pDB->getUserVolTables(m_vecUserVolTables);
							m_nSelectedIndex = m_vecUserVolTables.size() - 1;	// Point to last entry
							populateData();
							setNavigationButtons(m_nSelectedIndex > 0,m_nSelectedIndex < (m_vecUserVolTables.size()-1));
						}
					}	// if (m_pDB != NULL)
					//----------------------------------------------------------------------
				}	// if (pSelectDlg->DoModal() == IDOK)
				delete pSelectDlg;

			}	// if (pSelectDlg != NULL)
		}	// if (pExcel->OpenExcel(sFile))

		delete pExcel;
	}

}


// Importera fr�n ex. Excel.
void CUserCreateVolFunc::Import()
{
	CString strFile = _T("*.xls;*.xlsx");
	CString strFilter = L"",sFile = L"",sValue = L"",S;
	strFilter.Format(L"%s (*.xls; *.xslx)|*.xls; *.xslx|",m_sOpenDlgEXCEL);
	int nRet = -1;

	CFileDialog dlg(TRUE,L"xls",strFile,OFN_OVERWRITEPROMPT|OFN_HIDEREADONLY,strFilter);
	if(dlg.DoModal() != IDOK) return;

	sFile = dlg.GetPathName();

	CExcelImportLibXl *pExcel = new CExcelImportLibXl();
	if (pExcel != NULL)
	{

		if (pExcel->OpenExcel(sFile))
		{
			pExcel->GetComment(0,0,sValue);
			if (sValue.Replace(';','$') == 3)
				nRet = 1;
			else
				nRet = 0;
		}
		delete pExcel;
	}
	if (nRet == 1)
		ImportFromExported(sFile);
	else if (nRet == 0)
		ImportSimple(sFile);
}

void CUserCreateVolFunc::Export()
{
	CString sToken = L"",sPath = L"",sFileName = L"",sTmp = L"",S;
	int f[4];
	CXTPTabManagerItem *pTab = NULL;
	CUserVolumeTableRec *pRec = NULL;
	CXTPReportRow *pRow = NULL;
	CXTPReportColumn *pCol = NULL;
	CReportSpcView *pSelectedView = NULL;
	CSpecies recSpc = CSpecies();

	Book* book = xlCreateBook();
	if (book)
	{
		book->setKey(L"Anders Gustafsson", L"windows-2322230e0dc9e00d6ab2656fa0hfeflb");

		f[0] = book->addCustomNumFormat(L"0");
		f[1] = book->addCustomNumFormat(L"0.0");
		f[2] = book->addCustomNumFormat(L"0.000");
		Format* fmt[3];
		fmt[0] = book->addFormat();
		fmt[0]->setNumFormat(f[0]);
		fmt[1] = book->addFormat();
		fmt[1]->setNumFormat(f[1]);
		fmt[2] = book->addFormat();
		fmt[2]->setNumFormat(f[2]);

		for (int tab = 0;tab < m_wndTabControl.getNumOfTabPages();tab++)
		{
			if ((pSelectedView = (CReportSpcView*)getReportSpcView(tab)) != NULL && 
					(pTab = m_wndTabControl.getTabPage(tab)) != NULL)
			{
				if (pSelectedView->GetReportCtrl().GetColumns()->GetCount() > 1 &&
						pSelectedView->GetReportCtrl().GetRows()->GetCount() > 1)
						
				{
	
					Sheet* sheet = book->addSheet(pTab->GetCaption());
					if (sheet)
					{
						getSpecies(m_mapSpcID[tab],recSpc);
						sTmp.Format(L"%d;%s;%s;",recSpc.getSpcID(),recSpc.getSpcCode(),recSpc.getSpcName());
						sheet->writeComment(0,0,sTmp);
						for (int row = 0;row < pSelectedView->GetReportCtrl().GetRows()->GetCount();row++)
						{
							if ((pRow = pSelectedView->GetReportCtrl().GetRows()->GetAt(row)) != NULL)
							{			
								if ((pRec = (CUserVolumeTableRec *)pRow->GetRecord()) != NULL)
								{

									for (int col = 0;col < pSelectedView->GetReportCtrl().GetColumns()->GetCount();col++)
									{
										if (row == 0 && col > 0)
										{
											pCol = pSelectedView->GetReportCtrl().GetColumns()->GetAt(col);
											AfxExtractSubString(sToken,pCol->GetInternalName(),0,'$');
											sheet->writeComment(row,col,pCol->GetInternalName());
											sheet->writeNum(row,col,_tstof(localeToStr(sToken)),fmt[1]);
											sheet->writeNum(row+1,col,pRec->getColFloat(col),fmt[2]);
										}
										else
										{
											if (col == 0)
											{
												sheet->writeNum(row+1,col,pRec->getColFloat(col),fmt[0]);
												sheet->writeComment(row+1,col,pRec->getDiamStep());
											}
											else
											{
												sheet->writeNum(row+1,col,pRec->getColFloat(col),fmt[2]);
											}
										}
									}	// for (int col = 0;col < pSelectedView->GetReportCtrl().GetColumns()->GetCount();col++)
								}	// if ((pRec = (CUserVolumeTableRec *)pRow->GetRecord()) != NULL)

							}	// if ((pRow = pSelectedView->GetReportCtrl().GetRows()->GetAt(col)) != NULL)
						}	// for (int row = 0;row < pSelectedView->GetReportCtrl().GetRows()->GetCount();row++)
						sheet->setProtect();
					}	// if (sheet)
							
				}	// if (pSelectedView->GetReportCtrl().GetColumns()->GetCount() > 1 &&
			}	// if ((pSelectedView = (CReportSpcView*)getReportSpcView(i)) != NULL)
		}	// for (int i = 0;i < m_wndTabControl.getNumOfTabPages();i++)

		sPath = regGetStr(REG_ROOT,VTABLE_EXPORT_TO_EXCEL_DIR,DIRECTORY_KEY);
		sPath += m_edit7_1.getText();
		
		CFileDialog dlg(FALSE,L".xls",sPath,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,L"EXCEL (*.xls)|*.xls|");
		if (dlg.DoModal() == IDOK)
		{
			if(book->save(dlg.GetPathName())) 
			{
#ifdef _OPEN_EXCEL_IN_VOLUMETABLE

				if (::MessageBox(GetSafeHwnd(),m_sMsgOpenEXCEL,m_sMsgCap,MB_ICONQUESTION | MB_YESNO) == IDYES)
						::ShellExecute(NULL, L"open", dlg.GetPathName(), NULL, NULL, SW_SHOW);        
#endif
			}
		
			regSetStr(REG_ROOT,VTABLE_EXPORT_TO_EXCEL_DIR,DIRECTORY_KEY,getFilePath(dlg.GetPathName()));
		}
	}	// if (book)
}

void CUserCreateVolFunc::PreView()
{
	AfxMessageBox(L"PreView()");
}

// CUserCreateVolFunc diagnostics

#ifdef _DEBUG
void CUserCreateVolFunc::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CUserCreateVolFunc::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG


void CUserCreateVolFunc::removeColumns(DEL_COLUMNS del_cols)
{
	CStringArray sarrData;
	CXTPReportColumn *pCol = NULL;
	CXTPReportColumn *pColNext = NULL;
	CReportSpcView *pSelectedView = NULL;
	if (del_cols == CUserCreateVolFunc::DEL_ALL_COLUMNS)
	{
		for (int i = 0;i < m_wndTabControl.getNumOfTabPages();i++)
		{
			if ((pSelectedView = (CReportSpcView*)getReportSpcView(i)) != NULL)
			{
				if (pSelectedView->GetReportCtrl().GetColumns()->GetCount() > 1)
				{
					for (int ii = pSelectedView->GetReportCtrl().GetColumns()->GetCount();ii > 0;ii--)
					{
						pCol = pSelectedView->GetReportCtrl().GetColumns()->GetAt(ii);
						if (pCol != NULL)
						{
							pSelectedView->GetReportCtrl().GetColumns()->Remove(pCol);
							pSelectedView->GetReportCtrl().GetColumns()->GetAt(ii);
						}
					}
				}
				m_edit7_7.SetWindowTextW(L"");
				m_edit7_8.SetWindowTextW(L"");
				pSelectedView->GetReportCtrl().Populate();
				pSelectedView->GetReportCtrl().UpdateWindow();
			}
		}

	}
	// Remove last column
	if (del_cols == CUserCreateVolFunc::DEL_LAST_COLUMN)
	{
		if ((pSelectedView = (CReportSpcView*)getReportSpcView()) != NULL)
		{
			if (pSelectedView->GetReportCtrl().GetColumns()->GetCount() > 1)
			{
				pCol = pSelectedView->GetReportCtrl().GetColumns()->GetAt(pSelectedView->GetReportCtrl().GetColumns()->GetCount()-1);
				if (pCol != NULL)
				{
					pSelectedView->GetReportCtrl().GetColumns()->Remove(pCol);
					pColNext = pSelectedView->GetReportCtrl().GetColumns()->GetAt(pSelectedView->GetReportCtrl().GetColumns()->GetCount()-1);
					if (pColNext != NULL)
					{
						tokenizeString(pColNext->GetInternalName(),'$',sarrData);
						if (sarrData.GetCount() == 2)
						{
							m_edit7_7.setFloat(_tstof(sarrData.GetAt(0)),1);
							m_edit7_8.setFloat(_tstof(sarrData.GetAt(1)),1);
						}
						else
						{
							m_edit7_7.SetWindowTextW(L"");
							m_edit7_8.SetWindowTextW(L"");
						}
					}
				}
			}
			pSelectedView->GetReportCtrl().Populate();
			pSelectedView->GetReportCtrl().UpdateWindow();
		}
	}
}

void CUserCreateVolFunc::removeRows(DEL_ROWS del_rows)
{
	CReportSpcView *pSelectedView = NULL;
	if (del_rows == CUserCreateVolFunc::DEL_ALL_ROWS)
	{
		for (int i = 0;i < m_wndTabControl.getNumOfTabPages();i++)
		{
			if ((pSelectedView = (CReportSpcView*)getReportSpcView(i)) != NULL)
			{
				for (int ii = pSelectedView->GetReportCtrl().GetRows()->GetCount();ii >= 0;ii--)
				{
					CUserVolumeTableRec *pRec = (CUserVolumeTableRec*)pSelectedView->GetReportCtrl().GetRecords()->GetAt(ii);
					if (pRec != NULL)
					{
						pSelectedView->GetReportCtrl().RemoveRecordEx(pRec);
					}
				}
				m_edit7_7.SetWindowTextW(L"");
				m_edit7_8.SetWindowTextW(L"");
				pSelectedView->GetReportCtrl().Populate();
				pSelectedView->GetReportCtrl().UpdateWindow();
			}
		}
	}
	// Remove last row
	if (del_rows == CUserCreateVolFunc::DEL_LAST_ROW)
	{
		if ((pSelectedView = (CReportSpcView*)getReportSpcView()) != NULL)
		{
			CUserVolumeTableRec *pRec = (CUserVolumeTableRec*)pSelectedView->GetReportCtrl().GetRecords()->GetAt(pSelectedView->GetReportCtrl().GetRows()->GetCount()-1);
			if (pRec != NULL)
			{
				pSelectedView->GetReportCtrl().RemoveRecordEx(pRec);
			}
		}
		pSelectedView->GetReportCtrl().Populate();
		pSelectedView->GetReportCtrl().UpdateWindow();
	}
}

void CUserCreateVolFunc::removeSpecies(DEL_SPECIES del_species)
{
	CString sMsg = L"";
	CXTPTabManagerItem *pItem = NULL;

	if (del_species == CUserCreateVolFunc::DEL_SELECTED_SPECIES)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.getSelectedTabPage();
		if (pItem != NULL)
		{
			sMsg.Format(m_sMsgRemoveSpecies1,pItem->GetCaption());
			if (::MessageBox(GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONSTOP | MB_YESNO | MB_DEFBUTTON2) == IDYES)
			{
				if (::MessageBox(GetSafeHwnd(),m_sMsgRemoveSpecies2,m_sMsgCap,MB_ICONSTOP | MB_YESNO | MB_DEFBUTTON2) == IDYES)
				{
					pItem->Remove();
				}
			}
		}
	}
	else if (del_species == CUserCreateVolFunc::DEL_ALL_SPECIES)
	{
		int nNumOfSpecies = m_wndTabControl.getNumOfTabPages();
		if (nNumOfSpecies > 0)
		{
			for (int i = 0;i < nNumOfSpecies;i++)
			{
				CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(0);
				if (pItem != NULL)
				{
					pItem->Remove();
				}
			}
		}
	}

	// Check if ALL tabs been removed. If so, clear start and step
	if (m_wndTabControl.getNumOfTabPages() == 0)
	{
		m_edit7_7.SetWindowTextW(L"");
		m_edit7_8.SetWindowTextW(L"");
		m_edit7_9.SetWindowTextW(L"");
		m_edit7_10.SetWindowTextW(L"");
	}
}

void CUserCreateVolFunc::addFirstColumnCaption(void)
{
	CStringArray sarrColCap;
	int nSelection1 = m_cbox7_1.GetCurSel();
	if (nSelection1 != CB_ERR)
	{
		tokenizeString(m_sColumnCaps,';',sarrColCap);
		if (sarrColCap.GetCount() > 0)
		{
			// Add to ALL views
			for (int i = 0;i < m_wndTabControl.getNumOfTabPages();i++)
			{

				CReportSpcView *pSelectedView = (CReportSpcView*)getReportSpcView(i);
				if (pSelectedView != NULL)
				{
					pSelectedView->GetReportCtrl().GetColumns()->GetAt(0)->SetCaption(sarrColCap.GetAt(nSelection1));
				}
			}
		}
	}
}

// Setup data
void CUserCreateVolFunc::populateData()
{
	CString sCBoxText = L"";
	CStringArray sarrMeasureMode;
	int nNumOfSpecies = 0,nTabNumber = -1;
	if (m_vecUserVolTables.size() > 0)
	{
		if (m_nSelectedIndex >= 0 && m_nSelectedIndex < m_vecUserVolTables.size())
		{

			removeSpecies(CUserCreateVolFunc::DEL_ALL_SPECIES);

			removeRows(CUserCreateVolFunc::DEL_ALL_ROWS);
			removeColumns(CUserCreateVolFunc::DEL_ALL_COLUMNS);
			removeSpecies(CUserCreateVolFunc::DEL_ALL_SPECIES);

			m_recSelectedVolTable = m_vecUserVolTables[m_nSelectedIndex];

			m_cbox7_1.SetCurSel(m_recSelectedVolTable.getTableType());
			m_cbox7_1.EnableWindow(FALSE);

			m_cbox7_1.GetWindowTextW(sCBoxText);
			if (sCBoxText.Right(1) != L",")
				sCBoxText += L",";
			// Set column and row measuretypes as caption
			tokenizeString(sCBoxText,',',sarrMeasureMode);
			if (sarrMeasureMode.GetCount() == 2)
			{
				m_lbl7_9.SetWindowTextW(sarrMeasureMode.GetAt(0));
				m_lbl7_10.SetWindowTextW(sarrMeasureMode.GetAt(1));
			}

			m_cbox7_2.SetCurSel(m_recSelectedVolTable.getMeasuringMode());
			m_cbox7_2.EnableWindow(FALSE);

			
			m_edit7_1.SetWindowTextW(m_recSelectedVolTable.getFullName());
			m_edit7_2.SetWindowTextW(m_recSelectedVolTable.getAbbrevName());
			m_edit7_3.SetWindowTextW(m_recSelectedVolTable.getBasisName());
			m_edit7_4.SetWindowTextW(m_recSelectedVolTable.getCreatedBy());
			m_csDate = m_recSelectedVolTable.getDate();
			m_edit7_6.SetWindowTextW(m_recSelectedVolTable.getNotes());

			if (m_recSelectedVolTable.getMeasuringMode() == 0)	// Inch
			{
				m_lbl7_15.SetWindowTextW(m_sFeet);
				m_lbl7_16.SetWindowTextW(m_sFeet);
				m_lbl7_17.SetWindowTextW(m_sInch);
				m_lbl7_18.SetWindowTextW(m_sInch);
			}
			else if (m_recSelectedVolTable.getMeasuringMode() == 1)	// Metric 
			{
				m_lbl7_15.SetWindowTextW(m_sM);
				m_lbl7_16.SetWindowTextW(m_sM);
				m_lbl7_17.SetWindowTextW(m_sMM);
				m_lbl7_18.SetWindowTextW(m_sMM);
			}

			setupSpeciesTabs(m_recSelectedVolTable.getVolTable());
			addFirstColumnCaption();
			setupSpeciesTableColumnData(m_recSelectedVolTable.getVolTable());
			setupSpeciesTableRowData(m_recSelectedVolTable.getVolTable());
			setStartAndStepData();

			UpdateData(FALSE);
		}
	}
	else
	{
		clearData();
	}
}

// Skapa data-filen som sparas i databasen
void CUserCreateVolFunc::createData()
{
	CStringArray sarr;	// Get column-data (e.g. length(s) etc.)
	CString sData = L"",sTmp = L"";
	CStringArray sarrData;
	CXTPReportColumn *pCol = NULL;
	CUserVolumeTableRec *pRec = NULL;
	CReportSpcView *pSelectedView = NULL;

	if (m_wndTabControl.getNumOfTabPages() > 0)
	{
		UpdateData(TRUE);

		// * Header *
		sTmp.Format(L"%d;%d;%d;%s;%s;%s;%s;%s;",
			FUNC_INDEX::HEADER,
			m_cbox7_1.GetCurSel(),	// Index for type of function
			m_cbox7_2.GetCurSel(),	// Index for type of measuring-mode
			m_edit7_1.getText(),		// Name of function
			m_edit7_2.getText(),		// Abbrevation
			m_edit7_3.getText(),		// Basis
			m_edit7_4.getText(),		// Created by
			m_csDate);									// Date
		sarr.Add(sTmp);

		for (int i = 0;i < m_wndTabControl.getNumOfTabPages();i++)
		{
			if ((pSelectedView = (CReportSpcView*)getReportSpcView(i)) != NULL)
			{
			
				// * Species *
				if (i >= 0 && i < m_mapSpcID.size())
				{
					sTmp.Format(L"%d;%d;",FUNC_INDEX::SPECIES,m_mapSpcID[i]);
					sarr.Add(sTmp);
				}

				// Column header (e.g. diam, length)
				sData.Format(L"%d;",FUNC_INDEX::COL_HEADER);
				for (int cols = 0;cols < pSelectedView->GetReportCtrl().GetColumns()->GetCount();cols++)
				{
					if ((pCol = pSelectedView->GetReportCtrl().GetColumns()->GetAt(cols)) != NULL)
					{
						if (cols > 0)	// We don't need to read first column
						{
							tokenizeString(pCol->GetInternalName(),'$',sarrData);
							if (sarrData.GetCount() == 2)
							{
								sTmp.Format(L"%s$%s$;",sarrData.GetAt(0),sarrData.GetAt(1));
								sData += sTmp;
							}
						}	// if (cols > 0)
					}
				}	// for (int cols = 0;cols < pSelectedView->GetReportCtrl().GetColumns()->GetCount();cols++)
				
				// * Column header data *
				sarr.Add(sData);

				// * Row data *
				for (int rows = 0;rows < pSelectedView->GetReportCtrl().GetRows()->GetCount();rows++)
				{
					sData.Format(L"%d;",FUNC_INDEX::DATA);
					if ((pRec = (CUserVolumeTableRec*)pSelectedView->GetReportCtrl().GetRecords()->GetAt(rows)) != NULL)
					{
						for (int row_cols = 0;row_cols < pSelectedView->GetReportCtrl().GetColumns()->GetCount();row_cols++)
						{
							if (row_cols == 0)
							{
								sTmp.Format(L"%.1f$%.1f$;",pRec->getColFloat(row_cols),pRec->getStep());
								sData += localeToStr(sTmp);
							}
							else
							{
								sTmp.Format(L"%.3f;",pRec->getColFloat(row_cols));
								sData += localeToStr(sTmp);
							}
						}				
					}	// if ((pRec = (CUserVolumeTableRec*)pSelectedView->GetReportCtrl().GetRecords()->GetAt(ii)) != NULL)			

					sarr.Add(sData);

				}	// for (int ii = 0;ii < pSelectedView->GetReportCtrl().GetRows()->GetCount();ii++)
			}	// if ((pSelectedView = (CReportSpcView*)getReportSpcView(i)) != NULL)

		}	// for (int i = 0;i < m_wndTabControl.getNumOfTabPages();i++)
		sData = L"";
		for (int i1 = 0;i1 < sarr.GetCount();i1++)
			sData += sarr.GetAt(i1) + L"\n";

		if (m_pDB != NULL)
		{
			if (m_recSelectedVolTable.getID() < 0)
			{
				m_pDB->addUserVolTable(CUserVolTables(-1,
																							createUniqueNumber(m_vecUserVolTables),	// equals function id
																							m_edit7_1.getText(),
																							m_edit7_2.getText(),
																							m_edit7_3.getText(),
																							m_edit7_4.getText(),
																							m_csDate,
																							m_cbox7_1.GetCurSel(),
																							m_cbox7_2.GetCurSel(),
																							m_edit7_6.getText(),
																							sData,
																							0));
				m_pDB->getUserVolTables(m_vecUserVolTables);
				m_nSelectedIndex = m_vecUserVolTables.size()-1;

			}
			else
			{
				m_pDB->updUserVolTable(CUserVolTables(m_recSelectedVolTable.getID(),
																							m_recSelectedVolTable.getFuncID(),
																							m_edit7_1.getText(),
																							m_edit7_2.getText(),
																							m_edit7_3.getText(),
																							m_edit7_4.getText(),
																							m_csDate,
																							m_cbox7_1.GetCurSel(),
																							m_cbox7_2.GetCurSel(),
																							m_edit7_6.getText(),
																							sData,
																							0));
				m_pDB->getUserVolTables(m_vecUserVolTables);
			}

			if (m_nSelectedIndex >= 0 && m_nSelectedIndex < m_vecUserVolTables.size())
				m_recSelectedVolTable = m_vecUserVolTables[m_nSelectedIndex];
	
			setNavigationButtons(m_nSelectedIndex > 0,m_nSelectedIndex < (m_vecUserVolTables.size()-1));

		}

	}	// if (m_wndTabControl.getNumOfTabPages() > 0)
}


BOOL CUserCreateVolFunc::getSpecies(int id,CSpecies& rec)
{
	BOOL bFound = FALSE;

	if (id == USER_CREATED_VOL_TABLE_SPC)
		return TRUE;

	if (m_vecSpecies.size() > 0)
	{
		for (UINT i = 0;i < m_vecSpecies.size();i++)
		{
			rec = m_vecSpecies[i];
			if (rec.getSpcID() == id)
			{
				bFound = TRUE;			
				break;
			}
		}
	}

	return bFound;
}

// If set as general tab, table is for all species
void CUserCreateVolFunc::addSpeciesToTabs(bool is_general_tab)
{
	CString sSpc = L"";
	if (!is_general_tab)
	{
		if (m_vecSpeciesSelected.size() > 0)
		{
			for (UINT i = 0;i < m_vecSpeciesSelected.size();i++)
			{
				if (!m_vecSpeciesSelected[i].getSpcName().IsEmpty())
					sSpc.Format(L"%s - %s",m_vecSpeciesSelected[i].getSpcCode(),m_vecSpeciesSelected[i].getSpcName());
				else
					sSpc.Format(L"%s",m_vecSpeciesSelected[i].getSpcCode());

				// Check that spceis isn't already in TabControl
				if (!isAlreadyOnTab(sSpc))
				{
					if (m_vecSpeciesSelected[i].getSpcID() > -1)
					{
						AddView(RUNTIME_CLASS(CReportSpcView), sSpc,-1,m_vecSpeciesSelected[i].getSpcID());
					}
					else
					{
						AddView(RUNTIME_CLASS(CReportSpcView), m_sGeneralCap,-1,USER_CREATED_VOL_TABLE_SPC);
					}
				}
			}
		}
	}
	else
	{
		// Check that spceis isn't already in TabControl
		if (!isAlreadyOnTab(m_sGeneralCap))
		{
			AddView(RUNTIME_CLASS(CReportSpcView), m_sGeneralCap,-1,USER_CREATED_VOL_TABLE_SPC);
		}
	}
}


void CUserCreateVolFunc::setStartAndStepData()
{
	CStringArray sarrData;
	CXTPReportColumn *pColLast = NULL;
	CXTPReportRecords *pRecs = NULL;
	CReportSpcView *pSelectedView = (CReportSpcView*)getReportSpcView();
	if (pSelectedView != NULL)
	{
		pColLast = pSelectedView->GetReportCtrl().GetColumns()->GetAt(pSelectedView->GetReportCtrl().GetColumns()->GetCount()-1);
		if (pColLast != NULL)
		{
			tokenizeString(pColLast->GetInternalName(),'$',sarrData);
			if (sarrData.GetCount() == 2)
			{
				m_edit7_7.setFloat(_tstof(sarrData.GetAt(0)),1);
				m_edit7_8.setFloat(_tstof(sarrData.GetAt(1)),1);
			}
			else
			{
				m_edit7_7.SetWindowTextW(L"");
				m_edit7_8.SetWindowTextW(L"");
			}
		}

		if ((pRecs = pSelectedView->GetReportCtrl().GetRecords()) != NULL)
		{
			if (pRecs->GetCount() > 0)
			{
				CUserVolumeTableRec *pRec = (CUserVolumeTableRec*)pRecs->GetAt(pRecs->GetCount()-1);
				if (pRec != NULL)
				{
					m_edit7_9.setFloat(pRec->getDiam(),0);
					m_edit7_10.setFloat(pRec->getStep(),0);
				}
			}
			else
			{
				m_edit7_9.SetWindowTextW(L"");
				m_edit7_10.SetWindowTextW(L"");
			}
		}

	}

}

void CUserCreateVolFunc::clearData()
{

	// Start clearing
	removeRows(CUserCreateVolFunc::DEL_ALL_ROWS);
	removeColumns(CUserCreateVolFunc::DEL_ALL_COLUMNS);
	removeSpecies(CUserCreateVolFunc::DEL_ALL_SPECIES);

	m_cbox7_1.SetCurSel(-1);
	m_cbox7_2.SetCurSel(-1);

	m_edit7_1.SetWindowTextW(L"");
	m_edit7_2.SetWindowTextW(L"");
	m_edit7_3.SetWindowTextW(L"");
	m_edit7_4.SetWindowTextW(getUserName().MakeUpper());
	//m_dt7_2.setDateInComboBox();
	m_dtDate.SetTime(&CTime::GetCurrentTime());
	m_edit7_6.SetWindowTextW(L"");
	m_edit7_8.SetWindowTextW(L"");
	m_edit7_9.SetWindowTextW(L"");
	m_edit7_10.SetWindowTextW(L"");

}


//*****************************************************************************************
// Hj�lpfunktioner f�r att l�sa funktions-data

void CUserCreateVolFunc::addColumnToTab()
{
	CXTPReportColumn *pCol = NULL;
	CReportSpcView *pSelectedView = (CReportSpcView*)getReportSpcView();
	double fStart = m_edit7_7.getFloat();
	double fStep = m_edit7_8.getFloat();
	int nLast = -1;
	CString sCap = L"";
	CString sInternal = L"";
	if (fStart >= 0.0 && fStep > 0.0 && pSelectedView != NULL)
	{
		if (pSelectedView->GetReportCtrl().GetColumns()->GetCount() > 1)
		{
			sCap.Format(L"%.1f-",fStart + fStep);
			sInternal.Format(L"%.1f$%.1f$;",fStart + fStep,fStep);
			m_edit7_7.setFloat(fStart + fStep,1);
		}
		else
		{
			sCap.Format(L"%.1f-",fStart);
			sInternal.Format(L"%.1f$%.1f$;",fStart,fStep);
			m_edit7_7.setFloat(fStart,1);
		}
		pCol = pSelectedView->GetReportCtrl().AddColumn(new CXTPReportColumn(pSelectedView->GetReportCtrl().GetColumns()->GetCount(), sCap, sInternal, 80,FALSE));
		pCol->SetHeaderAlignment(DT_CENTER);
		pCol->SetAlignment(DT_CENTER);
		pCol->GetEditOptions()->m_bAllowEdit = TRUE;
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
	}

	pSelectedView->GetReportCtrl().Populate();
	pSelectedView->GetReportCtrl().UpdateWindow();

	// We need to check if all columns have a recorditem 
	CXTPReportColumns *pCols = pSelectedView->GetReportCtrl().GetColumns();
	CXTPReportRecords *pRecs = pSelectedView->GetReportCtrl().GetRecords();
	if (pRecs != NULL && pCols != NULL)
	{
		for (int i = 0;i < pRecs->GetCount();i++)
		{
			CUserVolumeTableRec *pRec = (CUserVolumeTableRec*)pSelectedView->GetReportCtrl().GetRecords()->GetAt(i);
			if (pRec != NULL)
			{
				if (pRec->getNumOfColumns() < pCols->GetCount())
				{
					pRec->AddToColumn(pRec->getNumOfColumns(),pCols->GetCount()-1);
				}
			}

		}
		pSelectedView->GetReportCtrl().Populate();
		pSelectedView->GetReportCtrl().UpdateWindow();
	}
}

void CUserCreateVolFunc::addColumnToTab(double col_value,double step_value)
{
	CXTPReportColumn *pCol = NULL;
	CReportSpcView *pSelectedView = (CReportSpcView*)getReportSpcView();
	double fStart = col_value;
	double fStep = step_value;
	CString sCap = L"";
	CString sInternal = L"";
	if (fStart >= 0.0 && fStep > 0.0 && pSelectedView != NULL)
	{
		if (pSelectedView->GetReportCtrl().GetColumns()->GetCount() > 1)
		{
			sCap.Format(L"%.1f-",fStart);
			sInternal.Format(L"%.1f$%.1f$;",fStart,fStep);
			m_edit7_7.setFloat(fStep,1);
		}
		else
		{
			sCap.Format(L"%.1f-",fStart);
			sInternal.Format(L"%.1f$%.1f$;",fStart,fStep);
			m_edit7_7.setFloat(fStart,1);
		}
		pCol = pSelectedView->GetReportCtrl().AddColumn(new CXTPReportColumn(pSelectedView->GetReportCtrl().GetColumns()->GetCount(), sCap, sInternal, 80,FALSE));
		pCol->SetHeaderAlignment(DT_CENTER);
		pCol->SetAlignment(DT_CENTER);
		pCol->GetEditOptions()->m_bAllowEdit = TRUE;
		pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
	}

	pSelectedView->GetReportCtrl().Populate();
	pSelectedView->GetReportCtrl().UpdateWindow();
}

void CUserCreateVolFunc::addRowsToTab()
{
	double fStart = m_edit7_9.getFloat();
	double fStep = m_edit7_10.getFloat();
	CReportSpcView *pSelectedView = (CReportSpcView*)getReportSpcView();
	if (pSelectedView != NULL && fStart >= 0.0 && fStep > 0.0)
	{
		if (pSelectedView->GetReportCtrl().GetRows()->GetCount() == 0)
		{
			pSelectedView->GetReportCtrl().AddRecord(new CUserVolumeTableRec(fStart,fStep,pSelectedView->GetReportCtrl().GetColumns()->GetCount()));
		}
		else
		{
			CUserVolumeTableRec *pRec = (CUserVolumeTableRec*)pSelectedView->GetReportCtrl().GetRecords()->GetAt(pSelectedView->GetReportCtrl().GetRows()->GetCount()-1);
			if (pRec != NULL)
			{
				fStart = pRec->getColFloat(0)+fStep;
			}
			pSelectedView->GetReportCtrl().AddRecord(new CUserVolumeTableRec(fStart,fStep,pSelectedView->GetReportCtrl().GetColumns()->GetCount()));
		}
	}
	pSelectedView->GetReportCtrl().Populate();
	pSelectedView->GetReportCtrl().UpdateWindow();
}

void CUserCreateVolFunc::addRowsToTab(double row_value,double step_value,std::vector<double>& vec)
{
	double fStart = row_value*10.0;
	double fStep = step_value*10.0;
	CReportSpcView *pSelectedView = (CReportSpcView*)getReportSpcView();
	if (pSelectedView != NULL && fStart >= 0.0 && fStep > 0.0)
	{
		pSelectedView->GetReportCtrl().AddRecord(new CUserVolumeTableRec(fStart,fStep,vec));
	}
	pSelectedView->GetReportCtrl().Populate();
	pSelectedView->GetReportCtrl().UpdateWindow();
}

// Check number of Species (i.e. sum number of FUNC_INDEX::SPECIES)
int CUserCreateVolFunc::numberOfSpecies(LPCTSTR data)
{
	CString sData(data);
	int nCounter = 0;
	std::wstringstream ss;
	CString sToken = L"";
	std::wstring line;
	
	sData = m_recSelectedVolTable.getVolTable();
	ss << sData.GetBuffer();
	while (std::getline(ss,line))
	{
		AfxExtractSubString(sToken,line.c_str(),0,';');
		if (_tstoi(sToken) == FUNC_INDEX::SPECIES)
			nCounter++;
	}

	return nCounter;
}

// Add tabs to tabcontrol
void CUserCreateVolFunc::setupSpeciesTabs(LPCTSTR data)
{
	CSpecies rec = CSpecies();
	CString sData(data);
	int nCounter = 0;
	std::wstringstream ss;
	CString sToken = L"";
	std::wstring line;
	m_vecSpeciesSelected.clear();
	sData = m_recSelectedVolTable.getVolTable();
	ss << sData.GetBuffer();
	while (std::getline(ss,line))
	{
		AfxExtractSubString(sToken,line.c_str(),0,';');
		if (_tstoi(sToken) == FUNC_INDEX::SPECIES)
		{
			AfxExtractSubString(sToken,line.c_str(),1,';');
			if (getSpecies(_tstoi(sToken),rec))
			{
				m_vecSpeciesSelected.push_back(rec);
			}
		}
	}
	addSpeciesToTabs();
}

// Add columns and column-captions
void CUserCreateVolFunc::setupSpeciesTableColumnData(LPCTSTR data)
{
	CXTPReportColumn *pCol = NULL;
	CStringArray sarrData;
	CSpecies rec = CSpecies();
	CString sData(data),sCap = L"",sInternal = L"";;
	int nCounter = 1;
	int nTabNumber = -1;
	std::wstringstream ss;
	CString sToken = L"";
	std::wstring line;
	m_vecSpeciesSelected.clear();
	sData = m_recSelectedVolTable.getVolTable();
	ss << sData.GetBuffer();
	while (std::getline(ss,line))
	{
		AfxExtractSubString(sToken,line.c_str(),0,';');
		if (_tstoi(sToken) == FUNC_INDEX::SPECIES)
		{
			nCounter = 1;
			nTabNumber++;
		}
		// Add column headers
		if (_tstoi(sToken) == FUNC_INDEX::COL_HEADER && nTabNumber > -1)
		{
			CReportSpcView *pSelectedView = (CReportSpcView*)getReportSpcView(nTabNumber);
			if (pSelectedView != NULL)
			{
				while (AfxExtractSubString(sToken,line.c_str(),nCounter,';'))
				{
					tokenizeString(sToken,'$',sarrData);
					if (sarrData.GetCount() == 2)
					{
						sCap.Format(L"%s-",sarrData[0]);
						sInternal.Format(L"%s$%s$;",sarrData[0],sarrData[1]);

						pCol = pSelectedView->GetReportCtrl().AddColumn(new CXTPReportColumn(pSelectedView->GetReportCtrl().GetColumns()->GetCount(), sCap, sInternal, 80,FALSE));
						pCol->SetHeaderAlignment(DT_CENTER);
						pCol->SetAlignment(DT_CENTER);
						pCol->GetEditOptions()->m_bAllowEdit = TRUE;
						pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
	
					}	// if (sarrData.GetCount() == 2)
					nCounter++;
				}	// while (AfxExtractSubString(sToken,line.c_str(),nCounter,';'))
				pSelectedView->GetReportCtrl().Populate();
				pSelectedView->GetReportCtrl().UpdateWindow();

			}	// if (pSelectedView != NULL)
		}	// if (_tstoi(sToken) == FUNC_INDEX::COL_HEADER && nTabNumber > -1)
	}
}


// Add rows
void CUserCreateVolFunc::setupSpeciesTableRowData(LPCTSTR data)
{
	CXTPReportColumn *pCol = NULL;
	CStringArray sarrData;
	CSpecies rec = CSpecies();
	CString sData(data),sCap = L"";
	int nCounter = 1;
	int nTabNumber = -1;
	std::wstringstream ss;
	CString sToken = L"";
	std::wstring line;
	m_vecSpeciesSelected.clear();
	sData = m_recSelectedVolTable.getVolTable();
	ss << sData.GetBuffer();
	std::vector<double> vec;
	while (std::getline(ss,line))
	{
		AfxExtractSubString(sToken,line.c_str(),0,';');
		if (_tstoi(sToken) == FUNC_INDEX::SPECIES)
		{
			nTabNumber++;
		}
		// Add column headers
		if (_tstoi(sToken) == FUNC_INDEX::DATA && nTabNumber > -1)
		{
			CReportSpcView *pSelectedView = (CReportSpcView*)getReportSpcView(nTabNumber);
			if (pSelectedView != NULL)
			{
				pSelectedView->GetReportCtrl().BeginUpdate();
				while (AfxExtractSubString(sToken,line.c_str(),nCounter,';'))
				{
					if (nCounter == 1)
					{
						tokenizeString(sToken,'$',sarrData);
					}
					if (sarrData.GetCount() == 2 && nCounter == 1)
					{
						vec.push_back(_tstof(sarrData[0]));
					}
					else
					{
						vec.push_back(_tstof(sToken));
					}
					nCounter++;
				}	// while (AfxExtractSubString(sToken,line.c_str(),nCounter,';'))

				if (sarrData.GetCount() == 2)
				{
					pSelectedView->GetReportCtrl().AddRecord(new CUserVolumeTableRec(_tstoi(sarrData[0]),_tstoi(sarrData[1]),vec));
				}
				else
				{
					pSelectedView->GetReportCtrl().AddRecord(new CUserVolumeTableRec(0,0,vec));
				}
				pSelectedView->GetReportCtrl().Populate();
				pSelectedView->GetReportCtrl().UpdateWindow();
				pSelectedView->GetReportCtrl().EndUpdate();

	
				nCounter = 1;
				vec.clear();

			}	// if (pSelectedView != NULL)
		}	// if (_tstoi(sToken) == FUNC_INDEX::COL_HEADER && nTabNumber > -1)
	}
}

//*****************************************************************************************

void CUserCreateVolFunc::doPopulate(CUserVT& rec)
{
	if (m_vecUserVolTables.size() > 0)
	{
		for (UINT i = 0;i < m_vecUserVolTables.size();i++)
		{
			if (rec.getID() == m_vecUserVolTables[i].getID())
			{
				m_nSelectedIndex = i;
				break;
			}
		}
		populateData();
	}
}

void CUserCreateVolFunc::OnCbnSelchangeCombo71()
{
	CStringArray sarrMeasureMode;
	CString sCBoxText = L"";
	int nSelection1 = m_cbox7_1.GetCurSel();
	int nSelection2 = m_cbox7_2.GetCurSel();
	
	addFirstColumnCaption();
	
	if (nSelection1 != CB_ERR && nSelection2 != CB_ERR)
	{
		m_cbox7_1.GetWindowTextW(sCBoxText);
		if (sCBoxText.Right(1) != L",")
			sCBoxText += L",";
		// Set column and row measuretypes as caption
		tokenizeString(sCBoxText,',',sarrMeasureMode);
		if (sarrMeasureMode.GetCount() == 2)
		{
			m_lbl7_9.SetWindowTextW(sarrMeasureMode.GetAt(0));
			m_lbl7_10.SetWindowTextW(sarrMeasureMode.GetAt(1));
		}

		m_edit7_7.EnableWindow(TRUE);
		m_edit7_7.SetReadOnly(FALSE);
		m_edit7_8.EnableWindow(TRUE);
		m_edit7_8.SetReadOnly(FALSE);
		m_edit7_9.EnableWindow(TRUE);
		m_edit7_9.SetReadOnly(FALSE);
		m_edit7_10.EnableWindow(TRUE);
		m_edit7_10.SetReadOnly(FALSE);
		if (nSelection2 == 0)	// Inch
		{
			m_lbl7_15.SetWindowTextW(m_sFeet);
			m_lbl7_16.SetWindowTextW(m_sFeet);
			m_lbl7_17.SetWindowTextW(m_sInch);
			m_lbl7_18.SetWindowTextW(m_sInch);
		}
		else if (nSelection2 == 1)	// Metric 
		{
			m_lbl7_15.SetWindowTextW(m_sM);
			m_lbl7_16.SetWindowTextW(m_sM);
			m_lbl7_17.SetWindowTextW(m_sMM);
			m_lbl7_18.SetWindowTextW(m_sMM);
		}
	}
}

void CUserCreateVolFunc::OnCbnSelchangeCombo73()
{	
	CStringArray sarrMeasureMode;
	CString sCBoxText = L"";
	int nSelection1 = m_cbox7_1.GetCurSel();
	int nSelection2 = m_cbox7_2.GetCurSel();
/*
	if (m_bMeasuringModeSelected)
	{
		if (::MessageBox(GetSafeHwnd(),m_sMsgChangeMeasuringMode,m_sMsgCap,MB_ICONSTOP | MB_YESNO | MB_DEFBUTTON2) == IDNO)
		{
			m_cbox7_2.SetCurSel(m_nSelectedMeasureMode);		
			return;
		}
	}
*/
	if (nSelection1 != CB_ERR && nSelection2 != CB_ERR)
	{
		m_nSelectedMeasureMode = nSelection2;
		m_bMeasuringModeSelected = TRUE;

		m_cbox7_1.GetWindowTextW(sCBoxText);
		if (sCBoxText.Right(1) != L",")
			sCBoxText += L",";
		tokenizeString(sCBoxText,',',sarrMeasureMode);
		if (sarrMeasureMode.GetCount() == 2)
		{
			m_lbl7_9.SetWindowTextW(sarrMeasureMode.GetAt(0));
			m_lbl7_10.SetWindowTextW(sarrMeasureMode.GetAt(1));
		}

		removeColumns(CUserCreateVolFunc::DEL_ALL_COLUMNS);
		removeRows(CUserCreateVolFunc::DEL_ALL_ROWS);

		m_edit7_7.EnableWindow(TRUE);
		m_edit7_7.SetReadOnly(FALSE);
		m_edit7_8.EnableWindow(TRUE);
		m_edit7_8.SetReadOnly(FALSE);
		m_edit7_9.EnableWindow(TRUE);
		m_edit7_9.SetReadOnly(FALSE);
		m_edit7_10.EnableWindow(TRUE);
		m_edit7_10.SetReadOnly(FALSE);

		m_edit7_7.SetWindowTextW(L"");
		m_edit7_8.SetWindowTextW(L"");
		m_edit7_9.SetWindowTextW(L"");
		m_edit7_10.SetWindowTextW(L"");

		if (nSelection2 == 0)	// Inch
		{
			m_lbl7_15.SetWindowTextW(m_sFeet);
			m_lbl7_16.SetWindowTextW(m_sFeet);
			m_lbl7_17.SetWindowTextW(m_sInch);
			m_lbl7_18.SetWindowTextW(m_sInch);	
		}
		else if (nSelection2 == 1)	// Metric 
		{
			m_lbl7_15.SetWindowTextW(m_sM);
			m_lbl7_16.SetWindowTextW(m_sM);
			m_lbl7_17.SetWindowTextW(m_sMM);
			m_lbl7_18.SetWindowTextW(m_sMM);
		}
	}
}

// L�gga till kolumner
void CUserCreateVolFunc::OnBnClickedButton71()
{
	addColumnToTab();
}

// Ta bort kolumn; senast inl�sta
void CUserCreateVolFunc::OnBnClickedButton72()
{
	removeColumns(CUserCreateVolFunc::DEL_LAST_COLUMN);
}

// L�gg till rader
void CUserCreateVolFunc::OnBnClickedButton73()
{
	addRowsToTab();
}

// Ta bort sista inl�sta raden
void CUserCreateVolFunc::OnBnClickedButton74()
{
	removeRows(CUserCreateVolFunc::DEL_LAST_ROW);

	// get info on Diam and step
	CReportSpcView *pSelectedView = (CReportSpcView*)getReportSpcView();
	CUserVolumeTableRec *pRec = (CUserVolumeTableRec*)pSelectedView->GetReportCtrl().GetRecords()->GetAt(pSelectedView->GetReportCtrl().GetRows()->GetCount()-1);
	if (pRec != NULL)
	{
		m_edit7_9.setFloat(pRec->getDiam(),0);
		m_edit7_10.setFloat(pRec->getStep(),0);
	}
	else
	{
		m_edit7_9.SetWindowText(L"");
		m_edit7_10.SetWindowText(L"");
	}

}

CReportSpcView *CUserCreateVolFunc::getReportSpcView(void)
{
	CXTPTabManagerItem *pManager = m_wndTabControl.getSelectedTabPage();
	if (pManager)
	{
		CReportSpcView* pView = DYNAMIC_DOWNCAST(CReportSpcView, CWnd::FromHandle(pManager->GetHandle()));
		ASSERT_KINDOF(CReportSpcView, pView);
		return pView;
	}
	return NULL;
}

CReportSpcView *CUserCreateVolFunc::getReportSpcView(int idx)
{
	if (idx >= 0 && idx < m_wndTabControl.getNumOfTabPages())
	{
		CXTPTabManagerItem *pManager = m_wndTabControl.getTabPage(idx);
		if (pManager)
		{
			CReportSpcView* pView = DYNAMIC_DOWNCAST(CReportSpcView, CWnd::FromHandle(pManager->GetHandle()));
			ASSERT_KINDOF(CReportSpcView, pView);
			return pView;
		}
	}
	return NULL;
}


BOOL CUserCreateVolFunc::isAlreadyOnTab(LPCTSTR spc)
{
	if (m_wndTabControl.getNumOfTabPages() > 0)
	{
		for (int i = 0;i < m_wndTabControl.getNumOfTabPages();i++)
		{
			CXTPTabManagerItem *pManagerItem =	m_wndTabControl.getTabPage(i);
			if (pManagerItem != NULL)
			{
				if (pManagerItem->GetCaption().CompareNoCase(spc) == 0)
					return TRUE;
			}
		}
	}
	return FALSE;

}

BOOL CUserCreateVolFunc::AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon,int spc_id)
{
	CCreateContext contextT;
	contextT.m_pCurrentDoc     = GetDocument();
	contextT.m_pNewViewClass   = pViewClass;
	contextT.m_pNewDocTemplate = GetDocument()->GetDocTemplate();

	CWnd* pWnd;
	TRY
	{
		pWnd = (CWnd*)pViewClass->CreateObject();
		if (pWnd == NULL)
		{
			AfxThrowMemoryException();
		}
	}
	CATCH_ALL(e)
	{
		TRACE0( "Out of memory creating a view.\n" );
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;

	int nTab = m_wndTabControl.GetItemCount();

	// Create with the right size (wrong position)
	CRect rect(0,0,0,0);
	if (!pWnd->Create(NULL, NULL, dwStyle,rect, &m_wndTabControl, (AFX_IDW_PANE_FIRST + nTab), &contextT))
	{
		TRACE0( "Warning: couldn't create client tab for view.\n" );
		// pWnd will be cleaned up by PostNcDestroy
		return NULL;
	}
	m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);
	m_mapSpcID[nTab] = spc_id;

	pWnd->SetOwner(this);

	return TRUE;
}


// CReportSpcView

IMPLEMENT_DYNCREATE(CReportSpcView,  CXTPReportView)

BEGIN_MESSAGE_MAP(CReportSpcView, CXTPReportView)
	ON_COMMAND_RANGE(ID_BUTTON32799,ID_BUTTON32806, OnSave)
	ON_COMMAND_RANGE(ID_TOOLS_IMPORT_EXCEL,ID_TOOLS_EXPORT_EXCEL, OnTools)
END_MESSAGE_MAP()


CReportSpcView::CReportSpcView()
{
	CXTPReportColumn *pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(0,L"",90,FALSE));
	pCol->AllowRemove(FALSE);
	pCol->SetHeaderAlignment(DT_LEFT);
	pCol->SetAlignment(DT_LEFT);
	pCol->GetEditOptions()->m_bAllowEdit = FALSE;

	GetReportCtrl().GetReportHeader()->AllowColumnRemove(FALSE);
	GetReportCtrl().GetReportHeader()->SetAutoColumnSizing( FALSE );
	GetReportCtrl().SetMultipleSelection( FALSE );
	GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );
	GetReportCtrl().SetGridStyle( FALSE, xtpReportGridSmallDots );
	GetReportCtrl().FocusSubItems(TRUE);
	GetReportCtrl().AllowEdit(TRUE);
	GetReportCtrl().GetPaintManager()->SetFixedRowHeight(FALSE);

}

CReportSpcView::~CReportSpcView()
{
}

void CReportSpcView::OnSave(UINT nID)	
{ 
	if (nID != ID_BUTTON32804)
	{
		CUserCreateVolFunc *pView = (CUserCreateVolFunc*)getFormViewByID(IDD_FORMVIEW7);
		if (pView != NULL)
		{
			::SendMessage(pView->GetSafeHwnd(),WM_COMMAND,nID,0);
		}
	}
}

void CReportSpcView::OnTools(UINT nID)	
{ 
	CUserCreateVolFunc *pView = (CUserCreateVolFunc*)getFormViewByID(IDD_FORMVIEW7);
	if (pView != NULL)
	{
		::SendMessage(pView->GetSafeHwnd(),WM_COMMAND,nID,0);
	}
}
