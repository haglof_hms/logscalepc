#pragma once

#include "Resource.h"

#include "DatePickerCombo.h"

#include "DBHandling.h"
#include "afxdtctl.h"

// CTicketDlg dialog

class CTicketDlg : public CXTResizeDialog
{
	DECLARE_DYNAMIC(CTicketDlg)

	// Data members
	BOOL m_bInitialized;
	CString m_sLangFN;
	CString m_sMsgCap;
	CString m_sMsgDataMissing;

	CMyExtStatic m_lbl8_1;
	CMyExtStatic m_lbl8_2;
	CMyExtStatic m_lbl8_3;
	CMyExtStatic m_lbl8_4;
	CMyExtStatic m_lbl8_5;
	CMyExtStatic m_lbl8_6;
	CMyExtStatic m_lbl8_7;
	CMyExtStatic m_lbl8_8;
	CMyExtStatic m_lbl8_9;
	CMyExtStatic m_lbl8_10;
	CMyExtStatic m_lbl8_11;
	CMyExtStatic m_lbl8_12;
	CMyExtStatic m_lbl8_13;
	CMyExtStatic m_lbl8_14;
	CMyExtStatic m_lbl8_15;

	//CMyDatePickerCombo m_cbx8_1;
	CComboBox m_cbx8_2;
	CComboBox m_cbx8_3;
	CComboBox m_cbx8_4;
	CComboBox m_cbx8_5;
	CComboBox m_cbx8_6;
	CComboBox m_cbx8_7;
	CComboBox m_cbx8_8;
	CComboBox m_cbx8_9;

	CMyExtEdit m_ed8_1;
	CMyExtEdit m_ed8_2;
	CMyExtEdit m_ed8_3;
	CMyExtEdit m_ed8_4;
	CMyExtEdit m_ed8_5;
	CMyExtEdit m_ed8_6;

	CButton m_btnOK;
	CButton m_btnCancel;

	CVecRegister m_vecRegister;

	CStringArray m_sarrLoadType;
	CStringArray m_sarrDeductionType;

	CTickets m_recTicket;

	// Methods
	void setCBoxdata();
	void setData();

public:
	CTicketDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTicketDlg();

	inline void setRegister(CVecRegister& vec)	{ m_vecRegister = vec; }
	inline void setTicket(CTickets& rec) { m_recTicket = rec; }
	inline CTickets& getTicket()	{ return m_recTicket; }

// Dialog Data
	enum { IDD = IDD_DIALOG8 };

protected:
	//{{AFX_VIRTUAL(CTicketDlg)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	//}}AFX_VIRTUAL

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	CDateTimeCtrl m_dtDate;
	CString m_csDate;
};
