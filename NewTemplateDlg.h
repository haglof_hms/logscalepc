#if !defined(__NEWTEMPLATEDLG_H__)
#define __NEWTEMPLATEDLG_H__


#pragma once

#include "Resource.h"

/////////////////////////////////////////////////////////////////////////////////////
// Select pricelist and enter name of template

// CNewTemplateDlg dialog

class CNewTemplateDlg : public CDialog
{
	DECLARE_DYNAMIC(CNewTemplateDlg)
	BOOL m_bInitialized;
	CString m_sLangFN;

	CString m_sName;
	CString m_sPrl;
	CString m_sPrlName;
	int m_nPrlID;

	CString m_sTemplateName;
	CString m_sPricelistName;

	CButton m_wndOKBtn;
	CButton m_wndCancelBtn;
	CMyExtStatic m_lbldlg9_1;
	CMyExtStatic m_lbldlg9_2;

	CListBox m_lbdlg9_1;
	
	CMyExtEdit m_editdlg9_1;

	CPricelist m_recPricelist;
	CVecPricelist m_vecVecPricelist;

	int m_nSelectedIndex;

	int m_nOpenAs;	// 0 = New template,1 = Change/Update pricelist
public:
	CNewTemplateDlg(CWnd* pParent = NULL,int open_as = 0);   // standard constructor
	virtual ~CNewTemplateDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG9 };

	void setData(CVecPricelist& vec);
	CString getName()	{ return m_sName; }
	CString getPrl()	{ return m_sPrl; }
	CString getPrlName()	{ return m_sPrlName; }
	int getPrlID()	{ return m_nPrlID; }
	BOOL isSelected();
	void setTemplateName(LPCTSTR name = L"") { m_sTemplateName = name; }
	void setNameOfSelectedPricelist(LPCTSTR prl = L"") { m_sPricelistName = prl; }
protected:
	//{{AFX_VIRTUAL(CSelectCalcTypeDlg)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	virtual BOOL OnInitDialog();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnLbnSelchangeListdlg91();
	afx_msg void OnEnChangeEditdlg91();
	afx_msg void OnBnClickedOk();
};

#endif