// ExcelExportDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ExcelExportDlg.h"

#include "ResLangFileReader.h"

// CExcelExportDlg dialog

IMPLEMENT_DYNAMIC(CExcelExportDlg, CXTResizeDialog)

BEGIN_MESSAGE_MAP(CExcelExportDlg, CXTResizeDialog)
	ON_BN_CLICKED(IDC_BUTTON10_3, &CExcelExportDlg::OnBnClickedButton103)
	ON_BN_CLICKED(IDC_BUTTON10_4, &CExcelExportDlg::OnBnClickedButton104)
	ON_BN_CLICKED(IDC_BUTTON10_1, &CExcelExportDlg::OnBnClickedButton101)
	ON_BN_CLICKED(IDC_BUTTON10_2, &CExcelExportDlg::OnBnClickedButton102)
	ON_BN_CLICKED(IDC_BUTTON10_5, &CExcelExportDlg::OnBnClickedButton105)
	ON_BN_CLICKED(IDOK, &CExcelExportDlg::OnBnClickedOk)
END_MESSAGE_MAP()

CExcelExportDlg::CExcelExportDlg(CWnd* pParent /*=NULL*/)
	: CXTResizeDialog(CExcelExportDlg::IDD, pParent)
{
	m_bInitialized = FALSE;
}

CExcelExportDlg::~CExcelExportDlg()
{
}

BOOL CExcelExportDlg::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeDialog::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CExcelExportDlg::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_LBL10_1, m_lbl10_1);
	DDX_Control(pDX, IDC_BUTTON10_1, m_btnSelectAll);
	DDX_Control(pDX, IDC_BUTTON10_2, m_btnDeSelectAll);
	DDX_Control(pDX, IDC_BUTTON10_3, m_btnPrevPage);
	DDX_Control(pDX, IDC_BUTTON10_4, m_btnNextPage);
	DDX_Control(pDX, IDC_BUTTON10_5, m_btnReset);
	DDX_Control(pDX, IDOK, m_btnOK);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDC_LIST10_1, m_list10_1);
	//}}AFX_DATA_MAP

}

BOOL CExcelExportDlg::OnInitDialog()
{
	CXTResizeDialog::OnInitDialog();
	if (!m_bInitialized)
	{
		m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		m_lbl10_1.SetLblFontEx(14,FW_NORMAL);
		m_lbl10_1.SetBkColor(INFOBK);

		//m_btnPrevPage.EnableWindow(FALSE);
		//m_btnOK.EnableWindow(FALSE);
		
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{

			SetWindowText(xml.str(IDS_STRING4000));

			m_lbl10_1.SetWindowText(xml.str(IDS_STRING4001));
			m_btnSelectAll.SetWindowText(xml.str(IDS_STRING4002));
			m_btnDeSelectAll.SetWindowText(xml.str(IDS_STRING4003));
//			m_btnPrevPage = xml.str(IDS_STRING4000);
//			m_btnNextPage = xml.str(IDS_STRING4000);
			m_btnReset.SetWindowText(xml.str(IDS_STRING4004));
			m_btnReset.ShowWindow(SW_HIDE);
			m_btnOK.SetWindowText(xml.str(IDS_STRING100));
			m_btnCancel.SetWindowText(xml.str(IDS_STRING101));

			m_list10_1.SetExtendedStyle(m_list10_1.GetStyle()|LVS_EX_CHECKBOXES|LVS_EX_GRIDLINES);
			m_list10_1.InsertColumn(0,xml.str(IDS_STRING4005),LVCFMT_LEFT,30);
			m_list10_1.InsertColumn(1,xml.str(IDS_STRING4006),LVCFMT_LEFT,220);

			m_sMsgCap = xml.str(IDS_STRING99);
			m_sMsgNoColumnsSelected = xml.str(IDS_STRING4007);


			OnBnClickedButton105();

			xml.clean();
		}

		m_bInitialized = TRUE;
	}

	return TRUE;
}

// CExcelExportDlg message handlers

// Prev. Page; not used for now
void CExcelExportDlg::OnBnClickedButton103()
{
	m_btnNextPage.EnableWindow(TRUE);
	m_btnPrevPage.EnableWindow(FALSE);
	m_btnOK.EnableWindow(FALSE);
}
// Next page; not used for now
void CExcelExportDlg::OnBnClickedButton104()
{
	m_btnNextPage.EnableWindow(FALSE);
	m_btnPrevPage.EnableWindow(TRUE);
	m_btnOK.EnableWindow(TRUE);
}
// Markera alla
void CExcelExportDlg::OnBnClickedButton101()
{
	if (m_list10_1.GetItemCount() > 0)
	{
		for (int i = 0;i < m_list10_1.GetItemCount();i++)
		{
			m_list10_1.SetCheck(i);
		}
	}
}
// AvMarkera alla
void CExcelExportDlg::OnBnClickedButton102()
{
	if (m_list10_1.GetItemCount() > 0)
	{
		for (int i = 0;i < m_list10_1.GetItemCount();i++)
		{
			m_list10_1.SetCheck(i,false);
		}
	}
}

// �terst�ll
void CExcelExportDlg::OnBnClickedButton105()
{
	int nRow = 0;
	if (m_list10_1.GetItemCount() > 0)
		m_list10_1.DeleteAllItems();
	if (m_vecColumnsSet.size() > 0)
	{
		for (UINT i = 0;i < m_vecColumnsSet.size();i++)
		{
			ColumnsSet rec = m_vecColumnsSet[i];			
			// Add information to List
			if (rec.getSet() == 1)
			{
				InsertRow(m_list10_1,nRow,true,2,L"",rec.getColName());
				
				m_list10_1.SetItemData(nRow,(DWORD)rec.getIndex());
				nRow++;
			}
		}
	}
}

void CExcelExportDlg::OnBnClickedOk()
{
	int nIndex = 0;
	if (m_list10_1.GetItemCount() > 0)
	{
		for (int i = 0;i < m_list10_1.GetItemCount();i++)
		{
			if (m_list10_1.GetCheck(i))
			{
				nIndex = (int)m_list10_1.GetItemData(i);
				if (nIndex >= 0 && nIndex < m_vecColumnsSet.size())
				{
					m_vecColumnsSelected.push_back(m_vecColumnsSet[nIndex]);
				}	// if (nIndex >= 0 && nIndex < m_vecColumnsSet.size())
			}	// if (m_list10_1.GetCheck(i))
		}	// for (int i = 0;i < m_list10_1.GetItemCount();i++)
	}	// if (m_list10_1.GetItemCount() > 0)

	if (m_vecColumnsSelected.size() == 0)
	{
		::MessageBox(GetSafeHwnd(),m_sMsgNoColumnsSelected,m_sMsgCap,MB_ICONSTOP | MB_OK);
	}
	else
		OnOK();
}
