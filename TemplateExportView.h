// CTemplateExportView form view

#if !defined(__TEMPLATEEXPORTVIEW_H__)
#define __TEMPLATEEXPORTVIEW_H__

#pragma once

#include "stdafx.h"

#include "dbhandling.h"

#include "Resource.h"

///////////////////////////////////////////////////////////////////////////////////////////
// CTemplateExportDoc

class CTemplateExportDoc : public CDocument
{
protected: // create from serialization only
	CTemplateExportDoc();
	DECLARE_DYNCREATE(CTemplateExportDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTemplateExportDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTemplateExportDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CTemplateExportDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
/////////////////////////////////////////////////////////////////////////////
// class CTemplateExportFrame

class CTemplateExportFrame : public CChildFrameBase
{
//private:

	DECLARE_DYNCREATE(CTemplateExportFrame)
	CXTPToolBar m_wndToolBar;
	CXTPDockingPaneManager m_paneManager;
	CString m_sLangFN;

	BOOL m_bFirstOpen;
	BOOL m_bInitReports;

	BOOL m_bIsSysCommand;

	void cleanUpLogScaleTmplDir();

protected:
	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	HICON m_hIcon;
public:
	CTemplateExportFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

// Attributes

// Operations

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	virtual ~CTemplateExportFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	
	//{{AFX_MSG(CTemplateExportFrame)
	afx_msg int OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl);
	afx_msg void OnSysCommand(UINT nID,LPARAM lParam);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnClose();
	afx_msg void OnDestroy(void);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);

	//}}AFX_MSG
	
	DECLARE_MESSAGE_MAP()
};


// CTemplateExportView form view

class CTemplateExportView : public CXTResizeFormView //CXTResizeFormView
{
	DECLARE_DYNCREATE(CTemplateExportView)

	// Data members
	BOOL m_bInitialized;
	CString m_sLangFN;

	CString m_sMsgCap;
	CString m_sMsgCaliperAlert;
	CString m_sMsgUnableToCreateTmplFile;
	CString m_sMsgDoneSavingToDisk;
	CString m_sMsgDoneUploadToCaliper;

	CListCtrl m_list23_1;

	CDBHandling *m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;

	CVecRegister m_vecRegister;

	CLogScaleTemplates m_recSelectedTemplate;
	vecLogScaleTemplates m_vecLogScaleTemplates;

	vecFuncDesc m_vecFuncDesc;
	CVecUserVolTables m_vecUserVolTables;

	CDefaults m_recDefaults;
	CVecDefaults m_vecDefaults;

	CGrades m_recGrades;
	CVecGrades m_vecGrades;

	CSpecies m_recSpecies;
	CVecSpecies m_vecSpecies;

	CStringArray m_sarrDeductionType;
	CStringArray m_sarrLoadType;

	BOOL m_bEnableToolBarBtn;

	BOOL m_bChangeIndex;

	CString m_sDirectory;

	// Methods
	void setupReport();
	void populateReport();

	CLogScaleTemplates getLogScaleTemplate(int id);
	BOOL createTemplate(LPCTSTR file_name);

	void saveTemplateToDisk();
	void sendTemplatesToCaliper();
	void selectAll();
	void deSelectAll();

	CGrades getGradesData(int grades_id);
	CSpecies getSpeciesData(int spc_id);

public:
	CTemplateExportView();           // protected constructor used by dynamic creation
	virtual ~CTemplateExportView();

public:
	enum { IDD = IDD_FORMVIEW23 };

#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	//{{AFX_VIRTUAL(CTemplateExportView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CTemplateExportView)
	afx_msg void OnDestroy(void);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnCommand(UINT nID);
	afx_msg void OnUpdateToolbar(CCmdUI* pCmdUI);
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);
	afx_msg void OnItemchangedList(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

#endif