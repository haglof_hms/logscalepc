// CTabSpcGradesView form view

#if !defined(__TABSPCGRADESFORMVIEW_H__)
#define __TABSPCGRADESFORMVIEW_H__

#include "DerivedClasses.h"

#include "ReportClasses.h"

#include "DBHandling.h"

#include "speciesformview.h"
#include "gradesformview.h"

#pragma once

///////////////////////////////////////////////////////////////////////////////////////////
// CTabSpcGradesDoc

class CTabSpcGradesDoc : public CDocument
{
protected: // create from serialization only
	CTabSpcGradesDoc();
	DECLARE_DYNCREATE(CTabSpcGradesDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTabSpcGradesDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTabSpcGradesDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CTabSpcGradesDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
/////////////////////////////////////////////////////////////////////////////
// class CTabSpcGradesFrame

class CTabSpcGradesFrame : public CChildFrameBase
{
//private:

	DECLARE_DYNCREATE(CTabSpcGradesFrame)
	CXTPStatusBar m_wndStatusBar;
	CXTPToolBar m_wndToolBar;
	CXTPDockingPaneManager m_paneManager;

	BOOL m_bFirstOpen;

	BOOL m_bEnableToolBar;

protected:
	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	HICON m_hIcon;
public:
	CTabSpcGradesFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

	void toolbarEnableDisable(BOOL enable);

// Attributes

// Operations

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	virtual ~CTabSpcGradesFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif


// Generated message map functions
protected:
	
	//{{AFX_MSG(CTabSpcGradesFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnClose();
	afx_msg void OnDestroy(void);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );

	afx_msg void OnUpdateToolbarBtn(CCmdUI* pCmdUI);

	afx_msg void OnAdd();
	afx_msg void OnDelete();
	afx_msg void OnSave();

	//}}AFX_MSG
	
	DECLARE_MESSAGE_MAP()
};


class CTabSpcGradesView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CTabSpcGradesView)
	// Data members
	BOOL m_bInitialized;
	CString m_sLangFN;
	CXTPDockingPaneManager m_paneManager;
	BOOL m_bFirstOpen;
	CFont m_fntTab;

	CDBHandling *m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;


	CMyTabControl m_wndTabControl;

	CXTPTabManagerItem *m_tabManager;
	// Methods
	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon);
protected:
	CTabSpcGradesView();           // protected constructor used by dynamic creation
	virtual ~CTabSpcGradesView();

public:
	enum { IDD = IDD_FORMVIEW1 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

	int getSelectedTabPageIndex(void);

	CSpeciesReportView* getSpeciesPage(void);
	CGradesReportView* getGradesPage(void);
//	CGradesperSpcView* getGradesPerSpeciesPage(void);

	void addSpecies();
	void deleteSpecies();
	void saveSpecies();

	void addGrades();
	void deleteGrades();
	void saveGrades();

	void saveGradesPerSpecies();

protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTabSpcGradesView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL
	//{{AFX_MSG(CTabSpcGradesView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnClose();
	afx_msg void OnDestroy(void);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg LRESULT OnSuiteMessge(WPARAM wParam, LPARAM lParam);
	afx_msg void OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};


#endif