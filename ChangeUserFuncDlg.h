#if !defined(__CHANGEUSERFUNCDLG_H__)
#define __CHANGEUSERFUNCDLG_H__
#pragma once

#include "Resource.h"

// CChangeUserFuncDlg dialog

class CChangeUserFuncDlg : public CXTResizeDialog
{
	DECLARE_DYNAMIC(CChangeUserFuncDlg)

	BOOL m_bInitialized;
	CString m_sLangFN;

	CMyExtStatic m_lbl5_1;
	CXTListCtrl m_list5_1;
	CButton m_btnOK;
	CButton m_btnCancel;

	int m_nTicketID;

	vecFuncDesc m_vecFuncDesc;
	vecSelectedVolFuncs m_vecSelectedVolFuncs;
	vecSelectedVolFuncs m_vecReturnSelectedVolFuncs;

	BOOL isActive(int func_id);

	void getSelectedFunctionData(int func_id);

public:
	CChangeUserFuncDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CChangeUserFuncDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG5 };

	void setData(int ticket_id,vecFuncDesc& v1,vecSelectedVolFuncs& v2)
	{
		m_nTicketID = ticket_id;
		m_vecFuncDesc = v1;
		m_vecSelectedVolFuncs = v2;
	}
	
	vecSelectedVolFuncs selectedFunctions()	{ return m_vecReturnSelectedVolFuncs; }

protected:
	//{{AFX_VIRTUAL(CRegisterDlg)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
 	//}}AFX_VIRTUAL

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};


#endif