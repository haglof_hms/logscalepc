f// LogScaleTemplatesFormView.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "LogScaleTemplatesFormView.h"

#include "SelectSpeciesDlg.h"
#include "selectcalctypedlg.h"
#include "NewTemplateDlg.h"
#include "MatchPrlGrdDlg.h"

#include "ReportClasses.h"

#include "ResLangFileReader.h"

#include "LogScaleContractParser.h"

#include <algorithm>

#include <sstream>


BEGIN_MESSAGE_MAP(CMyExtEditEx, CMyExtEdit)
	//{{AFX_MSG_MAP(CMyExtEditEx)
	ON_CONTROL_REFLECT_EX(EN_CHANGE,OnEnChange)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


BOOL CMyExtEditEx::OnEnChange()
{
	return !m_bNotify;
}



///////////////////////////////////////////////////////////////////////////////////////////
// CLogScaleTemplatesDoc


IMPLEMENT_DYNCREATE(CLogScaleTemplatesDoc, CDocument)

BEGIN_MESSAGE_MAP(CLogScaleTemplatesDoc, CDocument)
	//{{AFX_MSG_MAP(CLogScaleTemplatesDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLogScaleTemplatesDoc construction/destruction

CLogScaleTemplatesDoc::CLogScaleTemplatesDoc()
{
	// TODO: add one-time construction code here

}

CLogScaleTemplatesDoc::~CLogScaleTemplatesDoc()
{
}


BOOL CLogScaleTemplatesDoc::OnNewDocument()
{
	// CHECK FOR LICENSE HERE!!!!! 2011-12-22 P�D
	if (!License())
	{
		return FALSE;
	}

	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CLogScaleTemplatesDoc serialization

void CLogScaleTemplatesDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CLogScaleTemplatesDoc diagnostics

#ifdef _DEBUG
void CLogScaleTemplatesDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CLogScaleTemplatesDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

///////////////////////////////////////////////////////////////////////////////////////////
// CLogScaleTemplatesFrame

IMPLEMENT_DYNCREATE(CLogScaleTemplatesFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CLogScaleTemplatesFrame, CMDIChildWnd)
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_WM_CLOSE()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_WM_MDIACTIVATE()
	ON_WM_SHOWWINDOW()
	ON_WM_SETFOCUS()
	ON_WM_SYSCOMMAND()
	//ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)

	ON_XTP_CREATECONTROL()
END_MESSAGE_MAP()


// CLogScaleTemplatesFrame construction/destruction

XTPDockingPanePaintTheme CLogScaleTemplatesFrame::m_themeCurrent = xtpPaneThemeOffice2003;

CLogScaleTemplatesFrame::CLogScaleTemplatesFrame()
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_FORMVIEW);
	m_bInitReports = FALSE;
	m_bIsPrintOutTBtn = FALSE;
	m_bIsSysCommand = FALSE;
}

CLogScaleTemplatesFrame::~CLogScaleTemplatesFrame()
{
}

void CLogScaleTemplatesFrame::OnDestroy(void)
{
}

void CLogScaleTemplatesFrame::OnClose(void)
{

	// save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6501_KEY);
	SavePlacement(this, csBuf);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

	if (!m_bIsSysCommand)
	{
		CLogScaleTemplatesFormView *pView = (CLogScaleTemplatesFormView*)getFormViewByID(IDD_FORMVIEW8);
		if (pView != NULL)
		{
			QUIT_TYPES::Q_T_RETURN qtt;
			qtt = pView->checkDataBeforeSave(CHECK_SAVE_TYPES::CHECK_SAVE_ON_QUIT);
			if (qtt == QUIT_TYPES::DO_SAVE)
			{
				pView->doSave();
				setNavBarButtons();
				CMDIChildWnd::OnClose();
			}
			else if (qtt == QUIT_TYPES::QUIT_ANYWAY)
			{
				setNavBarButtons();
				CMDIChildWnd::OnClose();
			}
		}
	}
	else
	{
		setNavBarButtons();
		CMDIChildWnd::OnClose();
	}

}

void CLogScaleTemplatesFrame::OnSysCommand(UINT nID,LPARAM lParam)
{
	if ((nID & 0xFFF0) == SC_CLOSE)
	{
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);

		CLogScaleTemplatesFormView *pView = (CLogScaleTemplatesFormView*)getFormViewByID(IDD_FORMVIEW8);
		if (pView != NULL)
		{
			QUIT_TYPES::Q_T_RETURN qtt;
			qtt = pView->checkDataBeforeSave(CHECK_SAVE_TYPES::CHECK_SAVE_ON_QUIT);
			if (qtt == QUIT_TYPES::DO_SAVE)
			{
				m_bIsSysCommand = TRUE;

				pView->doSave();

				CMDIChildWnd::OnSysCommand(nID,lParam);
			}
			else if (qtt == QUIT_TYPES::QUIT_ANYWAY)
			{
				CMDIChildWnd::OnSysCommand(nID,lParam);
			}
		}
	}
	else if ((nID & 0xFFF0) != SC_CLOSE)
	{
		CMDIChildWnd::OnSysCommand(nID,lParam);
	}
}

int CLogScaleTemplatesFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	EnableDocking(CBRS_ALIGN_ANY);

	// Create and Load toolbar; 090107 p�d
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR7);
	m_wndToolBar.EnableDocking(xtpFlagAlignTop|xtpFlagStretched);

	HICON hIcon = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getToolBarResourceFN();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{
			//***************************************************************************************
			// Setup commandbars and manues; 051114 p�d
			CXTPToolBar* pToolBar = &m_wndToolBar;
			if (pToolBar->IsBuiltIn())
			{
				if (pToolBar->GetType() != xtpBarTypeMenuBar)
				{
					UINT nBarID = pToolBar->GetBarID();
					pToolBar->LoadToolBar(nBarID, FALSE);
					CXTPControls *p = pToolBar->GetControls();
					// Setup icons on toolbars, using resource dll; 051208 p�d
					if (nBarID == IDR_TOOLBAR7)
					{		
						setToolbarBtn(sTBResFN,p->GetAt(0),RES_TB_NEW,xml.str(IDS_STRING44550),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(1),RES_TB_SAVE,xml.str(IDS_STRING44551),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(2),RES_TB_DEL,xml.str(IDS_STRING44552),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(3),RES_TB_TOOLS,xml.str(IDS_STRING118),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(4),RES_TB_IMPORT,xml.str(IDS_STRING44554),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(5),RES_TB_EXPORT,xml.str(IDS_STRING44555),TRUE);	//
						setToolbarBtn(sTBResFN,p->GetAt(6),RES_TB_PREVIEW,xml.str(IDS_STRING44556),TRUE);	//

						//p->GetAt(4)->SetVisible(FALSE);
						//p->GetAt(5)->SetVisible(FALSE);
						p->GetAt(6)->SetVisible(FALSE);
						p->GetAt(7)->SetVisible(FALSE);

					}	// if (nBarID == IDR_TOOLBAR5)
				}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
			}	// if (pToolBar->IsBuiltIn())
			//***************************************************************************************
			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))

	m_bFirstOpen = TRUE;

	return 0; // creation ok
}

int CLogScaleTemplatesFrame::OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl)
{
	if (lpCreateControl->nID == ID_BUTTON32810)
	{
		CMyControlPopup *m_pToolsPopup = new CMyControlPopup();
		m_pToolsPopup->SetStyle(xtpButtonIcon);
		if (fileExists(m_sLangFN))
		{
			RLFReader xml; // = new RLFReader;
			if (xml.Load(m_sLangFN))
			{
				m_pToolsPopup->addMenuIDAndText(ID_TMPL_TOOLS_UPD_PRL,xml.str(IDS_STRING44553));
				m_pToolsPopup->addMenuIDAndText();	// Add separator
				m_pToolsPopup->addMenuIDAndText(ID_TMPL_TOOLS_EXPORT_TMPL,xml.str(IDS_STRING44557));
			}	// if (xml->Load(m_sLangFN))
			xml.clean();
		}

		lpCreateControl->pControl = m_pToolsPopup;
		return TRUE;
	}

	return TRUE;
}


LRESULT CLogScaleTemplatesFrame::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	return 0L;
}

BOOL CLogScaleTemplatesFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CLogScaleTemplatesFrame::OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd)
{
	CMDIChildWnd::OnMDIActivate( bActivate,pActivateWnd,pDeactivateWnd);
  
	::SendMessage(GetMDIFrame()->m_hWndMDIClient,WM_MDISETMENU,0, 0);
  
	if(!bActivate)
		RedrawWindow(NULL, NULL, RDW_INVALIDATE | RDW_FRAME | RDW_NOCHILDREN);
}

void CLogScaleTemplatesFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CXTPFrameWndBase<CMDIChildWnd>::OnShowWindow(bShow, nStatus);

	if(bShow && !IsWindowVisible() && m_bFirstOpen)
  {
		m_bFirstOpen = FALSE;

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT,REG_LOGSCALE_6501_KEY);
		LoadPlacement(this, csBuf);
  }
}

void CLogScaleTemplatesFrame::OnSetFocus(CWnd* pWnd)
{
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,ID_DO_SOMETHING_IN_SHELL,ID_LPARAM_COMMAND2);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);

	CLogScaleTemplatesFormView *pView = (CLogScaleTemplatesFormView*)getFormViewByID(IDD_FORMVIEW8);
	if (pView != NULL)
	{
		pView->doSetNavigationButtons();
	}


	CMDIChildWnd::OnSetFocus(pWnd);
}

// Recieve message (WM_USER_MSG_SUITE) from HMSShell toolbar button click or menu item 
// selected etc.; 060215 p�d
LRESULT CLogScaleTemplatesFrame::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	CLogScaleTemplatesFormView *pView = (CLogScaleTemplatesFormView*)getFormViewByID(IDD_FORMVIEW8);
	if (pView != NULL)
	{
		::SendMessage(pView->GetSafeHwnd(),WM_USER_MSG_SUITE,wParam,lParam);
	}

	return 0L;
}


// CLogScaleTemplatesFrame diagnostics

#ifdef _DEBUG
void CLogScaleTemplatesFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CLogScaleTemplatesFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

void CLogScaleTemplatesFrame::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMinTrackSize.x = MIN_X_SIZE;
	lpMMI->ptMinTrackSize.y = MIN_Y_SIZE;

	CMDIChildWnd::OnGetMinMaxInfo(lpMMI);
}

void CLogScaleTemplatesFrame::OnSize(UINT nType,int cx,int cy)
{
	CMDIChildWnd::OnSize(nType,cx,cy);
}



// CLogScaleTemplatesFormView

IMPLEMENT_DYNCREATE(CLogScaleTemplatesFormView, CXTResizeFormView)

BEGIN_MESSAGE_MAP(CLogScaleTemplatesFormView, CXTResizeFormView)
	ON_WM_SIZE()
	ON_WM_CREATE()
	ON_WM_COPYDATA()

	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)

	ON_COMMAND_RANGE(ID_BUTTON32807,ID_BUTTON32814, OnCommand)
	ON_COMMAND_RANGE(ID_TMPL_TOOLS_UPD_PRL,ID_TMPL_TOOLS_EXPORT_TMPL, OnCommand2)

	ON_UPDATE_COMMAND_UI_RANGE(ID_BUTTON32807,ID_BUTTON32814, OnUpdateToolbar)

	ON_UPDATE_COMMAND_UI(ID_TMPL_TOOLS_UPD_PRL, OnUpdateToolbarAddPrl)
	ON_UPDATE_COMMAND_UI(ID_TMPL_TOOLS_EXPORT_TMPL, OnUpdateToolbarExportTmpl)


	ON_NOTIFY(TCN_SELCHANGE, ID_TABCONTROL, OnSelectedChanged)

	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST8_1, OnItemchangedList)

	ON_EN_CHANGE(IDC_EDIT8_3, &CLogScaleTemplatesFormView::OnEnChangeEdit83)
	ON_EN_CHANGE(IDC_EDIT8_4, &CLogScaleTemplatesFormView::OnEnChangeEdit84)
END_MESSAGE_MAP()

CLogScaleTemplatesFormView::CLogScaleTemplatesFormView()
	: CXTResizeFormView(CLogScaleTemplatesFormView::IDD),
	m_bInitialized(FALSE),
	m_nSelectedIndex(0),
	m_nSelectedSpeciesTab(0),
	m_bOnlySave(FALSE)
	, m_csDate(_T(""))
{

}

CLogScaleTemplatesFormView::~CLogScaleTemplatesFormView()
{
}

void CLogScaleTemplatesFormView::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLogScaleTemplatesFormView)
	DDX_Control(pDX, IDC_GRP8_1, m_wndGrp8_1);

	DDX_Control(pDX, IDC_LBL8_1, m_lbl8_1);
	DDX_Control(pDX, IDC_LBL8_2, m_lbl8_2);
	DDX_Control(pDX, IDC_LBL8_3, m_lbl8_3);
	DDX_Control(pDX, IDC_LBL8_4, m_lbl8_4);
	DDX_Control(pDX, IDC_LBL8_5, m_lbl8_5);
	DDX_Control(pDX, IDC_LBL8_6, m_lbl8_6);
	DDX_Control(pDX, IDC_LBL8_7, m_lbl8_7);
	DDX_Control(pDX, IDC_LBL8_8, m_lbl8_8);
	DDX_Control(pDX, IDC_LBL8_9, m_lbl8_9);
	DDX_Control(pDX, IDC_LBL8_10, m_lbl8_10);
	DDX_Control(pDX, IDC_LBL8_11, m_lbl8_11);
	DDX_Control(pDX, IDC_LBL8_12, m_lbl8_12);
	DDX_Control(pDX, IDC_LBL8_13, m_lbl8_13);
	DDX_Control(pDX, IDC_LBL8_14, m_lbl8_14);
	DDX_Control(pDX, IDC_LBL8_15, m_lbl8_15);
	DDX_Control(pDX, IDC_LBL8_16, m_lbl8_16);
	DDX_Control(pDX, IDC_LBL8_17, m_lbl8_17);
	DDX_Control(pDX, IDC_LBL8_19, m_lbl8_19);
	DDX_Control(pDX, IDC_LBL8_20, m_lbl8_20);

	DDX_Control(pDX, IDC_EDIT8_1, m_edit8_1);
	DDX_Control(pDX, IDC_EDIT8_2, m_edit8_2);
	DDX_Control(pDX, IDC_EDIT8_3, m_edit8_3);
	DDX_Control(pDX, IDC_EDIT8_4, m_edit8_4);
	DDX_Control(pDX, IDC_EDIT8_5, m_edit8_5);

	DDX_Control(pDX, IDC_COMBO8_2, m_cbox8_2);
	DDX_Control(pDX, IDC_COMBO8_3, m_cbox8_3);
	DDX_Control(pDX, IDC_COMBO8_4, m_cbox8_4);
	DDX_Control(pDX, IDC_COMBO8_5, m_cbox8_5);
	DDX_Control(pDX, IDC_COMBO8_6, m_cbox8_6);
	DDX_Control(pDX, IDC_COMBO8_7, m_cbox8_7);
	DDX_Control(pDX, IDC_COMBO8_8, m_cbox8_8);
	DDX_Control(pDX, IDC_COMBO8_9, m_cbox8_9);
	DDX_Control(pDX, IDC_COMBO8_10, m_cbox8_10);
	DDX_Control(pDX, IDC_COMBO8_11, m_cbox8_11);
	DDX_Control(pDX, IDC_COMBO8_13, m_cbox8_13);

	//}}AFX_DATA_MAP

	DDX_Control(pDX, IDC_DATE, m_dtDate);
	DDX_DateTimeCtrl(pDX, IDC_DATE, m_csDate);
}

void CLogScaleTemplatesFormView::OnUpdateToolbar(CCmdUI* pCmdUI)
{
	if (pCmdUI->m_nID == ID_BUTTON32808)
		pCmdUI->Enable(m_bEnableToolBarBtnSave);
	if (pCmdUI->m_nID == ID_BUTTON32809)
		pCmdUI->Enable(m_bEnableToolBarBtnDelete);
	if (pCmdUI->m_nID == ID_BUTTON32810)
		pCmdUI->Enable(m_bEnableToolBarBtnTools);
	if (pCmdUI->m_nID == ID_BUTTON32811)
		pCmdUI->Enable(m_bEnableToolBarBtnImport);
	if (pCmdUI->m_nID == ID_BUTTON32812)
		pCmdUI->Enable(m_bEnableToolBarBtnExport);
}

void CLogScaleTemplatesFormView::OnUpdateToolbarAddPrl(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableToolBarBtnChangePricelist);

}

void CLogScaleTemplatesFormView::OnUpdateToolbarExportTmpl(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bEnableToolBarBtnExportTmpl);
}



// CLogScaleTemplatesFormView diagnostics

#ifdef _DEBUG
void CLogScaleTemplatesFormView::AssertValid() const
{
	CXTResizeFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CLogScaleTemplatesFormView::Dump(CDumpContext& dc) const
{
	CXTResizeFormView::Dump(dc);
}
#endif
#endif //_DEBUG

BOOL CLogScaleTemplatesFormView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying the CREATESTRUCT cs
	if( !CXTResizeFormView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

int CLogScaleTemplatesFormView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CXTResizeFormView::OnCreate(lpCreateStruct) == -1)
		return -1;

	CFont fnt;
	LOGFONT lf;
	VERIFY(fnt.CreateFont(
   18,                        // nHeight
   0,                         // nWidth
   0,                         // nEscapement
   0,                         // nOrientation
   FW_NORMAL,                 // nWeight
   FALSE,                     // bItalic
   FALSE,                     // bUnderline
   0,                         // cStrikeOut
   ANSI_CHARSET,              // nCharSet
   OUT_DEFAULT_PRECIS,        // nOutPrecision
   CLIP_DEFAULT_PRECIS,       // nClipPrecision
   DEFAULT_QUALITY,           // nQuality
   DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
   _T("Times New Roman")));   // lpszFacename

	fnt.GetLogFont( &lf );

	m_wndTabControl.Create(WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS, CRect(0, 0, 0, 0), this, ID_TABCONTROL);
	m_wndTabControl.GetPaintManager()->SetAppearance(xtpTabAppearanceVisualStudio2005);
	m_wndTabControl.GetPaintManager()->m_bHotTracking = TRUE;
	m_wndTabControl.GetPaintManager()->m_bShowIcons = TRUE;
	m_wndTabControl.GetPaintManager()->m_bBoldSelected = TRUE;
	m_wndTabControl.GetPaintManager()->SetFontIndirect( &lf );
	m_wndTabControl.GetPaintManager()->DisableLunaColors( FALSE );

	return 0;
}


// CLogScaleTemplatesFormView message handlers
void CLogScaleTemplatesFormView::OnInitialUpdate()
{
	CXTResizeFormView::OnInitialUpdate();

	m_sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

	if (! m_bInitialized )
	{
		m_edit8_1.SetLimitText(50);
		m_edit8_2.SetLimitText(25);

		setupForDBConnection(AfxGetMainWnd()->GetSafeHwnd(),this->GetSafeHwnd());

		if (fileExists(m_sLangFN))
		{
			RLFReader xml;
			if (xml.Load(m_sLangFN))
			{
				m_sMsgCap = xml.str(IDS_STRING99);
				m_sMsgRemoveSpecies1 = xml.str(IDS_STRING4257);
				m_sMsgRemoveSpecies2 = xml.str(IDS_STRING4258);
				m_sMsgRemoveTable1 = xml.str(IDS_STRING4470);
				m_sMsgRemoveTable2 = xml.str(IDS_STRING4471);
				m_sMsgMissingData1.Format(L"%s%s\n\n%s\n\n",
					xml.str(IDS_STRING4460),
					xml.str(IDS_STRING4461),
					xml.str(IDS_STRING4462));
				m_sMsgMissingData2.Format(L"%s%s\n\n",
					xml.str(IDS_STRING4460),
					xml.str(IDS_STRING4461));
	
				m_sMsgMaxUserdefFunctions.Format(xml.str(IDS_STRING4463),MAX_NUMBER_OF_EXTRA_VFUNC);
				m_sMsgMaxUserdefFunctions += L"\n" + xml.str(IDS_STRING4464);

				m_sMsgTemplateNameAlreadyUsed1.Format(L"%s\n%s\n\n%s\n\n",
					xml.str(IDS_STRING4465),
					xml.str(IDS_STRING4466),
					xml.str(IDS_STRING4467));
				m_sMsgTemplateNameAlreadyUsed2.Format(L"%s\n%s\n\n",
					xml.str(IDS_STRING4465),
					xml.str(IDS_STRING4466));

				m_sMsgNoPricelist = xml.str(IDS_STRING4468);

				m_sMsgCanNotRemove = xml.str(IDS_STRING4469);

				m_sMsgNotAContractFile.Format(L"%s\n%s\n\n",
					xml.str(IDS_STRING4474),
					xml.str(IDS_STRING4475));

				m_sExportContractFilter = xml.str(IDS_STRING4472);
				m_sImportContractFilter = xml.str(IDS_STRING4473);

				tokenizeString(xml.str(IDS_STRING205),';',m_sarrMeasuringMode);
			
				xml.clean();
			}

		}

//		m_lbl8_13.SetLblFontEx(-1,FW_NORMAL);
//		m_lbl8_13.ShowWindow(SW_HIDE);
		m_lbl8_14.SetLblFontEx(16,FW_NORMAL);
		m_lbl8_14.SetBkColor(INFOBK);

		m_edit8_3.SetAsNumeric();
		m_edit8_3.ModifyStyle(0,ES_RIGHT);
		m_edit8_4.SetAsNumeric();
		m_edit8_4.ModifyStyle(0,ES_RIGHT);


		getDLLVolumeFuncDesc(m_vecFuncDesc,m_vecUserVolTables);

		setupReport();

		if (m_vecLogScaleTemplates.size() > 0)
		{
			m_nSelectedIndex = m_vecLogScaleTemplates.size()-1;
			enableView(TRUE);
			setNavigationButtons(m_nSelectedIndex > 0,m_nSelectedIndex < (m_vecLogScaleTemplates.size()-1));

			m_bEnableToolBarBtnSave = TRUE;
			m_bEnableToolBarBtnDelete = TRUE;
			m_bEnableToolBarBtnTools = TRUE;
			m_bEnableToolBarBtnImport = TRUE;
			m_bEnableToolBarBtnExport = TRUE;
			
			m_bEnableToolBarBtnChangePricelist = TRUE;
			m_bEnableToolBarBtnExportTmpl = TRUE;

		}
		else
		{
			m_nSelectedIndex = -1;
			enableView(FALSE);
			setNavigationButtons(FALSE,FALSE);

			m_bEnableToolBarBtnSave = FALSE;
			m_bEnableToolBarBtnDelete = FALSE;
			m_bEnableToolBarBtnTools = FALSE;
			m_bEnableToolBarBtnImport = TRUE;
			m_bEnableToolBarBtnExport = FALSE;

			m_bEnableToolBarBtnChangePricelist = FALSE;
			m_bEnableToolBarBtnExportTmpl = FALSE;

		}

		m_cbox8_10.ResetContent();
		for (int i = 0;i < m_sarrMeasuringMode.GetCount();i++)
		{
			m_cbox8_10.AddString(m_sarrMeasuringMode.GetAt(i));
		}

		populateData();

		m_bInitialized = TRUE;
	}
}

void CLogScaleTemplatesFormView::OnSize(UINT nType, int cx, int cy)
{
	if (m_wndTabControl.GetSafeHwnd())
	{
		setResize(&m_wndTabControl,300,100,cx-320,cy-105);
	}

	if (m_lbl8_13.GetSafeHwnd())
	{
		setResize(&m_lbl8_13,300,10,110,23);
	}

	if (m_lbl8_14.GetSafeHwnd())
	{
		setResize(&m_lbl8_14,411,8,300,22);
	}

	if (m_lbl8_19.GetSafeHwnd())
	{
		setResize(&m_lbl8_19,300,39,110,22);
	}
	if (m_cbox8_11.GetSafeHwnd())
	{
		setResize(&m_cbox8_11,411,38,150,22);
	}

	if (m_lbl8_20.GetSafeHwnd())
	{
		setResize(&m_lbl8_20,300,66,110,22);
	}
	if (m_cbox8_13.GetSafeHwnd())
	{
		setResize(&m_cbox8_13,411,65,150,22);
	}


	CXTResizeFormView::OnSize(nType, cx, cy);
}

BOOL CLogScaleTemplatesFormView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{

	// if size doesn't match we don't know what this is
	if (pData->cbData == sizeof( DB_CONNECTION_DATA))
	{
		memcpy(&m_dbConnectionData,pData->lpData,sizeof(DB_CONNECTION_DATA));
		if (m_dbConnectionData.conn->isConnected())
		{
			m_pDB = new CDBHandling(m_dbConnectionData);

			if (m_pDB != NULL)
			{
				m_pDB->getTmplTables(m_vecLogScaleTemplates);
				m_pDB->getSpecies(m_vecSpecies);
				m_pDB->getUserVolTables(m_vecUserVolTables);
				m_pDB->getGrades(m_vecGrades);
				m_pDB->getDefaults(m_vecDefaults);
				m_pDB->getPricelist(m_vecVecPricelist);
				m_pDB->getRegister(m_vecRegister);
			}
		}
	}

	return CXTResizeFormView::OnCopyData(pWnd, pData);
}

void CLogScaleTemplatesFormView::OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult)
{
	UNUSED_ALWAYS(pNMHDR);
	*pResult = 0;

	CXTPTabManagerItem *pItem = m_wndTabControl.getSelectedTabPage();
	if (pItem != NULL)
	{
		m_nSelectedSpeciesTab = pItem->GetIndex();
	}
}

void CLogScaleTemplatesFormView::OnCommand(UINT nID)
{
	switch(nID)
	{
		case ID_BUTTON32807 : New(); break;
		case ID_BUTTON32808 : m_bOnlySave = TRUE; Save(); m_bOnlySave = FALSE; break;
		case ID_BUTTON32809 : Delete(); break;
		case ID_BUTTON32811 : Import(); break;
		case ID_BUTTON32812 : Export(); break;
	};
}
void CLogScaleTemplatesFormView::OnCommand2(UINT nID)
{
	switch(nID)
	{

		case ID_TMPL_TOOLS_UPD_PRL : ChangeUpdPricelist(); break;
		case ID_TMPL_TOOLS_EXPORT_TMPL : ExportTemplate(); break;
	};
}


LRESULT CLogScaleTemplatesFormView::OnSuiteMessage(WPARAM wParam,LPARAM lParam)
{
	CString S;
	if (wParam >= REGISTER_TYPES::BUYER && wParam <= REGISTER_TYPES::SUPPLIER)
	{
		if (m_pDB != NULL)
			m_pDB->getRegister(m_vecRegister);
		switch (wParam)
		{
			case REGISTER_TYPES::BUYER:	cbRegisterData(REGISTER_TYPES::BUYER,&m_cbox8_2,m_vecRegister,m_recLogScaleTemplates.getBuyer());	break;
			case REGISTER_TYPES::HAULER:	cbRegisterData(REGISTER_TYPES::HAULER,&m_cbox8_3,m_vecRegister,m_recLogScaleTemplates.getHauler());	break;
			case REGISTER_TYPES::LOCATION:	cbRegisterData(REGISTER_TYPES::LOCATION,&m_cbox8_4,m_vecRegister,m_recLogScaleTemplates.getLocation());	break;
			case REGISTER_TYPES::TRACTID:	cbRegisterData(REGISTER_TYPES::TRACTID,&m_cbox8_5,m_vecRegister,m_recLogScaleTemplates.getTractID());	break;
			case REGISTER_TYPES::SCALER:	cbRegisterData(REGISTER_TYPES::SCALER,&m_cbox8_6,m_vecRegister,m_recLogScaleTemplates.getScaler());	break;
			case REGISTER_TYPES::SOURCEID:	cbRegisterData(REGISTER_TYPES::SOURCEID,&m_cbox8_7,m_vecRegister,m_recLogScaleTemplates.getSourceID());	break;
			case REGISTER_TYPES::VENDOR:	cbRegisterData(REGISTER_TYPES::VENDOR,&m_cbox8_8,m_vecRegister,m_recLogScaleTemplates.getVendor());	break;
			case REGISTER_TYPES::SUPPLIER:	cbRegisterData(REGISTER_TYPES::SUPPLIER,&m_cbox8_9,m_vecRegister,m_recLogScaleTemplates.getSupplier());	break;
		};
	}

	if (wParam == IDD_FORMVIEW9)
	{
		if (m_pDB != NULL)
		{
			m_pDB->getPricelist(m_vecVecPricelist);
			if (m_vecVecPricelist.size() > 0)
			{
				for (UINT i = 0;i < m_vecVecPricelist.size();i++)
				{
					if (m_recLogScaleTemplates.getPrlID() == m_vecVecPricelist[i].getPrlID())
					{
						m_pDB->updTmplTablePricelist(m_recLogScaleTemplates.getTmplID(),
																				 m_vecVecPricelist[i].getPrlID(),
																				 m_vecVecPricelist[i].getName(),
																				 m_vecVecPricelist[i].getPricelist());
						m_pDB->getTmplTables(m_vecLogScaleTemplates);
						populateData();
					}
				}
				
			}
		}
	}

	
	return 0L;
}

LRESULT CLogScaleTemplatesFormView::OnMessageFromShell( WPARAM wParam, LPARAM lParam )
{
	switch (wParam)
	{
		case ID_DBNAVIG_LIST:
		{
			showFormView(ID_LIST_CONTRACT,m_sLangFN,0,0);
			break;
		}

		case ID_NEW_ITEM :
		{
			break;
		}	// case ID_NEW_ITEM :

		case ID_SAVE_ITEM :
		{
			break;
		}	// case ID_SAVE_ITEM :
		
		case ID_DELETE_ITEM :
		{
			break;
		}	// case ID_DELETE_ITEM :

		// Messages from HMSShell; Database navigation toolbar
		case ID_DBNAVIG_START :
		{
			m_bOnlySave = FALSE;
			if (Save())
			{
				m_nSelectedIndex = 0;
				populateData();
				setNavigationButtons(FALSE,TRUE);
			}
			break;
		}
		case ID_DBNAVIG_PREV :
		{
			m_bOnlySave = FALSE;
			if (Save())
			{
				m_nSelectedIndex--;
				if (m_nSelectedIndex < 0)
					m_nSelectedIndex = 0;

				populateData();

				if (m_nSelectedIndex == 0)
				{
					setNavigationButtons(FALSE,TRUE);
				}
				else
				{
					setNavigationButtons(TRUE,TRUE);
				}
			}
			break;
		}
		case ID_DBNAVIG_NEXT :
		{
			m_bOnlySave = FALSE;
			if (Save())
			{
				m_nSelectedIndex++;
				if (m_nSelectedIndex > ((long)m_vecLogScaleTemplates.size() - 1))
					m_nSelectedIndex = (long)m_vecLogScaleTemplates.size() - 1;

				populateData();
						
				if (m_nSelectedIndex == (long)m_vecLogScaleTemplates.size() - 1)
				{
					setNavigationButtons(TRUE,FALSE);
				}
				else
				{
					setNavigationButtons(TRUE,TRUE);
				}
			}
			break;
		}
		case ID_DBNAVIG_END :
		{
			m_bOnlySave = FALSE;
			if (Save())
			{
				m_nSelectedIndex = (long)m_vecLogScaleTemplates.size()-1;
	
				populateData();

				setNavigationButtons(TRUE,FALSE);	
			}
			break;
		}	// case ID_NEW_ITEM :

	}	// switch (wParam)
	return 0L;
}


void CLogScaleTemplatesFormView::Import()
{
	long nDataFileSize = 0;
	CString sToken = L"",sSpcID = L"",sSpcCode = L"",sSpcName = L"",sBarkInch = L"",sBarkMM = L"";
	CString sGrdID = L"",sGrdCode = L"",sGrdName = L"",sIsPulpwood = L"",sFuncID = L"",sPrice = L"";
	CVecSpecies vecImportSpecies;
	CVecGradesExt vecImportGrades;

	BOOL bIsNameUsed = TRUE;
	TCHAR szName[128],szCreatedBy[128],szDate[32],szPrlName[128],szSpcCode[128],szGrdCode[128],szNotes[2048];
	CString sTmp = L"",sTmplName = L"",sPrlName = L"",sData = L"";
	int nMeasuringMode = -1,nCnt = 1,nItemsInPricelist = 0;
	double fTrimFt = 0.0,fTrimCM = 0.0;

	sTmp.Format(L"%s (*.xml)|*.xml|",m_sImportContractFilter);
	
	CFileDialog dlg(TRUE,L".xml",m_edit8_1.getText(),OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,sTmp);
	szName[0] = '\0';
	szCreatedBy [0] = '\0';
	szDate [0] = '\0';
	szPrlName[0] = '\0';
	szSpcCode[0] = '\0';
	szGrdCode[0] = '\0';
	szNotes [0] = '\0';
	if (dlg.DoModal() == IDOK)
	{
		LogScaleContractParser *pars = new LogScaleContractParser();
		if (pars->LoadFromFile(dlg.GetPathName()))
		{
			if (!pars->isALogScaleContract())
			{
				::MessageBox(GetSafeHwnd(),m_sMsgNotAContractFile,m_sMsgCap,MB_ICONSTOP | MB_OK);
				return;
			}

			// Name of contract
			pars->getHeaderName(szName);
			sTmplName = szName;
			// Check that name of Contract isn't already used
			nCnt = 1;
			bIsNameUsed = TRUE;
			while (bIsNameUsed)
			{
				bIsNameUsed = m_pDB->isTmplNameAlreadyUsed(sTmplName);
				if (bIsNameUsed)
				{
					sTmp.Format(L"%s(%d)",szName,nCnt);
					sTmplName = sTmp;
					nCnt++;
				}
			}

			// Name of pricelist in contract
			pars->getHeaderPrlName(szPrlName);
			sPrlName = szPrlName;
			// Check that name of Pricelist isn't already used
			nCnt = 1;
			bIsNameUsed = TRUE;
			while (bIsNameUsed)
			{
				bIsNameUsed = m_pDB->isPricelistNameAlreadyUsed(sPrlName);
				if (bIsNameUsed)
				{
					sTmp.Format(L"%s(%d)",szPrlName,nCnt);
					sPrlName = sTmp;
					nCnt++;
				}
			}
		}

		pars->getHeaderDoneBy(szCreatedBy);
		pars->getHeaderDate(szDate);
		pars->getHeaderMode(&nMeasuringMode);
		pars->getHeaderTrimFt(&fTrimFt);
		pars->getHeaderTrimCM(&fTrimCM);
		pars->getHeaderDefSpcCode(szSpcCode);
		pars->getHeaderDefGrdCode(szGrdCode);
		pars->getHeaderNotes(szNotes);

		// * Header *
		sTmp.Format(L"%d;%s;%s;%s;",
			FUNC_INDEX::HEADER,
			sPrlName,								// Name of pricelist
			szCreatedBy,						// Created by
			szDate);								// Date

		TCHAR *szDataFile;
		nDataFileSize = pars->getDataFileSize() + 100;
		szDataFile = (TCHAR *)malloc(nDataFileSize * sizeof(TCHAR));
		pars->getDataFile(szDataFile);

#ifdef _DO_PRICELIST_CHECK_ON_IMPORT_IN_CONTRACT
			//----------------------------------------------------------------------
			// Check species in imported pricelist compared to species in DB
			// If they don't match ask user if he likes to proceed, and if so,
			// match species to already exisiting species
			CSpecies recSpc = CSpecies();
			CGrades recGrd = CGrades();
			BOOL bFound = FALSE;
			std::wstringstream ss;
			std::wstring line;
			ss << szDataFile; // sData.GetBuffer();
			while (std::getline(ss,line))
			{
				AfxExtractSubString(sToken,line.c_str(),0,';');

				// Check Species
				if (_tstoi(sToken) == FUNC_INDEX::SPECIES)
				{
					bFound = FALSE;

					sSpcID.Empty();
					sSpcCode.Empty();
					sSpcName.Empty();
					AfxExtractSubString(sSpcID,line.c_str(),1,';');
					AfxExtractSubString(sSpcCode,line.c_str(),2,';');
					AfxExtractSubString(sSpcName,line.c_str(),3,';');
					AfxExtractSubString(sBarkInch,line.c_str(),4,';');
					AfxExtractSubString(sBarkMM,line.c_str(),5,';');
					for (int i = 0;i < m_vecSpecies.size();i++)
					{
						recSpc = m_vecSpecies[i];
						vecImportSpecies.push_back(CSpecies(_tstoi(sSpcID),sSpcCode,sSpcName,_tstof(localeToStr(sBarkInch)),_tstof(localeToStr(sBarkMM))));
					}

				}	// if (_tstoi(sToken) == FUNC_INDEX::SPECIES)

				// Check Grades
				if (_tstoi(sToken) == FUNC_INDEX::DATA)
				{
					bFound = FALSE;

					AfxExtractSubString(sGrdID,line.c_str(),2,';');
					AfxExtractSubString(sPrice,line.c_str(),3,';');
					AfxExtractSubString(sFuncID,line.c_str(),4,';');
					AfxExtractSubString(sGrdCode,line.c_str(),5,';');
					AfxExtractSubString(sGrdName,line.c_str(),6,';');
					AfxExtractSubString(sIsPulpwood,line.c_str(),7,';');
					vecImportGrades.push_back(CGradesExt(_tstoi(sGrdID),sSpcCode,sGrdCode,sGrdName,_tstoi(sIsPulpwood),_tstoi(sFuncID),_tstof(localeToStr(sPrice))));
				}	// if (_tstoi(sToken) == FUNC_INDEX::DATA)
			}	// while (std::getline(ss,line))


			if (vecImportGrades.size() > 0 || vecImportSpecies.size() > 0)
			{

				CMatchPrlGrdDlg *pDlg = new CMatchPrlGrdDlg();
				if (pDlg != NULL)
				{
					pDlg->setDB(m_pDB);
					pDlg->setSpecies(m_vecSpecies);
					pDlg->setGrades(m_vecGrades);
					pDlg->setImportSpecies(vecImportSpecies);
					pDlg->setImportGrades(vecImportGrades);
					if (pDlg->DoModal() == IDOK)
					{
						clearView();
						sData = pDlg->getData();

						if (m_pDB != NULL)
						{
							m_pDB->newPricelist(CPricelist(-1,sPrlName,szCreatedBy,szDate,sData,0,L""));
							nItemsInPricelist = m_vecVecPricelist.size();
							m_pDB->getPricelist(m_vecVecPricelist);
						}
	
						if (m_vecVecPricelist.size() > nItemsInPricelist)
						{
							m_vecLogScaleTemplates.push_back(CLogScaleTemplates(-1,sTmplName,szCreatedBy,szDate,-1,-1,-1,-1,-1,-1,-1,-1,
																							szNotes,szDataFile,m_vecVecPricelist[m_vecVecPricelist.size()-1].getPrlID(),sPrlName,
																							fTrimFt,fTrimCM,nMeasuringMode,-1,-1));
							m_nSelectedIndex = m_vecLogScaleTemplates.size() - 1;
							enableView(TRUE);
							// Need to reload Species and Grades
							m_pDB->getSpecies(m_vecSpecies);
							m_pDB->getGrades(m_vecGrades);

							populateData();
							// Set default Species and Grade
							m_cbox8_11.SetCurSel(	m_cbox8_11.FindString(0,szSpcCode));
							m_cbox8_13.SetCurSel(	m_cbox8_13.FindString(0,szGrdCode));

							Save();
							setNavigationButtons(m_nSelectedIndex > 0,m_nSelectedIndex < (m_vecLogScaleTemplates.size()-1));
							m_bEnableToolBarBtnImport =  m_vecLogScaleTemplates.size() > 0;
							m_bEnableToolBarBtnExport = m_vecLogScaleTemplates.size() > 0;
							m_bEnableToolBarBtnSave = m_vecLogScaleTemplates.size() > 0;
							m_bEnableToolBarBtnDelete = m_vecLogScaleTemplates.size() > 0;
							m_bEnableToolBarBtnTools = m_vecLogScaleTemplates.size() > 0;


						}
					}	// if (pDlg->DoModal() == IDOK)
				}	// if (pDlg != NULL)

			}	// if (vecImportGrades.size() > 0 || vecImportSpecies.size() > 0)

#elif

		clearView();

		if (m_pDB != NULL)
		{
			m_pDB->newPricelist(CPricelist(-1,sPrlName,szCreatedBy,szDate,sData,0,L""));
			nItemsInPricelist = m_vecVecPricelist.size();
			m_pDB->getPricelist(m_vecVecPricelist);
		}
		
		if (m_vecVecPricelist.size() > nItemsInPricelist)
		{
			m_vecLogScaleTemplates.push_back(CLogScaleTemplates(-1,sTmplName,szCreatedBy,szDate,-1,-1,-1,-1,-1,-1,-1,-1,
																			szNotes,szDataFile,m_vecVecPricelist[m_vecVecPricelist.size()-1].getPrlID(),sPrlName,
																			fTrimFt,fTrimCM,nMeasuringMode,-1,-1));
			m_nSelectedIndex = m_vecLogScaleTemplates.size() - 1;
			enableView(TRUE);
			populateData();
			Save();
			setNavigationButtons(m_nSelectedIndex > 0,m_nSelectedIndex < (m_vecLogScaleTemplates.size()-1));
		}
#endif
		free(szDataFile);
		if (pars != NULL)
			delete pars;
	}	// if (dlg.DoModal() == IDOK)
}

void CLogScaleTemplatesFormView::Export()
{
	int nFuncID = -99;
	CStringArray sarrXML;
	CString sXML = L"",sTmp = L"",sDate = L"",sPrice = L"";
	int nSpcID = -1,nGrdID = -1;

	UpdateData(TRUE);

	CSpecies spc = CSpecies();

	//------------------------------------------------------------------
	// Start creating xml contract file
	sarrXML.RemoveAll();
	sarrXML.Add(TAG_FIRST_ROW);
	sarrXML.Add(TAG_START_CONTRACT);
	
	// Add header information
	sarrXML.Add(TAG_START_HEADER2);
	// Name of contract
	sTmp.Format(TAG_HEADER_NAME2,m_edit8_1.getText());
	sarrXML.Add(sTmp);
	// Done by
	sTmp.Format(TAG_HEADER_DONE_BY2,m_edit8_2.getText());
	sarrXML.Add(sTmp);
	// Date
	sTmp.Format(TAG_HEADER_DATE2, m_csDate);
	sarrXML.Add(sTmp);
	// Measuringmode 0 = Inch, 1 = Metric
	sTmp.Format(TAG_HEADER_MODE2,m_cbox8_10.GetCurSel());
	sarrXML.Add(sTmp);
	// Trim in feet
	sTmp.Format(TAG_HEADER_TRIM_FT2,m_edit8_3.getFloat());
	sarrXML.Add(sTmp);
	// Trim in cm
	sTmp.Format(TAG_HEADER_TRIM_CM2,m_edit8_4.getFloat());
	sarrXML.Add(sTmp);
	// Name of pricelist
	sTmp.Format(TAG_HEADER_PRL_NAME2,m_recLogScaleTemplates.getPrlName());
	sarrXML.Add(sTmp);
	// Species code
	nSpcID = (m_cbox8_11.GetCurSel() >= 0 && m_cbox8_11.GetCurSel() < vecSpeciesInPricelist.size() ?  vecSpeciesInPricelist[m_cbox8_11.GetCurSel()].nID : -1);
	getSpecies(nSpcID,spc);
	sTmp.Format(TAG_HEADER_DEF_SPCCODE2,spc.getSpcCode());
	sarrXML.Add(sTmp);
	// Grades code
	nGrdID = (m_cbox8_13.GetCurSel() >= 0 && m_cbox8_13.GetCurSel() < vecGradesInPricelist.size() ?  vecGradesInPricelist[m_cbox8_13.GetCurSel()].nID : -1);
	sTmp.Format(TAG_HEADER_DEF_GRDCODE2,getGradeCode(nGrdID));
	sarrXML.Add(sTmp);
	// Notes
	sTmp.Format(TAG_HEADER_NOTES2,m_edit8_5.getText());
	sarrXML.Add(sTmp);
	
	sarrXML.Add(TAG_END_HEADER2);

	createData(TRUE);
	sTmp.Format(TAG_DATA_FILE,m_sDataPrl);
	sarrXML.Add(sTmp);

	// End of xml file
	sarrXML.Add(TAG_END_CONTRACT);
	//------------------------------------------------------------------
	// Add data from sarrXML to sXML
	sXML.Empty();
	for (int i = 0;i < sarrXML.GetCount();i++)
	{
		sXML += sarrXML.GetAt(i);
	}

	sTmp.Format(L"%s (*.xml)|*.xml|",m_sExportContractFilter);
	CFileDialog dlg(FALSE,L".xml",m_edit8_1.getText(),OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,sTmp);
	if (dlg.DoModal() == IDOK)
	{
		LogScaleContractParser pars;
		if (pars.LoadFromBuffer(sXML))
		{
			pars.SaveToFile(dlg.GetPathName());
		}
	}

}


void CLogScaleTemplatesFormView::OnItemchangedList(NMHDR* pNMHDR, LRESULT* pResult) 
{
   	UNUSED_ALWAYS(pNMHDR);
		pResult = 0;
}

QUIT_TYPES::Q_T_RETURN CLogScaleTemplatesFormView::checkDataBeforeSave(CHECK_SAVE_TYPES::CHECK_SAVE cs)
{
	QUIT_TYPES::Q_T_RETURN qt;
	// Check name of template
	if ((qt = isNameOfTemplateOK(m_edit8_1.getText())) != QUIT_TYPES::DO_SAVE)
		return qt;

#ifdef _FORCE_ERROR_CORRECTION_CONTRACT
	if (m_vecLogScaleTemplates.size() == 0 && !isViewEnabled())
		return QUIT_TYPES::QUIT_ANYWAY;	// Nothing to save

	if (m_edit8_1.getText().IsEmpty() || 
			m_cbox8_10.GetCurSel() == CB_ERR ||
			m_cbox8_11.GetCurSel() == CB_ERR ||
			m_cbox8_13.GetCurSel() == CB_ERR ||
		  m_wndTabControl.getNumOfTabPages() == 0)
	{
			::MessageBox(GetSafeHwnd(),m_sMsgMissingData2,m_sMsgCap,MB_ICONSTOP | MB_OK);
			return QUIT_TYPES::NO_SAVE;
	}
	else
	{
		return QUIT_TYPES::DO_SAVE;
	}
#else
	if (cs == CHECK_SAVE_TYPES::CHECK_SAVE_ON_QUIT)
	{
		if (m_vecLogScaleTemplates.size() == 0 && !isViewEnabled())
			return QUIT_TYPES::QUIT_ANYWAY;	// Nothing to save

		if (m_edit8_1.getText().IsEmpty() || 
				m_cbox8_10.GetCurSel() == CB_ERR ||
				m_cbox8_11.GetCurSel() == CB_ERR ||
				m_cbox8_13.GetCurSel() == CB_ERR ||
			  m_wndTabControl.getNumOfTabPages() == 0)
		{
			if (::MessageBox(GetSafeHwnd(),m_sMsgMissingData1,m_sMsgCap,MB_ICONSTOP | MB_YESNO | MB_DEFBUTTON2) == IDNO)
				return QUIT_TYPES::NO_SAVE;
			else
				return QUIT_TYPES::QUIT_ANYWAY;
		}
		else
		{
			return QUIT_TYPES::DO_SAVE;
		}
	}
	else if (cs == CHECK_SAVE_TYPES::CHECK_SAVE_NO_QUIT)
	{
		if (m_edit8_1.getText().IsEmpty() || 
				m_cbox8_10.GetCurSel() == CB_ERR ||
				m_cbox8_11.GetCurSel() == CB_ERR ||
				m_cbox8_13.GetCurSel() == CB_ERR ||
			  m_wndTabControl.getNumOfTabPages() == 0)

		{
			::MessageBox(GetSafeHwnd(),m_sMsgMissingData2,m_sMsgCap,MB_ICONSTOP | MB_OK);
			return QUIT_TYPES::NO_SAVE;
		}
		else
		{
			return QUIT_TYPES::DO_SAVE;
		}
	}
#endif
}

BOOL CLogScaleTemplatesFormView::isViewEnabled()
{
	return m_wndTabControl.IsWindowEnabled();
}

// Set navigation buttons in shell, depending on index of item to display; 061002 p�d
void CLogScaleTemplatesFormView::setNavigationButtons(BOOL start_prev,BOOL end_next)
{
	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 060126 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,start_prev);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,end_next);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,end_next);
	
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,TRUE);
	AfxGetMainWnd()->UpdateWindow();
}


QUIT_TYPES::Q_T_RETURN CLogScaleTemplatesFormView::isNameOfTemplateOK(LPCTSTR name)
{
	if (m_vecLogScaleTemplates.size() > 0)
	{
		for (UINT i = 0;i < m_vecLogScaleTemplates.size();i++)
		{
			if (m_vecLogScaleTemplates[i].getTmplName().CompareNoCase(name) == 0 && 
				  m_nSelectedIndex != i)
			{
				::MessageBox(GetSafeHwnd(),m_sMsgTemplateNameAlreadyUsed2,m_sMsgCap,MB_ICONSTOP | MB_OK);
				return QUIT_TYPES::NO_SAVE;
			}
		}	// for (UINT i = 0;i < m_vecLogScaleTemplates.size();i++)
	}	// if (m_vecLogScaleTemplates.size() > 0)
	return QUIT_TYPES::DO_SAVE;
}


void CLogScaleTemplatesFormView::populateData()
{
	CPricelist rec = CPricelist();
	if (m_vecLogScaleTemplates.size() > 0)
	{
		if (m_nSelectedIndex >= 0 && m_nSelectedIndex < m_vecLogScaleTemplates.size())
		{
			removeSpecies(CLogScaleTemplatesFormView::DEL_ALL_SPECIES);

			m_recLogScaleTemplates	= m_vecLogScaleTemplates[m_nSelectedIndex];

			// Collect data on pricelist from pricelist-table.
			// We'll always have the "correct" pricelist
			if (m_pDB != NULL)
			{
				m_pDB->getPricelist(rec,m_recLogScaleTemplates.getPrlID());
				m_recLogScaleTemplates.setPricelist(rec.getPricelist());
				m_recLogScaleTemplates.setPrlID(rec.getPrlID());
				m_recLogScaleTemplates.setPrlName(rec.getName());
			}

			m_edit8_1.SetWindowTextW(m_recLogScaleTemplates.getTmplName());
			m_edit8_2.SetWindowTextW(m_recLogScaleTemplates.getCreatedBy());
			m_csDate = m_recLogScaleTemplates.getDate();
			m_edit8_3.setFloat(m_recLogScaleTemplates.getTrimFT(),1);
			m_edit8_4.setFloat(m_recLogScaleTemplates.getTrimCM(),0);
			m_edit8_5.SetWindowTextW(m_recLogScaleTemplates.getNotes());
			m_lbl8_14.SetWindowTextW(m_recLogScaleTemplates.getPrlName());

			UpdateData(FALSE);

			// Interpret template data and setup tabs
			
			setupSpeciesTabs();

			setupPricelistPerSpecies();
		}
	}	// if (m_vecLogScaleTemplates.size() > 0)

	cbRegisterData(REGISTER_TYPES::BUYER,&m_cbox8_2,m_vecRegister,m_recLogScaleTemplates.getBuyer());
	cbRegisterData(REGISTER_TYPES::HAULER,&m_cbox8_3,m_vecRegister,m_recLogScaleTemplates.getHauler());
	cbRegisterData(REGISTER_TYPES::LOCATION,&m_cbox8_4,m_vecRegister,m_recLogScaleTemplates.getLocation());
	cbRegisterData(REGISTER_TYPES::TRACTID,&m_cbox8_5,m_vecRegister,m_recLogScaleTemplates.getTractID());
	cbRegisterData(REGISTER_TYPES::SCALER,&m_cbox8_6,m_vecRegister,m_recLogScaleTemplates.getScaler());
	cbRegisterData(REGISTER_TYPES::SOURCEID,&m_cbox8_7,m_vecRegister,m_recLogScaleTemplates.getSourceID());
	cbRegisterData(REGISTER_TYPES::VENDOR,&m_cbox8_8,m_vecRegister,m_recLogScaleTemplates.getVendor());
	cbRegisterData(REGISTER_TYPES::SUPPLIER,&m_cbox8_9,m_vecRegister,m_recLogScaleTemplates.getSupplier());
	m_cbox8_10.SetCurSel(m_recLogScaleTemplates.getMeasureMode());

}

BOOL CLogScaleTemplatesFormView::getSpecies(int id,CSpecies& rec)
{
	BOOL bFound = FALSE;
	if (m_vecSpecies.size() > 0)
	{
		for (UINT i = 0;i < m_vecSpecies.size();i++)
		{
			rec = m_vecSpecies[i];
			if (rec.getSpcID() == id)
			{
				bFound = TRUE;			
				break;
			}
		}
	}

	return bFound;
}

CString CLogScaleTemplatesFormView::getCalculationBasis(int id)
{
	CString sBasis = L"";
	if (m_vecFuncDesc.size() > 0)
	{
		for (UINT i = 0;i < m_vecFuncDesc.size();i++)
		{
			if (m_vecFuncDesc[i].getFuncID() == id)
			{
				sBasis = m_vecFuncDesc[i].getBasis();
				break;
			}
		}
	}

	return sBasis;
}


// Add tabs to tabcontrol
void CLogScaleTemplatesFormView::setupSpeciesTabs()
{
	CSpecies rec = CSpecies();
	CString sData;
	int nCounter = 0;
	std::wstringstream ss;
	CString sToken = L"";
	std::wstring line;
	m_vecSpeciesSelected.clear();
	sData = m_recLogScaleTemplates.getPricelist();
	ss << sData.GetBuffer();
	while (std::getline(ss,line))
	{
		AfxExtractSubString(sToken,line.c_str(),0,';');
		if (_tstoi(sToken) == FUNC_INDEX::SPECIES)
		{
			AfxExtractSubString(sToken,line.c_str(),1,';');
			if (getSpecies(_tstoi(sToken),rec))
			{
				m_vecSpeciesSelected.push_back(rec);
			}
		}
	}
	addSpeciesToTabs();

}

void CLogScaleTemplatesFormView::setupPricelistPerSpecies()
{
	CStringArray sarrData;
	int nRowCnt = 0;
	std::wstringstream ss;
	CString sData = L"";
	CString sToken = L"";
	CString sSpcID = L"";
	CString sGrade = L"";
	CString sPrice = L"";
	CString sBasis = L"";
	CString sDefSpc = L"";
	CString sDefGrade = L"";
	std::wstring line;
	BOOL bSpeciesMatchTab = FALSE;

	int nNumOfTabs = m_wndTabControl.getNumOfTabPages();

	CXTPReportRow *pRow = NULL;

	CPricelistTmplReportRec *pRec = NULL;

	sData = m_recLogScaleTemplates.getPricelist();
	ss << sData.GetBuffer();
	while (std::getline(ss,line))
	{
		AfxExtractSubString(sToken,line.c_str(),0,';');
		if (_tstoi(sToken) == FUNC_INDEX::DATA)
		{
			sarrData.Add(line.c_str());
		}
	}

	vecSpeciesInPricelist.clear();
	vecGradesInPricelist.clear();

	CSpecies rec = CSpecies();
	for (int nTab = 0;nTab < nNumOfTabs;nTab++)
	{
		nRowCnt = 0;
		CReportPricelistView *pSelectedView = getReportView(nTab);
		if (pSelectedView != NULL)
		{
			pSelectedView->GetReportCtrl().ResetContent();
			getSpecies(m_mapSpcID[nTab],rec);
			if (rec.getSpcName().IsEmpty())
				vecSpeciesInPricelist.push_back(VDefault(rec.getSpcCode(),rec.getSpcID()));
			else
				vecSpeciesInPricelist.push_back(VDefault(rec.getSpcCode()+ L" - " + rec.getSpcName(),rec.getSpcID()));
			for (int i = 0;i < sarrData.GetCount();i++)
			{
				AfxExtractSubString(sToken,sarrData.GetAt(i),0,';');
				if (_tstoi(sToken) == FUNC_INDEX::DATA)
				{
					AfxExtractSubString(sSpcID,sarrData.GetAt(i),1,';');
					if (_tstoi(sSpcID) == m_mapSpcID[nTab])
					{
						AfxExtractSubString(sGrade,sarrData.GetAt(i),2,';');
						AfxExtractSubString(sPrice,sarrData.GetAt(i),3,';');
						AfxExtractSubString(sBasis,sarrData.GetAt(i),4,';');
						pSelectedView->GetReportCtrl().AddRecord(new CPricelistTmplReportRec(getGradeCode(_tstoi(sGrade)),_tstof(sPrice),getCalculationBasis(_tstoi(sBasis)),_tstoi(sBasis)));	//#4258 lagt till FuncID ox�
						nRowCnt++;
						// Add grades only from first tab
						// For now, grades are the same for each species
						if (nTab == 0)
						{
							vecGradesInPricelist.push_back(VDefault(getGradeCode(_tstoi(sGrade)),_tstoi(sGrade)));
						}
					}
				}
			}

			pSelectedView->GetReportCtrl().Populate();
			pSelectedView->GetReportCtrl().UpdateWindow();
		}
	}

	// Add Species and Grades to defaults
	m_cbox8_11.ResetContent();	// Default Species
	m_cbox8_13.ResetContent();	// Default grade
	int nIndex = -1;
	for (UINT i = 0;i < vecSpeciesInPricelist.size();i++)
	{
		m_cbox8_11.AddString(vecSpeciesInPricelist[i].sName);
		if (vecSpeciesInPricelist[i].nID == m_recLogScaleTemplates.getDefSpecies())
			nIndex = i;
	}
	m_cbox8_11.SetCurSel(nIndex);
	for (int i = 0;i < vecGradesInPricelist.size();i++)
	{
		m_cbox8_13.AddString(vecGradesInPricelist[i].sName);
		if (vecGradesInPricelist[i].nID == m_recLogScaleTemplates.getDefGrade())
			nIndex = i;
	}
	m_cbox8_13.SetCurSel(nIndex);

}



void CLogScaleTemplatesFormView::doReportViewClick(NMHDR * pNotifyStruct)
{
	CRect rect;
	POINT pt;
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	// prevents us from crashing when clicking inside the report, but not on an item
	if(pItemNotify == NULL) 
		return;

	// prevents us from crashing when clicking inside the report, but not on an item
	if(pItemNotify->pItem == NULL)
		return;

	switch (pItemNotify->pColumn->GetItemIndex())
	{
			case COLUMN_2 :
			{
				// Do a hit-test; 080513 p�d
				rect = pItemNotify->pColumn->GetRect();		
				pt = pItemNotify->pt;
				// Check if the user clicked on the Icon or not; 080513 p�d
				if (hitTest_X(pt.x,rect.left,13))
				{
					CXTPReportRow *pRow = getReportView()->GetReportCtrl().GetFocusedRow();
					if (pRow != NULL)
					{
						CPricelistTmplReportRec *pRec = (CPricelistTmplReportRec *)pRow->GetRecord();
						if (pRec != NULL)
						{
							CSelectCalcTypeDlg *pDlg = new CSelectCalcTypeDlg();
							if (pDlg != NULL)
							{
								pDlg->setVecFuncDesc(m_vecFuncDesc,pRec->getIconColText(COLUMN_2),pRec->getFuncID());	//#4258 skickar med FuncID
								if (pDlg->DoModal() == IDOK)
								{
									pRec->setIconColText(COLUMN_2,pDlg->getSelectedCalcBasis());
								}
								delete pDlg;
								getReportView()->GetReportCtrl().Populate();
								getReportView()->GetReportCtrl().UpdateWindow();
							}
						}
					}
				}	// if (hitTest_X(pt.x,rect.left,13))
			}
			break;
	};

}

void CLogScaleTemplatesFormView::doSave()
{
	m_bOnlySave = FALSE;
	createData();
}

void CLogScaleTemplatesFormView::doSetNavigationButtons()
{
	setNavigationButtons(m_nSelectedIndex > 0,m_nSelectedIndex < (m_vecLogScaleTemplates.size()-1));
}


void CLogScaleTemplatesFormView::New()
{
	CString sName = L"",sPrl = L"",sDate = L"";

	if (m_vecVecPricelist.size() == 0)
	{
		::MessageBox(GetSafeHwnd(),m_sMsgNoPricelist,m_sMsgCap,MB_ICONSTOP | MB_OK);
		return;
	}

	if (m_vecLogScaleTemplates.size() > 0)
	{
		if (!Save())
			return;
//		createData();
	}

	
	CNewTemplateDlg *pDlg = new CNewTemplateDlg();
	if (pDlg != NULL)
	{

		pDlg->setData(m_vecVecPricelist);
		if (pDlg->DoModal() == IDOK)
		{
			m_bEnableToolBarBtnChangePricelist = FALSE;
			m_bEnableToolBarBtnExportTmpl = FALSE;

			m_recLogScaleTemplates = CLogScaleTemplates();

			if (pDlg->isSelected())
			{
				enableView(TRUE);
				clearView();
				
				m_vecLogScaleTemplates.push_back(CLogScaleTemplates(-1,pDlg->getName(),
																															 m_edit8_2.getText(),
																															 m_csDate,
																															 -1,
																															 -1,
																															 -1,
																															 -1,
																															 -1,
																															 -1,
																															 -1,
																															 -1,
																															 L"",
																															 pDlg->getPrl(),
																															 pDlg->getPrlID(),
																															 pDlg->getPrlName(),
																															 0.0,
																															 0.0,
																															 -1,
																															 -1,
																															 -1));
				m_nSelectedIndex = m_vecLogScaleTemplates.size() - 1;
				m_recLogScaleTemplates.setPrlName(pDlg->getPrlName());
				populateData();
			
				m_bEnableToolBarBtnSave = TRUE;
				m_bEnableToolBarBtnDelete = TRUE;
				m_bEnableToolBarBtnTools = TRUE;

				m_edit8_1.SetFocus();
			}
		}
	}

}

BOOL CLogScaleTemplatesFormView::Save()
{
	BOOL bReturn = FALSE;
	QUIT_TYPES::Q_T_RETURN cst;
	if (cst = checkDataBeforeSave(CHECK_SAVE_TYPES::CHECK_SAVE_NO_QUIT))
	{
		if (cst == QUIT_TYPES::DO_SAVE)
		{
			createData();
			bReturn = TRUE;
		}
		else if (cst == QUIT_TYPES::QUIT_ANYWAY)
		{
			bReturn = TRUE;
		}

	}

	return bReturn;
}

void CLogScaleTemplatesFormView::Delete()
{
	CString sMsg;
	sMsg.Format(L"%s : %s",m_sMsgRemoveTable1,m_edit8_1.getText());
	if (m_pDB != NULL)
	{
		// Check if contract is in use
		if (m_pDB->isTmplInUse(m_recLogScaleTemplates.getTmplID()))
		{
			::MessageBox(GetSafeHwnd(),m_sMsgCanNotRemove,m_sMsgCap,MB_ICONSTOP | MB_OK);
			return;
		}

		if (::MessageBox(GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONSTOP | MB_YESNO | MB_DEFBUTTON2) == IDYES)
		{
			if (::MessageBox(GetSafeHwnd(),m_sMsgRemoveTable2,m_sMsgCap,MB_ICONSTOP | MB_YESNO | MB_DEFBUTTON2) == IDYES)
			{
				if (m_pDB->delTmplTable(m_recLogScaleTemplates))
				{
					m_pDB->getTmplTables(m_vecLogScaleTemplates);
					m_nSelectedIndex = m_vecLogScaleTemplates.size()-1;
					populateData();
					// Check if table is empty
					if (m_vecLogScaleTemplates.size() == 0)
					{
						m_bEnableToolBarBtnSave = FALSE;
						m_bEnableToolBarBtnDelete = FALSE;
						m_bEnableToolBarBtnTools = FALSE;

						// Remove Tabs
						removeSpecies(CLogScaleTemplatesFormView::DEL_ALL_SPECIES);

						clearView();

						enableView(FALSE);

						// Reseed IDENTITY-field (i.e. reset)
						m_pDB->resetTmplTableIdentityField();
					}
					setNavigationButtons(m_nSelectedIndex > 0,m_nSelectedIndex < (m_vecLogScaleTemplates.size()-1));
					m_bEnableToolBarBtnExport = m_vecLogScaleTemplates.size() > 0;
					m_bEnableToolBarBtnSave = m_vecLogScaleTemplates.size() > 0;
					m_bEnableToolBarBtnDelete = m_vecLogScaleTemplates.size() > 0;
					m_bEnableToolBarBtnTools = m_vecLogScaleTemplates.size() > 0;

					msgToModuleWindowOpen(L"Module6514",0);

				}
			}
		}
	}	// if (m_pDB != NULL)
}

BOOL CLogScaleTemplatesFormView::ChangeUpdPricelist()
{
	CString sName = L"",sPrl = L"",sDate = L"";
	CNewTemplateDlg *pDlg = new CNewTemplateDlg(NULL,1);
	if (pDlg != NULL)
	{
		pDlg->setTemplateName(m_edit8_1.getText());
		pDlg->setNameOfSelectedPricelist(m_recLogScaleTemplates.getPrlName());
		pDlg->setData(m_vecVecPricelist);
		if (pDlg->DoModal() == IDOK)
		{
			if (pDlg->isSelected())
			{
				if (m_pDB != NULL)
				{
					m_pDB->updTmplTablePricelist(m_recLogScaleTemplates.getTmplID(),pDlg->getPrlID(),pDlg->getPrlName(),pDlg->getPrl());
					m_pDB->getTmplTables(m_vecLogScaleTemplates);
					populateData();

					msgToModuleWindowOpen(L"Module6514",0);
				}
			}
		}
	}

	return TRUE;
}

void CLogScaleTemplatesFormView::ExportTemplate()
{
	showFormView(IDD_FORMVIEW23,m_sLangFN,0,ID_SHOWVIEW_MSG);
}

int CLogScaleTemplatesFormView::getGradesIDFromName(LPCTSTR namn)
{
	CString sTmp(namn);
	if (m_vecGrades.size() > 0)
	{
		for (UINT i = 0;i < m_vecGrades.size();i++)
		{
			if (sTmp.CompareNoCase(m_vecGrades[i].getGradesCode()) == 0)
				return m_vecGrades[i].getGradesID();
		}
	}
	return -1;
}

CString CLogScaleTemplatesFormView::getGradeCode(int id)
{
	if (m_vecGrades.size() > 0)
	{
		for (UINT i = 0;i < m_vecGrades.size();i++)
		{
			if (m_vecGrades[i].getGradesID() == id )
				return m_vecGrades[i].getGradesCode();
		}
	}
	return L"";
}


int CLogScaleTemplatesFormView::getFuncIDFromName(LPCTSTR namn)
{
	CString sTmp(namn);
	if (m_vecFuncDesc.size() > 0)
	{
		for (UINT i = 0;i < m_vecFuncDesc.size();i++)
		{
			if (sTmp.CompareNoCase(m_vecFuncDesc[i].getBasis()) == 0)
				return m_vecFuncDesc[i].getFuncID();
		}
	}
	return -1;
}

int CLogScaleTemplatesFormView::getGradesIDFromCode(LPCTSTR code)
{
	CString sTmp(code);
	if (m_vecGrades.size() > 0)
	{
		for (UINT i = 0;i < m_vecGrades.size();i++)
		{
			if (sTmp.CompareNoCase(m_vecGrades[i].getGradesCode()) == 0)
				return m_vecGrades[i].getGradesID();
		}
	}
	return -1;
}

int CLogScaleTemplatesFormView::getGradesIsPulpwood(LPCTSTR code)
{
	CString sTmp(code);
	if (m_vecGrades.size() > 0)
	{
		for (UINT i = 0;i < m_vecGrades.size();i++)
		{
			if (sTmp.CompareNoCase(m_vecGrades[i].getGradesCode()) == 0)
				return m_vecGrades[i].getIsPulpwood();
		}
	}
	return 0;
}

CString CLogScaleTemplatesFormView::getGradesNameFromCode(LPCTSTR code)
{
	CString sTmp(code);
	if (m_vecGrades.size() > 0)
	{
		for (UINT i = 0;i < m_vecGrades.size();i++)
		{
			if (sTmp.CompareNoCase(m_vecGrades[i].getGradesCode()) == 0)
				return m_vecGrades[i].getGradesName();
		}
	}
	return L"";
}


// Skapa data-filen som sparas i databasen
void CLogScaleTemplatesFormView::createData(BOOL only_create)
{
	CString S;
	int nFuncID = -1,nSelDefSpc = m_cbox8_11.GetCurSel(),nSelDefGrade = m_cbox8_13.GetCurSel();
	CStringArray sarr;	// Get column-data (e.g. length(s) etc.)
	CString sDataUVTables = L"",sTmp = L"",sDate = L"";
	CXTPReportRows *pRows = NULL;
	CPricelistTmplReportRec *pRec = NULL;
	CReportPricelistView *pSelectedView = NULL;

	UpdateData(TRUE);

	if (m_wndTabControl.getNumOfTabPages() > 0)
	{
		for (int nTab = 0;nTab < m_wndTabControl.getNumOfTabPages();nTab++)
		{
			if ((pSelectedView = (CReportPricelistView*)getReportView(nTab)) != NULL)
			{

				// * Species *
				if (nTab >= 0 && nTab < m_mapSpcID.size())
				{
					CSpecies recSpc = CSpecies();
					getSpecies(m_mapSpcID[nTab],recSpc);
					sTmp.Format(L"%d;%d;%s;%s;%.1f;%.0f;",FUNC_INDEX::SPECIES,m_mapSpcID[nTab],recSpc.getSpcCode(),recSpc.getSpcName(),recSpc.getBarkReductionInch(),recSpc.getBarkReductionMM());
					sarr.Add(sTmp);
				}
			
				// * Pricelist for specie *
				pRows = pSelectedView->GetReportCtrl().GetRows();
				if (pRows != NULL)
				{
					for (int ii = 0;ii < pRows->GetCount();ii++)
					{
					
						if ((pRec = (CPricelistTmplReportRec*)pRows->GetAt(ii)->GetRecord()) != NULL)
						{
							//nFuncID = getFuncIDFromName(pRec->getIconColText(COLUMN_2));	
							nFuncID = pRec->getFuncID();	//#4258 �ndrat anv�nder FuncID ist�llet
							if(nFuncID <= 0)
							{
								//blir inte riktigt bra n�r man g�r �ver fr�n gammal databas, kolla om kan h�mta ut FuncID fr�n basisnamnet ist�llet, finns liten risk att det kan bli fel, g�ra om?
								nFuncID = getFuncIDFromName(pRec->getIconColText(COLUMN_2));
							}
							if (!only_create)
							{
								sTmp.Format(L"%d;%d;%d;%.3f;%d;",
									FUNC_INDEX::DATA,
									m_mapSpcID[nTab],
									getGradesIDFromName(pRec->getColText(COLUMN_0)),
									pRec->getColDbl(COLUMN_1),
									nFuncID);
							}
							else
							{
								sTmp.Format(L"%d;%d;%d;%.3f;%d;%s;%s;%d;",
									FUNC_INDEX::DATA,
									m_mapSpcID[nTab],
									getGradesIDFromCode(pRec->getColText(COLUMN_0)),
									pRec->getColDbl(COLUMN_1),
									nFuncID,
									pRec->getColText(COLUMN_0),
									getGradesNameFromCode(pRec->getColText(COLUMN_0)),
									getGradesIsPulpwood(pRec->getColText(COLUMN_0)));
							}
							sarr.Add(sTmp);
						}	// if ((pRec = (CPricelistTmplReportRec*)pRows->GetAt(ii)->GetRecord()) != NULL)										
					}	// for (int ii = 0;ii < pRows->GetCount();ii++)
				}	// if (pRows != NULL)

			}	// if ((pSelectedView = (CReportSpcView*)getReportSpcView(i)) != NULL)
		}	// for (int i = 0;i < m_wndTabControl.getNumOfTabPages();i++)

		m_sDataPrl.Empty();
		for (int i1 = 0;i1 < sarr.GetCount();i1++)
			m_sDataPrl += sarr.GetAt(i1) + L"\n";

		if (only_create) return;

		if (m_pDB != NULL)
		{
			if (m_recLogScaleTemplates.getTmplID() < 0)
			{
				
				m_pDB->addTmplTable(CLogScaleTemplates(-1,
																							m_edit8_1.getText(),
																							m_edit8_2.getText(),
																							m_csDate,
																							cbGetID(&m_cbox8_2),
																							cbGetID(&m_cbox8_3),
																							cbGetID(&m_cbox8_4),
																							cbGetID(&m_cbox8_5),
																							cbGetID(&m_cbox8_6),
																							cbGetID(&m_cbox8_7),
																							cbGetID(&m_cbox8_8),
																							cbGetID(&m_cbox8_9),
																							m_edit8_5.getText(),
																							m_sDataPrl,
																							m_recLogScaleTemplates.getPrlID(),
																							m_recLogScaleTemplates.getPrlName(),
																							m_edit8_3.getFloat(),
																							m_edit8_4.getFloat(),
																							m_cbox8_10.GetCurSel(),
																							(m_cbox8_11.GetCurSel() >= 0 && m_cbox8_11.GetCurSel() < vecSpeciesInPricelist.size() ?  vecSpeciesInPricelist[m_cbox8_11.GetCurSel()].nID : -1),
																							(m_cbox8_13.GetCurSel() >= 0 && m_cbox8_13.GetCurSel() < vecGradesInPricelist.size() ?  vecGradesInPricelist[m_cbox8_13.GetCurSel()].nID : -1)));
				
				m_pDB->getTmplTables(m_vecLogScaleTemplates);
				m_nSelectedIndex = m_vecLogScaleTemplates.size()-1;
			}
			else
			{
				m_pDB->updTmplTable(CLogScaleTemplates(m_recLogScaleTemplates.getTmplID(),
																							m_edit8_1.getText(),
																							m_edit8_2.getText(),
																							m_csDate,
																							cbGetID(&m_cbox8_2),
																							cbGetID(&m_cbox8_3),
																							cbGetID(&m_cbox8_4),
																							cbGetID(&m_cbox8_5),
																							cbGetID(&m_cbox8_6),
																							cbGetID(&m_cbox8_7),
																							cbGetID(&m_cbox8_8),
																							cbGetID(&m_cbox8_9),
																							m_edit8_5.getText(),
																							m_sDataPrl,
																							m_recLogScaleTemplates.getPrlID(),
																							m_recLogScaleTemplates.getPrlName(),
																							m_edit8_3.getFloat(),
																							m_edit8_4.getFloat(),
																							m_cbox8_10.GetCurSel(),
																							(m_cbox8_11.GetCurSel() >= 0 && m_cbox8_11.GetCurSel() < vecSpeciesInPricelist.size() ?  vecSpeciesInPricelist[m_cbox8_11.GetCurSel()].nID : -1),
																							(m_cbox8_13.GetCurSel() >= 0 && m_cbox8_13.GetCurSel() < vecGradesInPricelist.size() ?  vecGradesInPricelist[m_cbox8_13.GetCurSel()].nID : -1)));



				m_pDB->getTmplTables(m_vecLogScaleTemplates);
			}

			if (m_nSelectedIndex >= 0 && m_nSelectedIndex < m_vecLogScaleTemplates.size())
				m_recLogScaleTemplates	= m_vecLogScaleTemplates[m_nSelectedIndex];
	
			setNavigationButtons(m_nSelectedIndex > 0,m_nSelectedIndex < (m_vecLogScaleTemplates.size()-1));

			// Send message to windows; Buyer,Hauler,Location etc.
			// If any of them's open, we'll refresh data.
			msgToModuleWindowOpen(L"Module6505",0);
			msgToModuleWindowOpen(L"Module6506",0);
			msgToModuleWindowOpen(L"Module6507",0);
			msgToModuleWindowOpen(L"Module6508",0);
			msgToModuleWindowOpen(L"Module6509",0);
			msgToModuleWindowOpen(L"Module6510",0);
			msgToModuleWindowOpen(L"Module6511",0);
			msgToModuleWindowOpen(L"Module6512",0);

			msgToModuleWindowOpen(L"Module6514",0);
			
			m_bEnableToolBarBtnChangePricelist = TRUE;
			m_bEnableToolBarBtnExportTmpl = TRUE;

		}
	}	// if (m_wndTabControl.getNumOfTabPages() > 0)

}

void CLogScaleTemplatesFormView::addSpeciesToTabs()
{
	CString sSpc = L"";
	if (m_vecSpeciesSelected.size() > 0)
	{
		for (UINT i = 0;i < m_vecSpeciesSelected.size();i++)
		{
			if (!m_vecSpeciesSelected[i].getSpcName().IsEmpty())
				sSpc.Format(L"%s - %s",m_vecSpeciesSelected[i].getSpcCode(),m_vecSpeciesSelected[i].getSpcName());
			else
				sSpc.Format(L"%s",m_vecSpeciesSelected[i].getSpcCode());

			// Check that spceis isn't already in TabControl
			if (!isAlreadyOnTab(sSpc))
			{
				AddView(RUNTIME_CLASS(CReportPricelistView), sSpc,-1,m_vecSpeciesSelected[i].getSpcID());
				addGradesToSpecies();
			}
		}
		if (m_nSelectedSpeciesTab >= 0 && m_nSelectedSpeciesTab < m_wndTabControl.getNumOfTabPages())
			m_wndTabControl.SetCurSel(m_nSelectedSpeciesTab);
	}
}

void CLogScaleTemplatesFormView::removeSpecies(DEL_SPECIES del_species)
{
	CString sMsg = L"";
	CXTPTabManagerItem *pItem = NULL;

	if (del_species == CLogScaleTemplatesFormView::DEL_SELECTED_SPECIES)
	{
		CXTPTabManagerItem *pItem = m_wndTabControl.getSelectedTabPage();
		if (pItem != NULL)
		{
			sMsg.Format(m_sMsgRemoveSpecies1,pItem->GetCaption());
			if (::MessageBox(GetSafeHwnd(),sMsg,m_sMsgCap,MB_ICONSTOP | MB_YESNO | MB_DEFBUTTON2) == IDYES)
			{
				if (::MessageBox(GetSafeHwnd(),m_sMsgRemoveSpecies2,m_sMsgCap,MB_ICONSTOP | MB_YESNO | MB_DEFBUTTON2) == IDYES)
				{
					pItem->Remove();
				}
			}
		}
	}
	else if (del_species == CLogScaleTemplatesFormView::DEL_ALL_SPECIES)
	{
		int nNumOfSpecies = m_wndTabControl.getNumOfTabPages();
		if (nNumOfSpecies > 0)
		{
			for (int i = 0;i < nNumOfSpecies;i++)
			{
				CXTPTabManagerItem *pItem = m_wndTabControl.getTabPage(0);
				if (pItem != NULL)
				{
					pItem->Remove();
				}
			}
		}
	}
}

void CLogScaleTemplatesFormView::doPopulate(int tmpl_id)
{
	if (m_vecLogScaleTemplates.size() > 0)
	{
		for (UINT i = 0;i < m_vecLogScaleTemplates.size();i++)
		{
			if (m_vecLogScaleTemplates[i].getTmplID() == tmpl_id)
			{
				m_nSelectedIndex = i;
				populateData();
				doSetNavigationButtons();
			}
		}
	}

}

void CLogScaleTemplatesFormView::doPopulate(CListContract& rec)
{
	if (m_vecLogScaleTemplates.size() > 0)
	{
		for (UINT i = 0;i < m_vecLogScaleTemplates.size();i++)
		{
			if (m_vecLogScaleTemplates[i].getTmplID() == rec.getPKID())
			{
				m_nSelectedIndex = i;
				populateData();
				doSetNavigationButtons();
			}
		}
	}

}

BOOL CLogScaleTemplatesFormView::isAlreadyOnTab(LPCTSTR spc)
{
	if (m_wndTabControl.getNumOfTabPages() > 0)
	{
		for (int i = 0;i < m_wndTabControl.getNumOfTabPages();i++)
		{
			CXTPTabManagerItem *pManagerItem =	m_wndTabControl.getTabPage(i);
			if (pManagerItem != NULL)
			{
				if (pManagerItem->GetCaption().CompareNoCase(spc) == 0)
					return TRUE;
			}
		}
	}
	return FALSE;

}

void CLogScaleTemplatesFormView::addGradesToSpecies()
{
	int nSpcID = -1,nDefWood = -1;
	int nNumOfTabs = m_wndTabControl.getNumOfTabPages();
	CReportPricelistView *pSelectedView = getReportView(nNumOfTabs-1);
	if (pSelectedView != NULL)
	{
		pSelectedView->SendMessage(WM_SETREDRAW,FALSE,0);
		int nTabIndex = m_wndTabControl.getTabPage(nNumOfTabs-1)->GetIndex();
		if (m_vecGrades.size() > 0)
		{
			for (UINT i = 0;i < m_vecGrades.size();i++)
			{
				if (nTabIndex >= 0 && nTabIndex < m_mapSpcID.size())
					nSpcID = m_mapSpcID[nTabIndex];
				nDefWood = ((m_vecGrades[i].getIsPulpwood() == 0) ? DEF_SAWLOG : DEF_PULPWOOD);

				pSelectedView->GetReportCtrl().AddRecord(new CPricelistTmplReportRec(m_vecGrades[i].getGradesCode(),0.0,getDefaults(nDefWood,m_vecDefaults,m_vecFuncDesc),getDefaultFuncID(nDefWood,m_vecDefaults)));	//#4258 lagt till FuncID ox�
			}
		}
		pSelectedView->GetReportCtrl().Populate();
		pSelectedView->GetReportCtrl().UpdateWindow();
		pSelectedView->SendMessage(WM_SETREDRAW,TRUE,0);
	}
}

void CLogScaleTemplatesFormView::setupReport()
{
	if (fileExists(m_sLangFN))
	{
		RLFReader xml;
		if (xml.Load(m_sLangFN))
		{

			m_lbl8_1.SetWindowTextW(xml.str(IDS_STRING44000));
			m_lbl8_2.SetWindowTextW(xml.str(IDS_STRING44001));
			m_lbl8_3.SetWindowTextW(xml.str(IDS_STRING44002));
			m_lbl8_4.SetWindowTextW(xml.str(IDS_STRING44003));
			m_lbl8_5.SetWindowTextW(xml.str(IDS_STRING44004));
			m_lbl8_6.SetWindowTextW(xml.str(IDS_STRING44005));
			m_lbl8_7.SetWindowTextW(xml.str(IDS_STRING44006));
			m_lbl8_8.SetWindowTextW(xml.str(IDS_STRING44007));
			m_lbl8_9.SetWindowTextW(xml.str(IDS_STRING44008));
			m_lbl8_10.SetWindowTextW(xml.str(IDS_STRING44009));
			m_lbl8_11.SetWindowTextW(xml.str(IDS_STRING44010));
			m_lbl8_12.SetWindowTextW(xml.str(IDS_STRING44011));
			m_lbl8_19.SetWindowTextW(xml.str(IDS_STRING44013));
			m_lbl8_20.SetWindowTextW(xml.str(IDS_STRING44014));
			m_lbl8_13.SetWindowTextW(xml.str(IDS_STRING44015));

			m_wndGrp8_1.SetWindowTextW(xml.str(IDS_STRING4410));
			m_lbl8_15.SetWindowTextW(xml.str(IDS_STRING4411));
			m_lbl8_16.SetWindowTextW(xml.str(IDS_STRING4412));
			m_lbl8_17.SetWindowTextW(xml.str(IDS_STRING4413));

			xml.clean();
		}	// if (xml.Load(m_sLangFN))
	}	// if (fileExists(m_sLangFN))
}

void CLogScaleTemplatesFormView::enableView(BOOL enable)
{
	m_edit8_1.EnableWindow(enable);
	m_edit8_1.SetReadOnly(!enable);
	m_edit8_2.EnableWindow(enable);
	m_edit8_2.SetReadOnly(!enable);
	m_edit8_3.EnableWindow(enable);
	m_edit8_3.SetReadOnly(!enable);
	m_edit8_4.EnableWindow(enable);
	m_edit8_4.SetReadOnly(!enable);
	m_edit8_5.EnableWindow(enable);
	m_edit8_5.SetReadOnly(!enable);
	m_dtDate.EnableWindow(enable);
	m_cbox8_2.EnableWindow(enable);
	m_cbox8_3.EnableWindow(enable);
	m_cbox8_4.EnableWindow(enable);
	m_cbox8_5.EnableWindow(enable);
	m_cbox8_6.EnableWindow(enable);
	m_cbox8_7.EnableWindow(enable);
	m_cbox8_8.EnableWindow(enable);
	m_cbox8_9.EnableWindow(enable);
	m_cbox8_10.EnableWindow(enable);
	m_cbox8_11.EnableWindow(enable);
	m_cbox8_13.EnableWindow(enable);
	m_lbl8_14.EnableWindow(enable);
	m_wndTabControl.EnableWindow(enable);
}

void CLogScaleTemplatesFormView::clearView()
{
	m_edit8_1.SetWindowTextW(L"");
	m_edit8_2.SetWindowTextW(getUserName().MakeUpper());
	m_edit8_3.SetWindowTextW(L"");
	m_edit8_4.SetWindowTextW(L"");
	m_edit8_5.SetWindowTextW(L"");
	m_cbox8_2.SetCurSel(-1);
	m_cbox8_3.SetCurSel(-1);
	m_cbox8_4.SetCurSel(-1);
	m_cbox8_5.SetCurSel(-1);
	m_cbox8_6.SetCurSel(-1);
	m_cbox8_7.SetCurSel(-1);
	m_cbox8_8.SetCurSel(-1);
	m_cbox8_9.SetCurSel(-1);
	m_cbox8_10.SetCurSel(-1);
	m_cbox8_11.SetCurSel(-1);
	m_cbox8_13.SetCurSel(-1);
	m_lbl8_14.SetWindowTextW(L"");
	removeSpecies(CLogScaleTemplatesFormView::DEL_ALL_SPECIES);
}

CReportPricelistView *CLogScaleTemplatesFormView::getReportView(void)
{
	CXTPTabManagerItem *pManager = m_wndTabControl.getSelectedTabPage();
	if (pManager)
	{
		CReportPricelistView* pView = DYNAMIC_DOWNCAST(CReportPricelistView, CWnd::FromHandle(pManager->GetHandle()));
		ASSERT_KINDOF(CReportPricelistView, pView);
		return pView;
	}
	return NULL;
}

CReportPricelistView *CLogScaleTemplatesFormView::getReportView(int idx)
{
	if (idx >= 0 && idx < m_wndTabControl.getNumOfTabPages())
	{
		CXTPTabManagerItem *pManager = m_wndTabControl.getTabPage(idx);
		if (pManager)
		{
			CReportPricelistView* pView = DYNAMIC_DOWNCAST(CReportPricelistView, CWnd::FromHandle(pManager->GetHandle()));
			ASSERT_KINDOF(CReportPricelistView, pView);
			return pView;
		}
	}
	return NULL;
}

BOOL CLogScaleTemplatesFormView::AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon,int spc_id)
{
	CCreateContext contextT;
	contextT.m_pCurrentDoc     = GetDocument();
	contextT.m_pNewViewClass   = pViewClass;
	contextT.m_pNewDocTemplate = GetDocument()->GetDocTemplate();

	CWnd* pWnd;
	TRY
	{
		pWnd = (CWnd*)pViewClass->CreateObject();
		if (pWnd == NULL)
		{
			AfxThrowMemoryException();
		}
	}
	CATCH_ALL(e)
	{
		TRACE0( "Out of memory creating a view.\n" );
		// Note: DELETE_EXCEPTION(e) not required
		return FALSE;
	}
	END_CATCH_ALL

	DWORD dwStyle = AFX_WS_DEFAULT_VIEW;
	dwStyle &= ~WS_BORDER;

	int nTab = m_wndTabControl.GetItemCount();

	// Create with the right size (wrong position)
	CRect rect(0,0,0,0);
	if (!pWnd->Create(NULL, NULL, dwStyle,rect, &m_wndTabControl, (AFX_IDW_PANE_FIRST + nTab), &contextT))
	{
		TRACE0( "Warning: couldn't create client tab for view.\n" );
		// pWnd will be cleaned up by PostNcDestroy
		return NULL;
	}
	m_wndTabControl.InsertItem(nTab, lpszTitle, pWnd->GetSafeHwnd(), nIcon);
	m_mapSpcID[nTab] = spc_id;

	pWnd->SetOwner(this);

	return TRUE;
}

void CLogScaleTemplatesFormView::OnEnChangeEdit83()
{
	double fTrimFT = m_edit8_3.getFloat();
	if (m_bInitialized)
	{
		m_edit8_3.setDoNotify(true);
		m_edit8_4.setDoNotify(false);
		m_edit8_4.setFloat(fTrimFT*Feet2CM,0);
		m_edit8_4.setDoNotify(true);
	}
}

void CLogScaleTemplatesFormView::OnEnChangeEdit84()
{
	double fTrimCM = m_edit8_4.getFloat();
	if (m_bInitialized)
	{
		m_edit8_4.setDoNotify(true);
		m_edit8_3.setDoNotify(false);
		m_edit8_3.setFloat(fTrimCM*CM2Feet,1);
		m_edit8_3.setDoNotify(true);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////
// CReportPricelistView

IMPLEMENT_DYNCREATE(CReportPricelistView,  CXTPReportView)

BEGIN_MESSAGE_MAP(CReportPricelistView, CXTPReportView)
	ON_COMMAND_RANGE(ID_BUTTON32807,ID_BUTTON32814, OnCommand)
	ON_NOTIFY(NM_CLICK, XTP_ID_REPORT_CONTROL, OnReportItemClick)
END_MESSAGE_MAP()


CReportPricelistView::CReportPricelistView()
{
	CString sLangFN(getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV));
	if (fileExists(sLangFN))
	{
		RLFReader xml;
		if (xml.Load(sLangFN))
		{

			VERIFY(m_ilIcons.Create(16,13, ILC_COLOR24|ILC_MASK, 0, 1));
			CBitmap bmp;
			VERIFY(bmp.LoadBitmap(IDB_BITMAP1));
			m_ilIcons.Add(&bmp, RGB(255, 0, 255));
			GetReportCtrl().SetImageList(&m_ilIcons);

			// Grade code
			CXTPReportColumn *pCol = NULL;
			pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_0,xml.str(IDS_STRING4430),90,FALSE));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_LEFT);
			pCol->SetAlignment(DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = FALSE;

#ifdef _LOCK_PRICELIST_IN_TEMPLATES
			// Prices
			pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_1,xml.str(IDS_STRING4431),120,FALSE));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_LEFT);
			pCol->SetAlignment(DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = FALSE;
			pCol->GetEditOptions()->m_bSelectTextOnEdit = FALSE;
#else
			// Prices
			pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_1,xml.str(IDS_STRING4431),120,FALSE));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_LEFT);
			pCol->SetAlignment(DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = TRUE;
			pCol->GetEditOptions()->m_bSelectTextOnEdit = TRUE;
#endif
			// Basis for calculation
			pCol = GetReportCtrl().AddColumn(new CXTPReportColumn(COLUMN_2,xml.str(IDS_STRING4432),150,FALSE));
			pCol->AllowRemove(FALSE);
			pCol->SetHeaderAlignment(DT_LEFT);
			pCol->SetAlignment(DT_LEFT);
			pCol->GetEditOptions()->m_bAllowEdit = FALSE;

			xml.clean();
		}
	}

	GetReportCtrl().GetReportHeader()->AllowColumnRemove(FALSE);
	GetReportCtrl().GetReportHeader()->SetAutoColumnSizing( FALSE );
	GetReportCtrl().SetMultipleSelection( FALSE );
	GetReportCtrl().SetGridStyle( TRUE, xtpReportGridSolid );
	GetReportCtrl().SetGridStyle( FALSE, xtpReportGridSmallDots );
	GetReportCtrl().FocusSubItems(TRUE);
	GetReportCtrl().AllowEdit(TRUE);
	GetReportCtrl().GetPaintManager()->SetFixedRowHeight(FALSE);

}

CReportPricelistView::~CReportPricelistView()
{
}

void CReportPricelistView::OnCommand(UINT nID)	
{ 
	CLogScaleTemplatesFormView *pView = (CLogScaleTemplatesFormView*)getFormViewByID(IDD_FORMVIEW8);
	if (pView != NULL)
	{
		::SendMessage(pView->GetSafeHwnd(),WM_COMMAND,nID,0);
	}
}

void CReportPricelistView::OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/)
{
	XTP_NM_REPORTRECORDITEM* pItemNotify = (XTP_NM_REPORTRECORDITEM*) pNotifyStruct;
	// prevents us from crashing when clicking inside the report, but not on an item
	if(pItemNotify == NULL) 
		return;

	// prevents us from crashing when clicking inside the report, but not on an item
	if(pItemNotify->pItem == NULL)
		return;

	CLogScaleTemplatesFormView *pView = (CLogScaleTemplatesFormView*)getFormViewByID(IDD_FORMVIEW8);
	if (pView != NULL)
	{
		pView->doReportViewClick(pNotifyStruct);
	}
}
