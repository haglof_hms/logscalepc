// dllmain.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include <afxwin.h>
#include <afxdllx.h>

#include "Resource.h"

#include "CreateTables.h"

#include "LogScaleView.h"	// �ven doc och frame
#include "TicketLogsFormView.h"	// �ven doc och frame
#include "LogSalesFormView.h"	// �ven doc och frame
#include "UserCreateVolFunc.h"
#include "LogScaleTemplatesFormView.h"
#include "CreatePricelistsFormView.h"
#include "registerbuyerview.h"
#include "registerhaulerview.h"
#include "RegisterLocationsView.h"
#include "RegisterTractIDsView.h"
#include "RegisterScalersView.h"
#include "RegisterSourceIDsView.h"
#include "RegisterVendorsView.h"
#include "RegisterSuppliersView.h"
#include "TemplateExportView.h"
#include "logsalesview.h"
#include "ListPrlReportView.h"
#include "ListContractReportView.h"
#include "ListUserVTReportView.h"

#include "StorageSearchReportView.h"

#include "DefaultsView.h"

#include "SpeciesDlg.h"
#include "GradesDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define __BUILD

#ifdef __BUILD
#define DLL_BUILD __declspec(dllexport)
#else
#define DLL_BUILD __declspec(dllimport)
#endif

/////////////////////////////////////////////////////////////////////////////
// Initialization of MFC Extension DLL

HINSTANCE hInst = NULL;

//////////////////////////////////////////////////////////////////////////////////////////
// External data
CString m_sShellDataFile = L"";

// Added: EXPORTED function create tables in database; 080131 p�d
extern "C" AFX_EXT_API void DoDatabaseTables(LPCTSTR);

extern "C" AFX_EXT_API void DoAlterTables(LPCTSTR);

extern "C" AFX_EXT_API void InitModule(CWinApp *app,LPCTSTR suite,vecINDEX_TABLE &,vecINFO_TABLE &);

static AFX_EXTENSION_MODULE LogScalePCDLL = { NULL, NULL };

extern "C" int APIENTRY
DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("LogScalePC.DLL Initializing!\n");
		
		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(LogScalePCDLL, hInstance))
			return 0;

		// Insert this DLL into the resource chain
		// NOTE: If this Extension DLL is being implicitly linked to by
		//  an MFC Regular DLL (such as an ActiveX Control)
		//  instead of an MFC application, then you will want to
		//  remove this line from DllMain and put it in a separate
		//  function exported from this Extension DLL.  The Regular DLL
		//  that uses this Extension DLL should then explicitly call that
		//  function to initialize this Extension DLL.  Otherwise,
		//  the CDynLinkLibrary object will not be attached to the
		//  Regular DLL's resource chain, and serious problems will
		//  result.

		new CDynLinkLibrary(LogScalePCDLL);

	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("LogScalePC.DLL Terminating!\n");

		// Terminate the library before destructors are called
		AfxTermExtensionModule(LogScalePCDLL);
	}
	hInst = hInstance;
	return 1;   // ok
}

void DLL_BUILD InitModule(CWinApp *app,LPCTSTR suite,vecINDEX_TABLE &idx,vecINFO_TABLE &vecInfo)
{
	new CDynLinkLibrary(LogScalePCDLL);
	CString sLangFN;
	CString sVersion;
	CString sCopyright;
	CString sCompany;

	// Setup the searchpath and name of the program.
	// This information is used e.g. in setting up the
	// Language filename in an OpenSuite() function; 051214 p�d
	sLangFN.Format(_T("%s%s"),getLanguageDir(),PROGRAM_NAME);

	// Form view to enter data

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW,
			RUNTIME_CLASS(CLogScaleDoc),
			RUNTIME_CLASS(CLogScaleFrame),
			RUNTIME_CLASS(CLogScaleView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW5,
			RUNTIME_CLASS(CTicketLogsDoc),
			RUNTIME_CLASS(CTicketLogsFrame),
			RUNTIME_CLASS(CTicketLogsFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW5,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW11,
			RUNTIME_CLASS(CLogScaleSalesDoc),
			RUNTIME_CLASS(CLogScaleSalesFrame),
			RUNTIME_CLASS(CLogSalesView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW11,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW6,
			RUNTIME_CLASS(CLogSalesDoc),
			RUNTIME_CLASS(CLogSalesFrame),
			RUNTIME_CLASS(CLogSalesFormView)));
//			RUNTIME_CLASS(CLogSalesReportView)));
//			RUNTIME_CLASS(CLogSalesFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW6,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW7,
			RUNTIME_CLASS(CUserCreateVolFuncDoc),
			RUNTIME_CLASS(CUserCreateVolFuncFrame),
			RUNTIME_CLASS(CUserCreateVolFunc)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW7,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW8,
			RUNTIME_CLASS(CLogScaleTemplatesDoc),
			RUNTIME_CLASS(CLogScaleTemplatesFrame),
			RUNTIME_CLASS(CLogScaleTemplatesFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW8,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW9,
			RUNTIME_CLASS(CCreatePricelistsDoc),
			RUNTIME_CLASS(CCreatePricelistsFrame),
			RUNTIME_CLASS(CCreatePricelistsFormView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW9,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW12,
			RUNTIME_CLASS(CSpeciesDlgDoc),
			RUNTIME_CLASS(CSpeciesDlgFrame),
			RUNTIME_CLASS(CSpeciesDlgView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW12,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW13,
			RUNTIME_CLASS(CGradesDlgDoc),
			RUNTIME_CLASS(CGradesDlgFrame),
			RUNTIME_CLASS(CGradesDlgView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW13,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW14,
			RUNTIME_CLASS(CRegisterBuyerDoc),
			RUNTIME_CLASS(CRegisterBuyerFrame),
			RUNTIME_CLASS(CRegisterBuyerView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW14,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW15,
			RUNTIME_CLASS(CRegisterHaulerDoc),
			RUNTIME_CLASS(CRegisterHaulerFrame),
			RUNTIME_CLASS(CRegisterHaulerView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW15,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW16,
			RUNTIME_CLASS(CRegisterLocationsDoc),
			RUNTIME_CLASS(CRegisterLocationsFrame),
			RUNTIME_CLASS(CRegisterLocationsView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW16,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW17,
			RUNTIME_CLASS(CRegisterTractIDsDoc),
			RUNTIME_CLASS(CRegisterTractIDsFrame),
			RUNTIME_CLASS(CRegisterTractIDsView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW17,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW18,
			RUNTIME_CLASS(CRegisterScalersDoc),
			RUNTIME_CLASS(CRegisterScalersFrame),
			RUNTIME_CLASS(CRegisterScalersView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW18,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW19,
			RUNTIME_CLASS(CRegisterSourceIDsDoc),
			RUNTIME_CLASS(CRegisterSourceIDsFrame),
			RUNTIME_CLASS(CRegisterSourceIDsView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW19,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW20,
			RUNTIME_CLASS(CRegisterVendorsDoc),
			RUNTIME_CLASS(CRegisterVendorsFrame),
			RUNTIME_CLASS(CRegisterVendorsView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW20,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW21,
			RUNTIME_CLASS(CRegisterSuppliersDoc),
			RUNTIME_CLASS(CRegisterSuppliersFrame),
			RUNTIME_CLASS(CRegisterSuppliersView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW21,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW22,
			RUNTIME_CLASS(CDefaultsDoc),
			RUNTIME_CLASS(CDefaultsFrame),
			RUNTIME_CLASS(CDefaultsView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW22,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_FORMVIEW23,
			RUNTIME_CLASS(CTemplateExportDoc),
			RUNTIME_CLASS(CTemplateExportFrame),
			RUNTIME_CLASS(CTemplateExportView)));
	idx.push_back(INDEX_TABLE(IDD_FORMVIEW23,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(ID_LIST_PRL,
			RUNTIME_CLASS(CListPrlDoc),
			RUNTIME_CLASS(CListPrlFrame),
			RUNTIME_CLASS(CListPrlView)));
	idx.push_back(INDEX_TABLE(ID_LIST_PRL,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(ID_LIST_CONTRACT,
			RUNTIME_CLASS(CListContractDoc),
			RUNTIME_CLASS(CListContractFrame),
			RUNTIME_CLASS(CListContractView)));
	idx.push_back(INDEX_TABLE(ID_LIST_CONTRACT,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(ID_LIST_USER_VTABLE,
			RUNTIME_CLASS(CListUserVTDoc),
			RUNTIME_CLASS(CListUserVTFrame),
			RUNTIME_CLASS(CListUserVTView)));
	idx.push_back(INDEX_TABLE(ID_LIST_USER_VTABLE,suite,sLangFN,TRUE));

	app->AddDocTemplate(new CMultiDocTemplate(IDD_REPORTVIEW6516,
			RUNTIME_CLASS(CStorageSearchDoc),
			RUNTIME_CLASS(CStorageSearchFrame),
			RUNTIME_CLASS(CStorageSearchView)));
	idx.push_back(INDEX_TABLE(IDD_REPORTVIEW6516,suite,sLangFN,TRUE));

	// Get version information; 060803 p�d
	const LPCTSTR VER_NUMBER						= _T("FileVersion");
	const LPCTSTR VER_COMPANY						= _T("CompanyName");
	const LPCTSTR VER_COPYRIGHT					= _T("LegalCopyright");

	sVersion		= getVersionInfo(hInst,VER_NUMBER);
	sCopyright	= getVersionInfo(hInst,VER_COPYRIGHT);
	sCompany		= getVersionInfo(hInst,VER_COMPANY);
	vecInfo.push_back(INFO_TABLE(-999,2 /* User module */,
															 (TCHAR*)sLangFN.GetBuffer(),
															 (TCHAR*)sVersion.GetBuffer(),
															 (TCHAR*)sCopyright.GetBuffer(),
															 (TCHAR*)sCompany.GetBuffer()));

	// Do a check to see if database tables are created; 080131 p�d
	DoDatabaseTables(_T(""));	// Empty arg = use default database;
	DoAlterTables(_T("")); // Empty arg = use default database;
}

void DoDatabaseTables(LPCTSTR db_name)
{
	// Check if there's a connection set; 070329 p�d
	if (getIsDBConSet() == 1)
	{
		runSQLScriptFileEx(TBL_TICKETS,table_Tickets,db_name);
		runSQLScriptFileEx(TBL_LOGS,table_Logs,db_name);
		runSQLScriptFileEx(TBL_REGISTER,table_Register,db_name);
		runSQLScriptFileEx(TBL_SPECIES,table_Species,db_name);
		runSQLScriptFileEx(TBL_GRADES,table_Grades,db_name);
		runSQLScriptFileEx(TBL_PRICELIST,table_Pricelist,db_name);

		// Not used in new LogScalePC; 120117 p�d
		//runSQLScriptFileEx(TBL_CALCTYPES,table_CalcTypes,db_name);
		runSQLScriptFileEx(TBL_DEFAULTS,table_Defaults,db_name);

		runSQLScriptFileEx(TBL_TICKET_SPC,table_TicketSpc,db_name);
		
		runSQLScriptFileEx(TBL_TICKET_PRL,table_TicketPrl,db_name);

		runSQLScriptFileEx(TBL_USER_VOL_TABLES,table_UserVolTables,db_name);

		runSQLScriptFileEx(TBL_SEL_VOL_FUNCS,table_SelectedVolFunctions,db_name);

		runSQLScriptFileEx(TBL_TEMPLATES,table_Templates,db_name);

		runSQLScriptFileEx(TBL_DEF_USER_VOL_SEL,table_DefaultUserVolTables,db_name);	

		runSQLScriptFileEx(TBL_INVENTORY,table_Invent,db_name);
		runSQLScriptFileEx(TBL_INVENTORY_LOGS,table_Invent_Logs,db_name);
	}
}

void DoAlterTables(LPCTSTR db_name)
{
	vecScriptFiles vec;
	// Check if there's a connection set; 090525 p�d
	if (getIsDBConSet() == 1)
	{
		// Alter table scripts
		vec.push_back(Scripts(TBL_LOGS,alter_Logs120305_1,db_name));
		vec.push_back(Scripts(TBL_TICKETS,alter_Logs120314_1,db_name));
		vec.push_back(Scripts(TBL_TICKETS,alter_Tickets120618_1,db_name));
		vec.push_back(Scripts(TBL_DEFAULTS,alter_Defaults150205_1,db_name));	//#4258 l�gg till FuncID i defaulttabellen om den inte finns

		runSQLScriptFileEx2(vec,Scripts::TBL_ALTER);
		vec.clear();
	}
}
