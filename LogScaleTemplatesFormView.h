#pragma once
#include "DatePickerCombo.h"

#include "DBHandling.h"
#include "afxdtctl.h"



class CMyExtEditEx : public CMyExtEdit
{
	bool m_bNotify;
public:
	CMyExtEditEx() : CMyExtEdit() { m_bNotify = TRUE; }

	void setDoNotify(bool notify) { m_bNotify = notify; }
protected:

	afx_msg BOOL OnEnChange();

	DECLARE_MESSAGE_MAP()

};

///////////////////////////////////////////////////////////////////////////////////////////
// CLogScaleTemplatesDoc

class CLogScaleTemplatesDoc : public CDocument
{
protected: // create from serialization only
	CLogScaleTemplatesDoc();
	DECLARE_DYNCREATE(CLogScaleTemplatesDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLogScaleTemplatesDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CLogScaleTemplatesDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CLogScaleTemplatesDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};



#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
/////////////////////////////////////////////////////////////////////////////
// class CLogScaleTemplatesFrame

class CLogScaleTemplatesFrame : public CChildFrameBase
{
//private:

	DECLARE_DYNCREATE(CLogScaleTemplatesFrame)
	CXTPToolBar m_wndToolBar;
	CXTPDockingPaneManager m_paneManager;
	CString m_sLangFN;

	BOOL m_bFirstOpen;
	BOOL m_bInitReports;
	BOOL m_bIsPrintOutTBtn;

	BOOL m_bIsSysCommand;

	void setNavBarButtons()
	{
		// Send messages to HMSShell, disable buttons on toolbar; 120122 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

		// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 120122 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);
	}
protected:
	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	HICON m_hIcon;
public:
	CLogScaleTemplatesFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

// Attributes

// Operations

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	virtual ~CLogScaleTemplatesFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	
	//{{AFX_MSG(CLogScaleTemplatesFrame)
	afx_msg int OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl);
	afx_msg void OnSysCommand(UINT nID,LPARAM lParam);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnClose();
	afx_msg void OnDestroy(void);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);

	//}}AFX_MSG
	
	DECLARE_MESSAGE_MAP()
};



// CLogScaleTemplatesFormView form view

class CReportPricelistView;

class CLogScaleTemplatesFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CLogScaleTemplatesFormView)

	enum DEL_SPECIES { DEL_ALL_SPECIES, DEL_SELECTED_SPECIES };

	class VDefault 
	{
	public:
		CString sName;
		int nID;

		VDefault()	{ sName = L""; nID = -1; }
		VDefault(LPCTSTR name,int id)	{ sName = name; nID = id; }
	};

	std::vector<VDefault> vecSpeciesInPricelist;
	std::vector<VDefault> vecGradesInPricelist;

	BOOL m_bInitialized;
	CString m_sLangFN;

	CString m_sMsgCap;
	CString m_sMsgRemoveSpecies1;
	CString m_sMsgRemoveSpecies2;
	CString m_sMsgRemoveTable1;
	CString m_sMsgRemoveTable2;
	CString m_sMsgMissingData1;
	CString m_sMsgMissingData2;

	CString m_sMsgMaxUserdefFunctions;

	CString m_sMsgTemplateNameAlreadyUsed1;
	CString m_sMsgTemplateNameAlreadyUsed2;

	CString m_sMsgNoPricelist;

	CString m_sMsgCanNotRemove;

	CString m_sMsgNotAContractFile;


	CMyTabControl m_wndTabControl;

	CDBHandling *m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;

	CVecSpecies m_vecSpecies;
	CVecSpecies m_vecSpeciesSelected;

	CVecGrades m_vecGrades;

	CXTResizeGroupBox m_wndGrp8_1;

	CMyExtStatic m_lbl8_1;
	CMyExtStatic m_lbl8_2;
	CMyExtStatic m_lbl8_3;
	CMyExtStatic m_lbl8_4;
	CMyExtStatic m_lbl8_5;
	CMyExtStatic m_lbl8_6;
	CMyExtStatic m_lbl8_7;
	CMyExtStatic m_lbl8_8;
	CMyExtStatic m_lbl8_9;
	CMyExtStatic m_lbl8_10;
	CMyExtStatic m_lbl8_11;
	CMyExtStatic m_lbl8_12;
	CMyExtStatic m_lbl8_13;
	CMyExtStatic m_lbl8_14;
	CMyExtStatic m_lbl8_15;
	CMyExtStatic m_lbl8_16;
	CMyExtStatic m_lbl8_17;
	CMyExtStatic m_lbl8_19;
	CMyExtStatic m_lbl8_20;

	CMyExtEdit m_edit8_1;
	CMyExtEdit m_edit8_2;
	CMyExtEditEx m_edit8_3;
	CMyExtEditEx m_edit8_4;
	CMyExtEdit m_edit8_5;
	//CMyDatePickerCombo m_dt8_1;

	CComboBox m_cbox8_2;
	CComboBox m_cbox8_3;
	CComboBox m_cbox8_4;
	CComboBox m_cbox8_5;
	CComboBox m_cbox8_6;
	CComboBox m_cbox8_7;
	CComboBox m_cbox8_8;
	CComboBox m_cbox8_9;
	CComboBox m_cbox8_10;
	CComboBox m_cbox8_11;
	CComboBox m_cbox8_13;

	BOOL m_bEnableToolBarBtnSave;
	BOOL m_bEnableToolBarBtnDelete;
	BOOL m_bEnableToolBarBtnTools;
	BOOL m_bEnableToolBarBtnImport;
	BOOL m_bEnableToolBarBtnExport;

	BOOL m_bEnableToolBarBtnChangePricelist;
	BOOL m_bEnableToolBarBtnExportTmpl;

	CLogScaleTemplates m_recLogScaleTemplates;
	vecLogScaleTemplates m_vecLogScaleTemplates;

	CVecRegister m_vecRegister;

	vecFuncDesc m_vecFuncDesc;
	CVecUserVolTables m_vecUserVolTables;
	CVecDefaults m_vecDefaults;

	CPricelist m_recPricelist;
	CVecPricelist m_vecVecPricelist;
	
	CStringArray m_sarrMeasuringMode;

	std::map<int,int> m_mapSpcID;
	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon,int spc_id);

	CReportPricelistView *getReportView(void);
	CReportPricelistView *getReportView(int idx);

	int m_nSelectedIndex;

	int m_nSelectedSpeciesTab;

	BOOL m_bOnlySave;

	CString m_sDataPrl;
	CString m_sImportContractFilter;
	CString m_sExportContractFilter;

	void setupReport();

	void New();
	BOOL Save();
	void Delete();

	BOOL ChangeUpdPricelist();
	void ExportTemplate();

	void Import();
	void Export();
	void PreView();

	void createData(BOOL only_create = FALSE);

	void addSpeciesToTabs();
	BOOL isAlreadyOnTab(LPCTSTR spc);

	void removeSpecies(DEL_SPECIES del_species);

	void addGradesToSpecies();

	void enableView(BOOL enable);

	void clearView();

	void setNavigationButtons(BOOL start_prev,BOOL end_next);

	void populateData();

	int getFuncIDFromName(LPCTSTR namn);
	int getGradesIDFromName(LPCTSTR namn);
	int getGradesIDFromCode(LPCTSTR code);
	CString getGradesNameFromCode(LPCTSTR code);
	int getGradesIsPulpwood(LPCTSTR code);

	BOOL getSpecies(int id,CSpecies& rec);
	CString getCalculationBasis(int id);
	CString getGradeCode(int id);

	void setupSpeciesTabs();
	void setupPricelistPerSpecies();

	QUIT_TYPES::Q_T_RETURN isNameOfTemplateOK(LPCTSTR name);

	BOOL isViewEnabled();

	QUIT_TYPES::Q_T_RETURN checkDataBeforeSave(CHECK_SAVE_TYPES::CHECK_SAVE cs);
	
protected:
	CLogScaleTemplatesFormView();           // protected constructor used by dynamic creation
	virtual ~CLogScaleTemplatesFormView();

public:
	enum { IDD = IDD_FORMVIEW8 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif


	void doReportViewClick(NMHDR * pNotifyStruct);
	void doSave();
	void doSetNavigationButtons();

	void doPopulate(int tmpl_id);
	void doPopulate(CListContract& rec);
protected:
	//{{AFX_VIRTUAL(CUserCreateVolFunc)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CUserCreateVolFunc)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);

	afx_msg void OnCommand(UINT nID);
	afx_msg void OnCommand2(UINT nID);

	afx_msg void OnUpdateToolbar(CCmdUI* pCmdUI);

	afx_msg void OnUpdateToolbarAddPrl(CCmdUI* pCmdUI);
	afx_msg void OnUpdateToolbarExportTmpl(CCmdUI* pCmdUI);


	afx_msg void OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnItemchangedList(NMHDR* pNMHDR, LRESULT* pResult);


	//{{AFX_MSG(CUserCreateVolFunc)

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnChangeEdit83();
	afx_msg void OnEnChangeEdit84();
	CDateTimeCtrl m_dtDate;
	CString m_csDate;
};


// ReportView used for each specie set in list
class CReportPricelistView : public CXTPReportView
{
	DECLARE_DYNCREATE(CReportPricelistView)

	CImageList m_ilIcons;
	vecFuncDesc m_vecFuncDesc;
	CVecUserVolTables m_vecUserVolTables;
public:
	CReportPricelistView();
	virtual ~CReportPricelistView();

	//{{AFX_MSG(CReportSpcView)
	afx_msg void OnCommand(UINT nID);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	//{{AFX_MSG(CReportSpcView)

	DECLARE_MESSAGE_MAP()
};
