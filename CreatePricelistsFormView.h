#pragma once

#include "StdAfx.h"

#include "DatePickerCombo.h"

#include "DBHandling.h"

#include "Resource.h"
#include "afxdtctl.h"

///////////////////////////////////////////////////////////////////////////////////////////
// CCreatePricelistsDoc

class CCreatePricelistsDoc : public CDocument
{
protected: // create from serialization only
	CCreatePricelistsDoc();
	DECLARE_DYNCREATE(CCreatePricelistsDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCreatePricelistsDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CCreatePricelistsDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CCreatePricelistsDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
/////////////////////////////////////////////////////////////////////////////
// class CCreatePricelistsFrame

class CCreatePricelistsFrame : public CChildFrameBase
{
//private:

	DECLARE_DYNCREATE(CCreatePricelistsFrame)
	CXTPToolBar m_wndToolBar;
	CXTPDockingPaneManager m_paneManager;
	CString m_sLangFN;

	BOOL m_bFirstOpen;
	BOOL m_bInitReports;
	BOOL m_bIsPrintOutTBtn;

	BOOL m_bIsSysCommand;	// TRUE if syscommad have ben activeted

	void setNavBarButtons(bool list = true)
	{
		// Send messages to HMSShell, disable buttons on toolbar; 120122 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

		// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 120122 p�d
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
		
		AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,list);
	}

protected:
	CXTPDockingPaneManager* GetDockingPaneManager() 
	{	
		return &m_paneManager; 
	}
	void SetDockingPaneTheme(XTPDockingPanePaintTheme theme)
	{
		GetDockingPaneManager()->SetTheme(theme);
	}

	HICON m_hIcon;
public:
	CCreatePricelistsFrame();

	static XTPDockingPanePaintTheme m_themeCurrent;

// Attributes

// Operations

// Overrides
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	virtual ~CCreatePricelistsFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	
	//{{AFX_MSG(CCreatePricelistsFrame)
	afx_msg int OnCreateControl(LPCREATECONTROLSTRUCT lpCreateControl);
	afx_msg void OnSysCommand(UINT nID,LPARAM lParam);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg void OnClose();
	afx_msg void OnDestroy(void);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnMDIActivate(BOOL bActivate,CWnd* pActivateWnd,CWnd* pDeactivateWnd);
	afx_msg	void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSetFocus(CWnd*);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );

	//}}AFX_MSG
	
	DECLARE_MESSAGE_MAP()
};


class CReportCreatePricelistView;
// CCreatePricelistsFormView form view

class CCreatePricelistsFormView : public CXTResizeFormView
{
	DECLARE_DYNCREATE(CCreatePricelistsFormView)

	enum DEL_SPECIES { DEL_ALL_SPECIES, DEL_SELECTED_SPECIES };

	BOOL m_bInitialized;
	BOOL m_bIsNameOfPricelistOK;
	CString m_sLangFN;

	CString m_sMsgCap;
	CString m_sMsgRemoveSpecies1;
	CString m_sMsgRemoveSpecies2;
	CString m_sMsgRemoveTable1;
	CString m_sMsgRemoveTable2;
	CString m_sMsgMissingData1;
	CString m_sMsgMissingData2;
	CString m_sMsgPricelistInTmpl;
	CString m_sMsgPricelistNameAlreadyUsed1;
	CString m_sMsgPricelistNameAlreadyUsed2;
	CString m_sMsgDeafultSettingsMissing;
	CString m_sMsgPricelistIsMissingData;
	CString m_sMsgNotAPricelistFile;

	CString m_sExportPrlFilter;
	CString m_sImportPrlFilter;


	CString m_sDataPRL;

	CMyExtStatic m_lbl9_1;
	CMyExtStatic m_lbl9_2;
	CMyExtStatic m_lbl9_3;
	CMyExtStatic m_lbl9_4;

	CMyExtEdit m_edit9_1;
	CMyExtEdit m_edit9_2;
	CMyExtEdit m_edit9_3;

	BOOL m_bEnableToolBarBtnSave;
	BOOL m_bEnableToolBarBtnDelete;
	BOOL m_bEnableToolBarBtnAddSpc;
	BOOL m_bEnableToolBarBtnDelSpc;
	BOOL m_bEnableToolBarBtnImport;
	BOOL m_bEnableToolBarBtnExport;
	BOOL m_bEnableToolBarBtnPreview;

	CMyTabControl m_wndTabControl;

	vecFuncDesc m_vecFuncDesc;
	CVecUserVolTables m_vecUserVolTables;

	CVecGrades m_vecGrades;
	CVecGrades m_vecGradesSelected;

	CVecDefaults m_vecDefaults;

	CPricelist m_recPricelist;
	CVecPricelist m_vecVecPricelist;

	CVecSpecies m_vecSpecies;
	CVecSpecies m_vecSpeciesSelected;

	CVecSpeciesGradesAndPrice m_vecSpeciesGradesAndPrice;

	vecLogScaleTemplates m_vecLogScaleTemplates;

	int m_nSelectedIndex;
	int m_nSelectedSpeciesTab;

	BOOL m_bOnlySave;	// TRUE = Savebutton clicked, FALSE from Arrow or Quit 

	CDBHandling *m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;

	void New();
	BOOL Save();
	void Delete();

	BOOL AddSpeciesAndGrades();
	BOOL AddSpecies();
	void DelSpecies();

	void Import();
	void Export();
	void PreView();

	void populateData();

	void removeSpecies(DEL_SPECIES del_species);

	void createData(BOOL only_create = FALSE);
	void addSpeciesToTabs();
	void addGradesToSpecies();
	BOOL getSpecies(int id,CSpecies& rec);
	CString getCalculationBasis(int id);
	BOOL getGrades(int id,CGrades& rec);

	BOOL isAlreadyOnTab(LPCTSTR spc);
	void enableView(BOOL enable);
	void clearView();
	
	inline BOOL isViewEnabled() { return m_wndTabControl.IsWindowEnabled(); }

	BOOL isPricelistInTemplate(int prl_id);

	BOOL isGradePulpwood(LPCTSTR grade_code);

	BOOL isVolumeFunctionsMissing();

	QUIT_TYPES::Q_T_RETURN isNameOfPricelistOK(LPCTSTR name);

	int getFuncIDFromName(LPCTSTR namn);
	int getGradesIDFromCode(LPCTSTR code);
	CString getGradesNameFromCode(LPCTSTR code);
	int getGradesIsPulpwood(LPCTSTR code);
	void setupSpeciesTabs();
	void setupPricelistPerSpecies();

	void setNavigationButtons(BOOL start_prev,BOOL end_next);

	std::map<int,int> m_mapSpcID;
	BOOL AddView(CRuntimeClass* pViewClass, LPCTSTR lpszTitle, int nIcon,int spc_id);
	CReportCreatePricelistView *getReportView(void);
	CReportCreatePricelistView *getReportView(int idx);

	QUIT_TYPES::Q_T_RETURN checkDataBeforeSave(CHECK_SAVE_TYPES::CHECK_SAVE cs);

	void UpdateSpeciesGrades();

protected:
	CCreatePricelistsFormView();           // protected constructor used by dynamic creation
	virtual ~CCreatePricelistsFormView();

public:
	enum { IDD = IDD_FORMVIEW9 };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

	void doReportViewClick(NMHDR * pNotifyStruct);
	void doSave();
	void doSetNavigationButtons();

	void doPopulate(CPricelist& rec);

protected:
	//{{AFX_VIRTUAL(CCreatePricelistsFormView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CCreatePricelistsFormView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	afx_msg void OnSize(UINT nType,int cx,int cy);
	afx_msg LRESULT OnMessageFromShell( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnOpenSuiteArgMessage(WPARAM wParam,LPARAM lParam);

	afx_msg void OnCommand(UINT nID);
	afx_msg void OnCommandTools(UINT nID);

	afx_msg void OnUpdateToolbar(CCmdUI* pCmdUI);
	
	afx_msg void OnSelectedChanged(NMHDR* pNMHDR, LRESULT* pResult);
	//{{AFX_MSG(CCreatePricelistsFormView)

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnChangeEdit91();
	CDateTimeCtrl m_dtDate;
	CString m_csDate;
};


// ReportView used for each specie set in list
class CReportCreatePricelistView : public CXTPReportView
{
	DECLARE_DYNCREATE(CReportCreatePricelistView)

	CImageList m_ilIcons;
	vecFuncDesc m_vecFuncDesc;
	CVecUserVolTables m_vecUserVolTables;
public:
	CReportCreatePricelistView();
	virtual ~CReportCreatePricelistView();

	//{{AFX_MSG(CReportSpcView)
	afx_msg void OnCommand(UINT nID);
	afx_msg void OnCommandTools(UINT nID);
	afx_msg void OnReportItemClick(NMHDR * pNotifyStruct, LRESULT * /*result*/);
	//{{AFX_MSG(CReportSpcView)

	DECLARE_MESSAGE_MAP()
};
