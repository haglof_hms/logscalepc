#pragma once

#include "dbhandling.h"
#include "Resource.h"

// CDefaultsDlg dialog

class CDefaultsDlg : public CXTResizeDialog
{
	DECLARE_DYNAMIC(CDefaultsDlg)

	// Data members
	BOOL m_bInitialized;
	CString m_sLangFN;

	CString m_sYes;
	CString m_sNo;

	CXTPPropertyGrid m_propDefaults;

	CDBHandling *m_pDB;
	DB_CONNECTION_DATA m_dbConnectionData;
	
	// Bind data members to propertygrid
	CString m_sSawlogVolumeUnit;
	CString m_sPulpwoodVolumeUnit;
	CString m_sDeductionType;
	CString m_sSpeciesCode;
	CString m_sLogGrade;
	CString m_sLoadType;
	long m_nFrequency;
	double m_fLogLengthInch;
	double m_fLogLengthMM;
	CString m_sUseSouthernDoyle;
	CString m_sRoundScaling;
	CString m_sPoundsPerCubicFoot;
	CString m_sBarkRatio;
	CString m_sTaperIB;

	CString m_sSettingsSWtext;	//#4258 ny variabel
	CString m_sSettingsPWtext;	//#4258 ny variabel
	int m_nSawlogFuncID;	//#4258 ny variabel
	int m_nPulpwoodFuncID;	//#4258 ny variabel
	
	//CCalcTypes m_recCalcTypes;
	//CVecCalcTypes m_vecCalcTypes;
	vecFuncDesc m_vecFuncDesc;
	CVecUserVolTables m_vecUserVolTables;
	CFuncDesc m_recFuncDesc;

	CGrades m_recGrades;
	CVecGrades m_vecGrades;

	CSpecies m_recSpecies;
	CVecSpecies m_vecSpecies;

	CDefaults m_recDefaults;
	CVecDefaults m_vecDefaults;

	CStringArray m_sarrDeductionTypes;
	CStringArray m_sarrLoadTypes;

	CButton m_btnOK;
	CButton m_btnCancel;
	CButton m_btnSetStd;
	// Methods
	void setupPropGrid();

	CString getCalcTypeBasisValue(LPCTSTR calc_type);
	CString getCalcTypeValue(int func_id);	//#4258
public:
	CDefaultsDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDefaultsDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG4 };

protected:
	//{{AFX_VIRTUAL(CDefaultsDlg)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	//{{AFX_MSG(CRegisterDlg)
	afx_msg	BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButton1();
};
