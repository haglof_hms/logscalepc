#if !defined(__LOGSCALEVOLUME_H__)
#define __LOGSCALEVOLUME_H__
#pragma once

class CCalcVolumes
{
// private

public:
	CCalcVolumes();
	virtual ~CCalcVolumes();

	void calculate(CVecLogs& vec,CTickets& rec);
	void calculate(CLogs& recLogs,CTickets& recTickets);

};


#endif