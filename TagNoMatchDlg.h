#pragma once

#include "resource.h"

// CTagNoMatchDlg dialog

class CTagNoMatchDlg : public CXTResizeDialog
{
	DECLARE_DYNAMIC(CTagNoMatchDlg)

	// Data members
	BOOL m_bInitialized;
	CString m_sLangFN;

	CMyExtStatic m_lbl16_1;

	CButton m_btnOK;
	CButton m_btnCancel;
	CButton m_btn16_1;

	CListCtrl m_list16_1;

	CString m_sMsgCap;
	CString m_sMsgOpenEXCEL;
	CString m_sMsgCapEXCEL;
	
	CString m_sMissing;
	CString m_sNotInStock;
	CString m_sDuplicateTags;

	CString m_sTotalNumOfLogs;
	CString m_sAlreadyRegOnOtherSale;
	CString m_sAlreadyRegOnOtherTicket;
	CString m_sNotRegistered;
	CString m_sDuplicateTagNumbers;
	CString m_sAllLogsOK;
	CString m_sOKTags;

	CString m_sMsgTotalNumOfLogs;
	CString m_sMsgAlreadyRegOnOtherSale;
	CString m_sMsgAlreadyRegOnOtherTicket;
	CString m_sMsgDuplicateTagNumbers;
	CString m_sMsgNotRegistered;
	CString m_sMsgOKTags;

	CString m_sMsgAllLogsOK;

	CString m_sMsgNoInfoOnSheet;

	CString m_sTicketFile;
	CString m_sTicket;
	int m_nTotalNumberOfLogs;
	CVecLogs m_vecLogTagsNotOK;
	CVecLogs m_vecLogTagsOK;
	CVecSpecies m_vecSpecies;

	CStrIntMap m_mapLogTags;
	CStrIntMap m_mapLogTagsDup;

	CStringArray m_sarrStatus;

	short m_nOpenAs;

	int m_nMissing;
	int m_nDuplicateTagNumbers;

	void setupListCtrl();

	CString getSpcName(LPCTSTR spc_code);

public:
	CTagNoMatchDlg(CWnd* pParent = NULL,short open_as = 0);   // standard constructor
	virtual ~CTagNoMatchDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG16 };

	// Logs with tags already in logs-inventory
	void setLogTagsNotOK(CVecLogs &vec)	{ m_vecLogTagsNotOK = vec; }
	void setLogTagsOK(CVecLogs &vec)	{ m_vecLogTagsOK = vec; }
	void setLogTags(CStrIntMap &map)	{ m_mapLogTags = map; }
	void setLogTagsDup(CStrIntMap &map)	{ m_mapLogTagsDup = map; }
	void setTotalNumberOfLogs(int v)	{ m_nTotalNumberOfLogs = v; }
	void setSpecies(CVecSpecies &vec)	{ m_vecSpecies = vec; }
	void setTicketFile(LPCTSTR v)	{ m_sTicketFile = v; }
	void setTicket(LPCTSTR v)	{ m_sTicket = v; }

protected:
	//{{AFX_VIRTUAL(CDefaultsDlg)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton161();
};
