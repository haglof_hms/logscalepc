
#include "stdafx.h"

#include <memory>

#include "fileparser.h"

//#include "ResLangFileReader.h"

CFileParser::CFileParser() :
	m_sFile(L""),
	m_bSUCCESS(false),
	m_nNumOfTicketsInFile(0),
	m_sFileVersion(L""),
	m_nLoadID(-1),
	m_fBarkRatio(0.0)
{
}


CFileParser::CFileParser(LPCTSTR file) :
	m_sFile(file),
	m_bSUCCESS(false),
	m_nNumOfTicketsInFile(0),
	m_sFileVersion(L""),
	m_nLoadID(-1),
	m_fBarkRatio(0.0)
{
	char szLine[255];
	CString sLine = L"";
	// Try to open the file
	if (fileExists(file))
	{
		fs.open(m_sFile);
		m_bSUCCESS = fs.good();

		if (m_bSUCCESS)
		{
			memset(szLine,0,255);
			while(!fs.eof())
			{
				szLine[0] = '\0';
				fs.getline(szLine,255);
				sLine = szLine;
				if (sLine != _T(""))
				{
					// Clean up
					sLine.Trim();
//					sLine.Remove(CHAR(32));
					if (sLine.Left(1) == '/')
						sLine.Delete(0);
					sLine.Replace(L"|",L";");
					if (!isIdentifer(sLine) && sLine.Left(sLine.GetLength()) != _T(";")) 
						sLine += _T(";");
					m_sarrFileContent.Add(sLine);
				}	// if (sLine != _T(""))
			}	// while(!fs.eof())
			m_bSUCCESS = m_sarrFileContent.GetCount() > 0;
		}	// if (m_bSUCCESS)
	}	// if (fileExists(file))
}

// PRIVATE METHODS

bool CFileParser::parsIdentifer(void)
{
	CString sLine = L"";
	bool bFirstIDOK = false;
	bool bSecondIDOK = false;
	int nCountTickets = 0;
	bool bFound = false;
	m_nNumOfTicketsInFile = 0;
	for (int i = 0;i < m_sarrFileContent.GetCount();i++)
	{
		sLine = m_sarrFileContent.GetAt(i);
		if (sLine.CompareNoCase(HEADERSTRING_CHECK) == 0 || sLine.CompareNoCase(HEADERSTRING_CHECK2) == 0) bFirstIDOK = true;
		if ((sLine.CompareNoCase(GUID_CHECK) == 0 || sLine.CompareNoCase(GUID_CHECK2) == 0) && bFirstIDOK) bSecondIDOK = true;
		if (bFirstIDOK && bSecondIDOK)
		{
			bFound = true;
			m_nNumOfTicketsInFile++;
		}
	}
	return bFound;

}

void CFileParser::parsHeader(void)
{
	int nIdentifer = -1;
	CString sName = L"";
	CString sValue = L"";
	CString sLine = L"";
	CString sToken = L"",S;
	TCHAR szLine[255];
	int nTokenCnt = 0;
	BOOL bOK = FALSE;
	int nHeaderItemsCnt = 0;

	memset(szLine,0,255);
	for (int i = 0;i < m_sarrFileContent.GetCount();i++)
	{
		sLine = m_sarrFileContent.GetAt(i);
		if (!isIdentifer(sLine))
		{
			_tcscpy(szLine,sLine);
			// Count tokens in string
			nTokenCnt = 0;
			for (TCHAR *p=szLine;p < _tcslen(szLine) + szLine;p++)
			{
				if (*p == ';')
					nTokenCnt++;
			}
			// Tokenize data
			if (nTokenCnt == 4)
			{
				for (int pos = 0;pos < nTokenCnt;pos++)
				{
					AfxExtractSubString(sToken,szLine,pos,';');
					switch (pos)
					{
						case 0 : break;		// Empty
						// Identifer
						case 1 : 
							if (!sToken.IsEmpty())
							{
								nIdentifer = _tstoi(sToken);
							}
						break;
						// Value
						case 3 : 
							if (nIdentifer > 0 && nIdentifer <= 29)
							{
								m_mapHeaderData[nIdentifer] = sToken.Trim();
							}
							if (nIdentifer == PARS_BARK_RATIO)
							{
								m_fBarkRatio = 0.0; //StrToDbl(sToken); Not used, barkreduction set per species; 120213 p�d
							}
						break;
					};
				}	// for (int i = 0;i < nTokenCnt;i++)
				nHeaderItemsCnt++;
			}	// if (nTokenCnt == 4)
		}	// if (!isIdentifer(sLine))
	}
	// Set fileversion, depending on number of items in header
	// if number of items <= 8; v1
	// if number of items > 8; v2
	if (nHeaderItemsCnt <= 8)
		m_sFileVersion = FILEVERSION1;
	else if (nHeaderItemsCnt > 8)
		m_sFileVersion = FILEVERSION2;

}

void CFileParser::parsLogs(void)
{
	CString sLine = L"",S;
	CString sToken = L"";
	TCHAR szLine[255];
	int nTokenCnt = 0;

	memset(szLine,0,255);
	for (int i = 0;i < m_sarrFileContent.GetCount();i++)
	{
		sLine = m_sarrFileContent.GetAt(i);
		if (!isIdentifer(sLine))
		{
			CLogs rec = CLogs();
			_tcscpy(szLine,sLine);
			// Count tokens in string
			nTokenCnt = 0;
			for (TCHAR *p=szLine;p < _tcslen(szLine) + szLine;p++)
			{
				if (*p == ';')
					nTokenCnt++;
			}
			// Tokenize data
			if (nTokenCnt == 15)
			{
				for (int pos = 0;pos < nTokenCnt;pos++)
				{
					AfxExtractSubString(sToken,szLine,pos,';');
					switch (pos)
					{
						case 0 : rec.setSpcCode(sToken.Trim()); break;		// SpecieCode
						case 1 : rec.setFrequency(_tstoi(sToken)); break;		// Frequency
						// Inm�tt toppdiam; under bark
						case 2 : 
							if (m_nMeasureMode == 1) // Meter to Inch
								rec.setDIBTop(StrToDbl(sToken)*MM2IN); 
							else if (m_nMeasureMode == 2) // Inch to Meter
								rec.setDIBTop(StrToDbl(sToken)*IN2MM); 
							else 
								rec.setDIBTop(StrToDbl(sToken)); 
							break;		
						// Length
						case 3 : 
							if (m_nMeasureMode == 1) // DM to Feet
								rec.setLengthMeas(StrToDbl(sToken)*DM2FT); 
							else if (m_nMeasureMode == 2) // Feet to DM
								rec.setLengthMeas(StrToDbl(sToken)*FT2DM); 
							else if (m_nMeasureMode == 4) // Feet
								rec.setLengthMeas(StrToDbl(sToken)); 
							else if (m_nMeasureMode == 5) // M
								rec.setLengthMeas(StrToDbl(sToken)/10.0); 
							break;		
						case 4 : rec.setGrade(sToken); break;		// Grade
						case 5 : rec.setDeduction(StrToDbl(sToken)); break;		// Deduction (procent el. absolut v�rde)
						case 6 : rec.setTagNumber(sToken.Trim()); break;		// Tagnummer (plastbricka)
						case 7 : rec.setUVol(StrToDbl(sToken)); break;		// UserVolume
						case 8 : rec.setNotes(sToken); break;		// Notes
						// Egeninl�st toppdiameter
						case 9 : 
							if (m_nMeasureMode == 1) // MM to Inch
								rec.setUTopDia(StrToDbl(sToken)*MM2IN); 
							else if (m_nMeasureMode == 2) // Inch to MM
								rec.setUTopDia(StrToDbl(sToken)*IN2MM); 
							else
								rec.setUTopDia(StrToDbl(sToken)); 
							break;		
						// Egeninl�st l�ngd
						case 10 : 
							if (m_nMeasureMode == 1) // DM to Feet
								rec.setULength(StrToDbl(sToken)*DM2FT); 
							else if (m_nMeasureMode == 2) // Feet to DM
								rec.setULength(StrToDbl(sToken)*FT2DM); 
							else 
								rec.setULength(StrToDbl(sToken)); 
							break;	
						case 11 : rec.setReason(sToken); break;	// Anledning f�r nedklassning
						case 12 : rec.setUPrice(StrToDbl(sToken)); break;	// Egeninl�st pris
						case 13 : 
							if (m_nMeasureMode == 1) // MM to Inch
								rec.setDIBRoot(StrToDbl(sToken)*MM2IN); 
							else if (m_nMeasureMode == 2) // Inch to MM
								rec.setDIBRoot(StrToDbl(sToken)*IN2MM); 
							else
								rec.setDIBRoot(StrToDbl(sToken)); 
							break;	// Inm�tt rotdiamter
						case 14 : 
							if (m_nMeasureMode == 1) // MM to Inch
								rec.setURootDia(StrToDbl(sToken)*MM2IN); 
							else if (m_nMeasureMode == 2) // Inch to MM
								rec.setURootDia(StrToDbl(sToken)*IN2MM); 
							else
								rec.setURootDia(StrToDbl(sToken)); 
							break;	// Egeninl�st rotdiamter
					};
				}	// for (int i = 0;i < nTokenCnt;i++)

				m_fBarkRatio = 0.0;

				rec.setDIBRoot2(0.0);
				rec.setDIBTop2(0.0);
				rec.setDOBRoot2(0.0);
				rec.setDOBTop2(0.0);
				rec.setURootDia2(0.0);
				rec.setUTopDia2(0.0);
				rec.setLoadID(m_nLoadID);
				rec.setDOBTop(0.0);
				rec.setDOBRoot(0.0);
				// Bark reduction
				rec.setBark(0.0);

				rec.setVolCust1(0.0);
				rec.setVolCust2(0.0);
				rec.setVolCust3(0.0);
				rec.setVolCust4(0.0);
				rec.setVolCust5(0.0);
				rec.setVolCust6(0.0);
				rec.setVolCust7(0.0);
				rec.setVolCust8(0.0);
				rec.setVolCust9(0.0);
				rec.setVolCust10(0.0);
				rec.setVolCust11(0.0);
				rec.setVolCust12(0.0);
				rec.setVolCust13(0.0);
				rec.setVolCust14(0.0);
				rec.setVolCust15(0.0);
				rec.setVolCust16(0.0);
				rec.setVolCust17(0.0);
				rec.setVolCust18(0.0);
				rec.setVolCust19(0.0);
				rec.setVolCust20(0.0);
			
				rec.setGISCoord1(L"");
				rec.setGISCoord2(L"");

				rec.setStatusFlag(0);	// I lager

				rec.setVolFuncName(L"");
				rec.setVolFuncID(-1);
				rec.setTicketOrigin(LOGORIGIN::FROM_FILE);

				m_vecLogs.push_back(rec);
			}	// if (nTokenNum == 15)
			
			// Tokenize data
			if (nTokenCnt == 19)
			{
				for (int pos = 0;pos < nTokenCnt;pos++)
				{
					AfxExtractSubString(sToken,szLine,pos,';');
					switch (pos)
					{
						// SpecieCode
						case 0 : rec.setSpcCode(sToken.Trim()); break;		
						// Frequency
						case 1 : rec.setFrequency(_tstoi(sToken)); break;		
						// Inm�tt toppdiam; under bark
						case 2 : 
							if (m_nMeasureMode == 1) // Meter to Inch
								rec.setDIBTop(StrToDbl(sToken)*MM2IN); 
							else if (m_nMeasureMode == 2) // Inch to Meter
								rec.setDIBTop(StrToDbl(sToken)*IN2MM); 
							else 
								rec.setDIBTop(StrToDbl(sToken)); 
							break;		
						// Inm�tt toppdiam; under bark
						case 3 : 
							if (m_nMeasureMode == 1) // Meter to Inch
								rec.setDIBTop2(StrToDbl(sToken)*MM2IN); 
							else if (m_nMeasureMode == 2) // Inch to Meter
								rec.setDIBTop2(StrToDbl(sToken)*IN2MM); 
							else
								rec.setDIBTop2(StrToDbl(sToken)); 
							break;		
						// Length
						case 4 : 
							if (m_nMeasureMode == 1) // DM to Feet
								rec.setLengthMeas(StrToDbl(sToken)*DM2FT); 
							else if (m_nMeasureMode == 2) // Feet to DM
								rec.setLengthMeas(StrToDbl(sToken)*FT2DM); 
							else if (m_nMeasureMode == 4) // Feet
								rec.setLengthMeas(StrToDbl(sToken)); 
							else if (m_nMeasureMode == 5) // M
								rec.setLengthMeas(StrToDbl(sToken)/10.0); 
							break;		
						// Grade
						case 5 : rec.setGrade(sToken); break;		
						// Deduction (procent el. absolut v�rde)
						case 6 : rec.setDeduction(StrToDbl(sToken)); break;	
						// Tagnummer (plastbricka)
						case 7 : rec.setTagNumber(sToken.Trim()); break;	
						// UserVolume
						case 8 : rec.setUVol(StrToDbl(sToken)); break;		
						// Notes
						case 9 : rec.setNotes(sToken); break;		
						// Egeninl�st toppdiameter
						case 10 : 
							if (m_nMeasureMode == 1) // MM to Inch
								rec.setUTopDia(StrToDbl(sToken)*MM2IN); 
							else if (m_nMeasureMode == 2) // Inch to MM
								rec.setUTopDia(StrToDbl(sToken)*IN2MM); 
							else
								rec.setUTopDia(StrToDbl(sToken)); 
							break;		
						// Egeninl�st toppdiameter
						case 11 : 
							if (m_nMeasureMode == 1) // MM to Inch
								rec.setUTopDia2(StrToDbl(sToken)*MM2IN); 
							else if (m_nMeasureMode == 2) // Inch to MM
								rec.setUTopDia2(StrToDbl(sToken)*IN2MM); 
							else
								rec.setUTopDia2(StrToDbl(sToken)); 
							break;	
						// Egeninl�st l�ngd
						case 12 : 
							if (m_nMeasureMode == 1) // DM to Feet
								rec.setULength(StrToDbl(sToken)*DM2FT); 
							else if (m_nMeasureMode == 2) // Feet to DM
								rec.setULength(StrToDbl(sToken)*FT2DM); 
							else if (m_nMeasureMode == 4) // feet
								rec.setULength(StrToDbl(sToken)); 
							else if (m_nMeasureMode == 5) // M
								rec.setULength(StrToDbl(sToken)/10.0); 
							break;	
						// Anledning f�r nedklassning
						case 13 : rec.setReason(sToken); break;	
						// Egeninl�st pris
						case 14 : rec.setUPrice(StrToDbl(sToken)); break;	
						// Inm�tt rotdiamter
						case 15 : 
							if (m_nMeasureMode == 1) // MM to Inch
								rec.setDIBRoot(StrToDbl(sToken)*MM2IN); 
							else if (m_nMeasureMode == 2) // Inch to MM
								rec.setDIBRoot(StrToDbl(sToken)*IN2MM); 
							else
								rec.setDIBRoot(StrToDbl(sToken)); 
							break;	
						// Inm�tt rotdiamter
						case 16 : 
							if (m_nMeasureMode == 1) // MM to Inch
								rec.setDIBRoot2(StrToDbl(sToken)*MM2IN); 
							else if (m_nMeasureMode == 2) // Inch to MM
								rec.setDIBRoot2(StrToDbl(sToken)*IN2MM); 
							else
								rec.setDIBRoot2(StrToDbl(sToken)); 
							break;	
						// Egeninl�st rotdiamter
						case 17 : 
							if (m_nMeasureMode == 1) // MM to Inch
								rec.setURootDia(StrToDbl(sToken)*MM2IN); 
							else if (m_nMeasureMode == 2) // Inch to MM
								rec.setURootDia(StrToDbl(sToken)*IN2MM); 
							else
								rec.setURootDia(StrToDbl(sToken)); 
							break;	
						// Egeninl�st rotdiamter
						case 18 : 
							if (m_nMeasureMode == 1) // MM to Inch
								rec.setURootDia2(StrToDbl(sToken)*MM2IN); 
							else if (m_nMeasureMode == 2) // Inch to MM
								rec.setURootDia2(StrToDbl(sToken)*IN2MM); 
							else
								rec.setURootDia2(StrToDbl(sToken)); 
							break;	
					};
				}	// for (int i = 0;i < nTokenCnt;i++)
				m_fBarkRatio = 0.0;
				rec.setLoadID(m_nLoadID);
				rec.setDOBTop(0.0);
				rec.setDOBTop2(0.0);
				rec.setDOBRoot(0.0);
				rec.setDOBRoot2(0.0);
				// Bark reduction
				rec.setBark(0.0);

				rec.setVolCust1(0.0);
				rec.setVolCust2(0.0);
				rec.setVolCust3(0.0);
				rec.setVolCust4(0.0);
				rec.setVolCust5(0.0);
				rec.setVolCust6(0.0);
				rec.setVolCust7(0.0);
				rec.setVolCust8(0.0);
				rec.setVolCust9(0.0);
				rec.setVolCust10(0.0);
				rec.setVolCust11(0.0);
				rec.setVolCust12(0.0);
				rec.setVolCust13(0.0);
				rec.setVolCust14(0.0);
				rec.setVolCust15(0.0);
				rec.setVolCust16(0.0);
				rec.setVolCust17(0.0);
				rec.setVolCust18(0.0);
				rec.setVolCust19(0.0);
				rec.setVolCust20(0.0);
			
				rec.setGISCoord1(L"");
				rec.setGISCoord2(L"");

				rec.setStatusFlag(0);	// I lager

				rec.setVolFuncName(L"");
				rec.setVolFuncID(-1);
				rec.setTicketOrigin(LOGORIGIN::FROM_FILE);


				m_vecLogs.push_back(rec);
			}	// if (nTokenNum == 19)

			// Tokenize data
			if (nTokenCnt == 21)
			{
				for (int pos = 0;pos < nTokenCnt;pos++)
				{
					AfxExtractSubString(sToken,szLine,pos,';');
					switch (pos)
					{
						// SpecieCode
						case 0 : rec.setSpcCode(sToken.Trim()); break;		
						// Frequency
						case 1 : rec.setFrequency(_tstoi(sToken)); break;		
						// Inm�tt toppdiam; under bark
						case 2 : 
							if (m_nMeasureMode == 1) // Meter to Inch
								rec.setDIBTop(StrToDbl(sToken)*MM2IN); 
							else if (m_nMeasureMode == 2) // Inch to Meter
								rec.setDIBTop(StrToDbl(sToken)*IN2MM); 
							else 
								rec.setDIBTop(StrToDbl(sToken)); 
							break;		
						// Inm�tt toppdiam; under bark
						case 3 : 
							if (m_nMeasureMode == 1) // Meter to Inch
								rec.setDIBTop2(StrToDbl(sToken)*MM2IN); 
							else if (m_nMeasureMode == 2) // Inch to Meter
								rec.setDIBTop2(StrToDbl(sToken)*IN2MM); 
							else
								rec.setDIBTop2(StrToDbl(sToken)); 
							break;		
						// Length
						case 4 : 
							if (m_nMeasureMode == 1) // CM to Feet
								rec.setLengthMeas((StrToDbl(sToken)/100.0)*M2FT); 
							else if (m_nMeasureMode == 2) // Feet to CM
								rec.setLengthMeas((StrToDbl(sToken)*FT2M)/100.0); 
							else if (m_nMeasureMode == 4) // Feet
								rec.setLengthMeas(StrToDbl(sToken)); 
							else if (m_nMeasureMode == 5) // M
								rec.setLengthMeas(StrToDbl(sToken)/100.0); 
							break;		
						// Grade
						case 5 : rec.setGrade(sToken); break;		
						// Deduction (procent el. absolut v�rde)
						case 6 : rec.setDeduction(StrToDbl(sToken)); break;	
						// Tagnummer (plastbricka)
						case 7 : rec.setTagNumber(sToken.Trim()); break;	
						// UserVolume
						case 8 : rec.setUVol(StrToDbl(sToken)); break;		
						// Notes
						case 9 : rec.setNotes(sToken); break;		
						// Egeninl�st toppdiameter
						case 10 : 
							if (m_nMeasureMode == 1) // MM to Inch
								rec.setUTopDia(StrToDbl(sToken)*MM2IN); 
							else if (m_nMeasureMode == 2) // Inch to MM
								rec.setUTopDia(StrToDbl(sToken)*IN2MM); 
							else
								rec.setUTopDia(StrToDbl(sToken)); 
							break;		
						// Egeninl�st toppdiameter
						case 11 : 
							if (m_nMeasureMode == 1) // MM to Inch
								rec.setUTopDia2(StrToDbl(sToken)*MM2IN); 
							else if (m_nMeasureMode == 2) // Inch to MM
								rec.setUTopDia2(StrToDbl(sToken)*IN2MM); 
							else
								rec.setUTopDia2(StrToDbl(sToken)); 
							break;	
						// Egeninl�st l�ngd
						case 12 : 
							if (m_nMeasureMode == 1) // M to Feet
								rec.setULength((StrToDbl(sToken)/100.0)*M2FT); 
							else if (m_nMeasureMode == 2) // Feet to M
								rec.setULength((StrToDbl(sToken)*FT2M)/100.0); 
							else if (m_nMeasureMode == 4) // feet
								rec.setULength(StrToDbl(sToken)); 
							else if (m_nMeasureMode == 5) // M
								rec.setULength(StrToDbl(sToken)/100.0); 
							break;	
						// Anledning f�r nedklassning
						case 13 : rec.setReason(sToken); break;	
						// Egeninl�st pris
						case 14 : rec.setUPrice(StrToDbl(sToken)); break;	
						// Inm�tt rotdiamter
						case 15 : 
							if (m_nMeasureMode == 1) // MM to Inch
								rec.setDIBRoot(StrToDbl(sToken)*MM2IN); 
							else if (m_nMeasureMode == 2) // Inch to MM
								rec.setDIBRoot(StrToDbl(sToken)*IN2MM); 
							else
								rec.setDIBRoot(StrToDbl(sToken)); 
							break;	
						// Inm�tt rotdiamter
						case 16 : 
							if (m_nMeasureMode == 1) // MM to Inch
								rec.setDIBRoot2(StrToDbl(sToken)*MM2IN); 
							else if (m_nMeasureMode == 2) // Inch to MM
								rec.setDIBRoot2(StrToDbl(sToken)*IN2MM); 
							else
								rec.setDIBRoot2(StrToDbl(sToken)); 
							break;	
						// Egeninl�st rotdiamter
						case 17 : 
							if (m_nMeasureMode == 1) // MM to Inch
								rec.setURootDia(StrToDbl(sToken)*MM2IN); 
							else if (m_nMeasureMode == 2) // Inch to MM
								rec.setURootDia(StrToDbl(sToken)*IN2MM); 
							else
								rec.setURootDia(StrToDbl(sToken)); 
							break;	
						// Egeninl�st rotdiamter
						case 18 : 
							if (m_nMeasureMode == 1) // MM to Inch
								rec.setURootDia2(StrToDbl(sToken)*MM2IN); 
							else if (m_nMeasureMode == 2) // Inch to MM
								rec.setURootDia2(StrToDbl(sToken)*IN2MM); 
							else
								rec.setURootDia2(StrToDbl(sToken)); 
							break;	
						// Barkavdrag
						case 19 : 
							if (m_nMeasureMode == 4) // Inch
								rec.setBark((StrToDbl(sToken.Trim())/10.0)*DOUBLE_BARK_FACTOR); 
							else if (m_nMeasureMode == 5) // MM
								rec.setBark(StrToDbl(sToken.Trim())); 
							break;	
						// Stock; I lager etc.
						case 20 : 
							rec.setStatusFlag(_tstoi(sToken));
							break;	

					};
				}	// for (int i = 0;i < nTokenCnt;i++)
				m_fBarkRatio = 0.0;
				rec.setLoadID(m_nLoadID);
				rec.setDOBTop((rec.getDIBTop() > 0.0) ? rec.getDIBTop() + rec.getBark() : 0.0);
				rec.setDOBTop2((rec.getDIBTop2() > 0.0) ? rec.getDIBTop2() + rec.getBark() : 0.0);
				rec.setDOBRoot((rec.getDIBRoot() > 0.0) ? rec.getDIBRoot() + rec.getBark() : 0.0);
				rec.setDOBRoot2((rec.getDIBRoot2() > 0.0) ? rec.getDIBRoot2() + rec.getBark() : 0.0);

				rec.setVolCust1(0.0);
				rec.setVolCust2(0.0);
				rec.setVolCust3(0.0);
				rec.setVolCust4(0.0);
				rec.setVolCust5(0.0);
				rec.setVolCust6(0.0);
				rec.setVolCust7(0.0);
				rec.setVolCust8(0.0);
				rec.setVolCust9(0.0);
				rec.setVolCust10(0.0);
				rec.setVolCust11(0.0);
				rec.setVolCust12(0.0);
				rec.setVolCust13(0.0);
				rec.setVolCust14(0.0);
				rec.setVolCust15(0.0);
				rec.setVolCust16(0.0);
				rec.setVolCust17(0.0);
				rec.setVolCust18(0.0);
				rec.setVolCust19(0.0);
				rec.setVolCust20(0.0);
			
				rec.setVolFuncName(L"");
				rec.setVolFuncID(-1);
				rec.setTicketOrigin(LOGORIGIN::FROM_FILE);

				m_vecLogs.push_back(rec);
			}	// if (nTokenNum == 19)



		}	// if (!isIdentifer(sLine))
	}	// for (int i = 0;i < m_sarrFileContent.GetCount();i++)

}

bool CFileParser::isIdentifer(LPCTSTR str)
{
	return (_tcscmp(str,HEADERSTRING_CHECK) == 0 || _tcscmp(str,GUID_CHECK) == 0 || _tcscmp(str,HEADERSTRING_CHECK2) == 0 || _tcscmp(str,GUID_CHECK2) == 0);
}

// PUBLIC METHODS

bool CFileParser::getTicket(CTickets& rec)
{
	if (parsIdentifer())
	{
		parsHeader();
		// Setup CTickets record
		rec = CTickets(-1,
									m_mapHeaderData[PARS_SOURCE_DATE],
									m_mapHeaderData[PARS_SOURCE_ID],
									m_mapHeaderData[PARS_SCALER],
									m_mapHeaderData[PARS_LOCATION],
									m_mapHeaderData[PARS_BUYER],
									m_mapHeaderData[PARS_OTHER_INFO],
									m_mapHeaderData[PARS_LOAD_ID],
									m_mapHeaderData[PARS_TICKET],
									m_mapHeaderData[PARS_TRACT_ID],
									m_mapHeaderData[PARS_HAULER],
									m_mapHeaderData[PARS_SUPPLIER],
									m_mapHeaderData[PARS_VENDOR],
									m_mapHeaderData[PARS_LOAD_TYPE],
									m_mapHeaderData[PARS_DATE],
									StrToDbl(m_mapHeaderData[PARS_WEIGHT]),
									_tstoi(m_mapHeaderData[PARS_NUMBER_OF_LOGS]),
									m_mapHeaderData[PARS_LOAD_NOTE],
									_tstoi(m_mapHeaderData[PARS_SAWLOG_VOLUME_UNIT]),
									_tstoi(m_mapHeaderData[PARS_PULPWOOD_VOLUME_UNIT]),
									_tstoi(m_mapHeaderData[PARS_ROUND_SCALING]),
									_tstoi(m_mapHeaderData[PARS_USE_SOUTHERN_DOYLE]),
									m_mapHeaderData[PARS_DEDUCTION_TYPE],
									StrToDbl(m_mapHeaderData[PARS_IB_TAPER]),
									StrToDbl(m_mapHeaderData[PARS_BARK_RATIO]),
									_tstoi(m_mapHeaderData[PARS_MEASURING_MODE]),
									0.0,
									0.0,
									0.0,
									m_sFileVersion,
									m_mapHeaderData[PARS_TEMPLATE_NAME],
									L"",
									-1,
									m_mapHeaderData[PARS_LATITUDE],
									m_mapHeaderData[PARS_LONGITUDE],
									_tstoi(m_mapHeaderData[PARS_JAS]),
									-1,
									0.0,
									0,
									0);
		return true;
	}
	
	return false;
}

bool CFileParser::getLogs(int measure_mode,int load_id,CVecLogs& vec)
{
	m_nLoadID = load_id;
	m_nMeasureMode = measure_mode;
	if (parsIdentifer())
	{
		parsLogs();
		vec = m_vecLogs;
		return true;
	}

	return false;
}


