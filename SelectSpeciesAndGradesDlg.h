#pragma once

#include "Resource.h"

// CSelectSpeciesAndGradesDlg dialog
/*
class CSelectSpeciesAndGradesDlg : public CDialog
{
	DECLARE_DYNAMIC(CSelectSpeciesAndGradesDlg)

public:
	CSelectSpeciesAndGradesDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSelectSpeciesAndGradesDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG14 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
};


*/
class CSelectSpeciesAndGradesDlg : public CDialog
{
	DECLARE_DYNAMIC(CSelectSpeciesAndGradesDlg)

	BOOL m_bInitialized;

	CString m_sLangFN;

	CXTListCtrl m_wndSpcListCtrl;
	CXTHeaderCtrl   m_spcHeader;
	CXTListCtrl m_wndGradeListCtrl;
	CXTHeaderCtrl   m_gradeHeader;

	CMyExtStatic m_lbl14_1;

	CButton m_btn1;
	CButton m_btn2;
	CButton m_btn3;
	CButton m_btn4;

	CButton m_btnOK;
	CButton m_btnCancel;

	CVecSpecies m_vecSpecies;
	CVecSpecies m_vecSelectedSpecies;

	CVecGrades m_vecGrades;
	CVecGrades m_vecSelectedGradesIn;
	CVecGrades m_vecSelectedGradesOut;

	BOOL m_bEnableSpecies;
	BOOL m_bEnableGrades;

	void setup(void);

	BOOL isSpeciesSelected();
	BOOL isGradesSelected();

public:
	CSelectSpeciesAndGradesDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSelectSpeciesAndGradesDlg();

	void setSpecies(CVecSpecies& vec)	{ m_vecSpecies = vec; }
	void getSelectedSpecies(CVecSpecies& vec)	{ vec = m_vecSelectedSpecies; }
	void setSelectedSpecies(CVecSpecies& vec)	{ m_vecSelectedSpecies = vec; }

	void setGrades(CVecGrades& vec)	{ m_vecGrades = vec; }
	void getSelectedGrades(CVecGrades& vec)	{ vec = m_vecSelectedGradesOut; }
	void setSelectedGrades(CVecGrades& vec)	{ m_vecSelectedGradesIn = vec; }

// Dialog Data
	enum { IDD = IDD_DIALOG14 };

protected:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSelectSpeciesAndGradesDlg)
	public:
	virtual BOOL OnInitDialog();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	afx_msg void OnLvnItemchangedList2(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnLvnItemchangedList3(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
};

