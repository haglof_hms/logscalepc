#if !defined(__GRADESFORMVIEW_H__)
#define __GRADESFORMVIEW_H__

#include "DBHandling.h"

class CGradesReportView : public CXTPReportView
{
//private:

	DECLARE_DYNCREATE(CGradesReportView)

	// Data members
	BOOL m_bInitialized;
	CString m_sLangFN;
	CDBHandling *m_pDB;
	CVecGrades m_vecGrades;

	CString m_sMsgCap;
	CString m_sMsgDelete1;
	CString m_sMsgDelete2;

	// Methods
	void setupReport();
public:
	CGradesReportView();
	virtual ~CGradesReportView();

	void populateReport(CDBHandling *pDB);

	void addGrades(CDBHandling *pDB);
	void deleteGrades(CDBHandling *pDB);
	void saveGrades(CDBHandling *pDB);

	//{{AFX_VIRTUAL(CGradesReportView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
 	//}}AFX_VIRTUAL


	//{{AFX_MSG(CGradesReportView)
	afx_msg void OnDestroy();
	afx_msg LRESULT OnSuiteMessage(WPARAM wParam,LPARAM lParam);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

};


#endif
