// ViewProgress.cpp : implementation file
//

#include "stdafx.h"
#include "ViewProgress.h"

#include "ResLangFileReader.h"

// CViewProgress dialog

IMPLEMENT_DYNAMIC(CViewProgress, CXTResizeDialog)

BEGIN_MESSAGE_MAP(CViewProgress, CXTResizeDialog)
END_MESSAGE_MAP()

CViewProgress::CViewProgress(CWnd* pParent /*=NULL*/)
	: CXTResizeDialog(CViewProgress::IDD, pParent),
	m_bInitialized(FALSE)
{

}

CViewProgress::~CViewProgress()
{
}

void CViewProgress::DoDataExchange(CDataExchange* pDX)
{
	CXTResizeDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CForm)
	DDX_Control(pDX, IDC_PROGRESS19_1, m_pBar);
	DDX_Control(pDX, IDC_LBL19_1, m_lbl19_1);
	//}}AFX_DATA_MAP
}

BOOL CViewProgress::OnInitDialog()
{
	CString sLangFN = L"";
	CXTResizeDialog::OnInitDialog();
	if (!m_bInitialized)
	{
		sLangFN = getLanguageFN(getLanguageDir(),PROGRAM_NAME,getLangSet(),LANGUAGE_FN_EXT,DEF_LANGUAGE_ABREV);

		m_lbl19_1.SetTextColor(BLUE);
		m_lbl19_1.SetLblFontEx(14,FW_BOLD);

		if (fileExists(sLangFN))
		{
			RLFReader xml;
			if (xml.Load(sLangFN))
			{
			
				m_lbl19_1.SetWindowTextW(xml.str(IDS_STRING5900));

				xml.clean();
			}
		}

		m_bInitialized = TRUE;
	}

	return TRUE;
}

// CViewProgress message handlers
